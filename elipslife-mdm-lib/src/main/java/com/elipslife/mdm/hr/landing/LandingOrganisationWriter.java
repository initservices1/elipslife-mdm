package com.elipslife.mdm.hr.landing;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.LandingHrModelPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class LandingOrganisationWriter extends AbstractTableWriter<LandingOrganisationBean> {

	private LandingOrganisationReader reader;

	public LandingOrganisationWriter() {
		this(Constants.DataSpace.LANDING_HR_REFERENCE);
	}

	public LandingOrganisationWriter(String dataspace) {
		this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.LANDING_HR_MASTER),
				LandingHrModelPaths._Organisation.getPathInSchema()));
	}

	public LandingOrganisationWriter(AdaptationTable table) {
		super(table);
		reader = new LandingOrganisationReader(table);
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, LandingOrganisationBean bean) {
		values.put(LandingHrModelPaths._Organisation._Organisation_OrganisationId, bean.getOrganisationId());
		values.put(LandingHrModelPaths._Organisation._Organisation_DataOwner, bean.getDataOwner());
		values.put(LandingHrModelPaths._Organisation._Organisation_OrganisationBusId, bean.getOrganisationBusId());
		values.put(LandingHrModelPaths._Organisation._Organisation_OrganisationType, bean.getOrganisationType());
		values.put(LandingHrModelPaths._Organisation._Organisation_ParentBusId, bean.getParentBusId());
		values.put(LandingHrModelPaths._Organisation._Organisation_Name, bean.getName());
		values.put(LandingHrModelPaths._Organisation._Organisation_ManagerBusId, bean.getManagerBusId());
		values.put(LandingHrModelPaths._Organisation._Organisation_BuildingBusId, bean.getBuildingBusId());
		values.put(LandingHrModelPaths._Organisation._Organisation_IsActive, bean.getIsActive());
		//values.put(LandingHrModelPaths._Organisation._Control, bean.getControl());
		
		values.put(LandingHrModelPaths._Organisation._Control_SourceSystem, bean.getSourceSystem());
        values.put(LandingHrModelPaths._Organisation._Control_CreatedBy, bean.getCreatedBy());
        values.put(LandingHrModelPaths._Organisation._Control_CreationTimestamp, bean.getCreationTimeStamp());
        values.put(LandingHrModelPaths._Organisation._Control_LastUpdatedBy, bean.getLastUpdatedBy());
        values.put(LandingHrModelPaths._Organisation._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
        
	}

	@Override
	public AbstractTableReader<LandingOrganisationBean> getTableReader() {
		return reader;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub

	}
}
