package com.elipslife.mdm.hr.staging;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class STG_BuildingWriter extends AbstractTableWriter<STG_BuildingBean> {
	
	private STG_BuildingReader stgBuildingReader;
	private static String dataspaceName;

	public STG_BuildingWriter() {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Building.getPathInSchema()));
	}
	
	public STG_BuildingWriter(String dataspace) {
		
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Building.getPathInSchema()));
		
	}

	public STG_BuildingWriter(AdaptationTable table) {

		super(table);
		this.stgBuildingReader = new STG_BuildingReader(table);
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, STG_BuildingBean bean) {
		values.put(HrModelPaths._STG_Building._Building_BuildingId, bean.getBuildingId());
		values.put(HrModelPaths._STG_Building._Building_DataOwner, bean.getDataOwner());
		values.put(HrModelPaths._STG_Building._Building_BuildingBusId, bean.getBuildingBusId());
		values.put(HrModelPaths._STG_Building._Building_Name, bean.getName());
		values.put(HrModelPaths._STG_Building._Building_Street, bean.getStreet());
		values.put(HrModelPaths._STG_Building._Building_Street2, bean.getStreet2());
		values.put(HrModelPaths._STG_Building._Building_PostCode, bean.getPostCode());
		values.put(HrModelPaths._STG_Building._Building_POBox, bean.getPOBox());
		values.put(HrModelPaths._STG_Building._Building_POBoxPostCode, bean.getPOBoxPostCode());
		values.put(HrModelPaths._STG_Building._Building_POBoxTown, bean.getPOBoxTown());
		values.put(HrModelPaths._STG_Building._Building_Town, bean.getTown());
		values.put(HrModelPaths._STG_Building._Building_District, bean.getDistrict());
		values.put(HrModelPaths._STG_Building._Building_StateProvince, bean.getStateProvince());
		values.put(HrModelPaths._STG_Building._Building_Country, bean.getCountry());
		values.put(HrModelPaths._STG_Building._Building_IsActive, bean.getIsActive());
		values.put(HrModelPaths._STG_Building._Building_BuildingMaster, bean.getBuildingMaster());
		values.put(HrModelPaths._STG_Building._Control_SourceSystem, bean.getSourceSystem());
		values.put(HrModelPaths._STG_Building._Control_CreatedBy, bean.getCreatedBy());
		values.put(HrModelPaths._STG_Building._Control_CreationTimestamp, bean.getCreationTimeStamp());
		values.put(HrModelPaths._STG_Building._Control_LastUpdatedBy, bean.getLastUpdatedBy());
		values.put(HrModelPaths._STG_Building._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
		values.put(HrModelPaths._STG_Building._Control_LastSyncTimestamp, bean.getLastSyncTimestamp()); 
	}

	@Override
	public AbstractTableReader<STG_BuildingBean> getTableReader() {
		return stgBuildingReader;
	}

	public STG_BuildingReader getStgBuildingReader() {
		return stgBuildingReader;
	}

	public void setStgBuildingReader(STG_BuildingReader stgBuildingReader) {
		this.stgBuildingReader = stgBuildingReader;
	}

	public static String getDataspaceName() {
		return dataspaceName;
	}

	public static void setDataspaceName(String dataspaceName) {
		STG_BuildingWriter.dataspaceName = dataspaceName;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub

	}
}
