package com.elipslife.ebx.data.access;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;

public abstract class BasePartyReader<T> extends BaseBeanReader<T> {

	protected abstract Path getPathCrmId();
	
	protected BasePartyReader(AdaptationTable table) {
		super(table);
	}
	
	/**
	 * 
	 * @param partyId
	 * @return
	 */
	public T findBeanByPartyId(int partyId) {

		return super.findBeanByPks(new Object[] { partyId });
	}
	
	public Adaptation findRecordByPartyId(int partyId) {
		
		return super.findRecordByPks(new Object[] { partyId });
	}
	
	
	/**
	 * 
	 * @param sourceSystem
	 * @param sourceSystemPartyBusId
	 * @return
	 */
	public List<T> findBeansBySourceSystem(String sourceSystem, String sourceSystemPartyBusId) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		
		if(sourceSystem != null) {
			XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathControlSourceSystem(), sourceSystem);
			group.addPredicate(predicate1);
		}
		
		if(sourceSystemPartyBusId != null) {
			XPathPredicate predicate2 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathControlSourcePartyBusId(), sourceSystemPartyBusId);
			group.addPredicate(predicate2);
		}
		
		return readRecordsFor(group.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
	
	/**
	 * 
	 * @param sourceSystem
	 * @param sourceSystemPartyBusId
	 * @param validFrom
	 * @param validTo
	 * @return
	 */
	public List<T> findBeansBySourceSystem(String sourceSystem, String sourceSystemPartyBusId, Date validFrom, Date validTo) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		
		if(sourceSystem != null) {
			XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathControlSourceSystem(), sourceSystem);
			group.addPredicate(predicate1);
		}
		
		if(sourceSystemPartyBusId != null) {
			XPathPredicate predicate2 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathControlSourcePartyBusId(), sourceSystemPartyBusId);
			group.addPredicate(predicate2);
		}
		
		if(validFrom != null) {
			String validFromString = simpleDateFormat.format(validFrom);
			XPathPredicate predicate3 = new XPathPredicate(XPathPredicate.Operation.DateEqual, getPathControlValidFrom(), validFromString);
			group.addPredicate(predicate3);
		}
		
		if(validTo != null) {
			String validToString = simpleDateFormat.format(validTo);
			XPathPredicate predicate4 = new XPathPredicate(XPathPredicate.Operation.DateEqual, getPathControlValidTo(), validToString);
			group.addPredicate(predicate4);
		}
		
		return readRecordsFor(group.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
	
	/**
	 * Find beans by source system
	 * @param sourceSystems
	 * @param sourceSystemPartyBusId
	 * @param validFrom
	 * @param validTo
	 * @return
	 */
	public List<T> findBeansBySourceSystems(String[] sourceSystems, String sourceSystemPartyBusId, Date validFrom, Date validTo) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		
		XPathPredicateGroup groupSourceSystems = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.Or);
		
		for(String sourceSystem : sourceSystems) {
			XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathControlSourceSystem(), sourceSystem);
			groupSourceSystems.addPredicate(predicate1);
		}
		
		group.addPredicateGroup(groupSourceSystems);
		
		if(sourceSystemPartyBusId != null) {
			XPathPredicate predicate2 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathControlSourcePartyBusId(), sourceSystemPartyBusId);
			group.addPredicate(predicate2);
		}
		
		if(validFrom != null) {
			String validFromString = simpleDateFormat.format(validFrom);
			XPathPredicate predicate3 = new XPathPredicate(XPathPredicate.Operation.DateEqual, getPathControlValidFrom(), validFromString);
			group.addPredicate(predicate3);
		}
		
		if(validTo != null) {
			String validToString = simpleDateFormat.format(validTo);
			XPathPredicate predicate4 = new XPathPredicate(XPathPredicate.Operation.DateEqual, getPathControlValidTo(), validToString);
			group.addPredicate(predicate4);
		}
		
		return readRecordsFor(group.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
	/**
	 * Find beans by CRM ID
	 * @param crmId
	 * @return
	 */
	public List<T> findBeansByCrmId(String crmId) {
		
		XPathPredicate predicate = new XPathPredicate(XPathPredicate.Operation.Equals, getPathCrmId(), crmId);
		
		return readRecordsFor(predicate.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
}