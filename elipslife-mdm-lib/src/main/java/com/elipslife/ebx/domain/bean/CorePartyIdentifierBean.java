package com.elipslife.ebx.domain.bean;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class CorePartyIdentifierBean extends Entity<Integer> {
	
	private String partyId;
	private String sourceSystem;
	private String sourcePartyBusId;
	
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getSourcePartyBusId() {
		return sourcePartyBusId;
	}
	public void setSourcePartyBusId(String sourcePartyBusId) {
		this.sourcePartyBusId = sourcePartyBusId;
	}
}