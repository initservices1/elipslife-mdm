package com.elipslife.mdm.hr.constraint;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

import ch.butos.ebx.lib.validation.SmartRecordAndTableLevelValidationCheck;

public class OrganisationConstraint extends SmartRecordAndTableLevelValidationCheck{

	@Override
	protected void executeBusinessValidations(ValueContext recordContext,
			Map<SchemaNode, Set<UserMessage>> validationMessages, boolean firstRecord) {
		
		String buildingBusId = (String)recordContext.getValue(HrModelPaths._Organisation._Organisation_BuildingBusId);
		String buildingId = (String)recordContext.getValue(HrModelPaths._Organisation._Organisation_BuildingId);
		String managerBudId = (String)recordContext.getValue(HrModelPaths._Organisation._Organisation_ManagerBusId);
		String managerId = (String)recordContext.getValue(HrModelPaths._Organisation._Organisation_ManagerId);
		String parentId = (String)recordContext.getValue(HrModelPaths._Organisation._Organisation_ParentId);
		String parentBusId = (String)recordContext.getValue(HrModelPaths._Organisation._Organisation_ParentBusId);
		
		UserMessage userMsg = null;
		
		if(managerBudId != null && managerId == null) {
			if(!managerBudId.isEmpty()) {
				userMsg = UserMessage.createWarning("Manager Bus Id '" + managerBudId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(HrModelPaths._Organisation._Organisation_ManagerId), userMsg);
			}
		}
		
		if(buildingBusId != null && buildingId == null) {
			if(!buildingBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Building Bus Id '" + buildingBusId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(HrModelPaths._Organisation._Organisation_BuildingId), userMsg);
			}
		}
		
		if(parentBusId != null && parentId == null) {
			if(!parentBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Parent Bus Id '" + parentBusId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(HrModelPaths._Organisation._Organisation_ParentId), userMsg);
			}
		}
		
	}
	
	@Override
	public void setup(ConstraintContextOnTable aContext) {
		
		
	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		
		return null;
	}

}
