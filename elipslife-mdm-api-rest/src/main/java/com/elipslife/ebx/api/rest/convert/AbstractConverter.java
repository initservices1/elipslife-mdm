package com.elipslife.ebx.api.rest.convert;

import javax.xml.datatype.DatatypeConfigurationException;

public abstract class AbstractConverter<T, R> {
	
	public abstract T convert(R synchroniseObject) throws DatatypeConfigurationException;
}