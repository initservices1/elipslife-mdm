package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InsuranceContract {
	
	@JsonbProperty("DataOwner")
	@JsonProperty("DataOwner")
	public String dataOwner;
	
	@JsonbProperty("ContractNumber")
	@JsonProperty("ContractNumber")
	public String contractNumber;
	
	@JsonbProperty("ContractStageBusId")
	@JsonProperty("ContractStageBusId")
	public String contractStageBusId;
	
	@JsonbProperty("OfferNumber")
	@JsonProperty("OfferNumber")
	public String offerNumber;
	
	@JsonbProperty("ApplicationNumber")
	@JsonProperty("ApplicationNumber")
	public String applicationNumber;
	
	@JsonbProperty("ContractBusId2")
	@JsonProperty("ContractBusId2")
	public String contractBusId2;
	
	@JsonbProperty("SourceContractBusId")
	@JsonProperty("SourceContractBusId")
	public String sourceContractBusId;
	
	@JsonbProperty("CustomerRefNumber")
	@JsonProperty("CustomerRefNumber")
	public String customerRefNumber;
	
	
	@JsonbProperty("ContractStage")
	@JsonProperty("ContractStage")
	public String contractStage;
	
	@JsonbProperty("ContractState")
	@JsonProperty("ContractState")
	public String contractState;
	
	@JsonbProperty("OfferState")
	@JsonProperty("OfferState")
	public String offerState;
	
	@JsonbProperty("ApplicationState")
	@JsonProperty("ApplicationState")
	public String applicationState;
	
	@JsonbProperty("ContractClass")
	@JsonProperty("ContractClass")
	public String contractClass;
	
	@JsonbProperty("BusinessContractBeginDate")
	@JsonProperty("BusinessContractBeginDate")
	public LocalDate businessContractBeginDate;

	@JsonbProperty("ContractExpiryDate")
	@JsonProperty("ContractExpiryDate")
	public LocalDate contractExpiryDate;
	
	@JsonbProperty("ContractBeginDate")
	@JsonProperty("ContractBeginDate")
	public LocalDate contractBeginDate;
	
	@JsonbProperty("ContractEndDate")
	@JsonProperty("ContractEndDate")
	public LocalDate contractEndDate;
	
	@JsonbProperty("BusinessScope")
	@JsonProperty("BusinessScope")
	public String businessScope;
	
	@JsonbProperty("ContractName")
	@JsonProperty("ContractName")
	public String contractName;
	
	@JsonbProperty("AlternativeContractName")
	@JsonProperty("AlternativeContractName")
	public String alternativeContractName;
	
	@JsonbProperty("ClientSegment")
	@JsonProperty("ClientSegment")
	public String clientSegment;
	
	@JsonbProperty("DistributionAgreementBusId")
	@JsonProperty("DistributionAgreementBusId")
	public String distributionAgreementBusId;
	
	@JsonbProperty("IsActive")
	@JsonProperty("IsActive")
	public Boolean isActive;
	
	@JsonbProperty("IsProfitParticipation")
	@JsonProperty("IsProfitParticipation")
	public Boolean isProfitParticipation;
	
	@JsonbProperty("LineOfBusiness")
	@JsonProperty("LineOfBusiness")
	public String lineOfBusiness;
	
	@JsonbProperty("OperatingCountry")
	@JsonProperty("OperatingCountry")
	public String operatingCountry;
	
	@JsonbProperty("Exposures")
	@JsonProperty("Exposures")
	public Collection<Exposure> exposures;
	
	
	@JsonbProperty("AdminResponsible")
	@JsonProperty("AdminResponsible")
	public Employee adminResponsible;
	
	@JsonbProperty("ClaimResponsible")
	@JsonProperty("ClaimResponsible")
	public Employee claimResponsible;
	
	@JsonbProperty("CaseMgmtResponsible")
	@JsonProperty("CaseMgmtResponsible")
	public Employee caseMgmtResponsible;
	
	@JsonbProperty("SalesResponsible")
	@JsonProperty("SalesResponsible")
	public Employee salesResponsible;
	
	@JsonbProperty("Policyholder")
	@JsonProperty("Policyholder")
	public Party policyholder;
	
	@JsonbProperty("Broker")
	@JsonProperty("Broker")
	public Party broker;
	
	@JsonbProperty("AttachedPartner")
	@JsonProperty("AttachedPartner")
	public Party attachedPartner;
	
	
	@JsonbProperty("CreationTimestamp")
	@JsonProperty("CreationTimestamp")
	public LocalDateTime creationTimestamp;
	
	@JsonbProperty("LastUpdateTimestamp")
	@JsonProperty("LastUpdateTimestamp")
	public LocalDateTime lastUpdateTimestamp;
	
	@JsonbProperty("Action")
	@JsonProperty("Action")
	public String action;
}