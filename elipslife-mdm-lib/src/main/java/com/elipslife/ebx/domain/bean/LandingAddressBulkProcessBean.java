package com.elipslife.ebx.domain.bean;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class LandingAddressBulkProcessBean extends Entity<Integer> {
	
	private String addressType;
	private String addressUsage;
	private String sourceAddressBusId;
	private String street;
	private String street2;
	private String postCode;
	private String poBox;
	private String poBoxPostCode;
	private String poBoxTown;
	private String district;
	private String town;
	private String stateProvince;
	private String country;
	private String correlationId;
	private String landingPartyId;
	private String mdmStagingProcessResponse;
	private Date validFrom;
	private Date validTo;
	private String sourceSystem;
	private String sourcePartyBusId;
	private Date creationTimestamp;
	private Date lastUpdateTimestamp;
	private Date lastSyncTimeStamp;
	private String action;
	private String actionBy;
	private String electronicAddressType;
	private String address;
	
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getAddressUsage() {
		return addressUsage;
	}
	public void setAddressUsage(String addressUsage) {
		this.addressUsage = addressUsage;
	}
	public String getSourceAddressBusId() {
		return sourceAddressBusId;
	}
	public void setSourceAddressBusId(String sourceAddressBusId) {
		this.sourceAddressBusId = sourceAddressBusId;
	}
    public String getLandingPartyId() {
		return landingPartyId;
	}
	public void setLandingPartyId(String landingPartyId) {
		this.landingPartyId = landingPartyId;
	}
	public String getMdmStagingProcessResponse() {
		return mdmStagingProcessResponse;
	}
	public void setMdmStagingProcessResponse(String mdmStagingProcessResponse) {
		this.mdmStagingProcessResponse = mdmStagingProcessResponse;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getStreet2() {
		return street2;
	}
	public void setStreet2(String street2) {
		this.street2 = street2;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getPoBox() {
		return poBox;
	}
	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}
	public String getPoBoxPostCode() {
		return poBoxPostCode;
	}
	public void setPoBoxPostCode(String poBoxPostCode) {
		this.poBoxPostCode = poBoxPostCode;
	}
	public String getPoBoxTown() {
		return poBoxTown;
	}
	public void setPoBoxTown(String poBoxTown) {
		this.poBoxTown = poBoxTown;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getStateProvince() {
		return stateProvince;
	}
	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getSourcePartyBusId() {
		return sourcePartyBusId;
	}
	public void setSourcePartyBusId(String sourcePartyBusId) {
		this.sourcePartyBusId = sourcePartyBusId;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActionBy() {
		return actionBy;
	}
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	public String getElectronicAddressType() {
		return electronicAddressType;
	}
	public void setElectronicAddressType(String electronicAddressType) {
		this.electronicAddressType = electronicAddressType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public Date getUpdateTimestamp() {
		return getLastUpdateTimestamp() == null ? getCreationTimestamp() : getLastUpdateTimestamp();
	}
	public Date getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}
	public void setLastSyncTimeStamp(Date lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}
}