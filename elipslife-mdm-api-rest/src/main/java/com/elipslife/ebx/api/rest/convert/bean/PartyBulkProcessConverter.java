package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.Address;
import com.elipslife.ebx.api.rest.models.BankAccount;
import com.elipslife.ebx.api.rest.models.Party;
import com.elipslife.ebx.api.rest.models.SynchroniseParty;
import com.elipslife.ebx.data.access.LandingAddressBulkProcessWriterSession;
import com.elipslife.ebx.data.access.LandingBankAccountBulkProcessWriterSession;
import com.elipslife.ebx.data.access.LandingPartyBulkProcessWriterSession;
import com.elipslife.ebx.domain.bean.LandingAddressBulkProcessBean;
import com.elipslife.ebx.domain.bean.LandingBankAccountBulkProcessBean;
import com.elipslife.ebx.domain.bean.LandingPartyBulkProcessBean;
import com.orchestranetworks.service.Session;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class PartyBulkProcessConverter extends AbstractConverter<LandingPartyBulkProcessBean[], SynchroniseParty> {

    ArrayList<LandingPartyBulkProcessBean> landingPartyBeans = new ArrayList<>();
    ArrayList<LandingAddressBulkProcessBean> landingAddressBeans = new ArrayList<>();
    ArrayList<LandingBankAccountBulkProcessBean> landingBankAccountBeans = new ArrayList<>();
    Session session;

    @Override
    public LandingPartyBulkProcessBean[] convert(SynchroniseParty synchroniseParty) throws DatatypeConfigurationException {

        for (Party party : synchroniseParty.data) {

            LandingPartyBulkProcessBean landingPartyBean = new LandingPartyBulkProcessBean();
            landingPartyBeans.add(landingPartyBean);

            Date dateOfBirth = party.dateOfBirth == null ? null : DateHelper.convertToDate(party.dateOfBirth);
            Date creationTimeStamp = party.creationTimestamp == null ? null : Date.from(party.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
            Date lastUpdateTimestamp = party.lastUpdateTimestamp == null ? null : Date.from(party.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
            Date validFrom = party.validFrom == null ? null : DateHelper.convertToDate(party.validFrom);
            Date validTo = party.validTo == null ? null : DateHelper.convertToDate(party.validTo);
            Date idIssueDate = (party.identityDocument == null || party.identityDocument.issueDate == null) ? null : DateHelper.convertToDate(party.identityDocument.issueDate);
            Date idExpiryDate = (party.identityDocument == null || party.identityDocument.expiryDate == null) ? null : DateHelper.convertToDate(party.identityDocument.expiryDate);

            landingPartyBean.setDataOwner(party.dataOwner);
            landingPartyBean.setOperatingCountry(party.operatingCountry);
            landingPartyBean.setPartyType(party.partyType);
            landingPartyBean.setCrmId(party.crmId);
            landingPartyBean.setRelationshipOwner(party.relationshipOwner);
            landingPartyBean.setDataResponsible(party.dataResponsible);
            landingPartyBean.setLanguage(party.language);
            landingPartyBean.setCorrelationId(synchroniseParty.correlationId);  // correlation id
            landingPartyBean.setIsActive(party.isActive);
            landingPartyBean.setFirstName(party.firstName);
            landingPartyBean.setMiddleName(party.middleName);
            landingPartyBean.setLastName(party.lastName);
            landingPartyBean.setDateOfBirth(dateOfBirth);
            landingPartyBean.setGender(party.gender);
            landingPartyBean.setTitle(party.title);
            landingPartyBean.setSalutation(party.salutation);
            landingPartyBean.setLetterSalutation(party.letterSalutation);
            landingPartyBean.setCivilStatus(party.civilStatus);
            landingPartyBean.setBirthCity(party.birthCity);
            landingPartyBean.setBirthCountry(party.birthCountry);
            landingPartyBean.setIdDocumentNumber(party.identityDocument == null ? null : party.identityDocument.documentNumber);
            landingPartyBean.setIdDocumentType(party.identityDocument == null ? null : party.identityDocument.documentType);
            landingPartyBean.setIdIssueDate(idIssueDate);
            landingPartyBean.setIdExpiryDate(idExpiryDate);
            landingPartyBean.setIdIssuingAuthority(party.identityDocument == null ? null : party.identityDocument.issuingAuthority);
            landingPartyBean.setSourceParentBusId(party.parentBusId);
            landingPartyBean.setOrgName(party.orgName);
            landingPartyBean.setOrgName2(party.orgName2);
            landingPartyBean.setCompanyType(party.companyType);
            landingPartyBean.setCommercialRegNo(party.commercialRegNo);
            landingPartyBean.setUidNumber(party.uidNumber);
            landingPartyBean.setVatNumber(party.vatNumber);
            landingPartyBean.setIndustry(party.industry);
            landingPartyBean.setSubIndustry(party.subIndustry);
            landingPartyBean.setPreferredEmail(party.preferredEmail);
            landingPartyBean.setPreferredCommChannel(party.preferredCommChannel);
            landingPartyBean.setSourceOrganisationBusId(party.organisationBusId);
            landingPartyBean.setSourceLineManagerBusId(party.lineManagerBusId);
            landingPartyBean.setSourcePartnerBusId(party.partnerBusId);
            landingPartyBean.setDepartment(party.department);
            landingPartyBean.setFunction(party.function);
            landingPartyBean.setIsExecutor(party.isExecutor);
            landingPartyBean.setIsLegalRepresentative(party.isLegalRepresentative);
            landingPartyBean.setIsSeniorManager(party.isSeniorManager);
            landingPartyBean.setIsUltimateBeneficialOwner(party.isUltimateBeneficialOwner);
            landingPartyBean.setBoRelationship(party.boRelationship);
            landingPartyBean.setContactPersonState(party.contactPersonState);
            landingPartyBean.setRoles(party.roles);
            landingPartyBean.setIsVipClient(party.isVipClient);
            landingPartyBean.setBlacklist(party.blacklist);
            landingPartyBean.setPartnerState(party.partnerState);
            landingPartyBean.setCompanyPersonalNr(party.companyPersonalNr);
            landingPartyBean.setSocialSecurityNr(party.socialSecurityNr);
            landingPartyBean.setSocialSecurityNrType(party.socialSecurityNrType);
            landingPartyBean.setBrokerRegNo(party.brokerRegNo);
            landingPartyBean.setBrokerTiering(party.brokerTiering);
            landingPartyBean.setLineOfBusiness(party.lineOfBusiness);
            landingPartyBean.setFinancePartyBusId(party.financePartyBusId);
            landingPartyBean.setPaymentTerms(party.paymentTerms);
            landingPartyBean.setFinanceClients(party.financeClients);
            landingPartyBean.setCreditorClients(party.creditorClients);
            landingPartyBean.setSourceSystem(synchroniseParty.context.sourceSystemId);
            landingPartyBean.setSourcePartyBusId(party.partyBusId);
            landingPartyBean.setValidFrom(validFrom);
            landingPartyBean.setValidTo(validTo);
            landingPartyBean.setCreationTimestamp(creationTimeStamp);
            landingPartyBean.setLastUpdateTimestamp(lastUpdateTimestamp);
            landingPartyBean.setAction(party.action);
            landingPartyBean.setActionBy(synchroniseParty.context.userId);

            LandingPartyBulkProcessWriterSession landingPartyWriterSession = new LandingPartyBulkProcessWriterSession(this.session, true);
            Optional<LandingPartyBulkProcessBean> landingPartyBulkProcessBeanOptional = landingPartyWriterSession.createBeanOrThrow(landingPartyBean);
            landingPartyBulkProcessBeanOptional.get().getId();
            
            // Don't process future party address
            boolean isWithinRange = dateWithinRange(landingPartyBean.getValidFrom(), landingPartyBean.getValidTo());
            if (!isWithinRange) {
            	continue;
            }
            
            // Process Addresses
            for (Address address : (party.addresses == null ? new ArrayList<Address>() : party.addresses)) {

                LandingAddressBulkProcessWriterSession landingAddressWriterSession = new LandingAddressBulkProcessWriterSession(this.session, true);
                LandingAddressBulkProcessBean landingAddressBean = new LandingAddressBulkProcessBean();
                landingAddressBeans.add(landingAddressBean);

                Date creationTimeStampAddress = address.creationTimestamp == null ? null : Date.from(address.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
                Date lastUpdateTimestampAddress = address.lastUpdateTimestamp == null ? null : Date.from(address.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
                Date validFromAddress = address.validFrom == null ? null : DateHelper.convertToDate(address.validFrom);
                Date validToAddress = address.validTo == null ? null : DateHelper.convertToDate(address.validTo);

                landingAddressBean.setAddressType(address.addressType);
                landingAddressBean.setAddressUsage(address.addressUse);
                landingAddressBean.setSourceAddressBusId(address.sourceAddressBusId);
                landingAddressBean.setLandingPartyId(String.valueOf(landingPartyBulkProcessBeanOptional.get().getId()));
                landingAddressBean.setStreet(address.street);
                landingAddressBean.setStreet2(address.street2);
                landingAddressBean.setPostCode(address.postCode);
                landingAddressBean.setPoBox(address.poBox);
                landingAddressBean.setPoBoxPostCode(address.poBoxPostCode);
                landingAddressBean.setPoBoxTown(address.poBoxTown);
                landingAddressBean.setDistrict(address.district);
                landingAddressBean.setTown(address.town);
                landingAddressBean.setStateProvince(address.stateProvince);
                landingAddressBean.setCountry(address.country);
                landingAddressBean.setElectronicAddressType(address.electronicAddressType);
                landingAddressBean.setAddress(address.address);
                landingAddressBean.setCorrelationId(synchroniseParty.correlationId); // correlation id
                landingAddressBean.setSourceSystem(synchroniseParty.context.sourceSystemId);
                landingAddressBean.setSourcePartyBusId(party.partyBusId);
                landingAddressBean.setValidFrom(validFromAddress);
                landingAddressBean.setValidTo(validToAddress);
                landingAddressBean.setCreationTimestamp(creationTimeStampAddress);
                landingAddressBean.setLastUpdateTimestamp(lastUpdateTimestampAddress);
                landingAddressBean.setAction(address.action);
                landingAddressBean.setActionBy(synchroniseParty.context.userId);
                
                landingAddressWriterSession.createBeanOrThrow(landingAddressBean);
            }
            
            // Process Bank Accounts
            for (BankAccount bankAccount : (party.bankAccounts == null ? new ArrayList<BankAccount>() : party.bankAccounts)) {
                
                LandingBankAccountBulkProcessWriterSession landingBankAccountWriterSession = new LandingBankAccountBulkProcessWriterSession(this.session, true);
                LandingBankAccountBulkProcessBean landingBankAccountBean = new LandingBankAccountBulkProcessBean();
                landingBankAccountBeans.add(landingBankAccountBean);

                Date creationTimeStampBankAccount = bankAccount.creationTimestamp == null ? null : Date.from(bankAccount.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
                Date lastUpdateTimestampBankAccount = bankAccount.lastUpdateTimestamp == null ? null : Date.from(bankAccount.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
                Date validFromBankAccount = bankAccount.validFrom == null ? null : DateHelper.convertToDate(bankAccount.validFrom);
                Date validToBankAccount = bankAccount.validTo == null ? null : DateHelper.convertToDate(bankAccount.validTo);

                landingBankAccountBean.setAccountType(bankAccount.accountType);
                landingBankAccountBean.setAccountNumber(bankAccount.accountNumber);
                landingBankAccountBean.setIban(bankAccount.iban);
                landingBankAccountBean.setBic(bankAccount.bic);
                landingBankAccountBean.setClearingNumber(bankAccount.clearingNumber);
                landingBankAccountBean.setBankName(bankAccount.bankName);
                landingBankAccountBean.setCountry(bankAccount.country);
                landingBankAccountBean.setCurrency(bankAccount.currency);
                landingBankAccountBean.setIsMainAccount(bankAccount.isMainAccount);
                landingBankAccountBean.setSourceBankAccountBusId(bankAccount.sourceBankAccountBusId);
                landingBankAccountBean.setLandingPartyId(String.valueOf(landingPartyBulkProcessBeanOptional.get().getId()));
                landingBankAccountBean.setCorrelationId(synchroniseParty.correlationId);  // correlation id
                landingBankAccountBean.setSourceSystem(synchroniseParty.context.sourceSystemId);
                landingBankAccountBean.setSourcePartyBusId(party.partyBusId);
                landingBankAccountBean.setValidFrom(validFromBankAccount);
                landingBankAccountBean.setValidTo(validToBankAccount);
                landingBankAccountBean.setCreationTimestamp(creationTimeStampBankAccount);
                landingBankAccountBean.setLastUpdateTimestamp(lastUpdateTimestampBankAccount);
                landingBankAccountBean.setAction(bankAccount.action);
                landingBankAccountBean.setActionBy(synchroniseParty.context.userId);
                
                landingBankAccountWriterSession.createBeanOrThrow(landingBankAccountBean);
            }
        }

        return null;
    }

    public LandingPartyBulkProcessBean[] convert(Session session, SynchroniseParty synchroniseParty) throws DatatypeConfigurationException {

        this.session = session;
        
        // Convert Rest Party request
        convert(synchroniseParty);
        
        landingPartyBeans.clear();
        landingAddressBeans.clear();
        landingBankAccountBeans.clear();

        return null;
    }
    
    /**
     * 
     * @param dateFrom
     * @param dateTo
     * @return
     */
    private static boolean dateWithinRange(Date dateFrom, Date dateTo) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date dateNow = calendar.getTime();

        Date dateMinimum = new Date(Long.MIN_VALUE);
        Date dateMaximum = new Date(Long.MAX_VALUE);

        return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0 &&
            dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
    }

}
