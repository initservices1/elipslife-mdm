package com.elipslife.mdm.party.scheduler;

import java.util.List;

import com.elipslife.ebx.data.access.CoreAddressBuilder;
import com.elipslife.ebx.data.access.StagingAddressReader;
import com.elipslife.ebx.data.access.StagingAddressWriter;
import com.elipslife.ebx.data.access.StagingAddressWriterSession;
import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;

public class MigrateAddressFromStagingToCoreTask extends MigrateFromStagingToCoreTaskBase {

	@Override
	public void execute(ScheduledExecutionContext scheduledExecutionContext) {
		
  		StagingAddressWriter stagingAddressWriter = new StagingAddressWriterSession(scheduledExecutionContext.getSession());
		StagingAddressReader stagingAddressReader = (StagingAddressReader) stagingAddressWriter.getTableReader();

		List<StagingAddressBean> stagingAddressBeans = stagingAddressReader.findBeansUpToDate();

		stagingAddressBeans.forEach(sab ->
		{
			try {
				CoreAddressBuilder.createOrUpdateCoreAddressByStagingAddress(scheduledExecutionContext.getSession(), sab);
			}
			catch(Exception ex) {
				scheduledExecutionContext.addExecutionInformation(String.format("Migration error for staging address with ID %s:\r\n%s", sab.getId(), ex.toString()));
			}
		});
	}
}