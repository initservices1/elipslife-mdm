package com.elipslife.mdm.party;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PartyFunctionTest {

	@Test
	public void standardizeSearchText() {
		
		String input = "Hello Mr. ÄäüÜöÖéà";
		
		String output = PartyFunction.standardizeSearchText(input);
		
		assertEquals(output, "HELLOMRAEAEUEUEOEOEEA");
	}
}