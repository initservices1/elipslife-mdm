package com.elipslife.mdm.hr.staging;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.elipslife.mdm.hr.constants.LandingHrModelPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class STG_EmployeeReader extends AbstractTableReader<STG_EmployeeBean> {

    public STG_EmployeeReader() {
    	this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Employee.getPathInSchema()));
    }
    
    public STG_EmployeeReader(String dataspace) {
    	this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Employee.getPathInSchema()));
    }

    public STG_EmployeeReader(AdaptationTable table) {
        super(table);
    }

    @Override
    public STG_EmployeeBean createBeanFromRecord(Adaptation record) {
        STG_EmployeeBean bean = new STG_EmployeeBean();
        bean.setEmployeeId(record.get_int(HrModelPaths._STG_Employee._Employee_EmployeeId));
        bean.setDataOwner(record.getString(HrModelPaths._STG_Employee._Employee_DataOwner));
        bean.setEmployeeBusId(record.getString(HrModelPaths._STG_Employee._Employee_EmployeeBusId));
        bean.setUserName(record.getString(HrModelPaths._STG_Employee._Employee_UserName));
        bean.setFirstName(record.getString(HrModelPaths._STG_Employee._Employee_FirstName));
        bean.setPreferredFirstName(record.getString(HrModelPaths._STG_Employee._Employee_PreferredFirstName));
        bean.setMiddleName(record.getString(HrModelPaths._STG_Employee._Employee_MiddleName));
        bean.setLastName(record.getString(HrModelPaths._STG_Employee._Employee_LastName));
        bean.setGender(record.getString(HrModelPaths._STG_Employee._Employee_Gender));
        bean.setSalutation(record.getString(HrModelPaths._STG_Employee._Employee_Salutation));
        bean.setLanguage(record.getString(HrModelPaths._STG_Employee._Employee_Language));
        bean.setEmail(record.getString(HrModelPaths._STG_Employee._Employee_Email));
        bean.setTelephone(record.getString(HrModelPaths._STG_Employee._Employee_Telephone));
        bean.setMobile(record.getString(HrModelPaths._STG_Employee._Employee_Mobile));
        bean.setFax(record.getString(HrModelPaths._STG_Employee._Employee_Fax));
        bean.setIsActive(record.get_boolean(HrModelPaths._STG_Employee._Employee_IsActive));
        // bean.setEmployeeMaster(record.getString(HrModelPaths._STG_Employee._Employee_EmployeeMaster));
        bean.setIsSystemEnabled(record.get_boolean(HrModelPaths._STG_Employee._Employee_IsSystemEnabled));
        bean.setEmploymentCategory(record.getString(HrModelPaths._STG_Employee._Employment_EmploymentCategory));
        bean.setManagementLevel(record.getString(HrModelPaths._STG_Employee._Employment_ManagementLevel));
        bean.setSignOffRole(record.getString(HrModelPaths._STG_Employee._Employment_SignOffRole));
        bean.setFunctionalTitle(record.getString(HrModelPaths._STG_Employee._JobPosition_FunctionalTitle));
        bean.setPositionName(record.getString(HrModelPaths._STG_Employee._JobPosition_PositionName));
        bean.setJobProfileName(record.getString(HrModelPaths._STG_Employee._JobPosition_JobProfileName));
        bean.setIsLineManager(record.get_boolean(HrModelPaths._STG_Employee._JobPosition_IsLineManager));
        bean.setOrganisationBusId(record.getString(HrModelPaths._STG_Employee._JobPosition_OrganisationBusId));
        bean.setLineManagerBusId(record.getString(HrModelPaths._STG_Employee._JobPosition_LineManagerBusId));
        bean.setDottedLineManagerBusId(record.getString(HrModelPaths._STG_Employee._JobPosition_DottedLineManagerBusId));
        bean.setBuildingBusId(record.getString(HrModelPaths._STG_Employee._WorkplaceLocation_BuildingBusId));
        
        //Employee Private Address
        bean.setPrivateStreet(record.getString(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivateStreet));
        bean.setPrivatePostCode(record.getString(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivatePostCode));
        bean.setPrivateTown(record.getString(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivateTown));
        bean.setPrivateStateProvince(record.getString(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivateStateProvince));
        bean.setPrivateCountryCode(record.getString(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivateCountryCode));
        
        bean.setEmployerBusId(record.getString(HrModelPaths._STG_Employee._EmployerCompany_EmployerBusId));
        bean.setSourceSystem(record.getString(HrModelPaths._STG_Employee._Control_SourceSystem));
        bean.setCreatedBy(record.getString(HrModelPaths._STG_Employee._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(HrModelPaths._STG_Employee._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(HrModelPaths._STG_Employee._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(HrModelPaths._STG_Employee._Control_LastUpdateTimestamp));
        bean.setLastSyncTimestamp(record.getDate(HrModelPaths._STG_Employee._Control_LastSyncTimestamp));
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{ HrModelPaths._Employee._Employee_EmployeeId };
    }

    @Override
    public Object[] retrievePks(STG_EmployeeBean bean) {
        return new Object[]{bean.getEmployeeId()};
    }
    
    /**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}
}
