package com.elipslife.mdm.uw.landing;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class LandingUWCaseWriterProcedureContext extends LandingUWCaseWriter{
	
	private final ProcedureContext procedureContext;

	public LandingUWCaseWriterProcedureContext(ProcedureContext procedureContext) {
		super();
		this.procedureContext = procedureContext;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
