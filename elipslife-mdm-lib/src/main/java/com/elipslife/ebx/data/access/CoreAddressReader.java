package com.elipslife.ebx.data.access;

import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.CoreAddressBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.xpath.filter.XPathPredicateGroup;

public class CoreAddressReader extends BaseAddressReader<CoreAddressBean> {

	public CoreAddressReader() {
		super(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
				PartyPaths._Address.getPathInSchema()));
	}
	
	public CoreAddressReader(String dataSpace) {
        super(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.PARTY_MASTER),
                PartyPaths._Address.getPathInSchema()));
    }
	
	
	public CoreAddressReader(AdaptationTable table) {
		super(table);
	}
	

	@Override
	public CoreAddressBean createBeanFromRecord(Adaptation record) {
		
		CoreAddressBean coreAddressBean = new CoreAddressBean();
		
		coreAddressBean.setId(record.get_int(PartyPaths._Address._AddressInfo_AddressId));
		
		coreAddressBean.setPartyId(record.getString(PartyPaths._Address._AddressInfo_PartyId));
		coreAddressBean.setAddressType(record.getString(PartyPaths._Address._AddressInfo_AddressType));
		coreAddressBean.setAddressUsage(record.getString(PartyPaths._Address._AddressInfo_AddressUse));
		coreAddressBean.setSourceAddressBusId(record.getString(PartyPaths._Address._AddressInfo_SourceAddressBusId));
		coreAddressBean.setStreet(record.getString(PartyPaths._Address._PostalAddress_Street));
		coreAddressBean.setStreet2(record.getString(PartyPaths._Address._PostalAddress_Street2));
		coreAddressBean.setTown(record.getString(PartyPaths._Address._PostalAddress_Town));
		coreAddressBean.setPostCode(record.getString(PartyPaths._Address._PostalAddress_PostCode));
		coreAddressBean.setPoBox(record.getString(PartyPaths._Address._PostalAddress_POBox));
		coreAddressBean.setPoBoxPostCode(record.getString(PartyPaths._Address._PostalAddress_POBoxPostCode));
		coreAddressBean.setPoBoxTown(record.getString(PartyPaths._Address._PostalAddress_POBoxTown));
		coreAddressBean.setDistrict(record.getString(PartyPaths._Address._PostalAddress_District));
		coreAddressBean.setStateProvince(record.getString(PartyPaths._Address._PostalAddress_StateProvince));
		coreAddressBean.setCountry(record.getString(PartyPaths._Address._PostalAddress_Country));
		coreAddressBean.setElectronicAddressType(record.getString(PartyPaths._Address._ElectronicAddress_ElectronicAddressType));
		coreAddressBean.setAddress(record.getString(PartyPaths._Address._ElectronicAddress_Address));
		coreAddressBean.setSourceSystem(record.getString(PartyPaths._Address._Control_SourceSystem));
		coreAddressBean.setSourcePartyBusId(record.getString(PartyPaths._Address._Control_SourcePartyBusId));
		coreAddressBean.setCreatedBy(record.getString(PartyPaths._Address._Control_CreatedBy));
		coreAddressBean.setCreationTimestamp(record.getDate(PartyPaths._Address._Control_CreationTimestamp));
		coreAddressBean.setLastUpdatedBy(record.getString(PartyPaths._Address._Control_LastUpdatedBy));
		coreAddressBean.setLastUpdateTimestamp(record.getDate(PartyPaths._Address._Control_LastUpdateTimestamp));
		coreAddressBean.setLastSyncAction(record.getString(PartyPaths._Address._Control_LastSyncAction));
		coreAddressBean.setLastSyncTimestamp(record.getDate(PartyPaths._Address._Control_LastSyncTimestamp));
		coreAddressBean.setValidFrom(record.getDate(PartyPaths._Address._Control_ValidFrom));
		coreAddressBean.setValidTo(record.getDate(PartyPaths._Address._Control_ValidTo));
		coreAddressBean.setIsExpired((Boolean)record.get(PartyPaths._Address._Control_IsExpired));
		
		return coreAddressBean;
	}
	

	@Override
	protected Path[] getPkPaths() {
		
		return new Path[] { PartyPaths._STG_Address._Address_AddressId };
	}
	

	@Override
	protected Object[] retrievePks(CoreAddressBean bean) {

		return new Object[] { bean.getId() };
	}
	

	/**
	 * Finds beans by party ID
	 * @param partyId
	 * @return
	 */
	public List<CoreAddressBean> findBeansByPartyId(int partyId) {
		
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._Address._AddressInfo_PartyId, String.valueOf(partyId));
		
		return readRecordsFor(predicate1.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
	/**
	 * Find Core Addresses by Source Systems and SourcePartyBusid
	 * 
	 * @param sourceSystem
	 * @param sourceSystemPartyBusId
	 * @return
	 */
	public List<CoreAddressBean> findBeansBySourceSystemAndPartyBusId(String sourceSystem, String sourceSystemPartyBusId) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._Address._Control_SourceSystem, sourceSystem);
		group.addPredicate(predicate1);
		XPathPredicate predicate2 = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._Address._Control_SourcePartyBusId, sourceSystemPartyBusId);
		group.addPredicate(predicate2);
		
		return readRecordsFor(group.toString()).stream().map(this::createBeanFromRecord).collect(Collectors.toList());
	}
	

	@Override
	protected Path getPathAddressId() {

		return PartyPaths._Address._AddressInfo_AddressId;
	}
	
	
	@Override
	protected Path getPathSourceAddressBusId() {
		
		return PartyPaths._Address._AddressInfo_SourceAddressBusId;
	}

	
	@Override
	protected Path getPathControlSourceSystem() {

		return PartyPaths._Address._Control_SourceSystem;
	}

	
	@Override
	protected Path getPathControlSourcePartyBusId() {

		return null;
	}
	

	@Override
	protected Path getPathControlValidFrom() {

		return PartyPaths._Address._Control_ValidFrom;
	}
	

	@Override
	protected Path getPathControlValidTo() {

		return PartyPaths._Address._Control_ValidTo;
	}
}