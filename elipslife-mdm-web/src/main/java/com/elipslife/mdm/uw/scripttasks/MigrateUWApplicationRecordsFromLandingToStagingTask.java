package com.elipslife.mdm.uw.scripttasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.elipslife.mdm.uw.landing.LandingUWApplicationBean;
import com.elipslife.mdm.uw.landing.LandingUWApplicationReader;
import com.elipslife.mdm.uw.landing.LandingUWApplicationWriter;
import com.elipslife.mdm.uw.landing.LandingUWApplicationWriterSession;
import com.elipslife.mdm.uw.staging.StagingUWApplicationBean;
import com.elipslife.mdm.uw.staging.StagingUWApplicationWriter;
import com.elipslife.mdm.uw.staging.StagingUWApplicationWriterSession;
import com.onwbp.boot.VM;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class MigrateUWApplicationRecordsFromLandingToStagingTask extends ScriptTaskBean {
	
	public static final int BATCH_SIZE = 1000;
	
	@Override
	public void executeScript(ScriptTaskBeanContext aContext) throws OperationException {
		
		// migrate UW Application records
		migrateUWApplicationFromLandingToStaging(aContext);
	}
	
	/**
	 * Migrate UWApplication records from Landing to Staging
	 * 
	 * @param aContext
	 */
	private void migrateUWApplicationFromLandingToStaging(ScriptTaskBeanContext aContext) {

		LandingUWApplicationWriter landingUWApplicationWriter = new LandingUWApplicationWriterSession(
				aContext.getSession());
		LandingUWApplicationReader landingUWApplicationReader = (LandingUWApplicationReader) landingUWApplicationWriter
				.getTableReader();
		List<LandingUWApplicationBean> landingUWApplicationBeans = landingUWApplicationReader.readAllBeans();

		StagingUWApplicationWriter stagingUWApplicationWriter = new StagingUWApplicationWriterSession(
				aContext.getSession());
		
		migrateUWApplicationRecordsInBatchProcessing(landingUWApplicationBeans, landingUWApplicationWriter, stagingUWApplicationWriter, landingUWApplicationReader);
	}
	
	/**
	 * 
	 * Migrate UWApplication records in Batch processing
	 * 
	 * @param landingUWApplicationBeans
	 * @param landingUWApplicationWriter
	 * @param stagingUWApplicationWriter
	 */
	private void migrateUWApplicationRecordsInBatchProcessing(List<LandingUWApplicationBean> landingUWApplicationBeans, LandingUWApplicationWriter landingUWApplicationWriter, 
			StagingUWApplicationWriter stagingUWApplicationWriter, LandingUWApplicationReader applicationReader)
	{
		List<LandingUWApplicationBean> landingUWApplicationBatch = new ArrayList<LandingUWApplicationBean>();
		for(LandingUWApplicationBean luwcb : landingUWApplicationBeans) 
		{
			// If any validation error messages in Landing record, It should stay in Landing
			boolean hasValidationErrors = applicationReader.hasValidationErrors(new Object[] { luwcb.getUWApplicationId() });
			if(hasValidationErrors) {
				continue ;
			}
			if(landingUWApplicationBatch.size() == BATCH_SIZE) 
			{
				for(LandingUWApplicationBean luwc : landingUWApplicationBatch) {
					try 
					{
						StagingUWApplicationBean stagingUWCaseBean = createStagingUWApplicationBeanFromLandingUWApplicationBean(luwc);
						stagingUWApplicationWriter.createBeanOrThrow(stagingUWCaseBean);

						// Delete Landing UW Application record
						landingUWApplicationWriter.delete(luwc);
					} catch (Exception ex) {
						VM.log.kernelInfo(String.format("Migration error for landing UW Application with ID %s:\r\n%s",
								luwc.getUWApplicationId(), ex.toString()));
					}
				}
				landingUWApplicationBatch.clear();
			}
			landingUWApplicationBatch.add(luwcb);
		}
		
		if(landingUWApplicationBeans.size() != 0) {
			for(LandingUWApplicationBean luwc : landingUWApplicationBatch) 
			{
				boolean hasValidationErrors = applicationReader.hasValidationErrors(new Object[] { luwc.getUWApplicationId() });
				if(hasValidationErrors) {
					continue ;
				}
				try 
				{
					StagingUWApplicationBean stagingUWCaseBean = createStagingUWApplicationBeanFromLandingUWApplicationBean(luwc);
					stagingUWApplicationWriter.createBeanOrThrow(stagingUWCaseBean);
					// Delete Landing UW Application record
					landingUWApplicationWriter.delete(luwc);
		
				} catch (Exception ex) {
					VM.log.kernelInfo(String.format("Migration error for landing UW Application with ID %s:\r\n%s",
							luwc.getUWApplicationId(), ex.toString()));
				}
			}
		}
		landingUWApplicationBeans.clear();
	}
	
	private static StagingUWApplicationBean createStagingUWApplicationBeanFromLandingUWApplicationBean(
			LandingUWApplicationBean landingUWApplicationBean) {

		StagingUWApplicationBean stagingUWApplicationBean = new StagingUWApplicationBean();
		fillStagingUWApplicationWithLandingUWApplication(stagingUWApplicationBean, landingUWApplicationBean);
		return stagingUWApplicationBean;
	}

	public static void fillStagingUWApplicationWithLandingUWApplication(
			StagingUWApplicationBean stagingUWApplicationBean, LandingUWApplicationBean landingUWApplicationBean) {

		stagingUWApplicationBean.setUWApplicationBusId(landingUWApplicationBean.getUWApplicationBusId());
		stagingUWApplicationBean.setSourceSystem(landingUWApplicationBean.getSourceSystem());
		if(landingUWApplicationBean.getDataOwner() == null || landingUWApplicationBean.getDataOwner() == ""){
			stagingUWApplicationBean.setDataOwner("0");
		} else {
			stagingUWApplicationBean.setDataOwner(landingUWApplicationBean.getDataOwner());
		}
		stagingUWApplicationBean.setOperatingCountry(landingUWApplicationBean.getOperatingCountry());
		stagingUWApplicationBean.setPolicyholderBusId(landingUWApplicationBean.getPolicyholderBusId());
		stagingUWApplicationBean.setPolicyholderName(landingUWApplicationBean.getPolicyholderName());
		stagingUWApplicationBean.setMainAffiliateBusId(landingUWApplicationBean.getMainAffiliateBusId());
		stagingUWApplicationBean.setMainAffiliateFirstName(landingUWApplicationBean.getMainAffiliateFirstName());
		stagingUWApplicationBean.setMainAffiliateLastName(landingUWApplicationBean.getMainAffiliateLastName());
		stagingUWApplicationBean.setMainAffiliateDateOfBirth(landingUWApplicationBean.getMainAffiliateDateOfBirth());
		stagingUWApplicationBean.setMainAffiliateSSN(landingUWApplicationBean.getMainAffiliateSSN());
		stagingUWApplicationBean.setMainAffiliateTown(landingUWApplicationBean.getMainAffiliateTown());
		stagingUWApplicationBean.setMainAffiliatePostCode(landingUWApplicationBean.getMainAffiliatePostCode());
		stagingUWApplicationBean.setMainAffiliateCountry(landingUWApplicationBean.getMainAffiliateCountry());
		stagingUWApplicationBean.setSubmissionDate(landingUWApplicationBean.getSubmissionDate());
		if(landingUWApplicationBean.getIsActive() == null) {
			stagingUWApplicationBean.setIsActive(true);
		} else {
			stagingUWApplicationBean.setIsActive(landingUWApplicationBean.getIsActive());
		}
		stagingUWApplicationBean.setDocSignId(landingUWApplicationBean.getDocSignId());
		stagingUWApplicationBean.setSFSignedDate(landingUWApplicationBean.getSFSignedDate());
		stagingUWApplicationBean.setCreatedBy(landingUWApplicationBean.getCreatedBy());
		stagingUWApplicationBean.setCreationTimeStamp(landingUWApplicationBean.getCreationTimeStamp());
		stagingUWApplicationBean.setLastUpdatedBy(landingUWApplicationBean.getLastUpdatedBy());
		stagingUWApplicationBean.setLastUpdateTimeStamp(landingUWApplicationBean.getLastUpdateTimeStamp());
		stagingUWApplicationBean.setDeletionTimeStamp(landingUWApplicationBean.getDeletionTimeStamp());
		stagingUWApplicationBean.setLastSyncTimeStamp(new Date());
		stagingUWApplicationBean.setAction(landingUWApplicationBean.getAction());
	}	
}
