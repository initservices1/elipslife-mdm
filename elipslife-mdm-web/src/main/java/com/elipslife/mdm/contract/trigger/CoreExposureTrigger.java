package com.elipslife.mdm.contract.trigger;

import java.util.Date;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.ContractPaths;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class CoreExposureTrigger extends TableTrigger {

	@Override
	public void setup(TriggerSetupContext context) {
		
	}
	
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException {
		super.handleBeforeCreate(context);
		
		ValueContextForUpdate valueContextForUpdate = context.getOccurrenceContextForUpdate();
		valueContextForUpdate.setValueEnablingPrivilegeForNode(Constants.RecordOperations.CREATE, ContractPaths._Exposure._Control_LastSyncAction);
		valueContextForUpdate.setValueEnablingPrivilegeForNode(new Date(), ContractPaths._Exposure._Control_LastSyncTimestamp);
	}
	

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context) throws OperationException {
		super.handleBeforeModify(context);
		
		ValueContextForUpdate valueContextForUpdate = context.getOccurrenceContextForUpdate();
		String lastSyncAction = (String) context.getOccurrenceContext().getValue(ContractPaths._Exposure._Control_LastSyncAction);
		if(lastSyncAction != null && lastSyncAction.equals(Constants.RecordOperations.DELETE)) {
			valueContextForUpdate.setValueEnablingPrivilegeForNode(Constants.RecordOperations.DELETE, ContractPaths._Exposure._Control_LastSyncAction);
		} else {
			valueContextForUpdate.setValueEnablingPrivilegeForNode(Constants.RecordOperations.UPDATE, ContractPaths._Exposure._Control_LastSyncAction);
		}
		valueContextForUpdate.setValueEnablingPrivilegeForNode(new Date(), ContractPaths._Exposure._Control_LastSyncTimestamp);
	}
}