package com.elipslife.ebx.api.rest.models;

import javax.json.bind.annotation.JsonbProperty;

public class SynchroniseUWApplication {
	
	@JsonbProperty("Context")
	public BusinessContext context;
	
	@JsonbProperty("Data")
	public UWApplication data;

}
