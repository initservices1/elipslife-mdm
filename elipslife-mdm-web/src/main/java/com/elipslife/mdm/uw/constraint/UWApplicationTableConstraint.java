package com.elipslife.mdm.uw.constraint;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.elipslife.mdm.path.UWPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

import ch.butos.ebx.lib.validation.SmartRecordAndTableLevelValidationCheck;

public class UWApplicationTableConstraint extends SmartRecordAndTableLevelValidationCheck {

	@Override
	protected void executeBusinessValidations(ValueContext recordContext,
			Map<SchemaNode, Set<UserMessage>> validationMessages, boolean firstRecord) {
	
		String policyholderId = (String)recordContext.getValue(UWPaths._UWApplication._Policyholder_PolicyholderId);
		String policyholderBusId = (String)recordContext.getValue(UWPaths._UWApplication._Policyholder_PolicyholderBusId);
		String mainAfiliateId = (String)recordContext.getValue(UWPaths._UWApplication._MainAffiliate_MainAffiliateId);
		String mainAfiliateBusId = (String)recordContext.getValue(UWPaths._UWApplication._MainAffiliate_MainAffiliateBusId);
		
		UserMessage userMsg = null;
		
		if(policyholderBusId != null && policyholderId == null) {
			if(!policyholderBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Policyholder Bus Id '" + policyholderBusId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(UWPaths._UWApplication._Policyholder_PolicyholderId), userMsg);
			}
		}
		
		if(mainAfiliateBusId != null && mainAfiliateId == null) {
			if(!mainAfiliateBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("MainAfiliate Bus Id '" + mainAfiliateBusId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(UWPaths._UWApplication._MainAffiliate_MainAffiliateId), userMsg);
			}
		}
		
	}

	@Override
	public void setup(ConstraintContextOnTable aContext) {
		
	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		return null;
	}

}
