package com.elipslife.mdm.hr.core;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class BuildingWriterProcedureContext extends BuildingWriter{
	
	private final ProcedureContext procedureContext;

	public BuildingWriterProcedureContext(ProcedureContext procedureContext) {
		super();
		this.procedureContext = procedureContext;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
