package com.elipslife.mdm.common.trigger;

import com.elipslife.mdm.common.path.CommonReferencePaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessageLocalized;
import com.onwbp.boot.VM;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public class AccountHierarchyNodeTrigger extends TableTrigger {
	
	@Override
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext aContext) throws OperationException {
		
		VM.log.kernelDebug("[AccountHierarchyNodeTrigger][handleBeforeDelete] -- entry point");
		
		// dataset name
		Adaptation dataset = aContext.getAdaptationOccurrence().getContainer();
		boolean isFound = false;
		
		// account hierarchy node id 
		Integer accountHierarchyNodeId = (Integer) aContext.getOccurrenceContext().getValue(CommonReferencePaths._NonGeneric_AccountHierarchyNode._AccountHierarchyNodeId);
		
		// Account Hierarchy Link table
		AdaptationTable accountHierarchyLinkTable = dataset.getTable(CommonReferencePaths._NonGeneric_AccountHierarchyLink.getPathInSchema());
		
		// get all account hierarchy groups by account hierarchy group id
		RequestResult accountHierarchyParent = accountHierarchyLinkTable.createRequestResult("./Parent='"+ String.valueOf(accountHierarchyNodeId) +"'");
				
		// if reference hierarchy groups are found, show the error messages
		if(!accountHierarchyParent.isEmpty()) {
			isFound = true;
		} else {
			// get all account hierarchy groups by account hierarchy group id
			RequestResult accountHierarchyChild = accountHierarchyLinkTable.createRequestResult("./Child='"+ String.valueOf(accountHierarchyNodeId) +"'");
			if(!accountHierarchyChild.isEmpty()) {
				isFound = true;
			}
		}
		
		if(isFound) {
			// error messages
			UserMessageLocalized userMessage = new UserMessageLocalized();
			userMessage.setMessage("Please remove the referencing AccountHierarchyLinks, and try again!");
			throw OperationException.createError(userMessage);
		}
	}
	

	@Override
	public void setup(TriggerSetupContext arg0) { }


}
