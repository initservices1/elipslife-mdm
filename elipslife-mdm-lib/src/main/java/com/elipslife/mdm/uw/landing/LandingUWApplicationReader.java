package com.elipslife.mdm.uw.landing;

import java.util.List;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.LandingUWPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class LandingUWApplicationReader extends AbstractTableReader<LandingUWApplicationBean> {

    public LandingUWApplicationReader() {
        this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.LANDING_UW_REFERENCE,
            Constants.DataSet.LANDING_UW_MASTER), LandingUWPaths._UWApplication.getPathInSchema()));
    }

    public LandingUWApplicationReader(String dataspace) {
        this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.LANDING_UW_MASTER),
            LandingUWPaths._UWApplication.getPathInSchema()));
    }

    public LandingUWApplicationReader(AdaptationTable table) {
        super(table);
    }

    @Override
    public LandingUWApplicationBean createBeanFromRecord(Adaptation record) {

        LandingUWApplicationBean bean = new LandingUWApplicationBean();
        bean.setUWApplicationId(record.get_int(LandingUWPaths._UWApplication._UWApplicationId));
        bean.setUWApplicationBusId(record.getString(LandingUWPaths._UWApplication._UWApplicationBusId));
        bean.setSourceSystem(record.getString(LandingUWPaths._UWApplication._Context_SourceSystem));
        bean.setDataOwner(record.getString(LandingUWPaths._UWApplication._Context_DataOwner));
        bean.setOperatingCountry(record.getString(LandingUWPaths._UWApplication._Context_OperatingCountry));
        bean.setPolicyholderBusId(record.getString(LandingUWPaths._UWApplication._Policyholder_PolicyholderBusId));
        bean.setPolicyholderName(record.getString(LandingUWPaths._UWApplication._Policyholder_PolicyholderName));
        bean.setMainAffiliateBusId(record.getString(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateBusId));
        bean.setMainAffiliateFirstName(record.getString(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateFirstName));
        bean.setMainAffiliateLastName(record.getString(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateLastName));
        bean.setMainAffiliateDateOfBirth(record.getDate(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateDateOfBirth));
        bean.setMainAffiliateSSN(record.getString(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateSSN));
        bean.setMainAffiliateTown(record.getString(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateTown));
        bean.setMainAffiliatePostCode(record.getString(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliatePostCode));
        bean.setMainAffiliateCountry(record.getString(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateCountry));
        bean.setSubmissionDate(record.getDate(LandingUWPaths._UWApplication._Details_SubmissionDate));
        bean.setIsActive((Boolean) record.get(LandingUWPaths._UWApplication._Details_IsActive));
        bean.setDocSignId(record.getString(LandingUWPaths._UWApplication._DocumentSigning_DocSignId));
        bean.setSFSignedDate(record.getDate(LandingUWPaths._UWApplication._DocumentSigning_SFSignedDate));
        bean.setCreatedBy(record.getString(LandingUWPaths._UWApplication._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(LandingUWPaths._UWApplication._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(LandingUWPaths._UWApplication._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(LandingUWPaths._UWApplication._Control_LastUpdateTimestamp));
        bean.setDeletionTimeStamp(record.getDate(LandingUWPaths._UWApplication._Control_DeletionTimestamp));
        bean.setLastSyncTimeStamp(record.getDate(LandingUWPaths._UWApplication._Control_LastSyncTimestamp));
        bean.setAction(record.getString(LandingUWPaths._UWApplication._Control_Action));
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[] {LandingUWPaths._UWApplication._UWApplicationId};
    }

    @Override
    public Object[] retrievePks(LandingUWApplicationBean bean) {
        return new Object[] {bean.getUWApplicationId()};
    }

    /**
     * Retrieve LandingUWApplicationBean by primaryKey
     * 
     * @param uwApplicationId
     * @return
     */
    public LandingUWApplicationBean findBeanByPks(int uwApplicationId) {
        return super.findBeanByPks(new Object[] {uwApplicationId});
    }

    /**
     * Checks for validation errors with severity error or fatal
     * 
     * @param primaryKeys
     * @return
     */
    public boolean hasValidationErrors(Object[] primaryKeys) {

        Adaptation adaptation = super.findRecordByPks(primaryKeys);

        ValidationReport validationReport = adaptation.getValidationReport();

        boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);

        return hasErrors;
    }

    public List<LandingUWApplicationBean> findBeanByUWApplicationId(int uwApplicationId) {

        XPathPredicate predicate1 =
            new XPathPredicate(Operation.Equals, LandingUWPaths._UWApplication._UWApplicationId, String.valueOf(uwApplicationId));
        XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
        group.addPredicate(predicate1);

        return super.readBeansFor(group.toString());
    }
}
