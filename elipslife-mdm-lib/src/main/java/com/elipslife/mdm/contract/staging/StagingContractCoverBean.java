package com.elipslife.mdm.contract.staging;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class StagingContractCoverBean extends Entity<Integer> {
	
	private String exposureBusId;
	private String coverBusId;
	private String coverGroupType;
	private String coverGroupTypeName;
	private String coverName;
	private String coverTemplateBusId;
	private String beneficiaryType;
	private String contractCoverMaster;
	
	private String sourceSystem;
	private Date creationTimestamp;
	private Date lastUpdateTimestamp;
	private Date lastSyncTimeStamp;
	private String action;
	private String actionBy;
	
	public String getExposureBusId() {
		return exposureBusId;
	}
	public void setExposureBusId(String exposureBusId) {
		this.exposureBusId = exposureBusId;
	}
	public String getCoverBusId() {
		return coverBusId;
	}
	public void setCoverBusId(String coverBusId) {
		this.coverBusId = coverBusId;
	}
	public String getCoverGroupType() {
		return coverGroupType;
	}
	public void setCoverGroupType(String coverGroupType) {
		this.coverGroupType = coverGroupType;
	}
	public String getCoverGroupTypeName() {
		return coverGroupTypeName;
	}
	public void setCoverGroupTypeName(String coverGroupTypeName) {
		this.coverGroupTypeName = coverGroupTypeName;
	}
	public String getCoverName() {
		return coverName;
	}
	public void setCoverName(String coverName) {
		this.coverName = coverName;
	}
	public String getCoverTemplateBusId() {
		return coverTemplateBusId;
	}
	public void setCoverTemplateBusId(String coverTemplateBusId) {
		this.coverTemplateBusId = coverTemplateBusId;
	}
	public String getBeneficiaryType() {
		return beneficiaryType;
	}
	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActionBy() {
		return actionBy;
	}
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	public Date getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}
	public void setLastSyncTimeStamp(Date lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}
	public String getContractCoverMaster() {
		return contractCoverMaster;
	}
	public void setContractCoverMaster(String contractCoverMaster) {
		this.contractCoverMaster = contractCoverMaster;
	}
	
	
	
}