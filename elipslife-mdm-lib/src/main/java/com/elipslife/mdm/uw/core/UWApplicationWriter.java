package com.elipslife.mdm.uw.core;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.UWPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class UWApplicationWriter extends AbstractTableWriter<UWApplicationBean> {
    private UWApplicationReader reader;

    public UWApplicationWriter() {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._UWApplication.getPathInSchema()));
    }

    public UWApplicationWriter(String dataspace) {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._UWApplication.getPathInSchema()));
    }

    public UWApplicationWriter(AdaptationTable table) {
        super(table);
        reader = new UWApplicationReader(table);
    }

    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, UWApplicationBean bean) {
        values.put(UWPaths._UWApplication._UWApplicationId, bean.getUWApplicationId());
        values.put(UWPaths._UWApplication._UWApplicationBusId, bean.getUWApplicationBusId());
        values.put(UWPaths._UWApplication._Context_SourceSystem, bean.getSourceSystem());
        values.put(UWPaths._UWApplication._Context_DataOwner, bean.getDataOwner());
        values.put(UWPaths._UWApplication._Context_OperatingCountry, bean.getOperatingCountry());
        values.put(UWPaths._UWApplication._Policyholder_PolicyholderId, bean.getPolicyholderId());
        values.put(UWPaths._UWApplication._Policyholder_PolicyholderBusId, bean.getPolicyholderBusId());
        values.put(UWPaths._UWApplication._Policyholder_PolicyholderName, bean.getPolicyholderName());
        values.put(UWPaths._UWApplication._MainAffiliate_MainAffiliateId, bean.getMainAffiliateId());
        values.put(UWPaths._UWApplication._MainAffiliate_MainAffiliateBusId, bean.getMainAffiliateBusId());
        values.put(UWPaths._UWApplication._MainAffiliate_MainAffiliateFirstName, bean.getMainAffiliateFirstName());
        values.put(UWPaths._UWApplication._MainAffiliate_MainAffiliateLastName, bean.getMainAffiliateLastName());
        values.put(UWPaths._UWApplication._MainAffiliate_MainAffiliateDateOfBirth, bean.getMainAffiliateDateOfBirth());
        values.put(UWPaths._UWApplication._MainAffiliate_MainAffiliateSSN, bean.getMainAffiliateSSN());
        values.put(UWPaths._UWApplication._MainAffiliate_MainAffiliateTown, bean.getMainAffiliateTown());
        values.put(UWPaths._UWApplication._MainAffiliate_MainAffiliatePostCode, bean.getMainAffiliatePostCode());
        values.put(UWPaths._UWApplication._MainAffiliate_MainAffiliateCountry, bean.getMainAffiliateCountry());
        values.put(UWPaths._UWApplication._Details_SubmissionDate, bean.getSubmissionDate());
        values.put(UWPaths._UWApplication._Details_IsActive, bean.getIsActive());
        values.put(UWPaths._UWApplication._DocumentSigning_DocSignId, bean.getDocSignId());
        values.put(UWPaths._UWApplication._DocumentSigning_SFSignedDate, bean.getSFSignedDate());
        values.put(UWPaths._UWApplication._Control_CreatedBy, bean.getCreatedBy());
        values.put(UWPaths._UWApplication._Control_CreationTimestamp, bean.getCreationTimeStamp());
        values.put(UWPaths._UWApplication._Control_LastUpdatedBy, bean.getLastUpdatedBy());
        values.put(UWPaths._UWApplication._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
        values.put(UWPaths._UWApplication._Control_DeletionTimestamp, bean.getDeletionTimeStamp());
        values.put(UWPaths._UWApplication._Control_LastSyncAction, bean.getLastSyncAction());
    }

    @Override
    public AbstractTableReader<UWApplicationBean> getTableReader() {
        return reader;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub
		
	}
}
