package com.elipslife.mdm.claimcase.constraint;

import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import com.butos.ebx.smart.validation.SmartRecordAndTableLevelValidationCheck;
import com.elipslife.mdm.claimcase.constants.ClaimCasePaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class ClaimCaseConstraint extends SmartRecordAndTableLevelValidationCheck {

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void executeBusinessValidations(ValueContext recordContext, Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {

		String policyHolderBusId = (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_PolicyholderBusId);
		String policyHolderId = (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_PolicyholderId);

		String mainAffiliateBusId = (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_MainAffiliateBusId);
		String mainAffiliateId = (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_MainAffiliateId);

		String insuredPersonBusId = (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_InsuredPersonBusId);
		String insuredPersonId = (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_InsuredPersonId);

//		String claimMgtmTeamBusId = (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_ClaimMgtmTeamBusId);
//		String claimMgtmTeamId = (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_ClaimMgtmTeamId);
		
		String insuranceContractId =  (String) recordContext.getValue(ClaimCasePaths._ClaimCase._ClaimCase_General_InsuranceContractId);
		String contractNumber =  (String) recordContext.getValue(ClaimCasePaths._ClaimCase._ClaimCase_General_ContractNumber);
		
		String claimManagerUsername =  (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_ClaimManagerUserName);
		String claimManagerId =  (String) recordContext.getValue(ClaimCasePaths._ClaimCase._Party_ClaimManagerId);

		UserMessage userMsg = null;

		if (policyHolderBusId != null && policyHolderId == null) {
			if (!policyHolderBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Policy Holder " + policyHolderBusId + " is missing");
				this.insertUserMsg(validationInfoMap, recordContext.getNode(ClaimCasePaths._ClaimCase._Party_PolicyholderId), userMsg);
			}
		}

		if (mainAffiliateBusId != null && mainAffiliateId == null) {
			if (!mainAffiliateBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Main Affiliate " + mainAffiliateBusId + " is missing");
				this.insertUserMsg(validationInfoMap, recordContext.getNode(ClaimCasePaths._ClaimCase._Party_MainAffiliateId), userMsg);
			}
		}

		if (insuredPersonBusId != null && insuredPersonId == null) {
			if (!insuredPersonBusId.isEmpty()) {
			userMsg = UserMessage.createWarning("Insured Person " + insuredPersonBusId + " is missing");
			this.insertUserMsg(validationInfoMap, recordContext.getNode(ClaimCasePaths._ClaimCase._Party_InsuredPersonId), userMsg);
			}
		}
		
		/*
		if (claimMgtmTeamBusId != null && claimMgtmTeamId == null) {
			if (!claimMgtmTeamBusId.isEmpty()) {
			userMsg = UserMessage.createWarning("Claim Management Team (CRM-" + claimMgtmTeamBusId + ") is missing");
			this.insertUserMsg(validationInfoMap, recordContext.getNode(ClaimCasePaths._ClaimCase._Party_ClaimMgtmTeamId), userMsg);
			}
		}
		*/
		
		if(contractNumber != null && insuranceContractId == null) {
			if(!contractNumber.isEmpty()) {
				userMsg = UserMessage.createWarning("Insurance Contract Id ' " + contractNumber + " ' is missing");
				this.insertUserMsg(validationInfoMap, recordContext.getNode(ClaimCasePaths._ClaimCase._ClaimCase_General_InsuranceContractId), userMsg);
			}
		}
		
		if(claimManagerUsername != null && claimManagerId == null) {
			if(!claimManagerUsername.isEmpty()) {
				userMsg = UserMessage.createWarning("Claim Manager Id ' " + claimManagerUsername + " ' is missing");
				this.insertUserMsg(validationInfoMap, recordContext.getNode(ClaimCasePaths._ClaimCase._Party_ClaimManagerId), userMsg);
			}
		}

	}

}
