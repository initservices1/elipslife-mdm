package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.Address;
import com.elipslife.ebx.api.rest.models.BankAccount;
import com.elipslife.ebx.api.rest.models.Party;
import com.elipslife.ebx.api.rest.models.SynchroniseParty;
import com.elipslife.ebx.data.access.LandingAddressWriterSession;
import com.elipslife.ebx.data.access.LandingBankAccountWriterSession;
import com.elipslife.ebx.data.access.LandingPartyWriterSession;
import com.elipslife.ebx.domain.bean.LandingAddressBean;
import com.elipslife.ebx.domain.bean.LandingBankAccountBean;
import com.elipslife.ebx.domain.bean.LandingPartyBean;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.boot.VM;
import com.orchestranetworks.service.Session;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class PartyConverter extends AbstractConverter<LandingPartyBean[], SynchroniseParty> {
	
	public Session session;

	@Override
	public LandingPartyBean[] convert(SynchroniseParty synchroniseParty) throws DatatypeConfigurationException {

		ArrayList<LandingPartyBean> landingPartyBeans = new ArrayList<>();
		
		for(Party party : synchroniseParty.data ) {
			
			try {
				LandingPartyBean landingPartyBean = convertLandingPartyRequestData(party, synchroniseParty.context.sourceSystemId, synchroniseParty.context.userId, 
						synchroniseParty.correlationId);
				
				// Create Landing Party
				LandingPartyWriterSession landingPartyWriterSession = new LandingPartyWriterSession(session, true);
				Optional<LandingPartyBean> landingPartyBeanOptional = landingPartyWriterSession.createBeanOrThrow(landingPartyBean);
				String landingPartyId = String.valueOf(landingPartyBeanOptional.get().getId());
				
				// Don't process future party address
				boolean isWithinRange = DateNullableComparator.dateWithinRange(landingPartyBean.getValidFrom(), landingPartyBean.getValidTo());
	            if (!isWithinRange) {
	            	VM.log.kernelInfo("Landing Party: "+ landingPartyId +" is with future date. so MDM can't process addresses and bank accounts of future part");
	                continue;
	            }
				
	            // Create landing party addresses
	            LandingAddressBean[] landingAddressBeans = convertLandingAddressRequestData(party, landingPartyId, synchroniseParty.context.sourceSystemId, 
	            		synchroniseParty.context.userId, synchroniseParty.correlationId);
	            LandingAddressWriterSession landingAddressWriterSession = new LandingAddressWriterSession(session, true);
	            for (int i = 0;i < landingAddressBeans.length;i++) {
	                Optional<LandingAddressBean> landingAddressBeanOptional = landingAddressWriterSession.createBeanOrThrow(landingAddressBeans[i]);
	                landingAddressBeans[i] = landingAddressBeanOptional.get();
	            }
	            
	            // Create landing party bank accounts
	            LandingBankAccountBean[] landingBankAccountBeans = convertLandingBankAccountRequestData(party, landingPartyId, synchroniseParty.context.sourceSystemId,
	            		synchroniseParty.context.userId, synchroniseParty.correlationId);
	            LandingBankAccountWriterSession landingBankAccountWriterSession =new LandingBankAccountWriterSession(session, true);
	            for (int i = 0;i < landingBankAccountBeans.length;i++) {
	                Optional<LandingBankAccountBean> landingBankAccountBeanOptional =landingBankAccountWriterSession.createBeanOrThrow(landingBankAccountBeans[i]);
	                landingBankAccountBeans[i] = landingBankAccountBeanOptional.get();
	            }
				
			} catch (Exception e) {
				VM.log.kernelError("Exception occurred while processing REST party request to landing : "+e.getMessage());
			}
		}
		
		return landingPartyBeans.toArray(new LandingPartyBean[0]);
	}
	
	public void convert(Session session, SynchroniseParty synchroniseParty) throws DatatypeConfigurationException {
		
		this.session = session;
		convert(synchroniseParty); // Process party, address and bank account records in to landing
	}
	
	/**
	 * Convert and Process Landing Party REST data
	 * 
	 * @param party
	 * @param sourceSystem
	 * @param userId
	 * @param correlationId
	 * @return
	 */
	private LandingPartyBean convertLandingPartyRequestData(Party party, String sourceSystem, String userId, String correlationId) {
		
		LandingPartyBean landingPartyBean = new LandingPartyBean();
		
		Date dateOfBirth = party.dateOfBirth == null ? null :  DateHelper.convertToDate(party.dateOfBirth);
		Date creationTimeStamp = party.creationTimestamp == null ? null : Date.from(party.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
		Date lastUpdateTimestamp = party.lastUpdateTimestamp == null ? null : Date.from(party.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
		Date validFrom = party.validFrom == null ? null :DateHelper.convertToDate(party.validFrom);
		Date validTo = party.validTo == null ? null : DateHelper.convertToDate(party.validTo);
		Date idIssueDate = (party.identityDocument == null || party.identityDocument.issueDate == null) ? null : DateHelper.convertToDate(party.identityDocument.issueDate);
		Date idExpiryDate = (party.identityDocument == null || party.identityDocument.expiryDate == null) ? null : DateHelper.convertToDate(party.identityDocument.expiryDate);
		
		landingPartyBean.setDataOwner(party.dataOwner);
		landingPartyBean.setOperatingCountry(party.operatingCountry);
		landingPartyBean.setPartyType(party.partyType);
		landingPartyBean.setCrmId(party.crmId);
		landingPartyBean.setRelationshipOwner(party.relationshipOwner);
		landingPartyBean.setDataResponsible(party.dataResponsible);
		landingPartyBean.setLanguage(party.language);
		landingPartyBean.setIsActive(party.isActive);
		landingPartyBean.setFirstName(party.firstName);
		landingPartyBean.setMiddleName(party.middleName);
		landingPartyBean.setLastName(party.lastName);
		landingPartyBean.setDateOfBirth(dateOfBirth);
		landingPartyBean.setGender(party.gender);
		landingPartyBean.setTitle(party.title);
		landingPartyBean.setSalutation(party.salutation);
		landingPartyBean.setLetterSalutation(party.letterSalutation);
		landingPartyBean.setCivilStatus(party.civilStatus);
		landingPartyBean.setBirthCity(party.birthCity);
		landingPartyBean.setBirthCountry(party.birthCountry);
		landingPartyBean.setIdDocumentNumber(party.identityDocument == null ? null : party.identityDocument.documentNumber);
		landingPartyBean.setIdDocumentType(party.identityDocument == null ? null : party.identityDocument.documentType);
		landingPartyBean.setIdIssueDate(idIssueDate);
		landingPartyBean.setIdExpiryDate(idExpiryDate);
		landingPartyBean.setIdIssuingAuthority(party.identityDocument == null ? null : party.identityDocument.issuingAuthority);
		landingPartyBean.setSourceParentBusId(party.parentBusId);
		landingPartyBean.setOrgName(party.orgName);
		landingPartyBean.setOrgName2(party.orgName2);
		landingPartyBean.setCompanyType(party.companyType);
		landingPartyBean.setCommercialRegNo(party.commercialRegNo);
		landingPartyBean.setUidNumber(party.uidNumber);
		landingPartyBean.setVatNumber(party.vatNumber);
		landingPartyBean.setIndustry(party.industry);
		landingPartyBean.setSubIndustry(party.subIndustry);
		landingPartyBean.setPreferredEmail(party.preferredEmail);
		landingPartyBean.setPreferredCommChannel(party.preferredCommChannel);
		landingPartyBean.setSourceOrganisationBusId(party.organisationBusId);
		landingPartyBean.setSourceLineManagerBusId(party.lineManagerBusId);
		landingPartyBean.setSourcePartnerBusId(party.partnerBusId);
		landingPartyBean.setDepartment(party.department);
		landingPartyBean.setFunction(party.function);
		landingPartyBean.setIsExecutor(party.isExecutor);
		landingPartyBean.setIsLegalRepresentative(party.isLegalRepresentative);
		landingPartyBean.setIsSeniorManager(party.isSeniorManager);
		landingPartyBean.setIsUltimateBeneficialOwner(party.isUltimateBeneficialOwner);
		landingPartyBean.setBoRelationship(party.boRelationship);
		landingPartyBean.setContactPersonState(party.contactPersonState);
		landingPartyBean.setRoles(party.roles);
		landingPartyBean.setIsVipClient(party.isVipClient);
		landingPartyBean.setBlacklist(party.blacklist);
		landingPartyBean.setPartnerState(party.partnerState);
		landingPartyBean.setCompanyPersonalNr(party.companyPersonalNr);
		landingPartyBean.setSocialSecurityNr(party.socialSecurityNr);
		landingPartyBean.setSocialSecurityNrType(party.socialSecurityNrType);
		landingPartyBean.setBrokerRegNo(party.brokerRegNo);
		landingPartyBean.setBrokerTiering(party.brokerTiering);
		landingPartyBean.setLineOfBusiness(party.lineOfBusiness);
		landingPartyBean.setFinancePartyBusId(party.financePartyBusId);
		landingPartyBean.setPaymentTerms(party.paymentTerms);
		landingPartyBean.setFinanceClients(party.financeClients);
		landingPartyBean.setCreditorClients(party.creditorClients);
		landingPartyBean.setSourceSystem(sourceSystem);
		landingPartyBean.setSourcePartyBusId(party.partyBusId);
		landingPartyBean.setValidFrom(validFrom);
		landingPartyBean.setValidTo(validTo);
		landingPartyBean.setCreationTimestamp(creationTimeStamp);
		landingPartyBean.setLastUpdateTimestamp(lastUpdateTimestamp);
		landingPartyBean.setAction(party.action);
		landingPartyBean.setActionBy(userId);
		landingPartyBean.setCorrelationId(correlationId);  // correlation id
		
		return landingPartyBean;
	}
	
	/** 
	 * 
	 * Convert and Process Landing Party Address REST data
	 * 
	 * @param party
	 * @param landingPartyId
	 * @param sourceSystem
	 * @param userId
	 * @param correlationId
	 * @return
	 */
	private LandingAddressBean[] convertLandingAddressRequestData(Party party, String landingPartyId, String sourceSystem, String userId, String correlationId) {
		
		ArrayList<LandingAddressBean> landingAddressBeans = new ArrayList<>();		
		for(Address address : (party.addresses == null ? new ArrayList<Address>() : party.addresses)) {
			
			LandingAddressBean landingAddressBean = new LandingAddressBean();
			
			Date creationTimeStampAddress = address.creationTimestamp == null ? null : Date.from(address.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
			Date lastUpdateTimestampAddress = address.lastUpdateTimestamp == null ? null : Date.from(address.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
			Date validFromAddress = address.validFrom == null ? null : DateHelper.convertToDate(address.validFrom);
			Date validToAddress = address.validTo == null ? null : DateHelper.convertToDate(address.validTo);
			
			landingAddressBean.setAddressType(address.addressType);
			landingAddressBean.setAddressUsage(address.addressUse);
			landingAddressBean.setSourceAddressBusId(address.sourceAddressBusId);
			landingAddressBean.setStreet(address.street);
			landingAddressBean.setStreet2(address.street2);
			landingAddressBean.setPostCode(address.postCode);
			landingAddressBean.setPoBox(address.poBox);
			landingAddressBean.setPoBoxPostCode(address.poBoxPostCode);
			landingAddressBean.setPoBoxTown(address.poBoxTown);
			landingAddressBean.setDistrict(address.district);
			landingAddressBean.setTown(address.town);
			landingAddressBean.setStateProvince(address.stateProvince);
			landingAddressBean.setCountry(address.country);
			landingAddressBean.setCorrelationId(correlationId); // Correlation Id
			landingAddressBean.setLandingPartyId(landingPartyId); // Landing Party Id
			landingAddressBean.setElectronicAddressType(address.electronicAddressType);
			landingAddressBean.setAddress(address.address);
			landingAddressBean.setSourceSystem(sourceSystem);
			landingAddressBean.setSourcePartyBusId(party.partyBusId);
			landingAddressBean.setValidFrom(validFromAddress);
			landingAddressBean.setValidTo(validToAddress);
			landingAddressBean.setCreationTimestamp(creationTimeStampAddress);
			landingAddressBean.setLastUpdateTimestamp(lastUpdateTimestampAddress);
			landingAddressBean.setAction(address.action);
			landingAddressBean.setActionBy(userId);
			landingAddressBeans.add(landingAddressBean);
		}
		
		return landingAddressBeans.toArray(new LandingAddressBean[0]);
	}
	
	/**
	 * 
	 * Convert and Process Landing Party BankAccounts REST data
	 * 
	 * @param party
	 * @param landingPartyId
	 * @param sourceSystem
	 * @param userId
	 * @param correlationId
	 * @return
	 */
	private LandingBankAccountBean[] convertLandingBankAccountRequestData(Party party, String landingPartyId, String sourceSystem, String userId, String correlationId) {
		
		ArrayList<LandingBankAccountBean> landingBankAccountBeans = new ArrayList<>();
		for(BankAccount bankAccount : (party.bankAccounts == null ? new ArrayList<BankAccount>() : party.bankAccounts)) {
			
			LandingBankAccountBean landingBankAccountBean = new LandingBankAccountBean();
			landingBankAccountBeans.add(landingBankAccountBean);
			
			Date creationTimeStampBankAccount = bankAccount.creationTimestamp == null ? null : Date.from(bankAccount.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
			Date lastUpdateTimestampBankAccount = bankAccount.lastUpdateTimestamp == null ? null : Date.from(bankAccount.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
			Date validFromBankAccount = bankAccount.validFrom == null ? null : DateHelper.convertToDate(bankAccount.validFrom);
			Date validToBankAccount = bankAccount.validTo == null ? null : DateHelper.convertToDate(bankAccount.validTo);
			
			landingBankAccountBean.setAccountType(bankAccount.accountType);
			landingBankAccountBean.setAccountNumber(bankAccount.accountNumber);
			landingBankAccountBean.setIban(bankAccount.iban);
			landingBankAccountBean.setBic(bankAccount.bic);
			landingBankAccountBean.setClearingNumber(bankAccount.clearingNumber);
			landingBankAccountBean.setBankName(bankAccount.bankName);
			landingBankAccountBean.setCountry(bankAccount.country);
			landingBankAccountBean.setCurrency(bankAccount.currency);
			landingBankAccountBean.setIsMainAccount(bankAccount.isMainAccount);
			landingBankAccountBean.setCorrelationId(correlationId);
			landingBankAccountBean.setLandingPartyId(landingPartyId);
			landingBankAccountBean.setSourceBankAccountBusId(bankAccount.sourceBankAccountBusId);
			landingBankAccountBean.setSourceSystem(sourceSystem);
			landingBankAccountBean.setSourcePartyBusId(party.partyBusId);
			landingBankAccountBean.setValidFrom(validFromBankAccount);
			landingBankAccountBean.setValidTo(validToBankAccount);
			landingBankAccountBean.setCreationTimestamp(creationTimeStampBankAccount);
			landingBankAccountBean.setLastUpdateTimestamp(lastUpdateTimestampBankAccount);
			landingBankAccountBean.setAction(bankAccount.action);
			landingBankAccountBean.setActionBy(userId);
		}
		return landingBankAccountBeans.toArray(new LandingBankAccountBean[0]);
	}
}