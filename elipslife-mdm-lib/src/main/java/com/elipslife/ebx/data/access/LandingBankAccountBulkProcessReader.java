package com.elipslife.ebx.data.access;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.LandingBankAccountBulkProcessBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class LandingBankAccountBulkProcessReader extends BaseBankAccountReader<LandingBankAccountBulkProcessBean> {

    public LandingBankAccountBulkProcessReader() {
        this(PartyConstants.DataSpace.LANDING_REFERENCE);
    }

    public LandingBankAccountBulkProcessReader(String dataSpace) {

        super(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.LANDING_MASTER), 
        		LandingPaths._LDG_BankAccount_BulkProcess.getPathInSchema()));
    }


    public LandingBankAccountBulkProcessReader(AdaptationTable table) {
        super(table);
    }


    @Override
    public LandingBankAccountBulkProcessBean createBeanFromRecord(Adaptation record) {

        LandingBankAccountBulkProcessBean landingBankAccountBean = new LandingBankAccountBulkProcessBean();

        landingBankAccountBean.setId(record.get_int(LandingPaths._LDG_BankAccount_BulkProcess._Account_BankAccountId));

        landingBankAccountBean.setAccountType(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_AccountType));
        landingBankAccountBean.setAccountNumber(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_AccountNumber));
        landingBankAccountBean.setIban(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_IBAN));
        landingBankAccountBean.setBic(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_BIC));
        landingBankAccountBean.setClearingNumber(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_ClearingNumber));
        landingBankAccountBean.setBankName(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_BankName));
        landingBankAccountBean.setCountry(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_Country));
        landingBankAccountBean.setCurrency(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_Currency));
        landingBankAccountBean.setIsMainAccount((Boolean) record.get(LandingPaths._LDG_BankAccount_BulkProcess._Account_IsMainAccount));
        landingBankAccountBean.setSourceBankAccountBusId(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_SourceBankAccountBusId));
        landingBankAccountBean.setCorrelationId(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_CorrelationId)); // set correlation id
        landingBankAccountBean.setLandingPartyId(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_LandingPartyId)); // Landing Party Id
        landingBankAccountBean.setMdmStagingProcessResponse(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Account_MdmStagingProcessResponse)); // MDM Staging Process Response
        landingBankAccountBean.setSourceSystem(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Control_SourceSystem));
        landingBankAccountBean.setSourcePartyBusId(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Control_SourcePartyBusId));
        landingBankAccountBean.setValidFrom(record.getDate(LandingPaths._LDG_BankAccount_BulkProcess._Control_ValidFrom));
        landingBankAccountBean.setValidTo(record.getDate(LandingPaths._LDG_BankAccount_BulkProcess._Control_ValidTo));
        landingBankAccountBean.setCreationTimestamp(record.getDate(LandingPaths._LDG_BankAccount_BulkProcess._Control_CreationTimestamp));
        landingBankAccountBean.setLastUpdateTimestamp(record.getDate(LandingPaths._LDG_BankAccount_BulkProcess._Control_LastUpdateTimestamp));
        landingBankAccountBean.setAction(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Control_Action));
        landingBankAccountBean.setActionBy(record.getString(LandingPaths._LDG_BankAccount_BulkProcess._Control_ActionBy));

        return landingBankAccountBean;
    }
    
    /**
     * Get Landing Bank Accounts which are created after the mentioned lastSyncTimestamp
     * 
     * @param lastSyncTimestamp
     * @return
     */
    public List<LandingBankAccountBulkProcessBean> getLandingBankAccountsBulkProcessBeansByLastSyncTimeStamp(Date lastSyncTimestamp) {

        String dateNowString = DateHelper.convertToXpathFormat(lastSyncTimestamp);

        XPathPredicate predicate1 =
            new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, LandingPaths._LDG_BankAccount_BulkProcess._Control_LastSyncTimestamp, dateNowString);
        XPathPredicate predicate2 =
                new XPathPredicate(XPathPredicate.Operation.DateEqual, LandingPaths._LDG_BankAccount_BulkProcess._Control_LastSyncTimestamp, dateNowString);
        XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.Or);
        group.addPredicate(predicate1);
        group.addPredicate(predicate2);
        
        return readRecordsFor(group.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }


    @Override
    protected Path[] getPkPaths() {

        return new Path[] {LandingPaths._LDG_BankAccount_BulkProcess._Account_BankAccountId};
    }


    @Override
    protected Object[] retrievePks(LandingBankAccountBulkProcessBean bean) {

        return new Object[] {bean.getId()};
    }


    @Override
    protected Path getPathBankAccountId() {

        return LandingPaths._LDG_BankAccount_BulkProcess._Account_BankAccountId;
    }


    @Override
    protected Path getPathSourceBankAccountBusId() {

        return LandingPaths._LDG_BankAccount_BulkProcess._Account_SourceBankAccountBusId;
    }


    @Override
    protected Path getPathControlSourceSystem() {

        return LandingPaths._LDG_BankAccount_BulkProcess._Control_SourceSystem;
    }


    @Override
    protected Path getPathControlSourcePartyBusId() {

        return LandingPaths._LDG_BankAccount_BulkProcess._Control_SourcePartyBusId;
    }


    @Override
    protected Path getPathControlValidFrom() {

        return LandingPaths._LDG_BankAccount_BulkProcess._Control_ValidFrom;
    }


    @Override
    protected Path getPathControlValidTo() {

        return LandingPaths._LDG_BankAccount_BulkProcess._Control_ValidTo;
    }
}
