package com.elipslife.mdm.contract.staging;

import java.util.List;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;

public class StagingExposureReader extends AbstractTableReader<StagingExposureBean> {

	public StagingExposureReader() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public StagingExposureReader(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._STG_Exposure.getPathInSchema()));
	}
	
	public StagingExposureReader(AdaptationTable table) {
		super(table);
	}

	@Override
	protected StagingExposureBean createBeanFromRecord(Adaptation record) {
		
		StagingExposureBean bean = new StagingExposureBean();
		
		bean.setId(record.get_int(ContractPaths._STG_Exposure._Exposure_ExposureId));
		bean.setExposureBusId(record.getString(ContractPaths._STG_Exposure._Exposure_ExposureBusId));
		bean.setName(record.getString(ContractPaths._STG_Exposure._Exposure_Name));
		bean.setExposureMaster(record.getString(ContractPaths._STG_Exposure._Exposure_ExposureMaster));
		bean.setProductBusId(record.getString(ContractPaths._STG_Exposure._Exposure_ProductBusId));
		bean.setProductGroup(record.getString(ContractPaths._STG_Exposure._Exposure_ProductGroup));
		bean.setProductName(record.getString(ContractPaths._STG_Exposure._Exposure_ProductName));
		bean.setSourceProductBusId(record.getString(ContractPaths._STG_Exposure._Exposure_SourceProductBusId));
		
		bean.setSourceSystem(record.getString(ContractPaths._STG_Exposure._Control_SourceSystem));
		bean.setSourceContractBusId(record.getString(ContractPaths._STG_Exposure._Control_SourceContractBusId));
		bean.setCreationTimestamp(record.getDate(ContractPaths._STG_Exposure._Control_CreationTimestamp));
		bean.setLastUpdateTimestamp(record.getDate(ContractPaths._STG_Exposure._Control_LastUpdateTimestamp));
		bean.setLastSyncTimeStamp(record.getDate(ContractPaths._STG_Exposure._Control_LastSyncTimestamp));
		bean.setAction(record.getString(ContractPaths._STG_Exposure._Control_Action));
		bean.setActionBy(record.getString(ContractPaths._STG_Exposure._Control_ActionBy));
		
		return bean;
	}

	@Override
	protected Path[] getPkPaths() {
		return new Path[] { ContractPaths._STG_Exposure._Exposure_ExposureId };
	}

	@Override
	protected Object[] retrievePks(StagingExposureBean bean) {
		return new Object[] { bean.getId() };
	}
	
	
	/**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}
	
	/**
	 * Get all staging records with Exposure Master is null
	 * 
	 * @return
	 */
	public List<StagingExposureBean> getAllBeansWithExposureMasterIsEmpty() {
		
		XPathPredicate predicate1 = new XPathPredicate(Operation.IsNull, ContractPaths._STG_Exposure._Exposure_ExposureMaster, null);
		
		return super.readBeansFor(predicate1.toString());
	}
}