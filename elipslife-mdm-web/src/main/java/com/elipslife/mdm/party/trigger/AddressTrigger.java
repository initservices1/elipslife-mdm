package com.elipslife.mdm.party.trigger;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.codec.binary.StringUtils;

import com.elipslife.ebx.data.access.CoreAddressReader;
import com.elipslife.ebx.data.access.CorePartyReader;
import com.elipslife.ebx.domain.bean.CoreAddressBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.party.constants.PartyConstants.RecordOperations;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.schema.trigger.ValueChange;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class AddressTrigger extends TableTrigger{

	@Override
	public void setup(TriggerSetupContext arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		
		ValueContextForUpdate vcf = aContext.getOccurrenceContextForUpdate();
		this.updateValue(vcf);
	}
	
	public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {
		super.handleAfterCreate(aContext);
		
		ValueContext contextForUpdate = aContext.getOccurrenceContext();
		String addressType = (String) contextForUpdate.getValue(PartyPaths._Address._AddressInfo_AddressType);
		String addressUse = (String) contextForUpdate.getValue(PartyPaths._Address._AddressInfo_AddressUse);
		
		// check if it is a legal address or not
		if(StringUtils.equals(addressUse, "1") && StringUtils.equals(addressType, "P")) {
			String partyId = (String) contextForUpdate.getValue(PartyPaths._Address._AddressInfo_PartyId);
			CorePartyReader corePartyReader = new CorePartyReader(aContext.getAdaptationHome().getKey().getName());
			Adaptation corePartyBean = corePartyReader.findRecordByPartyId(Integer.valueOf(partyId));
			String legalAddressId = (String) corePartyBean.getString(PartyPaths._Party._PartyInfo_LegalPostalAddress);
			if(legalAddressId == null) {  // if party legal postal address field is empty, update with this address
				int addressId = (Integer) contextForUpdate.getValue(PartyPaths._Address._AddressInfo_AddressId);
				updateRecord(aContext.getProcedureContext(), corePartyBean , PartyPaths._Party._PartyInfo_LegalPostalAddress, String.valueOf(addressId));
			}
		}
	}
	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
		
		ValueContextForUpdate vcf = aContext.getOccurrenceContextForUpdate();
		this.updateValue(vcf);
	}
	
	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext aContext) throws OperationException {
		super.handleAfterModify(aContext);
		
		String addressUseValue = (String) aContext.getOccurrenceContext().getValue(PartyPaths._Address._AddressInfo_AddressUse);
		String addressTypeValue = (String) aContext.getOccurrenceContext().getValue(PartyPaths._Address._AddressInfo_AddressType);
		ValueChange addressUseValueChange = aContext.getChanges().getChange(PartyPaths._Address._AddressInfo_AddressUse);
		ValueChange addressTypeValueChange = aContext.getChanges().getChange(PartyPaths._Address._AddressInfo_AddressType);
		if((addressTypeValueChange != null && addressTypeValueChange.getValueBefore().equals("P")) || (addressUseValueChange != null && addressUseValueChange.getValueBefore().equals("1"))) {
			String partyId = (String) aContext.getOccurrenceContext().getValue(PartyPaths._Address._AddressInfo_PartyId);
			CorePartyReader corePartyReader = new CorePartyReader(aContext.getAdaptationHome().getKey().getName());
			Adaptation corePartyBean = corePartyReader.findRecordByPartyId(Integer.valueOf(partyId));
			if(corePartyBean != null) {
				String legalAddressId = (String) corePartyBean.getString(PartyPaths._Party._PartyInfo_LegalPostalAddress);
				if(legalAddressId != null) {
					updateRecord(aContext.getProcedureContext(), corePartyBean , PartyPaths._Party._PartyInfo_LegalPostalAddress, null);
				}
			}
		} else if(addressTypeValueChange != null && addressTypeValueChange.getValueAfter().equals("P") && addressUseValue.equals("1")) {
			
			String partyId = (String) aContext.getOccurrenceContext().getValue(PartyPaths._Address._AddressInfo_PartyId);
			CorePartyReader corePartyReader = new CorePartyReader(aContext.getAdaptationHome().getKey().getName());
			Adaptation corePartyBean = corePartyReader.findRecordByPartyId(Integer.valueOf(partyId));
			if(corePartyBean != null) {
				int addressId = (Integer) aContext.getOccurrenceContext().getValue(PartyPaths._Address._AddressInfo_AddressId);
				String partyLegalAddressId = corePartyBean.getString(PartyPaths._Party._PartyInfo_LegalPostalAddress);
				// if party legal address id and current address id's are different, Update the party
				if(!StringUtils.equals(partyLegalAddressId, String.valueOf(addressId))) {
					updateRecord(aContext.getProcedureContext(), corePartyBean , PartyPaths._Party._PartyInfo_LegalPostalAddress, String.valueOf(addressId));
				}
			}
		} else if (addressUseValueChange != null && addressUseValueChange.getValueAfter().equals("1") && addressTypeValue.equals("P")) {
			String partyId = (String) aContext.getOccurrenceContext().getValue(PartyPaths._Address._AddressInfo_PartyId);
			CorePartyReader corePartyReader = new CorePartyReader(aContext.getAdaptationHome().getKey().getName());
			Adaptation corePartyBean = corePartyReader.findRecordByPartyId(Integer.valueOf(partyId));
			if(corePartyBean != null) {
				int addressId = (Integer) aContext.getOccurrenceContext().getValue(PartyPaths._Address._AddressInfo_AddressId);
				String partyLegalAddressId = corePartyBean.getString(PartyPaths._Party._PartyInfo_LegalPostalAddress);
				// if party legal address id and current address id's are different, Update the party
				if(!StringUtils.equals(partyLegalAddressId, String.valueOf(addressId))) {
					updateRecord(aContext.getProcedureContext(), corePartyBean , PartyPaths._Party._PartyInfo_LegalPostalAddress, String.valueOf(addressId));
				}
			}
		}
		
	}
	
	CoreAddressBean coreAddressBeanToDelete;
	
	@Override
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext aContext) throws OperationException {

		super.handleBeforeDelete(aContext);
		this.coreAddressBeanToDelete = new CoreAddressReader().createBeanFromRecord(aContext.getAdaptationOccurrence());
	}

	@Override
	public void handleAfterDelete(AfterDeleteOccurrenceContext aContext) throws OperationException {
		
		super.handleAfterDelete(aContext);
		
		boolean rootDataSpace = Objects.equals(PartyConstants.DataSpace.PARTY_REFERENCE, aContext.getAdaptationHome().getKey().getName());
		if (rootDataSpace) {  // if it is partyReference data space
			boolean isWithinRange = dateWithinRange(this.coreAddressBeanToDelete.getValidFrom(), this.coreAddressBeanToDelete.getValidTo());
			if (!isWithinRange) {
				return;
			}
			
			// If this is a postal address and if it is deleted,Update the core party legal address
			if(StringUtils.equals(coreAddressBeanToDelete.getAddressUsage(), "1") && StringUtils.equals(coreAddressBeanToDelete.getAddressType(), "P")) {
				String legalAddressRecordId = String.valueOf(coreAddressBeanToDelete.getId());
				CorePartyReader corePartyReader = new CorePartyReader(aContext.getAdaptationHome().getKey().getName());
				List<Adaptation> corePartyBeans = corePartyReader.findCorePartyByLegalAddress(legalAddressRecordId);
				for(Adaptation corePartyBean : corePartyBeans) {
					String legalAddressId = (String) corePartyBean.getString(PartyPaths._Party._PartyInfo_LegalPostalAddress);
					if(legalAddressId != null) {
						updateRecord(aContext.getProcedureContext(), corePartyBean , PartyPaths._Party._PartyInfo_LegalPostalAddress, null);
					}
				}
			}
		}
	}

	private void updateValue(final ValueContextForUpdate vcf) throws OperationException {

		vcf.setValueEnablingPrivilegeForNode(new Date(), PartyPaths._Address._Control_LastSyncTimestamp);
	}
	
	/**
	 * Update legalAddress field in Core Party
	 * 
	 * @param procedureContext
	 * @param record
	 * @param legalAddressIdPath
	 * @param value
	 * @throws OperationException
	 */
	private void updateRecord(ProcedureContext procedureContext, Adaptation record, Path legalAddressIdPath, String value) throws OperationException {
        ValueContextForUpdate contextForUpdate = procedureContext.getContext(record.getAdaptationName());
        contextForUpdate.setValue(value, legalAddressIdPath);
        contextForUpdate.setValue(RecordOperations.UPDATE, PartyPaths._Party._Control_LastSyncAction);
        procedureContext.doModifyContent(record, contextForUpdate);
    }
	
	/**
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @return
	 */
	private boolean dateWithinRange(Date dateFrom, Date dateTo) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		Date dateNow = calendar.getTime();

		Date dateMinimum = new Date(Long.MIN_VALUE);
		Date dateMaximum = new Date(Long.MAX_VALUE);

		return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0
				&& dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
	}
}