package com.elipslife.mdm.uw.core;

import java.math.BigDecimal;
import java.util.List;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.UWPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class UWCaseReader extends AbstractTableReader<UWCaseBean> {

    public UWCaseReader() {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._UWCase.getPathInSchema()));
    }

    public UWCaseReader(String dataspace) {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._UWCase.getPathInSchema()));
    }

    public UWCaseReader(AdaptationTable table) {
		super(table);
	}

	@Override
    public UWCaseBean createBeanFromRecord(Adaptation record) {
        UWCaseBean bean = new UWCaseBean();
        bean.setUWCaseId(record.get_int(UWPaths._UWCase._UWCaseId));
        bean.setUWCaseBusId(record.getString(UWPaths._UWCase._UWCaseBusId));
        bean.setSourceSystem(record.getString(UWPaths._UWCase._Context_SourceSystem));
        bean.setDataOwner(record.getString(UWPaths._UWCase._Context_DataOwner));
        bean.setUWApplicationId(record.getString(UWPaths._UWCase._Context_UWApplicationId));
        bean.setUWApplicationBusId(record.getString(UWPaths._UWCase._Context_UWApplicationBusId));
        bean.setInsuranceContractId(record.getString(UWPaths._UWCase._Context_InsuranceContractId));
        bean.setContractNumber(record.getString(UWPaths._UWCase._Context_ContractNumber));
        bean.setApplicantId(record.getString(UWPaths._UWCase._Applicant_ApplicantId));
        bean.setApplicantBusId(record.getString(UWPaths._UWCase._Applicant_ApplicantBusId));
        bean.setApplicantRelationship(record.getString(UWPaths._UWCase._Applicant_ApplicantRelationship));
        bean.setApplicantFirstName(record.getString(UWPaths._UWCase._Applicant_ApplicantFirstName));
        bean.setApplicantLastName(record.getString(UWPaths._UWCase._Applicant_ApplicantLastName));
        bean.setApplicantDateOfBirth(record.getDate(UWPaths._UWCase._Applicant_ApplicantDateOfBirth));
        bean.setApplicantSSN(record.getString(UWPaths._UWCase._Applicant_ApplicantSSN));
        bean.setApplicantTown(record.getString(UWPaths._UWCase._Applicant_ApplicantTown));
        bean.setApplicantPostCode(record.getString(UWPaths._UWCase._Applicant_ApplicantPostCode));
        bean.setApplicantCountry(record.getString(UWPaths._UWCase._Applicant_ApplicantCountry));
        bean.setApplicantClass(record.getString(UWPaths._UWCase._Details_ApplicantClass));
        bean.setProductType(record.getString(UWPaths._UWCase._Details_ProductType));
        bean.setEligiblityDate(record.getDate(UWPaths._UWCase._Details_EligiblityDate));
        bean.setApplicationReason(record.getString(UWPaths._UWCase._Details_ApplicationReason));
        bean.setApplicationReasonText(record.getString(UWPaths._UWCase._Details_ApplicationReasonText));
        bean.setInforceCoverage((BigDecimal)record.get(UWPaths._UWCase._Details_InforceCoverage));
        bean.setAdditionalCoverage((BigDecimal)record.get(UWPaths._UWCase._Details_AdditionalCoverage));
        bean.setIsActive(record.get_boolean(UWPaths._UWCase._State_IsActive));
        bean.setCaseState(record.getString(UWPaths._UWCase._State_CaseState));
        bean.setCreationTimeStamp(record.getDate(UWPaths._UWCase._Control_CreationTimestamp));
		bean.setLastUpdatedBy(record.getString(UWPaths._UWCase._Control_LastUpdatedBy));
		bean.setLastUpdateTimeStamp(record.getDate(UWPaths._UWCase._Control_LastUpdateTimestamp));
		bean.setDeletionTimeStamp(record.getDate(UWPaths._UWCase._Control_DeletionTimestamp));
		bean.setLastSyncAction(record.getString(UWPaths._UWCase._Control_LastSyncAction));
		
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{UWPaths._UWCase._UWCaseId};
    }

    @Override
    public Object[] retrievePks(UWCaseBean bean) {
        return new Object[]{bean.getUWCaseId()};
    }
    
    public List<UWCaseBean> findBeansByUWCaseBusId(String uwCaseBusId) {
    	XPathPredicate predicate = new XPathPredicate(Operation.Equals, UWPaths._UWCase._UWCaseBusId, uwCaseBusId);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate);
		
		return super.readBeansFor(group.toString());
    }
    
    public List<UWCaseBean> findBeansByInsuranceContractId(String insuranceContractId) {
    	XPathPredicate insuranceContractPredicate = new XPathPredicate(XPathPredicate.Operation.Equals, UWPaths._UWCase._Context_InsuranceContractId, insuranceContractId);
		return super.readBeansFor(insuranceContractPredicate.toString());
    }

    
}
