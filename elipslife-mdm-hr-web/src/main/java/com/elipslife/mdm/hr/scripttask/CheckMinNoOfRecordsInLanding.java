package com.elipslife.mdm.hr.scripttask;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.LandingHrModelPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class CheckMinNoOfRecordsInLanding extends ScriptTaskBean {
	
	private String minNoOfRecordsInBuilding;
	private String minNoOfRecordsInOrganisation;
	private String minNoOfRecordsInEmployee;
	private String hasMinNoOfRecordsInBuilding;
	private String hasMinNoOfRecordsInOrganisation;
	private String hasMinNoOfRecordsInEmployee;
	@Override
	public void executeScript(ScriptTaskBeanContext aContext) throws OperationException {
		
		AdaptationTable buildingTable = RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.LANDING_HR_REFERENCE, Constants.DataSet.LANDING_HR_MASTER),
				LandingHrModelPaths._Building.getPathInSchema());
		AdaptationTable organisationTable = RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.LANDING_HR_REFERENCE, Constants.DataSet.LANDING_HR_MASTER),
				LandingHrModelPaths._Organisation.getPathInSchema());
		AdaptationTable employeeTable = RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.LANDING_HR_REFERENCE, Constants.DataSet.LANDING_HR_MASTER),
				LandingHrModelPaths._Employee.getPathInSchema());
		try {
			// No of records in Building table
			Request buldingRequest = buildingTable.createRequest();
			RequestResult buldingRequestResult = buldingRequest.execute();
			if(buldingRequestResult.isSizeGreaterOrEqual(1) && 
					buldingRequestResult.isSizeGreaterOrEqual(Integer.valueOf(getMinNoOfRecordsInBuilding()).intValue())) {
				hasMinNoOfRecordsInBuilding = "1";
			}
			
			
			// No of records in Organisation table
			Request organisationRequest = organisationTable.createRequest();
			RequestResult organisationResult = organisationRequest.execute();
			if(organisationResult.isSizeGreaterOrEqual(1) && organisationResult.isSizeGreaterOrEqual(Integer.valueOf(getMinNoOfRecordsInOrganisation()).intValue())) {
				hasMinNoOfRecordsInOrganisation = "1";
			}
			
			// No of records in Employee table
			Request employeeRequest = employeeTable.createRequest();
			RequestResult employeeResult = employeeRequest.execute();
			if(employeeResult.isSizeGreaterOrEqual(1) && employeeResult.isSizeGreaterOrEqual(Integer.valueOf(getMinNoOfRecordsInEmployee()))) {
				hasMinNoOfRecordsInEmployee = "1";
			}
			
			// Closing request results
			buldingRequestResult.close();
			organisationResult.close();
			employeeResult.close();
			
		}catch (Exception e) {
			
		}
	}
	
	public String getMinNoOfRecordsInBuilding() {
		return minNoOfRecordsInBuilding;
	}
	public void setMinNoOfRecordsInBuilding(String minNoOfRecordsInBuilding) {
		this.minNoOfRecordsInBuilding = minNoOfRecordsInBuilding;
	}
	public String getMinNoOfRecordsInOrganisation() {
		return minNoOfRecordsInOrganisation;
	}
	public void setMinNoOfRecordsInOrganisation(String minNoOfRecordsInOrganisation) {
		this.minNoOfRecordsInOrganisation = minNoOfRecordsInOrganisation;
	}
	public String getMinNoOfRecordsInEmployee() {
		return minNoOfRecordsInEmployee;
	}
	public void setMinNoOfRecordsInEmployee(String minNoOfRecordsInEmployee) {
		this.minNoOfRecordsInEmployee = minNoOfRecordsInEmployee;
	}
	public String getHasMinNoOfRecordsInBuilding() {
		return hasMinNoOfRecordsInBuilding;
	}
	public void setHasMinNoOfRecordsInBuilding(String hasMinNoOfRecordsInBuilding) {
		this.hasMinNoOfRecordsInBuilding = hasMinNoOfRecordsInBuilding;
	}
	public String getHasMinNoOfRecordsInOrganisation() {
		return hasMinNoOfRecordsInOrganisation;
	}
	public void setHasMinNoOfRecordsInOrganisation(String hasMinNoOfRecordsInOrganisation) {
		this.hasMinNoOfRecordsInOrganisation = hasMinNoOfRecordsInOrganisation;
	}
	public String getHasMinNoOfRecordsInEmployee() {
		return hasMinNoOfRecordsInEmployee;
	}
	public void setHasMinNoOfRecordsInEmployee(String hasMinNoOfRecordsInEmployee) {
		this.hasMinNoOfRecordsInEmployee = hasMinNoOfRecordsInEmployee;
	}
}
