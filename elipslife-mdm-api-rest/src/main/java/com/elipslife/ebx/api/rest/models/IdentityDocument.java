package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;

import javax.json.bind.annotation.JsonbProperty;

public class IdentityDocument {
	
	@JsonbProperty("DocumentNumber")
	public String documentNumber;
	
	@JsonbProperty("DocumentType")
	public String documentType;
	
	@JsonbProperty("IssueDate")
	public LocalDate issueDate;
	
	@JsonbProperty("ExpiryDate")
	public LocalDate expiryDate;
	
	@JsonbProperty("IssuingAuthority")
	public String issuingAuthority;
}