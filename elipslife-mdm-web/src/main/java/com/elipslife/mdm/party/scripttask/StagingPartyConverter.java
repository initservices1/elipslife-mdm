package com.elipslife.mdm.party.scripttask;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.mdm.path.PartyPaths;
import com.orchestranetworks.schema.Path;

public class StagingPartyConverter {

    private static boolean useDefaultValues;

    public static HashMap<Path, Object> fillStagingPartyValuesFromLandingPartyBean(StagingPartyBean bean, boolean defaultValues) {

        HashMap<Path, Object> values = new HashMap<Path, Object>();
        useDefaultValues = defaultValues;

        values.put(PartyPaths._STG_Party._Party_PartyId, bean.getId());
        if (!useDefaultValues || bean.getDataOwner() != null) {
            values.put(PartyPaths._STG_Party._Party_DataOwner, bean.getDataOwner());
        }
        values.put(PartyPaths._STG_Party._Party_OperatingCountry, bean.getOperatingCountry());
        values.put(PartyPaths._STG_Party._Party_CRMId, bean.getCrmId());
        values.put(PartyPaths._STG_Party._Party_PartyType, bean.getPartyType());
        values.put(PartyPaths._STG_Party._Party_RelationshipOwner, bean.getRelationshipOwner());
        values.put(PartyPaths._STG_Party._Party_DataResponsible, bean.getDataResponsible());
        values.put(PartyPaths._STG_Party._Party_Language, bean.getLanguage());
        values.put(PartyPaths._STG_Party._Party_CorrelationId, bean.getCorrelationId()); // set correlation id
        
        if (!useDefaultValues || bean.getIsActive() != null) {
            values.put(PartyPaths._STG_Party._Party_IsActive, bean.getIsActive());
        }

        values.put(PartyPaths._STG_Party._Party_CorePartyId, bean.getCorePartyId());
        values.put(PartyPaths._STG_Party._Person_FirstName, bean.getFirstName());
        values.put(PartyPaths._STG_Party._Person_MiddleName, bean.getMiddleName());
        values.put(PartyPaths._STG_Party._Person_LastName, bean.getLastName());
        values.put(PartyPaths._STG_Party._Person_DateOfBirth, bean.getDateOfBirth());
        values.put(PartyPaths._STG_Party._Person_Gender, bean.getGender());
        values.put(PartyPaths._STG_Party._Person_Title, bean.getTitle());
        values.put(PartyPaths._STG_Party._Person_Salutation, bean.getSalutation());
        values.put(PartyPaths._STG_Party._Person_LetterSalutation, bean.getLetterSalutation());
        values.put(PartyPaths._STG_Party._Person_CivilStatus, bean.getCivilStatus());
        values.put(PartyPaths._STG_Party._Person_BirthCity, bean.getBirthCity());
        values.put(PartyPaths._STG_Party._Person_BirthCountry, bean.getBirthCountry());
        values.put(PartyPaths._STG_Party._IdDocument_ID_DocumentNumber, bean.getIdDocumentNumber());
        values.put(PartyPaths._STG_Party._IdDocument_ID_DocumentType, bean.getIdDocumentType());
        values.put(PartyPaths._STG_Party._IdDocument_ID_IssueDate, bean.getIdIssueDate());
        values.put(PartyPaths._STG_Party._IdDocument_ID_ExpiryDate, bean.getIdExpiryDate());
        values.put(PartyPaths._STG_Party._IdDocument_ID_IssuingAuthority, bean.getIdIssuingAuthority());
        values.put(PartyPaths._STG_Party._Organisation_SourceParentBusId, bean.getSourceParentBusId());
        values.put(PartyPaths._STG_Party._Organisation_OrgName, bean.getOrgName());
        values.put(PartyPaths._STG_Party._Organisation_OrgName2, bean.getOrgName2());
        values.put(PartyPaths._STG_Party._Organisation_CompanyType, bean.getCompanyType());
        values.put(PartyPaths._STG_Party._Organisation_CommercialRegNo, bean.getCommercialRegNo());
        values.put(PartyPaths._STG_Party._Organisation_UIDNumber, bean.getUidNumber());
        values.put(PartyPaths._STG_Party._Organisation_VATNumber, bean.getVatNumber());
        values.put(PartyPaths._STG_Party._Organisation_Industry, bean.getIndustry());
        values.put(PartyPaths._STG_Party._Organisation_SubIndustry, bean.getSubIndustry());
        values.put(PartyPaths._STG_Party._Organisation_PreferredEmail, bean.getPreferredEmail());
        values.put(PartyPaths._STG_Party._Organisation_PreferredCommChannel, bean.getPreferredCommChannel());
        values.put(PartyPaths._STG_Party._ContactPerson_SourceOrganisationBusId, bean.getSourceOrganisationBusId());
        values.put(PartyPaths._STG_Party._ContactPerson_SourceLineManagerBusId, bean.getSourceLineManagerBusId());
        values.put(PartyPaths._STG_Party._ContactPerson_SourcePartnerBusId, bean.getSourcePartnerBusId());
        values.put(PartyPaths._STG_Party._ContactPerson_Department, bean.getDepartment());
        values.put(PartyPaths._STG_Party._ContactPerson_Function, bean.getFunction());
        values.put(PartyPaths._STG_Party._ContactPerson_IsExecutor, bean.getIsExecutor());
        values.put(PartyPaths._STG_Party._ContactPerson_IsLegalRepresentative, bean.getIsLegalRepresentative());
        
        if (!useDefaultValues || bean.getIsSeniorManager() != null) {
            values.put(PartyPaths._STG_Party._ContactPerson_IsSeniorManager, bean.getIsSeniorManager());
        }

        if (!useDefaultValues || bean.getIsUltimateBeneficialOwner() != null) {
            values.put(PartyPaths._STG_Party._ContactPerson_IsUltimateBeneficialOwner, bean.getIsUltimateBeneficialOwner());
        }

        values.put(PartyPaths._STG_Party._ContactPerson_BORelationship, bean.getBoRelationship());
        values.put(PartyPaths._STG_Party._ContactPerson_ContactPersonState, bean.getContactPersonState());
        values.put(PartyPaths._STG_Party._Role_Roles, bean.getRoles());

        if (!useDefaultValues || bean.getIsVipClient() != null) {
            values.put(PartyPaths._STG_Party._Role_IsVIPClient, bean.getIsVipClient());
        }

        values.put(PartyPaths._STG_Party._Role_Blacklist, bean.getBlacklist());
        values.put(PartyPaths._STG_Party._Role_PartnerState, bean.getPartnerState());
        values.put(PartyPaths._STG_Party._Role_CompanyPersonalNr, bean.getCompanyPersonalNr());
        values.put(PartyPaths._STG_Party._Role_SocialSecurityNr, bean.getSocialSecurityNr());
        values.put(PartyPaths._STG_Party._Role_SocialSecurityNrType, bean.getSocialSecurityNrType());
        values.put(PartyPaths._STG_Party._Role_BrokerRegNo, bean.getBrokerRegNo());
        values.put(PartyPaths._STG_Party._Role_BrokerTiering, bean.getBrokerTiering());
        values.put(PartyPaths._STG_Party._Role_LineOfBusiness, bean.getLineOfBusiness());
        values.put(PartyPaths._STG_Party._Financial_FinancePartyBusId, bean.getFinancePartyBusId());
        values.put(PartyPaths._STG_Party._Financial_PaymentTerms, bean.getPaymentTerms());
        values.put(PartyPaths._STG_Party._Financial_FinanceClients, bean.getFinanceClients());
        values.put(PartyPaths._STG_Party._Financial_CreditorClients, bean.getCreditorClients());
        values.put(PartyPaths._STG_Party._Control_SourceSystem, bean.getSourceSystem());
        values.put(PartyPaths._STG_Party._Control_SourcePartyBusId, bean.getSourcePartyBusId());
        values.put(PartyPaths._STG_Party._Control_ValidFrom, bean.getValidFrom());
        values.put(PartyPaths._STG_Party._Control_ValidTo, bean.getValidTo());
        values.put(PartyPaths._STG_Party._Control_CreationTimestamp, bean.getCreationTimestamp());
        values.put(PartyPaths._STG_Party._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
        values.put(PartyPaths._STG_Party._Control_Action, bean.getAction());
        values.put(PartyPaths._STG_Party._Control_ActionBy, bean.getActionBy());

        return values;

    }

}
