package com.elipslife.mdm.contract.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.elipslife.ebx.data.access.UpdateRecords;
import com.elipslife.mdm.claimcase.constants.ClaimCasePaths;
import com.elipslife.mdm.claimcase.core.ClaimCaseBean;
import com.elipslife.mdm.common.rules.WorkflowRules;
import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.contract.core.CoreContractCoverBean;
import com.elipslife.mdm.contract.core.CoreContractCoverReader;
import com.elipslife.mdm.contract.core.CoreExposureBean;
import com.elipslife.mdm.contract.core.CoreExposureReader;
import com.elipslife.mdm.contract.core.CoreInsuranceContractBean;
import com.elipslife.mdm.contract.core.CoreInsuranceContractReader;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.elipslife.mdm.path.UWPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.elipslife.mdm.uw.core.UWCaseBean;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.boot.VM;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.presales.toolbox.procedure.RecordValuesBean;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;


public class DeleteDuplicateInsuranceContractsTask extends ScriptTask {

	@Override
	public void executeScript(ScriptTaskContext aContext) throws OperationException {
		
		// get context variables
		HashMap<String, String> contextVariables = WorkflowRules.getContextVariables(aContext);
		
		String masterDataspace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		Repository repository = aContext.getRepository();
		AdaptationHome masterDataspaceHome =  repository.lookupHome(HomeKey.forBranchName(masterDataspace));
		Adaptation dataset = masterDataspaceHome.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
		
		// AdaptationHome claimCaseDsp =  repository.lookupHome(HomeKey.forBranchName(ClaimCaseConstants.DataSpace.CLAIMCASE_REF));
		// Adaptation claimCaseDs = claimCaseDsp.findAdaptationOrNull(AdaptationName.forName(ClaimCaseConstants.DataSet.CLAIMCASE_MASTER));
		// AdaptationTable claimCaseTable = claimCaseDs.getTable(ClaimCasePaths._ClaimCase.getPathInSchema());
		
		// AdaptationHome uwCaseDsp =  repository.lookupHome(HomeKey.forBranchName(Constants.DataSpace.UW_REFERENCE));
		// Adaptation uwCaseDs = uwCaseDsp.findAdaptationOrNull(AdaptationName.forName(Constants.DataSet.UW_MASTER));
		// AdaptationTable uwCaseTable = uwCaseDs.getTable(UWPaths._UWCase.getPathInSchema());
		
		CoreInsuranceContractReader insuranceContractReader = new CoreInsuranceContractReader(masterDataspace);
		List<CoreInsuranceContractBean> insuranceContractsList = insuranceContractReader.readAllBeans();
		
		// ClaimCaseReader claimCaseReader = new ClaimCaseReader();
    	// UWCaseReader uwCaseReader = new UWCaseReader();
    	CoreExposureReader exposureReader = new CoreExposureReader(masterDataspace);
    	CoreContractCoverReader coreContractCoverReader = new CoreContractCoverReader(masterDataspace);
    	
    	List<CoreInsuranceContractBean> insuranceContractsToBeDelete = new ArrayList<CoreInsuranceContractBean>();
    	List<CoreExposureBean> exposuresToDelete = new ArrayList<CoreExposureBean>();
    	List<CoreContractCoverBean> contractConversToDelete = new ArrayList<CoreContractCoverBean>();
    	int deleteRecordCount = 0;
    	
    	Map<String, List<CoreInsuranceContractBean>> insuranceContractsGroupedBySourceSystem = insuranceContractsList.stream().collect(Collectors.groupingBy(cpb -> cpb.getSourceSystem() + cpb.getSourceContractBusId()));
		for (Map.Entry<String, List<CoreInsuranceContractBean>> sourceSysAndBusIdKey : insuranceContractsGroupedBySourceSystem.entrySet()) {
			List<CoreInsuranceContractBean> coreInsuranceContractsList = sourceSysAndBusIdKey.getValue();
			if (coreInsuranceContractsList.size() == 1) {
				VM.log.kernelInfo("Single entry found for insurance contract source id "+coreInsuranceContractsList.get(0).getSourceContractBusId());
			} else {
				// Latest insurance contract based on last updated timestamp
				CoreInsuranceContractBean coreInsuranceContractBean = coreInsuranceContractsList.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getLastUpdateTimestamp(), a.getLastUpdateTimestamp())).findFirst().get();
				
				// remove latest insurance contract
				coreInsuranceContractsList.remove(coreInsuranceContractBean);
				
				for(CoreInsuranceContractBean insuranceContractBean : coreInsuranceContractsList) {
					
					deleteRecordCount++;
					// find the references in Claim Case
					// List<ClaimCaseBean> claimCaseList = claimCaseReader.findClaimCasesByInsuranceContracts(String.valueOf(insuranceContractBean.getId()));
					// updateInsuranceContractRefInClaimCase(claimCaseList, coreInsuranceContractBean, claimCaseTable, aContext.getSession(), claimCaseDsp);
					
					// find the references in UW
					// List<UWCaseBean> uwCaseList = uwCaseReader.findBeansByInsuranceContractId(String.valueOf(insuranceContractBean.getId()));
					// updateInsuranceContractRefInUWCase(uwCaseList, coreInsuranceContractBean, uwCaseTable, aContext.getSession(), uwCaseDsp);
					
					// add insurance contracts to delete
					insuranceContractsToBeDelete.add(insuranceContractBean);
					
					List<CoreExposureBean> exposureBeans = exposureReader.findExposuresByInsuranceContracts(String.valueOf(insuranceContractBean.getId()));
					for(CoreExposureBean coreExposureBean : exposureBeans) {
						// add exposures to delete
						exposuresToDelete.add(coreExposureBean);				
						
						List<CoreContractCoverBean> contractCoverBeans = coreContractCoverReader.findContractCoversByInsuranceContracts(String.valueOf(coreExposureBean.getId()));
						contractConversToDelete.addAll(contractCoverBeans);
					}
				}
				
				// Delete the duplicated insurance contracts
				if(deleteRecordCount > 500) {
					deleteInsuranceContracts(insuranceContractsToBeDelete, masterDataspaceHome, dataset, aContext.getSession());
					deleteExposures(exposuresToDelete, masterDataspaceHome, dataset, aContext.getSession());
					deleteContractCovers(contractConversToDelete, masterDataspaceHome, dataset, aContext.getSession());
					insuranceContractsToBeDelete.clear();
					exposuresToDelete.clear();
					contractConversToDelete.clear();
					deleteRecordCount = 0;
				}
			}
		}
			deleteInsuranceContracts(insuranceContractsToBeDelete, masterDataspaceHome, dataset, aContext.getSession());
			deleteExposures(exposuresToDelete, masterDataspaceHome, dataset, aContext.getSession());
			deleteContractCovers(contractConversToDelete, masterDataspaceHome, dataset, aContext.getSession());
			insuranceContractsToBeDelete.clear();
			exposuresToDelete.clear();
			contractConversToDelete.clear();
			deleteRecordCount = 0;
		
	}
	
	
	private void deleteInsuranceContracts(List<CoreInsuranceContractBean> insuranceContractBeans, AdaptationHome dataspaceName, Adaptation dataset, Session session) throws OperationException {
		
		AdaptationTable aTable = dataset.getTable(ContractPaths._InsuranceContract.getPathInSchema());
		ArrayList<Adaptation> recordValuesBeanList = new ArrayList<Adaptation>();
		for (CoreInsuranceContractBean coreInsuranceContractBean : insuranceContractBeans) {
			recordValuesBeanList.add(aTable.lookupAdaptationByPrimaryKey(
					PrimaryKey.parseString(coreInsuranceContractBean.getId().toString())));
		}

		// delete all insurance contracts
		if (!recordValuesBeanList.isEmpty()) {
			WorkflowRules.deleteRecords(dataspaceName, session, recordValuesBeanList);
			recordValuesBeanList.clear();
		}
	}

	private void deleteExposures(List<CoreExposureBean> exposureBeans, AdaptationHome dataspaceName, Adaptation dataset, Session session) throws OperationException {
		
		AdaptationTable aTable = dataset.getTable(ContractPaths._Exposure.getPathInSchema());
		ArrayList<Adaptation> recordValuesBeanList = new ArrayList<Adaptation>();
		for (CoreExposureBean exposureBean : exposureBeans) {
			recordValuesBeanList.add(aTable.lookupAdaptationByPrimaryKey(
					PrimaryKey.parseString(exposureBean.getId().toString())));
		}

		// Delete all exposures
		if (!recordValuesBeanList.isEmpty()) {
			WorkflowRules.deleteRecords(dataspaceName, session, recordValuesBeanList);
			recordValuesBeanList.clear();
		}
	}

	private void deleteContractCovers(List<CoreContractCoverBean> coreContractCovers, AdaptationHome dataspaceName, Adaptation dataset, Session session) throws OperationException {
	
		AdaptationTable aTable = dataset.getTable(ContractPaths._ContractCover.getPathInSchema());
		ArrayList<Adaptation> recordValuesBeanList = new ArrayList<Adaptation>();
		for (CoreContractCoverBean coreContractCoversBean : coreContractCovers) {
			recordValuesBeanList.add(aTable.lookupAdaptationByPrimaryKey(
					PrimaryKey.parseString(coreContractCoversBean.getId().toString())));
		}
		
		// Delete all contract covers
		if (!recordValuesBeanList.isEmpty()) {
			WorkflowRules.deleteRecords(dataspaceName, session, recordValuesBeanList);
			recordValuesBeanList.clear();
		}
	}
	
	private void updateInsuranceContractRefInClaimCase(List<ClaimCaseBean> claimCaseList, CoreInsuranceContractBean insuranceContractBean, AdaptationTable claimCaseTable,
			Session session, AdaptationHome cliamCaseDataspace) {
		
		if(!claimCaseList.isEmpty()) {
			List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
			HashMap<Path, Object> values = null;
			RecordValuesBean rvb = null;
			for (ClaimCaseBean claimCaseBean : claimCaseList) {
				values = new HashMap<>();
				values.put(ClaimCasePaths._ClaimCase._ClaimCase_General_InsuranceContractId, String.valueOf(insuranceContractBean.getId()));
				rvb = new RecordValuesBean(claimCaseTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(claimCaseBean.getId().toString())), values);
				recordValuesBeanList.add(rvb);
			}
			
			final ProgrammaticService service = ProgrammaticService.createForSession(session, cliamCaseDataspace);
			UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
			ProcedureResult result = service.execute(updateRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
			}
			claimCaseList.clear();
		}
	}
	
	private void updateInsuranceContractRefInUWCase(List<UWCaseBean> uwCaseList, CoreInsuranceContractBean insuranceContractBean, AdaptationTable uwCaseTable,
			Session session, AdaptationHome uwCaseDsp) {
		
		if(!uwCaseList.isEmpty()) {
			List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
			HashMap<Path, Object> values = null;
			RecordValuesBean rvb = null;
			for (UWCaseBean uwCaseBean : uwCaseList) {
				values = new HashMap<>();
				values.put(UWPaths._UWCase._Context_InsuranceContractId, String.valueOf(insuranceContractBean.getId()));
				rvb = new RecordValuesBean(uwCaseTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(uwCaseBean.getUWCaseId().toString())), values);
				recordValuesBeanList.add(rvb);
			}
			
			final ProgrammaticService service = ProgrammaticService.createForSession(session, uwCaseDsp);
			UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
			ProcedureResult result = service.execute(updateRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
			}
			uwCaseList.clear();
		}
	}
}
