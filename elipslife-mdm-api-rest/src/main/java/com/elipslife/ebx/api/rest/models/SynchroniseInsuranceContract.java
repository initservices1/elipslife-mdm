package com.elipslife.ebx.api.rest.models;

import javax.json.bind.annotation.JsonbProperty;

public class SynchroniseInsuranceContract {
	
	@JsonbProperty("Context")
	public BusinessContext context;
	
	@JsonbProperty("Data")
	public InsuranceContract data;
}