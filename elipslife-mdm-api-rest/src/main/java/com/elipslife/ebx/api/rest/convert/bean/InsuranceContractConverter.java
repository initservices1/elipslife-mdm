package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.Employee;
import com.elipslife.ebx.api.rest.models.InsuranceContract;
import com.elipslife.ebx.api.rest.models.Party;
import com.elipslife.ebx.api.rest.models.SynchroniseInsuranceContract;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractBean;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class InsuranceContractConverter extends AbstractConverter<LandingInsuranceContractBean, SynchroniseInsuranceContract> {

	@Override
	public LandingInsuranceContractBean convert(SynchroniseInsuranceContract synchroniseInsuranceContract)
			throws DatatypeConfigurationException {
		
		InsuranceContract insuranceContract = synchroniseInsuranceContract.data;
		
		LandingInsuranceContractBean landingInsuranceContractBean = new LandingInsuranceContractBean();
		
		landingInsuranceContractBean.setDataOwner(insuranceContract.dataOwner);
		landingInsuranceContractBean.setContractStageBusId(insuranceContract.contractStageBusId);
		landingInsuranceContractBean.setContractNumber(insuranceContract.contractNumber);
		landingInsuranceContractBean.setOfferNumber(insuranceContract.offerNumber);
		landingInsuranceContractBean.setApplicationNumber(insuranceContract.applicationNumber);
		landingInsuranceContractBean.setContractBusId2(insuranceContract.contractBusId2);
		landingInsuranceContractBean.setCustomerRefNumber(insuranceContract.customerRefNumber);

		Date businessContractBeginDate = insuranceContract.businessContractBeginDate == null ? null : DateHelper.convertToDate(insuranceContract.businessContractBeginDate);
		Date contractExpiryDate = insuranceContract.contractExpiryDate == null ? null : DateHelper.convertToDate(insuranceContract.contractExpiryDate);
		Date contractBeginDate = insuranceContract.contractBeginDate == null ? null : DateHelper.convertToDate(insuranceContract.contractBeginDate);
		Date contractEndDate = insuranceContract.contractEndDate == null ? null : DateHelper.convertToDate(insuranceContract.contractEndDate);
		
		landingInsuranceContractBean.setContractStage(insuranceContract.contractStage);
		landingInsuranceContractBean.setContractState(insuranceContract.contractState);
		landingInsuranceContractBean.setOfferState(insuranceContract.offerState);
		landingInsuranceContractBean.setApplicationState(insuranceContract.applicationState);
		landingInsuranceContractBean.setContractClass(insuranceContract.contractClass);
		landingInsuranceContractBean.setBusinessContractBeginDate(businessContractBeginDate);
		landingInsuranceContractBean.setContractExpiryDate(contractExpiryDate);
		landingInsuranceContractBean.setContractBeginDate(contractBeginDate);
		landingInsuranceContractBean.setContractEndDate(contractEndDate);
		landingInsuranceContractBean.setBusinessScope(insuranceContract.businessScope);
		landingInsuranceContractBean.setContractName(insuranceContract.contractName);
		landingInsuranceContractBean.setAlternativeContractName(insuranceContract.alternativeContractName);
		landingInsuranceContractBean.setClientSegment(insuranceContract.clientSegment);
		landingInsuranceContractBean.setDistributionAgreementBusId(insuranceContract.distributionAgreementBusId);
		landingInsuranceContractBean.setIsActive(insuranceContract.isActive);
		landingInsuranceContractBean.setIsProfitParticipation(insuranceContract.isProfitParticipation);
		landingInsuranceContractBean.setLineOfBusiness(insuranceContract.lineOfBusiness);
		landingInsuranceContractBean.setOperatingCountry(insuranceContract.operatingCountry);
		
		Party policyholder = insuranceContract.policyholder;
		if(policyholder != null) {
			landingInsuranceContractBean.setSourcePolicyholderBusId(policyholder.partyBusId);
			landingInsuranceContractBean.setPolicyholderBusId(policyholder.crmId);
		}
		
		Party broker = insuranceContract.broker;
		if(broker != null) {
			landingInsuranceContractBean.setSourceBrokerBusId(broker.partyBusId);
			landingInsuranceContractBean.setBrokerBusId(broker.crmId);
		}
		
		Party attachedPartner = insuranceContract.attachedPartner;
		if(attachedPartner != null) {
			landingInsuranceContractBean.setSourceAttachedPartnerBusId(attachedPartner.partyBusId);
			landingInsuranceContractBean.setAttachedPartnerBusId(attachedPartner.crmId);
		}
		
		Employee adminResponsible = insuranceContract.adminResponsible;
		if(adminResponsible != null) {
			landingInsuranceContractBean.setSourceAdminResponsibleBusId(adminResponsible.partyBusId);
			landingInsuranceContractBean.setAdminResponsibleUserName(adminResponsible.userName);
		}
		
		Employee claimResponsible = insuranceContract.claimResponsible;
		if(claimResponsible != null) {
			landingInsuranceContractBean.setSourceClaimResponsibleBusId(claimResponsible.partyBusId);
			landingInsuranceContractBean.setClaimResponsibleUserName(claimResponsible.userName);
		}
		
		Employee caseMgmtResponsible = insuranceContract.caseMgmtResponsible;
		if(caseMgmtResponsible != null) {
			landingInsuranceContractBean.setSourceCaseMgmtResponsibleBusId(caseMgmtResponsible.partyBusId);
			landingInsuranceContractBean.setCaseMgmtResponsibleUserName(caseMgmtResponsible.userName);
		}
		
		Employee salesResponsible = insuranceContract.salesResponsible;
		if(salesResponsible != null) {
			landingInsuranceContractBean.setSourceSalesResponsibleBusId(salesResponsible.partyBusId);
			landingInsuranceContractBean.setSalesResponsibleUserName(salesResponsible.userName);
		}
		
		Date creationTimestamp = insuranceContract.creationTimestamp == null ? null : Date.from(insuranceContract.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
		Date lastUpdateTimestamp = insuranceContract.lastUpdateTimestamp == null ? null : Date.from(insuranceContract.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
		
		landingInsuranceContractBean.setSourceSystem(synchroniseInsuranceContract.context.sourceSystemId);
		landingInsuranceContractBean.setSourceContractBusId(insuranceContract.sourceContractBusId);
		landingInsuranceContractBean.setCreationTimestamp(creationTimestamp);
		landingInsuranceContractBean.setLastUpdateTimestamp(lastUpdateTimestamp);
		landingInsuranceContractBean.setAction(insuranceContract.action);
		landingInsuranceContractBean.setActionBy(synchroniseInsuranceContract.context.userId);
		
		return landingInsuranceContractBean;
	}
}