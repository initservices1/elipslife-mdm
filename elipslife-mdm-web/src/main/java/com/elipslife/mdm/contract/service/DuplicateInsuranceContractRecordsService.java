package com.elipslife.mdm.contract.service;

import java.util.Locale;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.base.text.UserMessageLocalized;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserService;
import com.orchestranetworks.userservice.declaration.ActivationContextOnTableView;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;
import com.orchestranetworks.userservice.declaration.UserServicePropertiesDefinitionContext;
import com.orchestranetworks.userservice.declaration.WebComponentDeclarationContext;

public class DuplicateInsuranceContractRecordsService implements UserServiceDeclaration.OnTableView {

	@Override
	public void declareWebComponent(WebComponentDeclarationContext aContext) {
		aContext.setAvailableAsPerspectiveAction(Boolean.TRUE);

	}

	@Override
	public void defineActivation(ActivationContextOnTableView aContext) {
		aContext.includeSchemaNodesMatching(ContractPaths._InsuranceContract.getPathInSchema());
	}

	@Override
	public void defineProperties(UserServicePropertiesDefinitionContext aContext) {
		aContext.setLabel("Download insurance contract refs(excel)");
		UserMessageLocalized userMessage = new UserMessageLocalized();
		userMessage.setMessage(Locale.ENGLISH,"This will download duplicated insurance contracts(Excel).");
		//userMessage.setMessage(Locale.GERMAN,"Dadurch wird mit dem aktuellen Datenmodell synchronisiert und die Metadaten des Datenmodells (Excel) heruntergeladen.");
		aContext.setConfirmationMessageBeforeLaunch(userMessage);
	}

	@Override
	public ServiceKey getServiceKey() {
		return ContractConstants.USER_SERVICE_EXPORT_DATAMODEL_TO_EXCEL;
	}

	@Override
	public UserService<TableViewEntitySelection> createUserService() {
		return new ExportDuplicateInsuranceContractUserService();
	}
}
