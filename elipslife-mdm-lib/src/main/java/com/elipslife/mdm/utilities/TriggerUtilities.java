package com.elipslife.mdm.utilities;

import java.util.Calendar;
import java.util.Date;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.ValueContextForUpdate;

public class TriggerUtilities {
	
	/**
	 * 
	 * @param valueContextForUpdate
	 * @param path
	 */
	public static void setDefaultValueIfNull(ValueContextForUpdate valueContextForUpdate, Path path) {
		
		SchemaNode schemaNode = valueContextForUpdate.getNode(path);
		Object defaultValue = schemaNode.getDefaultValue();
		
		Object object = valueContextForUpdate.getValue(path);
		if(object == null) {
			valueContextForUpdate.setValue(defaultValue, path);
		}
	}
	
	/**
	 * 
	 * @param valueContextForUpdate
	 * @param path
	 */
	public static void setMinimumDateIfNull(ValueContextForUpdate valueContextForUpdate, Path path) {

		Object object = valueContextForUpdate.getValue(path);
		if(object == null) {
			
			Calendar calendar = Calendar.getInstance();
			
			calendar.set(1900, java.util.Calendar.JANUARY, 1);
			Date dateMinimum = calendar.getTime();
			
			valueContextForUpdate.setValue(dateMinimum, path);
		}
	}
	
	/**
	 * 
	 * @param valueContextForUpdate
	 * @param path
	 */
	public static void setMaximumDateIfNull(ValueContextForUpdate valueContextForUpdate, Path path) {

		Object object = valueContextForUpdate.getValue(path);
		if(object == null) {
			
			Calendar calendar = Calendar.getInstance();
			
			calendar.set(9999, java.util.Calendar.DECEMBER, 31);
			Date dateMaximum = calendar.getTime();
			
			valueContextForUpdate.setValue(dateMaximum, path);
		}
	}
}