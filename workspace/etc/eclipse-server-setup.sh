#!/bin/bash

realpath() {
    if ! pushd $1 &> /dev/null; then
        pushd ${1##*/} &> /dev/null
        echo $( pwd -P )/${1%/*}
    else
        pwd -P
    fi
}

export WORKSPACE_DIR=$(realpath $(dirname $0)/..)
export ROOT_DIR=$(realpath $(dirname $0)/../../..)
export OS_WORKSPACE_DIR=$(echo ${WORKSPACE_DIR} | sed 's|/c/|c:/|')
echo "use ${OS_WORKSPACE_DIR} as base dir in all relevant configuration files"

# create and copy settings folder for ebx-server to indicate this project as server module
mkdir -p ${WORKSPACE_DIR}/.settings
cp ${WORKSPACE_DIR}/etc/_settings/* ${WORKSPACE_DIR}/.settings/

# copy the tomcat server launch options - set the ebx.home. The file must not be checked into the git repo
for launch in ${WORKSPACE_DIR}/etc/*.launch.template
do 
	envsubst < ${launch} > ${WORKSPACE_DIR}/$(basename ${launch%.template})
done

# copy metadata preferences and replace paths
if [ ! -f ${ROOT_DIR}/.metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.wst.server.core.prefs ]; then
	# create metadata preferences folder
	mkdir -p ${ROOT_DIR}/.metadata/.plugins/org.eclipse.core.runtime/.settings/
	for pref in ${WORKSPACE_DIR}/etc/_metadata/org.eclipse.core.runtime/_settings/*.prefs;
	do
	  echo "copy ${pref} to .metadata"
	  envsubst < ${pref} > ${ROOT_DIR}/.metadata/.plugins/org.eclipse.core.runtime/.settings/$(basename ${pref})
	done
else
	echo "*"
	echo "* This is an existings workspace. Server Runtime must be registered manualy."
	echo "* !!! Register Tomcat Server v9.0 with id='Tomcat-$(basename $(realpath ${WORKSPACE_DIR}/../))' and location='${OS_WORKSPACE_DIR}/tomcat' in eclipse !!!"
	echo "* !!! (Window > Preferences > Server > Runtime Environment > Add... !!!"
	echo "* !!! (Window > Preferences > General > Workspace > Enable 'Refresh using native hooks or polling' !!!"
	echo "*"
fi

# initial build and download of the Tomcat runtime
mvn install -f ${WORKSPACE_DIR}/pom.xml -q

echo
echo "******************************************************************************************"
echo "*  Setup Completed."
echo "*"
echo "*  Start Eclipse workspace in $(realpath ${ROOT_DIR})"
echo "*    and 'Import existing Maven Projects' from $(realpath ${WORKSPACE_DIR}/../)/pom.xml"
echo "******************************************************************************************"
