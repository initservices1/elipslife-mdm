package com.elipslife.mdm.party.scheduler;

import java.util.List;

import com.elipslife.ebx.data.access.CoreBankAccountBuilder;
import com.elipslife.ebx.data.access.StagingBankAccountReader;
import com.elipslife.ebx.data.access.StagingBankAccountWriter;
import com.elipslife.ebx.data.access.StagingBankAccountWriterSession;
import com.elipslife.ebx.domain.bean.StagingBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;

public class MigrateBankAccountFromStagingToCoreTask extends MigrateFromStagingToCoreTaskBase {

	@Override
	public void execute(ScheduledExecutionContext scheduledExecutionContext) {

  		StagingBankAccountWriter stagingBankAccountWriter = new StagingBankAccountWriterSession(scheduledExecutionContext.getSession(), true);
		StagingBankAccountReader stagingBankAccountReader = (StagingBankAccountReader) stagingBankAccountWriter.getTableReader();

		List<StagingBankAccountBean> stagingBankAccountBeans = stagingBankAccountReader.findBeansUpToDate();

		stagingBankAccountBeans.forEach(sbab ->
		{
			try {
				CoreBankAccountBuilder.createOrUpdateCoreBankAccountByStagingBankAccount(scheduledExecutionContext.getSession(), sbab, PartyConstants.DataSpace.PARTY_REFERENCE);
			}
			catch(Exception ex) {
				scheduledExecutionContext.addExecutionInformation(String.format("Migration error for staging bank account with ID %s:\r\n%s", sbab.getId(), ex.toString()));
			}
		});
	}
}