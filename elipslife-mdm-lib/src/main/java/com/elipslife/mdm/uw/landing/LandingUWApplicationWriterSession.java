package com.elipslife.mdm.uw.landing;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class LandingUWApplicationWriterSession extends LandingUWApplicationWriter {
    private Session session;

    public LandingUWApplicationWriterSession(Session session) {
        super();
        this.session = session;
    }

    public LandingUWApplicationWriterSession(Session session, String dataspace) {
        super(dataspace);
        this.session = session;
    }

    @Override
    protected void executeProcedure(Procedure procedure) {
        ProcedureResult result = ProgrammaticService.createForSession(session, dataSpace).execute(procedure);
        if (result.hasFailed()) {
            throw new RuntimeException(result.getException());
        }
    }
}
