package com.elipslife.mdm.conditions;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class LastScriptTaskAccepted extends ConditionBean {	
	private String errorMessage;
	
	@Override
	public boolean evaluateCondition(ConditionBeanContext context) throws OperationException {
		if(errorMessage != null && errorMessage.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return this.errorMessage;
	}

}
