package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.json.bind.annotation.JsonbProperty;

public class Address {
	
	@JsonbProperty("AddressType")
	public String addressType;
	
	@JsonbProperty("AddressUse")
	public String addressUse;
	
	@JsonbProperty("SourceAddressBusId")
	public String sourceAddressBusId;
	
	@JsonbProperty("ValidFrom")
	public LocalDate validFrom;
	
	@JsonbProperty("ValidTo")
	public LocalDate validTo;
	
	@JsonbProperty("CreationTimestamp")
	public LocalDateTime creationTimestamp;
	
	@JsonbProperty("LastUpdateTimestamp")
	public LocalDateTime lastUpdateTimestamp;
	
	@JsonbProperty("Action")
	public String action;
	
	
	//Postal Address
	@JsonbProperty("Street")
	public String street;
	
	@JsonbProperty("Street2")
	public String street2;
	
	@JsonbProperty("POBox")
	public String poBox;
	
	@JsonbProperty("POBoxPostCode")
	public String poBoxPostCode;
	
	@JsonbProperty("POBoxTown")
	public String poBoxTown;
	
	@JsonbProperty("PostCode")
	public String postCode;
	
	@JsonbProperty("District")
	public String district;
	
	@JsonbProperty("Town")
	public String town;
	
	@JsonbProperty("StateProvince")
	public String stateProvince;
	
	@JsonbProperty("Country")
	public String country;
	
	
	//Electronic Address
	@JsonbProperty("ElectronicAddressType")
	public String electronicAddressType;
	
	@JsonbProperty("Address")
	public String address;
}