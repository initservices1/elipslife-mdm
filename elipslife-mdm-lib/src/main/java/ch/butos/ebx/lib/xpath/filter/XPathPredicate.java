package ch.butos.ebx.lib.xpath.filter;


import com.onwbp.adaptation.XPathExpressionHelper;
import com.orchestranetworks.schema.Path;

/**
 * https://dl.orchestranetworks.com/restricted/documentation/en/advanced/references/xpath_ref.html
 *
 */
public class XPathPredicate {

    public enum Operation {
        Equals, Contains, StartsWith, EndsWith, IsNull, IsNotNull, DateEqual, DateLessThan, DateGreaterThan
    }

    private Operation filterOperation;
    private Path path;
    private String value;

    public XPathPredicate(Operation filterOperation, String fieldName, String value) {
        this(filterOperation, Path.parse(String.format("./%s", fieldName)), value);
    }

    public XPathPredicate(Operation filterOperation, Path path, String value) {
        this.filterOperation = filterOperation;
        this.path = path;
        this.value = value;
    }

    @Override
    public String toString() {

    	String encodedLiteralStringWithDelimiters = XPathExpressionHelper.encodeLiteralStringWithDelimiters(this.value != null ? this.value : "null");
    	
        switch (this.filterOperation) {
            case Equals:
                return String.format("%s=%s", this.path.format(), encodedLiteralStringWithDelimiters);
            case Contains:
                return String.format("osd:contains-case-insensitive(%s, %s)", this.path.format(), encodedLiteralStringWithDelimiters);
            case StartsWith:
                return String.format("osd:starts-with-case-insensitive(%s, %s)", this.path.format(), encodedLiteralStringWithDelimiters);
            case EndsWith:
                return String.format("ends-with-case-insensitive(%s, %s)", this.path.format(), encodedLiteralStringWithDelimiters);
            case IsNull:
                return String.format("osd:is-null(%s)", this.path.format());
            case IsNotNull:
                return String.format("osd:is-not-null(%s)", this.path.format());
            case DateEqual:
                return String.format("date-equal(%s, '%s')", this.path.format(), this.value);
            case DateLessThan:
                return String.format("date-less-than(%s, '%s')", this.path.format(), this.value);
            case DateGreaterThan:
                return String.format("date-greater-than(%s, '%s')", this.path.format(), this.value);
            default:
                throw new RuntimeException(String.format("FilterOperation %s not implemented", this.filterOperation));
        }
    }
}