package com.elipslife.mdm.contract.trigger;

import java.util.Date;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.ContractPaths;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TableTriggerExecutionContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class CoreInsuranceContractTrigger extends TableTrigger {

	@Override
	public void setup(TriggerSetupContext context) {
		
	}
	

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException {
		super.handleBeforeCreate(context);
		
		ValueContextForUpdate valueContextForUpdate = context.getOccurrenceContextForUpdate();
		
		String sourceSystem = (String) valueContextForUpdate.getValue(ContractPaths._InsuranceContract._Control_SourceSystem);
		String contractStageBusId = (String) valueContextForUpdate.getValue(ContractPaths._InsuranceContract._ContractIds_ContractStageBusId);
		if(contractStageBusId == null) {
			contractStageBusId = updateContractStageBusIdIfNull(context, valueContextForUpdate, contractStageBusId);
			valueContextForUpdate.setValueEnablingPrivilegeForNode(contractStageBusId, ContractPaths._InsuranceContract._ContractIds_ContractStageBusId);
		}
		
		valueContextForUpdate.setValueEnablingPrivilegeForNode(
				String.format(
						"%s-%s",
						sourceSystem,
						contractStageBusId),
				ContractPaths._InsuranceContract._ContractIds_InsuranceContractUUID);
		
		valueContextForUpdate.setValueEnablingPrivilegeForNode(Constants.RecordOperations.CREATE, ContractPaths._Exposure._Control_LastSyncAction);
		valueContextForUpdate.setValueEnablingPrivilegeForNode(new Date(), ContractPaths._Exposure._Control_LastSyncTimestamp);
	}
	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context) throws OperationException {
		super.handleBeforeModify(context);
		
		ValueContextForUpdate valueContextForUpdate = context.getOccurrenceContextForUpdate();
		
		String sourceSystem = (String) valueContextForUpdate.getValue(ContractPaths._InsuranceContract._Control_SourceSystem);
		String contractStageBusId = (String) valueContextForUpdate.getValue(ContractPaths._InsuranceContract._ContractIds_ContractStageBusId);
		String lastSyncAction = (String) valueContextForUpdate.getValue(ContractPaths._InsuranceContract._Control_LastSyncAction);
		if(contractStageBusId == null) {
			contractStageBusId = updateContractStageBusIdIfNull(context, valueContextForUpdate, contractStageBusId);
			valueContextForUpdate.setValueEnablingPrivilegeForNode(contractStageBusId, ContractPaths._InsuranceContract._ContractIds_ContractStageBusId);
		}
		
		
		valueContextForUpdate.setValueEnablingPrivilegeForNode(
				String.format(
						"%s-%s",
						sourceSystem,
						contractStageBusId),
				ContractPaths._InsuranceContract._ContractIds_InsuranceContractUUID);
		if(lastSyncAction != null && lastSyncAction.equals(Constants.RecordOperations.DELETE)) {
			valueContextForUpdate.setValueEnablingPrivilegeForNode(Constants.RecordOperations.DELETE, ContractPaths._InsuranceContract._Control_LastSyncAction);
		} else {
			valueContextForUpdate.setValueEnablingPrivilegeForNode(Constants.RecordOperations.UPDATE, ContractPaths._InsuranceContract._Control_LastSyncAction);
		}
		valueContextForUpdate.setValueEnablingPrivilegeForNode(new Date(), ContractPaths._InsuranceContract._Control_LastSyncTimestamp);
	}
	
	/**
	 * Update ContractStage Bus Id if it is null
	 * 
	 * @param aContext
	 * @param valueContext
	 */
	private String updateContractStageBusIdIfNull(TableTriggerExecutionContext aContext, ValueContextForUpdate valueContext, String contractStageBusId) {
		
		//String contractStageBusId = (String) aContext.getOccurrenceContext().getValue(ContractPaths._InsuranceContract._ContractIds_ContractStageBusId);
		//if(contractStageBusId == null || contractStageBusId.isEmpty()) {
			String contractStage = (String) aContext.getOccurrenceContext().getValue(ContractPaths._InsuranceContract._ContractCore_ContractStage);
			if(contractStage != null) {
				if(contractStage.equals("1")) {
					contractStageBusId = (String) aContext.getOccurrenceContext().getValue(ContractPaths._InsuranceContract._ContractIds_OfferNumber);
				} else if(contractStage.equals("2")) {
					contractStageBusId = (String) aContext.getOccurrenceContext().getValue(ContractPaths._InsuranceContract._ContractIds_ApplicationNumber);
				} else if(contractStage.equals("3")) {
					contractStageBusId = (String) aContext.getOccurrenceContext().getValue(ContractPaths._InsuranceContract._ContractIds_ContractNumber);
				}
			}
		//}
		
		return contractStageBusId;
	}
}