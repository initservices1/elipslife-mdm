package com.elipslife.mdm.contract.constraint;

import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import com.butos.ebx.smart.validation.SmartRecordAndTableLevelValidationCheck;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;

public class StagingInsuranceContractConstraint extends SmartRecordAndTableLevelValidationCheck {

	@Override
	protected void executeBusinessValidations(ValueContext valueContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {
		
		validateMandatoryCheck(
				valueContext,
				validationInfoMap);
	}
		
	/**
	 * Application Number, Offer Number, Contract Number : Any of these fields are mandatory
	 * 
	 * @param valueContext
	 * @param validationInfoMap
	 */
	private void validateMandatoryCheck(
			ValueContext valueContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap) {
		
		if(!(isFilledWithAnyValue(valueContext, ContractPaths._STG_InsuranceContract._ContractIds_ApplicationNumber) ||
				isFilledWithAnyValue(valueContext, ContractPaths._STG_InsuranceContract._ContractIds_OfferNumber)  ||
				isFilledWithAnyValue(valueContext, ContractPaths._STG_InsuranceContract._ContractIds_ContractNumber))) {
			
			UserMessage userMessage = UserMessage.createError(String.format("Application Number, Offer Number, Contract Number : Any of these fields are mandatory"));
			super.insertUserMsg(validationInfoMap, valueContext.getNode(ContractPaths._InsuranceContract._ContractIds_ApplicationNumber), userMessage);
			return;
		}
		
	}
	
	/**
	 * Check the field have some value
	 * 
	 * @param valueContext
	 * @param pathId
	 * @return
	 */
	private boolean isFilledWithAnyValue(ValueContext valueContext, Path pathId) {
		
		String valueId = (String) valueContext.getValue(pathId);
		if(valueId == null || valueId.isEmpty()) {
			return false;
		}
		
		return true;
	}
	

	@Override
	public void setup(ConstraintContextOnTable aContext) {
		
	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		return null;
	}

}
