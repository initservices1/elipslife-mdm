package com.elipslife.mdm.contract.core;

import java.util.List;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class CoreExposureReader extends AbstractTableReader<CoreExposureBean> {

	public CoreExposureReader() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public CoreExposureReader(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._Exposure.getPathInSchema()));
	}
	
	public CoreExposureReader(AdaptationTable table) {
		super(table);
	}

	@Override
	public CoreExposureBean createBeanFromRecord(Adaptation record) {
		
		CoreExposureBean bean = new CoreExposureBean();
		
		bean.setInsuranceContractId(record.getString(ContractPaths._Exposure._InsuranceContract_InsuranceContractId));
		
		bean.setId(record.get_int(ContractPaths._Exposure._ExposureGeneral_ExposureId));
		bean.setExposureBusId(record.getString(ContractPaths._Exposure._ExposureGeneral_ExposureBusId));
		bean.setName(record.getString(ContractPaths._Exposure._ExposureGeneral_Name));
		bean.setProductId(record.getString(ContractPaths._Exposure._ExposureGeneral_ProductId));
		bean.setProductBusId(record.getString(ContractPaths._Exposure._ExposureGeneral_ProductBusId));
		bean.setProductGroup(record.getString(ContractPaths._Exposure._ExposureGeneral_ProductGroup));
		bean.setProductName(record.getString(ContractPaths._Exposure._ExposureGeneral_ProductName));
		
		bean.setSourceSystem(record.getString(ContractPaths._Exposure._Control_SourceSystem));
		bean.setCreatedBy(record.getString(ContractPaths._Exposure._Control_CreatedBy));
		bean.setCreationTimestamp(record.getDate(ContractPaths._Exposure._Control_CreationTimestamp));
		bean.setLastUpdatedBy(record.getString(ContractPaths._Exposure._Control_LastUpdatedBy));
		bean.setLastUpdateTimestamp(record.getDate(ContractPaths._Exposure._Control_LastUpdateTimestamp));
		bean.setLastSyncAction(record.getString(ContractPaths._Exposure._Control_LastSyncAction));
		bean.setLastSyncTimestamp(record.getDate(ContractPaths._Exposure._Control_LastSyncTimestamp));
		
		return bean;
	}

	@Override
	protected Path[] getPkPaths() {
		return new Path[] { ContractPaths._Exposure._ExposureGeneral_ExposureId };
	}

	@Override
	protected Object[] retrievePks(CoreExposureBean bean) {
		return new Object[] { bean.getId() };
	}
	
	/**
	 * 
	 * @param sourceSystem
	 * @param exposureBusId
	 * @return
	 */
	public List<CoreExposureBean> findBeansBySourceSystemAndExposureBusId(String sourceSystem, String exposureBusId) {
		
		XPathPredicate predicate1 = new XPathPredicate(Operation.Equals, ContractPaths._Exposure._Control_SourceSystem, sourceSystem);
		XPathPredicate predicate2 = new XPathPredicate(Operation.Equals, ContractPaths._Exposure._ExposureGeneral_ExposureBusId, exposureBusId);
		
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate1);
		group.addPredicate(predicate2);
		
		return super.readBeansFor(group.toString());
	}
	
	public List<CoreExposureBean> findExposuresByInsuranceContracts(String insuranceContractId) {
    	XPathPredicate insuranceContractPredicate = new XPathPredicate(XPathPredicate.Operation.Equals, ContractPaths._Exposure._InsuranceContract_InsuranceContractId, insuranceContractId);
		return super.readBeansFor(insuranceContractPredicate.toString());
	}
	
	
	
}