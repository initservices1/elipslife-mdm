package com.elipslife.mdm.contract.core;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class CoreInsuranceContractWriterProcedureContext extends CoreInsuranceContractWriter {

	private ProcedureContext procedureContext;
	
	public CoreInsuranceContractWriterProcedureContext(ProcedureContext procedureContext) {
		this(procedureContext, false);
	}
	
	public CoreInsuranceContractWriterProcedureContext(ProcedureContext procedureContext, boolean useDefaultValues) {
		super(useDefaultValues);
		this.procedureContext = procedureContext;
	}
	
	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}