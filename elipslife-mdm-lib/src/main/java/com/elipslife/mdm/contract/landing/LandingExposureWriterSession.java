package com.elipslife.mdm.contract.landing;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class LandingExposureWriterSession extends LandingExposureWriter {

	private Session session;
	
	public LandingExposureWriterSession(Session session) {
		this.session = session;
	}
	
	@Override
	protected void executeProcedure(Procedure procedure) {
		ProcedureResult procedureResult = ProgrammaticService.createForSession(this.session,this.dataSpace).execute(procedure);
		if(procedureResult.hasFailed()) {
			throw new RuntimeException(procedureResult.getException());
		}
	}
}