package com.elipslife.ebx.api.rest.models;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.json.bind.annotation.JsonbProperty;

public class UWCases {
	
	@JsonbProperty("DataOwner")
	public String dataOwner;
    
	@JsonbProperty("UWCaseBusId")
	public String UWCaseBusId;
	
	@JsonbProperty("UWApplicationBusId")
	public String uwApplicationBusId;
	
	@JsonbProperty("ContractNumber")
	public String contractNumber;
	
	@JsonbProperty("ApplicantBusId")
	public String applicantBusId;
	
	@JsonbProperty("ApplicantRelationship")
	public String applicantRelationship;
	
	@JsonbProperty("ApplicantFirstName")
	public String applicantFirstName;
	
	@JsonbProperty("ApplicantLastName")
	public String applicantLastName;
	
	@JsonbProperty("ApplicantDateOfBirth")
	public LocalDate applicantDateOfBirth;
	
	@JsonbProperty("ApplicantSSN")
	public String applicantSSN;
	
	@JsonbProperty("ApplicantTown")
	public String applicantTown;
	
	@JsonbProperty("ApplicantPostCode")
	public String applicantPostCode;
	
	@JsonbProperty("ApplicantCountry")
	public String applicantCountry;
	
	@JsonbProperty("ApplicantClass")
	public String applicantClass;		
	
	@JsonbProperty("ProductType")
	public String productType;
	
	@JsonbProperty("EligibilityDate")
	public LocalDate eligibilityDate;

	@JsonbProperty("ApplicationReason")
	public String applicationReason;
	
	@JsonbProperty("ApplicationReasonText")
	public String applicationReasonText;
	
	@JsonbProperty("InforceCoverage")
	public BigDecimal inforceCoverage;
	
	@JsonbProperty("AdditionalCoverage")
	public BigDecimal additionalCoverage;
	
	@JsonbProperty("IsActive")
	public Boolean isActive;
	
	@JsonbProperty("CaseState ")
	public String caseState;
	
	@JsonbProperty("CreatedBy")
    public String createdBy;
    
    @JsonbProperty("CreationTimestamp")
    public LocalDateTime creationTimestamp;
    
    @JsonbProperty("LastUpdatedBy")
    public String lastUpdatedBy;
    
    @JsonbProperty("LastUpdateTimestamp")
    public LocalDateTime lastUpdateTimestamp;
    
    @JsonbProperty("DeletionTimestamp")
	public LocalDateTime deletionTimestamp;
    
    @JsonbProperty("Action")
    public String action;
}
