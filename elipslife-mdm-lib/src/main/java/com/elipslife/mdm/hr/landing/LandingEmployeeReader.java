package com.elipslife.mdm.hr.landing;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.LandingHrModelPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class LandingEmployeeReader extends AbstractTableReader<LandingEmployeeBean> {

    public LandingEmployeeReader() {
        this(Constants.DataSpace.LANDING_HR_REFERENCE);
    }

    public LandingEmployeeReader(String dataspace) {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.LANDING_HR_MASTER),
				LandingHrModelPaths._Employee.getPathInSchema()));
    }

    public LandingEmployeeReader(AdaptationTable table) {
		super(table);
	}

	@Override
    public LandingEmployeeBean createBeanFromRecord(Adaptation record) {
    	LandingEmployeeBean bean = new LandingEmployeeBean();
        bean.setEmployeeId(record.get_int(LandingHrModelPaths._Employee._Employee_EmployeeId));
        bean.setDataOwner(record.getString(LandingHrModelPaths._Employee._Employee_DataOwner));
        bean.setEmployeeBusId(record.getString(LandingHrModelPaths._Employee._Employee_EmployeeBusId));
        bean.setUserName(record.getString(LandingHrModelPaths._Employee._Employee_UserName));
        bean.setFirstName(record.getString(LandingHrModelPaths._Employee._Employee_FirstName));
        bean.setPreferredFirstName(record.getString(LandingHrModelPaths._Employee._Employee_PreferredFirstName));
        bean.setMiddleName(record.getString(LandingHrModelPaths._Employee._Employee_MiddleName));
        bean.setLastName(record.getString(LandingHrModelPaths._Employee._Employee_LastName));
        bean.setGender(record.getString(LandingHrModelPaths._Employee._Employee_Gender));
        bean.setSalutation(record.getString(LandingHrModelPaths._Employee._Employee_Salutation));
        bean.setLanguage(record.getString(LandingHrModelPaths._Employee._Employee_Language));
        bean.setEmail(record.getString(LandingHrModelPaths._Employee._Employee_Email));
        bean.setTelephone(record.getString(LandingHrModelPaths._Employee._Employee_Telephone));
        bean.setMobile(record.getString(LandingHrModelPaths._Employee._Employee_Mobile));
        bean.setFax(record.getString(LandingHrModelPaths._Employee._Employee_Fax));
        bean.setIsActive((Boolean)record.get(LandingHrModelPaths._Employee._Employee_IsActive));
        bean.setIsSystemEnabled((Boolean)record.get(LandingHrModelPaths._Employee._Employee_IsSystemEnabled));
        bean.setEmploymentCategory(record.getString(LandingHrModelPaths._Employee._Employment_EmploymentCategory));
        bean.setManagementLevel(record.getString(LandingHrModelPaths._Employee._Employment_ManagementLevel));
        bean.setSignOffRole(record.getString(LandingHrModelPaths._Employee._Employment_SignOffRole));
        bean.setFunctionalTitle(record.getString(LandingHrModelPaths._Employee._JobPosition_FunctionalTitle));
        bean.setPositionName(record.getString(LandingHrModelPaths._Employee._JobPosition_PositionName));
        bean.setJobProfileName(record.getString(LandingHrModelPaths._Employee._JobPosition_JobProfileName));
        bean.setIsLineManager(record.get_boolean(LandingHrModelPaths._Employee._JobPosition_IsLineManager));
        bean.setOrganisationBusId(record.getString(LandingHrModelPaths._Employee._JobPosition_OrganisationBusId));
        bean.setLineManagerBusId(record.getString(LandingHrModelPaths._Employee._JobPosition_LineManagerBusId));
        bean.setDottedLineManagerBusId(record.getString(LandingHrModelPaths._Employee._JobPosition_DottedLineManagerBusId));
        bean.setBuildingBusId(record.getString(LandingHrModelPaths._Employee._WorkplaceLocation_BuildingBusId));
        
        //Employee Private Address
        bean.setPrivateStreet(record.getString(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivateStreet));
        bean.setPrivatePostCode(record.getString(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivatePostCode));
        bean.setPrivateTown(record.getString(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivateTown));
        bean.setPrivateStateProvince(record.getString(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivateStateProvince));
        bean.setPrivateCountryCode(record.getString(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivateCountryCode));
        
        bean.setEmployerBusId(record.getString(LandingHrModelPaths._Employee._EmployerCompany_EmployerBusId));
        //Control
        bean.setSourceSystem(record.getString(LandingHrModelPaths._Employee._Control_SourceSystem));
        bean.setCreatedBy(record.getString(LandingHrModelPaths._Employee._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(LandingHrModelPaths._Employee._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(LandingHrModelPaths._Employee._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(LandingHrModelPaths._Employee._Control_LastUpdateTimestamp));
        
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{LandingHrModelPaths._Employee._Employee_EmployeeId};
    }

    @Override
    public Object[] retrievePks(LandingEmployeeBean bean) {
        return new Object[]{bean.getEmployeeId()};
    }

    

	
}
