package com.elipslife.mdm.hr.scheduler;

import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.WorkflowEngine;

public class HrMigrateFromLandingToCoreTask extends ScheduledTask{
	
	private String initialDataspace;
	private String dataset;
	private String landingDataspace;
	private String landingDataset;
	private String tempDataspace;
	private String minNoOfRecordsInBuilding;
	private String minNoOfRecordsInOrganisation;
	private String minNoOfRecordsInEmployee;
	private String workflowToLaunch;
	
	public String getInitialDataspace() {
		return initialDataspace;
	}

	public void setInitialDataspace(String initialDataspace) {
		this.initialDataspace = initialDataspace;
	}

	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getLandingDataspace() {
		return landingDataspace;
	}

	public void setLandingDataspace(String landingDataspace) {
		this.landingDataspace = landingDataspace;
	}

	public String getLandingDataset() {
		return landingDataset;
	}

	public void setLandingDataset(String landingDataset) {
		this.landingDataset = landingDataset;
	}

	public String getTempDataspace() {
		return tempDataspace;
	}

	public void setTempDataspace(String tempDataspace) {
		this.tempDataspace = tempDataspace;
	}
	
	public String getMinNoOfRecordsInBuilding() {
		return minNoOfRecordsInBuilding;
	}

	public void setMinNoOfRecordsInBuilding(String minNoOfRecordsInBuilding) {
		this.minNoOfRecordsInBuilding = minNoOfRecordsInBuilding;
	}

	public String getMinNoOfRecordsInOrganisation() {
		return minNoOfRecordsInOrganisation;
	}

	public void setMinNoOfRecordsInOrganisation(String minNoOfRecordsInOrganisation) {
		this.minNoOfRecordsInOrganisation = minNoOfRecordsInOrganisation;
	}

	public String getMinNoOfRecordsInEmployee() {
		return minNoOfRecordsInEmployee;
	}

	public void setMinNoOfRecordsInEmployee(String minNoOfRecordsInEmployee) {
		this.minNoOfRecordsInEmployee = minNoOfRecordsInEmployee;
	}
	
	public String getWorkflowToLaunch() {
		return workflowToLaunch;
	}

	public void setWorkflowToLaunch(String workflowToLaunch) {
		this.workflowToLaunch = workflowToLaunch;
	}

	@Override
	public void execute(ScheduledExecutionContext aContext) throws OperationException, ScheduledTaskInterruption {
		
		aContext.addExecutionInformation("****** HR Data workflow scheduled task started *****");
		WorkflowEngine workflowEngine = WorkflowEngine.getFromRepository(aContext.getRepository(), aContext.getSession());
		ProcessLauncher processLauncher = workflowEngine.getProcessLauncher(PublishedProcessKey.forName(workflowToLaunch));
		processLauncher.setInputParameter("initialDataspace", getInitialDataspace());
		processLauncher.setInputParameter("dataset", getDataset());
		processLauncher.setInputParameter("landingDataspace", getLandingDataspace());
		processLauncher.setInputParameter("landingDataset", getLandingDataset());
		processLauncher.setInputParameter("minNoOfRecordsInBuilding", getMinNoOfRecordsInBuilding());
		processLauncher.setInputParameter("minNoOfRecordsInOrganisation", getMinNoOfRecordsInOrganisation());
		processLauncher.setInputParameter("minNoOfRecordsInEmployee", getMinNoOfRecordsInEmployee());
		processLauncher.launchProcess();
		aContext.addExecutionInformation("****** HR Data workflow scheduled task ended *****");
		
	}

}
