package com.elipslife.mdm.contract.landing;

import java.util.Date;
import java.util.HashMap;

import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class LandingContractCoverWriter extends AbstractTableWriter<LandingContractCoverBean> {

	private LandingContractCoverReader landingContractCoverReader;
	
	public LandingContractCoverWriter() {
		this(PartyConstants.DataSpace.LANDING_REFERENCE);
	}
	
	public LandingContractCoverWriter(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_ContractCover.getPathInSchema()));
	}
	
	public LandingContractCoverWriter(AdaptationTable table) {
		super(table);
		
		this.landingContractCoverReader = new LandingContractCoverReader(table);
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, LandingContractCoverBean bean) {
		
		values.put(LandingPaths._LDG_ContractCover._Cover_ContractCoverId, bean.getId());
		values.put(LandingPaths._LDG_ContractCover._Exposure_ExposureBusId, bean.getExposureBusId());
		values.put(LandingPaths._LDG_ContractCover._Cover_CoverBusId, bean.getCoverBusId());
		values.put(LandingPaths._LDG_ContractCover._Cover_CoverGroupType, bean.getCoverGroupType());
		values.put(LandingPaths._LDG_ContractCover._Cover_CoverGroupTypeName, bean.getCoverGroupTypeName());
		values.put(LandingPaths._LDG_ContractCover._Cover_CoverName, bean.getCoverName());
		values.put(LandingPaths._LDG_ContractCover._Cover_CoverTemplateBusId, bean.getCoverTemplateBusId());
		values.put(LandingPaths._LDG_ContractCover._Cover_BeneficiaryType, bean.getBeneficiaryType());
		values.put(LandingPaths._LDG_ContractCover._Exposure_ExposureId, bean.getExposureId());
		
		values.put(LandingPaths._LDG_ContractCover._Control_SourceSystem, bean.getSourceSystem());
		values.put(LandingPaths._LDG_ContractCover._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(LandingPaths._LDG_ContractCover._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(LandingPaths._LDG_ContractCover._Control_LastSyncTimestamp, new Date());
		values.put(LandingPaths._LDG_ContractCover._Control_Action, bean.getAction());
		values.put(LandingPaths._LDG_ContractCover._Control_ActionBy, bean.getActionBy());
	}

	@Override
	public AbstractTableReader<LandingContractCoverBean> getTableReader() {
		return this.landingContractCoverReader;
	}
}