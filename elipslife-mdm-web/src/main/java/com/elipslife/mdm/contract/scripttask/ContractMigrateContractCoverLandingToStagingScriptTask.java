package com.elipslife.mdm.contract.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.butos.ebx.procedure.CreateRecords;
import com.butos.ebx.procedure.DeleteRecords;
import com.butos.ebx.procedure.RecordValuesBean;
import com.elipslife.ebx.domain.bean.convert.ContractConverter;
import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.contract.landing.LandingContractCoverBean;
import com.elipslife.mdm.contract.landing.LandingContractCoverReader;
import com.elipslife.mdm.contract.landing.LandingContractCoverWriter;
import com.elipslife.mdm.contract.landing.LandingContractCoverWriterSession;
import com.elipslife.mdm.contract.landing.LandingExposureBean;
import com.elipslife.mdm.contract.landing.LandingExposureReader;
import com.elipslife.mdm.contract.landing.LandingExposureWriter;
import com.elipslife.mdm.contract.landing.LandingExposureWriterSession;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class ContractMigrateContractCoverLandingToStagingScriptTask extends ScriptTask {

	private static int COMMITSIZE = 1;
    private static int recordCount = 0;
    
	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException {
	    
	    Map<String, String> dataContextVariables = ContractConverter.getContextVariables(context);
        String commitSize = dataContextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);
        String dataSpace = dataContextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
        String landingDataspace = dataContextVariables.get(PartyConstants.DataContextVariables.TEMP_LANDING_DATASPACE);
		if (commitSize != null) {
			COMMITSIZE = Integer.parseInt(commitSize);
		}
        String errorMessage = dataContextVariables.get("errorMessage");
        
        AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(dataSpace);
        LandingContractCoverWriter landingContractCoverWriter = new LandingContractCoverWriterSession(context.getSession());
		LandingContractCoverReader landingContractCoverReader = (LandingContractCoverReader) landingContractCoverWriter.getTableReader();
		List<LandingContractCoverBean> landingContractCoverBean = landingContractCoverReader.readAllBeans();
		StringBuilder stringBuilderErrorMessage = new StringBuilder(errorMessage == null ? "" : errorMessage);
		List<RecordValuesBean> stagingContractCoverListToCreate = new ArrayList<RecordValuesBean>();
        List<LandingContractCoverBean> landingContractCoverToDelete = new ArrayList<LandingContractCoverBean>();
        
		landingContractCoverBean.forEach(lccb -> {
			try {
				boolean hasValidationErrors = landingContractCoverReader.hasValidationErrors(new Object[] { lccb.getId() });
				boolean isParentExistsInLanding = findLandingExposureByExposureId(context.getSession(), lccb);
				if(hasValidationErrors || !isParentExistsInLanding) {
					return ;
				}
				
				recordCount++;
                HashMap<Path, Object> stagingContractCoverValues = ContractConverter.fillStagingContractCoverWithLandingContractCoverBean(lccb);
                RecordValuesBean recordValuesBean = new RecordValuesBean(stagingContractCoverValues);
                stagingContractCoverListToCreate.add(recordValuesBean);
                landingContractCoverToDelete.add(lccb);
                if (recordCount > COMMITSIZE) {
                    createStagingContractCovers(dataspaceName, context.getSession(), stagingContractCoverListToCreate);
                    stagingContractCoverListToCreate.clear();
                    recordCount = 0;
                }
			}
			catch(Exception ex) {
				stringBuilderErrorMessage.append(String.format("Migration error for landing contract cover with ID %s:\r\n%s", lccb.getId(), ex.toString()));
			}
		});
		
		if(!stagingContractCoverListToCreate.isEmpty()) {  // create staging records out of batch size
		    createStagingContractCovers(dataspaceName, context.getSession(), stagingContractCoverListToCreate);
            stagingContractCoverListToCreate.clear();
            recordCount = 0;
        }
		
		// Delete Landing  Contract Cover Table records
		deleteLandingContractCoverRecords(landingDataspace, context.getSession(), landingContractCoverToDelete);
		
		context.setVariableString("errorMessage", stringBuilderErrorMessage.toString());
	}
	
	private static boolean findLandingExposureByExposureId(Session session, LandingContractCoverBean landingContractCoverBean) {
		
		LandingExposureWriter lanExposureWriter = new LandingExposureWriterSession(session);
		LandingExposureReader landingExposureReader = (LandingExposureReader) lanExposureWriter.getTableReader();
		List<LandingExposureBean> landingExposureBeans = landingExposureReader
				.findBeansByExposureId(landingContractCoverBean.getExposureId());
		if(landingExposureBeans.size() > 0) {
			return true;
		}
		
		return false;
	}
	
	/**
     * Create staging contract covers
     * 
     * @param dataspace
     * @param session
     * @param stagingInsuranceContractsList
     * @throws OperationException
     */
    
    private static void createStagingContractCovers(AdaptationHome dataspace, Session session, List<RecordValuesBean> stagingContractCoversList) throws OperationException {
        
        Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
        AdaptationTable aTable = dataset.getTable(ContractPaths._STG_ContractCover.getPathInSchema());

        final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
        
        CreateRecords createRecordsProcedure = new CreateRecords(aTable, stagingContractCoversList);
        ProcedureResult result = service.execute(createRecordsProcedure);
        if (result.hasFailed()) {
            final OperationException exception = result.getException();
            throw exception;
        }
    }
    
    /**
	 * Delete Landing Exposure tables records based on commit size
	 * 
	 * @param dataspace
	 * @param session
	 * @param landingInsuranceContractBeans
	 * @throws OperationException
	 */
	private static void deleteLandingContractCoverRecords(String dataspace, Session session,
			List<LandingContractCoverBean> landingContractCoverBeans) throws OperationException {

		int deleteRecordCount = 0;
		AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(dataspace);
		Adaptation dataset = dataspaceName.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.LANDING_MASTER));
		AdaptationTable aTable = dataset.getTable(LandingPaths._LDG_ContractCover.getPathInSchema());
		ArrayList<Adaptation> recordValuesBeanList = new ArrayList<Adaptation>();
		for (LandingContractCoverBean landingContractCoverBean : landingContractCoverBeans) {
			deleteRecordCount++;
			recordValuesBeanList.add(aTable.lookupAdaptationByPrimaryKey(
					PrimaryKey.parseString(landingContractCoverBean.getId().toString())));
			if (deleteRecordCount > 1000) {
				deleteRecords(dataspaceName, session, recordValuesBeanList);
				recordValuesBeanList.clear();
				deleteRecordCount = 0;
			}
		}

		if (!recordValuesBeanList.isEmpty()) {
			deleteRecords(dataspaceName, session, recordValuesBeanList);
			recordValuesBeanList.clear();
			deleteRecordCount = 0;
		}
	}

	/**
	 * Delete table records
	 * 
	 * @param dataspace
	 * @param session
	 * @param recordValuesBeanList
	 * @throws OperationException
	 */
	private static void deleteRecords(AdaptationHome dataspace, Session session,
			ArrayList<Adaptation> recordValuesBeanList) throws OperationException {

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
		DeleteRecords deleteRecordsProcedure = new DeleteRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(deleteRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}
}