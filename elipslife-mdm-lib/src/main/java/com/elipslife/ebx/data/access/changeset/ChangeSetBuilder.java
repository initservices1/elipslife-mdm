package com.elipslife.ebx.data.access.changeset;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.LandingAddressBean;
import com.elipslife.ebx.domain.bean.LandingAddressBulkProcessBean;
import com.elipslife.ebx.domain.bean.LandingBankAccountBean;
import com.elipslife.ebx.domain.bean.LandingBankAccountBulkProcessBean;
import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.ebx.domain.bean.StagingBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;

public class ChangeSetBuilder {

    public static java.util.HashMap<Character, List<StagingBankAccountBean>> buildStagingBankAccountBeansChangeSet(
        List<LandingBankAccountBean> landingBankAccountBeans,
        List<StagingBankAccountBean> stagingBankAccountBeans) {

        java.util.HashMap<Character, List<StagingBankAccountBean>> stagingBankAccountBeanWithOperation =
            new java.util.HashMap<Character, List<StagingBankAccountBean>>();

        List<StagingBankAccountBean> stagingBankAccountBeansToCreate = new ArrayList<StagingBankAccountBean>();
        List<StagingBankAccountBean> stagingBankAccountBeansToUpdate = new ArrayList<StagingBankAccountBean>();
        List<StagingBankAccountBean> stagingBankAccountBeansToDelete = new ArrayList<StagingBankAccountBean>();

        List<StagingBankAccountBean> stagingBankAccountBeansWithSourceBankAccountBusId = stagingBankAccountBeans
            .stream()
            .filter(sab -> sab.getSourceBankAccountBusId() != null && !sab.getSourceBankAccountBusId().isEmpty())
            .collect(Collectors.toList());

        // For initial load
        if (stagingBankAccountBeans.size() == 0) {
            stagingBankAccountBeansToCreate = landingBankAccountBeans.stream().map(
                com.elipslife.ebx.domain.bean.convert.BankAccountConverter::createStagingBankAccountBeanFromLandingBankAccountBean)
                .collect(Collectors.toList());
        } else {

            for (LandingBankAccountBean landingBankAccountBean : landingBankAccountBeans) {

                if (landingBankAccountBean.getSourceBankAccountBusId() != null
                    && !landingBankAccountBean.getSourceBankAccountBusId().isEmpty()) {

                    if (stagingBankAccountBeansWithSourceBankAccountBusId.size() > 0) {

                        boolean isUpdateAvilable = false;
                        for (StagingBankAccountBean stagingBankAccountBean : stagingBankAccountBeansWithSourceBankAccountBusId) {

                            boolean isValid =
                                Objects.equals(stagingBankAccountBean.getSourceBankAccountBusId(), landingBankAccountBean.getSourceBankAccountBusId())
                                    && landingBankAccountBean.getUpdateTimestamp()
                                        .compareTo(stagingBankAccountBean.getUpdateTimestamp()) >= 0
                                    && Objects.equals(landingBankAccountBean.getValidFrom(),
                                        stagingBankAccountBean.getValidFrom())
                                    && Objects.equals(landingBankAccountBean.getValidTo(), stagingBankAccountBean.getValidTo());

                            if (isValid) {
                                if (!stagingBankAccountBeansToUpdate.contains(stagingBankAccountBean)) {
                                    com.elipslife.ebx.domain.bean.convert.BankAccountConverter.fillStagingBankAccountBeanWithLandingBankAccountBean(
                                        landingBankAccountBean,
                                        stagingBankAccountBean);
                                    stagingBankAccountBeansToUpdate.add(stagingBankAccountBean);
                                    isUpdateAvilable = true;
                                    break;
                                }
                            }
                        }
                        // if update is not avilable
                        if (!isUpdateAvilable) {
                            StagingBankAccountBean staBankAccountBean = com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                                .createStagingBankAccountBeanFromLandingBankAccountBean(landingBankAccountBean);
                            stagingBankAccountBeansToCreate.add(staBankAccountBean);
                        }
                    } else {
                        StagingBankAccountBean staBnkAccountBean = com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                            .createStagingBankAccountBeanFromLandingBankAccountBean(landingBankAccountBean);
                        stagingBankAccountBeansToCreate.add(staBnkAccountBean);
                    }

                } else {

                    boolean isNotPresentInStaging = false;
                    // if not found
                    for (StagingBankAccountBean stagingBankAccountBean : stagingBankAccountBeans) {

                        boolean isValid = checkIbanOrAccountNumberIfSourceBankAccountIsNull(landingBankAccountBean, stagingBankAccountBean)
                            && landingBankAccountBean.getUpdateTimestamp().compareTo(stagingBankAccountBean.getUpdateTimestamp()) >= 0
                            && Objects.equals(landingBankAccountBean.getValidFrom(), stagingBankAccountBean.getValidFrom())
                            && Objects.equals(landingBankAccountBean.getValidTo(), stagingBankAccountBean.getValidTo());

                        if (isValid) {
                            if (!stagingBankAccountBeansToUpdate.contains(stagingBankAccountBean)) {
                                com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                                    .fillStagingBankAccountBeanWithLandingBankAccountBean(landingBankAccountBean,
                                        stagingBankAccountBean);
                                stagingBankAccountBeansToUpdate.add(stagingBankAccountBean);
                                isNotPresentInStaging = true;
                                break;
                            }
                        }
                    }
                    // if update is not avilable
                    if (!isNotPresentInStaging) {
                        StagingBankAccountBean staBnkAccountBean = com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                            .createStagingBankAccountBeanFromLandingBankAccountBean(landingBankAccountBean);
                        stagingBankAccountBeansToCreate.add(staBnkAccountBean);
                    }
                }
            }

        }

        stagingBankAccountBeanWithOperation.put(PartyConstants.RecordOperations.CREATE.charAt(0),
            stagingBankAccountBeansToCreate);
        stagingBankAccountBeanWithOperation.put(PartyConstants.RecordOperations.UPDATE.charAt(0),
            stagingBankAccountBeansToUpdate);

        stagingBankAccountBeans.removeAll(stagingBankAccountBeansToUpdate);
        stagingBankAccountBeansToDelete.addAll(stagingBankAccountBeans);

        stagingBankAccountBeanWithOperation.put(PartyConstants.RecordOperations.DELETE.charAt(0),
            stagingBankAccountBeansToDelete);

        return stagingBankAccountBeanWithOperation;
    }

    /**
     * 
     * buildStagingBankAccountBeansChangeSetInBulkProcess
     * 
     * @param landingBankAccountBeans
     * @param stagingBankAccountBeans
     * @return
     */
    public static java.util.HashMap<Character, List<StagingBankAccountBean>> buildStagingBankAccountBeansChangeSetInBulkProcess(
        List<LandingBankAccountBulkProcessBean> landingBankAccountBulkProcessBeans,
        List<StagingBankAccountBean> stagingBankAccountBeans) {

        java.util.HashMap<Character, List<StagingBankAccountBean>> stagingBankAccountBeanWithOperation =
            new java.util.HashMap<Character, List<StagingBankAccountBean>>();

        List<StagingBankAccountBean> stagingBankAccountBeansToCreate = new ArrayList<StagingBankAccountBean>();
        List<StagingBankAccountBean> stagingBankAccountBeansToUpdate = new ArrayList<StagingBankAccountBean>();
        List<StagingBankAccountBean> stagingBankAccountBeansToDelete = new ArrayList<StagingBankAccountBean>();

        List<StagingBankAccountBean> stagingBankAccountBeansWithSourceBankAccountBusId = stagingBankAccountBeans
            .stream()
            .filter(sab -> sab.getSourceBankAccountBusId() != null && !sab.getSourceBankAccountBusId().isEmpty())
            .collect(Collectors.toList());

        // For initial load
        if (stagingBankAccountBeans.size() == 0) {
            stagingBankAccountBeansToCreate = landingBankAccountBulkProcessBeans.stream().map(
                com.elipslife.ebx.domain.bean.convert.BankAccountConverter::createStagingBankAccountBeanFromLandingBankAccountBulkProcessBean)
                .collect(Collectors.toList());
        } else {

            for (LandingBankAccountBulkProcessBean landingBankAccountBean : landingBankAccountBulkProcessBeans) {

                if (landingBankAccountBean.getSourceBankAccountBusId() != null
                    && !landingBankAccountBean.getSourceBankAccountBusId().isEmpty()) {

                    if (stagingBankAccountBeansWithSourceBankAccountBusId.size() > 0) {

                        boolean isUpdateAvilable = false;
                        for (StagingBankAccountBean stagingBankAccountBean : stagingBankAccountBeansWithSourceBankAccountBusId) {

                            boolean isValid =
                                Objects.equals(stagingBankAccountBean.getSourceBankAccountBusId(), landingBankAccountBean.getSourceBankAccountBusId())
                                    && landingBankAccountBean.getUpdateTimestamp()
                                        .compareTo(stagingBankAccountBean.getUpdateTimestamp()) >= 0
                                    && Objects.equals(landingBankAccountBean.getValidFrom(),
                                        stagingBankAccountBean.getValidFrom())
                                    && Objects.equals(landingBankAccountBean.getValidTo(), stagingBankAccountBean.getValidTo());

                            if (isValid) {
                                if (!stagingBankAccountBeansToUpdate.contains(stagingBankAccountBean)) {
                                    com.elipslife.ebx.domain.bean.convert.BankAccountConverter.fillStagingBankAccountBeanWithLandingBankAccountBean(
                                        landingBankAccountBean,
                                        stagingBankAccountBean);
                                    stagingBankAccountBeansToUpdate.add(stagingBankAccountBean);
                                    isUpdateAvilable = true;
                                    break;
                                }
                            }
                        }
                        // if update is not avilable
                        if (!isUpdateAvilable) {
                            StagingBankAccountBean staBankAccountBean = com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                                .createStagingBankAccountBeanFromLandingBankAccountBulkProcessBean(landingBankAccountBean);
                            stagingBankAccountBeansToCreate.add(staBankAccountBean);
                        }
                    } else {
                        StagingBankAccountBean staBnkAccountBean = com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                            .createStagingBankAccountBeanFromLandingBankAccountBulkProcessBean(landingBankAccountBean);
                        stagingBankAccountBeansToCreate.add(staBnkAccountBean);
                    }

                } else {

                    boolean isNotPresentInStaging = false;
                    // if not found
                    for (StagingBankAccountBean stagingBankAccountBean : stagingBankAccountBeans) {

                        boolean isValid = checkIbanOrAccountNumberIfSourceBankAccountIsNull(landingBankAccountBean, stagingBankAccountBean)
                            && landingBankAccountBean.getUpdateTimestamp().compareTo(stagingBankAccountBean.getUpdateTimestamp()) >= 0
                            && Objects.equals(landingBankAccountBean.getValidFrom(), stagingBankAccountBean.getValidFrom())
                            && Objects.equals(landingBankAccountBean.getValidTo(), stagingBankAccountBean.getValidTo());

                        if (isValid) {
                            if (!stagingBankAccountBeansToUpdate.contains(stagingBankAccountBean)) {
                                com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                                    .fillStagingBankAccountBeanWithLandingBankAccountBean(landingBankAccountBean,
                                        stagingBankAccountBean);
                                stagingBankAccountBeansToUpdate.add(stagingBankAccountBean);
                                isNotPresentInStaging = true;
                                break;
                            }
                        }
                    }
                    // if update is not avilable
                    if (!isNotPresentInStaging) {
                        StagingBankAccountBean staBnkAccountBean = com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                            .createStagingBankAccountBeanFromLandingBankAccountBulkProcessBean(landingBankAccountBean);
                        stagingBankAccountBeansToCreate.add(staBnkAccountBean);
                    }
                }
            }

        }

        stagingBankAccountBeanWithOperation.put(PartyConstants.RecordOperations.CREATE.charAt(0),
            stagingBankAccountBeansToCreate);
        stagingBankAccountBeanWithOperation.put(PartyConstants.RecordOperations.UPDATE.charAt(0),
            stagingBankAccountBeansToUpdate);

        stagingBankAccountBeans.removeAll(stagingBankAccountBeansToUpdate);
        stagingBankAccountBeansToDelete.addAll(stagingBankAccountBeans);

        stagingBankAccountBeanWithOperation.put(PartyConstants.RecordOperations.DELETE.charAt(0),
            stagingBankAccountBeansToDelete);

        return stagingBankAccountBeanWithOperation;
    }


    public static java.util.HashMap<Character, List<StagingAddressBean>> buildStagingAddressBeansChangeSet(
        List<LandingAddressBean> landingAddressBeans, List<StagingAddressBean> stagingAddressBeans) {

        java.util.HashMap<Character, List<StagingAddressBean>> stagingAddressBeanWithOperation =
            new java.util.HashMap<Character, List<StagingAddressBean>>();

        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusIdToUpdate = new ArrayList<StagingAddressBean>();
        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusIdToCreate = new ArrayList<StagingAddressBean>();
        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusIdToDelete = new ArrayList<StagingAddressBean>();

        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusId = stagingAddressBeans.stream()
            .filter(sab -> sab.getSourceAddressBusId() != null && !sab.getSourceAddressBusId().isEmpty())
            .collect(Collectors.toList());

        // First time load
        // Staging does not have any records. So load all of them
        if (stagingAddressBeans.size() == 0) {

            stagingAddressBeansWithSourceAddressBusIdToCreate = landingAddressBeans.stream().map(
                com.elipslife.ebx.domain.bean.convert.AddressConverter::createStagingAddressBeanFromLandingAddressBean)
                .collect(Collectors.toList());
        } else {

            for (LandingAddressBean landingAddressBean : landingAddressBeans) {

                if (landingAddressBean.getSourceAddressBusId() != null
                    && !landingAddressBean.getSourceAddressBusId().isEmpty()) {

                    if (stagingAddressBeansWithSourceAddressBusId.size() > 0) {
                        boolean isUpdateAvilable = false;
                        for (StagingAddressBean stagingAddressBean : stagingAddressBeansWithSourceAddressBusId) {

                            boolean isValid = Objects.equals(stagingAddressBean.getSourceAddressBusId(),
                                landingAddressBean.getSourceAddressBusId())
                                && landingAddressBean.getUpdateTimestamp()
                                    .compareTo(stagingAddressBean.getUpdateTimestamp()) >= 0
                                && Objects.equals(landingAddressBean.getValidFrom(),
                                    stagingAddressBean.getValidFrom())
                                && Objects.equals(landingAddressBean.getValidTo(), stagingAddressBean.getValidTo());
                            if (isValid) {
                                if (!stagingAddressBeansWithSourceAddressBusIdToUpdate.contains(stagingAddressBean)) {
                                    com.elipslife.ebx.domain.bean.convert.AddressConverter.fillStagingAddressBeanWithLandingAddressBean(
                                        landingAddressBean,
                                        stagingAddressBean);
                                    stagingAddressBeansWithSourceAddressBusIdToUpdate.add(stagingAddressBean);
                                    isUpdateAvilable = true;
                                    break;
                                }
                            }
                        }
                        // if update is not avilable
                        if (!isUpdateAvilable) {
                            StagingAddressBean staAddressBean = com.elipslife.ebx.domain.bean.convert.AddressConverter
                                .createStagingAddressBeanFromLandingAddressBean(landingAddressBean);
                            stagingAddressBeansWithSourceAddressBusIdToCreate.add(staAddressBean);
                        }
                    } else {
                        StagingAddressBean staAddressBean = com.elipslife.ebx.domain.bean.convert.AddressConverter
                            .createStagingAddressBeanFromLandingAddressBean(landingAddressBean);
                        stagingAddressBeansWithSourceAddressBusIdToCreate.add(staAddressBean);
                    }

                } else {

                    boolean isNotPresentInStaging = false;

                    // if not found
                    for (StagingAddressBean stagingAddressBean : stagingAddressBeans) {

                        boolean isValid = Objects.equals(stagingAddressBean.getAddressType(),
                            landingAddressBean.getAddressType())
                            && Objects.equals(stagingAddressBean.getElectronicAddressType(),
                                landingAddressBean.getElectronicAddressType())
                            && landingAddressBean.getUpdateTimestamp()
                                .compareTo(stagingAddressBean.getUpdateTimestamp()) >= 0
                            && Objects.equals(landingAddressBean.getValidFrom(), stagingAddressBean.getValidFrom())
                            && Objects.equals(landingAddressBean.getValidTo(), stagingAddressBean.getValidTo());
                        if (isValid) {
                            if (!stagingAddressBeansWithSourceAddressBusIdToUpdate.contains(stagingAddressBean)) {
                                com.elipslife.ebx.domain.bean.convert.AddressConverter.fillStagingAddressBeanWithLandingAddressBean(
                                    landingAddressBean,
                                    stagingAddressBean);
                                stagingAddressBeansWithSourceAddressBusIdToUpdate.add(stagingAddressBean);
                                stagingAddressBeans.remove(stagingAddressBean);
                                isNotPresentInStaging = true;
                                break;
                            }
                        }
                    }

                    if (!isNotPresentInStaging) {
                        StagingAddressBean staAddressBean = com.elipslife.ebx.domain.bean.convert.AddressConverter
                            .createStagingAddressBeanFromLandingAddressBean(landingAddressBean);
                        stagingAddressBeansWithSourceAddressBusIdToCreate.add(staAddressBean);
                    }

                }
            }
        }

        stagingAddressBeanWithOperation.put(PartyConstants.RecordOperations.UPDATE.charAt(0),
            stagingAddressBeansWithSourceAddressBusIdToUpdate);

        stagingAddressBeans.removeAll(stagingAddressBeansWithSourceAddressBusIdToUpdate);
        stagingAddressBeansWithSourceAddressBusIdToDelete.addAll(stagingAddressBeans);

        stagingAddressBeanWithOperation.put(PartyConstants.RecordOperations.DELETE.charAt(0),
            stagingAddressBeansWithSourceAddressBusIdToDelete);

        stagingAddressBeanWithOperation.put(PartyConstants.RecordOperations.CREATE.charAt(0),
            stagingAddressBeansWithSourceAddressBusIdToCreate);

        return stagingAddressBeanWithOperation;
    }

    /**
     * 
     * 
     * 
     * @param landingAddressBeans
     * @param stagingAddressBeans
     * @return
     */
    public static java.util.HashMap<Character, List<StagingAddressBean>> buildStagingAddressBeansChangeSetInBulkProcess(
        List<LandingAddressBulkProcessBean> landingAddressBeans, List<StagingAddressBean> stagingAddressBeans) {

        java.util.HashMap<Character, List<StagingAddressBean>> stagingAddressBeanWithOperation =
            new java.util.HashMap<Character, List<StagingAddressBean>>();

        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusIdToUpdate = new ArrayList<StagingAddressBean>();
        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusIdToCreate = new ArrayList<StagingAddressBean>();
        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusIdToDelete = new ArrayList<StagingAddressBean>();

        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusId = stagingAddressBeans.stream()
            .filter(sab -> sab.getSourceAddressBusId() != null && !sab.getSourceAddressBusId().isEmpty())
            .collect(Collectors.toList());

        // First time load
        // Staging does not have any records. So load all of them
        if (stagingAddressBeans.size() == 0) {

            stagingAddressBeansWithSourceAddressBusIdToCreate = landingAddressBeans.stream().map(
                com.elipslife.ebx.domain.bean.convert.AddressConverter::createStagingAddressBeanFromLandingAddressBean)
                .collect(Collectors.toList());
        } else {

            for (LandingAddressBulkProcessBean landingAddressBean : landingAddressBeans) {

                if (landingAddressBean.getSourceAddressBusId() != null
                    && !landingAddressBean.getSourceAddressBusId().isEmpty()) {

                    if (stagingAddressBeansWithSourceAddressBusId.size() > 0) {
                        boolean isUpdateAvilable = false;
                        for (StagingAddressBean stagingAddressBean : stagingAddressBeansWithSourceAddressBusId) {

                            boolean isValid = Objects.equals(stagingAddressBean.getSourceAddressBusId(),
                                landingAddressBean.getSourceAddressBusId())
                                && landingAddressBean.getUpdateTimestamp()
                                    .compareTo(stagingAddressBean.getUpdateTimestamp()) >= 0
                                && Objects.equals(landingAddressBean.getValidFrom(),
                                    stagingAddressBean.getValidFrom())
                                && Objects.equals(landingAddressBean.getValidTo(), stagingAddressBean.getValidTo());
                            if (isValid) {
                                if (!stagingAddressBeansWithSourceAddressBusIdToUpdate.contains(stagingAddressBean)) {
                                    com.elipslife.ebx.domain.bean.convert.AddressConverter.fillStagingAddressBeanWithLandingAddressBulkProcessBean(
                                        landingAddressBean,
                                        stagingAddressBean);
                                    stagingAddressBeansWithSourceAddressBusIdToUpdate.add(stagingAddressBean);
                                    stagingAddressBeans.remove(stagingAddressBean);
                                    isUpdateAvilable = true;
                                    break;
                                }
                            }
                        }
                        // if update is not avilable
                        if (!isUpdateAvilable) {
                            StagingAddressBean staAddressBean = com.elipslife.ebx.domain.bean.convert.AddressConverter
                                .createStagingAddressBeanFromLandingAddressBean(landingAddressBean);
                            stagingAddressBeansWithSourceAddressBusIdToCreate.add(staAddressBean);
                        }
                    } else {
                        StagingAddressBean staAddressBean = com.elipslife.ebx.domain.bean.convert.AddressConverter
                            .createStagingAddressBeanFromLandingAddressBean(landingAddressBean);
                        stagingAddressBeansWithSourceAddressBusIdToCreate.add(staAddressBean);
                    }

                } else {

                    boolean isNotPresentInStaging = false;
                    // if not found
                    for (StagingAddressBean stagingAddressBean : stagingAddressBeans) {

                        boolean isValid = Objects.equals(stagingAddressBean.getAddressType(),
                            landingAddressBean.getAddressType())
                            && Objects.equals(stagingAddressBean.getElectronicAddressType(),
                                landingAddressBean.getElectronicAddressType())
                            && landingAddressBean.getUpdateTimestamp()
                                .compareTo(stagingAddressBean.getUpdateTimestamp()) >= 0
                            && Objects.equals(landingAddressBean.getValidFrom(), stagingAddressBean.getValidFrom())
                            && Objects.equals(landingAddressBean.getValidTo(), stagingAddressBean.getValidTo());
                        if (isValid) {
                            if (!stagingAddressBeansWithSourceAddressBusIdToUpdate.contains(stagingAddressBean)) {
                                com.elipslife.ebx.domain.bean.convert.AddressConverter.fillStagingAddressBeanWithLandingAddressBulkProcessBean(
                                    landingAddressBean,
                                    stagingAddressBean);
                                stagingAddressBeansWithSourceAddressBusIdToUpdate.add(stagingAddressBean);
                                stagingAddressBeans.remove(stagingAddressBean);
                                isNotPresentInStaging = true;
                                break;
                            }
                        }
                    }

                    if (!isNotPresentInStaging) {
                        StagingAddressBean staAddressBean = com.elipslife.ebx.domain.bean.convert.AddressConverter
                            .createStagingAddressBeanFromLandingAddressBean(landingAddressBean);
                        stagingAddressBeansWithSourceAddressBusIdToCreate.add(staAddressBean);
                    }

                }
            }
        }

        stagingAddressBeanWithOperation.put(PartyConstants.RecordOperations.UPDATE.charAt(0),
            stagingAddressBeansWithSourceAddressBusIdToUpdate);

        stagingAddressBeans.removeAll(stagingAddressBeansWithSourceAddressBusIdToUpdate);
        stagingAddressBeansWithSourceAddressBusIdToDelete.addAll(stagingAddressBeans);

        stagingAddressBeanWithOperation.put(PartyConstants.RecordOperations.DELETE.charAt(0),
            stagingAddressBeansWithSourceAddressBusIdToDelete);

        stagingAddressBeanWithOperation.put(PartyConstants.RecordOperations.CREATE.charAt(0),
            stagingAddressBeansWithSourceAddressBusIdToCreate);

        return stagingAddressBeanWithOperation;
    }

    private static Boolean checkIbanOrAccountNumberIfSourceBankAccountIsNull(LandingBankAccountBean landingBankAccountBean,
        StagingBankAccountBean stagingBankAccountBean) {
        String accountNumber = stagingBankAccountBean.getAccountNumber();
        if (accountNumber != null) {
            return Objects.equals(stagingBankAccountBean.getAccountNumber(), landingBankAccountBean.getAccountNumber());
        }

        String iban = stagingBankAccountBean.getIban();
        if (iban != null) {
            return Objects.equals(stagingBankAccountBean.getIban(), landingBankAccountBean.getIban());
        }

        return false;
    }

    private static Boolean checkIbanOrAccountNumberIfSourceBankAccountIsNull(LandingBankAccountBulkProcessBean landingBankAccountBean,
        StagingBankAccountBean stagingBankAccountBean) {
        String accountNumber = stagingBankAccountBean.getAccountNumber();
        if (accountNumber != null) {
            return Objects.equals(stagingBankAccountBean.getAccountNumber(), landingBankAccountBean.getAccountNumber());
        }

        String iban = stagingBankAccountBean.getIban();
        if (iban != null) {
            return Objects.equals(stagingBankAccountBean.getIban(), landingBankAccountBean.getIban());
        }

        return false;
    }

    /**
     * Collects staging address beans with operation
     * 
     * @param landingAddressBeans
     * @param stagingAddressBeans
     * @return HashMap with operations: U (Update), C (Create), D (Delete) as key and a list of StagingAddressBeans as value
     */
    public static java.util.HashMap<Character, List<StagingAddressBean>> buildStagingAddressBeanChangeSet(
        List<LandingAddressBean> landingAddressBeans, List<StagingAddressBean> stagingAddressBeans) {

        java.util.HashMap<Character, List<StagingAddressBean>> stagingAddressBeanWithOperation =
            new java.util.HashMap<Character, List<StagingAddressBean>>();

        //Handle staging address beans which have a SourceAddressBusId		
        List<LandingAddressBean> landingAddressBeansWithSourceAddressBusId = landingAddressBeans.stream()
            .filter(lab -> lab.getSourceAddressBusId() != null && !lab.getSourceAddressBusId().isEmpty()).collect(Collectors.toList());
        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusId = stagingAddressBeans.stream()
            .filter(sab -> sab.getSourceAddressBusId() != null && !sab.getSourceAddressBusId().isEmpty()).collect(Collectors.toList());

        //Create: Available in landing but not in staging
        //		  If ValidTo and ValidFrom are not equal
        List<LandingAddressBean> landingAddressBeansWithSourceAddressBusIdNotInStaging = landingAddressBeansWithSourceAddressBusId.stream().filter(
            lab -> stagingAddressBeansWithSourceAddressBusId.stream().filter(
                sab -> Objects.equals(lab.getSourceAddressBusId(), sab.getSourceAddressBusId()) &&
                    (Objects.equals(lab.getValidFrom(), sab.getValidFrom()) && Objects.equals(lab.getValidTo(), sab.getValidTo())))
                .count() == 0)
            .collect(Collectors.toList());

        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusIdToCreate = landingAddressBeansWithSourceAddressBusIdNotInStaging.stream()
            .map(com.elipslife.ebx.domain.bean.convert.AddressConverter::createStagingAddressBeanFromLandingAddressBean).collect(Collectors.toList());
        stagingAddressBeanWithOperation.put(PartyConstants.RecordOperations.CREATE.charAt(0), stagingAddressBeansWithSourceAddressBusIdToCreate);

        //Update: Available in landing and staging
        //		  If LastUpdateTimestamp of landing is newer than staging only
        //		  If ValidTo and ValidFrom are equal
        List<StagingAddressBean> stagingAddressBeansWithSourceAddressBusIdToUpdate = stagingAddressBeansWithSourceAddressBusId.stream().filter(
            sab -> landingAddressBeansWithSourceAddressBusId.stream().filter(
                lab -> Objects.equals(sab.getSourceAddressBusId(), lab.getSourceAddressBusId()) &&
                    lab.getUpdateTimestamp().compareTo(sab.getUpdateTimestamp()) >= 0
                    && (Objects.equals(lab.getValidFrom(), sab.getValidFrom()) && Objects.equals(lab.getValidTo(), sab.getValidTo())))
                .count() == 1)
            .collect(Collectors.toList());

        stagingAddressBeansWithSourceAddressBusIdToUpdate.stream().forEach(sab -> {
            Optional<LandingAddressBean> landingAddressBean = landingAddressBeansWithSourceAddressBusId.stream()
                .filter(lab -> Objects.equals(sab.getSourceAddressBusId(), lab.getSourceAddressBusId())).findFirst();
            com.elipslife.ebx.domain.bean.convert.AddressConverter.fillStagingAddressBeanWithLandingAddressBean(landingAddressBean.get(), sab);
        });

        stagingAddressBeanWithOperation.put(PartyConstants.RecordOperations.UPDATE.charAt(0), stagingAddressBeansWithSourceAddressBusIdToUpdate);

        //Delete: Not available in landing but in staging
        List<StagingAddressBean> stagingAddressBeansNotInLanding =
            stagingAddressBeansWithSourceAddressBusId.stream()
                .filter(sab -> landingAddressBeansWithSourceAddressBusId.stream()
                    .filter(lab -> Objects.equals(sab.getSourceAddressBusId(), lab.getSourceAddressBusId())).count() == 0)
                .collect(Collectors.toList());
        stagingAddressBeanWithOperation.put(PartyConstants.RecordOperations.DELETE.charAt(0), stagingAddressBeansNotInLanding);


        //Handle staging address beans which have no SourceAddressBusId
        List<LandingAddressBean> landingAddressBeansWithoutSourceAddressBusId = landingAddressBeans.stream()
            .filter(lab -> lab.getSourceAddressBusId() == null || lab.getSourceAddressBusId().isEmpty()).collect(Collectors.toList());
        List<StagingAddressBean> stagingAddressBeansWithoutSourceAddressBusId = stagingAddressBeans.stream()
            .filter(sab -> sab.getSourceAddressBusId() == null || sab.getSourceAddressBusId().isEmpty()).collect(Collectors.toList());

        // Group addresses by address type
        Map<String, List<LandingAddressBean>> landingAddressBeansGroupedByAddressType = landingAddressBeansWithoutSourceAddressBusId.stream()
            .collect(Collectors.groupingBy(lab -> lab.getAddressType() + lab.getElectronicAddressType()));
        Map<String, List<StagingAddressBean>> stagingAddressBeansGroupedByAddressType = stagingAddressBeansWithoutSourceAddressBusId.stream()
            .collect(Collectors.groupingBy(sab -> sab.getAddressType() + sab.getElectronicAddressType()));

        // Build a set of address types
        java.util.TreeSet<String> addressTypes = new java.util.TreeSet<String>();
        addressTypes.addAll(landingAddressBeansGroupedByAddressType.keySet());
        addressTypes.addAll(stagingAddressBeansGroupedByAddressType.keySet());

        // Create, update or delete addresses
        addressTypes.forEach(at -> {
            //If landing has more addresses than staging, update existing addresses from same address type, create the rest
            //If landing has fewer addresses than staging, update existing addresses from same address type, delete the rest

            List<LandingAddressBean> landingAddressBeansByAddressType =
                landingAddressBeansGroupedByAddressType.containsKey(at) ? landingAddressBeansGroupedByAddressType.get(at) : new ArrayList<>();
            List<StagingAddressBean> stagingAddressBeansByAddressType =
                stagingAddressBeansGroupedByAddressType.containsKey(at) ? stagingAddressBeansGroupedByAddressType.get(at) : new ArrayList<>();

            // Update existing addresses
            int numberOfAddressesToUpdate = Math.min(landingAddressBeansByAddressType.size(), stagingAddressBeansByAddressType.size());
            for (int i = 0;i < numberOfAddressesToUpdate;i++) {

                LandingAddressBean landingAddressBean = landingAddressBeansByAddressType.get(i);
                StagingAddressBean stagingAddressBean = stagingAddressBeansByAddressType.get(i);

                com.elipslife.ebx.domain.bean.convert.AddressConverter.fillStagingAddressBeanWithLandingAddressBean(landingAddressBean,
                    stagingAddressBean);
                stagingAddressBeanWithOperation.get(PartyConstants.RecordOperations.UPDATE.charAt(0)).add(stagingAddressBeansByAddressType.get(i));
            }

            // Create if true, delete if false
            boolean create = landingAddressBeansByAddressType.size() >= stagingAddressBeansByAddressType.size();

            if (create) {

                // Create addresses in staging which are in landing but not in staging
                List<LandingAddressBean> landingAddressBeansToCreate =
                    landingAddressBeansByAddressType.subList(stagingAddressBeansByAddressType.size(), landingAddressBeansByAddressType.size());
                List<StagingAddressBean> stagingAddressBeansToCreate = landingAddressBeansToCreate.stream()
                    .map(com.elipslife.ebx.domain.bean.convert.AddressConverter::createStagingAddressBeanFromLandingAddressBean)
                    .collect(Collectors.toList());

                stagingAddressBeanWithOperation.get(PartyConstants.RecordOperations.CREATE.charAt(0)).addAll(stagingAddressBeansToCreate);
            } else {

                // Delete addresses from staging which are in staging but not in landing
                List<StagingAddressBean> stagingAddressBeansToDelete =
                    stagingAddressBeansByAddressType.subList(landingAddressBeansByAddressType.size(), stagingAddressBeansByAddressType.size());
                stagingAddressBeanWithOperation.get(PartyConstants.RecordOperations.DELETE.charAt(0)).addAll(stagingAddressBeansToDelete);
            }
        });

        return stagingAddressBeanWithOperation;
    }


    /**
     * Collects staging bank account beans with operation
     * 
     * @param landingBankAccountBeans
     * @param stagingBankAccountBeans
     * @return HashMap with operations: U (Update), C (Create), D (Delete) as key and a list of StagingBankAccountBeans as value
     */
    public static java.util.HashMap<Character, List<StagingBankAccountBean>> buildStagingBankAccountBeanChangeSet(
        List<LandingBankAccountBean> landingBankAccountBeans, List<StagingBankAccountBean> stagingBankAccountBeans) {

        java.util.HashMap<Character, List<StagingBankAccountBean>> stagingBankAccountBeanWithOperation =
            new java.util.HashMap<Character, List<StagingBankAccountBean>>();

        //Handle staging bank account beans which have a SourceBankAccountBusId		
        List<LandingBankAccountBean> landingBankAccountBeansWithSourceBankAccountBusId = landingBankAccountBeans.stream()
            .filter(lab -> lab.getSourceBankAccountBusId() != null && !lab.getSourceBankAccountBusId().isEmpty()).collect(Collectors.toList());
        List<StagingBankAccountBean> stagingBankAccountBeansWithSourceBankAccountBusId = stagingBankAccountBeans.stream()
            .filter(sab -> sab.getSourceBankAccountBusId() != null && !sab.getSourceBankAccountBusId().isEmpty()).collect(Collectors.toList());

        //Create: Available in landing but not in staging
        //		  If ValidTo and ValidFrom are not equal
        List<LandingBankAccountBean> landingBankAccountBeansWithSourceBankAccountBusIdNotInStaging = landingBankAccountBeansWithSourceBankAccountBusId
            .stream().filter(
                lab -> stagingBankAccountBeansWithSourceBankAccountBusId.stream().filter(
                    sab -> Objects.equals(lab.getSourceBankAccountBusId(), sab.getSourceBankAccountBusId()) &&
                        (Objects.equals(lab.getValidFrom(), sab.getValidFrom()) && Objects.equals(lab.getValidTo(), sab.getValidTo())))
                    .count() == 0)
            .collect(Collectors.toList());

        List<StagingBankAccountBean> stagingBankAccountBeansWithSourceBankAccountBusIdToCreate =
            landingBankAccountBeansWithSourceBankAccountBusIdNotInStaging.stream()
                .map(com.elipslife.ebx.domain.bean.convert.BankAccountConverter::createStagingBankAccountBeanFromLandingBankAccountBean)
                .collect(Collectors.toList());
        stagingBankAccountBeanWithOperation.put(PartyConstants.RecordOperations.CREATE.charAt(0),
            stagingBankAccountBeansWithSourceBankAccountBusIdToCreate);

        //Update: Available in landing and staging
        //		  If LastUpdateTimestamp of landing is newer than staging only
        //		  If ValidTo and ValidFrom are equal
        List<StagingBankAccountBean> stagingBankAccountBeansWithSourceBankAccountBusIdToUpdate = stagingBankAccountBeansWithSourceBankAccountBusId
            .stream().filter(
                sab -> landingBankAccountBeansWithSourceBankAccountBusId.stream().filter(
                    lab -> Objects.equals(sab.getSourceBankAccountBusId(), lab.getSourceBankAccountBusId()) &&
                        lab.getUpdateTimestamp().compareTo(sab.getUpdateTimestamp()) >= 0 &&
                        (Objects.equals(lab.getValidFrom(), sab.getValidFrom()) && Objects.equals(lab.getValidTo(), sab.getValidTo())))
                    .count() == 1)
            .collect(Collectors.toList());

        stagingBankAccountBeansWithSourceBankAccountBusIdToUpdate.stream().forEach(sab -> {
            Optional<LandingBankAccountBean> landingBankAccountBean = landingBankAccountBeansWithSourceBankAccountBusId.stream()
                .filter(lab -> Objects.equals(sab.getSourceBankAccountBusId(), lab.getSourceBankAccountBusId())).findFirst();
            com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                .fillStagingBankAccountBeanWithLandingBankAccountBean(landingBankAccountBean.get(), sab);
        });

        stagingBankAccountBeanWithOperation.put(PartyConstants.RecordOperations.UPDATE.charAt(0),
            stagingBankAccountBeansWithSourceBankAccountBusIdToUpdate);

        //Delete: Not available in landing but in staging
        List<StagingBankAccountBean> stagingBankAccountBeansNotInLanding = stagingBankAccountBeansWithSourceBankAccountBusId.stream()
            .filter(sab -> landingBankAccountBeansWithSourceBankAccountBusId.stream()
                .filter(lab -> Objects.equals(sab.getSourceBankAccountBusId(), lab.getSourceBankAccountBusId())).count() == 0)
            .collect(Collectors.toList());
        stagingBankAccountBeanWithOperation.put(PartyConstants.RecordOperations.DELETE.charAt(0), stagingBankAccountBeansNotInLanding);


        //Handle staging bank account beans which have no SourceBankAccountBusId
        List<LandingBankAccountBean> landingBankAccountBeansWithoutSourceBankAccountBusId = landingBankAccountBeans.stream()
            .filter(lab -> lab.getSourceBankAccountBusId() == null || lab.getSourceBankAccountBusId().isEmpty()).collect(Collectors.toList());
        List<StagingBankAccountBean> stagingBankAccountBeansWithoutSourceBankAccountBusId = stagingBankAccountBeans.stream()
            .filter(sab -> sab.getSourceBankAccountBusId() == null || sab.getSourceBankAccountBusId().isEmpty()).collect(Collectors.toList());

        // Group bank accounts by bank account type
        Map<String, List<LandingBankAccountBean>> landingBankAccountBeansGroupedByBankAccountType =
            landingBankAccountBeansWithoutSourceBankAccountBusId.stream()
                .collect(Collectors.groupingBy(lab -> getIbanOrAccountNumberFromLandingBankAccount(lab)));
        Map<String, List<StagingBankAccountBean>> stagingBankAccountBeansGroupedByBankAccountType =
            stagingBankAccountBeansWithoutSourceBankAccountBusId.stream()
                .collect(Collectors.groupingBy(sab -> getIbanOrAccountNumberFromStagingBankAccount(sab)));

        // Build a set of bank account types
        java.util.TreeSet<String> bankAccountTypes = new java.util.TreeSet<String>();
        bankAccountTypes.addAll(landingBankAccountBeansGroupedByBankAccountType.keySet());
        bankAccountTypes.addAll(stagingBankAccountBeansGroupedByBankAccountType.keySet());

        // Create, update or delete bank accounts
        bankAccountTypes.forEach(at -> {
            //If landing has more bank accounts than staging, update existing bank accounts from same bank account type, create the rest
            //If landing has fewer bank accounts than staging, update existing bank accounts from same bank account type, delete the rest

            List<LandingBankAccountBean> landingBankAccountBeansByBankAccountType =
                landingBankAccountBeansGroupedByBankAccountType.containsKey(at) ? landingBankAccountBeansGroupedByBankAccountType.get(at)
                    : new ArrayList<>();
            List<StagingBankAccountBean> stagingBankAccountBeansByBankAccountType =
                stagingBankAccountBeansGroupedByBankAccountType.containsKey(at) ? stagingBankAccountBeansGroupedByBankAccountType.get(at)
                    : new ArrayList<>();

            // Update existing bank accounts
            int numberOfBankAccountesToUpdate =
                Math.min(landingBankAccountBeansByBankAccountType.size(), stagingBankAccountBeansByBankAccountType.size());
            for (int i = 0;i < numberOfBankAccountesToUpdate;i++) {

                LandingBankAccountBean landingBankAccountBean = landingBankAccountBeansByBankAccountType.get(i);
                StagingBankAccountBean stagingBankAccountBean = stagingBankAccountBeansByBankAccountType.get(i);

                com.elipslife.ebx.domain.bean.convert.BankAccountConverter
                    .fillStagingBankAccountBeanWithLandingBankAccountBean(landingBankAccountBean, stagingBankAccountBean);
                stagingBankAccountBeanWithOperation.get(PartyConstants.RecordOperations.UPDATE.charAt(0))
                    .add(stagingBankAccountBeansByBankAccountType.get(i));
            }

            // Create if true, delete if false
            boolean create = landingBankAccountBeansByBankAccountType.size() >= stagingBankAccountBeansByBankAccountType.size();

            if (create) {

                // Create bank accounts in staging which are in landing but not in staging
                List<LandingBankAccountBean> landingBankAccountBeansToCreate = landingBankAccountBeansByBankAccountType
                    .subList(stagingBankAccountBeansByBankAccountType.size(), landingBankAccountBeansByBankAccountType.size());
                List<StagingBankAccountBean> stagingBankAccountBeansToCreate = landingBankAccountBeansToCreate.stream()
                    .map(com.elipslife.ebx.domain.bean.convert.BankAccountConverter::createStagingBankAccountBeanFromLandingBankAccountBean)
                    .collect(Collectors.toList());

                stagingBankAccountBeanWithOperation.get(PartyConstants.RecordOperations.CREATE.charAt(0)).addAll(stagingBankAccountBeansToCreate);
            } else {

                // Delete bank accounts from staging which are in staging but not in landing
                List<StagingBankAccountBean> stagingBankAccountBeansToDelete = stagingBankAccountBeansByBankAccountType
                    .subList(landingBankAccountBeansByBankAccountType.size(), stagingBankAccountBeansByBankAccountType.size());
                stagingBankAccountBeanWithOperation.get(PartyConstants.RecordOperations.DELETE.charAt(0)).addAll(stagingBankAccountBeansToDelete);
            }
        });

        return stagingBankAccountBeanWithOperation;
    }

    private static String getIbanOrAccountNumberFromLandingBankAccount(LandingBankAccountBean landingBankAccountBean) {

        if (landingBankAccountBean != null) {

            if (landingBankAccountBean.getAccountNumber() != null) {
                return landingBankAccountBean.getAccountNumber();
            } else if (landingBankAccountBean.getIban() != null) {
                return landingBankAccountBean.getIban();
            }
        }

        return null;

    }

    private static String getIbanOrAccountNumberFromStagingBankAccount(StagingBankAccountBean stagingBankAccountBean) {

        if (stagingBankAccountBean != null) {

            if (stagingBankAccountBean.getAccountNumber() != null) {
                return stagingBankAccountBean.getAccountNumber();
            } else if (stagingBankAccountBean.getIban() != null) {
                return stagingBankAccountBean.getIban();
            }
        }

        return null;

    }
}
