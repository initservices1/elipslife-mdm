package com.elipslife.mdm.uw.landing;

import java.math.BigDecimal;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.LandingUWPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class LandingUWCaseReader extends AbstractTableReader<LandingUWCaseBean> {

    public LandingUWCaseReader() {
        this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.LANDING_UW_REFERENCE,
            Constants.DataSet.LANDING_UW_MASTER), LandingUWPaths._UWCase.getPathInSchema()));
    }

    public LandingUWCaseReader(String dataspace) {
        this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.LANDING_UW_MASTER),
            LandingUWPaths._UWCase.getPathInSchema()));
    }

    public LandingUWCaseReader(AdaptationTable table) {
        super(table);
    }

    @Override
    public LandingUWCaseBean createBeanFromRecord(Adaptation record) {
        LandingUWCaseBean bean = new LandingUWCaseBean();
        bean.setUWCaseId(record.get_int(LandingUWPaths._UWCase._UWCaseId));
        bean.setUWCaseBusId(record.getString(LandingUWPaths._UWCase._UWCaseBusId));
        bean.setUWApplicationId(record.get_int(LandingUWPaths._UWCase._UWApplicationId));
        bean.setSourceSystem(record.getString(LandingUWPaths._UWCase._Context_SourceSystem));
        bean.setDataOwner(record.getString(LandingUWPaths._UWCase._Context_DataOwner));
        bean.setUWApplicationBusId(record.getString(LandingUWPaths._UWCase._Context_UWApplicationBusId));
        bean.setContractNumber(record.getString(LandingUWPaths._UWCase._Context_ContractNumber));
        bean.setApplicantBusId(record.getString(LandingUWPaths._UWCase._Applicant_ApplicantBusId));
        bean.setApplicantRelationship(record.getString(LandingUWPaths._UWCase._Applicant_ApplicantRelationship));
        bean.setApplicantFirstName(record.getString(LandingUWPaths._UWCase._Applicant_ApplicantFirstName));
        bean.setApplicantLastName(record.getString(LandingUWPaths._UWCase._Applicant_ApplicantLastName));
        bean.setApplicantDateOfBirth(record.getDate(LandingUWPaths._UWCase._Applicant_ApplicantDateOfBirth));
        bean.setApplicantSSN(record.getString(LandingUWPaths._UWCase._Applicant_ApplicantSSN));
        bean.setApplicantTown(record.getString(LandingUWPaths._UWCase._Applicant_ApplicantTown));
        bean.setApplicantPostCode(record.getString(LandingUWPaths._UWCase._Applicant_ApplicantPostCode));
        bean.setApplicantCountry(record.getString(LandingUWPaths._UWCase._Applicant_ApplicantCountry));
        bean.setApplicantClass(record.getString(LandingUWPaths._UWCase._Details_ApplicantClass));
        bean.setProductType(record.getString(LandingUWPaths._UWCase._Details_ProductType));
        bean.setEligiblityDate(record.getDate(LandingUWPaths._UWCase._Details_EligiblityDate));
        bean.setApplicationReason(record.getString(LandingUWPaths._UWCase._Details_ApplicationReason));
        bean.setApplicationReasonText(record.getString(LandingUWPaths._UWCase._Details_ApplicationReasonText));
        bean.setInforceCoverage((BigDecimal) record.get(LandingUWPaths._UWCase._Details_InforceCoverage));
        bean.setAdditionalCoverage((BigDecimal) record.get(LandingUWPaths._UWCase._Details_AdditionalCoverage));
        bean.setIsActive((Boolean) record.get(LandingUWPaths._UWCase._State_IsActive));
        bean.setCaseState(record.getString(LandingUWPaths._UWCase._State_CaseState));
        bean.setCreatedBy(record.getString(LandingUWPaths._UWCase._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(LandingUWPaths._UWCase._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(LandingUWPaths._UWCase._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(LandingUWPaths._UWCase._Control_LastUpdateTimestamp));
        bean.setDeletionTimeStamp(record.getDate(LandingUWPaths._UWCase._Control_DeletionTimestamp));
        bean.setLastSyncTimeStamp(record.getDate(LandingUWPaths._UWCase._Control_LastSyncTimestamp));
        bean.setAction(record.getString(LandingUWPaths._UWCase._Control_Action));
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[] {LandingUWPaths._UWCase._UWCaseId};
    }

    @Override
    public Object[] retrievePks(LandingUWCaseBean bean) {
        return new Object[] {bean.getUWCaseId()};
    }

    /**
     * Checks for validation errors with severity error or fatal
     * 
     * @param primaryKeys
     * @return
     */
    public boolean hasValidationErrors(Object[] primaryKeys) {

        Adaptation adaptation = super.findRecordByPks(primaryKeys);

        ValidationReport validationReport = adaptation.getValidationReport();

        boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);

        return hasErrors;
    }
}
