package com.elipslife.mdm.hr.staging;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class STG_BuildingReader extends AbstractTableReader<STG_BuildingBean> {

	public STG_BuildingReader() {
		this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Building.getPathInSchema()));
	}
	
	public STG_BuildingReader(String dataspace) {
		this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Building.getPathInSchema()));
	}

	public STG_BuildingReader(AdaptationTable table) {
		super(table);
	}

	@Override
	public STG_BuildingBean createBeanFromRecord(Adaptation record) {
		STG_BuildingBean bean = new STG_BuildingBean();
		bean.setBuildingId(record.get_int(HrModelPaths._STG_Building._Building_BuildingId));
		bean.setDataOwner(record.getString(HrModelPaths._STG_Building._Building_DataOwner));
		bean.setBuildingBusId(record.getString(HrModelPaths._STG_Building._Building_BuildingBusId));
		bean.setName(record.getString(HrModelPaths._STG_Building._Building_Name));
		bean.setStreet(record.getString(HrModelPaths._STG_Building._Building_Street));
		bean.setStreet2(record.getString(HrModelPaths._STG_Building._Building_Street2));
		bean.setPostCode(record.getString(HrModelPaths._STG_Building._Building_PostCode));
		bean.setPOBox(record.getString(HrModelPaths._STG_Building._Building_POBox));
		bean.setPOBoxPostCode(record.getString(HrModelPaths._STG_Building._Building_POBoxPostCode));
		bean.setPOBoxTown(record.getString(HrModelPaths._STG_Building._Building_POBoxTown));
		bean.setTown(record.getString(HrModelPaths._STG_Building._Building_Town));
		bean.setDistrict(record.getString(HrModelPaths._STG_Building._Building_District));
		bean.setStateProvince(record.getString(HrModelPaths._STG_Building._Building_StateProvince));
		bean.setCountry(record.getString(HrModelPaths._STG_Building._Building_Country));
		bean.setIsActive(record.get_boolean(HrModelPaths._STG_Building._Building_IsActive));
		// bean.setBuildingMaster(record.getString(HrModelPaths._STG_Building._Building_BuildingMaster));
		bean.setSourceSystem(record.getString(HrModelPaths._STG_Building._Control_SourceSystem));
        bean.setCreatedBy(record.getString(HrModelPaths._STG_Building._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(HrModelPaths._STG_Building._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(HrModelPaths._STG_Building._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(HrModelPaths._STG_Building._Control_LastUpdateTimestamp));
        bean.setLastSyncTimestamp(record.getDate(HrModelPaths._STG_Building._Control_LastSyncTimestamp));
		return bean;
	}

	@Override
	public Path[] getPkPaths() {
		return new Path[] { HrModelPaths._Building._Building_BuildingId };
	}

	@Override
	public Object[] retrievePks(STG_BuildingBean bean) {
		return new Object[] { bean.getBuildingId() };
	}
	
	/**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}

}
