package com.elipslife.ebx.data.access;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class StagingPartyReader extends BasePartyReader<StagingPartyBean> {

    public StagingPartyReader() {
        super(RepositoryHelper.getTable(
            RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
            PartyPaths._STG_Party.getPathInSchema()));
    }

    public StagingPartyReader(String dataspace) {
        this(RepositoryHelper.getTable(
            RepositoryHelper.getDataSet(dataspace, PartyConstants.DataSet.PARTY_MASTER),
            PartyPaths._STG_Party.getPathInSchema()));
    }

    public StagingPartyReader(AdaptationTable table) {
        super(table);
    }

    @Override
    public StagingPartyBean createBeanFromRecord(Adaptation record) {

        StagingPartyBean stagingPartyBean = new StagingPartyBean();

        stagingPartyBean.setId(record.get_int(PartyPaths._STG_Party._Party_PartyId));

        stagingPartyBean.setDataOwner(record.getString(PartyPaths._STG_Party._Party_DataOwner));
        stagingPartyBean.setOperatingCountry(record.getString(PartyPaths._STG_Party._Party_OperatingCountry));
        stagingPartyBean.setCrmId(record.getString(PartyPaths._STG_Party._Party_CRMId));
        stagingPartyBean.setPartyType(record.getString(PartyPaths._STG_Party._Party_PartyType));
        stagingPartyBean.setRelationshipOwner(record.getString(PartyPaths._STG_Party._Party_RelationshipOwner));
        stagingPartyBean.setDataResponsible(record.getString(PartyPaths._STG_Party._Party_DataResponsible));
        stagingPartyBean.setLanguage(record.getString(PartyPaths._STG_Party._Party_Language));
        stagingPartyBean.setIsActive((Boolean) record.get(PartyPaths._STG_Party._Party_IsActive));
        stagingPartyBean.setCorePartyId(record.getString(PartyPaths._STG_Party._Party_CorePartyId));
        stagingPartyBean.setLegalAddressId(record.getString(PartyPaths._STG_Party._Party_LegalPostalAddress));
        stagingPartyBean.setCorrelationId(record.getString(PartyPaths._STG_Party._Party_CorrelationId));
        stagingPartyBean.setFirstName(record.getString(PartyPaths._STG_Party._Person_FirstName));
        stagingPartyBean.setLastName(record.getString(PartyPaths._STG_Party._Person_LastName));
        stagingPartyBean.setMiddleName(record.getString(PartyPaths._STG_Party._Person_MiddleName));
        stagingPartyBean.setDateOfBirth(record.getDate(PartyPaths._STG_Party._Person_DateOfBirth));
        stagingPartyBean.setGender(record.getString(PartyPaths._STG_Party._Person_Gender));
        stagingPartyBean.setTitle(record.getString(PartyPaths._STG_Party._Person_Title));
        stagingPartyBean.setSalutation(record.getString(PartyPaths._STG_Party._Person_Salutation));
        stagingPartyBean.setLetterSalutation(record.getString(PartyPaths._STG_Party._Person_LetterSalutation));
        stagingPartyBean.setCivilStatus(record.getString(PartyPaths._STG_Party._Person_CivilStatus));
        stagingPartyBean.setBirthCity(record.getString(PartyPaths._STG_Party._Person_BirthCity));
        stagingPartyBean.setBirthCountry(record.getString(PartyPaths._STG_Party._Person_BirthCountry));
        stagingPartyBean.setIdDocumentNumber(record.getString(PartyPaths._STG_Party._IdDocument_ID_DocumentNumber));
        stagingPartyBean.setIdDocumentType(record.getString(PartyPaths._STG_Party._IdDocument_ID_DocumentType));
        stagingPartyBean.setIdIssueDate(record.getDate(PartyPaths._STG_Party._IdDocument_ID_IssueDate));
        stagingPartyBean.setIdExpiryDate(record.getDate(PartyPaths._STG_Party._IdDocument_ID_ExpiryDate));
        stagingPartyBean.setIdIssuingAuthority(record.getString(PartyPaths._STG_Party._IdDocument_ID_IssuingAuthority));
        stagingPartyBean.setSourceParentBusId(record.getString(PartyPaths._STG_Party._Organisation_SourceParentBusId));
        stagingPartyBean.setOrgName(record.getString(PartyPaths._STG_Party._Organisation_OrgName));
        stagingPartyBean.setOrgName2(record.getString(PartyPaths._STG_Party._Organisation_OrgName2));
        stagingPartyBean.setCompanyType(record.getString(PartyPaths._STG_Party._Organisation_CompanyType));
        stagingPartyBean.setCommercialRegNo(record.getString(PartyPaths._STG_Party._Organisation_CommercialRegNo));
        stagingPartyBean.setUidNumber(record.getString(PartyPaths._STG_Party._Organisation_UIDNumber));
        stagingPartyBean.setVatNumber(record.getString(PartyPaths._STG_Party._Organisation_VATNumber));
        stagingPartyBean.setIndustry(record.getString(PartyPaths._STG_Party._Organisation_Industry));
        stagingPartyBean.setSubIndustry(record.getString(PartyPaths._STG_Party._Organisation_SubIndustry));
        stagingPartyBean.setPreferredEmail(record.getString(PartyPaths._STG_Party._Organisation_PreferredEmail));
        stagingPartyBean.setPreferredCommChannel(record.getString(PartyPaths._STG_Party._Organisation_PreferredCommChannel));
        stagingPartyBean.setSourceOrganisationBusId(record.getString(PartyPaths._STG_Party._ContactPerson_SourceOrganisationBusId));
        stagingPartyBean.setSourceLineManagerBusId(record.getString(PartyPaths._STG_Party._ContactPerson_SourceLineManagerBusId));
        stagingPartyBean.setSourcePartnerBusId(record.getString(PartyPaths._STG_Party._ContactPerson_SourcePartnerBusId));
        stagingPartyBean.setDepartment(record.getString(PartyPaths._STG_Party._ContactPerson_Department));
        stagingPartyBean.setFunction(record.getString(PartyPaths._STG_Party._ContactPerson_Function));
        stagingPartyBean.setIsExecutor((Boolean) record.get(PartyPaths._STG_Party._ContactPerson_IsExecutor));
        stagingPartyBean.setIsLegalRepresentative((Boolean) record.get(PartyPaths._STG_Party._ContactPerson_IsLegalRepresentative));
        stagingPartyBean.setIsSeniorManager((Boolean) record.get(PartyPaths._STG_Party._ContactPerson_IsSeniorManager));
        stagingPartyBean.setIsUltimateBeneficialOwner((Boolean) record.get(PartyPaths._STG_Party._ContactPerson_IsUltimateBeneficialOwner));
        stagingPartyBean.setBoRelationship(record.getString(PartyPaths._STG_Party._ContactPerson_BORelationship));
        stagingPartyBean.setContactPersonState(record.getString(PartyPaths._STG_Party._ContactPerson_ContactPersonState));
        stagingPartyBean.setRoles(record.getString(PartyPaths._STG_Party._Role_Roles));
        stagingPartyBean.setIsVipClient((Boolean) record.get(PartyPaths._STG_Party._Role_IsVIPClient));
        stagingPartyBean.setBlacklist(record.getString(PartyPaths._STG_Party._Role_Blacklist));
        stagingPartyBean.setPartnerState(record.getString(PartyPaths._STG_Party._Role_PartnerState));
        stagingPartyBean.setCompanyPersonalNr(record.getString(PartyPaths._STG_Party._Role_CompanyPersonalNr));
        stagingPartyBean.setSocialSecurityNr(record.getString(PartyPaths._STG_Party._Role_SocialSecurityNr));
        stagingPartyBean.setSocialSecurityNrType(record.getString(PartyPaths._STG_Party._Role_SocialSecurityNrType));
        stagingPartyBean.setBrokerRegNo(record.getString(PartyPaths._STG_Party._Role_BrokerRegNo));
        stagingPartyBean.setBrokerTiering(record.getString(PartyPaths._STG_Party._Role_BrokerTiering));
        stagingPartyBean.setLineOfBusiness(record.getString(PartyPaths._STG_Party._Role_LineOfBusiness));
        stagingPartyBean.setFinancePartyBusId(record.getString(PartyPaths._STG_Party._Financial_FinancePartyBusId));
        stagingPartyBean.setPaymentTerms(record.getString(PartyPaths._STG_Party._Financial_PaymentTerms));
        stagingPartyBean.setFinanceClients(record.getString(PartyPaths._STG_Party._Financial_FinanceClients));
        stagingPartyBean.setCreditorClients(record.getString(PartyPaths._STG_Party._Financial_CreditorClients));
        stagingPartyBean.setSourceSystem(record.getString(PartyPaths._STG_Party._Control_SourceSystem));
        stagingPartyBean.setSourcePartyBusId(record.getString(PartyPaths._STG_Party._Control_SourcePartyBusId));
        stagingPartyBean.setValidFrom(record.getDate(PartyPaths._STG_Party._Control_ValidFrom));
        stagingPartyBean.setValidTo(record.getDate(PartyPaths._STG_Party._Control_ValidTo));
        stagingPartyBean.setCreationTimestamp(record.getDate(PartyPaths._STG_Party._Control_CreationTimestamp));
        stagingPartyBean.setLastUpdateTimestamp(record.getDate(PartyPaths._STG_Party._Control_LastUpdateTimestamp));
        stagingPartyBean.setAction(record.getString(PartyPaths._STG_Party._Control_Action));
        stagingPartyBean.setActionBy(record.getString(PartyPaths._STG_Party._Control_ActionBy));

        return stagingPartyBean;
    }

    @Override
    protected Path[] getPkPaths() {

        return new Path[] {PartyPaths._STG_Party._Party_PartyId};
    }

    @Override
    protected Object[] retrievePks(StagingPartyBean bean) {

        return new Object[] {bean.getId()};
    }


    /**
     * 
     * @param corePartyId
     * @return
     */
    public List<StagingPartyBean> findBeansByCorePartyId(String corePartyId) {

        XPathPredicate predicate1 =
            (corePartyId == null || corePartyId.isEmpty())
                ? new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._STG_Party._Party_CorePartyId, corePartyId)
                : new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._STG_Party._Party_CorePartyId, corePartyId);

        return readRecordsFor(predicate1.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }


    /**
     * 
     * @param corePartyId
     * @return
     */
    public List<StagingPartyBean> findBeansByCorePartyIdUpToDate(String corePartyId) {

        Date dateNow = new Date();

        return findBeansByCorePartyIdAndDate(corePartyId, dateNow);
    }


    /**
     * 
     * @param corePartyId
     * @param date
     * @return
     */
    public List<StagingPartyBean> findBeansByCorePartyIdAndDate(String corePartyId, Date date) {

        XPathPredicate predicate1 =
            (corePartyId == null || corePartyId.isEmpty())
                ? new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._STG_Party._Party_CorePartyId, corePartyId)
                : new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._STG_Party._Party_CorePartyId, corePartyId);

        date = removeTime(date);

        XPathPredicateGroup groupValidDate1 = new XPathPredicateGroup(LinkingType.And);
        String dateNowString = simpleDateFormat.format(date);
        XPathPredicate predicate3 =
            new XPathPredicate(XPathPredicate.Operation.DateLessThan, PartyPaths._STG_Party._Control_ValidFrom, dateNowString);
        groupValidDate1.addPredicate(predicate3);

        XPathPredicate predicate4 =
            new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, PartyPaths._STG_Party._Control_ValidTo, dateNowString);
        groupValidDate1.addPredicate(predicate4);


        XPathPredicateGroup groupValidDate2 = new XPathPredicateGroup(LinkingType.Or);
        XPathPredicate predicate5 = new XPathPredicate(XPathPredicate.Operation.DateEqual, PartyPaths._STG_Party._Control_ValidFrom, dateNowString);
        groupValidDate2.addPredicate(predicate5);

        XPathPredicate predicate6 = new XPathPredicate(XPathPredicate.Operation.DateEqual, PartyPaths._STG_Party._Control_ValidTo, dateNowString);
        groupValidDate2.addPredicate(predicate6);

        XPathPredicateGroup groupValidDate = new XPathPredicateGroup(LinkingType.Or);
        groupValidDate.addPredicateGroup(groupValidDate1);
        groupValidDate.addPredicateGroup(groupValidDate2);


        XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
        group.addPredicate(predicate1);
        group.addPredicateGroup(groupValidDate);

        return readRecordsFor(group.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }

    public List<StagingPartyBean> getStagingPartyBeansByLastSyncTimeStamp(Date lastSyncTimestamp) {

        String dateNowString = DateHelper.convertToXpathFormat(lastSyncTimestamp);
        
        XPathPredicate predicate1 =
            new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, PartyPaths._STG_Party._Control_LastSyncTimestamp, dateNowString);
        XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
        group.addPredicate(predicate1);
        
        return readRecordsFor(group.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }
    
    /**
     * find duplicated records for Individual / Person
     * 
     * @param firstName
     * @param lastName
     * @param dateOfBirth
     * @param socialSecurityNumber
     * @param socialSecurityNumberType
     * @param organisationId
     * @return
     */
    public List<Adaptation> findBeansIndividualPerson(String firstName, String lastName, Date dateOfBirth, String socialSecurityNumber,
    		String socialSecurityNumberType, String organisationId) {
    	
    	XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
    	
    	// XPathPredicate predicatePartyType = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._STG_Party._Party_PartyType, PartyConstants.Entities.Party.PartyType.INDIVIDUAL);
    	XPathPredicate predicateFirstName = new XPathPredicate(firstName != null ? XPathPredicate.Operation.Equals : XPathPredicate.Operation.IsNull, PartyPaths._STG_Party._Person_FirstName, firstName);
		XPathPredicate predicateLastName = new XPathPredicate(lastName != null ? XPathPredicate.Operation.Equals : XPathPredicate.Operation.IsNull, PartyPaths._STG_Party._Person_LastName, lastName);
		group.addPredicate(predicateFirstName);
		group.addPredicate(predicateLastName);
		
		/*
		XPathPredicateGroup groupDateOfBirthSocialSecurity = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		String dateOfBirthString = dateOfBirth != null ? simpleDateFormat.format(dateOfBirth) : null;
		XPathPredicate predicateDateOfBirth = new XPathPredicate(dateOfBirthString != null ? XPathPredicate.Operation.DateEqual : XPathPredicate.Operation.IsNull, PartyPaths._STG_Party._Person_DateOfBirth, dateOfBirthString);
		groupDateOfBirthSocialSecurity.addPredicate(predicateDateOfBirth);
		group.addPredicateGroup(groupDateOfBirthSocialSecurity);
		*/
		
		return readRecordsFor(group.toString());
	}
    
    /**
     *  find duplicated records for Org.Unit
     *  
     * @param organisationName
     * @param parentOrganisationId
     * @param partyType
     * @return
     */
    public List<Adaptation> findBeansOrganisationUnit(String organisationName, String parentOrganisationId, String partyType) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		XPathPredicate predicateOrganisationName = new XPathPredicate(organisationName != null ? XPathPredicate.Operation.Equals : XPathPredicate.Operation.IsNull, PartyPaths._STG_Party._Organisation_OrgName, organisationName);
		XPathPredicate predicateParentOrganisationId = new XPathPredicate(parentOrganisationId != null ? XPathPredicate.Operation.Equals : XPathPredicate.Operation.IsNull, PartyPaths._STG_Party._Organisation_SourceParentBusId, parentOrganisationId);
		XPathPredicate predicatePartyTypeId = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._STG_Party._Party_PartyType, partyType);
		
		group.addPredicate(predicatePartyTypeId);
		group.addPredicate(predicateOrganisationName);
		group.addPredicate(predicateParentOrganisationId);
		
		
		return readRecordsFor(group.toString());
	}
    
    /**
     * find duplicate records for Company
     * 
     * @param organisationName
     * @param uidNumber
     * @param commercialRegNo
     * @param partyType
     * @return
     */
    public List<Adaptation> findBeansCompany(String organisationName, String organisationName2, String uidNumber, String commercialRegNo, String partyType) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		XPathPredicate predicatePartyType = new XPathPredicate(partyType != null ? XPathPredicate.Operation.Equals : XPathPredicate.Operation.IsNull, PartyPaths._STG_Party._Party_PartyType, partyType);
		group.addPredicate(predicatePartyType);
		XPathPredicate predicateOrganisationName = new XPathPredicate(organisationName != null ? XPathPredicate.Operation.Equals : XPathPredicate.Operation.IsNull, PartyPaths._STG_Party._Organisation_OrgName, organisationName);
		group.addPredicate(predicateOrganisationName);
		
		return readRecordsFor(group.toString());
	}


    @Override
    protected Path getPathControlSourceSystem() {

        return PartyPaths._STG_Party._Control_SourceSystem;
    }


    @Override
    protected Path getPathControlSourcePartyBusId() {

        return PartyPaths._STG_Party._Control_SourcePartyBusId;
    }


    @Override
    protected Path getPathControlValidFrom() {

        return PartyPaths._STG_Party._Control_ValidFrom;
    }


    @Override
    protected Path getPathControlValidTo() {

        return PartyPaths._STG_Party._Control_ValidTo;
    }

    @Override
    protected Path getPathCrmId() {

        return PartyPaths._STG_Party._Party_CRMId;
    }
}
