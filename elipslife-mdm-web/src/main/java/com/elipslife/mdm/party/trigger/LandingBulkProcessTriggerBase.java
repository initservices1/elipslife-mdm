package com.elipslife.mdm.party.trigger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.elipslife.ebx.data.access.LandingPartyBulkProcessReader;
import com.elipslife.ebx.data.access.StagingPartyReader;
import com.elipslife.ebx.domain.bean.LandingPartyBulkProcessBean;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public abstract class LandingBulkProcessTriggerBase extends TableTrigger {

    protected void setLogicalDefaultValues(ValueContextForUpdate valueContextForUpdate) {

    	LandingPartyBulkProcessBean landingPartyBean = null;
    	StagingPartyBean stagingPartyBean = null;
        LandingPartyBulkProcessReader landingPartyReader = new LandingPartyBulkProcessReader();
        List<LandingPartyBulkProcessBean> landingPartyBeans = new ArrayList<LandingPartyBulkProcessBean>();
        
    	String landingPartyId = (String) valueContextForUpdate.getValue(getPathLandingPartyId());
    	if(landingPartyId != null) {
    		landingPartyBean = landingPartyReader.findBeanByPartyId(Integer.parseInt(landingPartyId));
    	}
    	
    	if(landingPartyBean == null) {
    		String sourceSystem = (String) valueContextForUpdate.getValue(LandingPaths._LDG_Address._Control_SourceSystem);
            String sourcePartyBusId = (String) valueContextForUpdate.getValue(LandingPaths._LDG_Address._Control_SourcePartyBusId);
            landingPartyBeans = landingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId);
            if (!landingPartyBeans.isEmpty()) {
                landingPartyBean = landingPartyBeans.get(0);
            }
            
            // If landingParty or Staging Party not found, Update the staging process response
            if(landingPartyBean == null) {
            	
                StagingPartyReader stagingPartyReader = new StagingPartyReader();
                List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId);
                if (!stagingPartyBeans.isEmpty()) {
                    stagingPartyBean = stagingPartyBeans.get(0);
                }
                
                if(stagingPartyBean == null) {
                	String errorMessage = String.format("No landing or staging party with source sytem %s and source party bus id %s found", sourceSystem, sourcePartyBusId);
                	valueContextForUpdate.setValue(errorMessage, getPathMdmStagingProcessResponse());
                }
            }

    	}

        Date validFrom = (Date) valueContextForUpdate.getValue(LandingPaths._LDG_Address._Control_ValidFrom);
        if (validFrom == null) {
            valueContextForUpdate.setValue(
                landingPartyBean != null ? landingPartyBean.getValidFrom() : stagingPartyBean.getValidFrom(),
                getPathControlValidFrom());
        }

        Date validTo = (Date) valueContextForUpdate.getValue(LandingPaths._LDG_Address._Control_ValidTo);
        if (validTo == null) {
            valueContextForUpdate.setValue(
                landingPartyBean != null ? landingPartyBean.getValidTo() : stagingPartyBean.getValidTo(),
                getPathControlValidTo());
        }

        // Set the default values when the comes from REST
        if (valueContextForUpdate.getHome().getKey().getName().equals(PartyConstants.DataSpace.LANDING_REFERENCE)) {

            String action = (String) valueContextForUpdate.getValue(LandingPaths._LDG_Address._Control_Action);
            if (action == null || action.isEmpty()) {
                action = landingPartyBean != null ? landingPartyBean.getAction() : stagingPartyBean.getAction();
                valueContextForUpdate.setValue(action, getPathAction());
            }

            Date lastUpdateTimestamp = (Date) valueContextForUpdate
                .getValue(LandingPaths._LDG_Address._Control_LastUpdateTimestamp);
            // If lastUpdatedTimestamp is null and action is update, Get the
            // lastUpdatedTimestamp from landing party or staging party
            if (lastUpdateTimestamp == null && action.equals(PartyConstants.RecordOperations.UPDATE)) {
                valueContextForUpdate.setValue(landingPartyBean != null ? landingPartyBean.getLastUpdateTimestamp()
                    : stagingPartyBean.getLastUpdateTimestamp(), getPathLastUpdateTimestamp());
            }

            Date creationTimestamp = (Date) valueContextForUpdate
                .getValue(LandingPaths._LDG_Address._Control_CreationTimestamp);
            // If creationTimestamp is null and action is create, Get the creationTimestamp
            // from landing party or staging party
            // if (creationTimestamp == null &&
            // action.equals(PartyConstants.RecordOperations.CREATE)) {
            if (creationTimestamp == null) {
                valueContextForUpdate.setValue(landingPartyBean != null ? landingPartyBean.getCreationTimestamp()
                    : stagingPartyBean.getCreationTimestamp(), getPathCreationTimestamp());
            }
        }
    }

    protected abstract Path getPathLandingPartyId();
    
    protected abstract Path getPathMdmStagingProcessResponse();
    
    protected abstract Path getPathControlValidFrom();

    protected abstract Path getPathControlValidTo();

    protected abstract Path getPathCreationTimestamp();

    protected abstract Path getPathLastUpdateTimestamp();

    protected abstract Path getPathAction();

    @Override
    public void setup(TriggerSetupContext aContext) {
        // TODO Auto-generated method stub

    }

}
