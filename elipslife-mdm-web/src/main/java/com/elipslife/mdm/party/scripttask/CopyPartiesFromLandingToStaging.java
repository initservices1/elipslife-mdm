package com.elipslife.mdm.party.scripttask;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.butos.ebx.procedure.CreateRecords;
import com.butos.ebx.procedure.DeleteRecords;
import com.butos.ebx.procedure.RecordValuesBean;
import com.butos.ebx.procedure.UpdateRecords;
import com.elipslife.ebx.data.access.LandingPartyBulkProcessReader;
import com.elipslife.ebx.data.access.LandingPartyReader;
import com.elipslife.ebx.data.access.StagingPartyReader;
import com.elipslife.ebx.data.access.StagingPartyWriter;
import com.elipslife.ebx.data.access.StagingPartyWriterSession;
import com.elipslife.ebx.domain.bean.LandingPartyBean;
import com.elipslife.ebx.domain.bean.LandingPartyBulkProcessBean;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.ebx.domain.bean.convert.ReadPartyBulkProcesserProperty;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.elipslife.mdm.path.PartyPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.boot.VM;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

public class CopyPartiesFromLandingToStaging extends ScriptTask {

	private Integer recordCount = 0;
	private static AdaptationHome stagingDataSpaceHome;
	private static AdaptationHome templandingDataspace;
	private static Adaptation stagingDataSet;
	private static Adaptation landingDataSet;
	private static LocalDateTime currentTimeNow; 
	public static int batchCount = 1;
	public static int processedRecordsInThisBatch;
	public static int totalRecordsCount = 0;

	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException {

		HashMap<String, String> contextVariables = getContextVariables(context);
		String dataSpace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_LANDING_DATASPACE);
		String stagingDataspace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		String commitSize = contextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);

		Repository reposity = context.getRepository();
		setStagingDataSpaceHome(reposity.lookupHome(HomeKey.forBranchName(stagingDataspace)));
		setTemplandingDataspace(reposity.lookupHome(HomeKey.forBranchName(dataSpace)));
		setStagingDataSet(getStagingDataSpaceHome());
		setLandingDataSet(getTemplandingDataspace());

		List<LandingPartyBulkProcessBean> landingPartyBulkProcessBeansToDelete = new ArrayList<LandingPartyBulkProcessBean>();
		StringBuilder stringBuilderErrorMessage = new StringBuilder();
		ReadPartyBulkProcesserProperty bulkProcesserProperty = ReadPartyBulkProcesserProperty.getInstance();
		
		// Check If it is a Bulk load or Incremental load
		if (bulkProcesserProperty.getPartyBulkProcessProperty()
				.equals(PartyConstants.Entities.Party.RestBulkProcessState.REST_BULKPROCESS_STATE_FULL)) {

			recordCount = 0;

			// Create landing Party Bulk Process reader to read from child data space
			LandingPartyBulkProcessReader landingPartyBulkProcessReader = new LandingPartyBulkProcessReader(dataSpace);
			List<LandingPartyBulkProcessBean> landingBulkProcessBeans = landingPartyBulkProcessReader.readAllBeans();
			
			// Create staging address writer to write into main data space
			List<LandingPartyBulkProcessBean> landingPartyBulkProcessBeansOrdered = landingBulkProcessBeans.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getLastUpdateTimestamp(),
							b.getLastUpdateTimestamp())).collect(Collectors.toList());

			List<RecordValuesBean> stagingBeansToCreate = new ArrayList<RecordValuesBean>();
			List<StagingPartyBean> stagingBeansToUpdate = new ArrayList<StagingPartyBean>();
			List<RecordValuesBean> stagingBeanToDelete = new ArrayList<RecordValuesBean>();
			
			for (LandingPartyBulkProcessBean lpb : landingPartyBulkProcessBeansOrdered) { //List of all the beans from Landing Party Bulk Table
				boolean landingPartyBeanInvalid = landingPartyBulkProcessReader.hasValidationErrors(new Object[] { lpb.getId() });
				if (landingPartyBeanInvalid) {
					continue;
				}
				
				landingPartyBulkProcessBeansToDelete.add(lpb);
				recordCount++;
				try {
					StagingPartyBean stagingPartyBean = com.elipslife.ebx.domain.bean.convert.PartyConverter.createStagingPartyBeanFromLandingPartyBulkProcessBean(lpb);
					HashMap<Path, Object> values = StagingPartyConverter.fillStagingPartyValuesFromLandingPartyBean(stagingPartyBean, true);
					RecordValuesBean rvb = new RecordValuesBean(values);
					stagingBeansToCreate.add(rvb);
				} catch (Exception ex) {
					stringBuilderErrorMessage.append(String.format("Error while creating staging party bean with source system \"%s\" and source party bus id \"%s\"\r\n",
							lpb.getSourceSystem(), lpb.getSourcePartyBusId()));
					stringBuilderErrorMessage.append(ex.toString() + "\r\n");
				}
				
				if (recordCount > Integer.parseInt(commitSize)) {
					try {
						createStagingParties(stagingBeansToCreate, context.getSession(), stagingDataspace);
						// updateStagingParties(stagingBeansToUpdate, context.getSession(), stagingDataspace);
						stagingBeansToCreate.clear();
						stagingBeansToUpdate.clear();
						recordCount = 1;
					} catch (OperationException e) {
						e.printStackTrace();
					}
				}
			}

			if (!stagingBeansToCreate.isEmpty() || !stagingBeansToUpdate.isEmpty() || !stagingBeanToDelete.isEmpty()) 
			{
				try {
					createStagingParties(stagingBeansToCreate, context.getSession(), stagingDataspace);
					stagingBeansToCreate.clear();
					stagingBeansToUpdate.clear();
					stagingBeanToDelete.clear();
					recordCount = 1;
				} catch (OperationException e) {
					e.printStackTrace();
				}
			}
			
			// Delete Landing records
			deleteLandingRecords(landingPartyBulkProcessBeansToDelete, context.getSession(), dataSpace);
		} else {
			// Create landing address reader to read from child data space
			LandingPartyReader landingPartyReader = new LandingPartyReader(dataSpace);
			List<LandingPartyBean> landingPartyBeans = landingPartyReader.readAllBeans();

			// Create staging address writer to write into main data space
			StagingPartyWriter stagingPartyWriter = new StagingPartyWriterSession(context.getSession(), true);
			StagingPartyReader stagingPartyReader = (StagingPartyReader) stagingPartyWriter.getTableReader();
			List<LandingPartyBean> landingPartyBeansOrdered = landingPartyBeans.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getLastUpdateTimestamp(),
							b.getLastUpdateTimestamp())).collect(Collectors.toList());
			landingPartyBeansOrdered.forEach(lpb -> 
			{
				// Create or update party
				List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(lpb.getSourceSystem(), lpb.getSourcePartyBusId(), lpb.getValidFrom(), lpb.getValidTo());
				StagingPartyBean stagingPartyBean = null;
				if (stagingPartyBeans.isEmpty()) 
				{
					try {
						stagingPartyBean = com.elipslife.ebx.domain.bean.convert.PartyConverter.createStagingPartyBeanFromLandingPartyBean(lpb);
						stagingPartyWriter.createBeanOrThrow(stagingPartyBean);
					} catch (Exception ex) {
						stringBuilderErrorMessage.append(String.format("Error while creating staging party bean with source system \"%s\" and source party bus id \"%s\"\r\n", lpb.getSourceSystem(), lpb.getSourcePartyBusId()));
						stringBuilderErrorMessage.append(ex.toString() + "\r\n");
					}
				} else if (stagingPartyBeans.size() == 1) {
					stagingPartyBean = stagingPartyBeans.get(0);
					if (lpb.getLastUpdateTimestamp() != null && lpb.getLastUpdateTimestamp() != null && lpb.getLastUpdateTimestamp().compareTo(lpb.getLastUpdateTimestamp()) >= 0) {
						try {
							com.elipslife.ebx.domain.bean.convert.PartyConverter.fillStagingPartyBeanWithLandingPartyBean(lpb, stagingPartyBean);
							stagingPartyWriter.updateBean(stagingPartyBean);
						} catch (Exception ex) {
							stringBuilderErrorMessage.append(String.format("Error while updating staging party bean with source system \"%s\" and source party bus id \"%s\"\r\n", lpb.getSourceSystem(), lpb.getSourcePartyBusId()));
							stringBuilderErrorMessage.append(ex.toString() + "\r\n");
						}
					}
				} else {
					try {
						throw OperationException.createError(String.format("More than one party found with source system '%s', source party bus ID '%s', valid from '%s' and valid to '%s'",
								lpb.getSourceSystem(), lpb.getSourcePartyBusId(), lpb.getValidFrom(), lpb.getValidTo()));
					} catch (OperationException ex) {
						stringBuilderErrorMessage.append(ex.toString() + "\r\n");
					}
				}
			});
		}
		
		// Update error messages in dataContext variables 
		updateErrorMessageInDataContext(stringBuilderErrorMessage, context);
	}

	/**
	 * 
	 * @param dataContext
	 * @return
	 */
	private static HashMap<String, String> getContextVariables(
			com.orchestranetworks.workflow.DataContextReadOnly dataContext) {

		HashMap<String, String> hashMap = new HashMap<String, String>();
		Iterator<String> variableNames = dataContext.getVariableNames();
		while (variableNames.hasNext()) {
			String variableName = variableNames.next();
			String variableValue = dataContext.getVariableString(variableName);
			hashMap.put(variableName, variableValue);
		}

		return hashMap;
	}

	private static void updateStagingParties(List<StagingPartyBean> stagingPartiesListToUpdate, Session session,
			String dataspace) throws OperationException {

		AdaptationHome dataspaceName = getStagingDataSpaceHome();
		Adaptation dataset = getStagingDataSet();
		AdaptationTable aTable = dataset.getTable(PartyPaths._STG_Party.getPathInSchema());

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspaceName);

		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();

		HashMap<Path, Object> values = null;
		RecordValuesBean rvb = null;
		for (StagingPartyBean stagingPartyBean : stagingPartiesListToUpdate) {
			values = new HashMap<Path, Object>();
			values = StagingPartyConverter.fillStagingPartyValuesFromLandingPartyBean(stagingPartyBean, true);
			rvb = new RecordValuesBean(
					aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(stagingPartyBean.getId().toString())),
					values);
			recordValuesBeanList.add(rvb);
		}

		// For Updates
		UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();

			throw exception;
		}
	}

	private static void deleteLandingRecords(List<LandingPartyBulkProcessBean> landingPartiesBulkProcessBeansList,
			Session session, String dataSpace) throws OperationException {

		AdaptationHome dataspaceName = getTemplandingDataspace();
		Adaptation dataset = getLandingDataSet();
		AdaptationTable aTable = dataset.getTable(LandingPaths._LDG_Party_BulkProcess.getPathInSchema());
		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspaceName);

		ArrayList<Adaptation> recordValuesBeanList = new ArrayList<Adaptation>();

		for (LandingPartyBulkProcessBean landingPartyBulkProcessBean : landingPartiesBulkProcessBeansList) {
			recordValuesBeanList.add(aTable.lookupAdaptationByPrimaryKey(
					PrimaryKey.parseString(landingPartyBulkProcessBean.getId().toString())));
		}

		DeleteRecords deleteRecordsProcedure = new DeleteRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(deleteRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}

	private static void createStagingParties(List<RecordValuesBean> stagingPartiesListToCreate, Session session,
			String dataspace) throws OperationException {

		if(currentTimeNow == null) {
			currentTimeNow = LocalDateTime.now();
		}
		
		AdaptationHome dataspaceName = getStagingDataSpaceHome();
		Adaptation dataset = getStagingDataSet();
		AdaptationTable aTable = dataset.getTable(PartyPaths._STG_Party.getPathInSchema());

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspaceName);

		// For Creates
		CreateRecords createRecordsProcedure = new CreateRecords(aTable, stagingPartiesListToCreate);
		ProcedureResult result = service.execute(createRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
		
		totalRecordsCount = totalRecordsCount + stagingPartiesListToCreate.size();
		batchCount++;
		VM.log.kernelInfo(stagingPartiesListToCreate.size()+" records are processed in "+ batchCount);
		VM.log.kernelInfo("Total processed records till now is "+totalRecordsCount);
		LocalDateTime dateTime4 = LocalDateTime.now();
		long diffInNano1 = ChronoUnit.NANOS.between(currentTimeNow, dateTime4);
		long diffInSeconds1 = ChronoUnit.SECONDS.between(currentTimeNow, dateTime4);
		long diffInMilli1 = ChronoUnit.MILLIS.between(currentTimeNow, dateTime4);
		long diffInMinutes1 = ChronoUnit.MINUTES.between(currentTimeNow, dateTime4);
		long diffInHours1 = ChronoUnit.HOURS.between(currentTimeNow, dateTime4);
		VM.log.kernelInfo("Total execution time in Adaptation "+ diffInHours1 + " hours "+diffInMinutes1+ " minutes "+ diffInSeconds1+ " seconds "+ diffInMilli1+ " milliseconds "+ diffInNano1 +" nanoseconds");
		currentTimeNow = LocalDateTime.now();
	}
	
	/**
	 * Trim and Update errorMessages in DataContext variables
	 * 
	 * @param stringBuilderErrorMessage
	 * @param aContext
	 */
	private void updateErrorMessageInDataContext(StringBuilder stringBuilderErrorMessage, ScriptTaskContext aContext) {
		
		if(stringBuilderErrorMessage != null) {
			StringBuffer errorMessage = new StringBuffer();
			String errorMessages = aContext.getVariableString(PartyConstants.DataContextVariables.ERROR_MESSAGES);
			if(errorMessages != null && errorMessages.length() > 2000) {
				String errorMessageString = errorMessages.substring(errorMessages.length() - 2000);
				errorMessage.append(errorMessageString);
				if(stringBuilderErrorMessage.length() > 2000) {
					errorMessage.append(stringBuilderErrorMessage.substring(stringBuilderErrorMessage.length() - 2000));
				} else {
					errorMessage.append(stringBuilderErrorMessage);
				}
				errorMessage.append(" Please look in to kernel.log file to see more errors");
			} else {
				if(errorMessages != null) {
					errorMessage.append(errorMessages);
				}
				if(stringBuilderErrorMessage.length() > 2000) {
					errorMessage.append(stringBuilderErrorMessage.substring(stringBuilderErrorMessage.length() - 2000));
					errorMessage.append(" Please look in to kernel.log file to see more errors");
				} else {
					errorMessage.append(stringBuilderErrorMessage);
				}
			}
			aContext.setVariableString(PartyConstants.DataContextVariables.ERROR_MESSAGES, errorMessage.toString());
		}
	}

	public static AdaptationHome getStagingDataSpaceHome() {
		return stagingDataSpaceHome;
	}

	public void setStagingDataSpaceHome(AdaptationHome stagingDataSpace) {
		stagingDataSpaceHome = stagingDataSpace;
	}

	public static AdaptationHome getTemplandingDataspace() {
		return templandingDataspace;
	}

	public void setTemplandingDataspace(AdaptationHome tempLandingDataspace) {
		templandingDataspace = tempLandingDataspace;
	}

	public static Adaptation getStagingDataSet() {
		return stagingDataSet;
	}

	public void setStagingDataSet(AdaptationHome stagingDataspace) {
		stagingDataSet = stagingDataspace
				.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.PARTY_MASTER));
	}

	public static Adaptation getLandingDataSet() {
		return landingDataSet;
	}

	public void setLandingDataSet(AdaptationHome landingAdaptationHome) {
		landingDataSet = landingAdaptationHome
				.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.LANDING_MASTER));
	}

}
