#!/bin/bash

BASEDIR=$(realpath $(dirname $0)/../..)

echo "Build Image"
az acr login -n butos
mvn clean install -f ${BASEDIR}/pom.xml
mvn deploy -f ${BASEDIR}/deployment/pom.xml

namespace=elipslife-mdm

echo "Create Namespace"
az aks get-credentials -g butos -n mixedAKSCluster
kubectl config use-context mixedAKSCluster
kubectl create namespace ${namespace}
kubectl config set-context ${namespace} --cluster mixedAKSCluster --namespace ${namespace} --user clusterUser_butos_mixedAKSCluster
kubectl config use-context ${namespace}

echo "Create Database"
az sql db create -g butos -s butos-dev \
	--name ${namespace} \
	--collation SQL_Latin1_General_CP1_CS_AS \
	--max-size 500MB \
	-c 5 \
	-e Basic \
	--service-objective Basic

echo "Create ConfigMap"
kubectl delete configmap ebx-config >> /dev/null
kubectl create configmap ebx-config --from-file=${BASEDIR}/deployment/target/dockerfile-context/ebx-home/aks/

echo "Deploy EBX"
kubectl apply -f ${BASEDIR}/deployment/target/k8s-scripts/ebx.yml -n ${namespace}

echo "Wait for deployment to finish"
sleep 10

echo "Copy the project marker file into the container"
container=$(kubectl get pods -l app=ebx -o jsonpath="{.items[*].metadata.name}")
touch .${namespace}
kubectl cp .${namespace} ${container}:/app/ebx-home/repository/.${namespace}
rm .${namespace}

echo "Copy the LDAP Config into the archive"
kubectl exec ${container} -- mkdir -p /app/ebx-home/repository/archives
for archive in ${BASEDIR}/deployment/src/main/resources/ebx-archives/*;
do
  kubectl cp ${archive} ${container}:/app/ebx-home/repository/archives/$(basename ${archive});
done

echo "Install finished."
