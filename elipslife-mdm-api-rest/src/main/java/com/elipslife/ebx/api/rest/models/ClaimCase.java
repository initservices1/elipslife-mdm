package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class ClaimCase {
	
	@JsonbProperty("DataOwner")
	@JsonProperty("DataOwner")
	public String dataOwner;
	
	@JsonbProperty("ClaimCaseBusId")
	@JsonProperty("ClaimCaseBusId")
	public String claimCaseBusId;
	
	@JsonbProperty("ClaimCaseExtRef")
	@JsonProperty("ClaimCaseExtRef")
	public String claimCaseExtRef;
	
	@JsonbProperty("ContractNumber")
	@JsonProperty("ContractNumber")
	public String contractNumber;
	
	@JsonbProperty("ContractExtRef")
	@JsonProperty("ContractExtRef")
	public String contractExtRef;
	
	@JsonbProperty("IsActive")
	@JsonProperty("IsActive")
	public Boolean isActive;
	
	
	//Details
	@JsonbProperty("AssessmentState")
	@JsonProperty("AssessmentState")
	public String assessmentState;
	
	@JsonbProperty("BusinessScope")
	@JsonProperty("BusinessScope")
	public String businessScope;
	
	@JsonbProperty("ClaimCause")
	@JsonProperty("ClaimCause")
	public String claimCause;
	
	@JsonbProperty("ClaimState")
	@JsonProperty("ClaimState")
	public String claimState;
	
	@JsonbProperty("ClaimStateDetail")
	@JsonProperty("ClaimStateDetail")
	public String claimStateDetail;
	
	@JsonbProperty("ClaimType")
	@JsonProperty("ClaimType")
	public String claimType;
	
	@JsonbProperty("ClientSegment")
	@JsonProperty("ClientSegment")
	public String clientSegment;
	
	@JsonbProperty("OperatingCountry")
	@JsonProperty("OperatingCountry")
	public String operatingCountry;
	
	
	//Dates
	@JsonbProperty("BenefitStartDate")
	@JsonProperty("BenefitStartDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate benefitStartDate;
	
	@JsonbProperty("BenefitEndDate")
	@JsonProperty("BenefitEndDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate benefitEndDate;
	
	@JsonbProperty("CancellationDate")
	@JsonProperty("CancellationDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate cancellationDate;
	
	@JsonbProperty("ClaimDate")
	@JsonProperty("ClaimDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate claimDate;
	
	@JsonbProperty("DiagnosisDate")
	@JsonProperty("DiagnosisDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate diagnosisDate;
	
	@JsonbProperty("DisabilityDate")
	@JsonProperty("DisabilityDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate disabilityDate;
	
	@JsonbProperty("HospitalStartDate")
	@JsonProperty("HospitalStartDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate hospitalStartDate;
	
	@JsonbProperty("LastDayWorked")
	@JsonProperty("LastDayWorked")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate lastDayWorked;
	
	@JsonbProperty("ReactivationDate")
	@JsonProperty("ReactivationDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate reactivationDate;
	
	@JsonbProperty("ReportingDate")
	@JsonProperty("ReportingDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate reportingDate;
	
	@JsonbProperty("ReturnToWorkDate")
	@JsonProperty("ReturnToWorkDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate returnToWorkDate;
	
	@JsonbProperty("SymptomsStartDate")
	@JsonProperty("SymptomsStartDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate symptomsStartDate;
	
	@JsonbProperty("TerminationDate")
	@JsonProperty("TerminationDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate terminationDate;
	
	@JsonbProperty("TreatmentStartDate")
	@JsonProperty("TreatmentStartDate")
	@JsonSerialize(using = LocalDateSerializer.class)
	public LocalDate treatmentStartDate;
	
	
	//Parties
	@JsonbProperty("ClaimManager")
	@JsonProperty("ClaimManager")
	public Employee claimManager;
	
	@JsonbProperty("ClaimMgmtTeam")
	@JsonProperty("ClaimMgmtTeam")
	public Party claimMgmtTeam;
	
	@JsonbProperty("MainAffiliate")
	@JsonProperty("MainAffiliate")
	public Party mainAffiliate;
	
	@JsonbProperty("InjuredPerson")
	@JsonProperty("InjuredPerson")
	public Party injuredPerson;
	
	@JsonbProperty("InsuredPersonType")
	@JsonProperty("InsuredPersonType")
	public String insuredPersonType;
	
	// Control
	@JsonbProperty("CreationTimestamp")
	@JsonProperty("CreationTimestamp")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	public LocalDateTime creationTimestamp;
	
	@JsonbProperty("LastUpdateTimestamp")
	@JsonProperty("LastUpdateTimestamp")
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	public LocalDateTime lastUpdateTimestamp;
	
	@JsonbProperty("Action")
	@JsonProperty("Action")
	public String action;
}