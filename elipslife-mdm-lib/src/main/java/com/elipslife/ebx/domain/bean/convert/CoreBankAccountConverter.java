package com.elipslife.ebx.domain.bean.convert;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.CoreBankAccountBean;
import com.elipslife.ebx.domain.bean.StagingBankAccountBean;
import com.elipslife.mdm.path.PartyPaths;
import com.orchestranetworks.schema.Path;

public class CoreBankAccountConverter {

    public static HashMap<Path, Object> fillCoreBankAccountValuesFromStagingBankAccountBean(CoreBankAccountBean bean, boolean useDefaultValues) {

        HashMap<Path, Object> values = new HashMap<Path, Object>();


        values.put(PartyPaths._BankAccount._Account_BankAccountId, bean.getId());

        values.put(PartyPaths._BankAccount._Account_PartyId, bean.getPartyId());
        values.put(PartyPaths._BankAccount._Account_AccountType, bean.getAccountType());
        values.put(PartyPaths._BankAccount._Account_AccountNumber, bean.getAccountNumber());
        values.put(PartyPaths._BankAccount._Account_IBAN, bean.getIban());
        values.put(PartyPaths._BankAccount._Account_BIC, bean.getBic());
        values.put(PartyPaths._BankAccount._Account_ClearingNumber, bean.getClearingNumber());
        values.put(PartyPaths._BankAccount._Account_BankName, bean.getBankName());
        values.put(PartyPaths._BankAccount._Account_Country, bean.getCountry());
        values.put(PartyPaths._BankAccount._Account_Currency, bean.getCurrency());
        values.put(PartyPaths._BankAccount._Account_IsMainAccount, bean.getIsMainAccount());
        values.put(PartyPaths._BankAccount._Account_SourceBankAccountBusId, bean.getSourceBankAccountBusId());
        values.put(PartyPaths._BankAccount._Control_SourceSystem, bean.getSourceSystem());
        values.put(PartyPaths._BankAccount._Control_SourcePartyBusId, bean.getSourcePartyBusId());
        values.put(PartyPaths._BankAccount._Control_CreatedBy, bean.getCreatedBy());
        values.put(PartyPaths._BankAccount._Control_CreationTimestamp, bean.getCreationTimestamp());
        values.put(PartyPaths._BankAccount._Control_LastUpdatedBy, bean.getLastUpdatedBy());
        values.put(PartyPaths._BankAccount._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
        values.put(PartyPaths._BankAccount._Control_LastSyncAction, bean.getLastSyncAction());
        values.put(PartyPaths._BankAccount._Control_LastSyncTimestamp, bean.getLastSyncTimestamp());
        values.put(PartyPaths._BankAccount._Control_ValidFrom, bean.getValidFrom());
        values.put(PartyPaths._BankAccount._Control_ValidTo, bean.getValidTo());

        if (!useDefaultValues || bean.getIsExpired() != null) {
            values.put(PartyPaths._BankAccount._Control_IsExpired, bean.getIsExpired());
        }

        return values;
    }
    
    public static HashMap<Path, Object> fillStagingBankAccountBeanWithCoreankAccountBulkProcessBean(StagingBankAccountBean bean,
        boolean useDefaultValues) {

        
        HashMap<Path, Object> values = new HashMap<Path, Object>();

        values.put(PartyPaths._STG_BankAccount._Account_BankAccountId, bean.getId());

        values.put(PartyPaths._STG_BankAccount._Account_AccountType, bean.getAccountType());
        values.put(PartyPaths._STG_BankAccount._Account_AccountNumber, bean.getAccountNumber());
        values.put(PartyPaths._STG_BankAccount._Account_IBAN, bean.getIban());
        values.put(PartyPaths._STG_BankAccount._Account_BIC, bean.getBic());
        values.put(PartyPaths._STG_BankAccount._Account_ClearingNumber, bean.getClearingNumber());
        values.put(PartyPaths._STG_BankAccount._Account_BankName, bean.getBankName());
        values.put(PartyPaths._STG_BankAccount._Account_Country, bean.getCountry());
        values.put(PartyPaths._STG_BankAccount._Account_Currency, bean.getCurrency());
        values.put(PartyPaths._STG_BankAccount._Account_IsMainAccount, bean.getIsMainAccount());
        values.put(PartyPaths._STG_BankAccount._Account_SourceBankAccountBusId, bean.getSourceBankAccountBusId());
        values.put(PartyPaths._STG_BankAccount._Account_CoreBankAccountId, bean.getCoreBankAccountId());
        values.put(PartyPaths._STG_BankAccount._Control_SourceSystem, bean.getSourceSystem());
        values.put(PartyPaths._STG_BankAccount._Control_SourcePartyBusId, bean.getSourcePartyBusId());
        values.put(PartyPaths._STG_BankAccount._Control_ValidFrom, bean.getValidFrom());
        values.put(PartyPaths._STG_BankAccount._Control_ValidTo, bean.getValidTo());
        values.put(PartyPaths._STG_BankAccount._Control_CreationTimestamp, bean.getCreationTimestamp());
        values.put(PartyPaths._STG_BankAccount._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
        values.put(PartyPaths._STG_BankAccount._Control_Action, bean.getAction());
        values.put(PartyPaths._STG_BankAccount._Control_ActionBy, bean.getActionBy());

        return values;
    }

}
