package com.elipslife.mdm.party.trigger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.StringUtils;

import com.elipslife.ebx.data.access.CorePartyIdentifierReader;
import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.mdm.party.PartyFunction;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.party.constants.PartyConstants.Entities.Party;
import com.elipslife.mdm.party.constants.PartyConstants.Entities.Party.Delimiter;
import com.elipslife.mdm.path.PartyPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.schema.trigger.ValueChange;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

public class PartyTrigger extends TableTrigger {

    @Override
    public void setup(TriggerSetupContext arg0) {}

    @Override
    public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {

        ValueContextForUpdate vcf = aContext.getOccurrenceContextForUpdate();

//		Object pkId = vcf.getValue(PartyPaths._Party._PartyInfo_PartyId);
//		pkId = aContext.getOccurrenceContext().getValue(PartyPaths._Party._PartyInfo_PartyId);

        String partyName = getPartyName(vcf);

        vcf.setValueEnablingPrivilegeForNode(partyName, PartyPaths._Party._PartyInfo_PartyName);

        vcf.setValueEnablingPrivilegeForNode(PartyFunction.standardizeSearchText(partyName), PartyPaths._Party._PartyInfo_PartyNameSearchText);

        String lineOfBusiness = String.valueOf(vcf.getValue(PartyPaths._Party._Role_LineOfBusiness));
        String roles = String.valueOf(vcf.getValue(PartyPaths._Party._Role_Roles));

        this.deriveLineOfBusinessRolesForParty(vcf, lineOfBusiness, Delimiter.LINE_OF_BUSINESS);
        this.deriveRolesForParty(vcf, roles, Delimiter.ROLES);

        this.setBusinessPartyLink(vcf, PartyPaths._Party._Organisation_SourceParentBusId, PartyPaths._Party._Organisation_ParentId);

        this.setBusinessPartyLink(vcf, PartyPaths._Party._ContactPerson_SourceOrganisationBusId, PartyPaths._Party._ContactPerson_OrganisationId);
        this.setBusinessPartyLink(vcf, PartyPaths._Party._ContactPerson_SourcePartnerBusId, PartyPaths._Party._ContactPerson_PartnerId);
        this.setBusinessPartyLink(vcf, PartyPaths._Party._ContactPerson_SourceLineManagerBusId, PartyPaths._Party._ContactPerson_LineManagerId);

        this.updateValue(vcf);
    }

    @Override
    public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {

        ValueContextForUpdate vcf = aContext.getOccurrenceContextForUpdate();

        String partyName = getPartyName(vcf);

        vcf.setValueEnablingPrivilegeForNode(partyName, PartyPaths._Party._PartyInfo_PartyName);

        vcf.setValueEnablingPrivilegeForNode(PartyFunction.standardizeSearchText(partyName), PartyPaths._Party._PartyInfo_PartyNameSearchText);

        String lineOfBusiness = String.valueOf(vcf.getValue(PartyPaths._Party._Role_LineOfBusiness));
        String roles = String.valueOf(vcf.getValue(PartyPaths._Party._Role_Roles));

        this.deriveLineOfBusinessRolesForParty(vcf, lineOfBusiness, Delimiter.LINE_OF_BUSINESS);
        this.deriveRolesForParty(vcf, roles, Delimiter.ROLES);
        if (Objects.equals(PartyConstants.DataSpace.PARTY_REFERENCE, aContext.getAdaptationHome().getKey().getName())) {
        	String sourceParentBusId = String.valueOf(vcf.getValue(PartyPaths._Party._Organisation_SourceParentBusId));
        	if(sourceParentBusId != null) {
        		this.setBusinessPartyLink(vcf, PartyPaths._Party._Organisation_SourceParentBusId, PartyPaths._Party._Organisation_ParentId);
        	}
        	this.setBusinessPartyLink(vcf, PartyPaths._Party._ContactPerson_SourceOrganisationBusId, PartyPaths._Party._ContactPerson_OrganisationId);
            this.setBusinessPartyLink(vcf, PartyPaths._Party._ContactPerson_SourcePartnerBusId, PartyPaths._Party._ContactPerson_PartnerId);
            this.setBusinessPartyLink(vcf, PartyPaths._Party._ContactPerson_SourceLineManagerBusId, PartyPaths._Party._ContactPerson_LineManagerId);
        }

        this.updateValue(vcf);
    }

    @Override
    public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {
    	
    	ValueContextForUpdate contextForUpdate =
                aContext.getProcedureContext().getContext(aContext.getAdaptationOccurrence().getAdaptationName());

        if (!Objects.equals(PartyConstants.DataSpace.PARTY_REFERENCE, aContext.getAdaptationHome().getKey().getName())) {
            String sourceSystem = (String) aContext.getOccurrenceContext().getValue(PartyPaths._Party._Control_SourceSystem);
            String partyBusId = (String) aContext.getOccurrenceContext().getValue(PartyPaths._Party._Control_SourcePartyBusId);
            String partyId = String.valueOf(aContext.getOccurrenceContext().getValue(PartyPaths._Party._PartyInfo_PartyId));
            
            if (Objects.equals(sourceSystem, PartyConstants.SourceSystems.CRM)) {
                contextForUpdate.setValue(partyBusId, PartyPaths._Party._PartyInfo_PartyBusId);
            } else {
                contextForUpdate.setValue(partyId ,
                    PartyPaths._Party._PartyInfo_PartyBusId);
            }
            aContext.getProcedureContext().doModifyContent(aContext.getAdaptationOccurrence(), contextForUpdate);
        } else {
        	String partyType = String.valueOf(aContext.getOccurrenceContext().getValue(PartyPaths._Party._PartyInfo_PartyType));
        	if(partyType.equals(PartyConstants.Entities.Party.PartyType.COMPANY) || partyType.equals(PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT)) {
        		String sourceParentBusId = String.valueOf(aContext.getOccurrenceContext().getValue(PartyPaths._Party._Organisation_SourceParentBusId));
        		String parentId = String.valueOf(aContext.getOccurrenceContext().getValue(PartyPaths._Party._Organisation_ParentId));
        		if(sourceParentBusId != null && (parentId == null || parentId.isEmpty())) {
        			this.setBusinessPartyLink(contextForUpdate, PartyPaths._Party._Organisation_SourceParentBusId, PartyPaths._Party._Organisation_ParentId);
        		}
        	}
        }
    }
    
    @Override
    public void handleAfterModify(AfterModifyOccurrenceContext aContext) throws OperationException {
    	super.handleAfterModify(aContext);
    	
    	ValueChange valueChange = aContext.getChanges().getChange(PartyPaths._Party._PartyInfo_LegalPostalAddress);
    	if(valueChange != null) {
    		RequestResult partyAddresses = aContext.getAdaptationOccurrence().getSchemaNode().getNode(PartyPaths._Party._Addresses).getAssociationLink().getAssociationResult(aContext.getAdaptationOccurrence());
    		List<Adaptation> partyAddressesList = new ArrayList<Adaptation>();
    		for(Adaptation partyAddress; (partyAddress = partyAddresses.nextAdaptation()) != null;) {
    			partyAddressesList.add(partyAddress);
    		}
    		
    		List<Adaptation> crmList = partyAddressesList.stream().filter(corePartyRecord -> isCrmLegalPostalAddress(corePartyRecord)).collect(Collectors.toList());
			if(!crmList.isEmpty()) {
				Adaptation legalAddress = crmList.get(0);
				int addressId = legalAddress.get_int(PartyPaths._Address._AddressInfo_AddressId);
				updateRecord(aContext.getProcedureContext(), aContext.getAdaptationOccurrence(), PartyPaths._Party._PartyInfo_LegalPostalAddress, addressId);
			} else {
				List<Adaptation> nonCRMList = partyAddressesList.stream().filter(corePartyRecord -> isLegalPostalAddress(corePartyRecord)).collect(Collectors.toList());
				if(!nonCRMList.isEmpty()) {
					Adaptation nonCrmRecord = nonCRMList.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getDate(PartyPaths._Address._Control_LastUpdateTimestamp),
							a.getDate(PartyPaths._Address._Control_LastUpdateTimestamp)))
					.findFirst().get();
					int addressId = nonCrmRecord.get_int(PartyPaths._Address._AddressInfo_AddressId);
					updateRecord(aContext.getProcedureContext(), aContext.getAdaptationOccurrence(), PartyPaths._Party._PartyInfo_LegalPostalAddress, addressId);
				}
			}
    	}
    }
    
    private boolean isCrmLegalPostalAddress(Adaptation record) {
    	
    	return StringUtils.equals("CRM", record.getString(PartyPaths._Address._Control_SourceSystem)) &&
    			StringUtils.equals("1", record.getString(PartyPaths._Address._AddressInfo_AddressUse)) &&
    			StringUtils.equals("P", record.getString(PartyPaths._Address._AddressInfo_AddressType));
    	
    }
    
    private boolean isLegalPostalAddress(Adaptation record) {
    	
    	return 	StringUtils.equals("1", record.getString(PartyPaths._Address._AddressInfo_AddressUse)) &&
    			StringUtils.equals("P", record.getString(PartyPaths._Address._AddressInfo_AddressType));
    	
    }
    
    private void updateRecord(ProcedureContext procedureContext, Adaptation record, Path legalAddressIdPath, int value) throws OperationException {
        ValueContextForUpdate contextForUpdate = procedureContext.getContext(record.getAdaptationName());
        contextForUpdate.setValue(String.valueOf(value), legalAddressIdPath);
        procedureContext.doModifyContent(record, contextForUpdate);
    }
    
    private void updateValue(final ValueContextForUpdate vcf) throws OperationException {

        vcf.setValueEnablingPrivilegeForNode(new Date(), PartyPaths._Party._Control_LastSyncTimestamp);
    }

    private ValueContextForUpdate deriveRolesForParty(ValueContextForUpdate vcf, String roles, String delimiter) {

        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsElipsLifeLegalEntity);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsPolicyholder);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsReCoInsurer);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsVendor);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsMedicalServiceProvider);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsStateRegulatoryAuthority);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsDistributionPartner);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsBroker);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsAssociation);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsBank);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsConsultant);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsTPA);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsInsurance);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsInsuredPerson);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsInjuredPerson);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsBeneficiary);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsVIPClient);
        vcf.setValueEnablingPrivilegeForNode(false, PartyPaths._Party._Role_IsBusinessPartner);

        if (roles != null && !roles.isEmpty()) {

            List<String> listOfRoles = Arrays.asList(roles.split(delimiter));

            for (String role : listOfRoles) {

                switch (role.trim()) {

                    case Party.PartyRole.ELIPSLIFE_LEGALENTITY:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsElipsLifeLegalEntity);
                        break;

                    case Party.PartyRole.POLICY_HOLDER:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsPolicyholder);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        break;

                    case Party.PartyRole.RE_COINSURER:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsReCoInsurer);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        break;

                    case Party.PartyRole.VENDOR:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsVendor);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        break;

                    case Party.PartyRole.STATE_REGULATORY_AUTHORITY:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsStateRegulatoryAuthority);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        break;

                    case Party.PartyRole.DISTRIBUTION_PARTNER:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsDistributionPartner);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        break;

                    case Party.PartyRole.BROKER:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBroker);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsDistributionPartner);
                        break;

                    case Party.PartyRole.ASSOCIATION:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsAssociation);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsDistributionPartner);
                        break;

                    case Party.PartyRole.BANK:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBank);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsDistributionPartner);
                        break;

                    case Party.PartyRole.CONSULLTANT:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsConsultant);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsDistributionPartner);
                        break;

                    case Party.PartyRole.TPA:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsTPA);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsDistributionPartner);
                        break;

                    case Party.PartyRole.INSURANCE:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsInsurance);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsDistributionPartner);
                        break;

                    case Party.PartyRole.INSURED_PERSON:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsInsuredPerson);
                        break;
                    
                    case Party.PartyRole.INJURED_PERSON:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsInjuredPerson);
                        break;

                    case Party.PartyRole.BENEFICIARY:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBeneficiary);
                        break;

                    case Party.PartyRole.MEDICAL_SERVICE_PROVIDER:
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsMedicalServiceProvider);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsBusinessPartner);
                        vcf.setValueEnablingPrivilegeForNode(true, PartyPaths._Party._Role_IsVendor);
                        break;
                }
            }
        }

        return vcf;
    }

    private ValueContextForUpdate deriveLineOfBusinessRolesForParty(ValueContextForUpdate vcf, String linneOfBusRoles, String delimiter) {

        vcf.setValueEnablingPrivilegeForNode(false,
            PartyPaths._Party._Role_IsLHLOB);
        vcf.setValueEnablingPrivilegeForNode(false,
            PartyPaths._Party._Role_IsAHLOB);

        if (linneOfBusRoles != null && !linneOfBusRoles.trim().isEmpty()) {

            List<String> listOfLOBRoles = Arrays.asList(linneOfBusRoles.split(delimiter));

            for (String lobRole : listOfLOBRoles) {


                switch (lobRole.trim()) {

                    case Party.LineOfBusiness.LHLOB:
                        vcf.setValueEnablingPrivilegeForNode(true,
                            PartyPaths._Party._Role_IsLHLOB);
                        break;

                    case Party.LineOfBusiness.AHLOB:
                        vcf.setValueEnablingPrivilegeForNode(true,
                            PartyPaths._Party._Role_IsAHLOB);
                        break;
                }

            }
        }

        return vcf;
    }

    /**
     * Links business party by source business ID
     * 
     * @param valueContextForUpdate
     * @param sourceBusinessIdPath
     * @param linkedPartyIdPath
     */
    private void setBusinessPartyLink(ValueContextForUpdate valueContextForUpdate, Path sourceBusinessIdPath, Path linkedPartyIdPath) {

        CorePartyIdentifierReader corePartyIdentifierReader = new CorePartyIdentifierReader();
        String sourceSystem = (String) valueContextForUpdate.getValue(PartyPaths._Party._Control_SourceSystem);
        String sourceOrganisationParentBusinessId = (String) valueContextForUpdate.getValue(sourceBusinessIdPath);

        List<CorePartyIdentifierBean> corePartyIdentifierBeans =
            corePartyIdentifierReader.getBeansBySourceSystem(sourceSystem, sourceOrganisationParentBusinessId);
        CorePartyIdentifierBean corePartyIdentifierBean = corePartyIdentifierBeans.size() > 0 ? corePartyIdentifierBeans.get(0) : null;
        String parentOrgansationPartyId =
            (corePartyIdentifierBean == null || corePartyIdentifierBean.getPartyId() == null || corePartyIdentifierBean.getPartyId().isEmpty()) ? null
                : corePartyIdentifierBean.getPartyId();

        valueContextForUpdate.setValueEnablingPrivilegeForNode(parentOrgansationPartyId, linkedPartyIdPath);
    }

    private String getPartyName(ValueContextForUpdate partyRecord) {

        String partyName = "";

        String partyType = String.valueOf(partyRecord.getValue(PartyPaths._Party._PartyInfo_PartyType));

        switch (partyType) {

            case Party.PartyType.INDIVIDUAL:
            case Party.PartyType.CONTACT:

                String lastName = String.valueOf(partyRecord.getValue(PartyPaths._Party._Person_LastName));
                String firstName = String.valueOf(partyRecord.getValue(PartyPaths._Party._Person_FirstName));

                lastName = lastName != null ? lastName : "";
                firstName = firstName != null ? firstName : "";

                partyName = firstName.concat(" ").concat(lastName);

                break;

            case Party.PartyType.COMPANY:
            case Party.PartyType.ORGANISATION_UNIT:

                //For Person Information//
                partyName = String.valueOf(partyRecord.getValue(PartyPaths._Party._Organisation_OrgName));
                break;

        }

        partyName = partyName == null || Objects.equals(partyName, "null") ? "" : partyName; //partyName != null ? partyName : "";

        return partyName;
    }
}
