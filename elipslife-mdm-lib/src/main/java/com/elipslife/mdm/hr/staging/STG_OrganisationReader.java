package com.elipslife.mdm.hr.staging;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class STG_OrganisationReader extends AbstractTableReader<STG_OrganisationBean> {

	public STG_OrganisationReader() {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Organisation.getPathInSchema()));
	}
	
	public STG_OrganisationReader(String dataspace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Organisation.getPathInSchema()));
	}

	public STG_OrganisationReader(AdaptationTable table) {
		super(table);
	}

	@Override
	public STG_OrganisationBean createBeanFromRecord(Adaptation record) {
		STG_OrganisationBean bean = new STG_OrganisationBean();
		bean.setOrganisationId(record.get_int(HrModelPaths._Organisation._Organisation_OrganisationId));
		bean.setDataOwner(record.getString(HrModelPaths._STG_Organisation._Organisation_DataOwner));
		bean.setOrganisationBusId(record.getString(HrModelPaths._STG_Organisation._Organisation_OrganisationBusId));
		bean.setOrganisationType(record.getString(HrModelPaths._STG_Organisation._Organisation_OrganisationType));
		bean.setParentBusId(record.getString(HrModelPaths._STG_Organisation._Organisation_ParentBusId));
		bean.setName(record.getString(HrModelPaths._STG_Organisation._Organisation_Name));
		bean.setManagerBusId(record.getString(HrModelPaths._STG_Organisation._Organisation_ManagerBusId));
		bean.setBuildingBusId(record.getString(HrModelPaths._STG_Organisation._Organisation_BuildingBusId));
		bean.setIsActive(record.get_boolean(HrModelPaths._STG_Organisation._Organisation_IsActive));
		//bean.setOrganisationMaster(record.getString(HrModelPaths._STG_Organisation._Organisation_OrganisationMaster));
		bean.setSourceSystem(record.getString(HrModelPaths._STG_Organisation._Control_SourceSystem));
        bean.setCreatedBy(record.getString(HrModelPaths._STG_Organisation._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(HrModelPaths._STG_Organisation._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(HrModelPaths._STG_Organisation._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(HrModelPaths._STG_Organisation._Control_LastUpdateTimestamp));
        bean.setLastSyncTimestamp(record.getDate(HrModelPaths._STG_Organisation._Control_LastSyncTimestamp));
		return bean;
	}

	@Override
	public Path[] getPkPaths() {
		return new Path[] { HrModelPaths._Organisation._Organisation_OrganisationId };
	}

	@Override
	public Object[] retrievePks(STG_OrganisationBean bean) {
		return new Object[] { bean.getOrganisationId() };
	}
	
	/**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}

	
}
