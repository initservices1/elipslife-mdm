package com.elipslife.mdm.uw.core;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.UWPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class UWCaseWriter extends AbstractTableWriter<UWCaseBean> {
    private UWCaseReader reader;

    public UWCaseWriter() {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._UWCase.getPathInSchema()));
    }

    public UWCaseWriter(String dataspace) {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._UWCase.getPathInSchema()));
    }

    public UWCaseWriter(AdaptationTable table) {
        super(table);
        reader = new UWCaseReader(table);
    }

    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, UWCaseBean bean) {
        values.put(UWPaths._UWCase._UWCaseId, bean.getUWCaseId());
        values.put(UWPaths._UWCase._UWCaseBusId, bean.getUWCaseBusId());
        values.put(UWPaths._UWCase._Context_SourceSystem, bean.getSourceSystem());
        values.put(UWPaths._UWCase._Context_DataOwner, bean.getDataOwner());
        values.put(UWPaths._UWCase._Context_UWApplicationId, bean.getUWApplicationId());
        values.put(UWPaths._UWCase._Context_UWApplicationBusId, bean.getUWApplicationBusId());
        values.put(UWPaths._UWCase._Context_InsuranceContractId, bean.getInsuranceContractId());
        values.put(UWPaths._UWCase._Context_ContractNumber, bean.getContractNumber());
        values.put(UWPaths._UWCase._Applicant_ApplicantId, bean.getApplicantId());
        values.put(UWPaths._UWCase._Applicant_ApplicantBusId, bean.getApplicantBusId());
        values.put(UWPaths._UWCase._Applicant_ApplicantRelationship, bean.getApplicantRelationship());
        values.put(UWPaths._UWCase._Applicant_ApplicantFirstName, bean.getApplicantFirstName());
        values.put(UWPaths._UWCase._Applicant_ApplicantLastName, bean.getApplicantLastName());
        values.put(UWPaths._UWCase._Applicant_ApplicantDateOfBirth, bean.getApplicantDateOfBirth());
        values.put(UWPaths._UWCase._Applicant_ApplicantSSN, bean.getApplicantSSN());
        values.put(UWPaths._UWCase._Applicant_ApplicantTown, bean.getApplicantTown());
        values.put(UWPaths._UWCase._Applicant_ApplicantPostCode, bean.getApplicantPostCode());
        values.put(UWPaths._UWCase._Applicant_ApplicantCountry, bean.getApplicantCountry());
        values.put(UWPaths._UWCase._Details_ApplicantClass, bean.getApplicantClass());
        values.put(UWPaths._UWCase._Details_ProductType, bean.getProductType());
        values.put(UWPaths._UWCase._Details_EligiblityDate, bean.getEligiblityDate());
        values.put(UWPaths._UWCase._Details_ApplicationReason, bean.getApplicationReason());
        values.put(UWPaths._UWCase._Details_ApplicationReasonText, bean.getApplicationReasonText());
        values.put(UWPaths._UWCase._Details_InforceCoverage, bean.getInforceCoverage());
        values.put(UWPaths._UWCase._Details_AdditionalCoverage, bean.getAdditionalCoverage());
        values.put(UWPaths._UWCase._State_IsActive, bean.getIsActive());
        values.put(UWPaths._UWCase._State_CaseState, bean.getCaseState());
        values.put(UWPaths._UWCase._Control_CreatedBy, bean.getCreatedBy());
        values.put(UWPaths._UWCase._Control_CreationTimestamp, bean.getCreationTimeStamp());
        values.put(UWPaths._UWCase._Control_LastUpdatedBy, bean.getLastUpdatedBy());
        values.put(UWPaths._UWCase._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
        values.put(UWPaths._UWCase._Control_DeletionTimestamp, bean.getDeletionTimeStamp());
        values.put(UWPaths._UWCase._Control_LastSyncAction, bean.getLastSyncAction());
    }

    @Override
    public AbstractTableReader<UWCaseBean> getTableReader() {
        return reader;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub
		
	}
}
