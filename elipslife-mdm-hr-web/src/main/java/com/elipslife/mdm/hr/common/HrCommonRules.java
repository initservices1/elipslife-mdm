package com.elipslife.mdm.hr.common;

import java.util.List;
import java.util.Optional;

import com.elipslife.ebx.data.access.CorePartyReader;
import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.ebx.domain.bean.convert.HrConverter;
import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.elipslife.mdm.hr.core.BuildingBean;
import com.elipslife.mdm.hr.core.BuildingReader;
import com.elipslife.mdm.hr.core.BuildingWriter;
import com.elipslife.mdm.hr.core.BuildingWriterSession;
import com.elipslife.mdm.hr.core.EmployeeBean;
import com.elipslife.mdm.hr.core.EmployeeReader;
import com.elipslife.mdm.hr.core.EmployeeWriter;
import com.elipslife.mdm.hr.core.EmployeeWriterSession;
import com.elipslife.mdm.hr.core.OrganisationBean;
import com.elipslife.mdm.hr.core.OrganisationReader;
import com.elipslife.mdm.hr.core.OrganisationWriter;
import com.elipslife.mdm.hr.core.OrganisationWriterSession;
import com.elipslife.mdm.hr.landing.LandingBuildingBean;
import com.elipslife.mdm.hr.landing.LandingBuildingReader;
import com.elipslife.mdm.hr.landing.LandingBuildingWriter;
import com.elipslife.mdm.hr.landing.LandingBuildingWriterSession;
import com.elipslife.mdm.hr.landing.LandingEmployeeBean;
import com.elipslife.mdm.hr.landing.LandingEmployeeReader;
import com.elipslife.mdm.hr.landing.LandingEmployeeWriter;
import com.elipslife.mdm.hr.landing.LandingEmployeeWriterSession;
import com.elipslife.mdm.hr.landing.LandingOrganisationBean;
import com.elipslife.mdm.hr.landing.LandingOrganisationReader;
import com.elipslife.mdm.hr.landing.LandingOrganisationWriter;
import com.elipslife.mdm.hr.landing.LandingOrganisationWriterSession;
import com.elipslife.mdm.hr.staging.STG_BuildingBean;
import com.elipslife.mdm.hr.staging.STG_BuildingWriter;
import com.elipslife.mdm.hr.staging.STG_BuildingWriterSession;
import com.elipslife.mdm.hr.staging.STG_EmployeeBean;
import com.elipslife.mdm.hr.staging.STG_EmployeeWriter;
import com.elipslife.mdm.hr.staging.STG_EmployeeWriterSession;
import com.elipslife.mdm.hr.staging.STG_OrganisationBean;
import com.elipslife.mdm.hr.staging.STG_OrganisationWriter;
import com.elipslife.mdm.hr.staging.STG_OrganisationWriterSession;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.XPathFilter;
import com.onwbp.boot.VM;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.TableTriggerExecutionContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class HrCommonRules {

	public static String xpathFilterParam1 = "value1";
	public static String xpathFilterParam2 = "value2";

	/*
	 * Migrate employee records from Landing to staging
	 */
	public static void migrateEmployeeRecordsFromLandingToStaging(ScriptTaskBeanContext aContext,
			String tempDataspace) {

		LandingEmployeeWriter landingEmployeeWriter = new LandingEmployeeWriterSession(aContext.getSession());
		LandingEmployeeReader landingEmployeeReader = (LandingEmployeeReader) landingEmployeeWriter.getTableReader();
		STG_EmployeeWriter EmployeeWriter = new STG_EmployeeWriterSession(aContext.getSession(), tempDataspace);
		List<LandingEmployeeBean> landingEmployeeBean = landingEmployeeReader.readAllBeans();

		landingEmployeeBean.forEach(lob -> {
			STG_EmployeeBean stagingEmployeeBean = HrConverter.createStagingEmployeeBeanFromLandingEmployeeBean(lob);
			EmployeeWriter.createBeanOrThrow(stagingEmployeeBean);

		});
	}

	/*
	 * Migrate Organisation records from Landing to staging
	 * 
	 */
	public static void migrateOrganisationRecordsFromLandingToStaging(ScriptTaskBeanContext aContext,
			String tempDataspace) {

		LandingOrganisationWriter landingWriter = new LandingOrganisationWriterSession(aContext.getSession());
		LandingOrganisationReader landingOrganisationReader = (LandingOrganisationReader) landingWriter
				.getTableReader();
		STG_OrganisationWriter organisationWriter = new STG_OrganisationWriterSession(aContext.getSession(),
				tempDataspace);
		List<LandingOrganisationBean> landingOrganisationBean = landingOrganisationReader.readAllBeans();

		landingOrganisationBean.forEach(lob -> {

			STG_OrganisationBean stagingOrganisationBean = HrConverter
					.createStagingOrganisationBeanFromLandingOrganisationBean(lob);
			organisationWriter.createBeanOrThrow(stagingOrganisationBean);

		});
	}

	/*
	 * Migrate Building records from landing to staging
	 * 
	 */
	public static void migrateBuildingRecordsFromLandingToStaging(ScriptTaskBeanContext aContext,
			String tempDataspace) {

		LandingBuildingWriter landingWriter = new LandingBuildingWriterSession(aContext.getSession());
		LandingBuildingReader landingBuildingReader = (LandingBuildingReader) landingWriter.getTableReader();
		STG_BuildingWriter buildingWriter = new STG_BuildingWriterSession(aContext.getSession(), tempDataspace);
		List<LandingBuildingBean> landingBuildingBean = landingBuildingReader.readAllBeans();

		landingBuildingBean.forEach(lbb -> {
			STG_BuildingBean stagingBuildingBean = HrConverter.createStagingBuildingBeanFromLandingBuildingBean(lbb);
			buildingWriter.createBeanOrThrow(stagingBuildingBean);
		});
	}

	/*
	 * Create/Update building records in Staging area
	 * 
	 */
	public static void createOrUpdateCoreBuildingRecordByStagingRecord(Session session,
			STG_BuildingWriter stgBuildingWriter, String dataspace, STG_BuildingBean stgBuildingBean)
			throws OperationException {

		BuildingWriter buildingWriter = new BuildingWriterSession(session, dataspace);
		BuildingReader buildingReader = (BuildingReader) buildingWriter.getTableReader();

		List<BuildingBean> buildingBeans = buildingReader.findBeansByBuildingId(stgBuildingBean.getBuildingBusId());
		if (buildingBeans.size() > 1) {
			throw new RuntimeException(String.format("More than one Building record found with Buidling bus id",
					stgBuildingBean.getBuildingBusId()));
		}

		BuildingBean buildingBean = buildingBeans.isEmpty() ? null : buildingBeans.get(0);
		if (buildingBean != null) {
			// Update 
			boolean updateBuildingBean = DateNullableComparator.getInstance()
					.compare(stgBuildingBean.getLastUpdateTimeStamp(), buildingBean.getLastUpdateTimeStamp()) > 0;
					
			if (!updateBuildingBean) {
				stgBuildingBean.setBuildingMaster(buildingBean.getBuildingId());
				stgBuildingWriter.updateBean(stgBuildingBean);
				VM.log.kernelWarn("***************************************************************");
				VM.log.kernelWarn("Ignoring Record Update To Building Master for Building Stage Record : " +stgBuildingBean.getBuildingBusId());
				return;
			}

			HrConverter.fillBuildingBeanWithStagingBuildingBean(buildingBean, stgBuildingBean);
			buildingWriter.updateBean(buildingBean);
			stgBuildingBean.setBuildingMaster(buildingBean.getBuildingId());
			stgBuildingWriter.updateBean(stgBuildingBean);
		} else {
			// Create
			buildingBean = HrConverter.fillBuildingBeanWithStagingBuildingBean(stgBuildingBean);
			Optional<BuildingBean> coreBuildingBean = buildingWriter.createBeanOrThrow(buildingBean);
			stgBuildingBean.setBuildingMaster(String.valueOf(coreBuildingBean.get().getBuildingId()));
			stgBuildingWriter.updateBean(stgBuildingBean);
		}

	}

	/*
	 * Create/Update Organisation records in Staging area
	 * 
	 */
	public static void createOrUpdateCoreOrganisationRecordByStagingRecord(Session session,
			STG_OrganisationWriter stagingOrganisationWriter, String dataspace,
			STG_OrganisationBean stgOrganisationBean) throws OperationException {

		OrganisationWriter oranisationWriter = new OrganisationWriterSession(session, dataspace);
		OrganisationReader oranisationReader = (OrganisationReader) oranisationWriter.getTableReader();
		Optional<OrganisationBean> createdBeanOptional = null;

		List<OrganisationBean> oranisationBeans = oranisationReader
				.findBeansByOrganisationBusIdAndName(stgOrganisationBean.getOrganisationBusId(), stgOrganisationBean.getName());
		if (oranisationBeans.size() > 1) {
			throw new RuntimeException(String.format("More than one Oranisation record found with Organisation bus id and Name",
					stgOrganisationBean.getOrganisationBusId()));
		}
		
		OrganisationBean organisationBean = oranisationBeans.isEmpty() ? null : oranisationBeans.get(0);
		
		if(organisationBean != null) {
			// Update
			boolean updateoranisationBean = DateNullableComparator.getInstance().compare(
					stgOrganisationBean.getLastUpdateTimeStamp(), organisationBean.getLastUpdateTimeStamp()) > 0;
			if(!updateoranisationBean)
			{
				stgOrganisationBean.setOrganisationMaster(organisationBean.getOrganisationId());
				stagingOrganisationWriter.updateBean(stgOrganisationBean);
				VM.log.kernelWarn("***************************************************************");
				VM.log.kernelWarn("Ignoring Staging Organisation record update to Organisation Master : " +stgOrganisationBean.getOrganisationBusId());
				return;
			}
			HrConverter.fillOrganisationBeanWithStagingOrganisationBean(organisationBean, stgOrganisationBean);
			oranisationWriter.updateBean(organisationBean);
			stgOrganisationBean.setOrganisationMaster(organisationBean.getOrganisationId());
			stagingOrganisationWriter.updateBean(stgOrganisationBean);
			
		} else {
			// Create
			OrganisationBean oranisationBean = HrConverter.fillOrganisationBeanWithStagingOrganisationBean(stgOrganisationBean);
			createdBeanOptional = oranisationWriter.createBeanOrThrow(oranisationBean);
			stgOrganisationBean.setOrganisationMaster(String.valueOf(createdBeanOptional.get().getOrganisationId()));
			stagingOrganisationWriter.updateBean(stgOrganisationBean);
		}

	}

	/*
	 * Create/Update employee records in Staging area
	 * 
	 */
	public static void createOrUpdateCoreEmployeeRecordByStagingRecord(Session session,
			STG_EmployeeWriter stgEmployeeWriter, String dataspace, STG_EmployeeBean stgEmployeeBean)
			throws OperationException {

		EmployeeWriter employeeWriter = new EmployeeWriterSession(session, dataspace);
		EmployeeReader employeeReader = (EmployeeReader) employeeWriter.getTableReader();
		Optional<EmployeeBean> createdBeanOptional = null;

		List<EmployeeBean> employeeBeans = employeeReader.findBeansByEmployeeBusIdAndUserName(stgEmployeeBean.getUserName(), stgEmployeeBean.getEmployeeBusId());
		
		if (employeeBeans.size() > 1) {
			throw new RuntimeException(String.format("More than one Employee record found with Employee bus id and Username",
					stgEmployeeBean.getEmployeeBusId()));
		}
		
		EmployeeBean employeeBean = employeeBeans.isEmpty() ? null : employeeBeans.get(0);
		if(employeeBean != null)
		{
			// Update
			boolean updateEmployeeBean = DateNullableComparator.getInstance()
							.compare(stgEmployeeBean.getLastUpdateTimeStamp(), employeeBean.getLastUpdateTimeStamp()) > 0;
			if (!updateEmployeeBean) {
				VM.log.kernelWarn("***************************************************************");
				VM.log.kernelWarn("Ignoring Staging Employee record update to Employee Master : " +stgEmployeeBean.getEmployeeBusId());
				stgEmployeeBean.setEmployeeMaster(employeeBean.getEmployeeId());
				stgEmployeeWriter.updateBean(stgEmployeeBean);
				return;
			}
			HrConverter.fillEmployeeBeanWithStagingEmployeeBean(employeeBean, stgEmployeeBean);
			employeeWriter.updateBean(employeeBean);
			stgEmployeeBean.setEmployeeMaster(employeeBean.getEmployeeId());
			stgEmployeeWriter.updateBean(stgEmployeeBean);
		
		} else {
			// Create
			employeeBean = HrConverter.fillEmployeeBeanWithStagingEmployeeBean(stgEmployeeBean);
			createdBeanOptional = employeeWriter.createBeanOrThrow(employeeBean);
			stgEmployeeBean.setEmployeeMaster(String.valueOf(createdBeanOptional.get().getEmployeeId()));
			stgEmployeeWriter.updateBean(stgEmployeeBean);
		}
	}

	/*
	 * Get PartyId from Party
	 */
	private static String getPartyIdByPartyBusId(String partyBusId) {
		
		CorePartyReader corePartyReader = new CorePartyReader();
		List<CorePartyBean> corePartyReaders = corePartyReader.findPartyIdByPartyBusId(partyBusId);
		CorePartyBean corePartyBean = corePartyReaders.size() > 0 ? corePartyReaders.get(0) : null;
		
		String partyId = corePartyBean != null ? String.valueOf(corePartyBean.getId()) : null;
		return partyId;

	}

	/*
	 * set Partyid
	 * 
	 */
	public static void setPartyIdByPartyBusId(TableTriggerExecutionContext aContext, ValueContextForUpdate valueContext,
			String partyBusId) {

		String partyId = getPartyIdByPartyBusId(partyBusId);
		if (partyId != null) {
			valueContext.setValue(partyId, HrModelPaths._Employee._EmployerCompany_EmployerId);
		}

	}

	/*
	 * Set OrganisationId
	 */
	public static void setOrganisationIdByParentBusId(TableTriggerExecutionContext aContext,
			ValueContextForUpdate valueContext, Path fieldPath, String orgBusId) {

		AdaptationTable aTable = aContext.getTable();
		if (!aTable.getTablePath().equals(HrModelPaths._Organisation.getPathInSchema())) {
			Adaptation dataset = aContext.getAdaptationHome()
					.findAdaptationOrNull(AdaptationName.forName(Constants.DataSet.HR_MASTER));
			aTable = dataset.getTable(HrModelPaths._Organisation.getPathInSchema());
		}

		RequestResult requestResult = findParentIdByOrganisationBusId(aTable, orgBusId);
		Adaptation organisationRecord = requestResult.isEmpty() ? null : requestResult.nextAdaptation();
		if (organisationRecord != null) {
			valueContext.setValue(organisationRecord.getString(HrModelPaths._Organisation._Organisation_OrganisationId),
					fieldPath);
		}
		
		requestResult.close();
	}

	public static RequestResult findParentIdByOrganisationBusId(AdaptationTable aTable, String organisationBusId) {

		Request request = aTable.createRequest();
		request.setXPathFilter(
				constructPredicateForBusId(HrModelPaths._Organisation._Organisation_OrganisationBusId.format()));
		request.setXPathParameter(xpathFilterParam1, organisationBusId);

		return request.execute();
	}

	/*
	 * Set ManagerId
	 */
	public static void setManagerIdByManagerBusId(TableTriggerExecutionContext aContext,
			ValueContextForUpdate valueContext, Path filedPath, String managerBusId) {

		AdaptationTable employeeTable = aContext.getTable();
		if (!employeeTable.getTablePath().equals(HrModelPaths._Employee.getPathInSchema())) {
			Adaptation dataset = aContext.getAdaptationHome()
					.findAdaptationOrNull(AdaptationName.forName(Constants.DataSet.HR_MASTER));
			employeeTable = dataset.getTable(HrModelPaths._Employee.getPathInSchema());

		}
		RequestResult requestResult = findEmployeeIdByEmployeeBusId(employeeTable, managerBusId);
		Adaptation employeeRecord = requestResult.isEmpty() ? null : requestResult.nextAdaptation();
		if (employeeRecord != null) {
			valueContext.setValue(String.valueOf(employeeRecord.get(HrModelPaths._Employee._Employee_EmployeeId)),
					filedPath);
		}
		requestResult.close();
	}

	/*
	 * Find employeeId
	 */
	public static RequestResult findEmployeeIdByEmployeeBusId(AdaptationTable aTable, String employeeBusId) {
		Request request = aTable.createRequest();
		request.setXPathFilter(constructPredicateForBusId(HrModelPaths._Employee._Employee_EmployeeBusId.format()));
		request.setXPathParameter(xpathFilterParam1, employeeBusId);

		return request.execute();
	}

	/*
	 * Set BuildingId
	 */

	public static void setBuildingIdByBuildingBusId(TableTriggerExecutionContext aContext,
			ValueContextForUpdate valueContext, Path fieldPath, String buildingBusId) {

		AdaptationTable buildingTable = aContext.getTable();
		if (!buildingTable.getTablePath().equals(HrModelPaths._Building.getPathInSchema())) {
			Adaptation dataset = aContext.getAdaptationHome()
					.findAdaptationOrNull(AdaptationName.forName(Constants.DataSet.HR_MASTER));
			buildingTable = dataset.getTable(HrModelPaths._Building.getPathInSchema());
		}
		RequestResult requestResult = findBuildingIdByBuildingBusId(buildingTable, buildingBusId);
		Adaptation buildingRecord = requestResult.isEmpty() ? null : requestResult.nextAdaptation();
		if (buildingRecord != null) {
			valueContext.setValue(String.valueOf(buildingRecord.get(HrModelPaths._Building._Building_BuildingId)),
					fieldPath);
		}
		requestResult.close();
	}

	/*
	 * Find buildingId
	 * 
	 */
	public static RequestResult findBuildingIdByBuildingBusId(AdaptationTable aTable, String buildingBusId) {
		Request request = aTable.createRequest();
		request.setXPathFilter(constructPredicateForBusId(HrModelPaths._Building._Building_BuildingBusId.format()));
		request.setXPathParameter(xpathFilterParam1, buildingBusId);

		return request.execute();
	}

	/*
	 * Construct predicate for single parameter
	 */
	private static XPathFilter constructPredicateForBusId(String fieldPath) {
		StringBuffer predicate = new StringBuffer();
		predicate.append(fieldPath);
		predicate.append("=$").append(xpathFilterParam1);
		return XPathFilter.newFilter(false, predicate.toString());

	}
	
	public static String findManagerIdFromEmployee(String managerBusId , String dataspace){
		
		EmployeeReader employeeReader = new EmployeeReader(dataspace);
		List<EmployeeBean> employeeBeans = employeeReader.findBeansByEmployeeBusId(managerBusId);
		EmployeeBean employeeBean = employeeBeans.isEmpty() ? null : employeeBeans.get(0);
		if(employeeBean != null) {
			return String.valueOf(employeeBean.getEmployeeId());
		}
		return null;	
	}
	
	public static String findOrganisationParentIdByParentBusId(String parentBusId, String dataspace) {
		
		OrganisationReader organisationReader = new OrganisationReader(dataspace);
		List<OrganisationBean> organisationBeans = organisationReader.findBeansByOrganisationBusId(parentBusId);
		OrganisationBean organisationBean = organisationBeans.isEmpty() ? null : organisationBeans.get(0);
		if(organisationBean != null) {
			return String.valueOf(organisationBean.getOrganisationId());
		}
		return null;
	}
}
