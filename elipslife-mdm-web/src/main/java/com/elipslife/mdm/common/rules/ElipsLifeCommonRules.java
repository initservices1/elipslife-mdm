package com.elipslife.mdm.common.rules;

import java.util.List;

import com.elipslife.ebx.data.access.CorePartyIdentifierReader;
import com.elipslife.ebx.data.access.CorePartyReader;
import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.mdm.contract.core.CoreInsuranceContractBean;
import com.elipslife.mdm.contract.core.CoreInsuranceContractReader;
import com.elipslife.mdm.uw.core.UWApplicationBean;
import com.elipslife.mdm.uw.core.UWApplicationReader;

public class ElipsLifeCommonRules {
	
	/*
	 * Get PartyId from Party
	 */
	public static String getPartyIdByPartyBusIdAndSourceSystem(String partyBusId, String sourceSystem) {
		CorePartyIdentifierReader corePartyIdentifierReader = new CorePartyIdentifierReader();

		List<CorePartyIdentifierBean> corePartyIdentifierBeans = corePartyIdentifierReader
				.getBeansBySourceSystem(sourceSystem, partyBusId);
		CorePartyIdentifierBean corePartyIdentifierBean = corePartyIdentifierBeans.size() > 0
				? corePartyIdentifierBeans.get(0)
				: null;
		String partyId = (corePartyIdentifierBean == null || corePartyIdentifierBean.getPartyId() == null
				|| corePartyIdentifierBean.getPartyId().isEmpty()) ? null : corePartyIdentifierBean.getPartyId();

		return partyId;

	}
	
	/*
	 * Get Insurance Contract id from Contract Number
	 */
	public static String getInsuranceContractIdFromContractNumber(String contractNumber) {
		CoreInsuranceContractReader coreInsuranceContractReader = new CoreInsuranceContractReader();
		List<CoreInsuranceContractBean> coreInsuranceContractBeans = coreInsuranceContractReader.findBeansByContractNumber(contractNumber);
		CoreInsuranceContractBean coreInsuranceContractBean = coreInsuranceContractBeans.size() > 0 ? coreInsuranceContractBeans.get(0) : null;
		String insuranceContractId = coreInsuranceContractBean == null ? null : coreInsuranceContractBean.getId().toString();
		return insuranceContractId;

	}
	
	/**
	 * Get insurance contract id by source system and source contract bus id 
	 * 
	 * @param sourceSystem
	 * @param sourceContractBusId
	 * @return
	 */
	public static String getInsuranceContractIdFromContractNumber(String sourceSystem, String sourceContractBusId) {
		CoreInsuranceContractReader coreInsuranceContractReader = new CoreInsuranceContractReader();
		List<CoreInsuranceContractBean> coreInsuranceContractBeans = coreInsuranceContractReader.findBeansBySourceSystemAndSourceContractBusId(sourceSystem, sourceContractBusId);
		CoreInsuranceContractBean coreInsuranceContractBean = coreInsuranceContractBeans.size() > 0 ? coreInsuranceContractBeans.get(0) : null;
		String insuranceContractId = coreInsuranceContractBean == null ? null : coreInsuranceContractBean.getId().toString();
		return insuranceContractId;
	}
	
	public static String getUWApplicationIdFromUWApplicationBusId(String uwApplicationBusId) {
		UWApplicationReader uwApplicationReader = new UWApplicationReader();
		List<UWApplicationBean> uwApplicationBeans = uwApplicationReader.findBeansByApplicationBusId(uwApplicationBusId);
		UWApplicationBean applicationBean = uwApplicationBeans.isEmpty() ? null : uwApplicationBeans.get(0);
		String uwApplicationId = applicationBean == null ? null : applicationBean.getUWApplicationId().toString();
		return uwApplicationId;
		
	}
	
	/*
	 * Get PartyId from Party
	 */
	public static String getPartyIdByPartyBusId(String partyBusId) {
		
		CorePartyReader corePartyReader = new CorePartyReader();
		List<CorePartyBean> corePartyReaders = corePartyReader.findPartyIdByPartyBusId(partyBusId);
		CorePartyBean corePartyBean = corePartyReaders.size() > 0 ? corePartyReaders.get(0) : null;
		
		String partyId = corePartyBean != null ? corePartyBean.getId().toString() : null;
		return partyId;

	}
	
	

}
