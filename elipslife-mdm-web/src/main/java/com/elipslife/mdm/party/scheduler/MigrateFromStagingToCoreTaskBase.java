package com.elipslife.mdm.party.scheduler;

import java.util.ArrayList;
import java.util.HashMap;

import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.boot.VM;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;

public abstract class MigrateFromStagingToCoreTaskBase extends ScheduledTask {

	public String fromDataSpace;
	public String toDataSpace;

	public String fromDataSet;
	public String toDataSet;

	protected Adaptation coreDataSet = null;
	protected AdaptationTable coreAdaptationTable = null;

	protected static final Path partyIdentifierTablePath = PartyPaths._PartyIdentifier.getPathInSchema();

	protected HashMap<String, ArrayList<Adaptation>> partyIdentifierMap = null;
	
	public String getFromDataSpace() {
		return fromDataSpace;
	}


	public void setFromDataSpace(String fromDataSpace) {
		this.fromDataSpace = fromDataSpace;
	}


	public String getToDataSpace() {
		return toDataSpace;
	}


	public void setToDataSpace(String toDataSpace) {
		this.toDataSpace = toDataSpace;
	}


	public String getFromDataSet() {
		return fromDataSet;
	}


	public void setFromDataSet(String fromDataSet) {
		this.fromDataSet = fromDataSet;
	}


	public String getToDataSet() {
		return toDataSet;
	}


	public void setToDataSet(String toDataSet) {
		this.toDataSet = toDataSet;
	}
	
	
	/**
	 * 
	 */
	protected void loadPartyIdentifierMap() {
		
		AdaptationTable partyIdentifierTable = coreDataSet.getTable(partyIdentifierTablePath);

		XPathPredicate predicatePartyIdentifierIdIsNotNull = new XPathPredicate(XPathPredicate.Operation.IsNotNull, PartyPaths._PartyIdentifier._PartyIdentifierId, null);
		String partyIdentifiersPredicate = predicatePartyIdentifierIdIsNotNull.toString();
		RequestResult partyIdentifierRequestResult = partyIdentifierTable.createRequestResult(partyIdentifiersPredicate);

		partyIdentifierMap = new HashMap<>();

		for (Adaptation partyIdentifierAdaptation; (partyIdentifierAdaptation = partyIdentifierRequestResult.nextAdaptation()) != null;) {

			String partyIdentifierSourcePartyBusId = partyIdentifierAdaptation.getString(PartyPaths._PartyIdentifier._SourcePartyBusId);

			if (!partyIdentifierMap.containsKey(partyIdentifierSourcePartyBusId)) {

				ArrayList<Adaptation> partyIdentifierAdaptations = new ArrayList<>();
				partyIdentifierAdaptations.add(partyIdentifierAdaptation);

				partyIdentifierMap.put(partyIdentifierSourcePartyBusId, partyIdentifierAdaptations);
			}

			else {

				partyIdentifierMap.get(partyIdentifierSourcePartyBusId).add(partyIdentifierAdaptation);
			}
		}
		
		partyIdentifierRequestResult.close();
	}
	
	protected void kernelInfo(String messagePattern, Object... arguments) {
		VM.log.kernelInfo(String.format(messagePattern, arguments));
	}
	
	protected void kernelWarn(String messagePattern, Object... arguments) {
		VM.log.kernelWarn(String.format(messagePattern, arguments));
	}
	
	protected void kernelError(String messagePattern, Object... arguments) {
		VM.log.kernelError(String.format(messagePattern, arguments));
	}
}
