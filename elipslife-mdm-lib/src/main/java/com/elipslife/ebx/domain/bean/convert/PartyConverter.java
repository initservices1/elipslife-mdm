package com.elipslife.ebx.domain.bean.convert;

import java.util.Arrays;
import java.util.Objects;

import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.ebx.domain.bean.LandingPartyBean;
import com.elipslife.ebx.domain.bean.LandingPartyBulkProcessBean;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.mdm.party.constants.PartyConstants;

public class PartyConverter {

	/**
	 * 
	 * @param landingPartyBean
	 * @return
	 */
	public static StagingPartyBean createStagingPartyBeanFromLandingPartyBean(LandingPartyBean landingPartyBean) {

		StagingPartyBean stagingPartyBean = new StagingPartyBean();
		fillStagingPartyBeanWithLandingPartyBean(landingPartyBean, stagingPartyBean);
		return stagingPartyBean;
	}

	/**
	 * 
	 * @param landingPartyBean
	 * @param stagingPartyBean
	 */
	public static void fillStagingPartyBeanWithLandingPartyBean(LandingPartyBean landingPartyBean,
			StagingPartyBean stagingPartyBean) {

		stagingPartyBean.setDataOwner(landingPartyBean.getDataOwner());
		stagingPartyBean.setOperatingCountry(landingPartyBean.getOperatingCountry());
		stagingPartyBean.setCrmId(landingPartyBean.getCrmId());
		stagingPartyBean.setPartyType(landingPartyBean.getPartyType());
		stagingPartyBean.setRelationshipOwner(landingPartyBean.getRelationshipOwner());
		stagingPartyBean.setDataResponsible(landingPartyBean.getDataResponsible());
		stagingPartyBean.setLanguage(landingPartyBean.getLanguage());
		stagingPartyBean.setIsActive(landingPartyBean.getIsActive());
		stagingPartyBean.setCorrelationId(landingPartyBean.getCorrelationId());  // set correlation id
		stagingPartyBean.setFirstName(landingPartyBean.getFirstName());
		stagingPartyBean.setMiddleName(landingPartyBean.getMiddleName());
		stagingPartyBean.setLastName(landingPartyBean.getLastName());
		stagingPartyBean.setDateOfBirth(landingPartyBean.getDateOfBirth());
		stagingPartyBean.setGender(landingPartyBean.getGender());
		stagingPartyBean.setTitle(landingPartyBean.getTitle());
		stagingPartyBean.setSalutation(landingPartyBean.getSalutation());
		stagingPartyBean.setLetterSalutation(landingPartyBean.getLetterSalutation());
		stagingPartyBean.setCivilStatus(landingPartyBean.getCivilStatus());
		stagingPartyBean.setBirthCity(landingPartyBean.getBirthCity());
		stagingPartyBean.setBirthCountry(landingPartyBean.getBirthCountry());
		stagingPartyBean.setIdDocumentNumber(landingPartyBean.getIdDocumentNumber());
		stagingPartyBean.setIdDocumentType(landingPartyBean.getIdDocumentType());
		stagingPartyBean.setIdIssueDate(landingPartyBean.getIdIssueDate());
		stagingPartyBean.setIdExpiryDate(landingPartyBean.getIdExpiryDate());
		stagingPartyBean.setIdIssuingAuthority(landingPartyBean.getIdIssuingAuthority());
		stagingPartyBean.setSourceParentBusId(landingPartyBean.getSourceParentBusId());
		stagingPartyBean.setOrgName(landingPartyBean.getOrgName());
		stagingPartyBean.setOrgName2(landingPartyBean.getOrgName2());
		stagingPartyBean.setCompanyType(landingPartyBean.getCompanyType());
		stagingPartyBean.setCommercialRegNo(landingPartyBean.getCommercialRegNo());
		stagingPartyBean.setUidNumber(landingPartyBean.getUidNumber());
		stagingPartyBean.setVatNumber(landingPartyBean.getVatNumber());
		stagingPartyBean.setIndustry(landingPartyBean.getIndustry());
		stagingPartyBean.setSubIndustry(landingPartyBean.getSubIndustry());
		stagingPartyBean.setPreferredEmail(landingPartyBean.getPreferredEmail());
		stagingPartyBean.setPreferredCommChannel(landingPartyBean.getPreferredCommChannel());
		stagingPartyBean.setSourceOrganisationBusId(landingPartyBean.getSourceOrganisationBusId());
		stagingPartyBean.setSourceLineManagerBusId(landingPartyBean.getSourceLineManagerBusId());
		stagingPartyBean.setSourcePartnerBusId(landingPartyBean.getSourcePartnerBusId());
		stagingPartyBean.setDepartment(landingPartyBean.getDepartment());
		stagingPartyBean.setFunction(landingPartyBean.getFunction());
		stagingPartyBean.setIsExecutor(landingPartyBean.getIsExecutor());
		stagingPartyBean.setIsLegalRepresentative(landingPartyBean.getIsLegalRepresentative());
		stagingPartyBean.setIsSeniorManager(landingPartyBean.getIsSeniorManager());
		stagingPartyBean.setIsUltimateBeneficialOwner(landingPartyBean.getIsUltimateBeneficialOwner());
		stagingPartyBean.setBoRelationship(landingPartyBean.getBoRelationship());
		stagingPartyBean.setContactPersonState(landingPartyBean.getContactPersonState());
		stagingPartyBean.setRoles(landingPartyBean.getRoles());
		stagingPartyBean.setIsVipClient(landingPartyBean.getIsVipClient());
		stagingPartyBean.setBlacklist(landingPartyBean.getBlacklist());
		stagingPartyBean.setPartnerState(landingPartyBean.getPartnerState());
		stagingPartyBean.setCompanyPersonalNr(landingPartyBean.getCompanyPersonalNr());
		stagingPartyBean.setSocialSecurityNr(landingPartyBean.getSocialSecurityNr());
		stagingPartyBean.setSocialSecurityNrType(landingPartyBean.getSocialSecurityNrType());
		stagingPartyBean.setBrokerRegNo(landingPartyBean.getBrokerRegNo());
		stagingPartyBean.setBrokerTiering(landingPartyBean.getBrokerTiering());
		stagingPartyBean.setLineOfBusiness(landingPartyBean.getLineOfBusiness());
		stagingPartyBean.setFinancePartyBusId(landingPartyBean.getFinancePartyBusId());
		stagingPartyBean.setPaymentTerms(landingPartyBean.getPaymentTerms());
		stagingPartyBean.setFinanceClients(landingPartyBean.getFinanceClients());
		stagingPartyBean.setCreditorClients(landingPartyBean.getCreditorClients());
		stagingPartyBean.setSourceSystem(landingPartyBean.getSourceSystem());
		stagingPartyBean.setSourcePartyBusId(landingPartyBean.getSourcePartyBusId());
		stagingPartyBean.setValidFrom(landingPartyBean.getValidFrom());
		stagingPartyBean.setValidTo(landingPartyBean.getValidTo());
		stagingPartyBean.setCreationTimestamp(landingPartyBean.getCreationTimestamp());
		stagingPartyBean.setLastUpdateTimestamp(landingPartyBean.getLastUpdateTimestamp());
		stagingPartyBean.setAction(landingPartyBean.getAction());
		stagingPartyBean.setActionBy(landingPartyBean.getActionBy());
	}

	/**
	 * Create Staging Party Bean from Landing Party Bulk Process Bean
	 * 
	 * @param landingPartyBulkProcessBean
	 * @return
	 */
	public static StagingPartyBean createStagingPartyBeanFromLandingPartyBulkProcessBean(
			LandingPartyBulkProcessBean landingPartyBulkProcessBean) {

		StagingPartyBean stagingPartyBean = new StagingPartyBean();
		fillStagingPartyBeanWithLandingPartyBulkProcessBean(landingPartyBulkProcessBean, stagingPartyBean);
		return stagingPartyBean;
	}

	/**
	 * Fill staging party bean with Landing Party Bulk Process Bean
	 * 
	 * @param landingPartyBulkProcessBean
	 * @param stagingPartyBean
	 */
	public static void fillStagingPartyBeanWithLandingPartyBulkProcessBean(
			LandingPartyBulkProcessBean landingPartyBulkProcessBean, StagingPartyBean stagingPartyBean) {

		stagingPartyBean.setDataOwner(landingPartyBulkProcessBean.getDataOwner());
		stagingPartyBean.setOperatingCountry(landingPartyBulkProcessBean.getOperatingCountry());
		stagingPartyBean.setCrmId(landingPartyBulkProcessBean.getCrmId());
		stagingPartyBean.setPartyType(landingPartyBulkProcessBean.getPartyType());
		stagingPartyBean.setRelationshipOwner(landingPartyBulkProcessBean.getRelationshipOwner());
		stagingPartyBean.setDataResponsible(landingPartyBulkProcessBean.getDataResponsible());
		stagingPartyBean.setLanguage(landingPartyBulkProcessBean.getLanguage());
		stagingPartyBean.setCorrelationId(landingPartyBulkProcessBean.getCorrelationId()); // set correlation id
		stagingPartyBean.setIsActive(landingPartyBulkProcessBean.getIsActive());
		stagingPartyBean.setFirstName(landingPartyBulkProcessBean.getFirstName());
		stagingPartyBean.setMiddleName(landingPartyBulkProcessBean.getMiddleName());
		stagingPartyBean.setLastName(landingPartyBulkProcessBean.getLastName());
		stagingPartyBean.setDateOfBirth(landingPartyBulkProcessBean.getDateOfBirth());
		stagingPartyBean.setGender(landingPartyBulkProcessBean.getGender());
		stagingPartyBean.setTitle(landingPartyBulkProcessBean.getTitle());
		stagingPartyBean.setSalutation(landingPartyBulkProcessBean.getSalutation());
		stagingPartyBean.setLetterSalutation(landingPartyBulkProcessBean.getLetterSalutation());
		stagingPartyBean.setCivilStatus(landingPartyBulkProcessBean.getCivilStatus());
		stagingPartyBean.setBirthCity(landingPartyBulkProcessBean.getBirthCity());
		stagingPartyBean.setBirthCountry(landingPartyBulkProcessBean.getBirthCountry());
		stagingPartyBean.setIdDocumentNumber(landingPartyBulkProcessBean.getIdDocumentNumber());
		stagingPartyBean.setIdDocumentType(landingPartyBulkProcessBean.getIdDocumentType());
		stagingPartyBean.setIdIssueDate(landingPartyBulkProcessBean.getIdIssueDate());
		stagingPartyBean.setIdExpiryDate(landingPartyBulkProcessBean.getIdExpiryDate());
		stagingPartyBean.setIdIssuingAuthority(landingPartyBulkProcessBean.getIdIssuingAuthority());
		stagingPartyBean.setSourceParentBusId(landingPartyBulkProcessBean.getSourceParentBusId());
		stagingPartyBean.setOrgName(landingPartyBulkProcessBean.getOrgName());
		stagingPartyBean.setOrgName2(landingPartyBulkProcessBean.getOrgName2());
		stagingPartyBean.setCompanyType(landingPartyBulkProcessBean.getCompanyType());
		stagingPartyBean.setCommercialRegNo(landingPartyBulkProcessBean.getCommercialRegNo());
		stagingPartyBean.setUidNumber(landingPartyBulkProcessBean.getUidNumber());
		stagingPartyBean.setVatNumber(landingPartyBulkProcessBean.getVatNumber());
		stagingPartyBean.setIndustry(landingPartyBulkProcessBean.getIndustry());
		stagingPartyBean.setSubIndustry(landingPartyBulkProcessBean.getSubIndustry());
		stagingPartyBean.setPreferredEmail(landingPartyBulkProcessBean.getPreferredEmail());
		stagingPartyBean.setPreferredCommChannel(landingPartyBulkProcessBean.getPreferredCommChannel());
		stagingPartyBean.setSourceOrganisationBusId(landingPartyBulkProcessBean.getSourceOrganisationBusId());
		stagingPartyBean.setSourceLineManagerBusId(landingPartyBulkProcessBean.getSourceLineManagerBusId());
		stagingPartyBean.setSourcePartnerBusId(landingPartyBulkProcessBean.getSourcePartnerBusId());
		stagingPartyBean.setDepartment(landingPartyBulkProcessBean.getDepartment());
		stagingPartyBean.setFunction(landingPartyBulkProcessBean.getFunction());
		stagingPartyBean.setIsExecutor(landingPartyBulkProcessBean.getIsExecutor());
		stagingPartyBean.setIsLegalRepresentative(landingPartyBulkProcessBean.getIsLegalRepresentative());
		stagingPartyBean.setIsSeniorManager(landingPartyBulkProcessBean.getIsSeniorManager());
		stagingPartyBean.setIsUltimateBeneficialOwner(landingPartyBulkProcessBean.getIsUltimateBeneficialOwner());
		stagingPartyBean.setBoRelationship(landingPartyBulkProcessBean.getBoRelationship());
		stagingPartyBean.setContactPersonState(landingPartyBulkProcessBean.getContactPersonState());
		stagingPartyBean.setRoles(landingPartyBulkProcessBean.getRoles());
		stagingPartyBean.setIsVipClient(landingPartyBulkProcessBean.getIsVipClient());
		stagingPartyBean.setBlacklist(landingPartyBulkProcessBean.getBlacklist());
		stagingPartyBean.setPartnerState(landingPartyBulkProcessBean.getPartnerState());
		stagingPartyBean.setCompanyPersonalNr(landingPartyBulkProcessBean.getCompanyPersonalNr());
		stagingPartyBean.setSocialSecurityNr(landingPartyBulkProcessBean.getSocialSecurityNr());
		stagingPartyBean.setSocialSecurityNrType(landingPartyBulkProcessBean.getSocialSecurityNrType());
		stagingPartyBean.setBrokerRegNo(landingPartyBulkProcessBean.getBrokerRegNo());
		stagingPartyBean.setBrokerTiering(landingPartyBulkProcessBean.getBrokerTiering());
		stagingPartyBean.setLineOfBusiness(landingPartyBulkProcessBean.getLineOfBusiness());
		stagingPartyBean.setFinancePartyBusId(landingPartyBulkProcessBean.getFinancePartyBusId());
		stagingPartyBean.setPaymentTerms(landingPartyBulkProcessBean.getPaymentTerms());
		stagingPartyBean.setFinanceClients(landingPartyBulkProcessBean.getFinanceClients());
		stagingPartyBean.setCreditorClients(landingPartyBulkProcessBean.getCreditorClients());
		stagingPartyBean.setSourceSystem(landingPartyBulkProcessBean.getSourceSystem());
		stagingPartyBean.setSourcePartyBusId(landingPartyBulkProcessBean.getSourcePartyBusId());
		stagingPartyBean.setValidFrom(landingPartyBulkProcessBean.getValidFrom());
		stagingPartyBean.setValidTo(landingPartyBulkProcessBean.getValidTo());
		stagingPartyBean.setCreationTimestamp(landingPartyBulkProcessBean.getCreationTimestamp());
		stagingPartyBean.setLastUpdateTimestamp(landingPartyBulkProcessBean.getLastUpdateTimestamp());
		stagingPartyBean.setAction(landingPartyBulkProcessBean.getAction());
		stagingPartyBean.setActionBy(landingPartyBulkProcessBean.getActionBy());
	}

	/**
	 * 
	 * @param stagingPartyBean
	 * @return
	 */
	public static CorePartyBean createCorePartyBeanFromStagingPartyBean(StagingPartyBean stagingPartyBean,
			String organisationParentPartyId) {

		CorePartyBean corePartyBean = new CorePartyBean();
		fillCorePartyBeanWithStagingPartyBean(stagingPartyBean, corePartyBean, organisationParentPartyId);
		return corePartyBean;
	}

	/**
	 * 
	 * @param stagingPartyBean
	 * @param corePartyBean
	 */
	public static void fillCorePartyBeanWithStagingPartyBean(StagingPartyBean stagingPartyBean,
			CorePartyBean corePartyBean, String organisationParentPartyId) {

//		corePartyBean.setMasterRecord(stagingPartyBean.getMasterRecord());
		corePartyBean.setDataOwner(stagingPartyBean.getDataOwner());
		corePartyBean.setOperatingCountry(stagingPartyBean.getOperatingCountry());

		if (corePartyBean.getPartyBusId() == null || corePartyBean.getPartyBusId().isEmpty()) {

			if (!Objects.equals(stagingPartyBean.getSourceSystem(), PartyConstants.SourceSystems.CRM)
					&& corePartyBean.getId() == null) {
				corePartyBean.setPartyBusId("");
			}

			if (!Objects.equals(stagingPartyBean.getSourceSystem(), PartyConstants.SourceSystems.CRM)
					&& corePartyBean.getId() != null) {
				corePartyBean.setPartyBusId(corePartyBean.getId().toString());
			}

			if (Objects.equals(stagingPartyBean.getSourceSystem(), PartyConstants.SourceSystems.CRM)
					&& corePartyBean.getId() == null) {
				corePartyBean.setPartyBusId(stagingPartyBean.getSourcePartyBusId());
			}

			if (Objects.equals(stagingPartyBean.getSourceSystem(), PartyConstants.SourceSystems.CRM)
					&& corePartyBean.getId() != null) {
				corePartyBean.setPartyBusId(corePartyBean.getPartyBusId());
			}
		}

		corePartyBean.setCrmId(stagingPartyBean.getCrmId());
		corePartyBean.setPartyType(stagingPartyBean.getPartyType());
//		corePartyBean.setPartyName(stagingPartyBean.getPartyName());
//		corePartyBean.setPartyNameSearchText(stagingPartyBean.getPartyNameSearchText());
		corePartyBean.setRelationshipOwner(stagingPartyBean.getRelationshipOwner());
		corePartyBean.setDataResponsible(stagingPartyBean.getDataResponsible());
		corePartyBean.setLanguage(stagingPartyBean.getLanguage());

		boolean isActive = getIsActive(stagingPartyBean);
		corePartyBean.setIsActive(isActive);

		corePartyBean.setLegalAddressId(stagingPartyBean.getLegalAddressId());
		corePartyBean.setFirstName(stagingPartyBean.getFirstName());
		corePartyBean.setMiddleName(stagingPartyBean.getMiddleName());
		corePartyBean.setLastName(stagingPartyBean.getLastName());
		corePartyBean.setDateOfBirth(stagingPartyBean.getDateOfBirth());
		corePartyBean.setGender(stagingPartyBean.getGender());
		corePartyBean.setTitle(stagingPartyBean.getTitle());
		corePartyBean.setSalutation(stagingPartyBean.getSalutation());
		corePartyBean.setLetterSalutation(stagingPartyBean.getLetterSalutation());
		corePartyBean.setCivilStatus(stagingPartyBean.getCivilStatus());
		corePartyBean.setBirthCity(stagingPartyBean.getBirthCity());
		corePartyBean.setBirthCountry(stagingPartyBean.getBirthCountry());
		corePartyBean.setIdDocumentNumber(stagingPartyBean.getIdDocumentNumber());
		corePartyBean.setIdDocumentType(stagingPartyBean.getIdDocumentType());
		corePartyBean.setIdIssueDate(stagingPartyBean.getIdIssueDate());
		corePartyBean.setIdExpiryDate(stagingPartyBean.getIdExpiryDate());
		corePartyBean.setIdIssuingAuthority(stagingPartyBean.getIdIssuingAuthority());
		corePartyBean.setParentId(organisationParentPartyId);
		corePartyBean.setSourceParentBusId(stagingPartyBean.getSourceParentBusId());
		corePartyBean.setOrgName(stagingPartyBean.getOrgName());
		corePartyBean.setOrgName2(stagingPartyBean.getOrgName2());
		corePartyBean.setCompanyType(stagingPartyBean.getCompanyType());
		corePartyBean.setCommercialRegNo(stagingPartyBean.getCommercialRegNo());
		corePartyBean.setUidNumber(stagingPartyBean.getUidNumber());
		corePartyBean.setVatNumber(stagingPartyBean.getVatNumber());
		corePartyBean.setIndustry(stagingPartyBean.getIndustry());
		corePartyBean.setSubIndustry(stagingPartyBean.getSubIndustry());
		corePartyBean.setPreferredEmail(stagingPartyBean.getPreferredEmail());
		corePartyBean.setPreferredCommChannel(stagingPartyBean.getPreferredCommChannel());
		corePartyBean.setSourceOrganisationBusId(stagingPartyBean.getSourceOrganisationBusId());
		corePartyBean.setSourceLineManagerBusId(stagingPartyBean.getSourceLineManagerBusId());
		corePartyBean.setSourcePartnerBusId(stagingPartyBean.getSourcePartnerBusId());
		corePartyBean.setDepartment(stagingPartyBean.getDepartment());
		corePartyBean.setFunction(stagingPartyBean.getFunction());
		corePartyBean.setIsExecutor(stagingPartyBean.getIsExecutor());
		corePartyBean.setIsLegalRepresentative(stagingPartyBean.getIsLegalRepresentative());
		corePartyBean.setIsSeniorManager(stagingPartyBean.getIsSeniorManager());
		corePartyBean.setIsUltimateBeneficialOwner(stagingPartyBean.getIsUltimateBeneficialOwner());
		corePartyBean.setBoRelationship(stagingPartyBean.getBoRelationship());
		corePartyBean.setContactPersonState(stagingPartyBean.getContactPersonState());
		corePartyBean.setRoles(stagingPartyBean.getRoles());
		corePartyBean.setIsVipClient(stagingPartyBean.getIsVipClient());
		corePartyBean.setBlacklist(stagingPartyBean.getBlacklist());
		corePartyBean.setPartnerState(stagingPartyBean.getPartnerState());
		corePartyBean.setCompanyPersonalNr(stagingPartyBean.getCompanyPersonalNr());
		corePartyBean.setSocialSecurityNr(stagingPartyBean.getSocialSecurityNr());
		corePartyBean.setSocialSecurityNrType(stagingPartyBean.getSocialSecurityNrType());
		corePartyBean.setBrokerRegNo(stagingPartyBean.getBrokerRegNo());
		corePartyBean.setBrokerTiering(stagingPartyBean.getBrokerTiering());
		corePartyBean.setLineOfBusiness(stagingPartyBean.getLineOfBusiness());
//		corePartyBean.setIsLhlob(stagingPartyBean.getIsLhlob());
//		corePartyBean.setIsAhlob(stagingPartyBean.getIsAhlob());
		corePartyBean.setFinancePartyBusId(stagingPartyBean.getFinancePartyBusId());
		corePartyBean.setPaymentTerms(stagingPartyBean.getPaymentTerms());
		corePartyBean.setFinanceClients(stagingPartyBean.getFinanceClients());
		corePartyBean.setCreditorClients(stagingPartyBean.getCreditorClients());
		corePartyBean.setValidFrom(stagingPartyBean.getValidFrom());
		corePartyBean.setValidTo(stagingPartyBean.getValidTo());
//		corePartyBean.setIsExpired(stagingPartyBean.getIsExpired());
		corePartyBean.setSourceSystem(stagingPartyBean.getSourceSystem());
		corePartyBean.setSourcePartyBusId(stagingPartyBean.getSourcePartyBusId());

		if (Objects.equals(stagingPartyBean.getAction(), PartyConstants.RecordOperations.CREATE)) {
			corePartyBean.setCreatedBy(stagingPartyBean.getActionBy());
		}

		corePartyBean.setCreationTimestamp(stagingPartyBean.getCreationTimestamp());

		if (Objects.equals(stagingPartyBean.getAction(), PartyConstants.RecordOperations.UPDATE)) {
			corePartyBean.setLastUpdatedBy(stagingPartyBean.getActionBy());
		}

		corePartyBean.setLastUpdateTimestamp(stagingPartyBean.getLastUpdateTimestamp());
		corePartyBean.setLastSyncAction(stagingPartyBean.getAction());
//		corePartyBean.setLastSyncTimestamp(stagingPartyBean.getLastSyncTimestamp());
	}

	public static void fillCorePartyBeanWithStagingPartyBean(CorePartyBean corePartyBeanToUpdate,
			CorePartyBean corePartyBean, String organisationParentPartyId) {

//		corePartyBean.setMasterRecord(stagingPartyBean.getMasterRecord());
		corePartyBean.setDataOwner(corePartyBeanToUpdate.getDataOwner());
		corePartyBean.setOperatingCountry(corePartyBeanToUpdate.getOperatingCountry());

		if (corePartyBean.getPartyBusId() == null || corePartyBean.getPartyBusId().isEmpty()) {

			if (!Objects.equals(corePartyBeanToUpdate.getSourceSystem(), PartyConstants.SourceSystems.CRM)
					&& corePartyBean.getId() == null) {
				corePartyBean.setPartyBusId("");
			}

			if (!Objects.equals(corePartyBeanToUpdate.getSourceSystem(), PartyConstants.SourceSystems.CRM)
					&& corePartyBean.getId() != null) {
				corePartyBean.setPartyBusId(corePartyBean.getId().toString());
			}

			if (Objects.equals(corePartyBeanToUpdate.getSourceSystem(), PartyConstants.SourceSystems.CRM)
					&& corePartyBean.getId() == null) {
				corePartyBean.setPartyBusId(corePartyBeanToUpdate.getSourcePartyBusId());
			}

			if (Objects.equals(corePartyBeanToUpdate.getSourceSystem(), PartyConstants.SourceSystems.CRM)
					&& corePartyBean.getId() != null) {
				corePartyBean.setPartyBusId(corePartyBean.getPartyBusId());
			}
		}

		corePartyBean.setCrmId(corePartyBeanToUpdate.getCrmId());
		corePartyBean.setPartyType(corePartyBeanToUpdate.getPartyType());
//		corePartyBean.setPartyName(corePartyBeanToUpdate.getPartyName());
//		corePartyBean.setPartyNameSearchText(corePartyBeanToUpdate.getPartyNameSearchText());
		corePartyBean.setRelationshipOwner(corePartyBeanToUpdate.getRelationshipOwner());
		corePartyBean.setDataResponsible(corePartyBeanToUpdate.getDataResponsible());
		corePartyBean.setLanguage(corePartyBeanToUpdate.getLanguage());
		corePartyBean.setLegalAddressId(corePartyBeanToUpdate.getLegalAddressId());

		//boolean isActive = getIsActive(corePartyBeanToUpdate);
		corePartyBean.setIsActive(corePartyBeanToUpdate.getIsActive());
		corePartyBean.setLegalAddressId(corePartyBeanToUpdate.getLegalAddressId());
		corePartyBean.setFirstName(corePartyBeanToUpdate.getFirstName());
		corePartyBean.setMiddleName(corePartyBeanToUpdate.getMiddleName());
		corePartyBean.setLastName(corePartyBeanToUpdate.getLastName());
		corePartyBean.setDateOfBirth(corePartyBeanToUpdate.getDateOfBirth());
		corePartyBean.setGender(corePartyBeanToUpdate.getGender());
		corePartyBean.setTitle(corePartyBeanToUpdate.getTitle());
		corePartyBean.setSalutation(corePartyBeanToUpdate.getSalutation());
		corePartyBean.setLetterSalutation(corePartyBeanToUpdate.getLetterSalutation());
		corePartyBean.setCivilStatus(corePartyBeanToUpdate.getCivilStatus());
		corePartyBean.setBirthCity(corePartyBeanToUpdate.getBirthCity());
		corePartyBean.setBirthCountry(corePartyBeanToUpdate.getBirthCountry());
		corePartyBean.setIdDocumentNumber(corePartyBeanToUpdate.getIdDocumentNumber());
		corePartyBean.setIdDocumentType(corePartyBeanToUpdate.getIdDocumentType());
		corePartyBean.setIdIssueDate(corePartyBeanToUpdate.getIdIssueDate());
		corePartyBean.setIdExpiryDate(corePartyBeanToUpdate.getIdExpiryDate());
		corePartyBean.setIdIssuingAuthority(corePartyBeanToUpdate.getIdIssuingAuthority());
		corePartyBean.setParentId(organisationParentPartyId);
		corePartyBean.setSourceParentBusId(corePartyBeanToUpdate.getSourceParentBusId());
		corePartyBean.setOrgName(corePartyBeanToUpdate.getOrgName());
		corePartyBean.setOrgName2(corePartyBeanToUpdate.getOrgName2());
		corePartyBean.setCompanyType(corePartyBeanToUpdate.getCompanyType());
		corePartyBean.setCommercialRegNo(corePartyBeanToUpdate.getCommercialRegNo());
		corePartyBean.setUidNumber(corePartyBeanToUpdate.getUidNumber());
		corePartyBean.setVatNumber(corePartyBeanToUpdate.getVatNumber());
		corePartyBean.setIndustry(corePartyBeanToUpdate.getIndustry());
		corePartyBean.setSubIndustry(corePartyBeanToUpdate.getSubIndustry());
		corePartyBean.setPreferredEmail(corePartyBeanToUpdate.getPreferredEmail());
		corePartyBean.setPreferredCommChannel(corePartyBeanToUpdate.getPreferredCommChannel());
		corePartyBean.setSourceOrganisationBusId(corePartyBeanToUpdate.getSourceOrganisationBusId());
		corePartyBean.setSourceLineManagerBusId(corePartyBeanToUpdate.getSourceLineManagerBusId());
		corePartyBean.setSourcePartnerBusId(corePartyBeanToUpdate.getSourcePartnerBusId());
		corePartyBean.setDepartment(corePartyBeanToUpdate.getDepartment());
		corePartyBean.setFunction(corePartyBeanToUpdate.getFunction());
		corePartyBean.setIsExecutor(corePartyBeanToUpdate.getIsExecutor());
		corePartyBean.setIsLegalRepresentative(corePartyBean.getIsLegalRepresentative());
		corePartyBean.setIsSeniorManager(corePartyBeanToUpdate.getIsSeniorManager());
		corePartyBean.setIsUltimateBeneficialOwner(corePartyBeanToUpdate.getIsUltimateBeneficialOwner());
		corePartyBean.setBoRelationship(corePartyBeanToUpdate.getBoRelationship());
		corePartyBean.setContactPersonState(corePartyBeanToUpdate.getContactPersonState());
		corePartyBean.setRoles(corePartyBeanToUpdate.getRoles());
		corePartyBean.setIsVipClient(corePartyBeanToUpdate.getIsVipClient());
		corePartyBean.setBlacklist(corePartyBeanToUpdate.getBlacklist());
		corePartyBean.setPartnerState(corePartyBeanToUpdate.getPartnerState());
		corePartyBean.setCompanyPersonalNr(corePartyBeanToUpdate.getCompanyPersonalNr());
		corePartyBean.setSocialSecurityNr(corePartyBeanToUpdate.getSocialSecurityNr());
		corePartyBean.setSocialSecurityNrType(corePartyBeanToUpdate.getSocialSecurityNrType());
		corePartyBean.setBrokerRegNo(corePartyBeanToUpdate.getBrokerRegNo());
		corePartyBean.setBrokerTiering(corePartyBeanToUpdate.getBrokerTiering());
		corePartyBean.setLineOfBusiness(corePartyBeanToUpdate.getLineOfBusiness());
//		corePartyBean.setIsLhlob(corePartyBeanToUpdate.getIsLhlob());
//		corePartyBean.setIsAhlob(corePartyBeanToUpdate.getIsAhlob());
		corePartyBean.setFinancePartyBusId(corePartyBeanToUpdate.getFinancePartyBusId());
		corePartyBean.setPaymentTerms(corePartyBeanToUpdate.getPaymentTerms());
		corePartyBean.setFinanceClients(corePartyBeanToUpdate.getFinanceClients());
		corePartyBean.setCreditorClients(corePartyBeanToUpdate.getCreditorClients());
		corePartyBean.setValidFrom(corePartyBeanToUpdate.getValidFrom());
		corePartyBean.setValidTo(corePartyBeanToUpdate.getValidTo());
//		corePartyBean.setIsExpired(corePartyBeanToUpdate.getIsExpired());
		corePartyBean.setSourceSystem(corePartyBeanToUpdate.getSourceSystem());
		corePartyBean.setSourcePartyBusId(corePartyBeanToUpdate.getSourcePartyBusId());
		corePartyBean.setCreatedBy(corePartyBeanToUpdate.getCreatedBy());
		corePartyBean.setCreationTimestamp(corePartyBeanToUpdate.getCreationTimestamp());
		corePartyBean.setLastUpdatedBy(corePartyBeanToUpdate.getLastUpdatedBy());

		corePartyBean.setLastUpdateTimestamp(corePartyBeanToUpdate.getLastUpdateTimestamp());
		corePartyBean.setLastSyncAction(PartyConstants.RecordOperations.UPDATE);
//		corePartyBean.setLastSyncTimestamp(stagingPartyBean.getLastSyncTimestamp());
	}

	/**
	 * Get IsActive by "Identifying Party and Synchronising Data" rules
	 * 
	 * @param stagingPartyBean
	 * @return
	 */
	private static boolean getIsActive(StagingPartyBean stagingPartyBean) {

		if (PartyConstants.Entities.Party.PartyType.CONTACT.equals(stagingPartyBean.getPartyType())
				&& stagingPartyBean.getContactPersonState() != null
				&& !stagingPartyBean.getContactPersonState().isEmpty()) {

			return stagingPartyBean.getContactPersonState()
					.equals(PartyConstants.Entities.Party.ContactPersonState.ACTIVE);
		}

		else if (!PartyConstants.Entities.Party.PartyType.CONTACT.equals(stagingPartyBean.getPartyType())
				&& stagingPartyBean.getPartnerState() != null && !stagingPartyBean.getPartnerState().isEmpty()) {

			String[] activePartnerStates = new String[] { PartyConstants.Entities.Party.PartnerState.ACTIVE_PARTNER,
					PartyConstants.Entities.Party.PartnerState.PROSPECTED_PARTNER,
					PartyConstants.Entities.Party.PartnerState.BLACKLISTED_PARTNER };

			return Arrays.asList(activePartnerStates).contains(stagingPartyBean.getPartnerState());
		} else {
			return stagingPartyBean.getIsActive();
		}
	}
}
