package com.elipslife.mdm.hr.trigger;

import java.util.Date;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.common.HrCommonRules;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public class OrganisationTableTrigger extends TableTrigger {

	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		super.handleBeforeCreate(aContext);

		aContext.getOccurrenceContextForUpdate().setValue(Constants.RecordOperations.CREATE,
				HrModelPaths._Building._Control_LastSyncAction);
		aContext.getOccurrenceContextForUpdate().setValue(new Date(),
				HrModelPaths._Organisation._Control_LastSyncTimestamp);
		
		String organisationBusId = (String)aContext.getOccurrenceContext().getValue(HrModelPaths._Organisation._Organisation_OrganisationBusId);
		if( organisationBusId != null) {
			aContext.getOccurrenceContextForUpdate().setValue(organisationBusId, HrModelPaths._Organisation._Organisation_OrganisationId);
		}
		
		// Set ParentId
		String parentBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Organisation._Organisation_ParentBusId);
		if (parentBusId != null) {
			HrCommonRules.setOrganisationIdByParentBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Organisation._Organisation_ParentId, parentBusId);
		}

		// Set ManagerId
		String managerBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Organisation._Organisation_ManagerBusId);
		if (managerBusId != null) {
			HrCommonRules.setManagerIdByManagerBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Organisation._Organisation_ManagerId, managerBusId);
		}

		// Set BuildingId
		String buildingBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Organisation._Organisation_BuildingBusId);
		if (buildingBusId != null) {
			HrCommonRules.setBuildingIdByBuildingBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Organisation._Organisation_BuildingId, buildingBusId);
		}

	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
		super.handleBeforeModify(aContext);

		aContext.getOccurrenceContextForUpdate().setValue(Constants.RecordOperations.UPDATE,
				HrModelPaths._Building._Control_LastSyncAction);
		aContext.getOccurrenceContextForUpdate().setValue(new Date(),
				HrModelPaths._Organisation._Control_LastSyncTimestamp);

		
		// Set ParentId
		String parentBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Organisation._Organisation_ParentBusId);
		if (parentBusId != null) {
			HrCommonRules.setOrganisationIdByParentBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Organisation._Organisation_ParentId, parentBusId);
		}

		// Set ManagerId
		if(aContext.getChanges().getChange(HrModelPaths._Organisation._Organisation_ManagerBusId) != null) {
			
			String managerBusId = (String) aContext.getOccurrenceContext()
					.getValue(HrModelPaths._Organisation._Organisation_ManagerBusId);
			if (managerBusId != null) {
				HrCommonRules.setManagerIdByManagerBusId(aContext, aContext.getOccurrenceContextForUpdate(),
						HrModelPaths._Organisation._Organisation_ManagerId, managerBusId);
			}
		}
		

		// Set BuildingId
		String buildingBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Organisation._Organisation_BuildingBusId);
		if (buildingBusId != null) {
			HrCommonRules.setBuildingIdByBuildingBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Organisation._Organisation_BuildingId, buildingBusId);
		}

	}

	@Override
	public void setup(TriggerSetupContext aContext) {
		// TODO Auto-generated method stub

	}

}
