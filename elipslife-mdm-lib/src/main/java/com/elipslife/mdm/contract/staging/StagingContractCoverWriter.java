package com.elipslife.mdm.contract.staging;

import java.util.Date;
import java.util.HashMap;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class StagingContractCoverWriter extends AbstractTableWriter<StagingContractCoverBean> {

	private StagingContractCoverReader stagingContractCoverReader;
	
	public StagingContractCoverWriter() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public StagingContractCoverWriter(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._STG_ContractCover.getPathInSchema()));
	}
	
	public StagingContractCoverWriter(AdaptationTable table) {
		super(table);
		
		this.stagingContractCoverReader = new StagingContractCoverReader(table);
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, StagingContractCoverBean bean) {
		
		values.put(ContractPaths._STG_ContractCover._Cover_ContractCoverId, bean.getId());
		values.put(ContractPaths._STG_ContractCover._Exposure_ExposureBusId, bean.getExposureBusId());
		values.put(ContractPaths._STG_ContractCover._Exposure_ContractCoverMaster, bean.getContractCoverMaster());
		values.put(ContractPaths._STG_ContractCover._Cover_CoverBusId, bean.getCoverBusId());
		values.put(ContractPaths._STG_ContractCover._Cover_CoverGroupType, bean.getCoverGroupType());
		values.put(ContractPaths._STG_ContractCover._Cover_CoverGroupTypeName, bean.getCoverGroupTypeName());
		values.put(ContractPaths._STG_ContractCover._Cover_CoverName, bean.getCoverName());
		values.put(ContractPaths._STG_ContractCover._Cover_CoverTemplateBusId, bean.getCoverTemplateBusId());
		values.put(ContractPaths._STG_ContractCover._Cover_BeneficiaryType, bean.getBeneficiaryType());
		
		values.put(ContractPaths._STG_ContractCover._Control_SourceSystem, bean.getSourceSystem());
		values.put(ContractPaths._STG_ContractCover._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(ContractPaths._STG_ContractCover._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(ContractPaths._STG_ContractCover._Control_LastSyncTimestamp, new Date());
		values.put(ContractPaths._STG_ContractCover._Control_Action, bean.getAction());
		values.put(ContractPaths._STG_ContractCover._Control_ActionBy, bean.getActionBy());
	}

	@Override
	public AbstractTableReader<StagingContractCoverBean> getTableReader() {
		return this.stagingContractCoverReader;
	}
}