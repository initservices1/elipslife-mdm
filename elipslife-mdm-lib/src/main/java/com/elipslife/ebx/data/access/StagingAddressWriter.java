package com.elipslife.ebx.data.access;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class StagingAddressWriter extends BaseAddressWriter<StagingAddressBean> {
	
	public StagingAddressWriter() {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
				PartyPaths._STG_Address.getPathInSchema()));
	}
	
	public StagingAddressWriter(String dataspace) {
        this(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataspace, PartyConstants.DataSet.PARTY_MASTER),
                PartyPaths._STG_Address.getPathInSchema()));
    }
	
	public StagingAddressWriter(AdaptationTable adaptationTable) {
		super(new StagingAddressReader(adaptationTable), adaptationTable);
	}
	

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, StagingAddressBean bean) {

		values.put(PartyPaths._STG_Address._Address_AddressId, bean.getId());
		
		values.put(PartyPaths._STG_Address._Address_AddressType, bean.getAddressType());
		values.put(PartyPaths._STG_Address._Address_AddressUse, bean.getAddressUsage());
		values.put(PartyPaths._STG_Address._Address_SourceAddressBusId, bean.getSourceAddressBusId());
		values.put(PartyPaths._STG_Address._Address_CoreAddressId, bean.getCoreAddressId());
		values.put(PartyPaths._STG_Address._PostalAddress_Street, bean.getStreet());
		values.put(PartyPaths._STG_Address._PostalAddress_Street2, bean.getStreet2());
		values.put(PartyPaths._STG_Address._PostalAddress_PostCode, bean.getPostCode());
		values.put(PartyPaths._STG_Address._PostalAddress_POBox, bean.getPoBox());
		values.put(PartyPaths._STG_Address._PostalAddress_POBoxPostCode, bean.getPoBoxPostCode());
		values.put(PartyPaths._STG_Address._PostalAddress_POBoxTown, bean.getPoBoxTown());
		values.put(PartyPaths._STG_Address._PostalAddress_District, bean.getDistrict());
		values.put(PartyPaths._STG_Address._PostalAddress_Town, bean.getTown());
		values.put(PartyPaths._STG_Address._PostalAddress_StateProvince, bean.getStateProvince());
		values.put(PartyPaths._STG_Address._PostalAddress_Country, bean.getCountry());
		values.put(PartyPaths._STG_Address._Address_CorrelationId, bean.getCorrelationId());
		values.put(PartyPaths._STG_Address._Control_SourceSystem, bean.getSourceSystem());
		values.put(PartyPaths._STG_Address._Control_SourcePartyBusId, bean.getSourcePartyBusId());
		values.put(PartyPaths._STG_Address._Control_ValidFrom, bean.getValidFrom());
		values.put(PartyPaths._STG_Address._Control_ValidTo, bean.getValidTo());
		values.put(PartyPaths._STG_Address._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(PartyPaths._STG_Address._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(PartyPaths._STG_Address._Control_Action, bean.getAction());
		values.put(PartyPaths._STG_Address._Control_ActionBy, bean.getActionBy());
		values.put(PartyPaths._STG_Address._ElectronicAddress_ElectronicAddressType, bean.getElectronicAddressType());
		values.put(PartyPaths._STG_Address._ElectronicAddress_Address, bean.getAddress());
	}
}