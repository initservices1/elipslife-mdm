package com.elipslife.mdm.uw.trigger;

import java.util.Date;

import com.elipslife.mdm.common.rules.ElipsLifeCommonRules;
import com.elipslife.mdm.path.UWPaths;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TableTriggerExecutionContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class UWCaseTableTrigger extends TableTrigger{
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		super.handleBeforeCreate(aContext);
		// set LastSyncAction and LastSyncTimestamp
		aContext.getOccurrenceContextForUpdate().setValue(new Date(), UWPaths._UWCase._Control_LastSyncTimestamp);
		
		String sourceSystem = (String)aContext.getOccurrenceContext().getValue(UWPaths._UWCase._Context_SourceSystem);
		if(sourceSystem != null) {
			// update Applicant Id
			setApplicateId(aContext, aContext.getOccurrenceContextForUpdate(), sourceSystem);
			// Update Insurance Contract Id
			// setInsuranceContractId(aContext, aContext.getOccurrenceContextForUpdate(), sourceSystem);
			// Update UWApplication Id
			setUWApplicationId(aContext, aContext.getOccurrenceContextForUpdate());
		}
	}
	
	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
		super.handleBeforeModify(aContext);
		
		aContext.getOccurrenceContextForUpdate().setValue(new Date(), UWPaths._UWCase._Control_LastSyncTimestamp);
		
		String sourceSystem = (String)aContext.getOccurrenceContext().getValue(UWPaths._UWCase._Context_SourceSystem);
		if(sourceSystem != null) {
			// update Applicant Id
			setApplicateId(aContext, aContext.getOccurrenceContextForUpdate(), sourceSystem);
			// Update Insurance Contract Id
			// setInsuranceContractId(aContext, aContext.getOccurrenceContextForUpdate(), sourceSystem);
			// Update UWApplication Id
			setUWApplicationId(aContext, aContext.getOccurrenceContextForUpdate());
		}
		
	}
	
	private void setApplicateId(TableTriggerExecutionContext aContext, ValueContextForUpdate valueContext, String sourceSystem) {
		String applicantBusId = (String)aContext.getOccurrenceContext().getValue(UWPaths._UWCase._Applicant_ApplicantBusId);
		if(applicantBusId != null) {
			String applicantId = ElipsLifeCommonRules.getPartyIdByPartyBusId(applicantBusId);
			if(applicantId != null) {
				valueContext.setValue(applicantId, UWPaths._UWCase._Applicant_ApplicantId);
			} else {
				valueContext.setValue("", UWPaths._UWCase._Applicant_ApplicantId);
			}
		}
	}
	
	private void setInsuranceContractId(TableTriggerExecutionContext aContext, ValueContextForUpdate valueContext, String sourceSystem) {
		String contractNumber = (String)aContext.getOccurrenceContext().getValue(UWPaths._UWCase._Context_ContractNumber);
		if(contractNumber != null) {
			String insuranceContractId = ElipsLifeCommonRules.getInsuranceContractIdFromContractNumber(contractNumber);
			if(insuranceContractId != null) {
				valueContext.setValue(insuranceContractId, UWPaths._UWCase._Context_InsuranceContractId);
			} else {
				valueContext.setValue("", UWPaths._UWCase._Context_InsuranceContractId);
			}
		}
	}
	
	private void setUWApplicationId(TableTriggerExecutionContext aContext, ValueContextForUpdate valueContext) {
		String uwApplicationBusId = (String)aContext.getOccurrenceContext().getValue(UWPaths._UWCase._Context_UWApplicationBusId);
		if(uwApplicationBusId != null) {
			String uwApplicationId = ElipsLifeCommonRules.getUWApplicationIdFromUWApplicationBusId(uwApplicationBusId);
			if(uwApplicationId != null) {
				valueContext.setValue(uwApplicationId, UWPaths._UWCase._Context_UWApplicationId);
			} else {
				valueContext.setValue("", UWPaths._UWCase._Context_UWApplicationId);
			}
		}
	}

	@Override
	public void setup(TriggerSetupContext aContext) {
		
	}

}
