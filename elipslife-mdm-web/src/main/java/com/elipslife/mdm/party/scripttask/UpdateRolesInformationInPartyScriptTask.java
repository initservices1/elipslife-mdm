package com.elipslife.mdm.party.scripttask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.elipslife.ebx.data.access.CorePartyReader;
import com.elipslife.ebx.data.access.UpdateRecords;
import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.ebx.domain.bean.convert.CorePartyConverter;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.party.constants.PartyConstants.Entities.Party;
import com.elipslife.mdm.party.constants.PartyConstants.Entities.Party.Delimiter;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.boot.VM;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.presales.toolbox.procedure.RecordValuesBean;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

public class UpdateRolesInformationInPartyScriptTask extends ScriptTask {

	@Override
	public void executeScript(ScriptTaskContext aContext) throws OperationException {
		
		HashMap<String, String> contextVariables = getContextVariables(aContext);
		String masterDataspace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		AdaptationHome masterDataspaceHome =  aContext.getRepository().lookupHome(HomeKey.forBranchName(masterDataspace));
        CorePartyReader corePartyReader = new CorePartyReader(masterDataspace);
		
        // Update party roles information
        updateRolesInfoInPartyTable(corePartyReader, aContext, masterDataspaceHome, masterDataspace);
	}
	
	/**
	 * Update party roles information
	 * 
	 * @param corePartyReader
	 * @param aContext
	 * @param masterDataspaceHome
	 * @param masterDataspace
	 */
	private void updateRolesInfoInPartyTable(CorePartyReader corePartyReader, ScriptTaskContext aContext, AdaptationHome masterDataspaceHome, String masterDataspace) {
		
		List<CorePartyBean> updateCorePartyRoles = new ArrayList<CorePartyBean>();
		List<CorePartyBean> corePartyRoles = corePartyReader.findAllPartyRoles();
		int recordsCount = 0;
		for(CorePartyBean corePartyBean : corePartyRoles) {
			
			corePartyBean.setIsElipsLifeLegalEntity(false);
			corePartyBean.setIsPolicyholder(false);
	        corePartyBean.setIsReCoInsurer(false);
	        corePartyBean.setIsVendor(false);
	        corePartyBean.setIsMedicalServiceProvider(false);
	        corePartyBean.setIsStateRegulatoryAuthority(false);
	        corePartyBean.setIsDistributionPartner(false);
	        corePartyBean.setIsBroker(false);
	        corePartyBean.setIsAssociation(false);
	        corePartyBean.setIsBank(false);
	        corePartyBean.setIsConsultant(false);
	        corePartyBean.setIsTpa(false);
	        corePartyBean.setIsInsurance(false);
	        corePartyBean.setIsInsuredPerson(false);
	        corePartyBean.setIsInjuredPerson(false);
	        corePartyBean.setIsBeneficiary(false);
	        corePartyBean.setIsVipClient(false);
	        corePartyBean.setIsBusinessPartner(false);
	        
			recordsCount++;
			String roles = String.valueOf(corePartyBean.getRoles());
			if (roles != null && !roles.isEmpty()) {
				List<String> listOfRoles = Arrays.asList(roles.split(Delimiter.ROLES));
				for (String role : listOfRoles) {
	                switch (role.trim()) {
	                	
	                case Party.PartyRole.ELIPSLIFE_LEGALENTITY:
	                	corePartyBean.setIsElipsLifeLegalEntity(true);
                        break;

                    case Party.PartyRole.POLICY_HOLDER:
                    	corePartyBean.setIsPolicyholder(true);
                    	corePartyBean.setIsBusinessPartner(true);
                        break;

                    case Party.PartyRole.RE_COINSURER:
                    	corePartyBean.setIsReCoInsurer(true);
                    	corePartyBean.setIsBusinessPartner(true);
                        break;

                    case Party.PartyRole.VENDOR:
                    	corePartyBean.setIsVendor(true);
                    	corePartyBean.setIsBusinessPartner(true);
                        break;

                    case Party.PartyRole.STATE_REGULATORY_AUTHORITY:
                    	corePartyBean.setIsStateRegulatoryAuthority(true);
                    	corePartyBean.setIsBusinessPartner(true);
                        break;

                    case Party.PartyRole.DISTRIBUTION_PARTNER:
                    	corePartyBean.setIsDistributionPartner(true);
                    	corePartyBean.setIsBusinessPartner(true);
                        break;

                    case Party.PartyRole.BROKER:
                    	corePartyBean.setIsBroker(true);
                    	corePartyBean.setIsBusinessPartner(true);
                    	corePartyBean.setIsDistributionPartner(true);
                        break;

                    case Party.PartyRole.ASSOCIATION:
                    	corePartyBean.setIsAssociation(true);
                    	corePartyBean.setIsBusinessPartner(true);
                    	corePartyBean.setIsDistributionPartner(true);
                        break;

                    case Party.PartyRole.BANK:
                    	corePartyBean.setIsBank(true);
                    	corePartyBean.setIsBusinessPartner(true);
                    	corePartyBean.setIsDistributionPartner(true);
                        break;

                    case Party.PartyRole.CONSULLTANT:
                    	corePartyBean.setIsConsultant(true);
                    	corePartyBean.setIsBusinessPartner(true);
                    	corePartyBean.setIsDistributionPartner(true);
                        break;

                    case Party.PartyRole.TPA:
                    	corePartyBean.setIsTpa(true);
                    	corePartyBean.setIsBusinessPartner(true);
                    	corePartyBean.setIsDistributionPartner(true);
                        break;

                    case Party.PartyRole.INSURANCE:
                    	corePartyBean.setIsInsurance(true);
                    	corePartyBean.setIsBusinessPartner(true);
                    	corePartyBean.setIsDistributionPartner(true);
                        break;

                    case Party.PartyRole.INSURED_PERSON:
                    	corePartyBean.setIsInsuredPerson(true);
                        break;
                    
                    case Party.PartyRole.INJURED_PERSON:
                    	corePartyBean.setIsInjuredPerson(true);
                        break;

                    case Party.PartyRole.BENEFICIARY:
                    	corePartyBean.setIsBeneficiary(true);
                        break;

                    case Party.PartyRole.MEDICAL_SERVICE_PROVIDER:
                    	corePartyBean.setIsMedicalServiceProvider(true);
                    	corePartyBean.setIsBusinessPartner(true);
                    	corePartyBean.setIsVendor(true);
                        break;
	                }
				}
				updateCorePartyRoles.add(corePartyBean);
			}
			
			if(recordsCount > 1000) {
				// Update core party roles
				updateCorePartyRoles(updateCorePartyRoles, masterDataspaceHome, aContext.getSession());
				recordsCount = 0;
				updateCorePartyRoles.clear();
			}
		}
		
		if(!updateCorePartyRoles.isEmpty()) {
			// Update core party roles
			updateCorePartyRoles(updateCorePartyRoles, masterDataspaceHome, aContext.getSession());
			recordsCount = 0;
			updateCorePartyRoles.clear();
		}
	}
	
	/**
	 * Update core parties with Party roles
	 * 
	 * @param updateCorePartyList
	 * @param partyDataspace
	 * @param session
	 */
	private void updateCorePartyRoles(List<CorePartyBean> updateCorePartyList, AdaptationHome partyDataspace, Session session){
		
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		RecordValuesBean rvb = null;
		HashMap<Path, Object> values = null;
		try {
			AdaptationTable partyTable = partyDataspace.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.PARTY_MASTER)).getTable(PartyPaths._Party.getPathInSchema());
			for (CorePartyBean corePartyBean : updateCorePartyList) {
				values = CorePartyConverter.fillCorePartyValuesFromStagingPartyBean(corePartyBean, true);
				rvb = new RecordValuesBean(partyTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(corePartyBean.getId().toString())), values);
				recordValuesBeanList.add(rvb);
			}

			final ProgrammaticService service = ProgrammaticService.createForSession(session, partyDataspace);
			UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
			
			ProcedureResult result = service.execute(updateRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
				 VM.log.kernelError("Exception occurred while updating the roles in Core Parties"+ exception);
			}
		} catch (Exception e) {
			VM.log.kernelError("Exception occurred while updating the roles in Core Parties"+ e);
		}
		
		
	}
	
	/**
	 * 
	 * @param dataContext
	 * @return
	 */
	private static HashMap<String, String> getContextVariables(
			com.orchestranetworks.workflow.DataContextReadOnly dataContext) {

		HashMap<String, String> hashMap = new HashMap<String, String>();
		Iterator<String> variableNames = dataContext.getVariableNames();
		while (variableNames.hasNext()) {
			String variableName = variableNames.next();
			String variableValue = dataContext.getVariableString(variableName);
			hashMap.put(variableName, variableValue);
		}

		return hashMap;
	}

}
