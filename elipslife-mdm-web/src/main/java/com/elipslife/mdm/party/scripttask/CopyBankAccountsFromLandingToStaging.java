package com.elipslife.mdm.party.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.butos.ebx.procedure.CreateRecords;
import com.butos.ebx.procedure.DeleteRecords;
import com.butos.ebx.procedure.RecordValuesBean;
import com.butos.ebx.procedure.UpdateRecords;
import com.elipslife.ebx.data.access.LandingBankAccountBulkProcessReader;
import com.elipslife.ebx.data.access.LandingBankAccountBulkProcessWriter;
import com.elipslife.ebx.data.access.LandingBankAccountBulkProcessWriterSession;
import com.elipslife.ebx.data.access.LandingBankAccountReader;
import com.elipslife.ebx.data.access.LandingBankAccountWriter;
import com.elipslife.ebx.data.access.LandingBankAccountWriterSession;
import com.elipslife.ebx.data.access.LandingPartyBulkProcessReader;
import com.elipslife.ebx.data.access.StagingBankAccountReader;
import com.elipslife.ebx.data.access.StagingBankAccountWriter;
import com.elipslife.ebx.data.access.StagingBankAccountWriterSession;
import com.elipslife.ebx.data.access.StagingPartyReader;
import com.elipslife.ebx.data.access.changeset.ChangeSetBuilder;
import com.elipslife.ebx.domain.bean.LandingBankAccountBean;
import com.elipslife.ebx.domain.bean.LandingBankAccountBulkProcessBean;
import com.elipslife.ebx.domain.bean.LandingPartyBulkProcessBean;
import com.elipslife.ebx.domain.bean.StagingBankAccountBean;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.ebx.domain.bean.convert.ReadPartyBulkProcesserProperty;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.elipslife.mdm.path.PartyPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

public class CopyBankAccountsFromLandingToStaging extends ScriptTask {

	private Integer recordCount = 0;

	private static AdaptationHome stagingDataSpaceHome;
	private static AdaptationHome templandingDataspace;
	private static Adaptation stagingDataSet;
	private static Adaptation landingDataSet;

	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException {

		HashMap<String, String> contextVariables = getContextVariables(context);
		String dataSpace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_LANDING_DATASPACE);
		String stagingDataspace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		String commitSize = contextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);

		Repository reposity = context.getRepository();
		setStagingDataSpaceHome(reposity.lookupHome(HomeKey.forBranchName(stagingDataspace)));
		setTemplandingDataspace(reposity.lookupHome(HomeKey.forBranchName(dataSpace)));
		setStagingDataSet(getStagingDataSpaceHome());
		setLandingDataSet(getTemplandingDataspace());

		StringBuilder stringBuilderErrorMessage = new StringBuilder();

		ReadPartyBulkProcesserProperty bulkProcesserProperty = ReadPartyBulkProcesserProperty.getInstance();
		// Check If it is a Bulk load or Incremental load
		if (bulkProcesserProperty.getPartyBulkProcessProperty().equals(PartyConstants.Entities.Party.RestBulkProcessState.REST_BULKPROCESS_STATE_FULL)) {

			recordCount = 0;
			// Create landing bank account reader to read from child data space
			LandingBankAccountBulkProcessReader landingBankAccountBulkProcessReader = new LandingBankAccountBulkProcessReader(dataSpace);
			 List<LandingBankAccountBulkProcessBean> landingBankAccountBulkProcessBeans =
			 landingBankAccountBulkProcessReader.readAllBeans();
			
			// Create landing bank account writer to write into main data space
			LandingBankAccountBulkProcessWriter landingBankAccountWriter = new LandingBankAccountBulkProcessWriterSession(context.getSession(), true, dataSpace);
			List<LandingBankAccountBulkProcessBean> landingBankAccountBulkProcessToDelete = new ArrayList<LandingBankAccountBulkProcessBean>();

			// Create staging party reader to read from main data space
			StagingPartyReader stagingPartyReader = new StagingPartyReader(stagingDataspace);

			// Create staging bank account writer to write into main data space
			// StagingBankAccountWriter stagingBankAccountWriter = new StagingBankAccountWriterSession(context.getSession(), true, stagingDataspace);
			// StagingBankAccountReader stagingBankAccountReader = (StagingBankAccountReader) stagingBankAccountWriter.getTableReader();

			// Group landing bankAccountes by SourceSystem and SourcePartyBusId
			Map<String, List<LandingBankAccountBulkProcessBean>> landingBankAccountBeansGroupedBySourceSystem = landingBankAccountBulkProcessBeans
					.stream().collect(Collectors.groupingBy(lab -> lab.getSourceSystem() + lab.getSourcePartyBusId()));

			List<RecordValuesBean> stagingBeansToCreate = new ArrayList<RecordValuesBean>();
			List<StagingBankAccountBean> stagingBeansToUpdate = new ArrayList<StagingBankAccountBean>();
			List<RecordValuesBean> stagingBeanToDelete = new ArrayList<RecordValuesBean>();
			
			for (Map.Entry<String, List<LandingBankAccountBulkProcessBean>> sourceSystemAndBusId : landingBankAccountBeansGroupedBySourceSystem.entrySet()) {
				List<LandingBankAccountBulkProcessBean> stagingBankAccountBeanList = sourceSystemAndBusId.getValue();
				for (LandingBankAccountBulkProcessBean landingBankAccountBulkProcessBean : stagingBankAccountBeanList) {
					
					boolean landingBankAccountBeanInvalid = landingBankAccountBulkProcessReader.hasValidationErrors(new Object[] { landingBankAccountBulkProcessBean.getId() });
					if (landingBankAccountBeanInvalid) {
						continue;
					}
					
					String landingPartyBulkProcessId = landingBankAccountBulkProcessBean.getLandingPartyId();
					boolean parentPartyHasNoErrors = findParentPartyHasValidationErrorsOrNot(landingPartyBulkProcessId, dataSpace);
					if (!parentPartyHasNoErrors) {
						landingBankAccountBulkProcessToDelete.add(landingBankAccountBulkProcessBean);
						String sourceSystem = landingBankAccountBulkProcessBean.getSourceSystem();
						String sourcePartyBusId = landingBankAccountBulkProcessBean.getSourcePartyBusId();
						// Check for available related staging party
						List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId);
						// If none are available, write the bank account into landing
						if (stagingPartyBeans.isEmpty()) {
							try {
								landingBankAccountWriter.createBeanOrThrow(landingBankAccountBulkProcessBean);
							} catch (Exception ex) {
								stringBuilderErrorMessage.append(String.format("Error while creating staging bank account bean with source system \"%s\" and source bank account bus id \"%s\"\r\n",
										landingBankAccountBulkProcessBean.getSourceSystem(),
										landingBankAccountBulkProcessBean.getSourceBankAccountBusId()));
								stringBuilderErrorMessage.append(ex.toString() + "\r\n");
							}
						}

						StagingBankAccountBean bankAccountBean = com.elipslife.ebx.domain.bean.convert.BankAccountConverter.createStagingBankAccountBeanFromLandingBankAccountBulkProcessBean(
										landingBankAccountBulkProcessBean);

						try {
							HashMap<Path, Object> values = StagingBankAccountConverter.fillStagingBankAccountBeanWithLandingBankAccountBulkProcessBean(bankAccountBean, true);
							RecordValuesBean rvb = new RecordValuesBean(values);
							stagingBeansToCreate.add(rvb);
							recordCount++;
						} catch (Exception ex) {
							stringBuilderErrorMessage.append(String.format("Error while creating staging bank account bean with source system \"%s\" and source bank account bus id \"%s\"\r\n",
									bankAccountBean.getSourceSystem(),
									bankAccountBean.getSourceBankAccountBusId()));
							stringBuilderErrorMessage.append(ex.toString() + "\r\n");
						}
					}
					
					if (recordCount > Integer.parseInt(commitSize)) {
						try {
							createStagingBankAccounts(stagingBeansToCreate, context.getSession(), stagingDataspace);
							updateStagingBankAccounts(stagingBeansToUpdate, context.getSession(), stagingDataspace);
							stagingBeansToCreate.clear();
							stagingBeansToUpdate.clear();
							stagingBeanToDelete.clear();
							recordCount = 1;
						} catch (OperationException e) {
							e.printStackTrace();
						}
					}
				}
			}
			if (!stagingBeansToCreate.isEmpty() || !stagingBeansToUpdate.isEmpty() || !stagingBeanToDelete.isEmpty()) {
				try {
					createStagingBankAccounts(stagingBeansToCreate, context.getSession(), stagingDataspace);
					updateStagingBankAccounts(stagingBeansToUpdate, context.getSession(), stagingDataspace);
					stagingBeansToCreate.clear();
					stagingBeansToUpdate.clear();
					stagingBeanToDelete.clear();
					recordCount = 1;
				} catch (OperationException e) {
					e.printStackTrace();
				}
			}
			
			// Deleting all landing records
			deleteLandingRecords(landingBankAccountBulkProcessToDelete, context.getSession(), dataSpace);
		} else {

			// Create landing bank account reader to read from child data space
			LandingBankAccountReader landingBankAccountReader = new LandingBankAccountReader(dataSpace);
			List<LandingBankAccountBean> landingBankAccountBeans = landingBankAccountReader.readAllBeans();

			// Create landing bank account writer to write into main data space
			LandingBankAccountWriter landingBankAccountWriter = new LandingBankAccountWriterSession(context.getSession(), true);

			// Create staging party reader to read from main data space
			StagingPartyReader stagingPartyReader = new StagingPartyReader();

			// Create staging bank account writer to write into main data space
			StagingBankAccountWriter stagingBankAccountWriter = new StagingBankAccountWriterSession(context.getSession(), true);
			StagingBankAccountReader stagingBankAccountReader = (StagingBankAccountReader) stagingBankAccountWriter.getTableReader();

			// Group landing bankAccountes by SourceSystem and SourcePartyBusId
			Map<String, List<LandingBankAccountBean>> landingBankAccountBeansGroupedBySourceSystem = landingBankAccountBeans.stream().collect(Collectors.groupingBy(lab -> lab.getSourceSystem() + lab.getSourcePartyBusId()));

			landingBankAccountBeansGroupedBySourceSystem.forEach((key, landingBankAccountBeansFromGroup) -> {
				String sourceSystem = landingBankAccountBeansFromGroup.get(0).getSourceSystem();
				String sourcePartyBusId = landingBankAccountBeansFromGroup.get(0).getSourcePartyBusId();

				// Check for available related staging party
				List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId);

				// If none are available, write the bank account into landing
				if (stagingPartyBeans.isEmpty()) {
					landingBankAccountBeansFromGroup.forEach(lab -> {
						try {
							landingBankAccountWriter.createBeanOrThrow(lab);
						} catch (Exception ex) {
							stringBuilderErrorMessage.append(String.format("Error while creating staging bank account bean with source system \"%s\" and source bank account bus id \"%s\"\r\n", lab.getSourceSystem(), lab.getSourceBankAccountBusId()));
							stringBuilderErrorMessage.append(ex.toString() + "\r\n");
						}
					});

					return;
				}

				// Create, update or delete staging bank accounts
				List<StagingBankAccountBean> stagingBankAccountBeans = stagingBankAccountReader.findBeansByParty(sourceSystem, sourcePartyBusId);

				HashMap<Character, List<StagingBankAccountBean>> changeSetBankAccounts = ChangeSetBuilder.buildStagingBankAccountBeansChangeSet(landingBankAccountBeansFromGroup, stagingBankAccountBeans);
				changeSetBankAccounts.forEach((operation, stagingAddressBeansList) -> {
					if (operation == PartyConstants.RecordOperations.CREATE.charAt(0)) {
						stagingAddressBeansList.stream().forEach(sab -> {

							try {
								stagingBankAccountWriter.createBeanOrThrow(sab);
							} catch (Exception ex) {
								stringBuilderErrorMessage.append(String.format("Error while creating staging bank account bean with source system \"%s\" and source bank account bus id \"%s\"\r\n", sab.getSourceSystem(), sab.getSourceBankAccountBusId()));
								stringBuilderErrorMessage.append(ex.toString() + "\r\n");
							}
						});
					} else if (operation == PartyConstants.RecordOperations.UPDATE.charAt(0)) {
						stagingAddressBeansList.stream()
								.sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getLastUpdateTimestamp(), b.getLastUpdateTimestamp())).forEach(sab -> {
									try {
										stagingBankAccountWriter.updateBean(sab);
									} catch (Exception ex) {
										stringBuilderErrorMessage.append(String.format("Error while updating staging bank account bean with source system \"%s\" and source bank account bus id \"%s\"\r\n", sab.getSourceSystem(), sab.getSourceBankAccountBusId()));
										stringBuilderErrorMessage.append(ex.toString() + "\r\n");
									}
								});
					} else if (operation == PartyConstants.RecordOperations.DELETE.charAt(0)) {
						stagingAddressBeansList.stream().forEach(sab -> {
							try {
								stagingBankAccountWriter.delete(sab);
							} catch (Exception ex) {
								stringBuilderErrorMessage.append(String.format("Error while deleting staging bank account bean with source system \"%s\" and source bank account bus id \"%s\"\r\n", sab.getSourceSystem(), sab.getSourceBankAccountBusId()));
								stringBuilderErrorMessage.append(ex.toString() + "\r\n");
							}
						});
					}
				});
			});
		}
		
		// Update error messages in DataContext variables
		updateErrorMessageInDataContext(stringBuilderErrorMessage, context);
	}

	/**
	 * 
	 * @param dataContext
	 * @return
	 */
	private static HashMap<String, String> getContextVariables(com.orchestranetworks.workflow.DataContextReadOnly dataContext) {

		HashMap<String, String> hashMap = new HashMap<String, String>();
		Iterator<String> variableNames = dataContext.getVariableNames();
		while (variableNames.hasNext()) {
			String variableName = variableNames.next();
			String variableValue = dataContext.getVariableString(variableName);
			hashMap.put(variableName, variableValue);
		}
		return hashMap;
	}

	private static void updateStagingBankAccounts(List<StagingBankAccountBean> stagingBankAccountListToUpdate,
			Session session, String dataSpace) throws OperationException {

		AdaptationTable aTable = getStagingDataSet().getTable(PartyPaths._STG_BankAccount.getPathInSchema());
		final ProgrammaticService service = ProgrammaticService.createForSession(session, getStagingDataSpaceHome());
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();

		HashMap<Path, Object> values = null;
		RecordValuesBean rvb = null;
		for (StagingBankAccountBean stagingBankAccountBean : stagingBankAccountListToUpdate) {
			values = new HashMap<Path, Object>();
			values = StagingBankAccountConverter.fillStagingBankAccountBeanWithLandingBankAccountBulkProcessBean(stagingBankAccountBean, true);
			rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(stagingBankAccountBean.getId().toString())), values);
			recordValuesBeanList.add(rvb);
		}

		UpdateRecords createRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(createRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}

	private static void createStagingBankAccounts(List<RecordValuesBean> stagingBankAccountListToCreate,
			Session session, String dataSpace) throws OperationException {

		AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._STG_BankAccount.getPathInSchema());
		final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);
		CreateRecords createRecordsProcedure = new CreateRecords(aTable, stagingBankAccountListToCreate);
		ProcedureResult result = service.execute(createRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}

	private static void deleteLandingRecords(
			List<LandingBankAccountBulkProcessBean> landingBankAccountBulkProcessBeansList, Session session,
			String dataSpace) throws OperationException {

		AdaptationTable aTable = getLandingDataSet().getTable(LandingPaths._LDG_BankAccount_BulkProcess.getPathInSchema());
		final ProgrammaticService service = ProgrammaticService.createForSession(session, getTemplandingDataspace());
		ArrayList<Adaptation> recordValuesBeanList = new ArrayList<Adaptation>();
		for (LandingBankAccountBulkProcessBean landingBankAccountBean : landingBankAccountBulkProcessBeansList) {
			recordValuesBeanList.add(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(landingBankAccountBean.getId().toString())));
		}

		DeleteRecords deleteRecordsProcedure = new DeleteRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(deleteRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}
	
	/**
	 * Trim and Update errorMessages in DataContext variables
	 * 
	 * @param stringBuilderErrorMessage
	 * @param aContext
	 */
	private void updateErrorMessageInDataContext(StringBuilder stringBuilderErrorMessage, ScriptTaskContext aContext) {
		
		if(stringBuilderErrorMessage != null) {
			StringBuffer errorMessage = new StringBuffer();
			String errorMessages = aContext.getVariableString(PartyConstants.DataContextVariables.ERROR_MESSAGES);
			if(errorMessages != null && errorMessages.length() > 2000) {
				String errorMessageString = errorMessages.substring(errorMessages.length() - 2000);
				errorMessage.append(errorMessageString);
				if(stringBuilderErrorMessage.length() > 2000) {
					errorMessage.append(stringBuilderErrorMessage.substring(stringBuilderErrorMessage.length() - 2000));
				} else {
					errorMessage.append(stringBuilderErrorMessage);
				}
				errorMessage.append(" Please look in to kernel.log file to see more errors");
			} else {
				if(errorMessages != null) {
					errorMessage.append(errorMessages);
				}
				if(stringBuilderErrorMessage.length() > 2000) {
					errorMessage.append(stringBuilderErrorMessage.substring(stringBuilderErrorMessage.length() - 2000));
					errorMessage.append(" Please look in to kernel.log file to see more errors");
				} else {
					errorMessage.append(stringBuilderErrorMessage);
				}
			}
			aContext.setVariableString(PartyConstants.DataContextVariables.ERROR_MESSAGES, errorMessage.toString());
		}
	}

	/**
	 * Check Parent Party has any validation error messages or not
	 * 
	 * @param landingPartyBulkProcessId
	 * @param dataSpace
	 * @return
	 */
	private boolean findParentPartyHasValidationErrorsOrNot(String landingPartyBulkProcessId, String dataSpace) {

		try {
			if (landingPartyBulkProcessId != null) {
				LandingPartyBulkProcessReader bulkProcessReader = new LandingPartyBulkProcessReader(dataSpace);
				LandingPartyBulkProcessBean bulkProcessBean = bulkProcessReader.findBeanByPartyId(Integer.parseInt(landingPartyBulkProcessId));
				if (bulkProcessBean != null) {
					return true;
				}
			}
		} catch (Exception e) { }

		return false;
	}

	public static AdaptationHome getStagingDataSpaceHome() {
		return stagingDataSpaceHome;
	}

	public void setStagingDataSpaceHome(AdaptationHome stagingDataSpace) {
		stagingDataSpaceHome = stagingDataSpace;
	}

	public static AdaptationHome getTemplandingDataspace() {
		return templandingDataspace;
	}

	public void setTemplandingDataspace(AdaptationHome tempLandingDataspace) {
		templandingDataspace = tempLandingDataspace;
	}

	public static Adaptation getStagingDataSet() {
		return stagingDataSet;
	}

	public void setStagingDataSet(AdaptationHome stagingDataspace) {
		stagingDataSet = stagingDataspace
				.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.PARTY_MASTER));
	}

	public static Adaptation getLandingDataSet() {
		return landingDataSet;
	}

	public void setLandingDataSet(AdaptationHome landingAdaptationHome) {
		landingDataSet = landingAdaptationHome
				.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.LANDING_MASTER));
	}
}
