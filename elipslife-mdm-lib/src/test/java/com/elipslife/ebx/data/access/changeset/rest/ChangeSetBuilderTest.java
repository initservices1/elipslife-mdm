package com.elipslife.ebx.data.access.changeset.rest;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.elipslife.ebx.data.access.changeset.ChangeSetBuilder;
import com.elipslife.ebx.domain.bean.LandingAddressBean;
import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ChangeSetBuilderTest {

	@Test
	public void testOperationsAvailable() {

		List<LandingAddressBean> landingAddressBeanList = new ArrayList<>();
		List<StagingAddressBean> stagingAddressBeanList = new ArrayList<>();

		HashMap<Character, List<StagingAddressBean>> changeSet = ChangeSetBuilder.buildStagingAddressBeanChangeSet(landingAddressBeanList, stagingAddressBeanList);

		assertTrue(changeSet.containsKey(PartyConstants.RecordOperations.UPDATE.charAt(0)), "Operation U is not contained");
		assertTrue(changeSet.containsKey(PartyConstants.RecordOperations.CREATE.charAt(0)), "Operation C is not contained");
		assertTrue(changeSet.containsKey(PartyConstants.RecordOperations.DELETE.charAt(0)), "Operation D is not contained");
	}

	@Test
	public void testCreateOneAddress() {

		List<LandingAddressBean> landingAddressBeanList = new ArrayList<>();
		List<StagingAddressBean> stagingAddressBeanList = new ArrayList<>();

		LandingAddressBean landingAddressBean = new LandingAddressBean();
		landingAddressBeanList.add(landingAddressBean);

		landingAddressBean.setSourceSystem("BBTL");
		landingAddressBean.setSourcePartyBusId("1");

		HashMap<Character, List<StagingAddressBean>> changeSet = ChangeSetBuilder.buildStagingAddressBeanChangeSet(landingAddressBeanList, stagingAddressBeanList);

		assertTrue(changeSet.get(PartyConstants.RecordOperations.UPDATE.charAt(0)).isEmpty());
		assertTrue(changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).isEmpty());
		assertTrue(!changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).isEmpty());

		StagingAddressBean stagingAddressBeanToCreate = changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).get(0);
		assertEquals(landingAddressBean.getSourceSystem(), stagingAddressBeanToCreate.getSourceSystem());
	}

	@Test
	public void testUpdateOneAddress() {
		
		List<LandingAddressBean> landingAddressBeanList = new ArrayList<>();
		List<StagingAddressBean> stagingAddressBeanList = new ArrayList<>();
		
		LandingAddressBean landingAddressBean = new LandingAddressBean();
		landingAddressBeanList.add(landingAddressBean);
		landingAddressBean.setSourceSystem("BBTL");
		landingAddressBean.setSourcePartyBusId("1");
		landingAddressBean.setDistrict("District 2");
		landingAddressBean.setCreationTimestamp(Date.from(Instant.now().plusSeconds(3600)));
		
		StagingAddressBean stagingAddressBean = new StagingAddressBean();
		stagingAddressBeanList.add(stagingAddressBean);
		stagingAddressBean.setSourceSystem("BBTL");
		stagingAddressBean.setSourcePartyBusId("1");
		stagingAddressBean.setDistrict("District 1");
		stagingAddressBean.setCreationTimestamp(Date.from(Instant.now()));
		
		HashMap<Character, List<StagingAddressBean>> changeSet = ChangeSetBuilder.buildStagingAddressBeanChangeSet(landingAddressBeanList, stagingAddressBeanList);
		
		assertTrue(changeSet.get(PartyConstants.RecordOperations.UPDATE.charAt(0)).size() == 1);
		assertTrue(changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).isEmpty());
		assertTrue(changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).isEmpty());
	}

	@Test
	public void testUpdateOneAddressWithLastUpdateTimestamp() {

		List<LandingAddressBean> landingAddressBeanList = new ArrayList<>();
		List<StagingAddressBean> stagingAddressBeanList = new ArrayList<>();

		Calendar calendar = Calendar.getInstance();

		LandingAddressBean landingAddressBean = new LandingAddressBean();
		landingAddressBeanList.add(landingAddressBean);
		landingAddressBean.setSourceSystem("BBTL");
		landingAddressBean.setSourcePartyBusId("1");
		landingAddressBean.setDistrict("District 2");
		calendar.set(2019, 1, 1);
		landingAddressBean.setLastUpdateTimestamp(calendar.getTime());

		StagingAddressBean stagingAddressBean = new StagingAddressBean();
		stagingAddressBeanList.add(stagingAddressBean);
		stagingAddressBean.setSourceSystem("BBTL");
		stagingAddressBean.setSourcePartyBusId("1");
		stagingAddressBean.setDistrict("District 1");
		calendar.set(2019, 1, 2);
		stagingAddressBean.setLastUpdateTimestamp(calendar.getTime());

		HashMap<Character, List<StagingAddressBean>> changeSet = ChangeSetBuilder.buildStagingAddressBeanChangeSet(landingAddressBeanList, stagingAddressBeanList);

		assertTrue(changeSet.get(PartyConstants.RecordOperations.UPDATE.charAt(0)).size() == 1);
		assertTrue(changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).isEmpty());
		assertTrue(changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).isEmpty());
	}

	@Test
	public void testDeleteOneAddress() {

		List<LandingAddressBean> landingAddressBeanList = new ArrayList<>();
		List<StagingAddressBean> stagingAddressBeanList = new ArrayList<>();

		StagingAddressBean stagingAddressBean = new StagingAddressBean();
		stagingAddressBeanList.add(stagingAddressBean);
		stagingAddressBean.setSourceSystem("BBTL");
		stagingAddressBean.setSourcePartyBusId("1");
		stagingAddressBean.setDistrict("District 1");

		HashMap<Character, List<StagingAddressBean>> changeSet = ChangeSetBuilder.buildStagingAddressBeanChangeSet(landingAddressBeanList, stagingAddressBeanList);

		assertTrue(changeSet.get(PartyConstants.RecordOperations.UPDATE.charAt(0)).isEmpty());
		assertTrue(!changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).isEmpty());
		assertTrue(changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).isEmpty());

		StagingAddressBean stagingAddressBeanToDelete = changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).get(0);
		assertEquals(stagingAddressBean.getSourceSystem(), stagingAddressBeanToDelete.getSourceSystem());
		assertEquals(stagingAddressBean.getSourcePartyBusId(), stagingAddressBeanToDelete.getSourcePartyBusId());
	}

	@Test
	public void testCreateOneAddressWithSourceAddressBusId() {

		List<LandingAddressBean> landingAddressBeanList = new ArrayList<>();
		List<StagingAddressBean> stagingAddressBeanList = new ArrayList<>();

		LandingAddressBean landingAddressBean = new LandingAddressBean();
		landingAddressBeanList.add(landingAddressBean);

		landingAddressBean.setSourceSystem("BBTL");
		landingAddressBean.setSourcePartyBusId("1");
		landingAddressBean.setSourceAddressBusId("1");

		HashMap<Character, List<StagingAddressBean>> changeSet = ChangeSetBuilder.buildStagingAddressBeanChangeSet(landingAddressBeanList, stagingAddressBeanList);

		assertTrue(changeSet.get(PartyConstants.RecordOperations.UPDATE.charAt(0)).isEmpty());
		assertTrue(changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).isEmpty());
		assertTrue(!changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).isEmpty());

		StagingAddressBean stagingAddressBeanToCreate = changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).get(0);
		assertEquals(landingAddressBean.getSourceAddressBusId(), stagingAddressBeanToCreate.getSourceAddressBusId());
	}

	@Test
	public void testUpdateOneAddressWithSourceAddressBusId() {
		
		List<LandingAddressBean> landingAddressBeanList = new ArrayList<>();
		List<StagingAddressBean> stagingAddressBeanList = new ArrayList<>();
		
		LandingAddressBean landingAddressBean = new LandingAddressBean();
		landingAddressBeanList.add(landingAddressBean);
		landingAddressBean.setSourceSystem("BBTL");
		landingAddressBean.setSourcePartyBusId("1");
		landingAddressBean.setSourceAddressBusId("1");
		landingAddressBean.setDistrict("District 2");
		landingAddressBean.setCreationTimestamp(Date.from(Instant.now().plusSeconds(3600)));
		
		StagingAddressBean stagingAddressBean = new StagingAddressBean();
		stagingAddressBeanList.add(stagingAddressBean);
		stagingAddressBean.setSourceSystem("BBTL");
		stagingAddressBean.setSourcePartyBusId("1");
		stagingAddressBean.setSourceAddressBusId("1");
		stagingAddressBean.setDistrict("District 1");
		stagingAddressBean.setCreationTimestamp(Date.from(Instant.now()));
		
		HashMap<Character, List<StagingAddressBean>> changeSet = ChangeSetBuilder.buildStagingAddressBeanChangeSet(landingAddressBeanList, stagingAddressBeanList);
		
		assertTrue(changeSet.get(PartyConstants.RecordOperations.UPDATE.charAt(0)).size() == 1);
		assertTrue(changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).isEmpty());
		assertTrue(changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).isEmpty());
	}

	@Test
	public void testUpdateOneAddressWithSourceAddressBusIdWithLastUpdateTimestamp() {

		List<LandingAddressBean> landingAddressBeanList = new ArrayList<>();
		List<StagingAddressBean> stagingAddressBeanList = new ArrayList<>();

		Calendar calendar = Calendar.getInstance();

		LandingAddressBean landingAddressBean = new LandingAddressBean();
		landingAddressBeanList.add(landingAddressBean);
		landingAddressBean.setSourceSystem("BBTL");
		landingAddressBean.setSourcePartyBusId("1");
		landingAddressBean.setSourceAddressBusId("1");
		landingAddressBean.setDistrict("District 2");
		calendar.set(2019, 1, 1);
		landingAddressBean.setLastUpdateTimestamp(calendar.getTime());

		StagingAddressBean stagingAddressBean = new StagingAddressBean();
		stagingAddressBeanList.add(stagingAddressBean);
		stagingAddressBean.setSourceSystem("BBTL");
		stagingAddressBean.setSourcePartyBusId("1");
		stagingAddressBean.setSourceAddressBusId("1");
		stagingAddressBean.setDistrict("District 1");
		calendar.set(2019, 1, 2);
		stagingAddressBean.setLastUpdateTimestamp(calendar.getTime());

		HashMap<Character, List<StagingAddressBean>> changeSet = ChangeSetBuilder.buildStagingAddressBeanChangeSet(landingAddressBeanList, stagingAddressBeanList);

		assertTrue(changeSet.get(PartyConstants.RecordOperations.UPDATE.charAt(0)).isEmpty());
		assertTrue(changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).isEmpty());
		assertTrue(changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).isEmpty());
	}

	@Test
	public void testDeleteOneAddressWithSourceAddressBusId() {

		List<LandingAddressBean> landingAddressBeanList = new ArrayList<>();
		List<StagingAddressBean> stagingAddressBeanList = new ArrayList<>();

		StagingAddressBean stagingAddressBean = new StagingAddressBean();
		stagingAddressBeanList.add(stagingAddressBean);
		stagingAddressBean.setSourceSystem("BBTL");
		stagingAddressBean.setSourcePartyBusId("1");
		stagingAddressBean.setSourceAddressBusId("1");
		stagingAddressBean.setDistrict("District 1");

		HashMap<Character, List<StagingAddressBean>> changeSet = ChangeSetBuilder.buildStagingAddressBeanChangeSet(landingAddressBeanList, stagingAddressBeanList);

		assertTrue(changeSet.get(PartyConstants.RecordOperations.UPDATE.charAt(0)).isEmpty());
		assertTrue(!changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).isEmpty());
		assertTrue(changeSet.get(PartyConstants.RecordOperations.CREATE.charAt(0)).isEmpty());

		StagingAddressBean stagingAddressBeanToDelete = changeSet.get(PartyConstants.RecordOperations.DELETE.charAt(0)).get(0);
		assertEquals(stagingAddressBean.getSourceSystem(), stagingAddressBeanToDelete.getSourceSystem());
		assertEquals(stagingAddressBean.getSourcePartyBusId(), stagingAddressBeanToDelete.getSourcePartyBusId());
	}
}