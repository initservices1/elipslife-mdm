package com.elipslife.mdm.claimcase.core;

import java.util.List;

import com.elipslife.ebx.data.access.CorePartyIdentifierReader;
import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.mdm.claimcase.constants.ClaimCaseConstants;
import com.elipslife.mdm.claimcase.constants.ClaimCasePaths;
import com.elipslife.mdm.common.rules.ElipsLifeCommonRules;
import com.elipslife.mdm.hr.core.EmployeeBean;
import com.elipslife.mdm.hr.core.EmployeeReader;
import com.elipslife.mdm.hr.core.OrganisationBean;
import com.elipslife.mdm.hr.core.OrganisationReader;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.XPathFilter;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TableTriggerExecutionContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class ClaimCaseTableTrigger extends TableTrigger {
	
	
	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		super.handleBeforeCreate(aContext);
		
		updateClaimCaseFksInCoreTable(aContext, aContext.getOccurrenceContextForUpdate());
	}

	
	public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
		super.handleBeforeModify(aContext);
		
		updateClaimCaseFksInCoreTable(aContext, aContext.getOccurrenceContextForUpdate());
		
	}
	
	private void updateClaimCaseFksInCoreTable(TableTriggerExecutionContext aContext, ValueContextForUpdate valueContext) {
		
		// foreign key computation
		Adaptation partySet = RepositoryHelper.getDataSet(RepositoryHelper.getDataSpace(ClaimCaseConstants.DataSpace.PARTY_REF), ClaimCaseConstants.DataSet.PARTY_MASTER);
		AdaptationTable partyTable = RepositoryHelper.getTable(partySet, PartyPaths._Party.getPathInSchema());
		Request request = partyTable.createRequest();
		XPathFilter partyTableFilter = XPathFilter.newFilter("./PartyInfo/PartyBusId=$partyBusId");
		
		// Policy Holder Id
		String policyHolderBusId = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Party_PolicyholderBusId);
		if (policyHolderBusId != null) {
			RequestResult policyHolderResult = getRequestResultFromXPathAndFilterForPartyIdentifier(partyTableFilter, policyHolderBusId, request);
			if (!policyHolderResult.isEmpty()) {
				for (Adaptation adap; (adap = policyHolderResult.nextAdaptation()) != null;) {
					valueContext.setValue(String.valueOf(adap.get_int(PartyPaths._Party._PartyInfo_PartyId)), ClaimCasePaths._ClaimCase._Party_PolicyholderId);
				}
				policyHolderResult.close();
			} else {
				valueContext.setValue(null, ClaimCasePaths._ClaimCase._Party_PolicyholderId);
			}
		}
		
		// Main Affiliate Id
		String mainAffiliateBusId = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Party_MainAffiliateBusId);
		if (mainAffiliateBusId != null) {
			RequestResult mainAffiliateResult = getRequestResultFromXPathAndFilterForPartyIdentifier(partyTableFilter, mainAffiliateBusId, request);
			if (!mainAffiliateResult.isEmpty()) {
				for (Adaptation adap; (adap = mainAffiliateResult.nextAdaptation()) != null;) {
					valueContext.setValue(String.valueOf(adap.get_int(PartyPaths._Party._PartyInfo_PartyId)), ClaimCasePaths._ClaimCase._Party_MainAffiliateId);
				}
				mainAffiliateResult.close();
			} else {
				valueContext.setValue(null, ClaimCasePaths._ClaimCase._Party_MainAffiliateId);
			}
		} else {
			String sourceMainAffiliateBusId = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Party_SourceMainAffiliateBusId);
			if(sourceMainAffiliateBusId != null) {
				String sourceSystem = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Control_SourceSystem);
				CorePartyIdentifierBean partyIdentifier = getPartyIdentifierBySourceSystemAndBusId(sourceSystem, sourceMainAffiliateBusId);
				if(partyIdentifier != null) {
					valueContext.setValue(partyIdentifier.getPartyId(), ClaimCasePaths._ClaimCase._Party_MainAffiliateId);
				}
			}
		}
		
		// Insured Person Id
		String insuredPersonBusId = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Party_InsuredPersonBusId);
		if (insuredPersonBusId != null) {
			RequestResult insuredPersonResult = getRequestResultFromXPathAndFilterForPartyIdentifier(partyTableFilter, insuredPersonBusId, request);
			if (!insuredPersonResult.isEmpty()) {
				for (Adaptation adap; (adap = insuredPersonResult.nextAdaptation()) != null;) {
					valueContext.setValue(String.valueOf(adap.get_int(PartyPaths._Party._PartyInfo_PartyId)), ClaimCasePaths._ClaimCase._Party_InsuredPersonId);
				}
				insuredPersonResult.close();
			} else { 
				valueContext.setValue(null, ClaimCasePaths._ClaimCase._Party_InsuredPersonId);
			}
		} else {
			String sourceInsurencePersonId = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Party_SourceInsuredPersonBusId);
			if(sourceInsurencePersonId != null) {
				String sourceSystem = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Control_SourceSystem);
				CorePartyIdentifierBean partyIdentifier = getPartyIdentifierBySourceSystemAndBusId(sourceSystem, sourceInsurencePersonId);
				if(partyIdentifier != null) {
					valueContext.setValue(partyIdentifier.getPartyId(), ClaimCasePaths._ClaimCase._Party_InsuredPersonId);
				}
			}
		}
		
		// Claim Mgtm Tean Id
		String ClaimMgtmTeamBusId = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Party_ClaimMgtmTeamBusId);
		if (ClaimMgtmTeamBusId != null) {
			
			OrganisationReader organisationReader = new OrganisationReader();
			List<OrganisationBean> organisationBeans = organisationReader.findBeansByOrganisationBusId(ClaimMgtmTeamBusId);
			if(!organisationBeans.isEmpty()) {
				OrganisationBean organisationBean = organisationBeans.get(0);
				valueContext.setValue(organisationBean.getOrganisationId(), ClaimCasePaths._ClaimCase._Party_ClaimMgtmTeamId);
			} else {
				valueContext.setValue(null, ClaimCasePaths._ClaimCase._Party_ClaimMgtmTeamId);
			}
			
		}		
		
		// Claim Manager Id
		String claimManagerUserName = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Party_ClaimManagerUserName);
		if(claimManagerUserName != null) {
			
			EmployeeReader employeeReader = new EmployeeReader();
			List<EmployeeBean> employeeBeans = employeeReader.findBeansByUserName(claimManagerUserName);
			if(!employeeBeans.isEmpty()) {
				EmployeeBean employeeRecordBean = employeeBeans.get(0);
				valueContext.setValue(employeeRecordBean.getEmployeeId(), ClaimCasePaths._ClaimCase._Party_ClaimManagerId);
			} else {
				valueContext.setValue(null, ClaimCasePaths._ClaimCase._Party_ClaimManagerId);
			}
		}
		
		// Insurance Contract Id
		// String contractNumber = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._ClaimCase_General_ContractNumber);
		String sourceSystem = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._Control_SourceSystem);
		String sourceContractBusId = (String) aContext.getOccurrenceContext().getValue(ClaimCasePaths._ClaimCase._ClaimCase_General_SourceContractBusId);
		if(sourceSystem != null && sourceContractBusId != null) {
			String insuranceContractId = ElipsLifeCommonRules.getInsuranceContractIdFromContractNumber(sourceSystem, sourceContractBusId);
			valueContext.setValue(insuranceContractId, ClaimCasePaths._ClaimCase._ClaimCase_General_InsuranceContractId);
		}
		
	}
	
	public RequestResult getRequestResultFromXPathAndFilterForPartyIdentifier(XPathFilter filter, String partyBusId, Request request) {
		request.setXPathFilter(filter);
		request.setXPathParameter("partyBusId", partyBusId);
		//request.setXPathParameter("busId", busId);
		return request.execute();
	}
	
	private CorePartyIdentifierBean getPartyIdentifierBySourceSystemAndBusId(String sourceSystem, String sourcePartyBusId) {
		CorePartyIdentifierReader partyIdentifierReader = new CorePartyIdentifierReader();
		List<CorePartyIdentifierBean> corePartyIdentifierBeans = partyIdentifierReader.getBeansBySourceSystem(sourceSystem, sourcePartyBusId);
		if (corePartyIdentifierBeans.isEmpty()) {
			return null;
		}
		CorePartyIdentifierBean corePartyIdentifierBean = corePartyIdentifierBeans.get(0);
		
		return corePartyIdentifierBean;
	}
	
	@Override
	public void setup(TriggerSetupContext aContext) {
		
	}

}
