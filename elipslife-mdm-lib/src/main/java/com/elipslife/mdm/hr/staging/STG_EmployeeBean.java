package com.elipslife.mdm.hr.staging;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class STG_EmployeeBean extends Entity<Integer>{
    private Integer EmployeeId;
    private String DataOwner;
    private String EmployeeBusId;
    private String UserName;
    private String FirstName;
    private String PreferredFirstName;
    private String MiddleName;
    private String LastName;
    private String Gender;
    private String Salutation;
    private String Language;
    private String Email;
    private String Telephone;
    private String Mobile;
    private String Fax;
    private Boolean IsActive;
    private Boolean IsSystemEnabled;
    private String EmploymentCategory;
    private String ManagementLevel;
    private String SignOffRole;
    private String FunctionalTitle;
    private String PositionName;
    private String JobProfileName;
    private Boolean IsLineManager;
    private String OrganisationBusId;
    private String LineManagerBusId;
    private String DottedLineManagerBusId;
    private String BuildingBusId;
    
    // Employee Private Address
    private String PrivateStreet;
    private String PrivatePostCode;
    private String PrivateTown;
    private String PrivateStateProvince;
    private String PrivateCountryCode;
    
    private String EmployerBusId;
    private String EmployeeMaster;
    private String STG_Control;
    
    // Control
    private String sourceSystem;
	private String createdBy;
	private Date creationTimeStamp;
	private String lastUpdatedBy;
	private Date lastUpdateTimeStamp;
	private Date lastSyncTimestamp;
	
    public Integer getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Integer EmployeeId) {
        this.EmployeeId = EmployeeId;
    }

    public String getDataOwner() {
        return DataOwner;
    }

    public void setDataOwner(String DataOwner) {
        this.DataOwner = DataOwner;
    }

    public String getEmployeeBusId() {
        return EmployeeBusId;
    }

    public void setEmployeeBusId(String EmployeeBusId) {
        this.EmployeeBusId = EmployeeBusId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getPreferredFirstName() {
        return PreferredFirstName;
    }

    public void setPreferredFirstName(String PreferredFirstName) {
        this.PreferredFirstName = PreferredFirstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String MiddleName) {
        this.MiddleName = MiddleName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getSalutation() {
        return Salutation;
    }

    public void setSalutation(String Salutation) {
        this.Salutation = Salutation;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String Language) {
        this.Language = Language;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String Telephone) {
        this.Telephone = Telephone;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String Mobile) {
        this.Mobile = Mobile;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String Fax) {
        this.Fax = Fax;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    public Boolean getIsSystemEnabled() {
        return IsSystemEnabled;
    }

    public void setIsSystemEnabled(Boolean IsSystemEnabled) {
        this.IsSystemEnabled = IsSystemEnabled;
    }

    public String getEmploymentCategory() {
        return EmploymentCategory;
    }

    public void setEmploymentCategory(String EmploymentCategory) {
        this.EmploymentCategory = EmploymentCategory;
    }

    public String getManagementLevel() {
        return ManagementLevel;
    }

    public void setManagementLevel(String ManagementLevel) {
        this.ManagementLevel = ManagementLevel;
    }

    public String getSignOffRole() {
        return SignOffRole;
    }

    public void setSignOffRole(String SignOffRole) {
        this.SignOffRole = SignOffRole;
    }

    public Boolean getIsLineManager() {
        return IsLineManager;
    }

    public void setIsLineManager(Boolean IsLineManager) {
        this.IsLineManager = IsLineManager;
    }

    public String getOrganisationBusId() {
        return OrganisationBusId;
    }

    public void setOrganisationBusId(String OrganisationBusId) {
        this.OrganisationBusId = OrganisationBusId;
    }

    public String getLineManagerBusId() {
        return LineManagerBusId;
    }

    public void setLineManagerBusId(String LineManagerBusId) {
        this.LineManagerBusId = LineManagerBusId;
    }

    public String getDottedLineManagerBusId() {
        return DottedLineManagerBusId;
    }

    public void setDottedLineManagerBusId(String DottedLineManagerBusId) {
        this.DottedLineManagerBusId = DottedLineManagerBusId;
    }

    public String getBuildingBusId() {
        return BuildingBusId;
    }

    public void setBuildingBusId(String BuildingBusId) {
        this.BuildingBusId = BuildingBusId;
    }
    
    public String getPrivateStreet() {
		return PrivateStreet;
	}

	public void setPrivateStreet(String privateStreet) {
		PrivateStreet = privateStreet;
	}

	public String getPrivatePostCode() {
		return PrivatePostCode;
	}

	public void setPrivatePostCode(String privatePostCode) {
		PrivatePostCode = privatePostCode;
	}

	public String getPrivateTown() {
		return PrivateTown;
	}

	public void setPrivateTown(String privateTown) {
		PrivateTown = privateTown;
	}

	public String getPrivateStateProvince() {
		return PrivateStateProvince;
	}

	public void setPrivateStateProvince(String privateStateProvince) {
		PrivateStateProvince = privateStateProvince;
	}

	public String getPrivateCountryCode() {
		return PrivateCountryCode;
	}

	public void setPrivateCountryCode(String privateCountryCode) {
		PrivateCountryCode = privateCountryCode;
	}

	public String getEmployerBusId() {
        return EmployerBusId;
    }

    public void setEmployerBusId(String EmployerBusId) {
        this.EmployerBusId = EmployerBusId;
    }

    public String getSTG_Control() {
        return STG_Control;
    }

    public void setSTG_Control(String STG_Control) {
        this.STG_Control = STG_Control;
    }

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdateTimeStamp() {
		return lastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(Date lastUpdateTimeStamp) {
		this.lastUpdateTimeStamp = lastUpdateTimeStamp;
	}

	public String getEmployeeMaster() {
		return EmployeeMaster;
	}

	public void setEmployeeMaster(String employeeMaster) {
		EmployeeMaster = employeeMaster;
	}

	public String getFunctionalTitle() {
		return FunctionalTitle;
	}

	public void setFunctionalTitle(String functionalTitle) {
		FunctionalTitle = functionalTitle;
	}

	public String getPositionName() {
		return PositionName;
	}

	public void setPositionName(String positionName) {
		PositionName = positionName;
	}

	public String getJobProfileName() {
		return JobProfileName;
	}

	public void setJobProfileName(String jobProfileName) {
		JobProfileName = jobProfileName;
	}

	public Date getLastSyncTimestamp() {
		return lastSyncTimestamp;
	}

	public void setLastSyncTimestamp(Date lastSyncTimestamp) {
		this.lastSyncTimestamp = lastSyncTimestamp;
	}
	
}
