package com.elipslife.mdm.contract.core;

import java.util.HashMap;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class CoreExposureWriter extends AbstractTableWriter<CoreExposureBean> {

	private CoreExposureReader coreExposureReader;
	
	public CoreExposureWriter() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public CoreExposureWriter(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._Exposure.getPathInSchema()));
	}
	
	public CoreExposureWriter(AdaptationTable table) {
		super(table);
		
		this.coreExposureReader = new CoreExposureReader(table);
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, CoreExposureBean bean) {
		
		values.put(ContractPaths._Exposure._InsuranceContract_InsuranceContractId, bean.getInsuranceContractId());
		
		values.put(ContractPaths._Exposure._ExposureGeneral_ExposureId, bean.getId());
		values.put(ContractPaths._Exposure._ExposureGeneral_ExposureBusId, bean.getExposureBusId());
		values.put(ContractPaths._Exposure._ExposureGeneral_Name, bean.getName());
		values.put(ContractPaths._Exposure._ExposureGeneral_ProductBusId, bean.getProductId());
		values.put(ContractPaths._Exposure._ExposureGeneral_ProductBusId, bean.getProductBusId());
		values.put(ContractPaths._Exposure._ExposureGeneral_ProductGroup, bean.getProductGroup());
		values.put(ContractPaths._Exposure._ExposureGeneral_ProductName, bean.getProductName());
		
		values.put(ContractPaths._Exposure._Control_SourceSystem, bean.getSourceSystem());
		values.put(ContractPaths._Exposure._Control_CreatedBy, bean.getCreatedBy());
		values.put(ContractPaths._Exposure._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(ContractPaths._Exposure._Control_LastUpdatedBy, bean.getLastUpdatedBy());
		values.put(ContractPaths._Exposure._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(ContractPaths._Exposure._Control_LastSyncAction, bean.getLastSyncAction());
		values.put(ContractPaths._Exposure._Control_LastSyncTimestamp, bean.getLastSyncTimestamp());
	}

	@Override
	public AbstractTableReader<CoreExposureBean> getTableReader() {
		return this.coreExposureReader;
	}
}