package com.elipslife.mdm.scripttasks;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.elipslife.mdm.claimcase.constants.ClaimCaseConstants;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;
import com.orchestranetworks.workflow.WorkflowEngine;

public class CheckAndSortCSVFilesForImport extends ScriptTaskBean {
	private String errorDirectory;
	private String errorLogDirectory;
	private String fileList;
	private String initialDataSpace;
	private String inputDirectory;
	private String tableName;
	private String dataSetName;
	private String tmpDataSpace;
	private String processedDirectory;
	private String workflowToLaunch;

	private WorkflowEngine workflowEngine;
	private ProcessLauncher processLauncher;

	private String filePattern;
	private String fileNamePrefix = "LDG_";

	@Override
	public void executeScript(ScriptTaskBeanContext aContext) throws OperationException {

		List<String> filesPathes = splitFileList(fileList);
		List<File> files = filesPathes.stream().map(filesPath -> new File(filesPath)).collect(Collectors.toList());

		for (File file : files) {
			
			if (!file.isFile()) {
				continue;
			}

			if (!checkSuffix(file)) {
				
				createLogFile("Error: Wrong suffix. Must be .csv.",	file.getName().substring(0, file.getName().length() - 4));
				
				File fileError = new File(errorDirectory, file.getName());
				file.renameTo(fileError);
				
				continue;
			}

			if (!checkFileNameAndDate(file)) {

				createLogFile("Error: Wrong filename or date in filename invalid.",	file.getName().substring(0, file.getName().length() - 4));
				
				File fileError = new File(errorDirectory, file.getName());
				file.renameTo(fileError);
				
				continue;
			}

			workflowEngine = WorkflowEngine.getFromRepository(aContext.getRepository(), aContext.getSession());
			processLauncher = workflowEngine.getProcessLauncher(PublishedProcessKey.forName(workflowToLaunch));
			processLauncher.setInputParameter("fileName", file.getName());
			processLauncher.setInputParameter("errorDirectory", errorDirectory);
			processLauncher.setInputParameter("errorLogDirectory", errorLogDirectory);
			processLauncher.setInputParameter("initialDataSpace", initialDataSpace);
			processLauncher.setInputParameter("processedDirectory", processedDirectory);
			processLauncher.setInputParameter("inputDirectory", inputDirectory);
			String tmpDataSpaceName = file.getName().substring(4, file.getName().length() - 4); // 4 represents
																								// "LDG_"
			int removeFilePatternLength = filePattern.length();
			tmpDataSpaceName = tmpDataSpaceName.substring(removeFilePatternLength + 1); // + 1 to remove "_"
			processLauncher.setInputParameter("tmpDataSpace", tmpDataSpaceName);
			processLauncher.setInputParameter("tableName", tableName);
			processLauncher.setInputParameter("dataSetName", dataSetName);
			processLauncher.launchProcess();
		}
	}

	public List<String> splitFileList(String fileList) {
		return Arrays.asList(fileList.split("\\s*,\\s*"));
	}

	public void createLogFile(String error, String fileName) {
		if (fileName.substring(fileName.length() - 4, fileName.length()).equals(".csv")) {
			fileName = fileName.substring(0, fileName.length() - 4);
		}
		File logFile = new File(errorLogDirectory + "/" + fileName + ".log");

		try {
			logFile.createNewFile();

			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFile)));
			writer.write(error);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean checkFileNameAndDate(File file) {
		String fileNameBase = fileNamePrefix + filePattern;
		String expression = fileNameBase + "_[1-2][0-9][0-9][0-9]_(0[1-9]|1[0-2])_(0[1-9]|[1-2][0-9]|3[0-1])_(0[0-9]|1[0-9]|2[0-4])_(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])_(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]).csv";
		CharSequence inputStr = file.getName();
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		return matcher.matches();
	}

	public boolean checkSuffix(File file) {
		if (file.getName().substring(file.getName().length() - 4, file.getName().length()).equals(ClaimCaseConstants.File.fileSuffix)) {
			return true;
		} else {
			return false;
		}
	}

	public String getErrorDirectory() {
		return errorDirectory;
	}

	public void setErrorDirectory(String errorDirectory) {
		this.errorDirectory = errorDirectory;
	}

	public String getErrorLogDirectory() {
		return errorLogDirectory;
	}

	public void setErrorLogDirectory(String errorLogDirectory) {
		this.errorLogDirectory = errorLogDirectory;
	}

	public String getFileList() {
		return fileList;
	}

	public void setFileList(String fileList) {
		this.fileList = fileList;
	}

	public String getInitialDataSpace() {
		return initialDataSpace;
	}

	public void setInitialDataSpace(String initialDataSpace) {
		this.initialDataSpace = initialDataSpace;
	}

	public String getInputDirectory() {
		return inputDirectory;
	}

	public void setInputDirectory(String inputDirectory) {
		this.inputDirectory = inputDirectory;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getDataSetName() {
		return dataSetName;
	}

	public void setDataSetName(String dataSetName) {
		this.dataSetName = dataSetName;
	}

	public String getTmpDataSpace() {
		return tmpDataSpace;
	}

	public void setTmpDataSpace(String tmpDataSpace) {
		this.tmpDataSpace = tmpDataSpace;
	}

	public String getProcessedDirectory() {
		return processedDirectory;
	}

	public void setProcessedDirectory(String processedDirectory) {
		this.processedDirectory = processedDirectory;
	}

	public String getFilePattern() {
		return filePattern;
	}

	public void setFilePattern(String filePattern) {
		this.filePattern = filePattern;
	}

	public String getWorkflowToLaunch() {
		return workflowToLaunch;
	}

	public void setWorkflowToLaunch(String workflowToLaunch) {
		this.workflowToLaunch = workflowToLaunch;
	}
}