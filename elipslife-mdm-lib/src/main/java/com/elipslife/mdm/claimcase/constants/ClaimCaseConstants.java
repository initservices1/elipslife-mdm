package com.elipslife.mdm.claimcase.constants;

public class ClaimCaseConstants {
	public static class DataSpace {
		public static final String PARTY_REF = "PartyReference";
		public static final String CLAIMCASE_REF = "ClaimCaseReference";
	}

	public static class SourceSystems {
		public static final String CRM = "CRM";
	}

	public static final class DataSet {
		public static final String CLAIMCASE_MASTER = "ClaimCaseMaster";
		public static final String PARTY_MASTER = "PartyMaster";
	}

	public static class RecordOperations {
		public static final String CREATE = "C";
		public static final String UPDATE = "U";
		public static final String DELETE = "D";
	}

	public static class File {
		public static final String fileSuffix = ".csv";

	}
}