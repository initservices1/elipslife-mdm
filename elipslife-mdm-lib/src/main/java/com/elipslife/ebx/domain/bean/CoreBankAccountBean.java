package com.elipslife.ebx.domain.bean;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class CoreBankAccountBean extends Entity<Integer> {
	
	private int bankAccountId;
	private String partyId;
	private String accountType;
	private String accountNumber;
	private String iban;
	private String bic;
	private String clearingNumber;
	private String bankName;
	private String country;
	private String currency;
	private Boolean isMainAccount;
	private String sourceBankAccountBusId;
	private Date validFrom;
	private Date validTo;
	private Boolean isExpired;
	private String sourceSystem;
	private String sourcePartyBusId;
	private String createdBy;
	private Date creationTimestamp;
	private String lastUpdatedBy;
	private Date lastUpdateTimestamp;
	private String lastSyncAction;
	private Date lastSyncTimestamp;
	
	public int getBankAccountId() {
		return bankAccountId;
	}
	public void setBankAccountId(int bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getBic() {
		return bic;
	}
	public void setBic(String bic) {
		this.bic = bic;
	}
	public String getClearingNumber() {
		return clearingNumber;
	}
	public void setClearingNumber(String clearingNumber) {
		this.clearingNumber = clearingNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Boolean getIsMainAccount() {
		return isMainAccount;
	}
	public void setIsMainAccount(Boolean isMainAccount) {
		this.isMainAccount = isMainAccount;
	}
	public String getSourceBankAccountBusId() {
		return sourceBankAccountBusId;
	}
	public void setSourceBankAccountBusId(String sourceBankAccountBusId) {
		this.sourceBankAccountBusId = sourceBankAccountBusId;
	}
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public Boolean getIsExpired() {
		return isExpired;
	}
	public void setIsExpired(Boolean isExpired) {
		this.isExpired = isExpired;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getSourcePartyBusId() {
		return sourcePartyBusId;
	}
	public void setSourcePartyBusId(String sourcePartyBusId) {
		this.sourcePartyBusId = sourcePartyBusId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getLastSyncAction() {
		return lastSyncAction;
	}
	public void setLastSyncAction(String lastSyncAction) {
		this.lastSyncAction = lastSyncAction;
	}
	public Date getLastSyncTimestamp() {
		return lastSyncTimestamp;
	}
	public void setLastSyncTimestamp(Date lastSyncTimestamp) {
		this.lastSyncTimestamp = lastSyncTimestamp;
	}
}