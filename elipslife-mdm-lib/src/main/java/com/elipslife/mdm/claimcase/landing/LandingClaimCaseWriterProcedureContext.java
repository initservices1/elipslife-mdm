package com.elipslife.mdm.claimcase.landing;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class LandingClaimCaseWriterProcedureContext extends LandingClaimCaseWriter {
	private final ProcedureContext procedureContext;

	public LandingClaimCaseWriterProcedureContext(ProcedureContext procedureContext) {
		super();
		this.procedureContext = procedureContext;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
