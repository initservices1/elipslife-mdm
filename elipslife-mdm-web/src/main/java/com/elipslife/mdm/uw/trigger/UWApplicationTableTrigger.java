package com.elipslife.mdm.uw.trigger;

import java.util.Date;

import com.elipslife.mdm.common.rules.ElipsLifeCommonRules;
import com.elipslife.mdm.path.UWPaths;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TableTriggerExecutionContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class UWApplicationTableTrigger extends TableTrigger {

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		super.handleBeforeCreate(aContext);
		
		// set LastSyncAction and LastSyncTimestamp
		aContext.getOccurrenceContextForUpdate().setValue(new Date(), UWPaths._UWCase._Control_LastSyncTimestamp);

		String sourceSystem = (String) aContext.getOccurrenceContext().getValue(UWPaths._UWApplication._Context_SourceSystem);
		if (sourceSystem != null) {
			// set policyHolderId
			setPolicyHolderId(aContext, aContext.getOccurrenceContextForUpdate(), sourceSystem);
			// Set Main Affilate Id
			setMainAffilicateId(aContext, aContext.getOccurrenceContextForUpdate(), sourceSystem);
		}

	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
		super.handleBeforeModify(aContext);

		aContext.getOccurrenceContextForUpdate().setValue(new Date(), UWPaths._UWCase._Control_LastSyncTimestamp);
		String sourceSystem = (String) aContext.getOccurrenceContext().getValue(UWPaths._UWApplication._Context_SourceSystem);
		if (sourceSystem != null) {
			// set policyHolderId
			setPolicyHolderId(aContext, aContext.getOccurrenceContextForUpdate(), sourceSystem);
			// Set Main Affilate Id
			setMainAffilicateId(aContext, aContext.getOccurrenceContextForUpdate(), sourceSystem);
		}

	}

	private void setPolicyHolderId(TableTriggerExecutionContext aContext, ValueContextForUpdate valueContext,
			String sourceSystem) {
		// update poluHolderId
		String policyHolderBusId = (String) aContext.getOccurrenceContext().getValue(UWPaths._UWApplication._Policyholder_PolicyholderBusId);
		if (policyHolderBusId != null) {
			String policyHolderId = ElipsLifeCommonRules.getPartyIdByPartyBusId(policyHolderBusId);
			if (policyHolderId != null) {
				valueContext.setValue(policyHolderId, UWPaths._UWApplication._Policyholder_PolicyholderId);
			} else {
				valueContext.setValue("", UWPaths._UWApplication._Policyholder_PolicyholderId);
			}
		}
	}

	private void setMainAffilicateId(TableTriggerExecutionContext aContext, ValueContextForUpdate valueContext,
			String sourceSystem) {
		
		// Update Main Affilate Id
		String mainAffiliateBusId = (String) aContext.getOccurrenceContext().getValue(UWPaths._UWApplication._MainAffiliate_MainAffiliateBusId);
		if (mainAffiliateBusId != null) {
			String mainAffiliateId = ElipsLifeCommonRules.getPartyIdByPartyBusId(mainAffiliateBusId);
			if (mainAffiliateId != null) {
				valueContext.setValue(mainAffiliateId,UWPaths._UWApplication._MainAffiliate_MainAffiliateId);
			} else {
				valueContext.setValue("", UWPaths._UWApplication._MainAffiliate_MainAffiliateId);
			}
		}
	}

	@Override
	public void setup(TriggerSetupContext aContext) {

	}

}
