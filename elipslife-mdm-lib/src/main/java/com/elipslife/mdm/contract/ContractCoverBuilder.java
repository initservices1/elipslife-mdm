package com.elipslife.mdm.contract;

import java.util.List;
import java.util.Optional;

import com.elipslife.ebx.domain.bean.convert.ContractConverter;
import com.elipslife.mdm.contract.core.CoreContractCoverBean;
import com.elipslife.mdm.contract.core.CoreContractCoverReader;
import com.elipslife.mdm.contract.core.CoreContractCoverWriter;
import com.elipslife.mdm.contract.core.CoreContractCoverWriterSession;
import com.elipslife.mdm.contract.core.CoreExposureBean;
import com.elipslife.mdm.contract.core.CoreExposureReader;
import com.elipslife.mdm.contract.staging.StagingContractCoverBean;
import com.elipslife.mdm.contract.staging.StagingContractCoverWriter;
import com.elipslife.mdm.contract.staging.StagingContractCoverWriterSession;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.orchestranetworks.service.Session;

public class ContractCoverBuilder {
	
	/**
	 * 
	 * @param session
	 * @param stagingContractCoverBean
	 */
	public static void createOrUpdateCoreContractCoverByStagingContractCover(Session session, StagingContractCoverBean stagingContractCoverBean) {
		
		StagingContractCoverWriter stagingContractCoverWriter = new StagingContractCoverWriterSession(session);
		CoreContractCoverWriter coreContractCoverWriter = new CoreContractCoverWriterSession(session);
		
		createOrUpdateCoreContractCoverByStagingContractCover(stagingContractCoverWriter, coreContractCoverWriter, stagingContractCoverBean);
	}
	
	
	/**
	 * 
	 * @param stagingContractCoverWriter
	 * @param coreContractCoverWriter
	 * @param stagingContractCoverBean
	 */
	public static void createOrUpdateCoreContractCoverByStagingContractCover(
			StagingContractCoverWriter stagingContractCoverWriter,
			CoreContractCoverWriter coreContractCoverWriter,
			StagingContractCoverBean stagingContractCoverBean) {
		
		CoreContractCoverReader coreContractCoverReader = (CoreContractCoverReader) coreContractCoverWriter.getTableReader();
		
		List<CoreContractCoverBean> coreContractCoverBeans = coreContractCoverReader.findBeansBySourceSystemAndCoverBusId(
				stagingContractCoverBean.getSourceSystem(),
				stagingContractCoverBean.getCoverBusId());
		
		if(coreContractCoverBeans.size() > 1) {
			throw new RuntimeException(
					String.format(
							"More than one contract cover found with source system %s and cover business ID %s",
							stagingContractCoverBean.getSourceSystem(),
							stagingContractCoverBean.getCoverBusId()));
		}
		
		CoreContractCoverBean coreContractCoverBean = coreContractCoverBeans.isEmpty() ? null : coreContractCoverBeans.get(0);
		
		if(coreContractCoverBean != null) {
			
			// Update
			boolean updateCoreContractCoverBean = DateNullableComparator.getInstance().compare(stagingContractCoverBean.getLastUpdateTimestamp(), coreContractCoverBean.getLastUpdateTimestamp()) > 0;
			if(!updateCoreContractCoverBean) {
				stagingContractCoverBean.setContractCoverMaster(String.valueOf(coreContractCoverBean.getId()));
				stagingContractCoverWriter.updateBean(stagingContractCoverBean);
				return;
			}
			
			ContractConverter.fillCoreContractCoverBeanWithStagingContractCoverBean(stagingContractCoverBean, coreContractCoverBean);
			linkExposureReference(coreContractCoverBean);
			coreContractCoverWriter.updateBean(coreContractCoverBean);
			stagingContractCoverBean.setContractCoverMaster(String.valueOf(coreContractCoverBean.getId()));
		}
		else {
			
			// Create
			
			coreContractCoverBean = ContractConverter.createCoreContractCoverBeanFromStagingContractCoverBean(stagingContractCoverBean);
			
			linkExposureReference(coreContractCoverBean);
			Optional<CoreContractCoverBean> coreContractCoverBeanOptional  = coreContractCoverWriter.createBeanOrThrow(coreContractCoverBean);
			stagingContractCoverBean.setContractCoverMaster(String.valueOf(coreContractCoverBeanOptional.get().getId()));
		}
		stagingContractCoverWriter.updateBean(stagingContractCoverBean);
	}
	
	public static void linkExposureReference(CoreContractCoverBean coreContractCoverBean) {
		
		CoreExposureReader coreExposureReader = new CoreExposureReader();
		CoreExposureBean coreExposureBean = findExposureBySourceSystemAndExposureBusId(coreExposureReader,
				coreContractCoverBean.getSourceSystem(),coreContractCoverBean.getExposureBusId());
		
		coreContractCoverBean.setExposureId(coreExposureBean != null ? coreExposureBean.getId().toString() : null);
	}

	/**
	 * Link Exposure References
	 * 
	 * @param coreContractCoverBean
	 */
	public static void linkExposureReference(String dataspace, CoreContractCoverBean coreContractCoverBean) {
		
		CoreExposureReader coreExposureReader = new CoreExposureReader(dataspace);
		CoreExposureBean coreExposureBean = findExposureBySourceSystemAndExposureBusId(coreExposureReader,
				coreContractCoverBean.getSourceSystem(),coreContractCoverBean.getExposureBusId());
		
		coreContractCoverBean.setExposureId(coreExposureBean != null ? coreExposureBean.getId().toString() : null);
	}
	
	
	/**
	 * Find exposure by sourceSystem and ExposureBusId
	 * 
	 * @param coreExposureReader
	 * @param sourceSystem
	 * @param exposureBusId
	 * @return
	 */
	public static CoreExposureBean findExposureBySourceSystemAndExposureBusId(CoreExposureReader coreExposureReader, String sourceSystem, String exposureBusId) {

		List<CoreExposureBean> coreExposureBeans = coreExposureReader.findBeansBySourceSystemAndExposureBusId(sourceSystem, exposureBusId);
		if(coreExposureBeans.size() > 1) {
			throw new RuntimeException(String.format("More than one exposure found with source system %s and exposure business ID %s",
							sourceSystem, exposureBusId));
		}
		
		return coreExposureBeans.isEmpty() ? null : coreExposureBeans.get(0);
	}
}