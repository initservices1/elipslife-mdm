package com.elipslife.ebx.data.access;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.StringUtils;

import com.elipslife.ebx.domain.bean.CoreAddressBean;
import com.elipslife.ebx.domain.bean.CoreBankAccountBean;
import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.ebx.domain.bean.convert.CorePartyConverter;
import com.elipslife.ebx.domain.bean.convert.CorePartyIdentifierConverter;
import com.elipslife.ebx.domain.bean.convert.PartyConverter;
import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.boot.VM;
import com.orchestranetworks.presales.toolbox.procedure.RecordValuesBean;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class CorePartyBuilder {

	public static List<CorePartyBean> coreCRMPartyBeansToCreate = new ArrayList<CorePartyBean>();
	public static List<CorePartyBean> duplicateCheckCRMPartyBeansToCreate = new ArrayList<CorePartyBean>();
	public static List<CorePartyBean> duplicateCheckToReprocessList = new ArrayList<CorePartyBean>();
	public static List<CorePartyBean> coreNonCRMPartyBeansToCreate = new ArrayList<CorePartyBean>();
	public static List<CorePartyBean> corePartyBeansToUpdate = new ArrayList<CorePartyBean>();
	public static List<CorePartyBean> corePartyRecordsAfterCreate = new ArrayList<CorePartyBean>();
	public static List<StagingPartyBean> stagingPartyBeansChunk = new ArrayList<StagingPartyBean>();
	public static List<StagingPartyBean> stagingPartyBeanToreprocess = new ArrayList<StagingPartyBean>();
	public static List<StagingPartyBean> stagingPartyUpdate = new ArrayList<StagingPartyBean>();
	public static List<StagingPartyBean> stagingListToProcessAfterCoreCreations = new ArrayList<StagingPartyBean>();
	public static List<CorePartyIdentifierBean> corePartyIdentifierBeanToCreate = new ArrayList<CorePartyIdentifierBean>();
	public static Map<String, Map<String, List<StagingPartyBean>>> partyTypesMap = new HashMap<String, Map<String,List<StagingPartyBean>>>();
	public static int recordCount = 0;
	public static Integer commitSize = 0;
	public static boolean isFinalBatch = false;
	public static boolean isFullLoad = false;
	public static int batchCount = 1;
	public static int processedRecordsInThisBatch;
	public static int totalRecordsCount = 0;
	public static AdaptationHome stagingDataSpaceHome;
	public static Adaptation stagingDataSet;


	public static Integer getCommitSize() {
		return commitSize;
	}

	public static void setCommitSize(Integer commitSize) {
		CorePartyBuilder.commitSize = commitSize;
	}

	/**
	 * Gets party by source system
	 * 
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @return
	 */
	public static CorePartyBean getPartyBySourceSystem(String sourceSystem, String sourcePartyBusId, String dataSpace) {

		CorePartyIdentifierBean corePartyIdentifierBean = getPartyIdentifierBySourceSystem(sourceSystem, sourcePartyBusId, dataSpace);
		CorePartyBean corePartyBean = getPartyByPartyIdentifier(corePartyIdentifierBean, dataSpace);
		return corePartyBean;
	}

	/**
	 * 
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @return
	 */
	public static CorePartyIdentifierBean getPartyIdentifierBySourceSystem(String sourceSystem, String sourcePartyBusId, String dataSpace) {

		CorePartyIdentifierReader partyIdentifierReader = new CorePartyIdentifierReader(dataSpace);
		List<CorePartyIdentifierBean> corePartyIdentifierBeans = partyIdentifierReader.getBeansBySourceSystem(sourceSystem, sourcePartyBusId);
		if (corePartyIdentifierBeans.isEmpty()) {
			return null;
		}
		// Get Party by PartyIdentifier
		CorePartyIdentifierBean corePartyIdentifierBean = corePartyIdentifierBeans.get(0);

		return corePartyIdentifierBean;
	}

	/**
	 * 
	 * @param corePartyIdentifierBean
	 * @return
	 */
	public static CorePartyBean getPartyByPartyIdentifier(CorePartyIdentifierBean corePartyIdentifierBean, String dataSpace) {

		if (corePartyIdentifierBean == null || corePartyIdentifierBean.getPartyId() == null || corePartyIdentifierBean.getPartyId().isEmpty()) {
			return null;
		}
		CorePartyReader corePartyReader = new CorePartyReader(dataSpace);
		CorePartyBean corePartyBean = corePartyReader.findBeanByPartyId(Integer.valueOf(corePartyIdentifierBean.getPartyId()));

		return corePartyBean;
	}

	/**
	 * 
	 * @param crmId
	 * @return
	 * @throws Exception
	 */
	public static CorePartyBean getPartyByCrmId(String crmId) throws Exception {

		if (crmId == null || crmId.isEmpty()) {
			return null;
		}
		CorePartyReader corePartyReader = new CorePartyReader();
		List<CorePartyBean> corePartyBeans = corePartyReader.findBeansByCrmId(crmId);
		if (corePartyBeans.size() > 1) {
			throw new Exception(String.format("More than one core party with CRM ID %s found", crmId));
		}

		return corePartyBeans.size() == 1 ? corePartyBeans.get(0) : null;
	}

	/**
	 * 
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @param partyId
	 * @return
	 */
	public static CorePartyIdentifierBean createPartyIdentifier(String sourceSystem, String sourcePartyBusId, int partyId) {

		CorePartyIdentifierBean corePartyIdentifierBean = new CorePartyIdentifierBean();
		corePartyIdentifierBean.setSourceSystem(sourceSystem);
		corePartyIdentifierBean.setSourcePartyBusId(sourcePartyBusId);
		corePartyIdentifierBean.setPartyId(String.valueOf(partyId));

		return corePartyIdentifierBean;
	}

	/**
	 * 
	 * @param session
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @param validFrom
	 * @param validTo
	 * @throws Exception
	 */
	public static void createOrUpdateCorePartyByStagingParty(Session session, String sourceSystem, String sourcePartyBusId, Date validFrom, Date validTo) throws Exception {

		StagingPartyReader stagingPartyReader = new StagingPartyReader();
		List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId, validFrom, validTo);
		if (stagingPartyBeans.isEmpty()) {
			return;
		}

		StagingPartyBean stagingPartyBean = stagingPartyBeans.get(0);
		createOrUpdateCorePartyByStagingParty(session, stagingPartyBean);
	}

	public static void createOrUpdateCorePartyByStagingPartyInBulkProcessing(Session session, String dataSpace, String commitSize, AdaptationHome stagingDataspace, Adaptation stageDataset) throws Exception {

		stagingDataSpaceHome = stagingDataspace;
		stagingDataSet = stageDataset;
		isFullLoad = isBulkProcess();
		int startPositionAt = 0;
		int endPositionAt = 0;
				
		Date childDataspaceCreationDate = stagingDataSpaceHome.getCreationDate();
		StagingPartyReader stagingPartyReader = new StagingPartyReader(dataSpace);
		List<StagingPartyBean> stagingPartyBeanList = stagingPartyReader.getStagingPartyBeansByLastSyncTimeStamp(childDataspaceCreationDate);
		
		List<StagingPartyBean> stagingPartyBulkProcessBeansOrdered = stagingPartyBeanList.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getLastUpdateTimestamp(),
						b.getLastUpdateTimestamp())).collect(Collectors.toList());
		setCommitSize(Integer.valueOf(commitSize));

		StagingPartyWriter stagingPartyWriter = new StagingPartyWriterSession(session, true, dataSpace);
		CorePartyWriter corePartyWriter = new CorePartyWriterSession(session, true, dataSpace);
		CorePartyIdentifierWriter corePartyIdentifierWriter = new CorePartyIdentifierWriterSession(session, dataSpace);
		List<StagingPartyBean> stagingPartyBulkProcessBeans = stagingPartyBulkProcessBeansOrdered;
		int totalStagingRecords = stagingPartyBulkProcessBeans.size();
		float noOfIterations = totalStagingRecords/CorePartyBuilder.commitSize;
		for(int i = 0 ; i < noOfIterations+1; i++) {
			if(CorePartyBuilder.commitSize < totalStagingRecords) {
				endPositionAt = endPositionAt+CorePartyBuilder.commitSize;
				totalStagingRecords = totalStagingRecords - CorePartyBuilder.commitSize;
			} else {
				endPositionAt = endPositionAt+totalStagingRecords;
				totalStagingRecords = 0;
			}
			List<StagingPartyBean> batchStagingParties = stagingPartyBulkProcessBeans.subList(startPositionAt, endPositionAt);
			getBatachResults(batchStagingParties);
			startPositionAt = endPositionAt;
			for (StagingPartyBean spb : batchStagingParties) {
				boolean isWithinRange = dateWithinRange(spb.getValidFrom(), spb.getValidTo());
				if (!isWithinRange) {
					continue;
				}

				boolean hasValidationErrors = hasValidationErrors(spb, stageDataset.getTable(PartyPaths._STG_Party.getPathInSchema()));
				if (hasValidationErrors) {
					continue;
				}

				try {
					stagingPartyBeansChunk.add(spb);
					createOrUpdateCorePartyByStagingParty(stagingPartyWriter, corePartyWriter, corePartyIdentifierWriter, spb, dataSpace, session);
				} catch (Exception e) {
				    VM.log.kernelError("Exception occurred while executiong the parties from staging to core "+spb.getId() +" "+ e);
					throw OperationException.createError(e);
				}
			}
		}
		if (!coreCRMPartyBeansToCreate.isEmpty() || !corePartyIdentifierBeanToCreate.isEmpty()
				|| !corePartyBeansToUpdate.isEmpty() || !duplicateCheckCRMPartyBeansToCreate.isEmpty()
				|| !coreNonCRMPartyBeansToCreate.isEmpty() || !stagingListToProcessAfterCoreCreations.isEmpty()) {

			createCoreParty(coreCRMPartyBeansToCreate, session, dataSpace);
			createCoreParty(coreNonCRMPartyBeansToCreate, session, dataSpace);
			createCorePartyIdentifier(corePartyIdentifierBeanToCreate, session, dataSpace);
			updateCoreParty(corePartyBeansToUpdate, session, dataSpace);			coreCRMPartyBeansToCreate.clear();
			coreNonCRMPartyBeansToCreate.clear();
			corePartyIdentifierBeanToCreate.clear();
			duplicateCheckCRMPartyBeansToCreate.clear();
			corePartyBeansToUpdate.clear();

			if (!isFinalBatch) {
				isFinalBatch = true;
				for (StagingPartyBean stagingPartyBean : stagingPartyBeanToreprocess) {
					try {
						createOrUpdateCorePartyByStagingParty(stagingPartyWriter, corePartyWriter, corePartyIdentifierWriter, stagingPartyBean, dataSpace, session);
					} catch (Exception e) {
					    VM.log.kernelError("Exception occurred while executing the staging party : "+stagingPartyBean.getId()+ " "+ e.getMessage());
						throw OperationException.createError(e);
					}
				}
				isFinalBatch = false;
				recordCount = 0;
				executeCreateAndUpdateBatchProcessForParties(session, dataSpace);
				duplicateCheckToReprocessList.clear();
				stagingPartyBeansChunk.clear();
				// Clear already processed records
				coreCRMPartyBeansToCreate.clear();
				corePartyIdentifierBeanToCreate.clear();
				corePartyBeansToUpdate.clear();
				duplicateCheckCRMPartyBeansToCreate.clear();
				stagingPartyBulkProcessBeansOrdered.clear();
			}
			stagingPartyBeanToreprocess.clear();
		}
		totalRecordsCount =0;
		batchCount = 0;
	}

	public static void executeCreateAndUpdateBatchProcessForParties(Session session, String dataSpace)
			throws OperationException {

		try {

			if (!coreCRMPartyBeansToCreate.isEmpty() || !corePartyIdentifierBeanToCreate.isEmpty()
					|| !corePartyBeansToUpdate.isEmpty() || !duplicateCheckCRMPartyBeansToCreate.isEmpty()
					|| !coreNonCRMPartyBeansToCreate.isEmpty()) {

				createCoreParty(coreCRMPartyBeansToCreate, session, dataSpace);
				createCoreParty(coreNonCRMPartyBeansToCreate, session, dataSpace);
				createCorePartyIdentifier(corePartyIdentifierBeanToCreate, session, dataSpace);
				updateCoreParty(corePartyBeansToUpdate, session, dataSpace);

				// Clear already processed records
				coreCRMPartyBeansToCreate.clear();
				corePartyIdentifierBeanToCreate.clear();
				corePartyBeansToUpdate.clear();
			}

		} catch (Exception e) {
		    VM.log.kernelError("Exception occurred while executing the staging parties"+ e);
			throw OperationException.createError(e);
		}

	}

	/**
	 * 
	 * @param procedureContext
	 * @param stagingPartyBean
	 * @throws Exception
	 */
	public static void createOrUpdateCorePartyByStagingParty(ProcedureContext procedureContext,
			StagingPartyBean stagingPartyBean) throws Exception {

		StagingPartyWriter stagingPartyWriter = new StagingPartyWriterProcedureContext(procedureContext, true);
		CorePartyWriter corePartyWriter = new CorePartyWriterProcedureContext(procedureContext, true);
		CorePartyIdentifierWriter corePartyIdentifierWriter = new CorePartyIdentifierWriterProcedureContext(procedureContext);

		createOrUpdateCorePartyByStagingParty(stagingPartyWriter, corePartyWriter, corePartyIdentifierWriter, stagingPartyBean, PartyConstants.DataSpace.PARTY_REFERENCE, procedureContext.getSession());
	}

	/**
	 * 
	 * @param session
	 * @param stagingPartyBean
	 * @throws Exception
	 */
	public static void createOrUpdateCorePartyByStagingParty(Session session, StagingPartyBean stagingPartyBean) throws Exception {

		StagingPartyWriter stagingPartyWriter = new StagingPartyWriterSession(session, true);
		CorePartyWriter corePartyWriter = new CorePartyWriterSession(session, true);
		CorePartyIdentifierWriter corePartyIdentifierWriter = new CorePartyIdentifierWriterSession(session);

		createOrUpdateCorePartyByStagingParty(stagingPartyWriter, corePartyWriter, corePartyIdentifierWriter, stagingPartyBean, PartyConstants.DataSpace.PARTY_REFERENCE, session);
	}

	/**
	 * 
	 * @param stagingPartyWriter
	 * @param corePartyWriter
	 * @param corePartyIdentifierWriter
	 * @param stagingPartyBean
	 * @throws Exception
	 */
	public static void createOrUpdateCorePartyByStagingParty(StagingPartyWriter stagingPartyWriter, CorePartyWriter corePartyWriter, CorePartyIdentifierWriter corePartyIdentifierWriter,
			StagingPartyBean stagingPartyBean, String dataSpace, Session session) throws Exception {

		// boolean isBulkProcess = isBulkProcess();
		if (stagingPartyBean.getSourceSystem() == null || stagingPartyBean.getSourceSystem().isEmpty()) {
			throw new RuntimeException(String.format("Source system for staging party with ID %s not found", stagingPartyBean.getId()));
		}

		if (stagingPartyBean.getSourcePartyBusId() == null || stagingPartyBean.getSourcePartyBusId().isEmpty()) {
			throw new RuntimeException(String.format("Source party bus ID for staging party with ID %s not found", stagingPartyBean.getId()));
		}

		if (stagingPartyBean.getAction() == null || stagingPartyBean.getAction().isEmpty()) {
			throw new RuntimeException(String.format("Action for staging party with ID %s not found", stagingPartyBean.getId()));
		}
		
		CorePartyIdentifierBean	corePartyIdentifierBean = CorePartyBuilder.getPartyIdentifierBySourceSystem(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId(), dataSpace);
		CorePartyBean corePartyBean = CorePartyBuilder.getPartyByPartyIdentifier(corePartyIdentifierBean, dataSpace);
		Date dateMinimum = new Date(Long.MIN_VALUE);
		Date stagingPartyLastUpdateTimestamp;
		Date corePartyLastUpdateTimestamp;

		if (corePartyBean != null) {
			stagingPartyLastUpdateTimestamp = stagingPartyBean.getLastUpdateTimestamp() == null ? dateMinimum : stagingPartyBean.getLastUpdateTimestamp();
			corePartyLastUpdateTimestamp = corePartyBean.getLastUpdateTimestamp() == null ? dateMinimum : corePartyBean.getLastUpdateTimestamp();
			if (stagingPartyLastUpdateTimestamp.compareTo(corePartyLastUpdateTimestamp) >= 0) {
				if (isCrmRecord(stagingPartyBean.getSourceSystem()) || isStagingAndCoreNonCrmRecords(stagingPartyBean, corePartyBean)) {
					// Get parent organisation party
					CorePartyIdentifierBean corePartyIdentifierOrganisationParentBean = CorePartyBuilder.getPartyIdentifierBySourceSystem(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourceParentBusId(), dataSpace);
					PartyConverter.fillCorePartyBeanWithStagingPartyBean(stagingPartyBean, corePartyBean, corePartyIdentifierOrganisationParentBean != null  ? corePartyIdentifierOrganisationParentBean.getPartyId() : null);
					if (!(stagingPartyBean.getAction() != null && stagingPartyBean.getAction().equals(Constants.RecordOperations.DELETE))) {
						corePartyBean.setLastSyncAction(Constants.RecordOperations.UPDATE);
					}

					if (isFullLoad) {
						recordCount++;
						corePartyBeansToUpdate.add(corePartyBean);
					} else {
						corePartyWriter.updateBean(corePartyBean);
					}
				}
			}
		} else {
			if (stagingPartyBean.getPartyType() == null || stagingPartyBean.getPartyType().isEmpty()) {
				throw new RuntimeException(String.format("Party type for staging party with ID %s not found", stagingPartyBean.getId()));
			}

			// For Non CRM records, Check CRM Id presented
			if (!isCrmRecord(stagingPartyBean.getSourceSystem()) && stagingPartyBean.getCrmId() != null) {
				corePartyBean = hasCrmPartyIdentifierForNonCrmRecords(stagingPartyBean, dataSpace);
			} 
			
			if(corePartyBean == null) { // Look for core parties by duplicate checks
				corePartyBean = getCoreDuplicatedPartyBean(stagingPartyBean, corePartyBean, dataSpace);
			}
		
			if (corePartyBean != null) {
				stagingPartyLastUpdateTimestamp = stagingPartyBean.getLastUpdateTimestamp() == null ? dateMinimum : stagingPartyBean.getLastUpdateTimestamp();
				corePartyLastUpdateTimestamp = corePartyBean.getLastUpdateTimestamp() == null ? dateMinimum : corePartyBean.getLastUpdateTimestamp();
				// Look for core party with latest LastSyncTimestamp
				if (stagingPartyLastUpdateTimestamp.compareTo(corePartyLastUpdateTimestamp) >= 0) {
					// Core party found by duplicate checks and LastUpdateTimestamp is newer
					if (isCrmRecord(stagingPartyBean.getSourceSystem()) || isStagingAndCoreNonCrmRecords(stagingPartyBean, corePartyBean)) {

						// Get parent organisation party
						CorePartyIdentifierBean corePartyIdentifierOrganisationParentBean = CorePartyBuilder.getPartyIdentifierBySourceSystem(corePartyBean.getSourceSystem(), corePartyBean.getSourceParentBusId(), dataSpace);
						PartyConverter.fillCorePartyBeanWithStagingPartyBean(stagingPartyBean, corePartyBean, corePartyIdentifierOrganisationParentBean != null
										? corePartyIdentifierOrganisationParentBean.getPartyId() : null);
						if (!(stagingPartyBean.getAction() != null && stagingPartyBean.getAction().equals(Constants.RecordOperations.DELETE))) {
							corePartyBean.setLastSyncAction(Constants.RecordOperations.UPDATE);
						}
						if (isFullLoad) {
							recordCount++;
							corePartyBeansToUpdate.add(corePartyBean);
						} else {
							corePartyWriter.updateBean(corePartyBean);
						}
					}
				}

				if (corePartyIdentifierBean == null) {
					// Create a party identifier
					corePartyIdentifierBean = CorePartyBuilder.createPartyIdentifier(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId(), corePartyBean.getId());
					if (isFullLoad) {
						recordCount++;
						corePartyIdentifierBeanToCreate.add(corePartyIdentifierBean);
					} else {
						corePartyIdentifierWriter.createBeanOrThrow(corePartyIdentifierBean);
					}
				}
			} else {
				// Get parent organisation party
				CorePartyIdentifierBean corePartyIdentifierOrganisationParentBean = CorePartyBuilder.getPartyIdentifierBySourceSystem(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourceParentBusId(), dataSpace);

				// Create a core party
				corePartyBean = PartyConverter.createCorePartyBeanFromStagingPartyBean(stagingPartyBean, corePartyIdentifierOrganisationParentBean != null ? corePartyIdentifierOrganisationParentBean.getPartyId() : null);
				corePartyBean.setLastSyncAction(Constants.RecordOperations.CREATE);
				if (isFullLoad) {
					recordCount++;
					if (!corePartyBean.getPartyType().equals(PartyConstants.Entities.Party.PartyType.CONTACT)) {
						boolean shouldCreate = processDuplicateChecksInStagingParties(stagingPartyBean, dataSpace);
						if(shouldCreate) {
							coreCRMPartyBeansToCreate.add(corePartyBean);
						} else {
							stagingPartyBeanToreprocess.add(stagingPartyBean);
						}
					} else {
						if (isCrmRecord(corePartyBean.getSourceSystem())) {
							coreCRMPartyBeansToCreate.add(corePartyBean);
						} else {
							coreNonCRMPartyBeansToCreate.add(corePartyBean);
						}
					}
				} else {

					Optional<CorePartyBean> corePartyBeanOptional = corePartyWriter.createBeanOrThrow(corePartyBean);
					corePartyBean = corePartyBeanOptional.get();

					// Set PartyBusId
					PartyConverter.fillCorePartyBeanWithStagingPartyBean(stagingPartyBean, corePartyBean, corePartyIdentifierOrganisationParentBean != null
									? corePartyIdentifierOrganisationParentBean.getPartyId() : null);
					corePartyBean.setLastSyncAction(Constants.RecordOperations.CREATE);
					corePartyBean = corePartyWriter.updateBean(corePartyBean);

					// Create a party identifier
					corePartyIdentifierBean = CorePartyBuilder.createPartyIdentifier(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId(), corePartyBean.getId());
					corePartyIdentifierWriter.createBeanOrThrow(corePartyIdentifierBean);
				}
			}
		}

		if (!isFullLoad) {
			// Update staging party with core party ID
			if (corePartyBean != null) {
				stagingPartyBean.setCorePartyId(String.valueOf(corePartyBean.getId()));
				stagingPartyWriter.updateBean(stagingPartyBean);
			}
		}

		if (recordCount > getCommitSize()) {
			recordCount = 0;
			createCoreParty(coreCRMPartyBeansToCreate, session, dataSpace);
			createCoreParty(coreNonCRMPartyBeansToCreate, session, dataSpace);
			createCorePartyIdentifier(corePartyIdentifierBeanToCreate, session, dataSpace);
			updateCoreParty(corePartyBeansToUpdate, session, dataSpace);

			// To clear the process List
			coreCRMPartyBeansToCreate.clear();
			coreNonCRMPartyBeansToCreate.clear();
			corePartyIdentifierBeanToCreate.clear();
			corePartyBeansToUpdate.clear();
			duplicateCheckCRMPartyBeansToCreate.clear();
		}
}

	/**
	 * 
	 * @param corePartyReader
	 * @param stagingPartyBean
	 * @param corePartyBean
	 * @return
	 */
	private static List<CorePartyBean> getCorePartiesByDuplicateChecks(StagingPartyBean stagingPartyBean, CorePartyBean corePartyBean, String dataSpace) {

		CorePartyReader corePartyReader = new CorePartyReader(dataSpace);
		List<CorePartyBean> corePartyBeans = new ArrayList<CorePartyBean>();
		switch (stagingPartyBean.getPartyType()) {
		case PartyConstants.Entities.Party.PartyType.INDIVIDUAL:
			
			if (stagingPartyBean.getFirstName() != null && stagingPartyBean.getLastName() != null) {
				corePartyBean = getDuplicatePartyForIndividualPerson(stagingPartyBean, corePartyReader, dataSpace);
			}
			
			if (corePartyBean != null) {
				corePartyBeans.add(corePartyBean);
			}
			
			return corePartyBeans;

		case PartyConstants.Entities.Party.PartyType.CONTACT:  // No duplicate rules for party type contact defined
			if (corePartyBean == null) {
				return corePartyBeans;
			}
			corePartyBeans.add(corePartyBean);
			return corePartyBeans;

		case PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT:

			if(stagingPartyBean.getOrgName() != null && stagingPartyBean.getSourceParentBusId() != null) {
				corePartyBean = getDuplicatePartyForOrgUnitOrCompany(stagingPartyBean, corePartyReader, dataSpace);
			}
			if(corePartyBean != null) {
				corePartyBeans.add(corePartyBean);
			}
			
			return corePartyBeans;
					
		case PartyConstants.Entities.Party.PartyType.COMPANY:
			// OrgName is set and Same
			if(stagingPartyBean.getOrgName() != null) {
				corePartyBean = getDuplicatePartyForOrgUnitOrCompany(stagingPartyBean, corePartyReader, dataSpace);
			}
			if (corePartyBean != null) {
				corePartyBeans.add(corePartyBean);
			}
			
			return corePartyBeans;
		default:
			throw new RuntimeException(String.format("Unknown party type %s", stagingPartyBean.getPartyType()));
		}
	}
	
	private static CorePartyBean getDuplicatePartyForOrgUnitOrCompany(StagingPartyBean stagingPartyBean, CorePartyReader corePartyReader, String dataSpace) {
		
		// Check wheather staging party has legal address
		StagingAddressBean stagingAddressBean = hasLegalAddressFoundForStagingParty(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId(), dataSpace, null);
		if(stagingAddressBean != null) {
			String stagingPostCode = stagingAddressBean.getPostCode();
			String partyType = stagingPartyBean.getPartyType();
			List<Adaptation> duplicateCorePartyBeans = null ;
			if(partyType.equalsIgnoreCase(PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT)) {
				duplicateCorePartyBeans = corePartyReader.findBeansOrganisationUnit(stagingPartyBean.getOrgName(), stagingPartyBean.getSourceParentBusId(), stagingPartyBean.getPartyType());
			} else if(partyType.equalsIgnoreCase(PartyConstants.Entities.Party.PartyType.COMPANY)){
				duplicateCorePartyBeans = corePartyReader.findBeansCompany(stagingPartyBean.getOrgName(), stagingPartyBean.getOrgName2(), stagingPartyBean.getUidNumber(), stagingPartyBean.getCommercialRegNo(), stagingPartyBean.getPartyType());
			}
			if(duplicateCorePartyBeans == null || duplicateCorePartyBeans.isEmpty()) {
				return null;
			} else {
				// Check if duplicate list has CRM record or not
				List<Adaptation> crmList = duplicateCorePartyBeans.stream().filter(corePartyRecord -> StringUtils.equals("CRM", corePartyRecord.getString(PartyPaths._Party._Control_SourceSystem))).collect(Collectors.toList());
				List<Adaptation> nonCrmList = duplicateCorePartyBeans.stream().filter(corePartyRecord -> (!corePartyRecord.getString(PartyPaths._Party._Control_SourceSystem).equalsIgnoreCase("CRM"))).collect(Collectors.toList());
				if(!crmList.isEmpty()) {
					for(Adaptation crmRecord : crmList) {
						if(StringUtils.equals(partyType, PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT) || (StringUtils.equals(partyType, PartyConstants.Entities.Party.PartyType.COMPANY) && isCompanyMatches(stagingPartyBean, crmRecord))) {
							String postalAddress = crmRecord.getString(PartyPaths._Party._PartyInfo_LegalPostalAddress);
							if(postalAddress == null && isFullLoad) {
								StagingAddressBean corePartyStagingAddressBean = hasLegalAddressFoundForStagingParty(crmRecord.getString(PartyPaths._Party._Control_SourceSystem), crmRecord.getString(PartyPaths._Party._Control_SourcePartyBusId), dataSpace, null);
								if(corePartyStagingAddressBean != null && StringUtils.equals(stagingPostCode, corePartyStagingAddressBean.getPostCode())) {
									CorePartyBean bean = corePartyReader.createBeanFromRecord(crmRecord);
									return bean;
								}
							}
							if(postalAddress != null) {
								RequestResult partyAddresses = crmRecord.getSchemaNode().getNode(PartyPaths._Party._Addresses).getAssociationLink().getAssociationResult(crmRecord);
								for(Adaptation partyAddress; (partyAddress = partyAddresses.nextAdaptation()) != null;) {
									String addressUse = partyAddress.getString(PartyPaths._Address._AddressInfo_AddressUse);
									String addressType = partyAddress.getString(PartyPaths._Address._AddressInfo_AddressType);
									String postCode = partyAddress.getString(PartyPaths._Address._PostalAddress_PostCode);
									if(StringUtils.equals(addressUse, PartyConstants.Entities.Party.AddressType.ADDRESS_USE_ONE) && StringUtils.equals(addressType, PartyConstants.Entities.Party.AddressType.POSTAL_ADDRESS) 
											&& StringUtils.equals(postCode, stagingPostCode)) {
										CorePartyBean bean = corePartyReader.createBeanFromRecord(crmRecord);
										return bean;
									}
								}
								partyAddresses.close();
							}
						}
					}
				} else {
					List<Adaptation> corePartyRecordsOrdered = nonCrmList.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getDate(PartyPaths._Party._Control_LastUpdateTimestamp), b.getDate(PartyPaths._Party._Control_LastUpdateTimestamp)))
										.collect(Collectors.toList());
					for(Adaptation corePartyRecord : corePartyRecordsOrdered) {
						if(StringUtils.equals(partyType, PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT) || (StringUtils.equals(partyType, PartyConstants.Entities.Party.PartyType.COMPANY) && isCompanyMatches(stagingPartyBean, corePartyRecord))) {
							String postalAddress = corePartyRecord.getString(PartyPaths._Party._PartyInfo_LegalPostalAddress);
							if(postalAddress == null && isFullLoad) {
								StagingAddressBean corePartyStagingAddressBean = hasLegalAddressFoundForStagingParty(corePartyRecord.getString(PartyPaths._Party._Control_SourceSystem), corePartyRecord.getString(PartyPaths._Party._Control_SourcePartyBusId), dataSpace, null);
								if(corePartyStagingAddressBean != null && StringUtils.equals(stagingPostCode, corePartyStagingAddressBean.getPostCode())) {
									CorePartyBean bean = corePartyReader.createBeanFromRecord(corePartyRecord);
									return bean;
								}
							} else {
								RequestResult partyAddresses = corePartyRecord.getSchemaNode().getNode(PartyPaths._Party._Addresses).getAssociationLink().getAssociationResult(corePartyRecord);
								for(Adaptation partyAddress; (partyAddress = partyAddresses.nextAdaptation()) != null;) {
									String addressUse = partyAddress.getString(PartyPaths._Address._AddressInfo_AddressUse);
									String addressType = partyAddress.getString(PartyPaths._Address._AddressInfo_AddressType);
									String postCode = partyAddress.getString(PartyPaths._Address._PostalAddress_PostCode);
									// find legal address
									if(StringUtils.equals(addressUse, PartyConstants.Entities.Party.AddressType.ADDRESS_USE_ONE) && StringUtils.equals(addressType, PartyConstants.Entities.Party.AddressType.POSTAL_ADDRESS)
											&& StringUtils.equals(postCode, stagingPostCode)) {
										CorePartyBean bean = corePartyReader.createBeanFromRecord(corePartyRecord);
										return bean;
									}
								}
								partyAddresses.close();
							}
						}
					}
				}
			} 
		}
		return null;
	}
	
	private static Boolean isCompanyMatches(StagingPartyBean stagingPartyBean, Adaptation corePartyBean) {
		String coreOrgName2 = corePartyBean.getString(PartyPaths._Party._Organisation_OrgName2);
		String coreUidNumber = corePartyBean.getString(PartyPaths._Party._Organisation_UIDNumber);
		String coreCommercialRefNo = corePartyBean.getString(PartyPaths._Party._Organisation_CommercialRegNo);
		if((stagingPartyBean.getOrgName2() == null || coreOrgName2 == null || StringUtils.equals(coreOrgName2, stagingPartyBean.getOrgName2())) && 
				(coreUidNumber == null || stagingPartyBean.getUidNumber() == null || StringUtils.equals(coreUidNumber, stagingPartyBean.getUidNumber())) && 
					(coreCommercialRefNo == null || stagingPartyBean.getCommercialRegNo() == null || StringUtils.equals(coreCommercialRefNo, stagingPartyBean.getCommercialRegNo()))
				) {
			return true;
		}
		return false;
	}
	
	private static Boolean isCompanyMatches(StagingPartyBean stagingPartyBean, StagingPartyBean corePartyBean) {
		if((stagingPartyBean.getOrgName2() == null || corePartyBean.getOrgName2() == null || StringUtils.equals(corePartyBean.getOrgName2(), stagingPartyBean.getOrgName2())) && 
				(corePartyBean.getUidNumber() == null || stagingPartyBean.getUidNumber() == null || StringUtils.equals(corePartyBean.getUidNumber(), stagingPartyBean.getUidNumber())) && 
					(corePartyBean.getCommercialRegNo() == null || stagingPartyBean.getCommercialRegNo() == null || StringUtils.equals(corePartyBean.getCommercialRegNo(), stagingPartyBean.getCommercialRegNo()))
				) {
			return true;
		}
		return false;
	}
		
	private static Boolean isDateOfBirthAndSSNMatches(String stagingSocialSecurityNr, String coreSocialSecurityNr, Date stagingDateOfBirth, Date coreDateOfBirth) {
		if(stagingSocialSecurityNr != null && StringUtils.equals(stagingSocialSecurityNr, coreSocialSecurityNr)) {
			return true;
		} else {
			if(stagingSocialSecurityNr == null || coreSocialSecurityNr == null || 
					StringUtils.equals(stagingSocialSecurityNr, coreSocialSecurityNr)) {
				if(DateNullableComparator.getInstance().compare(stagingDateOfBirth, coreDateOfBirth) == 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	private static CorePartyBean getDuplicatePartyForIndividualPerson(StagingPartyBean stagingPartyBean, CorePartyReader corePartyReader, String dataSpace) {
		
		List<CorePartyBean> duplicatePartyBeans = new ArrayList<CorePartyBean>();
		// Check wheather staging party has legal address
		StagingAddressBean stagingAddressBean = hasLegalAddressFoundForStagingParty(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId(), dataSpace, null);
		if(stagingAddressBean != null) {
			String stagingPostCode = stagingAddressBean.getPostCode();
			// String partyType = stagingPartyBean.getPartyType();
			List<CorePartyBean> duplicateCorePartyBeans = corePartyReader.findBeansIndividualPerson(stagingPartyBean.getFirstName(), stagingPartyBean.getLastName(), stagingPartyBean.getDateOfBirth(),
						stagingPartyBean.getSocialSecurityNr(), stagingPartyBean.getSocialSecurityNrType(), stagingPartyBean.getSourceOrganisationBusId());
			if(duplicateCorePartyBeans.isEmpty()) {
				return null;
			} else {
				// Check if duplicate list has CRM record or not
				List<CorePartyBean> crmList = duplicateCorePartyBeans.stream().filter(corePartyRecord -> StringUtils.equals("CRM", corePartyRecord.getSourceSystem())).collect(Collectors.toList());
				List<CorePartyBean> nonCrmList = duplicateCorePartyBeans.stream().filter(corePartyRecord -> (!corePartyRecord.getSourceSystem().equalsIgnoreCase("CRM"))).collect(Collectors.toList());
				if(!crmList.isEmpty()) {
					for(CorePartyBean crmRecord : crmList) {
						if(isDateOfBirthAndSSNMatches(stagingPartyBean.getSocialSecurityNr(), 
								crmRecord.getSocialSecurityNr(), stagingPartyBean.getDateOfBirth(), crmRecord.getDateOfBirth())) {
								// (crmRecord.getSocialSecurityNr() == null || stagingPartyBean.getSocialSecurityNr() == null || Objects.equals(crmRecord.getSocialSecurityNr(), stagingPartyBean.getSocialSecurityNr()))) {
							String postalAddress = crmRecord.getLegalAddressId();
							if(postalAddress == null && isFullLoad) {
								StagingAddressBean corePartyStagingAddressBean = hasLegalAddressFoundForStagingParty(crmRecord.getSourceSystem(), crmRecord.getSourcePartyBusId(), dataSpace, null);
								if(corePartyStagingAddressBean != null && StringUtils.equals(stagingPostCode, corePartyStagingAddressBean.getPostCode())) {
									return crmRecord;
								}
							}
							if(postalAddress != null) {
								CoreAddressReader addressReader = new CoreAddressReader(dataSpace);
								CoreAddressBean addressBean = addressReader.findBeanByAddressId(Integer.parseInt(postalAddress));
								if(addressBean != null && StringUtils.equals(stagingPostCode, addressBean.getPostCode())) {
									return crmRecord;
								}
							}
						}
					}
				} else {
					List<CorePartyBean> corePartyRecordsOrdered = nonCrmList.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getLastUpdateTimestamp(), b.getLastUpdateTimestamp()))
							.collect(Collectors.toList());
					for(CorePartyBean corePartyRecord : corePartyRecordsOrdered) {
						if(isDateOfBirthAndSSNMatches(stagingPartyBean.getSocialSecurityNr(), 
								corePartyRecord.getSocialSecurityNr(), stagingPartyBean.getDateOfBirth(), corePartyRecord.getDateOfBirth())) {
								// (corePartyRecord.getSocialSecurityNr() == null || stagingPartyBean.getSocialSecurityNr() == null || Objects.equals(corePartyRecord.getSocialSecurityNr(), stagingPartyBean.getSocialSecurityNr()))) {
							String postalAddress = corePartyRecord.getLegalAddressId();
							if(postalAddress == null && isFullLoad) {
								StagingAddressBean corePartyStagingAddressBean = hasLegalAddressFoundForStagingParty(corePartyRecord.getSourceSystem(), corePartyRecord.getSourcePartyBusId(), dataSpace, null);
								if(corePartyStagingAddressBean != null && StringUtils.equals(stagingPostCode, corePartyStagingAddressBean.getPostCode())) {
									return corePartyRecord;
								}
							} 
							
							if(postalAddress != null) {
								CoreAddressReader addressReader = new CoreAddressReader(dataSpace);
								CoreAddressBean addressBean = addressReader.findBeanByAddressId(Integer.parseInt(postalAddress));
								if(addressBean != null && StringUtils.equals(stagingPostCode, addressBean.getPostCode())) {
									return corePartyRecord;
								}
							}
						}
					}
				}
			}
			
			CorePartyBean coreParty = duplicatePartyBeans.size() > 0 ? duplicatePartyBeans.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getLastSyncTimestamp(),a.getLastSyncTimestamp()))
					.findFirst().get() : null;
			return coreParty;
		}
		return null;
	}

	/**
	 * 
	 * @param session
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @param validFrom
	 * @param validTo
	 */
	public static void deleteCorePartyByStagingParty(Session session, String sourceSystem, String sourcePartyBusId, Date validFrom, Date validTo) {

		StagingPartyReader stagingPartyReader = new StagingPartyReader();
		// Get staging party bean
		List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId, validFrom, validTo);
		if (stagingPartyBeans.isEmpty()) {
			return;
		}
		StagingPartyBean stagingPartyBean = stagingPartyBeans.get(0);
		deleteCorePartyByStagingParty(session, stagingPartyBean);
	}

	/**
	 * 
	 * @param procedureContext
	 * @param stagingPartyBean
	 */
	public static void deleteCorePartyByStagingParty(ProcedureContext procedureContext, StagingPartyBean stagingPartyBean) {

		StagingPartyWriter stagingPartyWriter = new StagingPartyWriterProcedureContext(procedureContext, false);
		CorePartyWriter corePartyWriter = new CorePartyWriterProcedureContext(procedureContext, false);
		CorePartyIdentifierWriter corePartyIdentifierWriter = new CorePartyIdentifierWriterProcedureContext(procedureContext);

		deleteCorePartyByStagingParty(stagingPartyWriter, corePartyWriter, corePartyIdentifierWriter, stagingPartyBean);
	}

	/**
	 * 
	 * @param session
	 * @param stagingPartyBean
	 */
	public static void deleteCorePartyByStagingParty(Session session, StagingPartyBean stagingPartyBean) {

		StagingPartyWriter stagingPartyWriter = new StagingPartyWriterSession(session, false);
		CorePartyWriter corePartyWriter = new CorePartyWriterSession(session, false);
		CorePartyIdentifierWriter corePartyIdentifierWriter = new CorePartyIdentifierWriterSession(session);

		deleteCorePartyByStagingParty(stagingPartyWriter, corePartyWriter, corePartyIdentifierWriter, stagingPartyBean);
	}

	/**
	 * 
	 * @param stagingPartyWriter
	 * @param corePartyWriter
	 * @param stagingPartyBean
	 */
	public static void deleteCorePartyByStagingParty(StagingPartyWriter stagingPartyWriter, CorePartyWriter corePartyWriter, CorePartyIdentifierWriter corePartyIdentifierWriter,
			StagingPartyBean stagingPartyBean) {

		StagingPartyReader stagingPartyReader = (StagingPartyReader) stagingPartyWriter.getTableReader();
		CorePartyReader corePartyReader = (CorePartyReader) corePartyWriter.getTableReader();
		// Get core party bean referenced by staging party bean
		if (stagingPartyBean.getCorePartyId() == null || stagingPartyBean.getCorePartyId().isEmpty()) {
			return;
		}

		CorePartyBean corePartyBean = corePartyReader.findBeanByPartyId(Integer.valueOf(stagingPartyBean.getCorePartyId()));
		if (corePartyBean == null) {
			return;
		}

		// Get staging party beans referenced to core party bean
		List<StagingPartyBean> stagingPartyBeansLinked = stagingPartyReader.findBeansByCorePartyId(String.valueOf(corePartyBean.getId()));

		// Unlink all staging party beans referenced to core party bean
		stagingPartyBeansLinked.forEach(spb -> { spb.setCorePartyId(null); stagingPartyWriter.updateBean(spb); });

		// Delete party identifiers
		CorePartyIdentifierReader corePartyIdentifierReader = (CorePartyIdentifierReader) corePartyIdentifierWriter.getTableReader();

		List<CorePartyIdentifierBean> corePartyIdentifierBeans = corePartyIdentifierReader.findBeansByPartyId(stagingPartyBean.getCorePartyId());
		corePartyIdentifierBeans.forEach(cpi -> { corePartyIdentifierWriter.delete(cpi); });

		// Delete core party bean referenced by staging party bean
		corePartyWriter.delete(corePartyBean);
	}

	/**
	 * 
	 * @param coreAddressWriter
	 * @param coreBankAccountWriter
	 * @param corePartyBean
	 */
	public static void propagateIsExpired(CoreAddressWriter coreAddressWriter, CoreBankAccountWriter coreBankAccountWriter, CorePartyBean corePartyBean) {
		// Get all linked addresses
		CoreAddressReader coreAddressReader = (CoreAddressReader) coreAddressWriter.getTableReader();
		List<CoreAddressBean> coreAddressBeans = coreAddressReader.findBeansByPartyId(corePartyBean.getId());

		// Set isExpired to all linked addresses from party
		coreAddressBeans.forEach(cab -> {
			cab.setIsExpired(corePartyBean.getIsExpired());
			coreAddressWriter.updateBean(cab);
		});

		// Get all linked bank accounts
		CoreBankAccountReader coreBankAccountReader = (CoreBankAccountReader) coreBankAccountWriter.getTableReader();
		List<CoreBankAccountBean> coreBankAccountBeans = coreBankAccountReader.findBeansByPartyId(corePartyBean.getId());

		// Set isExpired to all linked bank accounts from party
		coreBankAccountBeans.forEach(cbab -> {
			cbab.setIsExpired(corePartyBean.getIsExpired());
			coreBankAccountWriter.updateBean(cbab);
		});
	}

	/**
	 * 
	 * @param corePartyWriter
	 * @param coreAddressWriter
	 * @param coreBankAccountWriter
	 */
	public static void markExpiredParties(CorePartyWriter corePartyWriter, CoreAddressWriter coreAddressWriter, CoreBankAccountWriter coreBankAccountWriter) {

		CorePartyReader corePartyReader = (CorePartyReader) corePartyWriter.getTableReader();
		List<CorePartyBean> corePartyBeans = corePartyReader.findBeansExpired();

		// Mark core party beans by valid from and valid to date as expired
		corePartyBeans.forEach(cpb -> {
			cpb.setIsExpired(true);
			corePartyWriter.updateBean(cpb);
			CoreAddressBuilder.markExpiredAddressesByParty(coreAddressWriter, cpb.getId());
			CoreBankAccountBuilder.markExpiredBankAccountsByParty(coreBankAccountWriter, cpb.getId());
		});
		
		/*
		StagingPartyReader stagingPartyReader = new StagingPartyReader();
		CorePartyIdentifierReader corePartyIdentifierReader = new CorePartyIdentifierReader();
		List<CorePartyBean> corePartyBeansExpired = corePartyReader.findBeansByIsExpired(false);

		// Mark core party beans by staging party beans valid to dates as expired
		corePartyBeansExpired.forEach(cpb -> {
			List<CorePartyIdentifierBean> corePartyIdentifierBeans = corePartyIdentifierReader.findBeansByPartyId(String.valueOf(cpb.getId()));
			if (corePartyIdentifierBeans == null || corePartyIdentifierBeans.isEmpty()) {
				return;
			}

			CorePartyIdentifierBean corePartyIdentifierBean = corePartyIdentifierBeans.get(0);
			List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(corePartyIdentifierBean.getSourceSystem(), corePartyIdentifierBean.getSourcePartyBusId());
			Optional<StagingPartyBean> stagingPartyBeanOptional = stagingPartyBeans.stream().max((a, b) -> DateNullableComparator.getInstance().compare(a.getUpdateTimestamp(), b.getUpdateTimestamp()));
			if (!stagingPartyBeanOptional.isPresent()) {
				return;
			}

			StagingPartyBean stagingPartyBean = stagingPartyBeanOptional.get();
			if (stagingPartyBean.getValidTo() == null) {
				return;
			}

			Date date = DateNullableComparator.removeTime(new Date());
			boolean before = stagingPartyBean.getValidTo().before(date);
			if (before) {
				cpb.setValidTo(stagingPartyBean.getValidTo());
				cpb.setIsExpired(true);
				corePartyWriter.updateBean(cpb);
				CoreAddressBuilder.markExpiredAddressesByParty(coreAddressWriter, cpb.getId());
				CoreBankAccountBuilder.markExpiredBankAccountsByParty(coreBankAccountWriter, cpb.getId());
			}
		});
		*/
	}

	/**
	 * 
	 * For NON CRM records, if CRM id is present, Check the CRM Party Identifier. If
	 * not, Check the duplicates.
	 * 
	 * @param corePartyWriter
	 * @param stagingPartyBean
	 * @param partyBean
	 * @return
	 */
	private static CorePartyBean hasCrmPartyIdentifierForNonCrmRecords(StagingPartyBean stagingPartyBean, String dataSpace) {

		String crmId = stagingPartyBean.getCrmId();
		CorePartyBean corePartyBean = getCrmPartyIdentifier(crmId, dataSpace);
		if (corePartyBean != null) {
			return corePartyBean;
		}
		return null;
	}

	/**
	 * Get CRM PartyIdentifier by sourcePartyBusId
	 * 
	 * @param sourcePartyBusId
	 * @return
	 */
	private static CorePartyBean getCrmPartyIdentifier(String sourcePartyBusId, String dataSpace) {

		CorePartyBean corePartyBean = null;
		if (sourcePartyBusId != null) {
			CorePartyIdentifierBean corePartyIdentifierBean = CorePartyBuilder.getPartyIdentifierBySourceSystem(PartyConstants.SourceSystems.CRM, sourcePartyBusId, dataSpace);
			corePartyBean = CorePartyBuilder.getPartyByPartyIdentifier(corePartyIdentifierBean, dataSpace);
		}
		return corePartyBean;
	}

	/**
	 * Check if it is CRM Record or not
	 * 
	 * @param stagingPartyBean
	 * @return
	 */
	private static boolean isCrmRecord(String sourceSystem) {
		if (sourceSystem != null && sourceSystem.equals(PartyConstants.SourceSystems.CRM)) {
			return true;
		}

		return false;

	}

	/**
	 * 
	 * @param stagingPartyBean
	 * @param corePartyBean
	 * @return
	 */
	private static boolean isStagingAndCoreNonCrmRecords(StagingPartyBean stagingPartyBean,
			CorePartyBean corePartyBean) {
		if (stagingPartyBean.getSourceSystem() != null && corePartyBean.getSourceSystem() != null) {
			if (corePartyBean.getSourceSystem().equals(PartyConstants.SourceSystems.CRM)) {
				return false;
			}
			return true;
		}

		return false;
	}

	/**
	 * Get duplicated records
	 * 
	 * @param corePartyWriter
	 * @param stagingPartyBean
	 * @param partyBean
	 * @return
	 */
	private static CorePartyBean getCoreDuplicatedPartyBean(StagingPartyBean stagingPartyBean, CorePartyBean partyBean, String dataSpace) {

		// Look for core parties by duplicate checks
		List<CorePartyBean> corePartyBeans = getCorePartiesByDuplicateChecks(stagingPartyBean, partyBean, dataSpace);
		// Look for core party with latest LastSyncTimestamp
		CorePartyBean coreParty = corePartyBeans.size() > 0 ? corePartyBeans.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getLastSyncTimestamp(),a.getLastSyncTimestamp()))
								.findFirst().get() : null;
		return coreParty;
	}

	private static void createCoreParty(List<CorePartyBean> corePartyBeans, Session session, String dataSpace) throws OperationException {
		List<CorePartyBean> corePartyBeansListToC = new ArrayList<CorePartyBean>();
		Map<String, List<CorePartyBean>> corePartyBeansGroupedBySourceSystem = corePartyBeans.stream().collect(Collectors.groupingBy(cpb -> cpb.getSourceSystem() + cpb.getSourcePartyBusId()));
		for (Map.Entry<String, List<CorePartyBean>> sourceSysAndBusIdKey : corePartyBeansGroupedBySourceSystem.entrySet()) {
			List<CorePartyBean> corePartyBeanList = sourceSysAndBusIdKey.getValue();
			if (corePartyBeanList.size() == 1) {
				corePartyBeansListToC.add(corePartyBeanList.get(0));
			} else {
				CorePartyBean corePartyBean = corePartyBeanList.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getLastUpdateTimestamp(), a.getLastUpdateTimestamp())).findFirst().get();
				corePartyBeansListToC.add(corePartyBean);
			}
		}

		AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._Party.getPathInSchema());
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);
		if (!corePartyBeansListToC.isEmpty()) {
			for (CorePartyBean corePartyBean : corePartyBeansListToC) {
				HashMap<Path, Object> values = CorePartyConverter.fillCorePartyValuesFromStagingPartyBean(corePartyBean, true);
				RecordValuesBean rvb = new RecordValuesBean(aTable, values);
				recordValuesBeanList.add(rvb);
			}

			// For Creates
			CreateRecords createRecordsProcedure = new CreateRecords(aTable, recordValuesBeanList);
			ProcedureResult result = service.execute(createRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
				throw exception;
			}
			totalRecordsCount = totalRecordsCount + recordValuesBeanList.size();
			batchCount++;
			VM.log.kernelDebug(recordValuesBeanList.size()+" records are processed in "+ batchCount);
			VM.log.kernelDebug("Total processed records till now is "+totalRecordsCount);
			List<Adaptation> createdPartyList = createRecordsProcedure.getCreateRecordList();
			if (!createdPartyList.isEmpty()) {
				createCorePartyIdentifiers(createdPartyList, session, dataSpace);
			}
			corePartyBeans.clear();
		}
	}

	private static void updateCoreParty(List<CorePartyBean> corePartyBeans, Session session, String dataSpace) {

		AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._Party.getPathInSchema());
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);
		RecordValuesBean rvb = null;
		HashMap<Path, Object> values = null;
		for (CorePartyBean corePartyBean : corePartyBeans) {
			values = CorePartyConverter.fillCorePartyValuesFromStagingPartyBean(corePartyBean, true);
			rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(corePartyBean.getId().toString())), values);
			recordValuesBeanList.add(rvb);
		}

		UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
		}
	}

	private static void createCorePartyIdentifier(List<CorePartyIdentifierBean> corePartyIdentifiers, Session session, String dataSpace) throws OperationException {

		if(!corePartyIdentifiers.isEmpty()) {
			AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._PartyIdentifier.getPathInSchema());
			List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
			final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);
			for (CorePartyIdentifierBean corePartyIdentifierBean : corePartyIdentifiers) {
				HashMap<Path, Object> values = CorePartyIdentifierConverter.fillCorePartyIdentifierValues(corePartyIdentifierBean);
				RecordValuesBean rvb = new RecordValuesBean(aTable, values);
				recordValuesBeanList.add(rvb);
			}

			// For Creates
			CreateRecords createRecordsProcedure = new CreateRecords(aTable, recordValuesBeanList);
			ProcedureResult result = service.execute(createRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
				throw exception;
			}
			if (!corePartyIdentifiers.isEmpty()) {
				linkStagingPartiesByCoreParties(session, dataSpace);
			}
		}
	}

	private static void createCorePartyIdentifiers(List<Adaptation> createPartyIdentifiersList, Session session, String dataSpace) throws OperationException {

		AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._PartyIdentifier.getPathInSchema());
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);

		for (Adaptation corePartyIdentifierRecord : createPartyIdentifiersList) {
			HashMap<Path, Object> values = CorePartyIdentifierConverter.fillCorePartyIdentifierValuesByRecord(corePartyIdentifierRecord);
			RecordValuesBean rvb = new RecordValuesBean(aTable, values);
			recordValuesBeanList.add(rvb);
		}

		// For Creates
		CreateRecords createRecordsProcedure = new CreateRecords(aTable, recordValuesBeanList);
		ProcedureResult result = service.execute(createRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}

		linkStagingPartiesByCoreParties(session, dataSpace);

	}

	private static void linkStagingPartiesByCoreParties(Session session, String dataSpace) throws OperationException {

		List<StagingPartyBean> stagingPartyBeansToUpdate = new ArrayList<StagingPartyBean>();
		CorePartyIdentifierReader partyIdentifierReader = new CorePartyIdentifierReader(stagingDataSet.getTable(PartyPaths._PartyIdentifier.getPathInSchema()));
		for (StagingPartyBean stagingPartyBean : stagingPartyBeansChunk) {
			List<CorePartyIdentifierBean> corePartyIdentifierBeans = partyIdentifierReader.getBeansBySourceSystem(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId());
			if (!corePartyIdentifierBeans.isEmpty()) {
				CorePartyIdentifierBean corePartyIdentifierBean = corePartyIdentifierBeans.get(0);
				stagingPartyBean.setCorePartyId(String.valueOf(corePartyIdentifierBean.getPartyId()));
				stagingPartyBeansToUpdate.add(stagingPartyBean);
			}
		}
		updateStagingParties(stagingPartyBeansToUpdate, session, dataSpace);
	}
	
	
	private static void processCrmDuplicateCheckRecords(List<CorePartyBean> corePartyBeansForDuplicatesChecks, Session session, String dataSpace) throws OperationException {

		List<CorePartyBean> corePartyBeansListToCreate = new ArrayList<CorePartyBean>();
		List<CorePartyBean> corePartyBeansListToReprocess = new ArrayList<CorePartyBean>();
		Map<String, List<CorePartyBean>> corePartyBeansGroupedBySourceSystem = corePartyBeansForDuplicatesChecks.stream().collect(Collectors.groupingBy(cpb -> cpb.getSourceSystem() + cpb.getSourcePartyBusId()));

		Date dateMinimum = new Date(Long.MIN_VALUE);
		String sourceSystem = null;
		for (Map.Entry<String, List<CorePartyBean>> sourceSysAndBusIdKey : corePartyBeansGroupedBySourceSystem.entrySet()) {
			List<CorePartyBean> corePartyBeanList = sourceSysAndBusIdKey.getValue();
			for (CorePartyBean corePartyBean : corePartyBeanList) {
				boolean isDuplicateNotFound = true;
				boolean isNotMatching = true;
				for (CorePartyBean partyBeans : corePartyBeansForDuplicatesChecks) {
					sourceSystem = partyBeans.getSourceSystem();
					if (StringUtils.equals(partyBeans.getSourceSystem(), corePartyBean.getSourceSystem()) && StringUtils.equals(partyBeans.getSourcePartyBusId(), corePartyBean.getSourcePartyBusId())) {

					} else {
						if (!corePartyBean.getSourceSystem().equals(PartyConstants.SourceSystems.CRM) && corePartyBean.getCrmId() != null) {
							isDuplicateNotFound = false;
							corePartyBeansListToReprocess.add(corePartyBean);
						} else {
							if(isBothInvidualOrPersonRecords(corePartyBean.getPartyType(), partyBeans.getPartyType()))
							{
								if (partyBeans.getFirstName() != null && partyBeans.getLastName() != null && (partyBeans.getDateOfBirth() != null || (partyBeans.getSocialSecurityNr() != null
												&& partyBeans.getSocialSecurityNrType() != null)) && partyBeans.getSourceOrganisationBusId() != null) {
									
									if (String.valueOf(partyBeans.getFirstName()).equalsIgnoreCase(String.valueOf(corePartyBean.getFirstName()))
											&& String.valueOf(partyBeans.getLastName()).equalsIgnoreCase(String.valueOf(corePartyBean.getLastName()))
											&& (partyBeans.getDateOfBirth().compareTo(corePartyBean.getDateOfBirth()) == 0 || (String.valueOf(partyBeans.getSocialSecurityNr()).equalsIgnoreCase(String.valueOf(corePartyBean.getSocialSecurityNr()))
															&& String.valueOf(partyBeans.getSocialSecurityNrType()).equalsIgnoreCase(String.valueOf(corePartyBean.getSocialSecurityNrType())))
															&& String.valueOf(partyBeans.getSourceOrganisationBusId()).equalsIgnoreCase(String.valueOf(
																			corePartyBean.getSourceOrganisationBusId())))) {
										isNotMatching = false;
									}
								}
							} else if(isBothOrgUnitRecords(corePartyBean.getPartyType(), partyBeans.getPartyType())) {
								if(String.valueOf(partyBeans.getOrgName()).equalsIgnoreCase(String.valueOf(corePartyBean.getOrgName())) && 
										String.valueOf(partyBeans.getSourceParentBusId()).equalsIgnoreCase(String.valueOf(corePartyBean.getSourceParentBusId()))) {
									
									StagingAddressBean legalAddressBean1 = hasLegalAddressFoundForStagingParty(corePartyBean.getSourceSystem(), corePartyBean.getSourcePartyBusId(), dataSpace, null);
									StagingAddressBean legalAddressBean2 = hasLegalAddressFoundForStagingParty(partyBeans.getSourceSystem(), partyBeans.getSourcePartyBusId(), dataSpace, null);
									if( legalAddressBean1 != null && legalAddressBean2 != null) {
										isNotMatching = false;
									}
								}
								
							} else if (isBothCompanyRecords(corePartyBean.getPartyType(), partyBeans.getPartyType())) {
								
								if(String.valueOf(partyBeans.getOrgName()).equalsIgnoreCase(String.valueOf(corePartyBean.getOrgName())) && 
										String.valueOf(partyBeans.getUidNumber()).equalsIgnoreCase(String.valueOf(corePartyBean.getUidNumber())) && 
											String.valueOf(partyBeans.getCommercialRegNo()).equalsIgnoreCase(String.valueOf(corePartyBean.getCommercialRegNo()))) {
									
									StagingAddressBean legalAddressBean1 = hasLegalAddressFoundForStagingParty(corePartyBean.getSourceSystem(), corePartyBean.getSourcePartyBusId(), dataSpace, null);
									StagingAddressBean legalAddressBean2 = hasLegalAddressFoundForStagingParty(partyBeans.getSourceSystem(), partyBeans.getSourcePartyBusId(), dataSpace, null);
									if( legalAddressBean1 != null && legalAddressBean2 != null) {
										isNotMatching = false;
									}
								}
							} 

							if(!isNotMatching) {
								isNotMatching = true;
								Date firstCorePartyLastUpdateTimestamp = corePartyBean.getLastUpdateTimestamp() == null ? dateMinimum : partyBeans.getLastUpdateTimestamp();
								Date secondCorePartyLastUpdateTimestamp2 = partyBeans.getLastUpdateTimestamp() == null ? dateMinimum : corePartyBean.getLastUpdateTimestamp();
								isDuplicateNotFound = false;
								if (corePartyBean.getSourceSystem().equals(PartyConstants.SourceSystems.CRM) && !isBothRecordsAreNonCRMRecords(corePartyBean, partyBeans)) {
									if(partyBeans.getSourceSystem().equals(PartyConstants.SourceSystems.CRM)) {
										if (corePartyBean.getLastUpdateTimestamp() == null && corePartyBean.getCreationTimestamp().compareTo(partyBeans.getCreationTimestamp()) > 0) {
											corePartyBeansListToCreate.add(corePartyBean);
										} else if(firstCorePartyLastUpdateTimestamp.compareTo(secondCorePartyLastUpdateTimestamp2) > 0) {
											corePartyBeansListToCreate.add(corePartyBean);
										}else {
											corePartyBeansListToReprocess.add(corePartyBean);
										}
									} else {
										corePartyBeansListToCreate.add(corePartyBean);
									}
									
								} else {
									
									if (corePartyBean.getLastUpdateTimestamp() == null && corePartyBean.getCreationTimestamp().compareTo(partyBeans.getCreationTimestamp()) > 0
											&& isBothRecordsAreNonCRMRecords(corePartyBean, partyBeans)) {

										corePartyBeansListToCreate.add(corePartyBean);
									} else if (firstCorePartyLastUpdateTimestamp.compareTo(secondCorePartyLastUpdateTimestamp2) > 0
											&& isBothRecordsAreNonCRMRecords(corePartyBean, partyBeans)) {

										corePartyBeansListToCreate.add(corePartyBean);
									} else {
										corePartyBeansListToReprocess.add(corePartyBean);
									}
								}
								
							}
						}
					}
				}
				if (isDuplicateNotFound && isNotMatching) {
					corePartyBeansListToCreate.add(corePartyBean);
				}
			}
		}

		// Create Core Party records
		createCoreParty(corePartyBeansListToCreate, session, dataSpace);
		processStagingPartyRecords(corePartyBeansListToReprocess, session, dataSpace);

	}
	
	private static Boolean processDuplicateChecksInStagingParties(StagingPartyBean stagingPartyBean, String dataSpace) {
		
		List<StagingPartyBean> duplicateCorePartyBeansInBatch = null;
		Boolean isTrue = false;
		List<StagingPartyBean> duplicatePartiesByLastUpdatedTimestamp = new ArrayList<StagingPartyBean>();
		if(stagingPartyBean.getPartyType().equalsIgnoreCase(PartyConstants.Entities.Party.PartyType.INDIVIDUAL))
		{
			StagingAddressBean stagingLegalAddress = hasLegalAddressFoundForStagingParty(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId(), dataSpace, null);
			if(stagingLegalAddress != null) {
				Map<String,List<StagingPartyBean>> stagingPartiesMap = partyTypesMap.get(PartyConstants.Entities.Party.PartyType.INDIVIDUAL);
				duplicatePartiesByLastUpdatedTimestamp.add(stagingPartyBean);
				if (stagingPartyBean.getFirstName() != null && stagingPartyBean.getLastName() != null && stagingPartyBean.getDateOfBirth() != null) {
					duplicateCorePartyBeansInBatch = stagingPartiesMap.get(stagingPartyBean.getFirstName()+stagingPartyBean.getLastName());
					if(duplicateCorePartyBeansInBatch != null && !duplicateCorePartyBeansInBatch.isEmpty()) {
						for(StagingPartyBean duplicateBean : duplicateCorePartyBeansInBatch) {
							if(!Objects.equals(duplicateBean.getId(), stagingPartyBean.getId()) && StringUtils.equals(duplicateBean.getPartyType(), PartyConstants.Entities.Party.PartyType.INDIVIDUAL)) {
								String socialSecurityNr =  duplicateBean.getSocialSecurityNr();
								if(isDateOfBirthAndSSNMatches(stagingPartyBean.getSocialSecurityNr(), socialSecurityNr, stagingPartyBean.getDateOfBirth(), duplicateBean.getDateOfBirth())) {
									StagingAddressBean legalAddressForDuplicateRecord = hasLegalAddressFoundForStagingParty(duplicateBean.getSourceSystem(), duplicateBean.getSourcePartyBusId(), dataSpace, null);
									if(legalAddressForDuplicateRecord != null && StringUtils.equals(legalAddressForDuplicateRecord.getPostCode(), stagingLegalAddress.getPostCode())) {
										duplicatePartiesByLastUpdatedTimestamp.add(duplicateBean);
									}
								}
							}
						}
					}
				}
			}
		} else {
			StagingAddressBean stagingLegalAddress = hasLegalAddressFoundForStagingParty(stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId(), dataSpace, null);
			if(stagingLegalAddress != null) {
				duplicatePartiesByLastUpdatedTimestamp.add(stagingPartyBean);
				String partyType = stagingPartyBean.getPartyType();
				if(partyType.equalsIgnoreCase(PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT)) {
				    if(stagingPartyBean.getOrgName() != null && stagingPartyBean.getSourceParentBusId() != null) {
				        Map<String,List<StagingPartyBean>> stagingOrgUnitsMap = partyTypesMap.get(PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT);
	                    if(stagingOrgUnitsMap != null) {
	                        duplicateCorePartyBeansInBatch = stagingOrgUnitsMap.get(stagingPartyBean.getOrgName()+stagingPartyBean.getSourceParentBusId());
	                    }
				    }
				} else if(partyType.equalsIgnoreCase(PartyConstants.Entities.Party.PartyType.COMPANY)){
				    if(stagingPartyBean.getOrgName() != null) {
				        Map<String,List<StagingPartyBean>> stagingOrgUnitsMap = partyTypesMap.get(PartyConstants.Entities.Party.PartyType.COMPANY);
	                    if(stagingOrgUnitsMap != null) {
	                        duplicateCorePartyBeansInBatch = stagingOrgUnitsMap.get(stagingPartyBean.getOrgName());
	                    }
				    }
				}
				
				if(duplicateCorePartyBeansInBatch != null && !duplicateCorePartyBeansInBatch.isEmpty()) {
					for(StagingPartyBean duplicateStagingRecord : duplicateCorePartyBeansInBatch) {
						if(!Objects.equals(duplicateStagingRecord.getId(), stagingPartyBean.getId())) {
							if(Objects.equals(duplicateStagingRecord.getPartyType(), PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT) || (Objects.equals(duplicateStagingRecord.getPartyType(), PartyConstants.Entities.Party.PartyType.COMPANY) && isCompanyMatches(stagingPartyBean, duplicateStagingRecord))) {
								StagingAddressBean stagingAddressBean = hasLegalAddressFoundForStagingParty(duplicateStagingRecord.getSourceSystem(), duplicateStagingRecord.getSourcePartyBusId(), dataSpace, null);
								if(stagingAddressBean != null && StringUtils.equals(stagingAddressBean.getPostCode(), stagingLegalAddress.getPostCode())) {
									duplicatePartiesByLastUpdatedTimestamp.add(duplicateStagingRecord);
									
								}
							}
						}
					}
				}
			}
		}

		StagingPartyBean stagingDuplicateParty = duplicatePartiesByLastUpdatedTimestamp.size() > 0 ? duplicatePartiesByLastUpdatedTimestamp.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getLastUpdateTimestamp(),a.getLastUpdateTimestamp()))
				.findFirst().get() : null;
		if(stagingDuplicateParty == null || Objects.equals(stagingDuplicateParty.getId(), stagingPartyBean.getId())) {
			isTrue = true;
		}
		return isTrue;
	}

	private static boolean isBothRecordsAreNonCRMRecords(CorePartyBean corePartyBean1, CorePartyBean corePartyBean2) {

		if (!corePartyBean1.getSourceSystem().equals(PartyConstants.SourceSystems.CRM) && !corePartyBean2.getSourceSystem().equals(PartyConstants.SourceSystems.CRM)) {
			return true;
		}

		return false;

	}
	
	/**
	 * Check if both records are individuals or person
	 * 
	 * @param firstRecordPartyType
	 * @param secondRecordPartyType
	 * @return
	 */
	private static boolean isBothInvidualOrPersonRecords(String firstRecordPartyType, String secondRecordPartyType) {
		
		if(StringUtils.equals(firstRecordPartyType, PartyConstants.Entities.Party.PartyType.INDIVIDUAL) && 
				StringUtils.equals(secondRecordPartyType, PartyConstants.Entities.Party.PartyType.INDIVIDUAL)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Check if both records are Org unit records or not
	 * 
	 * @param firstRecordPartyType
	 * @param secondRecordPartyType
	 * @return
	 */
	private static boolean isBothOrgUnitRecords(String firstRecordPartyType, String secondRecordPartyType) {
		
		if(StringUtils.equals(firstRecordPartyType, PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT) && 
				StringUtils.equals(secondRecordPartyType, PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * check if both records are company records or not
	 * 
	 * @param firstRecordPartyType
	 * @param secondRecordPartyType
	 * @return
	 */
	private static boolean isBothCompanyRecords(String firstRecordPartyType, String secondRecordPartyType) {
		
		if(StringUtils.equals(firstRecordPartyType, PartyConstants.Entities.Party.PartyType.COMPANY) && 
				StringUtils.equals(secondRecordPartyType, PartyConstants.Entities.Party.PartyType.COMPANY)) {
			return true;
		}
		
		return false;
	}

	private static void processStagingPartyRecords(List<CorePartyBean> corePartyBeansToReprocess, Session session, String dataSpace) {

		if (!corePartyBeansToReprocess.isEmpty() ) {
			if (!stagingPartyBeanToreprocess.isEmpty()) {
				for (CorePartyBean corePartyBean : corePartyBeansToReprocess) {
					for (StagingPartyBean stagingPartyBean : stagingPartyBeanToreprocess) {
						if (corePartyBean.getSourceSystem().equals(stagingPartyBean.getSourceSystem())
								&& corePartyBean.getSourcePartyBusId().equals(stagingPartyBean.getSourcePartyBusId())) {
							stagingListToProcessAfterCoreCreations.add(stagingPartyBean);
						}
					}
				}
			}
		}
	}

	public static void updatePartyBusIdAfterCreate(List<Adaptation> partyRecordsAfterCreate, Session session, String dataspace) throws OperationException {

		List<CorePartyBean> updateCorePartyBeansList = new ArrayList<CorePartyBean>();
		List<CorePartyIdentifierBean> createCorePartyIdentifierBeans = new ArrayList<CorePartyIdentifierBean>();
		List<StagingPartyBean> stagingPartyBeansToUpdate = new ArrayList<StagingPartyBean>();

		for (Adaptation partyRecord : partyRecordsAfterCreate) {

			CorePartyReader corePartyReader = new CorePartyReader();
			CorePartyBean corePartyBean = corePartyReader.createBeanFromRecord(partyRecord);
			for (StagingPartyBean stagingPartyBean : stagingPartyBeansChunk) {
				if (corePartyBean.getSourceSystem().equals(stagingPartyBean.getSourceSystem())
						&& corePartyBean.getSourcePartyBusId().equals(stagingPartyBean.getSourcePartyBusId())) {
					// Set PartyBusId
					PartyConverter.fillCorePartyBeanWithStagingPartyBean(stagingPartyBean, corePartyBean, null);
					corePartyBean.setLastSyncAction(Constants.RecordOperations.CREATE);
					updateCorePartyBeansList.add(corePartyBean);

					stagingPartyBean.setCorePartyId(String.valueOf(corePartyBean.getId()));
					stagingPartyBeansToUpdate.add(stagingPartyBean);

					// Create Core Party Identifiers
					CorePartyIdentifierBean corePartyIdentifierBean = createPartyIdentifier(
							stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId(),
							corePartyBean.getId());
					createCorePartyIdentifierBeans.add(corePartyIdentifierBean);
				}
			}
		}
		stagingPartyBeansChunk.clear();
		createCorePartyIdentifier(createCorePartyIdentifierBeans, session, dataspace);
		updateStagingParties(stagingPartyBeansToUpdate, session, dataspace);
	}

	public static void updateStagingParties(List<StagingPartyBean> stagingPartyBeansToUpdate, Session session, String dataspace) throws OperationException {

		AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._STG_Party.getPathInSchema());
		final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		HashMap<Path, Object> values = null;
		RecordValuesBean rvb = null;
		for (StagingPartyBean stagingPartyBean : stagingPartyBeansToUpdate) {
			values = new HashMap<Path, Object>();
			values.put(PartyPaths._STG_Party._Party_CorePartyId, stagingPartyBean.getCorePartyId());
			rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(stagingPartyBean.getId().toString())), values);
			recordValuesBeanList.add(rvb);
		}

		// For Updates
		UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}

	public static boolean isBulkProcess() {

		com.elipslife.ebx.domain.bean.convert.ReadPartyBulkProcesserProperty restBulkProcesserProperty = com.elipslife.ebx.domain.bean.convert.ReadPartyBulkProcesserProperty
				.getInstance();
		// Check If it is Full load or Incremental load from Property file
		if (restBulkProcesserProperty.getPartyBulkProcessProperty()
				.equals(PartyConstants.Entities.Party.RestBulkProcessState.REST_BULKPROCESS_STATE_FULL)) {
			return true;
		}
		return false;
	}

	private static boolean dateWithinRange(Date dateFrom, Date dateTo) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		Date dateNow = calendar.getTime();

		Date dateMinimum = new Date(Long.MIN_VALUE);
		Date dateMaximum = new Date(Long.MAX_VALUE);

		return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0
				&& dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
	}

	/**
	 * 
	 * @param stagingPartyBean
	 * @return
	 */
	private static boolean hasValidationErrors(StagingPartyBean stagingPartyBean, AdaptationTable stagingPartyTable) {

		StagingPartyReader stagingPartyReader = new StagingPartyReader(stagingPartyTable);
		boolean hasValidationErrors = stagingPartyReader.hasValidationErrors(new Object[] { stagingPartyBean.getId() });
		return hasValidationErrors;
	}
	
	public static StagingAddressBean hasLegalAddressFoundForStagingParty(String sourceSystem, String sourcePartyBusId, String dataSpace, String postCode) {
		
		StagingAddressReader stagingAddressReader = null;
		if(isFullLoad) {
			stagingAddressReader = new StagingAddressReader(stagingDataSet.getTable(PartyPaths._STG_Address.getPathInSchema()));
		} else {
			stagingAddressReader = new StagingAddressReader();
		}
		List<StagingAddressBean> stagingAddressBeans = stagingAddressReader.findBeansByParty(sourceSystem, sourcePartyBusId);
		for(StagingAddressBean stagingAddressBean : stagingAddressBeans)
		{
			if(StringUtils.equals(stagingAddressBean.getAddressUsage(), PartyConstants.Entities.Party.AddressType.ADDRESS_USE_ONE) && 
					StringUtils.equals(stagingAddressBean.getAddressType(), PartyConstants.Entities.Party.AddressType.POSTAL_ADDRESS) && stagingAddressBean.getPostCode() != null) {
				return stagingAddressBean;
			}
		}
		return null;
	}
	
	@SuppressWarnings("null")
	private static void getBatachResults(List<StagingPartyBean> stagingPartiesInCurrentBatchList) {
		partyTypesMap.clear();
		Map<String, List<StagingPartyBean>> individualMap = null;
		Map<String, List<StagingPartyBean>> organisationMap = null;
		Map<String, List<StagingPartyBean>> companyMap = null;
		for(StagingPartyBean stagingPartyBean : stagingPartiesInCurrentBatchList) {
			if(Objects.equals(PartyConstants.Entities.Party.PartyType.INDIVIDUAL, stagingPartyBean.getPartyType())) {
				individualMap = partyTypesMap.get(stagingPartyBean.getPartyType());
				if(individualMap == null) {
					individualMap = new HashMap<String, List<StagingPartyBean>>();
					List<StagingPartyBean> stagingInvidualsList = new ArrayList<StagingPartyBean>();
					stagingInvidualsList.add(stagingPartyBean);
					individualMap.put(stagingPartyBean.getFirstName()+stagingPartyBean.getLastName(), stagingInvidualsList);
				} else {
					List<StagingPartyBean> stagingInvidualsList = individualMap.get(stagingPartyBean.getFirstName()+stagingPartyBean.getLastName());
					if(stagingInvidualsList == null) {
						stagingInvidualsList = new ArrayList<StagingPartyBean>();
					}
					stagingInvidualsList.add(stagingPartyBean);
					individualMap.put(stagingPartyBean.getFirstName()+stagingPartyBean.getLastName(), stagingInvidualsList);
				}
				partyTypesMap.put(PartyConstants.Entities.Party.PartyType.INDIVIDUAL, individualMap);
			}
			if(Objects.equals(PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT,stagingPartyBean.getPartyType())) {
				organisationMap = partyTypesMap.get(stagingPartyBean.getPartyType());
				if(organisationMap == null) {
					organisationMap = new HashMap<String, List<StagingPartyBean>>();
					List<StagingPartyBean> stagingOrgList = new ArrayList<StagingPartyBean>();
					stagingOrgList.add(stagingPartyBean);
					organisationMap.put(stagingPartyBean.getOrgName()+stagingPartyBean.getSourceParentBusId(), stagingOrgList);
				} else {
					List<StagingPartyBean> stagingOrgList = organisationMap.get(stagingPartyBean.getOrgName()+stagingPartyBean.getSourceParentBusId());
					if(stagingOrgList == null) {
						stagingOrgList = new ArrayList<StagingPartyBean>();
					}
					stagingOrgList.add(stagingPartyBean);
					organisationMap.put(stagingPartyBean.getOrgName()+stagingPartyBean.getSourceParentBusId(), stagingOrgList);
				}
				partyTypesMap.put(PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT, organisationMap);
			}
			if(Objects.equals(PartyConstants.Entities.Party.PartyType.COMPANY,stagingPartyBean.getPartyType())) {
				companyMap = partyTypesMap.get(stagingPartyBean.getPartyType());
				if(companyMap == null) {
					companyMap = new HashMap<String, List<StagingPartyBean>>();
					List<StagingPartyBean> stagingCompanyList = new ArrayList<StagingPartyBean>();
					stagingCompanyList.add(stagingPartyBean);
					companyMap.put(stagingPartyBean.getOrgName(), stagingCompanyList);
				} else {
					List<StagingPartyBean> stagingCompanyList = companyMap.get(stagingPartyBean.getOrgName());
					if(stagingCompanyList == null) {
						stagingCompanyList = new ArrayList<StagingPartyBean>();
					}
					stagingCompanyList.add(stagingPartyBean);
					companyMap.put(stagingPartyBean.getOrgName(), stagingCompanyList);
				}
				partyTypesMap.put(PartyConstants.Entities.Party.PartyType.COMPANY, companyMap);
			}
		}
	}
	
	public AdaptationHome getStagingDataSpaceHome() {
		return stagingDataSpaceHome;
	}

	public void setStagingDataSpaceHome(AdaptationHome stagingDataSpaceHome) {
		CorePartyBuilder.stagingDataSpaceHome = stagingDataSpaceHome;
	}

	public Adaptation getStagingDataSet() {
		return stagingDataSet;
	}

	public void setStagingDataSet(Adaptation stagingDataset) {
		CorePartyBuilder.stagingDataSet = stagingDataset;
	}
}
