package com.elipslife.mdm.contract.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.butos.ebx.procedure.CreateRecords;
import com.butos.ebx.procedure.DeleteRecords;
import com.butos.ebx.procedure.RecordValuesBean;
import com.elipslife.ebx.domain.bean.convert.ContractConverter;
import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.contract.landing.LandingExposureBean;
import com.elipslife.mdm.contract.landing.LandingExposureReader;
import com.elipslife.mdm.contract.landing.LandingExposureWriter;
import com.elipslife.mdm.contract.landing.LandingExposureWriterSession;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractBean;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractReader;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractWriter;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractWriterSession;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class ContractMigrateExposureLandingToStagingScriptTask extends ScriptTask {

	private static int COMMITSIZE = 1;
    private static int recordCount = 0;
    
	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException {
	    
	    Map<String, String> dataContextVariables = ContractConverter.getContextVariables(context);
        String commitSize = dataContextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);
        String errorMessage = dataContextVariables.get("errorMessage");
        StringBuilder stringBuilderErrorMessage = new StringBuilder(errorMessage == null ? "" : errorMessage);
        String dataSpace = dataContextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
        String landingDataspace = dataContextVariables.get(PartyConstants.DataContextVariables.TEMP_LANDING_DATASPACE);
		if (commitSize != null) {
			COMMITSIZE = Integer.parseInt(commitSize);
		}
        LandingExposureWriter landingExposureWriter = new LandingExposureWriterSession(context.getSession());
		LandingExposureReader landingExposureReader = (LandingExposureReader) landingExposureWriter.getTableReader();
		List<LandingExposureBean> landingExposureBean = landingExposureReader.readAllBeans();
		List<LandingExposureBean> landingExposureBeansToDelete = new ArrayList<LandingExposureBean>();
		AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(dataSpace);
		List<RecordValuesBean> stagingExposureListToCreate = new ArrayList<RecordValuesBean>();
		landingExposureBean.forEach(leb -> {
			try {
				
				boolean hasValidationErrors = landingExposureReader.hasValidationErrors(new Object[] { leb.getId() });
				boolean isParentExistsInLanding = findLandingInsuranceContractByInsuranceContractId(context.getSession(), leb);
				if(hasValidationErrors || !isParentExistsInLanding) {
					return;
				}
				
				recordCount++;
                HashMap<Path, Object> stagingExposureValues = ContractConverter.fillStagingExposureWithLandingExposureBean(leb);
                RecordValuesBean recordValuesBean = new RecordValuesBean(stagingExposureValues);
                stagingExposureListToCreate.add(recordValuesBean);
                
                if (recordCount > COMMITSIZE) { // create staging records if the records count reaches batch size
                    createStagingExposures(dataspaceName, context.getSession(), stagingExposureListToCreate);
                    stagingExposureListToCreate.clear();
                    recordCount = 0;
                }
                landingExposureBeansToDelete.add(leb);
			}
			catch(Exception ex) {
				stringBuilderErrorMessage.append(String.format("Migration error for landing exposure with ID %s:\r\n%s", leb.getId(), ex.toString()));
			}
		});
		
		if(!stagingExposureListToCreate.isEmpty()) {  // create staging records out of batch size
		    createStagingExposures(dataspaceName, context.getSession(), stagingExposureListToCreate);
            stagingExposureListToCreate.clear();
            recordCount = 0;
        }
		
		// Delete Landing  Exposure Table records
		deleteLandingExposureRecords(landingDataspace, context.getSession(), landingExposureBeansToDelete);
				
		context.setVariableString("errorMessage", stringBuilderErrorMessage.toString());
	}
	
	/**
	 * Find Landing Insurance Contracts by id
	 * 
	 * @param session
	 * @param landingExposureBean
	 * @return
	 */
	public static boolean findLandingInsuranceContractByInsuranceContractId(Session session, LandingExposureBean landingExposureBean) {
		
		LandingInsuranceContractWriter insuranceContractWriter = new LandingInsuranceContractWriterSession(session);
		LandingInsuranceContractReader insuranceContractReader = (LandingInsuranceContractReader) insuranceContractWriter.getTableReader();
		List<LandingInsuranceContractBean> landingInsuranceContractBeans = insuranceContractReader.findBeansByInsuranceContractId(landingExposureBean.getInsuranceContractId());
		if(landingInsuranceContractBeans.size() > 0) {
			return true;
		}
		
		return false;
	}
	
	/**
     * Create staging exposures
     * 
     * @param dataspace
     * @param session
     * @param stagingInsuranceContractsList
     * @throws OperationException
     */
    
    private static void createStagingExposures(AdaptationHome dataspace, Session session, List<RecordValuesBean> stagingExposuresList) throws OperationException {
        
        Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
        AdaptationTable aTable = dataset.getTable(ContractPaths._STG_Exposure.getPathInSchema());

        final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
        
        CreateRecords createRecordsProcedure = new CreateRecords(aTable, stagingExposuresList);
        ProcedureResult result = service.execute(createRecordsProcedure);
        if (result.hasFailed()) {
            final OperationException exception = result.getException();
            throw exception;
        }
    }
    
    /**
	 * Delete Landing Exposure tables records based on commit size
	 * 
	 * @param dataspace
	 * @param session
	 * @param landingInsuranceContractBeans
	 * @throws OperationException
	 */
	private static void deleteLandingExposureRecords(String dataspace, Session session,
			List<LandingExposureBean> landingExposureBeans) throws OperationException {

		int deleteRecordCount = 0;
		AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(dataspace);
		Adaptation dataset = dataspaceName.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.LANDING_MASTER));
		AdaptationTable aTable = dataset.getTable(LandingPaths._LDG_Exposure.getPathInSchema());
		ArrayList<Adaptation> recordValuesBeanList = new ArrayList<Adaptation>();
		for (LandingExposureBean landingExposureBean : landingExposureBeans) {
			deleteRecordCount++;
			recordValuesBeanList.add(aTable.lookupAdaptationByPrimaryKey(
					PrimaryKey.parseString(landingExposureBean.getId().toString())));
			if (deleteRecordCount > 1000) {
				deleteRecords(dataspaceName, session, recordValuesBeanList);
				recordValuesBeanList.clear();
				deleteRecordCount = 0;
			}
		}

		if (!recordValuesBeanList.isEmpty()) {
			deleteRecords(dataspaceName, session, recordValuesBeanList);
			recordValuesBeanList.clear();
			deleteRecordCount = 0;
		}
	}

	/**
	 * Delete table records
	 * 
	 * @param dataspace
	 * @param session
	 * @param recordValuesBeanList
	 * @throws OperationException
	 */
	private static void deleteRecords(AdaptationHome dataspace, Session session,
			ArrayList<Adaptation> recordValuesBeanList) throws OperationException {

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
		DeleteRecords deleteRecordsProcedure = new DeleteRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(deleteRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}
}