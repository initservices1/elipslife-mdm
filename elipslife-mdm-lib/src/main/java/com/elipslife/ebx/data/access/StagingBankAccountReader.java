package com.elipslife.ebx.data.access;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.StagingBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class StagingBankAccountReader extends BaseBankAccountReader<StagingBankAccountBean> {

	public StagingBankAccountReader() {
		super(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
				PartyPaths._STG_BankAccount.getPathInSchema()));
	}
	
	public StagingBankAccountReader(String dataSpace) {
        super(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.PARTY_MASTER),
                PartyPaths._STG_BankAccount.getPathInSchema()));
    }
	
	public StagingBankAccountReader(AdaptationTable table) {
		super(table);
	}
	

	@Override
	public StagingBankAccountBean createBeanFromRecord(Adaptation record) {
		
		StagingBankAccountBean stagingBankAccountBean = new StagingBankAccountBean();
		
		stagingBankAccountBean.setId(record.get_int(PartyPaths._STG_BankAccount._Account_BankAccountId));
		
		stagingBankAccountBean.setAccountType(record.getString(PartyPaths._STG_BankAccount._Account_AccountType));
		stagingBankAccountBean.setAccountNumber(record.getString(PartyPaths._STG_BankAccount._Account_AccountNumber));
		stagingBankAccountBean.setIban(record.getString(PartyPaths._STG_BankAccount._Account_IBAN));
		stagingBankAccountBean.setBic(record.getString(PartyPaths._STG_BankAccount._Account_BIC));
		stagingBankAccountBean.setClearingNumber(record.getString(PartyPaths._STG_BankAccount._Account_ClearingNumber));
		stagingBankAccountBean.setBankName(record.getString(PartyPaths._STG_BankAccount._Account_BankName));
		stagingBankAccountBean.setCountry(record.getString(PartyPaths._STG_BankAccount._Account_Country));
		stagingBankAccountBean.setCurrency(record.getString(PartyPaths._STG_BankAccount._Account_Currency));
		stagingBankAccountBean.setIsMainAccount((Boolean)record.get(PartyPaths._STG_BankAccount._Account_IsMainAccount));
		stagingBankAccountBean.setCorrelationId(record.getString(PartyPaths._STG_BankAccount._Account_CorrelationId));
		stagingBankAccountBean.setSourceBankAccountBusId(record.getString(PartyPaths._STG_BankAccount._Account_SourceBankAccountBusId));
		stagingBankAccountBean.setCoreBankAccountId(record.getString(PartyPaths._STG_BankAccount._Account_CoreBankAccountId));
		stagingBankAccountBean.setSourceSystem(record.getString(PartyPaths._STG_BankAccount._Control_SourceSystem));
		stagingBankAccountBean.setSourcePartyBusId(record.getString(PartyPaths._STG_BankAccount._Control_SourcePartyBusId));
		stagingBankAccountBean.setValidFrom(record.getDate(PartyPaths._STG_BankAccount._Control_ValidFrom));
		stagingBankAccountBean.setValidTo(record.getDate(PartyPaths._STG_BankAccount._Control_ValidTo));
		stagingBankAccountBean.setCreationTimestamp(record.getDate(PartyPaths._STG_BankAccount._Control_CreationTimestamp));
		stagingBankAccountBean.setLastUpdateTimestamp(record.getDate(PartyPaths._STG_BankAccount._Control_LastUpdateTimestamp));
		stagingBankAccountBean.setAction(record.getString(PartyPaths._STG_BankAccount._Control_Action));
		stagingBankAccountBean.setActionBy(record.getString(PartyPaths._STG_BankAccount._Control_ActionBy));
		
		return stagingBankAccountBean;
	}

	
	@Override
	protected Path[] getPkPaths() {
		
		return new Path[] { PartyPaths._STG_BankAccount._Account_BankAccountId };
	}

	
	@Override
	protected Object[] retrievePks(StagingBankAccountBean bean) {

		return new Object[] { bean.getId() };
	}
	
	
	/**
	 * 
	 * @param coreAddressId
	 * @return
	 */
	public List<StagingBankAccountBean> findBeansByCoreBankAccountId(String coreBankAccountId) {
		
		XPathPredicate predicate1 =
				(coreBankAccountId == null || coreBankAccountId.isEmpty())
				? new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._STG_BankAccount._Account_CoreBankAccountId, coreBankAccountId)
				: new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._STG_BankAccount._Account_CoreBankAccountId, coreBankAccountId);
		
		return readRecordsFor(predicate1.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
    /**
     * Get staging Addresses which are created after the mentioned lastSyncTimestamp
     * 
     * @param lastSyncTimestamp
     * @return
     */
    public List<StagingBankAccountBean> getStagingBankAccountBeansByLastSyncTimeStamp(Date lastSyncTimestamp) {

        String dateNowString = DateHelper.convertToXpathFormat(lastSyncTimestamp);

        XPathPredicate predicate1 =
            new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, PartyPaths._STG_BankAccount._Control_LastSyncTimestamp, dateNowString);
        XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
        group.addPredicate(predicate1);

        return readRecordsFor(group.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }

	
	@Override
	protected Path getPathBankAccountId() {

		return PartyPaths._STG_BankAccount._Account_BankAccountId;
	}

	
	@Override
	protected Path getPathSourceBankAccountBusId() {

		return PartyPaths._STG_BankAccount._Account_SourceBankAccountBusId;
	}

	
	@Override
	protected Path getPathControlSourceSystem() {
		
		return PartyPaths._STG_BankAccount._Control_SourceSystem;
	}
	

	@Override
	protected Path getPathControlSourcePartyBusId() {

		return PartyPaths._STG_BankAccount._Control_SourcePartyBusId;
	}

	
	@Override
	protected Path getPathControlValidFrom() {

		return PartyPaths._STG_BankAccount._Control_ValidFrom;
	}

	
	@Override
	protected Path getPathControlValidTo() {

		return PartyPaths._STG_BankAccount._Control_ValidTo;
	}
}