package com.elipslife.mdm.contract.landing;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class LandingContractCoverWriterProcedureContext extends LandingContractCoverWriter {

	private ProcedureContext procedureContext;
	
	public LandingContractCoverWriterProcedureContext(ProcedureContext procedureContext) {
		this.procedureContext = procedureContext;
	}
	
	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}