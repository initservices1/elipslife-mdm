package com.elipslife.ebx.api.rest.models;

import java.util.Collection;

import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Exposure {
	
	@JsonbProperty("ExposureBusId")
	@JsonProperty("ExposureBusId")
	public String exposureBusId;
	
	@JsonbProperty("Name")
	@JsonProperty("Name")
	public String name;
	
	@JsonbProperty("Product")
	@JsonProperty("Product")
	public Product product;
	
	@JsonbProperty("Covers")
	@JsonProperty("Covers")
	public Collection<Cover> covers;
}