package com.elipslife.mdm.hr.core;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class OrganisationWriter extends AbstractTableWriter<OrganisationBean> {
    
	private OrganisationReader reader;

    public OrganisationWriter() {
    	super(RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._Organisation.getPathInSchema()));

		this.reader = new OrganisationReader();
    }
    
    public OrganisationWriter(String dataspace) {
    	super(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._Organisation.getPathInSchema()));

		this.reader = new OrganisationReader(dataspace);
    }

    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, OrganisationBean bean) {
    	values.put(HrModelPaths._Organisation._Organisation_OrganisationId, bean.getOrganisationId());
		values.put(HrModelPaths._Organisation._Organisation_DataOwner, bean.getDataOwner());
		values.put(HrModelPaths._Organisation._Organisation_OrganisationBusId, bean.getOrganisationBusId());
		values.put(HrModelPaths._Organisation._Organisation_OrganisationType, bean.getOrganisationType());
		values.put(HrModelPaths._Organisation._Organisation_ParentId, bean.getParentId());
		values.put(HrModelPaths._Organisation._Organisation_ParentBusId, bean.getParentBusId());
		values.put(HrModelPaths._Organisation._Organisation_Name, bean.getName());
		values.put(HrModelPaths._Organisation._Organisation_ManagerId, bean.getManagerId());
		values.put(HrModelPaths._Organisation._Organisation_ManagerBusId, bean.getManagerBusId());
		values.put(HrModelPaths._Organisation._Organisation_BuildingId, bean.getBuildingId());
		values.put(HrModelPaths._Organisation._Organisation_BuildingBusId, bean.getBuildingBusId());
		values.put(HrModelPaths._Organisation._Organisation_IsActive, bean.getIsActive());
		values.put(HrModelPaths._Organisation._Control_SourceSystem, bean.getSourceSystem());
		values.put(HrModelPaths._Organisation._Control_CreatedBy, bean.getCreatedBy());
		values.put(HrModelPaths._Organisation._Control_CreationTimestamp, bean.getCreationTimeStamp());
		values.put(HrModelPaths._Organisation._Control_LastUpdatedBy, bean.getLastUpdatedBy());
		values.put(HrModelPaths._Organisation._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
        
    }

    @Override
    public AbstractTableReader<OrganisationBean> getTableReader() {
        return reader;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub
		
	}
}
