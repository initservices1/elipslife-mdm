package com.elipslife.mdm.hr.constraint;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

import ch.butos.ebx.lib.validation.SmartRecordAndTableLevelValidationCheck;

public class EmployeeConstraint extends SmartRecordAndTableLevelValidationCheck {

	@Override
	protected void executeBusinessValidations(ValueContext recordContext,
			Map<SchemaNode, Set<UserMessage>> validationMessages, boolean firstRecord) {
		
		String employeerBusId = (String)recordContext.getValue(HrModelPaths._Employee._EmployerCompany_EmployerBusId);
		String employeerId = (String)recordContext.getValue(HrModelPaths._Employee._EmployerCompany_EmployerId);
		String organisationBusId = (String)recordContext.getValue(HrModelPaths._Employee._JobPosition_OrganisationBusId);
		String organisationId = (String)recordContext.getValue(HrModelPaths._Employee._JobPosition_OrganisationId);
		String buildingBusId = (String)recordContext.getValue(HrModelPaths._Employee._WorkplaceLocation_BuildingBusId);
		String buildingId = (String)recordContext.getValue(HrModelPaths._Employee._WorkplaceLocation_BuildingId);
		String lineManagerBusId = (String)recordContext.getValue(HrModelPaths._Employee._JobPosition_LineManagerBusId);
		String lineManagerId = (String)recordContext.getValue(HrModelPaths._Employee._JobPosition_LineManagerId);
		String dottedLineManagerBusId = (String)recordContext.getValue(HrModelPaths._Employee._JobPosition_DottedLineManagerBusId);
		String dottedLineManagerId = (String)recordContext.getValue(HrModelPaths._Employee._JobPosition_DottedLineManagerId);
		
		UserMessage userMsg = null;
		
		if(employeerBusId != null && employeerId == null) {
			if(!employeerBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Employeer Bus Id '" + employeerBusId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(HrModelPaths._Employee._EmployerCompany_EmployerId), userMsg);
			}
		}
		
		if(organisationBusId != null && organisationId == null) {
			if(!organisationBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Organisation Bus Id '" + organisationBusId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(HrModelPaths._Employee._JobPosition_OrganisationId), userMsg);
			}
		}
		
		if(buildingBusId != null && buildingId == null) {
			if(!buildingBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Building Bus Id '" + buildingBusId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(HrModelPaths._Employee._WorkplaceLocation_BuildingId), userMsg);
			}
		}
		
		if(lineManagerBusId != null && lineManagerId == null) {
			if(!lineManagerBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Line Manager Bus Id '" + lineManagerBusId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(HrModelPaths._Employee._JobPosition_LineManagerId), userMsg);
			}
		}
		
		if(dottedLineManagerBusId != null && dottedLineManagerId == null) {
			if(!dottedLineManagerBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Dotted Line Manager Bus Id '" + dottedLineManagerBusId + "' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(HrModelPaths._Employee._JobPosition_DottedLineManagerId), userMsg);
			}
		}
		
	}
	
	@Override
	public void setup(ConstraintContextOnTable aContext) {
		
		
	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		
		return null;
	}

}
