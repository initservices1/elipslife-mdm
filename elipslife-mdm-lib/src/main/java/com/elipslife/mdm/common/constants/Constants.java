package com.elipslife.mdm.common.constants;

public class Constants {
	
	public static class DataSpace {
		public static final String HR_REFERENCE = "HrReference";
		public static final String LANDING_HR_REFERENCE = "LandingHr";
		public static final String LANDING_UW_REFERENCE = "LandingUW";
		public static final String UW_REFERENCE = "UWReference";
	}
	
	public static final class DataSet {
		public static final String HR_MASTER = "HRMaster";
		public static final String LANDING_HR_MASTER = "LandingHr";
		public static final String LANDING_UW_MASTER = "LandingUW";
		public static final String UW_MASTER = "UWMaster";
	}

	public static class RecordOperations {
		public static final String CREATE = "C";
		public static final String UPDATE = "U";
		public static final String DELETE = "D";
	}
	
	public static class SourceSystems {
		public static final String CRM = "CRM";
	}
	
}
