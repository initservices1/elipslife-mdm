package com.elipslife.mdm.party.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.butos.ebx.procedure.CreateRecords;
import com.butos.ebx.procedure.DeleteRecords;
import com.butos.ebx.procedure.RecordValuesBean;
import com.elipslife.ebx.data.access.LandingAddressBulkProcessReader;
import com.elipslife.ebx.data.access.LandingAddressBulkProcessWriter;
import com.elipslife.ebx.data.access.LandingAddressBulkProcessWriterSession;
import com.elipslife.ebx.data.access.LandingAddressReader;
import com.elipslife.ebx.data.access.LandingAddressWriter;
import com.elipslife.ebx.data.access.LandingAddressWriterSession;
import com.elipslife.ebx.data.access.LandingPartyBulkProcessReader;
import com.elipslife.ebx.data.access.StagingAddressReader;
import com.elipslife.ebx.data.access.StagingAddressWriter;
import com.elipslife.ebx.data.access.StagingAddressWriterSession;
import com.elipslife.ebx.data.access.StagingPartyReader;
import com.elipslife.ebx.data.access.changeset.ChangeSetBuilder;
import com.elipslife.ebx.domain.bean.LandingAddressBean;
import com.elipslife.ebx.domain.bean.LandingAddressBulkProcessBean;
import com.elipslife.ebx.domain.bean.LandingPartyBulkProcessBean;
import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.ebx.domain.bean.convert.ReadPartyBulkProcesserProperty;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.elipslife.mdm.path.PartyPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

public class CopyAddressesFromLandingToStaging extends ScriptTask {

	private Integer recordCount = 0;
	private static AdaptationHome stagingDataSpaceHome;
	private static AdaptationHome templandingDataspace;
	private static Adaptation stagingDataSet;
	private static Adaptation landingDataSet;

	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException {

		HashMap<String, String> contextVariables = getContextVariables(context);
		String dataSpace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_LANDING_DATASPACE);
		String stagingDataspace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		String commitSize = contextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);

		Repository reposity = context.getRepository();
		setStagingDataSpaceHome(reposity.lookupHome(HomeKey.forBranchName(stagingDataspace)));
		setTemplandingDataspace(reposity.lookupHome(HomeKey.forBranchName(dataSpace)));
		setStagingDataSet(getStagingDataSpaceHome());
		setLandingDataSet(getTemplandingDataspace());

		// Date lastSyncTimestamp = getTemplandingDataspace().getCreationDate();
		StringBuilder stringBuilderErrorMessage = new StringBuilder();
		ReadPartyBulkProcesserProperty bulkProcesserProperty = ReadPartyBulkProcesserProperty.getInstance();

		// Check If it is a Bulk load or Incremental load
		if (bulkProcesserProperty.getPartyBulkProcessProperty()
				.equals(PartyConstants.Entities.Party.RestBulkProcessState.REST_BULKPROCESS_STATE_FULL)) {

			recordCount = 0;
			// Create landing address reader to read from child data space
			LandingAddressBulkProcessReader landingAddressBulkProcessReader = new LandingAddressBulkProcessReader(dataSpace);
			List<LandingAddressBulkProcessBean> landingAddressBulkProcessBeans = landingAddressBulkProcessReader.readAllBeans();
			
			// Create landing address writer to write into main data space
			LandingAddressBulkProcessWriter landingAddressWriter = new LandingAddressBulkProcessWriterSession(context.getSession(), true, dataSpace);
			List<LandingAddressBulkProcessBean> landingBulkProcessBeansToDelete = new ArrayList<LandingAddressBulkProcessBean>();
			
			// Create staging party reader to read from main data space
			StagingPartyReader stagingPartyReader = new StagingPartyReader(stagingDataspace);

			// Create staging address writer to write into main data space
			// StagingAddressWriter stagingAddressWriter = new StagingAddressWriterSession(context.getSession(), stagingDataspace);
			// StagingAddressReader stagingAddressReader = (StagingAddressReader) stagingAddressWriter.getTableReader();
			
			List<RecordValuesBean> stagingBeansToCreate = new ArrayList<RecordValuesBean>();
			List<StagingAddressBean> stagingBeansToUpdate = new ArrayList<StagingAddressBean>();
			List<RecordValuesBean> stagingBeanToDelete = new ArrayList<RecordValuesBean>();
			
			for (LandingAddressBulkProcessBean addressBulkProcessBean : landingAddressBulkProcessBeans) {
				
				boolean landingAddressBeanInvalid = landingAddressBulkProcessReader.hasValidationErrors(new Object[] { addressBulkProcessBean.getId() });
				if (landingAddressBeanInvalid) {
					continue;
				}
				String landingPartyBulkProcessId = addressBulkProcessBean.getLandingPartyId();
				boolean parentPartyHasNoErrors = findParentPartyHasValidationErrorsOrNot(landingPartyBulkProcessId, dataSpace);
				if (!parentPartyHasNoErrors) {
					landingBulkProcessBeansToDelete.add(addressBulkProcessBean);
					recordCount++;
					String sourceSystem = addressBulkProcessBean.getSourceSystem();
					String sourcePartyBusId = addressBulkProcessBean.getSourcePartyBusId();
					// Check for available related staging party
					List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId);
					
					// If none are available, write the address into landing
					if (stagingPartyBeans.isEmpty()) {
						try {
							landingAddressWriter.createBeanOrThrow(addressBulkProcessBean);
						} catch (Exception ex) {
							stringBuilderErrorMessage.append(String.format("Error while creating staging address bean with source system \"%s\" and source address bus id \"%s\"\r\n", addressBulkProcessBean.getSourceSystem(), addressBulkProcessBean.getSourceAddressBusId()));
							stringBuilderErrorMessage.append(ex.toString() + "\r\n");
						}
					}
					StagingAddressBean stagingAddressBean = new StagingAddressBean();
					com.elipslife.ebx.domain.bean.convert.AddressConverter.fillStagingAddressBeanWithLandingAddressBulkProcessBean(addressBulkProcessBean, stagingAddressBean);
					try {
						HashMap<Path, Object> values = StagingAddressConverter.fillStagingAddressValuesFromLandingAddressBulkProcessBean(stagingAddressBean);
						RecordValuesBean rvb = new RecordValuesBean(values);
						stagingBeansToCreate.add(rvb);
						recordCount++;
						
					} catch (Exception ex) {
						stringBuilderErrorMessage.append(String.format("Error while creating staging address bean with source system \"%s\" and source address bus id \"%s\"\r\n", stagingAddressBean.getSourceSystem(), stagingAddressBean.getSourceAddressBusId()));
						stringBuilderErrorMessage.append(ex.toString() + "\r\n");
					}

					if (recordCount > Integer.parseInt(commitSize)) {
						try {
							createStagingAddresses(stagingBeansToCreate, context.getSession(), stagingDataspace);
							stagingBeansToCreate.clear();
							stagingBeansToUpdate.clear();
							stagingBeanToDelete.clear();
							recordCount = 1;
						} catch (OperationException e) {
							e.printStackTrace();
						}
					}
				}				
			}
			
			if (!stagingBeansToCreate.isEmpty() || !stagingBeansToUpdate.isEmpty() || !stagingBeanToDelete.isEmpty()) {
                try {
                    createStagingAddresses(stagingBeansToCreate, context.getSession(), stagingDataspace);
                    stagingBeansToCreate.clear();
                    stagingBeansToUpdate.clear();
                    stagingBeanToDelete.clear();
                    recordCount = 1;
                } catch (OperationException e) {
                    e.printStackTrace();
                }
            }
			
			// delete Landing Address Records
			deleteLandingRecords(landingBulkProcessBeansToDelete, context.getSession(), dataSpace);
			
		} else {

			// Create landing address reader to read from child data space
			LandingAddressReader landingAddressReader = new LandingAddressReader(dataSpace);
			List<LandingAddressBean> landingAddressBeans = landingAddressReader.readAllBeans();

			// Create landing address writer to write into main data space
			LandingAddressWriter landingAddressWriter = new LandingAddressWriterSession(context.getSession(), true);

			// Create staging party reader to read from main data space
			StagingPartyReader stagingPartyReader = new StagingPartyReader();

			// Create staging address writer to write into main data space
			StagingAddressWriter stagingAddressWriter = new StagingAddressWriterSession(context.getSession());
			StagingAddressReader stagingAddressReader = (StagingAddressReader) stagingAddressWriter.getTableReader();

			// Group landing addresses by SourceSystem and SourcePartyBusId
			Map<String, List<LandingAddressBean>> landingAddressBeansGroupedBySourceSystem = landingAddressBeans.stream().collect(Collectors.groupingBy(lab -> lab.getSourceSystem() + lab.getSourcePartyBusId()));
			landingAddressBeansGroupedBySourceSystem.forEach((key, landingAddressBeansFromGroup) -> {

				String sourceSystem = landingAddressBeansFromGroup.get(0).getSourceSystem();
				String sourcePartyBusId = landingAddressBeansFromGroup.get(0).getSourcePartyBusId();

				// Check for available related staging party
				List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId);

				// If none are available, write the address into landing
				if (stagingPartyBeans.isEmpty()) {
					landingAddressBeansFromGroup.forEach(lab -> {
						try {
							landingAddressWriter.createBeanOrThrow(lab);
						} catch (Exception ex) {
							stringBuilderErrorMessage.append(String.format("Error while creating staging address bean with source system \"%s\" and source address bus id \"%s\"\r\n", lab.getSourceSystem(), lab.getSourceAddressBusId()));
							stringBuilderErrorMessage.append(ex.toString() + "\r\n");
						}
					});
					return;
				}

				// Create, update or delete staging addresses
				List<StagingAddressBean> stagingAddressBeans = stagingAddressReader.findBeansByParty(sourceSystem, sourcePartyBusId);
				HashMap<Character, List<StagingAddressBean>> changeSetAddresses = ChangeSetBuilder.buildStagingAddressBeansChangeSet(landingAddressBeansFromGroup, stagingAddressBeans);
				changeSetAddresses.forEach((operation, stagingAddressBeansList) -> {
					if (operation == PartyConstants.RecordOperations.CREATE.charAt(0)) {
						stagingAddressBeansList.stream().forEach(sab -> {
							try {
								stagingAddressWriter.createBeanOrThrow(sab);
							} catch (Exception ex) {
								stringBuilderErrorMessage.append(String.format("Error while creating staging address bean with source system \"%s\" and source address bus id \"%s\"\r\n", sab.getSourceSystem(), sab.getSourceAddressBusId()));
								stringBuilderErrorMessage.append(ex.toString() + "\r\n");
							}
						});
					} else if (operation == PartyConstants.RecordOperations.UPDATE.charAt(0)) {
						stagingAddressBeansList.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getLastUpdateTimestamp(), b.getLastUpdateTimestamp())).forEach(sab -> {
									try {
										stagingAddressWriter.updateBean(sab);
									} catch (Exception ex) {
										stringBuilderErrorMessage.append(String.format("Error while updating staging address bean with source system \"%s\" and source address bus id \"%s\"\r\n", sab.getSourceSystem(), sab.getSourceAddressBusId()));
										stringBuilderErrorMessage.append(ex.toString() + "\r\n");
									}
								});
					} else if (operation == PartyConstants.RecordOperations.DELETE.charAt(0)) {
						stagingAddressBeansList.stream().forEach(sab -> {
							try {
								stagingAddressWriter.delete(sab);
							} catch (Exception ex) {
								stringBuilderErrorMessage.append(String.format("Error while deleting staging address bean with source system \"%s\" and source address bus id \"%s\"\r\n", sab.getSourceSystem(), sab.getSourceAddressBusId()));
								stringBuilderErrorMessage.append(ex.toString() + "\r\n");
							}
						});
					}
				});
			});
		}
		
		//Update errorMessages in datacontext variables
		updateErrorMessageInDataContext(stringBuilderErrorMessage, context);
		
	}

	/**
	 * 
	 * @param dataContext
	 * @return
	 */
	private static HashMap<String, String> getContextVariables(com.orchestranetworks.workflow.DataContextReadOnly dataContext) {

		HashMap<String, String> hashMap = new HashMap<String, String>();
		Iterator<String> variableNames = dataContext.getVariableNames();
		while (variableNames.hasNext()) {
			String variableName = variableNames.next();
			String variableValue = dataContext.getVariableString(variableName);
			hashMap.put(variableName, variableValue);
		}
		return hashMap;
	}

	private static void createStagingAddresses(List<RecordValuesBean> stagingAddressesListToCreate, Session session, String stagingDataspace) throws OperationException {


		AdaptationTable aTable = getStagingDataSet().getTable(PartyPaths._STG_Address.getPathInSchema());
		final ProgrammaticService service = ProgrammaticService.createForSession(session, getStagingDataSpaceHome());

		CreateRecords createRecordsProcedure = new CreateRecords(aTable, stagingAddressesListToCreate);
		ProcedureResult result = service.execute(createRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}

	private static void deleteLandingRecords(List<LandingAddressBulkProcessBean> landingAddressBulkProcessBeansList, Session session, String dataSpace) throws OperationException {

		AdaptationTable aTable = getLandingDataSet().getTable(LandingPaths._LDG_Address_BulkProcess.getPathInSchema());
		final ProgrammaticService service = ProgrammaticService.createForSession(session, getTemplandingDataspace());

		ArrayList<Adaptation> recordValuesBeanList = new ArrayList<Adaptation>();

		for (LandingAddressBulkProcessBean landingAddressBulkProcessBean : landingAddressBulkProcessBeansList) {
			recordValuesBeanList.add(aTable.lookupAdaptationByPrimaryKey(
					PrimaryKey.parseString(landingAddressBulkProcessBean.getId().toString())));
		}

		DeleteRecords deleteRecordsProcedure = new DeleteRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(deleteRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}

	/**
	 * Check Parent Party has any validation error messages or not
	 * 
	 * @param landingPartyBulkProcessId
	 * @param dataSpace
	 * @return
	 */
	private boolean findParentPartyHasValidationErrorsOrNot(String landingPartyBulkProcessId, String dataSpace) {

		if (landingPartyBulkProcessId == null) {
			return false;
		}
		try {
			if (landingPartyBulkProcessId != null) {
				LandingPartyBulkProcessReader bulkProcessReader = new LandingPartyBulkProcessReader(dataSpace);
				LandingPartyBulkProcessBean bulkProcessBean = bulkProcessReader.findBeanByPartyId(Integer.parseInt(landingPartyBulkProcessId));
				if (bulkProcessBean != null) {
					return true;
				}
			}
		} catch (Exception e) {

		}
		return false;
	}
	
	/**
	 * Trim and Update errorMessages in DataContext variables
	 * 
	 * @param stringBuilderErrorMessage
	 * @param aContext
	 */
	private void updateErrorMessageInDataContext(StringBuilder stringBuilderErrorMessage, ScriptTaskContext aContext) {
		
		if(stringBuilderErrorMessage != null) {
			StringBuffer errorMessage = new StringBuffer();
			String errorMessages = aContext.getVariableString(PartyConstants.DataContextVariables.ERROR_MESSAGES);
			if(errorMessages != null && errorMessages.length() > 2000) {
				String errorMessageString = errorMessages.substring(errorMessages.length() - 2000);
				errorMessage.append(errorMessageString);
				if(stringBuilderErrorMessage.length() > 2000) {
					errorMessage.append(stringBuilderErrorMessage.substring(stringBuilderErrorMessage.length() - 2000));
				} else {
					errorMessage.append(stringBuilderErrorMessage);
				}
				errorMessage.append(" Please look in to kernel.log file to see more errors");
			} else {
				if(errorMessages != null) {
					errorMessage.append(errorMessages);
				}
				if(stringBuilderErrorMessage.length() > 2000) {
					errorMessage.append(stringBuilderErrorMessage.substring(stringBuilderErrorMessage.length() - 2000));
					errorMessage.append(" Please look in to kernel.log file to see more errors");
				} else {
					errorMessage.append(stringBuilderErrorMessage);
				}
			}
			aContext.setVariableString(PartyConstants.DataContextVariables.ERROR_MESSAGES, errorMessage.toString());
		}
	}

	public static AdaptationHome getStagingDataSpaceHome() {
		return stagingDataSpaceHome;
	}

	public void setStagingDataSpaceHome(AdaptationHome stagingDataSpace) {
		stagingDataSpaceHome = stagingDataSpace;
	}

	public static AdaptationHome getTemplandingDataspace() {
		return templandingDataspace;
	}

	public void setTemplandingDataspace(AdaptationHome tempLandingDataspace) {
		templandingDataspace = tempLandingDataspace;
	}

	public static Adaptation getStagingDataSet() {
		return stagingDataSet;
	}

	public void setStagingDataSet(AdaptationHome stagingDataspace) {
		stagingDataSet = stagingDataspace
				.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.PARTY_MASTER));
	}

	public static Adaptation getLandingDataSet() {
		return landingDataSet;
	}

	public void setLandingDataSet(AdaptationHome landingAdaptationHome) {
		landingDataSet = landingAdaptationHome
				.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.LANDING_MASTER));
	}
}
