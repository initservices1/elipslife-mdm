package com.elipslife.mdm.hr.core;

import java.util.List;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;


public class BuildingReader extends AbstractTableReader<BuildingBean> {

    public BuildingReader() {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._Building.getPathInSchema()));
    }
    
    public BuildingReader(String dataspace) {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._Building.getPathInSchema()));
    }

    public BuildingReader(AdaptationTable table) {
        super(table);
    }

    @Override
    public BuildingBean createBeanFromRecord(Adaptation record) {
        BuildingBean bean = new BuildingBean();
        bean.setBuildingId(record.getString(HrModelPaths._Building._Building_BuildingId));
        bean.setDataOwner(record.getString(HrModelPaths._Building._Building_DataOwner));
        bean.setBuildingBusId(record.getString(HrModelPaths._Building._Building_BuildingBusId));
        bean.setName(record.getString(HrModelPaths._Building._Building_Name));
        bean.setStreet(record.getString(HrModelPaths._Building._Building_Street));
        bean.setStreet2(record.getString(HrModelPaths._Building._Building_Street2));
        bean.setPostCode(record.getString(HrModelPaths._Building._Building_PostCode));
        bean.setPOBox(record.getString(HrModelPaths._Building._Building_POBox));
        bean.setPOBoxPostCode(record.getString(HrModelPaths._Building._Building_POBoxPostCode));
        bean.setPOBoxTown(record.getString(HrModelPaths._Building._Building_POBoxTown));
        bean.setTown(record.getString(HrModelPaths._Building._Building_Town));
        bean.setDistrict(record.getString(HrModelPaths._Building._Building_District));
        bean.setStateProvince(record.getString(HrModelPaths._Building._Building_StateProvince));
        bean.setCountry(record.getString(HrModelPaths._Building._Building_Country));
        bean.setIsActive(record.get_boolean(HrModelPaths._Building._Building_IsActive));
        bean.setSourceSystem(record.getString(HrModelPaths._Building._Control_SourceSystem));
        bean.setCreatedBy(record.getString(HrModelPaths._Building._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(HrModelPaths._Building._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(HrModelPaths._Building._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(HrModelPaths._Building._Control_LastUpdateTimestamp));
        
	
		return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{HrModelPaths._Building._Building_BuildingId};
    }

    @Override
    public Object[] retrievePks(BuildingBean bean) {
        return new Object[]{bean.getBuildingId()};
    }
    
    public List<BuildingBean> findBeansByBuildingId(String buildingBusId) {
    	
    	XPathPredicate predicate = new XPathPredicate(Operation.Equals, HrModelPaths._Building._Building_BuildingBusId, buildingBusId);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate);
		
		return super.readBeansFor(group.toString());
	}
    
}
