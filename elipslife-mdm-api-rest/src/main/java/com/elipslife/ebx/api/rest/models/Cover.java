package com.elipslife.ebx.api.rest.models;

import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cover {
	
	@JsonbProperty("CoverBusId")
	@JsonProperty("CoverBusId")
	public String coverBusId;
	
	@JsonbProperty("CoverGroupType")
	@JsonProperty("CoverGroupType")
	public String coverGroupType;
	
	@JsonbProperty("CoverGroupTypeName")
	@JsonProperty("CoverGroupTypeName")
	public String coverGroupTypeName;
	
	@JsonbProperty("CoverName")
	@JsonProperty("CoverName")
	public String coverName;
	
	@JsonbProperty("CoverTemplateBusId")
	@JsonProperty("CoverTemplateBusId")
	public String coverTemplateBusId;
	
	@JsonbProperty("BeneficiaryType")
	@JsonProperty("BeneficiaryType")
	public String beneficiaryType;
}