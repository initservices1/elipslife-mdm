package com.elipslife.mdm.party;

public class PartyFunction {

	private static String PATTERN = "[^a-zA-Z0-9]";
	
	public static String standardizeSearchText(String input)
	{
		String inputUpperCase = input.toUpperCase();
		
		String inputUpperCaseMutatedVowels = 
		inputUpperCase.replace("Ä", "AE").replace("À", "A").replace("Á", "A").replace("Â", "A").replace("Æ", "AE")
		.replace("Ć", "C").replace("Ĉ", "C").replace("Ç", "C")
		.replace("Ë", "EE").replace("É", "E").replace("È", "E").replace("Ê", "E")
		.replace("Ï", "IE").replace("Í", "I").replace("Ì", "I").replace("Î", "I").replace("¡", "I")
		.replace("Ñ", "N")
		.replace("Ö", "OE").replace("Ò", "O").replace("Ó", "O").replace("Ô", "O")
		.replace("Ü", "UE").replace("Ù", "U").replace("Ú", "U").replace("Û", "U")
		.replace("ß", "SS");
		
		String output = inputUpperCaseMutatedVowels.replaceAll(PATTERN, "");

		return output;
	}
}