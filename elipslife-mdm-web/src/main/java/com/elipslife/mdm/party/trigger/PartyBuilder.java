package com.elipslife.mdm.party.trigger;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.data.access.CoreAddressBuilder;
import com.elipslife.ebx.data.access.CoreBankAccountBuilder;
import com.elipslife.ebx.data.access.LandingAddressReader;
import com.elipslife.ebx.data.access.LandingBankAccountReader;
import com.elipslife.ebx.data.access.StagingAddressReader;
import com.elipslife.ebx.data.access.StagingAddressWriter;
import com.elipslife.ebx.data.access.StagingAddressWriterProcedureContext;
import com.elipslife.ebx.data.access.StagingBankAccountReader;
import com.elipslife.ebx.data.access.StagingBankAccountWriter;
import com.elipslife.ebx.data.access.StagingBankAccountWriterProcedureContext;
import com.elipslife.ebx.data.access.changeset.ChangeSetBuilder;
import com.elipslife.ebx.domain.bean.LandingAddressBean;
import com.elipslife.ebx.domain.bean.LandingBankAccountBean;
import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.ebx.domain.bean.StagingBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.boot.VM;
import com.orchestranetworks.service.ProcedureContext;

public class PartyBuilder {

	/**
	 * Create Party addresses from landing to staging
	 * -- Party addresses are always synchronized in staging and core
	 * -- If landing address have any validation errors, It will not move to staging and core
	 * 
	 * @param aSession
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 */
	public static List<LandingAddressBean> createOrUpdateStagingAddressesByLandingAddresses(ProcedureContext procedureContext,
			String sourceSystem, String sourcePartyBusId) {

		StagingAddressWriter stagingAddressWriter = new StagingAddressWriterProcedureContext(procedureContext);
		StagingAddressReader stagingAddressReader = new StagingAddressReader();
		List<StagingAddressBean> stagingAddressBeans = stagingAddressReader.findBeansByParty(sourceSystem, sourcePartyBusId);

		LandingAddressReader landingAddressReader = new LandingAddressReader();
		List<LandingAddressBean> landingAddressBeans = landingAddressReader.findBeansByParty(sourceSystem, sourcePartyBusId);

		// Filter landing address beans by validation errors
		List<LandingAddressBean> landingAddressBeansValid = landingAddressBeans.stream().filter(landingAddressBean -> !landingAddressReader.hasValidationErrors(
								new Object[]{landingAddressBean.getId()})).collect(Collectors.toList());

		if (stagingAddressBeans.isEmpty()) { // if staging parties are empty, migrate landing addresses to staging
			for (LandingAddressBean landingAddressBean : landingAddressBeansValid) {
				VM.log.kernelDebug("[PartyBuilder] Synchronize Addresses: Migrating landing address to staging");
				StagingAddressBean stagingAddressBean = com.elipslife.ebx.domain.bean.convert.AddressConverter.createStagingAddressBeanFromLandingAddressBean(
								landingAddressBean);
				stagingAddressWriter.createBeanOrThrow(stagingAddressBean);
			}
		}

		// if addresses are already exists in staging, synchronize the addresses from landing
		else {
			HashMap<Character, List<StagingAddressBean>> changeSetAddresses = ChangeSetBuilder.buildStagingAddressBeansChangeSet(landingAddressBeansValid,
							stagingAddressBeans);

			changeSetAddresses.forEach((operation, stagingAddressBeansList) -> {
				if (operation == PartyConstants.RecordOperations.DELETE.charAt(0)) {
					stagingAddressBeansList.stream().forEach(sab -> {
						VM.log.kernelDebug("[PartyBuilder] Synchronize Address: deleting addresses in staging " +sab.getSourceAddressBusId());
						stagingAddressWriter.delete(sab);
					});
				}
			});

			changeSetAddresses.forEach((operation, stagingAddressBeansList) -> {
				if (operation == PartyConstants.RecordOperations.CREATE.charAt(0)) {
					stagingAddressBeansList.stream().forEach(sab -> {
						VM.log.kernelDebug("[PartyBuilder] Synchronize Address: creating addresses in staging " + sab.getSourceAddressBusId());	
						stagingAddressWriter.createBeanOrThrow(sab);
					});
				} else if (operation == PartyConstants.RecordOperations.UPDATE.charAt(0)) {
					stagingAddressBeansList.stream().forEach(sab -> {
						VM.log.kernelDebug("[PartyBuilder] Synchronize Address: updating addresses in staging " + sab.getSourceAddressBusId());
						sab.setLastSyncTimeStamp(new Date());
						stagingAddressWriter.updateBean(sab);
					});
				}
			});
		}
		
		return landingAddressBeansValid;

        
	}
	
	public static List<LandingBankAccountBean> createOrUpdateStagingBankAccountsByLandingBankAccounts(ProcedureContext procedureContext,
			String sourceSystem, String sourcePartyBusId) {
		
		StagingBankAccountWriter stagingBankAccountWriter = new StagingBankAccountWriterProcedureContext(procedureContext, true);
		StagingBankAccountReader stagingBankAccountReader = new StagingBankAccountReader();
		List<StagingBankAccountBean> stagingBankAccountBeans = stagingBankAccountReader.findBeansByParty(sourceSystem, sourcePartyBusId);

		LandingBankAccountReader landingBankAccountReader = new LandingBankAccountReader();
		// LandingBankAccountWriterSession landingBankAccountWriterSession = new LandingBankAccountWriterSession(procedureContext.getSession(), true);
		List<LandingBankAccountBean> landingBankAccountBeans = landingBankAccountReader.findBeansByParty(sourceSystem, sourcePartyBusId);

		// Filter landing address beans by validation errors
		List<LandingBankAccountBean> landingBankAccountBeansValid = landingBankAccountBeans.stream().filter(landingBankAccountBean -> !landingBankAccountReader.hasValidationErrors(
								new Object[]{landingBankAccountBean.getId()})).collect(Collectors.toList());

		if (stagingBankAccountBeans.isEmpty()) {
			for (LandingBankAccountBean landingBankAccountBean : landingBankAccountBeansValid) {
				VM.log.kernelDebug("[PartyBuilder] Synchronize Bank Accounts: Migrating landing bank accounts to staging");
				StagingBankAccountBean stagingBankAccountBean = com.elipslife.ebx.domain.bean.convert.BankAccountConverter.createStagingBankAccountBeanFromLandingBankAccountBean(
								landingBankAccountBean);
				stagingBankAccountWriter.createBeanOrThrow(stagingBankAccountBean);
			}
		}

		// Party exists already and has to be updated
		else {
			
			HashMap<Character, List<StagingBankAccountBean>> changeSetBankAccounts =
                ChangeSetBuilder.buildStagingBankAccountBeansChangeSet(landingBankAccountBeansValid, stagingBankAccountBeans);

            changeSetBankAccounts.forEach((operation, stagingBankAccountBeansList) -> {
                if (operation == PartyConstants.RecordOperations.DELETE.charAt(0)) {
	                    stagingBankAccountBeansList.stream().forEach(sbab -> {
	                    VM.log.kernelDebug("[PartyBuilder] Synchronize Bank Accounts: deleting bank accounts in staging "+sbab.getSourceBankAccountBusId());
	                    stagingBankAccountWriter.delete(sbab);
	                });
                }
            });

            changeSetBankAccounts.forEach((operation, stagingBankAccountBeansList) -> {
                if (operation == PartyConstants.RecordOperations.CREATE.charAt(0)) {
                    stagingBankAccountBeansList.stream().forEach(sbab -> {
                    	VM.log.kernelDebug("[PartyBuilder] Synchronize Bank Accounts: creating bank accounts in staging " + sbab.getSourceBankAccountBusId());
                        stagingBankAccountWriter.createBeanOrThrow(sbab);
                    });
                } else if (operation == PartyConstants.RecordOperations.UPDATE.charAt(0)) {
                    stagingBankAccountBeansList.stream().forEach(sbab -> {
                    	VM.log.kernelDebug("[PartyBuilder] Synchronize Bank Accounts: updating bank accounts in staging " + sbab.getSourceBankAccountBusId());
                        sbab.setLastSyncTimeStamp(new Date());
                        stagingBankAccountWriter.updateBean(sbab);
                    });
                }
            });
		}
		
		return landingBankAccountBeansValid;
	}
	
	/**
	 * 
	 * Create or Update core addresses by staging addresses by party
	 * 
	 */
	public static void createOrUpdateCoreAddressByStagingAddress(ProcedureContext procedureContext, String sourceSystem, String sourcePartyBusId) {
		
		StagingAddressReader stagingAddressReader = new StagingAddressReader();
		List<StagingAddressBean> stagingAddressBeans = stagingAddressReader.findBeansByParty(sourceSystem, sourcePartyBusId);
		for(StagingAddressBean stagingAddressBean : stagingAddressBeans)
		{
			// Don't process future party address
			boolean isWithinRange = DateNullableComparator.dateWithinRange(stagingAddressBean.getValidFrom(), stagingAddressBean.getValidTo());
            if (!isWithinRange) {
                continue;
            }
            
			CoreAddressBuilder.createOrUpdateCoreAddressByStagingAddress(procedureContext, stagingAddressBean);
		}
	}
	
	/**
	 * Create or Update staging bank accounts by landing bank accounts by party
	 * 
	 * @param procedureContext
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 */
	public static void createOrUpdateCoreBankAccountByStagingBankAccount(ProcedureContext procedureContext, String sourceSystem, String sourcePartyBusId) {
		
		StagingBankAccountReader stagingBankAccountReader = new StagingBankAccountReader();
		List<StagingBankAccountBean> stagingBankAccountBeans = stagingBankAccountReader.findBeansByParty(sourceSystem, sourcePartyBusId);
		for(StagingBankAccountBean stagingBankAccountBean : stagingBankAccountBeans)
		{
			// Don't process future party bank accounts
			boolean isWithinRange = DateNullableComparator.dateWithinRange(stagingBankAccountBean.getValidFrom(), stagingBankAccountBean.getValidTo());
			if (!isWithinRange) {
				continue;
			}
            
			CoreBankAccountBuilder.createOrUpdateCoreBankAccountByStagingBankAccount(procedureContext, stagingBankAccountBean);
		}
	}
	
	
}
