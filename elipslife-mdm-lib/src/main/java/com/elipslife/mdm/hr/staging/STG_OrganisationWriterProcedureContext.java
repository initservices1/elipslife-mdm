package com.elipslife.mdm.hr.staging;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class STG_OrganisationWriterProcedureContext extends STG_BuildingWriter {

	private final ProcedureContext procedureContext;

	public STG_OrganisationWriterProcedureContext(ProcedureContext procedureContext) {
		super();
		this.procedureContext = procedureContext;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
