package com.elipslife.ebx.domain.bean;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class CoreAddressBean extends Entity<Integer> {
	
	private String partyId;
	private int addressId;
	private String addressType;
	private String addressUsage;
	private String sourceAddressBusId;
	private String street;
	private String street2;
	private String town;
	private String postCode;
	private String poBox;
	private String poBoxPostCode;
	private String poBoxTown;
	private String district;
	private String stateProvince;
	private String country;
	private String electronicAddressType;
	private String address;
	private Boolean isExpired;
	private Date validFrom;
	private Date validTo;
	private String sourceSystem;
	private String sourcePartyBusId;
	private String createdBy;
	private Date creationTimestamp;
	private String lastUpdatedBy;
	private Date lastUpdateTimestamp;
	private String lastSyncAction;
	private Date lastSyncTimestamp;
	

	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getAddressUsage() {
		return addressUsage;
	}
	public void setAddressUsage(String addressUsage) {
		this.addressUsage = addressUsage;
	}
	public String getSourceAddressBusId() {
		return sourceAddressBusId;
	}
	public void setSourceAddressBusId(String sourceAddressBusId) {
		this.sourceAddressBusId = sourceAddressBusId;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getStreet2() {
		return street2;
	}
	public void setStreet2(String street2) {
		this.street2 = street2;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getPoBox() {
		return poBox;
	}
	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}
	public String getPoBoxPostCode() {
		return poBoxPostCode;
	}
	public void setPoBoxPostCode(String poBoxPostCode) {
		this.poBoxPostCode = poBoxPostCode;
	}
	public String getPoBoxTown() {
		return poBoxTown;
	}
	public void setPoBoxTown(String poBoxTown) {
		this.poBoxTown = poBoxTown;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getStateProvince() {
		return stateProvince;
	}
	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getElectronicAddressType() {
		return electronicAddressType;
	}
	public void setElectronicAddressType(String electronicAddressType) {
		this.electronicAddressType = electronicAddressType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Boolean getIsExpired() {
		return isExpired;
	}
	public void setIsExpired(Boolean isExpired) {
		this.isExpired = isExpired;
	}
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	
	public String getSourcePartyBusId() {
		return sourcePartyBusId;
	}
	public void setSourcePartyBusId(String sourcePartyBusId) {
		this.sourcePartyBusId = sourcePartyBusId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getLastSyncAction() {
		return lastSyncAction;
	}
	public void setLastSyncAction(String lastSyncAction) {
		this.lastSyncAction = lastSyncAction;
	}
	public Date getLastSyncTimestamp() {
		return lastSyncTimestamp;
	}
	public void setLastSyncTimestamp(Date lastSyncTimestamp) {
		this.lastSyncTimestamp = lastSyncTimestamp;
	}
}