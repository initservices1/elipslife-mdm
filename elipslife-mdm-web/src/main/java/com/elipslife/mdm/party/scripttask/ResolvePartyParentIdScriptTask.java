package com.elipslife.mdm.party.scripttask;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.elipslife.ebx.data.access.CorePartyIdentifierReader;
import com.elipslife.ebx.data.access.CorePartyReader;
import com.elipslife.ebx.data.access.UpdateRecords;
import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.ebx.domain.bean.convert.CorePartyConverter;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.presales.toolbox.procedure.RecordValuesBean;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

public class ResolvePartyParentIdScriptTask extends ScriptTask {

	@Override
	public void executeScript(ScriptTaskContext aContext) throws OperationException {

		HashMap<String, String> contextVariables = getContextVariables(aContext);
		String masterDataspace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		Repository reposity = aContext.getRepository();
        AdaptationHome masterDataspaceHome =  reposity.lookupHome(HomeKey.forBranchName(masterDataspace));
        Date dataspaceCreationTimestamp = masterDataspaceHome.getCreationDate();
        CorePartyReader corePartyReader = new CorePartyReader(masterDataspace);
		
		resolveUnResolvedParentIds(corePartyReader, aContext, masterDataspace, dataspaceCreationTimestamp, masterDataspaceHome);
		
	}
	
	private void resolveUnResolvedParentIds(CorePartyReader corePartyReader, ScriptTaskContext aContext, String masterDataspace, Date dataspaceCreationTimestamp, AdaptationHome masterDataspaceHome) {
		
		// Get all unresolved parties 
		List<CorePartyBean> unresolvedParentRecords = corePartyReader.findUnResolvedParentIds(dataspaceCreationTimestamp);
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		List<CorePartyBean> updatedCoreParties = new ArrayList<CorePartyBean>();
		
		for (CorePartyBean corePartyBean : unresolvedParentRecords) {
			CorePartyIdentifierReader corePartyIdentifierReader = new CorePartyIdentifierReader(masterDataspace);
			String sourceSystem = corePartyBean.getSourceSystem();
			String sourceOrganisationParentBusinessId = corePartyBean.getSourceParentBusId();
			if(sourceSystem != null) {
				// Update Org.unit parent id
				if (corePartyBean.getParentId() == null && sourceOrganisationParentBusinessId != null) {
					List<CorePartyIdentifierBean> corePartyIdentifierBeans = corePartyIdentifierReader.getBeansBySourceSystem(sourceSystem, sourceOrganisationParentBusinessId);
					CorePartyIdentifierBean corePartyIdentifierBean = !corePartyIdentifierBeans.isEmpty() ? corePartyIdentifierBeans.get(0) : null;
					String partyId = corePartyIdentifierBean == null ? null : corePartyIdentifierBean.getPartyId();
					corePartyBean.setParentId(partyId);	
				}
				
				// Update Organization id
				if (corePartyBean.getOrganisationId() == null && corePartyBean.getSourceOrganisationBusId() != null) {
					List<CorePartyIdentifierBean> corePartyIdentifierBeans = corePartyIdentifierReader.getBeansBySourceSystem(sourceSystem, corePartyBean.getSourceOrganisationBusId());
					CorePartyIdentifierBean corePartyIdentifierBean = !corePartyIdentifierBeans.isEmpty() ? corePartyIdentifierBeans.get(0) : null;
					String partyId = corePartyIdentifierBean == null ? null : corePartyIdentifierBean.getPartyId();
					corePartyBean.setOrganisationId(partyId);	
				}
				
				// Update LineManager id
				if (corePartyBean.getLineManagerId() == null && corePartyBean.getSourceLineManagerBusId() != null) {
					List<CorePartyIdentifierBean> corePartyIdentifierBeans = corePartyIdentifierReader.getBeansBySourceSystem(sourceSystem, corePartyBean.getSourceLineManagerBusId());
					CorePartyIdentifierBean corePartyIdentifierBean = !corePartyIdentifierBeans.isEmpty() ? corePartyIdentifierBeans.get(0) : null;
					String partyId = corePartyIdentifierBean == null ? null : corePartyIdentifierBean.getPartyId();
					corePartyBean.setLineManagerId(partyId);	
				}
				
				// Update Partner id
				if (corePartyBean.getPartnerId() == null && corePartyBean.getSourcePartnerBusId() != null) {
					List<CorePartyIdentifierBean> corePartyIdentifierBeans = corePartyIdentifierReader.getBeansBySourceSystem(sourceSystem, corePartyBean.getSourcePartnerBusId());
					CorePartyIdentifierBean corePartyIdentifierBean = !corePartyIdentifierBeans.isEmpty() ? corePartyIdentifierBeans.get(0) : null;
					String partyId = corePartyIdentifierBean == null ? null : corePartyIdentifierBean.getPartyId();
					corePartyBean.setPartnerId(partyId);	
				}
				
				updatedCoreParties.add(corePartyBean);
			}
		}
		
		if(!updatedCoreParties.isEmpty()) {
			RecordValuesBean rvb = null;
			HashMap<Path, Object> values = null;
			AdaptationTable partyTable = masterDataspaceHome.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.PARTY_MASTER)).getTable(PartyPaths._Party.getPathInSchema());
			for (CorePartyBean corePartyBean : updatedCoreParties) {
				values = CorePartyConverter.fillCorePartyValuesFromStagingPartyBean(corePartyBean, true);
				rvb = new RecordValuesBean(partyTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(corePartyBean.getId().toString())), values);
				recordValuesBeanList.add(rvb);
			}

			final ProgrammaticService service = ProgrammaticService.createForSession(aContext.getSession(), masterDataspaceHome);
			UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
			ProcedureResult result = service.execute(updateRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
			}
		}
	}

	/**
	 * 
	 * @param dataContext
	 * @return
	 */
	private static HashMap<String, String> getContextVariables(
			com.orchestranetworks.workflow.DataContextReadOnly dataContext) {

		HashMap<String, String> hashMap = new HashMap<String, String>();
		Iterator<String> variableNames = dataContext.getVariableNames();
		while (variableNames.hasNext()) {
			String variableName = variableNames.next();
			String variableValue = dataContext.getVariableString(variableName);
			hashMap.put(variableName, variableValue);
		}

		return hashMap;
	}
}
