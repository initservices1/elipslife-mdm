package com.elipslife.mdm.contract.landing;

import java.util.Date;
import java.util.List;

import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class LandingExposureReader extends AbstractTableReader<LandingExposureBean> {

	public LandingExposureReader() {
		this(PartyConstants.DataSpace.LANDING_REFERENCE);
	}
	
	public LandingExposureReader(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_Exposure.getPathInSchema()));
	}
	
	public LandingExposureReader(AdaptationTable table) {
		super(table);
	}

	@Override
	protected LandingExposureBean createBeanFromRecord(Adaptation record) {
		
		LandingExposureBean bean = new LandingExposureBean();
		
		bean.setId(record.get_int(LandingPaths._LDG_Exposure._Exposure_ExposureId));
		bean.setExposureBusId(record.getString(LandingPaths._LDG_Exposure._Exposure_ExposureBusId));
		bean.setName(record.getString(LandingPaths._LDG_Exposure._Exposure_Name));
		bean.setProductBusId(record.getString(LandingPaths._LDG_Exposure._Exposure_ProductBusId));
		bean.setProductGroup(record.getString(LandingPaths._LDG_Exposure._Exposure_ProductGroup));
		bean.setProductName(record.getString(LandingPaths._LDG_Exposure._Exposure_ProductName));
		bean.setSourceProductBusId(record.getString(LandingPaths._LDG_Exposure._Exposure_SourceProductBusId));
		bean.setInsuranceContractId(record.get_int(LandingPaths._LDG_Exposure._Exposure_InsuranceContractId));
		
		bean.setSourceSystem(record.getString(LandingPaths._LDG_Exposure._Control_SourceSystem));
		bean.setSourceContractBusId(record.getString(LandingPaths._LDG_Exposure._Control_SourceContractBusId));
		bean.setCreationTimestamp(record.getDate(LandingPaths._LDG_Exposure._Control_CreationTimestamp));
		bean.setLastUpdateTimestamp(record.getDate(LandingPaths._LDG_Exposure._Control_LastUpdateTimestamp));
		bean.setLastSyncTimeStamp(record.getDate(LandingPaths._LDG_Exposure._Control_LastSyncTimestamp));
		bean.setAction(record.getString(LandingPaths._LDG_Exposure._Control_Action));
		bean.setActionBy(record.getString(LandingPaths._LDG_Exposure._Control_ActionBy));
		
		return bean;
	}

	@Override
	protected Path[] getPkPaths() {
		return new Path[] { LandingPaths._LDG_Exposure._Exposure_ExposureId };
	}

	@Override
	protected Object[] retrievePks(LandingExposureBean bean) {
		return new Object[] { bean.getId() };
	}
	
	/**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}
	
	/**
	 *  Find Exposure Records by Source System and Exposure Bus Id 
	 * 
	 * @param sourceSystem
	 * @param exposureBusId
	 * @return
	 */
	public List<LandingExposureBean> findBeansByExposureId(int exposureId)
	{
		XPathPredicate predicate1 = new XPathPredicate(Operation.Equals, LandingPaths._LDG_Exposure._Exposure_ExposureId, String.valueOf(exposureId));
//		XPathPredicate predicate1 = new XPathPredicate(Operation.Equals, LandingPaths._LDG_Exposure._Control_SourceSystem, sourceSystem);
//		XPathPredicate predicate2 = new XPathPredicate(Operation.Equals, LandingPaths._LDG_Exposure._Exposure_ExposureBusId, exposureBusId);
//		
//		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
//		group.addPredicate(predicate1);
//		group.addPredicate(predicate2);
		
		return super.readBeansFor(predicate1.toString());
	}
	
	public List<LandingExposureBean> findBeansBySourceSystemAndExposureBusId(String sourceSystem, String exposureBusId, Date creationTimeStamp, Date lastUpdatedTimestamp)
	{
		String creationDate = creationTimeStamp != null ? DateHelper.convertToXpathFormat(creationTimeStamp) : null;
		String lastUpdatedDate = lastUpdatedTimestamp != null ? DateHelper.convertToXpathFormat(lastUpdatedTimestamp) : null;
		
		XPathPredicate predicate1 = new XPathPredicate(Operation.Equals, LandingPaths._LDG_Exposure._Control_SourceSystem, sourceSystem);
		XPathPredicate predicate2 = new XPathPredicate(Operation.Equals, LandingPaths._LDG_Exposure._Exposure_ExposureBusId, exposureBusId);
		XPathPredicate predicate3;
		if(creationDate == null ) {
			predicate3 = new XPathPredicate(Operation.IsNull, LandingPaths._LDG_Exposure._Control_CreationTimestamp, creationDate);
		} else {
			predicate3 = new XPathPredicate(Operation.DateEqual, LandingPaths._LDG_Exposure._Control_CreationTimestamp, creationDate);
		}
		
		XPathPredicate predicate4;
		if(lastUpdatedDate == null ) {
			predicate4 = new XPathPredicate(Operation.IsNull, LandingPaths._LDG_Exposure._Control_LastUpdateTimestamp, lastUpdatedDate);
		} else {
			predicate4 = new XPathPredicate(Operation.DateEqual, LandingPaths._LDG_Exposure._Control_LastUpdateTimestamp, lastUpdatedDate);
		}
		
		
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate1);
		group.addPredicate(predicate2);
		group.addPredicate(predicate3);
		group.addPredicate(predicate4);
		
		return super.readBeansFor(group.toString());
	}
	
	
}