package com.elipslife.mdm.uw.staging;

import java.math.BigDecimal;
import java.util.Date;

public class StagingUWCaseBean {
    private Integer UWCaseId;
    private String UWCaseBusId;
    private String UWCaseMaster;
    private String SourceSystem;
    private String DataOwner;
    private String UWApplicationId;
    private String UWApplicationBusId;
    private String InsuranceContractId;
    private String ContractNumber;
    private String ApplicantId;
    private String ApplicantBusId;
    private String ApplicantRelationship;
    private String ApplicantFirstName;
    private String ApplicantLastName;
    private Date ApplicantDateOfBirth;
    private String ApplicantSSN;
    private String ApplicantTown;
    private String ApplicantPostCode;
    private String ApplicantCountry;
    private String ApplicantClass;
    private String ProductType;
    private Date EligiblityDate;
    private String ApplicationReason;
    private String ApplicationReasonText;
    private BigDecimal InforceCoverage;
    private BigDecimal AdditionalCoverage;
    private Boolean IsActive;
    private String CaseState;
    private String Control;
    private String CreatedBy;
    private Date CreationTimeStamp;
	private String LastUpdatedBy;
	private Date LastUpdateTimeStamp;
	private Date DeletionTimeStamp;
	private Date lastSyncTimeStamp;
	private String Action;

    public Integer getUWCaseId() {
        return UWCaseId;
    }

    public void setUWCaseId(Integer UWCaseId) {
        this.UWCaseId = UWCaseId;
    }

    public String getUWCaseBusId() {
        return UWCaseBusId;
    }

    public void setUWCaseBusId(String UWCaseBusId) {
        this.UWCaseBusId = UWCaseBusId;
    }
    

    public String getUWCaseMaster() {
		return UWCaseMaster;
	}

	public void setUWCaseMaster(String uWCaseMaster) {
		this.UWCaseMaster = uWCaseMaster;
	}

	public String getSourceSystem() {
        return SourceSystem;
    }

    public void setSourceSystem(String SourceSystem) {
        this.SourceSystem = SourceSystem;
    }

    public String getDataOwner() {
        return DataOwner;
    }

    public void setDataOwner(String DataOwner) {
        this.DataOwner = DataOwner;
    }

    public String getUWApplicationId() {
        return UWApplicationId;
    }

    public void setUWApplicationId(String UWApplicationId) {
        this.UWApplicationId = UWApplicationId;
    }

    public String getUWApplicationBusId() {
        return UWApplicationBusId;
    }

    public void setUWApplicationBusId(String UWApplicationBusId) {
        this.UWApplicationBusId = UWApplicationBusId;
    }

    public String getInsuranceContractId() {
        return InsuranceContractId;
    }

    public void setInsuranceContractId(String InsuranceContractId) {
        this.InsuranceContractId = InsuranceContractId;
    }

    public String getContractNumber() {
        return ContractNumber;
    }

    public void setContractNumber(String ContractNumber) {
        this.ContractNumber = ContractNumber;
    }

    public String getApplicantId() {
        return ApplicantId;
    }

    public void setApplicantId(String ApplicantId) {
        this.ApplicantId = ApplicantId;
    }

    public String getApplicantBusId() {
        return ApplicantBusId;
    }

    public void setApplicantBusId(String ApplicantBusId) {
        this.ApplicantBusId = ApplicantBusId;
    }

    public String getApplicantRelationship() {
        return ApplicantRelationship;
    }

    public void setApplicantRelationship(String ApplicantRelationship) {
        this.ApplicantRelationship = ApplicantRelationship;
    }

    public String getApplicantFirstName() {
        return ApplicantFirstName;
    }

    public void setApplicantFirstName(String ApplicantFirstName) {
        this.ApplicantFirstName = ApplicantFirstName;
    }

    public String getApplicantLastName() {
        return ApplicantLastName;
    }

    public void setApplicantLastName(String ApplicantLastName) {
        this.ApplicantLastName = ApplicantLastName;
    }

    public Date getApplicantDateOfBirth() {
        return ApplicantDateOfBirth;
    }

    public void setApplicantDateOfBirth(Date ApplicantDateOfBirth) {
        this.ApplicantDateOfBirth = ApplicantDateOfBirth;
    }

    public String getApplicantSSN() {
        return ApplicantSSN;
    }

    public void setApplicantSSN(String ApplicantSSN) {
        this.ApplicantSSN = ApplicantSSN;
    }

    public String getApplicantTown() {
        return ApplicantTown;
    }

    public void setApplicantTown(String ApplicantTown) {
        this.ApplicantTown = ApplicantTown;
    }

    public String getApplicantPostCode() {
        return ApplicantPostCode;
    }

    public void setApplicantPostCode(String ApplicantPostCode) {
        this.ApplicantPostCode = ApplicantPostCode;
    }

    public String getApplicantCountry() {
        return ApplicantCountry;
    }

    public void setApplicantCountry(String ApplicantCountry) {
        this.ApplicantCountry = ApplicantCountry;
    }

    public String getApplicantClass() {
        return ApplicantClass;
    }

    public void setApplicantClass(String ApplicantClass) {
        this.ApplicantClass = ApplicantClass;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String ProductType) {
        this.ProductType = ProductType;
    }

    public Date getEligiblityDate() {
        return EligiblityDate;
    }

    public void setEligiblityDate(Date EligiblityDate) {
        this.EligiblityDate = EligiblityDate;
    }

    public String getApplicationReason() {
        return ApplicationReason;
    }

    public void setApplicationReason(String ApplicationReason) {
        this.ApplicationReason = ApplicationReason;
    }

    public String getApplicationReasonText() {
        return ApplicationReasonText;
    }

    public void setApplicationReasonText(String ApplicationReasonText) {
        this.ApplicationReasonText = ApplicationReasonText;
    }

    public BigDecimal getInforceCoverage() {
        return InforceCoverage;
    }

    public void setInforceCoverage(BigDecimal InforceCoverage) {
        this.InforceCoverage = InforceCoverage;
    }

    public BigDecimal getAdditionalCoverage() {
        return AdditionalCoverage;
    }

    public void setAdditionalCoverage(BigDecimal AdditionalCoverage) {
        this.AdditionalCoverage = AdditionalCoverage;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    public String getCaseState() {
        return CaseState;
    }

    public void setCaseState(String CaseState) {
        this.CaseState = CaseState;
    }

    public String getControl() {
        return Control;
    }

    public void setControl(String Control) {
        this.Control = Control;
    }

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public Date getCreationTimeStamp() {
		return CreationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		CreationTimeStamp = creationTimeStamp;
	}

	public String getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public Date getDeletionTimeStamp() {
		return DeletionTimeStamp;
	}

	public void setDeletionTimeStamp(Date deletionTimeStamp) {
		DeletionTimeStamp = deletionTimeStamp;
	}

	public Date getLastUpdateTimeStamp() {
		return LastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(Date lastUpdateTimeStamp) {
		LastUpdateTimeStamp = lastUpdateTimeStamp;
	}
	
	public Date getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}

	public void setLastSyncTimeStamp(Date lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}

	public String getAction() {
		return Action;
	}

	public void setAction(String action) {
		Action = action;
	}
    
    
}
