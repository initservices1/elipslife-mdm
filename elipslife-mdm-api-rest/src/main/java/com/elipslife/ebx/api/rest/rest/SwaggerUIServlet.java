package com.elipslife.ebx.api.rest.rest;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "swagger-ui", urlPatterns = {"/api-documentation"}

)
public class SwaggerUIServlet extends HttpServlet {

    private static final long serialVersionUID = -936129003429862879L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/www/swagger-ui/dist/index.html");
        requestDispatcher.forward(req, resp);
    }
}
