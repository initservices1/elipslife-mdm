package com.elipslife.ebx.data.access;

import com.onwbp.adaptation.AdaptationTable;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;

public abstract class BaseAddressWriter<T> extends AbstractTableWriter<T> {

	private BaseAddressReader<T> baseAddressReader;
	
	public BaseAddressWriter(BaseAddressReader<T> baseAddressReader, AdaptationTable table) {
		super(table);
		
		this.baseAddressReader = baseAddressReader;
	}
	
	@Override
	public AbstractTableReader<T> getTableReader() {

		return this.baseAddressReader;
	}
}