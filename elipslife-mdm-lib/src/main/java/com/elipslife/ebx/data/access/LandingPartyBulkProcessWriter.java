package com.elipslife.ebx.data.access;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.LandingPartyBulkProcessBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class LandingPartyBulkProcessWriter extends BasePartyWriter<LandingPartyBulkProcessBean> {

	private boolean useDefaultValues;
	
	public LandingPartyBulkProcessWriter(boolean useDefaultValues) {
	    
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.LANDING_REFERENCE, PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_Party_BulkProcess.getPathInSchema()),
			useDefaultValues);
	}
	
	public LandingPartyBulkProcessWriter(boolean useDefaultValues, String dataspace) {
        
        this(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataspace, PartyConstants.DataSet.LANDING_MASTER),
                LandingPaths._LDG_Party_BulkProcess.getPathInSchema()),
            useDefaultValues);
    }
	
	
	public LandingPartyBulkProcessWriter(AdaptationTable adaptationTable, boolean useDefaultValues) {
		super(new LandingPartyBulkProcessReader(adaptationTable), adaptationTable);
		
		this.useDefaultValues = useDefaultValues;
	}
	

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, LandingPartyBulkProcessBean bean) {

		values.put(LandingPaths._LDG_Party_BulkProcess._Party_PartyId, bean.getId());
		
		if(!useDefaultValues || bean.getDataOwner() != null) {
			values.put(LandingPaths._LDG_Party_BulkProcess._Party_DataOwner, bean.getDataOwner());
		}
		
		values.put(LandingPaths._LDG_Party_BulkProcess._Party_OperatingCountry, bean.getOperatingCountry());
		values.put(LandingPaths._LDG_Party_BulkProcess._Party_CRMId, bean.getCrmId());
		values.put(LandingPaths._LDG_Party_BulkProcess._Party_PartyType, bean.getPartyType());
		values.put(LandingPaths._LDG_Party_BulkProcess._Party_RelationshipOwner, bean.getRelationshipOwner());
		values.put(LandingPaths._LDG_Party_BulkProcess._Party_DataResponsible, bean.getDataResponsible());
		values.put(LandingPaths._LDG_Party_BulkProcess._Party_Language, bean.getLanguage());
		values.put(LandingPaths._LDG_Party_BulkProcess._Party_CorrelationId, bean.getCorrelationId()); // correlation id
		values.put(LandingPaths._LDG_Party_BulkProcess._Party_MdmStagingProcessResponse, bean.getMdmStagingProcessResponse()); // MDM Staging Process Response

		if(!useDefaultValues || bean.getIsActive() != null) {
			values.put(LandingPaths._LDG_Party_BulkProcess._Party_IsActive, bean.getIsActive());
		}
		
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_FirstName, bean.getFirstName());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_MiddleName, bean.getMiddleName());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_LastName, bean.getLastName());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_DateOfBirth, bean.getDateOfBirth());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_Gender, bean.getGender());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_Title, bean.getTitle());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_Salutation, bean.getSalutation());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_LetterSalutation, bean.getLetterSalutation());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_CivilStatus, bean.getCivilStatus());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_BirthCity, bean.getBirthCity());
		values.put(LandingPaths._LDG_Party_BulkProcess._Person_BirthCountry, bean.getBirthCountry());
		values.put(LandingPaths._LDG_Party_BulkProcess._IdDocument_ID_DocumentNumber, bean.getIdDocumentNumber());
		values.put(LandingPaths._LDG_Party_BulkProcess._IdDocument_ID_DocumentType, bean.getIdDocumentType());
		values.put(LandingPaths._LDG_Party_BulkProcess._IdDocument_ID_IssueDate, bean.getIdIssueDate());
		values.put(LandingPaths._LDG_Party_BulkProcess._IdDocument_ID_ExpiryDate, bean.getIdExpiryDate());
		values.put(LandingPaths._LDG_Party_BulkProcess._IdDocument_ID_IssuingAuthority, bean.getIdIssuingAuthority());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_SourceParentBusId, bean.getSourceParentBusId());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_OrgName, bean.getOrgName());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_OrgName2, bean.getOrgName2());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_CompanyType, bean.getCompanyType());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_CommercialRegNo, bean.getCommercialRegNo());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_UIDNumber, bean.getUidNumber());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_VATNumber, bean.getVatNumber());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_Industry, bean.getIndustry());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_SubIndustry, bean.getSubIndustry());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_PreferredEmail, bean.getPreferredEmail());
		values.put(LandingPaths._LDG_Party_BulkProcess._Organisation_PreferredCommChannel, bean.getPreferredCommChannel());
		values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_SourceOrganisationBusId, bean.getSourceOrganisationBusId());
		values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_SourceLineManagerBusId, bean.getSourceLineManagerBusId());
		values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_SourcePartnerBusId, bean.getSourcePartnerBusId());
		values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_Department, bean.getDepartment());
		values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_Function, bean.getFunction());
		values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_IsExecutor, bean.getIsExecutor());
		values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_IsLegalRepresentative, bean.getIsLegalRepresentative());
		
		if(!useDefaultValues || bean.getIsSeniorManager() != null) {
			values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_IsSeniorManager, bean.getIsSeniorManager());
		}
		
		if(!useDefaultValues || bean.getIsUltimateBeneficialOwner() != null) {
			values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_IsUltimateBeneficialOwner, bean.getIsUltimateBeneficialOwner());
		}
		
		values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_BORelationship, bean.getBoRelationship());
		values.put(LandingPaths._LDG_Party_BulkProcess._ContactPerson_ContactPersonState, bean.getContactPersonState());
		values.put(LandingPaths._LDG_Party_BulkProcess._Role_Roles, bean.getRoles());
		
		if(!useDefaultValues || bean.getIsVipClient() != null) {
			values.put(LandingPaths._LDG_Party_BulkProcess._Role_IsVIPClient, bean.getIsVipClient());
		}
		
		values.put(LandingPaths._LDG_Party_BulkProcess._Role_Blacklist, bean.getBlacklist());
		values.put(LandingPaths._LDG_Party_BulkProcess._Role_PartnerState, bean.getPartnerState());
		values.put(LandingPaths._LDG_Party_BulkProcess._Role_CompanyPersonalNr, bean.getCompanyPersonalNr());
		values.put(LandingPaths._LDG_Party_BulkProcess._Role_SocialSecurityNr, bean.getSocialSecurityNr());
		values.put(LandingPaths._LDG_Party_BulkProcess._Role_SocialSecurityNrType, bean.getSocialSecurityNrType());
		values.put(LandingPaths._LDG_Party_BulkProcess._Role_BrokerRegNo, bean.getBrokerRegNo());
		values.put(LandingPaths._LDG_Party_BulkProcess._Role_BrokerTiering, bean.getBrokerTiering());
		values.put(LandingPaths._LDG_Party_BulkProcess._Role_LineOfBusiness, bean.getLineOfBusiness());
		values.put(LandingPaths._LDG_Party_BulkProcess._Financial_FinancePartyBusId, bean.getFinancePartyBusId());
		values.put(LandingPaths._LDG_Party_BulkProcess._Financial_PaymentTerms, bean.getPaymentTerms());
		values.put(LandingPaths._LDG_Party_BulkProcess._Financial_FinanceClients, bean.getFinanceClients());
		values.put(LandingPaths._LDG_Party_BulkProcess._Financial_CreditorClients, bean.getCreditorClients());
		values.put(LandingPaths._LDG_Party_BulkProcess._Control_SourceSystem, bean.getSourceSystem());
		values.put(LandingPaths._LDG_Party_BulkProcess._Control_SourcePartyBusId, bean.getSourcePartyBusId());
		values.put(LandingPaths._LDG_Party_BulkProcess._Control_ValidFrom, bean.getValidFrom());
		values.put(LandingPaths._LDG_Party_BulkProcess._Control_ValidTo, bean.getValidTo());
		values.put(LandingPaths._LDG_Party_BulkProcess._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(LandingPaths._LDG_Party_BulkProcess._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(LandingPaths._LDG_Party_BulkProcess._Control_Action, bean.getAction());
		values.put(LandingPaths._LDG_Party_BulkProcess._Control_ActionBy, bean.getActionBy());
	}
}