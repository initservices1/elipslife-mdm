package com.elipslife.mdm.hr.landing;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.LandingHrModelPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class LandingBuildingReader extends AbstractTableReader<LandingBuildingBean> {

    public LandingBuildingReader() {
        this(Constants.DataSpace.LANDING_HR_REFERENCE);
    }

    public LandingBuildingReader(String dataspace) {
        this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.LANDING_HR_MASTER),
				LandingHrModelPaths._Building.getPathInSchema()));
    }
    
    public LandingBuildingReader(AdaptationTable table)
    {
    	super(table);
    }

    @Override
    public LandingBuildingBean createBeanFromRecord(Adaptation record) {
        LandingBuildingBean bean = new LandingBuildingBean();
        bean.setBuildingId(record.get_int(LandingHrModelPaths._Building._Building_BuildingId));
        bean.setDataOwner(record.getString(LandingHrModelPaths._Building._Building_DataOwner));
        bean.setBuildingBusId(record.getString(LandingHrModelPaths._Building._Building_BuildingBusId));
        bean.setName(record.getString(LandingHrModelPaths._Building._Building_Name));
        bean.setStreet(record.getString(LandingHrModelPaths._Building._Building_Street));
        bean.setStreet2(record.getString(LandingHrModelPaths._Building._Building_Street2));
        bean.setPostCode(record.getString(LandingHrModelPaths._Building._Building_PostCode));
        bean.setPOBox(record.getString(LandingHrModelPaths._Building._Building_POBox));
        bean.setPOBoxPostCode(record.getString(LandingHrModelPaths._Building._Building_POBoxPostCode));
        bean.setPOBoxTown(record.getString(LandingHrModelPaths._Building._Building_POBoxTown));
        bean.setTown(record.getString(LandingHrModelPaths._Building._Building_Town));
        bean.setDistrict(record.getString(LandingHrModelPaths._Building._Building_District));
        bean.setStateProvince(record.getString(LandingHrModelPaths._Building._Building_StateProvince));
        bean.setCountry(record.getString(LandingHrModelPaths._Building._Building_Country));
        bean.setIsActive((Boolean)record.get(LandingHrModelPaths._Building._Building_IsActive));
        bean.setSourceSystem(record.getString(LandingHrModelPaths._Building._Control_SourceSystem));
        bean.setCreatedBy(record.getString(LandingHrModelPaths._Building._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(LandingHrModelPaths._Building._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(LandingHrModelPaths._Building._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(LandingHrModelPaths._Building._Control_LastUpdateTimestamp));
      
        
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{LandingHrModelPaths._Building._Building_BuildingId};
    }

    @Override
    public Object[] retrievePks(LandingBuildingBean bean) {
        return new Object[]{bean.getBuildingId()};
    }

}
