package com.elipslife.ebx.api.rest.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.lang3.StringUtils;

import com.elipslife.ebx.api.rest.convert.bean.ContractCoverConverter;
import com.elipslife.ebx.api.rest.convert.bean.ExposureConverter;
import com.elipslife.ebx.api.rest.convert.bean.InsuranceContractConverter;
import com.elipslife.ebx.api.rest.convert.bean.PartyConverter;
import com.elipslife.ebx.api.rest.convert.bean.UWApplicationConverter;
import com.elipslife.ebx.api.rest.convert.bean.UWCaseConverter;
import com.elipslife.ebx.api.rest.models.SynchroniseClaimCase;
import com.elipslife.ebx.api.rest.models.SynchroniseInsuranceContract;
import com.elipslife.ebx.api.rest.models.SynchroniseParty;
import com.elipslife.ebx.api.rest.models.SynchroniseUWApplication;
import com.elipslife.ebx.data.access.LandingAddressReader;
import com.elipslife.ebx.data.access.LandingAddressWriterSession;
import com.elipslife.ebx.data.access.LandingBankAccountReader;
import com.elipslife.ebx.data.access.LandingBankAccountWriterSession;
import com.elipslife.ebx.data.access.LandingPartyReader;
import com.elipslife.ebx.data.access.LandingPartyWriterSession;
import com.elipslife.ebx.data.access.StagingPartyReader;
import com.elipslife.ebx.data.access.StagingPartyWriter;
import com.elipslife.ebx.data.access.StagingPartyWriterSession;
import com.elipslife.ebx.domain.bean.LandingAddressBean;
import com.elipslife.ebx.domain.bean.LandingBankAccountBean;
import com.elipslife.ebx.domain.bean.LandingPartyBean;
import com.elipslife.ebx.domain.bean.LandingPartyBulkProcessBean;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.mdm.claimcase.landing.LandingClaimCaseBean;
import com.elipslife.mdm.claimcase.landing.LandingClaimCaseWriter;
import com.elipslife.mdm.claimcase.landing.LandingClaimCaseWriterSession;
import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.contract.landing.LandingContractCoverBean;
import com.elipslife.mdm.contract.landing.LandingContractCoverWriter;
import com.elipslife.mdm.contract.landing.LandingContractCoverWriterSession;
import com.elipslife.mdm.contract.landing.LandingExposureBean;
import com.elipslife.mdm.contract.landing.LandingExposureWriter;
import com.elipslife.mdm.contract.landing.LandingExposureWriterSession;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractBean;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractWriter;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractWriterSession;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.elipslife.mdm.uw.landing.LandingUWApplicationBean;
import com.elipslife.mdm.uw.landing.LandingUWApplicationReader;
import com.elipslife.mdm.uw.landing.LandingUWApplicationWriter;
import com.elipslife.mdm.uw.landing.LandingUWApplicationWriterSession;
import com.elipslife.mdm.uw.landing.LandingUWCaseBean;
import com.elipslife.mdm.uw.landing.LandingUWCaseReader;
import com.elipslife.mdm.uw.landing.LandingUWCaseWriter;
import com.elipslife.mdm.uw.landing.LandingUWCaseWriterSession;
import com.elipslife.mdm.uw.staging.StagingUWApplicationBean;
import com.elipslife.mdm.uw.staging.StagingUWApplicationWriter;
import com.elipslife.mdm.uw.staging.StagingUWApplicationWriterSession;
import com.elipslife.mdm.uw.staging.StagingUWCaseBean;
import com.elipslife.mdm.uw.staging.StagingUWCaseWriter;
import com.elipslife.mdm.uw.staging.StagingUWCaseWriterSession;
import com.onwbp.boot.VM;
import com.orchestranetworks.rest.annotation.Documentation;
import com.orchestranetworks.rest.inject.SessionContext;
import com.orchestranetworks.service.OperationException;

import ch.butos.ebx.lib.logging.EbxLoggable;

@Path("/v2")
public class Version2 implements EbxLoggable {

    @Context
    private SessionContext sessionContext;

    @POST
    @Path("synchroniseParty")
    @Documentation("Synchronises changes (i.e. creation and updates) to a master system party entity, in order to ensure that the redundant MDM data is consistent. Parties may be persons, organisations/companies or contact persons covering all party role types (e.g. business partners, insured persons and beneficiaries).")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response synchroniseParty(SynchroniseParty synchroniseParty) throws Exception {

        com.elipslife.ebx.domain.bean.convert.ReadPartyBulkProcesserProperty restBulkProcesserProperty = com.elipslife.ebx.domain.bean.convert.ReadPartyBulkProcesserProperty.getInstance();
        // Check If it is Full load or Incremental load from Property file
        if (restBulkProcesserProperty.getPartyBulkProcessProperty().equals(PartyConstants.Entities.Party.RestBulkProcessState.REST_BULKPROCESS_STATE_FULL)) {
            // Process Rest Party request 
            LandingPartyBulkProcessBean[] landingPartyBeans = new com.elipslife.ebx.api.rest.convert.bean.PartyBulkProcessConverter().convert(this.sessionContext.getSession(), synchroniseParty);

        } else {

        	PartyConverter partyConverter = new PartyConverter();
        	partyConverter.convert(this.sessionContext.getSession(), synchroniseParty);
            
        	LandingPartyReader landingPartyReader = new LandingPartyReader();
        	List<LandingPartyBean> landingPartyBeans = landingPartyReader.readAllBeans();
        	LandingPartyWriterSession landingPartyWriterSession = new LandingPartyWriterSession(this.sessionContext.getSession(), true);
            LandingAddressWriterSession landingAddressWriterSession = new LandingAddressWriterSession(this.sessionContext.getSession(), true);
            LandingBankAccountWriterSession landingBankAccountWriterSession = new LandingBankAccountWriterSession(this.sessionContext.getSession(), true);
            try {
                List<LandingPartyBean> landingPartyBeansToDelete = new ArrayList<>();
                
                StagingPartyWriter stagingPartyWriter = new StagingPartyWriterSession(this.sessionContext.getSession(), true);
                // Find staging party beans by source system
                StagingPartyReader stagingPartyReader = new StagingPartyReader();
                
                for (LandingPartyBean landingPartyBean : landingPartyBeans) {
                	// Check if landing party has any validation errors 
                	boolean landingPartyBeanInvalid = landingPartyReader.hasValidationErrors(new Object[] {landingPartyBean.getId()});
                    if (landingPartyBeanInvalid) {
                    	// if yes, don't process landing party to staging and update the error messages
                    	String errorMessages = "Landing Party has validation errors";
                    	updateErrorMessagesInLandingAddress(errorMessages, landingPartyBean.getId());
                    	updateErrorMessagesInLandingBankAccounts(errorMessages, landingPartyBean.getId());
                    	continue;
                    }
                    
                    landingPartyBeansToDelete.add(landingPartyBean);  // add landing Party to delete once it is processed to staging
                    
                    List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(landingPartyBean.getSourceSystem(), landingPartyBean.getSourcePartyBusId(), landingPartyBean.getValidFrom(), landingPartyBean.getValidTo());
                    StagingPartyBean stagingPartyBean = null;
                    
                    // If Party does not exist, it has to be created           
                    if (stagingPartyBeans.isEmpty()) {
                        stagingPartyBean = com.elipslife.ebx.domain.bean.convert.PartyConverter.createStagingPartyBeanFromLandingPartyBean(landingPartyBean);
                        stagingPartyWriter.createBeanOrThrow(stagingPartyBean);
                    } else { // if one or more staging parties found, get latest LastUpdatedTimestamp staging party and process it.  
                    	stagingPartyBean = stagingPartyBeans.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getLastUpdateTimestamp(),
            						b.getLastUpdateTimestamp())).findFirst().get();
                    	
                    	// check if landing party last updated timestamp is equal or more than the staging party last updated timestamp.
                    	if (landingPartyBean.getUpdateTimestamp().compareTo(stagingPartyBean.getUpdateTimestamp()) >= 0) {
                    		com.elipslife.ebx.domain.bean.convert.PartyConverter.fillStagingPartyBeanWithLandingPartyBean(landingPartyBean, stagingPartyBean);
                    		stagingPartyBean.setLastSyncTimeStamp(new Date());
                    		stagingPartyWriter.updateBean(stagingPartyBean);
                    	} else {
                    		VM.log.kernelDebug("Landing Party LastUpdatedTimeStamp is lessthan the Staging Party LastUpdatedTimeStamp : "+stagingPartyBean.getId());
                    	}
                    }
                }
                
                // Cleanup landing tables
                for (LandingPartyBean landingPartyBean : landingPartyBeansToDelete) {
                	
                	// Filter landing address beans by party
                	LandingAddressReader landingAddressReader = new LandingAddressReader();
                	List<LandingAddressBean> landingAddressBeansForParty = landingAddressReader.findBeansByLandingPartyId(String.valueOf(landingPartyBean.getId().intValue()));
                    
                    // Filter landing bank account beans by party
                    LandingBankAccountReader bankAccountReader = new LandingBankAccountReader();
                    List<LandingBankAccountBean> landingBankAccountBeansForParty = bankAccountReader.findBeansByLandingPartyId(String.valueOf(landingPartyBean.getId().intValue()));
                    
                    // Delete addresses by party
                	for(LandingAddressBean landingAddressBean : landingAddressBeansForParty) {
                		landingAddressWriterSession.delete(landingAddressBean);
                	}
                	
                	// Delete bank accounts by party
                	for (LandingBankAccountBean landingBankAccountBean : landingBankAccountBeansForParty) {
                        landingBankAccountWriterSession.delete(landingBankAccountBean);
                    }
                    
                    landingPartyWriterSession.delete(landingPartyBean);
                }
            } catch (Exception ex) {
                e(ex.toString());
            }

        }
        return Response.ok().build();
    }


    @POST
    @Path("synchroniseClaimCase")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response synchroniseClaimCase(SynchroniseClaimCase synchroniseClaimCase) throws Exception {
        System.out.println(synchroniseClaimCase);

        LandingClaimCaseBean landingClaimCaseBean = new com.elipslife.ebx.api.rest.convert.bean.ClaimCaseConverter().convert(synchroniseClaimCase);
        LandingClaimCaseWriter landingClaimCaseWriter = new LandingClaimCaseWriterSession(this.sessionContext.getSession());
        landingClaimCaseWriter.createBeanOrThrow(landingClaimCaseBean);

        return Response.ok().build();
    }

    @POST
    @Path("synchroniseInsuranceContract")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response synchroniseInsuranceContract(SynchroniseInsuranceContract synchroniseInsuranceContract) throws DatatypeConfigurationException {

        System.out.println(synchroniseInsuranceContract);
        LandingInsuranceContractWriter landingInsuranceContractWriter =
            new LandingInsuranceContractWriterSession(this.sessionContext.getSession(), true);

        LandingInsuranceContractBean landingInsuranceContractBean = new InsuranceContractConverter().convert(synchroniseInsuranceContract);

        Optional<LandingInsuranceContractBean> landingInsuranceContrctBeanOptional =
            landingInsuranceContractWriter.createBeanOrThrow(landingInsuranceContractBean);


        LandingExposureBean[] landingExposureBeans = new ExposureConverter().convert(synchroniseInsuranceContract);

        LandingContractCoverBean[] landingContractCoverBeans = new ContractCoverConverter().convert(synchroniseInsuranceContract);
        LandingContractCoverWriter landingContractCoverWriter = new LandingContractCoverWriterSession(this.sessionContext.getSession());

        LandingExposureWriter landingExposureWriter = new LandingExposureWriterSession(this.sessionContext.getSession());
        for (LandingExposureBean landingExposureBean : landingExposureBeans) {

            landingExposureBean.setInsuranceContractId(landingInsuranceContrctBeanOptional.get().getId());
            Optional<LandingExposureBean> landingExposureBeanOptional = landingExposureWriter.createBeanOrThrow(landingExposureBean);
            // Create Contract Covers based on Exposure
            for (LandingContractCoverBean landingContractCoverBean : landingContractCoverBeans) {

                if ((landingExposureBean.getExposureBusId() == null && landingContractCoverBean.getExposureBusId() == null) ||
                    StringUtils.equals(landingExposureBean.getExposureBusId(), landingContractCoverBean.getExposureBusId())) {
                    // set Exposure Id
                    landingContractCoverBean.setExposureId(landingExposureBeanOptional.get().getId());
                    landingContractCoverWriter.createBeanOrThrow(landingContractCoverBean);
                }
            }
        }
        return Response.ok().build();
    }

    @POST
    @Path("synchroniseUWApplication")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response synchroniseUWApplication(SynchroniseUWApplication synchroniseUWApplication) throws Exception {

        LandingUWApplicationBean landingUWApplicationBean = new UWApplicationConverter().convert(synchroniseUWApplication);
        LandingUWApplicationWriter landingUWApplicationWriter = new LandingUWApplicationWriterSession(this.sessionContext.getSession());
        LandingUWApplicationReader landingUWApplicationReader = (LandingUWApplicationReader) landingUWApplicationWriter.getTableReader();
        Optional<LandingUWApplicationBean> landingUWApplicationBeanOptional = landingUWApplicationWriter.createBeanOrThrow(landingUWApplicationBean);

        List<LandingUWCaseBean> landingUWCaseBeansList = new ArrayList<>();
        LandingUWCaseBean[] landingUWCaseBeans = new UWCaseConverter().convert(synchroniseUWApplication);
        LandingUWCaseWriter landingUWCaseWriter = new LandingUWCaseWriterSession(this.sessionContext.getSession());
        LandingUWCaseReader landingUwCaseReader = (LandingUWCaseReader) landingUWCaseWriter.getTableReader();
        for (LandingUWCaseBean landingUWCaseBean : landingUWCaseBeans) {
            // Set parent UWApplication id into UWCase
            landingUWCaseBean.setUWApplicationId(landingUWApplicationBeanOptional.get().getUWApplicationId());
            Optional<LandingUWCaseBean> landingUWCaseBeanOptional = landingUWCaseWriter.createBeanOrThrow(landingUWCaseBean);
            landingUWCaseBeansList.add(landingUWCaseBeanOptional.get());
        }
        try {

            if (landingUWApplicationBeanOptional.get().getUWApplicationId() > 0) {

                boolean hasUWAppValidationErrors =
                    landingUWApplicationReader.hasValidationErrors(new Object[] {landingUWApplicationBeanOptional.get().getUWApplicationId()});
                if (hasUWAppValidationErrors) {
                    VM.log.kernelWarn(
                        "Migration UW Application error with UW Application id " + landingUWApplicationBeanOptional.get().getUWApplicationId());
                    return Response.ok().build();
                }

                StagingUWApplicationBean stagingUWApplicationBean =
                    com.elipslife.ebx.domain.bean.convert.UWConverter.transferUWApplicationBean(landingUWApplicationBean);
                StagingUWApplicationWriter stagingUWApplicationWriter =
                    new StagingUWApplicationWriterSession(this.sessionContext.getSession(), Constants.DataSpace.UW_REFERENCE);
                stagingUWApplicationWriter.createBeanOrThrow(stagingUWApplicationBean);

                landingUWApplicationWriter.delete(landingUWApplicationBeanOptional.get());

                // UWCase
                for (LandingUWCaseBean uwCaseBean : landingUWCaseBeansList) {

                    boolean hasUWCaseValidationErrors = landingUwCaseReader.hasValidationErrors(new Object[] {uwCaseBean.getUWCaseId()});
                    if (hasUWCaseValidationErrors) {
                        VM.log.kernelWarn("Migration UW Case error with UW Case id " + uwCaseBean.getUWCaseId());
                        continue;
                    }

                    // Create Staging UWCase record
                    StagingUWCaseBean stagingUWCaseBean = com.elipslife.ebx.domain.bean.convert.UWConverter.transferUWCasebean(uwCaseBean);
                    StagingUWCaseWriter stagingUWCaseWriter = new StagingUWCaseWriterSession(this.sessionContext.getSession(), Constants.DataSpace.UW_REFERENCE);
                    stagingUWCaseWriter.createBeanOrThrow(stagingUWCaseBean);

                    // delete Landing UWCase
                    landingUWCaseWriter.delete(uwCaseBean);
                }
            }
        } catch (Exception e) {
            VM.log.kernelError("Exception occurred while migrating UW records from Landing to Staging " + e.getMessage());
        }

        landingUWCaseBeansList.clear();

        return Response.ok().build();
    }
    
    /**
     * Update error messages in Landing Party table 
     * 
     * @param errorMessages
     * @param landingPartyId
     * @throws OperationException
     */
    private void updateErrorMessagesInLandingParty(String errorMessages, LandingPartyBean landingPartyBean) throws OperationException {
    	
    	LandingPartyWriterSession landingPartyWriterSession = new LandingPartyWriterSession(this.sessionContext.getSession(), true);
		landingPartyBean.setMdmStagingProcessResponse(errorMessages);
		landingPartyWriterSession.updateBean(landingPartyBean);
    }
    
    /**
     * Update error messages in Landing Address table if the party has not moved
     * 
     * @param errorMessages
     * @param landingPartyId
     * @throws OperationException
     */
    private void updateErrorMessagesInLandingAddress(String errorMessages, Integer landingPartyId) throws OperationException {
    	
    	LandingAddressReader landingAddressReader = new LandingAddressReader();
		List<LandingAddressBean> landingAddressBeans = landingAddressReader.findBeansByLandingPartyId(String.valueOf(landingPartyId.intValue()));
		LandingAddressWriterSession landingAddressWriterSession = new LandingAddressWriterSession(this.sessionContext.getSession(), true);
		for(LandingAddressBean landingAddressBean : landingAddressBeans) {
			landingAddressBean.setMdmStagingProcessResponse(errorMessages);
			landingAddressWriterSession.updateBean(landingAddressBean);
		}
    }
    
    /**
     * Update error messages in Landing Bank Accounts table if the party has not moved
     * 
     * @param errorMessages
     * @param landingPartyId
     * @throws OperationException
     */
    private void updateErrorMessagesInLandingBankAccounts(String errorMessages, Integer landingPartyId) throws OperationException {
    	
    	LandingBankAccountReader landingBankAccountReader = new LandingBankAccountReader();
    	List<LandingBankAccountBean> landingBankAccountBeans = landingBankAccountReader.findBeansByLandingPartyId(String.valueOf(landingPartyId.intValue()));
    	LandingBankAccountWriterSession landingBankAccountWriterSession = new LandingBankAccountWriterSession(this.sessionContext.getSession(), true);
    	for(LandingBankAccountBean landingBankAccountBean : landingBankAccountBeans) {
    		landingBankAccountBean.setMdmStagingProcessResponse(errorMessages);
    		landingBankAccountWriterSession.updateBean(landingBankAccountBean);
    	}
    }
}
