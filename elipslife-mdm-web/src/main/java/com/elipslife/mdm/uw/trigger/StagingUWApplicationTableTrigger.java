package com.elipslife.mdm.uw.trigger;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.UWPaths;
import com.elipslife.mdm.uw.core.UWApplicationBean;
import com.elipslife.mdm.uw.core.UWApplicationReader;
import com.elipslife.mdm.uw.core.UWApplicationWriter;
import com.elipslife.mdm.uw.core.UWApplicationWriterProcedureContext;
import com.elipslife.mdm.uw.staging.StagingUWApplicationBean;
import com.elipslife.mdm.uw.staging.StagingUWApplicationReader;
import com.elipslife.mdm.uw.staging.StagingUWApplicationWriter;
import com.elipslife.mdm.uw.staging.StagingUWApplicationWriterProcedureContext;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.base.text.Severity;
import com.onwbp.boot.VM;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValidationReport;

public class StagingUWApplicationTableTrigger extends TableTrigger {

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {
		super.handleAfterCreate(aContext);

		ValidationReport validationReport = aContext.getAdaptationOccurrence().getValidationReport();
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		if(!hasErrors) {
			moveUWApplicationFromStagingToMaster(aContext.getSession(), aContext.getProcedureContext(),
					aContext.getAdaptationOccurrence());
		}
	}

	public void moveUWApplicationFromStagingToMaster(Session session, ProcedureContext context, Adaptation adaptation) {
		
		// instantiate bean, reader and writer
		StagingUWApplicationReader stagingUWApplicationReader = new StagingUWApplicationReader();
		StagingUWApplicationBean stagingUWApplicationBean = stagingUWApplicationReader.createBeanFromRecord(adaptation);
		StagingUWApplicationWriter stagingUWApplicationWriter = new StagingUWApplicationWriterProcedureContext(context);
		
		UWApplicationReader applicationReader = new UWApplicationReader();
		List<UWApplicationBean> applicationBeans = applicationReader.findBeansByApplicationBusId(adaptation.getString(UWPaths._STG_UWApplication._UWApplicationBusId));
		if (applicationBeans.size() > 1) {
			throw new RuntimeException(
					String.format("More than one UWApplication record found with UWApplication bus id",
							adaptation.getString(UWPaths._STG_UWApplication._UWApplicationBusId)));
		}
		UWApplicationBean applicationBean = applicationBeans.isEmpty() ? null : applicationBeans.get(0);
		UWApplicationWriter uwApplicationWriter = new UWApplicationWriterProcedureContext(context);
		UWApplicationBean coreUWApplicationBean = new UWApplicationBean();
		
		if (applicationBean != null) {
			// update
			Date coreRecordLastUpdateTimestamp = applicationBean.getLastUpdateTimeStamp();
			Date stagingRecordLastUpdateTimestamp = stagingUWApplicationBean.getLastUpdateTimeStamp();
			if (stagingRecordLastUpdateTimestamp != null && (coreRecordLastUpdateTimestamp == null
					|| stagingRecordLastUpdateTimestamp.after(coreRecordLastUpdateTimestamp)
					|| stagingRecordLastUpdateTimestamp.equals(coreRecordLastUpdateTimestamp))) {
				transferBean(applicationBean, stagingUWApplicationBean);
				String action = stagingUWApplicationBean.getAction() != null ? stagingUWApplicationBean.getAction() : null;
				if(action != null) {
					if(action.equals(Constants.RecordOperations.DELETE)) {
						applicationBean.setLastSyncAction(Constants.RecordOperations.DELETE);
						applicationBean.setIsActive(false);
					} else {
						applicationBean.setLastSyncAction(Constants.RecordOperations.UPDATE);
					}
				} else {
					applicationBean.setLastSyncAction(Constants.RecordOperations.UPDATE);
				}
				uwApplicationWriter.updateBean(applicationBean);
				stagingUWApplicationBean.setUWApplicationMaster(String.valueOf(applicationBean.getUWApplicationId()));
				stagingUWApplicationWriter.updateBean(stagingUWApplicationBean);
				
			} else {
				stagingUWApplicationBean.setUWApplicationMaster(String.valueOf(applicationBean.getUWApplicationId()));
				stagingUWApplicationWriter.updateBean(stagingUWApplicationBean);
				VM.log.kernelWarn("***************************************************************");
				VM.log.kernelWarn("Ignoring record update to UWApplication Master for UWApplication Stage Record : " +
						stagingUWApplicationBean.getSourceSystem() + " and " +
						stagingUWApplicationBean.getUWApplicationBusId());
			}
			
		} else {
			// create
			transferBean(coreUWApplicationBean, stagingUWApplicationBean);
			coreUWApplicationBean.setLastSyncAction(Constants.RecordOperations.CREATE);
			Optional<UWApplicationBean> coreUWApplicationBeanOptional = uwApplicationWriter.createBeanOrThrow(coreUWApplicationBean);
			stagingUWApplicationBean.setUWApplicationMaster(String.valueOf(coreUWApplicationBeanOptional.get().getUWApplicationId()));
			stagingUWApplicationWriter.updateBean(stagingUWApplicationBean);
		}

	}

	public void transferBean(UWApplicationBean applicationBean, StagingUWApplicationBean stagingUWApplicationBean) {
		
		applicationBean.setUWApplicationBusId(stagingUWApplicationBean.getUWApplicationBusId());
		applicationBean.setSourceSystem(stagingUWApplicationBean.getSourceSystem());
		applicationBean.setDataOwner(stagingUWApplicationBean.getDataOwner());
		applicationBean.setOperatingCountry(stagingUWApplicationBean.getOperatingCountry());
		applicationBean.setPolicyholderBusId(stagingUWApplicationBean.getPolicyholderBusId());
		applicationBean.setPolicyholderName(stagingUWApplicationBean.getPolicyholderName());
		applicationBean.setMainAffiliateBusId(stagingUWApplicationBean.getMainAffiliateBusId());
		applicationBean.setMainAffiliateFirstName(stagingUWApplicationBean.getMainAffiliateFirstName());
		applicationBean.setMainAffiliateLastName(stagingUWApplicationBean.getMainAffiliateLastName());
		applicationBean.setMainAffiliateDateOfBirth(stagingUWApplicationBean.getMainAffiliateDateOfBirth());
		applicationBean.setMainAffiliateSSN(stagingUWApplicationBean.getMainAffiliateSSN());
		applicationBean.setMainAffiliateTown(stagingUWApplicationBean.getMainAffiliateTown());
		applicationBean.setMainAffiliatePostCode(stagingUWApplicationBean.getMainAffiliatePostCode());
		applicationBean.setMainAffiliateCountry(stagingUWApplicationBean.getMainAffiliateCountry());
		applicationBean.setSubmissionDate(stagingUWApplicationBean.getSubmissionDate());
		applicationBean.setIsActive(stagingUWApplicationBean.getIsActive());
		applicationBean.setDocSignId(stagingUWApplicationBean.getDocSignId());
		applicationBean.setSFSignedDate(stagingUWApplicationBean.getSFSignedDate());
		applicationBean.setCreatedBy(stagingUWApplicationBean.getCreatedBy());
		applicationBean.setCreationTimeStamp(stagingUWApplicationBean.getCreationTimeStamp());
		applicationBean.setLastUpdatedBy(stagingUWApplicationBean.getLastUpdatedBy());
		applicationBean.setLastUpdateTimeStamp(stagingUWApplicationBean.getLastUpdateTimeStamp());
		applicationBean.setDeletionTimeStamp(stagingUWApplicationBean.getDeletionTimeStamp());
		applicationBean.setLastSyncAction(stagingUWApplicationBean.getAction());
		
	}

	@Override
	public void setup(TriggerSetupContext aContext) {

	}

}
