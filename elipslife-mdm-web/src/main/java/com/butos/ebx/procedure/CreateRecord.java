package com.butos.ebx.procedure;

import java.util.*;
import java.util.Map.*;

import com.onwbp.adaptation.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;

/**
 *
 */
public class CreateRecord implements Procedure
{

	private final HashMap<Path, Object> columnValuesMap;
	private final AdaptationTable tableToCreateIn;
	private Adaptation createdRecord;

	public CreateRecord(
		final HashMap<Path, Object> pathToValuesMap,
		final AdaptationTable adapTable)
	{
		super();
		this.columnValuesMap = pathToValuesMap;
		this.tableToCreateIn = adapTable;
	}

	@Override
	public void execute(final ProcedureContext context) throws Exception
	{

		ValueContextForUpdate contextForNewOccurrence = context
			.getContextForNewOccurrence(this.tableToCreateIn);

		if (this.columnValuesMap != null)
		{
			Iterator<Entry<Path, Object>> iterator = this.columnValuesMap.entrySet().iterator();
			while (iterator.hasNext())
			{
				Entry<Path, Object> valueDef = iterator.next();
				//Path path = valueDef.getKey();
				//Object value = valueDef.getValue();
				contextForNewOccurrence.setValue(valueDef.getValue(), valueDef.getKey());
			}
		}
		context.setAllPrivileges(true);
		this.createdRecord = context.doCreateOccurrence(contextForNewOccurrence, this.tableToCreateIn);
		context.setAllPrivileges(false);

	}

	public HashMap<Path, Object> getColumnValuesMap()
	{
		return this.columnValuesMap;
	}

	public AdaptationTable getTableToCreateIn()
	{
		return this.tableToCreateIn;
	}
	
	public Object getCreatedRecordPrimaryKey()
	{
		return this.createdRecord.getOccurrencePrimaryKey().format();
	}
	
	public Adaptation getCreatedRecord()
	{
		return this.createdRecord;
	}
}
