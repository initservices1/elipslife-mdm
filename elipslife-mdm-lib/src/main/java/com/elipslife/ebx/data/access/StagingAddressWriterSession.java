package com.elipslife.ebx.data.access;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class StagingAddressWriterSession extends StagingAddressWriter {

	private final Session session;
	
	public StagingAddressWriterSession(Session session) {
		super();
		this.session = session;
	}
	
	public StagingAddressWriterSession(Session session, String dataSpace) {
        super(dataSpace);
        this.session = session;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		ProcedureResult procedureResult = ProgrammaticService.createForSession(this.session, this.dataSpace).execute(procedure);
		if(procedureResult.hasFailed()) {
			throw new RuntimeException(procedureResult.getException());
		}
	}
}