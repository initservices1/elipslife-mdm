package com.elipslife.mdm.contract.staging;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class StagingContractCoverWriterProcedureContext extends StagingContractCoverWriter {

	private ProcedureContext procedureContext;
	
	public StagingContractCoverWriterProcedureContext(ProcedureContext procedureContext) {
		this.procedureContext = procedureContext;
	}
	
	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}