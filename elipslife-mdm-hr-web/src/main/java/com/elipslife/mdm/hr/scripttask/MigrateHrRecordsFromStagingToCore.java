package com.elipslife.mdm.hr.scripttask;

import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.mdm.hr.common.HrCommonRules;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.elipslife.mdm.hr.staging.STG_BuildingBean;
import com.elipslife.mdm.hr.staging.STG_BuildingReader;
import com.elipslife.mdm.hr.staging.STG_BuildingWriter;
import com.elipslife.mdm.hr.staging.STG_BuildingWriterSession;
import com.elipslife.mdm.hr.staging.STG_EmployeeBean;
import com.elipslife.mdm.hr.staging.STG_EmployeeReader;
import com.elipslife.mdm.hr.staging.STG_EmployeeWriter;
import com.elipslife.mdm.hr.staging.STG_EmployeeWriterSession;
import com.elipslife.mdm.hr.staging.STG_OrganisationBean;
import com.elipslife.mdm.hr.staging.STG_OrganisationReader;
import com.elipslife.mdm.hr.staging.STG_OrganisationWriter;
import com.elipslife.mdm.hr.staging.STG_OrganisationWriterSession;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.boot.VM;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class MigrateHrRecordsFromStagingToCore extends ScriptTaskBean {

	private String tempDataspace;
	private String tableNamePath;
	private String errorMessage;
	private Boolean hasMinNoOfRecords;

	@Override
	public void executeScript(ScriptTaskBeanContext context) throws OperationException {

		try {
			if (tempDataspace != null && tableNamePath != null) {

				if (tableNamePath.equals(HrModelPaths._STG_Building.getPathInSchema().format()) && (hasMinNoOfRecords != null && hasMinNoOfRecords) ) {
					// migrating building records
					migrateBuildingFromStagingToCore(context);
				}

				if (tableNamePath.equals(HrModelPaths._STG_Organisation.getPathInSchema().format()) && (hasMinNoOfRecords != null && hasMinNoOfRecords)) {
					// Migrating Organisation records
					migrateOrganisationFromStagingToCore(context);
				}

				if (tableNamePath.equals(HrModelPaths._STG_Employee.getPathInSchema().format()) && (hasMinNoOfRecords != null && hasMinNoOfRecords)) {
					// Migrating Employee records
					migrateEmployeeFromStagingToCore(context);
				}

			}
		} catch (Exception ex) {
			errorMessage.concat(ex.getMessage());
		}

	}

	/*
	 * Migrating building records from staging to Core
	 * 
	 */
	public void migrateBuildingFromStagingToCore(ScriptTaskBeanContext context) {

		STG_BuildingWriter stgBuildingWriter = new STG_BuildingWriterSession(context.getSession(), tempDataspace);
		STG_BuildingReader stgBuildingReader = (STG_BuildingReader) stgBuildingWriter.getTableReader();

		List<STG_BuildingBean> stgBuildingBean = stgBuildingReader.readAllBeans();
		List<STG_BuildingBean> stgBuildingBeanOrder = stgBuildingBean.stream().sorted((a, b) -> DateNullableComparator
				.getInstance().compare(a.getLastUpdateTimeStamp(), b.getLastUpdateTimeStamp()))
				.collect(Collectors.toList());
		stgBuildingBeanOrder.forEach(sicb -> {
			try {

				boolean hasValidationErrors = stgBuildingReader.hasValidationErrors(new Object[] { sicb.getBuildingId() });
				if (hasValidationErrors) {
					VM.log.kernelWarn("Migration staging Building error with Organisation id " + sicb.getBuildingId());
					return;
				}
				HrCommonRules.createOrUpdateCoreBuildingRecordByStagingRecord(context.getSession(), stgBuildingWriter,
						tempDataspace, sicb);
			} catch (Exception ex) {
				errorMessage.concat(ex.getMessage());
				OperationException.createError(ex.getMessage());
			}
		});
	}

	/*
	 * Migrating Organisation records from staging to core
	 * 
	 */
	public void migrateOrganisationFromStagingToCore(ScriptTaskBeanContext context) {

		STG_OrganisationWriter stgOrganisationWriter = new STG_OrganisationWriterSession(context.getSession(),
				tempDataspace);
		STG_OrganisationReader stgOrganisationReader = (STG_OrganisationReader) stgOrganisationWriter.getTableReader();
		List<STG_OrganisationBean> stgOrganisationBean = stgOrganisationReader.readAllBeans();
		List<STG_OrganisationBean> stgOrganisationBeanOrder = stgOrganisationBean.stream()
				.sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getLastUpdateTimeStamp(),
						b.getLastUpdateTimeStamp()))
				.collect(Collectors.toList());
		stgOrganisationBeanOrder.forEach(sicb -> {
			try {
				boolean hasValidationErrors = stgOrganisationReader.hasValidationErrors(new Object[] { sicb.getOrganisationId() });
				if (hasValidationErrors) {
					VM.log.kernelWarn("Migration staging Organisation error with Organisation id " + sicb.getOrganisationId());
					return;
				}
				HrCommonRules.createOrUpdateCoreOrganisationRecordByStagingRecord(context.getSession(),
						stgOrganisationWriter, tempDataspace, sicb);
			} catch (Exception ex) {
				errorMessage.concat(ex.getMessage());
				OperationException.createError(ex.getMessage());
			}
		});
	}

	/*
	 * 
	 * Migrate Employee table from Staging to Core
	 */
	public void migrateEmployeeFromStagingToCore(ScriptTaskBeanContext context) {

		STG_EmployeeWriter stgEmployeeWriter = new STG_EmployeeWriterSession(context.getSession(), tempDataspace);
		STG_EmployeeReader stgEmployeeReader = (STG_EmployeeReader) stgEmployeeWriter.getTableReader();
		List<STG_EmployeeBean> stgEmployeeBean = stgEmployeeReader.readAllBeans();
		List<STG_EmployeeBean> stgEmployeeBeansInOrder = stgEmployeeBean.stream()
				.sorted((a, b) -> DateNullableComparator.getInstance().compare(a.getLastUpdateTimeStamp(),
						b.getLastUpdateTimeStamp()))
				.collect(Collectors.toList());
		stgEmployeeBeansInOrder.forEach(sicb -> {
			try {
				boolean hasValidationErrors = stgEmployeeReader.hasValidationErrors(new Object[] { sicb.getEmployeeId() });
				if (hasValidationErrors) {
					VM.log.kernelWarn("Migration staging Employee error with Organisation id " + sicb.getEmployeeId());
					return;
				}
				HrCommonRules.createOrUpdateCoreEmployeeRecordByStagingRecord(context.getSession(), stgEmployeeWriter,
						tempDataspace, sicb);
			} catch (Exception ex) {
				errorMessage.concat(ex.getMessage());
				OperationException.createError(ex.getMessage());
			}
		});

	}

	public String getTableNamePath() {
		return tableNamePath;
	}

	public void setTableNamePath(String tableNamePath) {
		this.tableNamePath = tableNamePath;
	}

	public String getTempDataspace() {
		return tempDataspace;
	}

	public void setTempDataspace(String tempDataspace) {
		this.tempDataspace = tempDataspace;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Boolean getHasMinNoOfRecords() {
		return hasMinNoOfRecords;
	}

	public void setHasMinNoOfRecords(Boolean hasMinNoOfRecords) {
		this.hasMinNoOfRecords = hasMinNoOfRecords;
	}

}
