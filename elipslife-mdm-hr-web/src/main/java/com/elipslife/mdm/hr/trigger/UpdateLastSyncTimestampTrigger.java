package com.elipslife.mdm.hr.trigger;

import java.util.Date;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public class UpdateLastSyncTimestampTrigger extends TableTrigger{
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		super.handleBeforeCreate(aContext);
		
		// Update LastSyncTimestamp
		aContext.getOccurrenceContextForUpdate().setValue(new Date(), Path.parse("./Control/LastSyncTimestamp"));
	}

	@Override
	public void setup(TriggerSetupContext aContext) {
		
	}

}
