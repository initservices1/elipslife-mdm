package com.elipslife.mdm.claimcase.staging;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class StagingClaimCaseWriterProcedureContext extends StagingClaimCaseWriter {
	private final ProcedureContext procedureContext;

	public StagingClaimCaseWriterProcedureContext(ProcedureContext procedureContext) {
		super();
		this.procedureContext = procedureContext;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
