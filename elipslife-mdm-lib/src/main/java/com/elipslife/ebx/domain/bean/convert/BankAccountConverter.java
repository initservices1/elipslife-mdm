package com.elipslife.ebx.domain.bean.convert;

import java.util.Objects;

import com.elipslife.ebx.domain.bean.CoreBankAccountBean;
import com.elipslife.ebx.domain.bean.LandingBankAccountBean;
import com.elipslife.ebx.domain.bean.LandingBankAccountBulkProcessBean;
import com.elipslife.ebx.domain.bean.StagingBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;

public class BankAccountConverter {

    public static StagingBankAccountBean createStagingBankAccountBeanFromLandingBankAccountBulkProcessBean(LandingBankAccountBulkProcessBean landingBankAccountBean) {

        StagingBankAccountBean stagingBankAccountBean = new StagingBankAccountBean();
        fillStagingBankAccountBeanWithLandingBankAccountBean(landingBankAccountBean, stagingBankAccountBean);
        return stagingBankAccountBean;
    }
    
    public static void fillStagingBankAccountBeanWithLandingBankAccountBean(LandingBankAccountBulkProcessBean landingBankAccountBean,
        StagingBankAccountBean stagingBankAccountBean) {

        stagingBankAccountBean.setAccountType(landingBankAccountBean.getAccountType());
        stagingBankAccountBean.setAccountNumber(landingBankAccountBean.getAccountNumber());
        stagingBankAccountBean.setIban(landingBankAccountBean.getIban());
        stagingBankAccountBean.setBic(landingBankAccountBean.getBic());
        stagingBankAccountBean.setClearingNumber(landingBankAccountBean.getClearingNumber());
        stagingBankAccountBean.setBankName(landingBankAccountBean.getBankName());
        stagingBankAccountBean.setCountry(landingBankAccountBean.getCountry());
        stagingBankAccountBean.setCurrency(landingBankAccountBean.getCurrency());
        stagingBankAccountBean.setIsMainAccount(landingBankAccountBean.getIsMainAccount());
        stagingBankAccountBean.setSourceBankAccountBusId(landingBankAccountBean.getSourceBankAccountBusId());
        stagingBankAccountBean.setCorrelationId(landingBankAccountBean.getCorrelationId()); // set correlation id
        stagingBankAccountBean.setSourceSystem(landingBankAccountBean.getSourceSystem());
        stagingBankAccountBean.setSourcePartyBusId(landingBankAccountBean.getSourcePartyBusId());
        stagingBankAccountBean.setValidFrom(landingBankAccountBean.getValidFrom());
        stagingBankAccountBean.setValidTo(landingBankAccountBean.getValidTo());
        stagingBankAccountBean.setCreationTimestamp(landingBankAccountBean.getCreationTimestamp());
        stagingBankAccountBean.setLastUpdateTimestamp(landingBankAccountBean.getLastUpdateTimestamp());
        stagingBankAccountBean.setAction(landingBankAccountBean.getAction());
        stagingBankAccountBean.setActionBy(landingBankAccountBean.getActionBy());
    }

    /**
     * 
     * @param landingBankAccountBean
     * @return
     */
    public static StagingBankAccountBean createStagingBankAccountBeanFromLandingBankAccountBean(LandingBankAccountBean landingBankAccountBean) {

        StagingBankAccountBean stagingBankAccountBean = new StagingBankAccountBean();
        fillStagingBankAccountBeanWithLandingBankAccountBean(landingBankAccountBean, stagingBankAccountBean);
        return stagingBankAccountBean;
    }


    /**
     * 
     * @param landingBankAccountBean
     * @param stagingBankAccountBean
     */
    public static void fillStagingBankAccountBeanWithLandingBankAccountBean(LandingBankAccountBean landingBankAccountBean,
        StagingBankAccountBean stagingBankAccountBean) {

        stagingBankAccountBean.setAccountType(landingBankAccountBean.getAccountType());
        stagingBankAccountBean.setAccountNumber(landingBankAccountBean.getAccountNumber());
        stagingBankAccountBean.setIban(landingBankAccountBean.getIban());
        stagingBankAccountBean.setBic(landingBankAccountBean.getBic());
        stagingBankAccountBean.setClearingNumber(landingBankAccountBean.getClearingNumber());
        stagingBankAccountBean.setBankName(landingBankAccountBean.getBankName());
        stagingBankAccountBean.setCountry(landingBankAccountBean.getCountry());
        stagingBankAccountBean.setCurrency(landingBankAccountBean.getCurrency());
        stagingBankAccountBean.setIsMainAccount(landingBankAccountBean.getIsMainAccount());
        stagingBankAccountBean.setCorrelationId(landingBankAccountBean.getCorrelationId());  // set correlation id
        stagingBankAccountBean.setSourceBankAccountBusId(landingBankAccountBean.getSourceBankAccountBusId());
        stagingBankAccountBean.setSourceSystem(landingBankAccountBean.getSourceSystem());
        stagingBankAccountBean.setSourcePartyBusId(landingBankAccountBean.getSourcePartyBusId());
        stagingBankAccountBean.setValidFrom(landingBankAccountBean.getValidFrom());
        stagingBankAccountBean.setValidTo(landingBankAccountBean.getValidTo());
        stagingBankAccountBean.setCreationTimestamp(landingBankAccountBean.getCreationTimestamp());
        stagingBankAccountBean.setLastUpdateTimestamp(landingBankAccountBean.getLastUpdateTimestamp());
        stagingBankAccountBean.setAction(landingBankAccountBean.getAction());
        stagingBankAccountBean.setActionBy(landingBankAccountBean.getActionBy());
    }


    /**
     * 
     * @param stagingBankAccountBean
     * @return
     */
    public static CoreBankAccountBean createCoreBankAccountBeanFromStagingBankAccountBean(StagingBankAccountBean stagingBankAccountBean) {

        CoreBankAccountBean coreBankAccountBean = new CoreBankAccountBean();
        fillCoreBankAccountBeanWithStagingBankAccountBean(stagingBankAccountBean, coreBankAccountBean);
        return coreBankAccountBean;
    }


    /**
     * 
     * @param stagingBankAccountBean
     * @param coreBankAccountBean
     */
    public static void fillCoreBankAccountBeanWithStagingBankAccountBean(StagingBankAccountBean stagingBankAccountBean,
        CoreBankAccountBean coreBankAccountBean) {

//		coreBankAccountBean.setPartyId(stagingBankAccountBean.getPartyId());
        coreBankAccountBean.setAccountType(stagingBankAccountBean.getAccountType());
        coreBankAccountBean.setAccountNumber(stagingBankAccountBean.getAccountNumber());
        coreBankAccountBean.setIban(stagingBankAccountBean.getIban());
        coreBankAccountBean.setBic(stagingBankAccountBean.getBic());
        coreBankAccountBean.setClearingNumber(stagingBankAccountBean.getClearingNumber());
        coreBankAccountBean.setBankName(stagingBankAccountBean.getBankName());
        coreBankAccountBean.setCountry(stagingBankAccountBean.getCountry());
        coreBankAccountBean.setCurrency(stagingBankAccountBean.getCurrency());
        coreBankAccountBean.setIsMainAccount(stagingBankAccountBean.getIsMainAccount());
        coreBankAccountBean.setSourceBankAccountBusId(stagingBankAccountBean.getSourceBankAccountBusId());
        coreBankAccountBean.setValidFrom(stagingBankAccountBean.getValidFrom());
        coreBankAccountBean.setValidTo(stagingBankAccountBean.getValidTo());
//		coreBankAccountBean.setIsExpired(stagingBankAccountBean.getIsExpired());
        coreBankAccountBean.setSourceSystem(stagingBankAccountBean.getSourceSystem());
        coreBankAccountBean.setSourcePartyBusId(stagingBankAccountBean.getSourcePartyBusId());

        if (Objects.equals(stagingBankAccountBean.getAction(), PartyConstants.RecordOperations.CREATE)) {
            coreBankAccountBean.setCreatedBy(stagingBankAccountBean.getActionBy());
        }

        coreBankAccountBean.setCreationTimestamp(stagingBankAccountBean.getCreationTimestamp());

        if (Objects.equals(stagingBankAccountBean.getAction(), PartyConstants.RecordOperations.UPDATE)) {
            coreBankAccountBean.setLastUpdatedBy(stagingBankAccountBean.getActionBy());
        }

        coreBankAccountBean.setLastUpdateTimestamp(stagingBankAccountBean.getLastUpdateTimestamp());
        coreBankAccountBean.setLastSyncAction(stagingBankAccountBean.getAction());
//		coreBankAccountBean.setLastSyncTimestamp(stagingBankAccountBean.getLastSyncTimestamp());
    }
}
