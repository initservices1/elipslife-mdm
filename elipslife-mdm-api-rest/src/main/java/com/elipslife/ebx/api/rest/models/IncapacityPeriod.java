package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.json.bind.annotation.JsonbProperty;

public class IncapacityPeriod extends Address {
	
	@JsonbProperty("StartDate")
	public LocalDate startDate;
	
	@JsonbProperty("EndDate")
	public LocalDate endDate;
	
	@JsonbProperty("DisabilityDegree")
	public String disabilityDegree;
	
	@JsonbProperty("DisabilityDegreeNotes")
	public String disabilityDegreeNotes;
	
	@JsonbProperty("WorkIncapacityDegree")
	public String workIncapacityDegree;
	
	@JsonbProperty("WorkIncapacityDegreeNotes")
	public String workIncapacityDegreeNotes;
	
	@JsonbProperty("CreationTimestamp")
	public LocalDateTime creationTimestamp;
	
	@JsonbProperty("LastUpdateTimestamp")
	public LocalDateTime lastUpdateTimestamp;
	
	@JsonbProperty("Action")
	public String action;
	
	@JsonbProperty("ActionTimestamp")
	public LocalDateTime actionTimestamp;
}