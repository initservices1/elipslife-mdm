package com.elipslife.mdm.party.scheduler;

import com.elipslife.ebx.data.access.CoreBankAccountBuilder;
import com.elipslife.ebx.data.access.CoreBankAccountWriterSession;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;

public class MarkExpiredBankAccountsTask extends ScheduledTask {

	@Override
	public void execute(ScheduledExecutionContext scheduledExecutionContext) {
		
		CoreBankAccountBuilder.markExpiredBankAccounts(new CoreBankAccountWriterSession(scheduledExecutionContext.getSession(), true));
	}
}