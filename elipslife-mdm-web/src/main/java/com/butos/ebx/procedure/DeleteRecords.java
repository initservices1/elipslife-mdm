package com.butos.ebx.procedure;

import java.util.*;

import com.onwbp.adaptation.*;
import com.onwbp.boot.*;
import com.orchestranetworks.service.*;

public class DeleteRecords implements Procedure
{

	private ArrayList<Adaptation> recordsToDelete;
	private Adaptation recordToDelete;

	public DeleteRecords(final Adaptation recordToDelete)
	{
		super();
		this.recordToDelete = recordToDelete;
	}

	public DeleteRecords(final ArrayList<Adaptation> recordsToDelete)
	{
		super();
		this.recordsToDelete = recordsToDelete;
	}

	@Override
	public void execute(final ProcedureContext context) throws Exception
	{
		context.setAllPrivileges(true);
		if (this.recordToDelete != null)
		{

			context.doDelete(this.recordToDelete.getAdaptationName(), true);
		}

		else if (this.recordsToDelete != null)
		{
			for (Object element : this.recordsToDelete)
			{
				Adaptation record = (Adaptation) element;
				context.doDelete(record.getAdaptationName(), true);
			}
		}
		
		else
		{
			VM.log.kernelWarn(" ****************** No Record(s) to Delete ******************");
		}
		context.setAllPrivileges(false);
	}
	public ArrayList<Adaptation> getRecordsToDelete()
	{
		return this.recordsToDelete;
	}
	/*	public void setRecordsToDelete(final ArrayList<Adaptation> recordsToDelete)
		{
			this.recordsToDelete = recordsToDelete;
		}*/

	public Adaptation getRecordToDelete()
	{
		return this.recordToDelete;
	}
	/*	public void setRecordToDelete(final Adaptation recordToDelete)
		{
			this.recordToDelete = recordToDelete;
		}*/

}
