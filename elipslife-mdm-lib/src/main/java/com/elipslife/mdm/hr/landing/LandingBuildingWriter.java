package com.elipslife.mdm.hr.landing;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.LandingHrModelPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class LandingBuildingWriter extends AbstractTableWriter<LandingBuildingBean> {
    
	private LandingBuildingReader reader;

    public LandingBuildingWriter() {
        this(Constants.DataSpace.LANDING_HR_REFERENCE);
    }

    public LandingBuildingWriter(String dataspace) {
        this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.LANDING_HR_MASTER),
				LandingHrModelPaths._Building.getPathInSchema()));
    }

    public LandingBuildingWriter(AdaptationTable table) {
        super(table);
        reader = new LandingBuildingReader(table);
    }

    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, LandingBuildingBean bean) {
        values.put(LandingHrModelPaths._Building._Building_BuildingId, bean.getBuildingId());
        values.put(LandingHrModelPaths._Building._Building_DataOwner, bean.getDataOwner());
        values.put(LandingHrModelPaths._Building._Building_BuildingBusId, bean.getBuildingBusId());
        values.put(LandingHrModelPaths._Building._Building_Name, bean.getName());
        values.put(LandingHrModelPaths._Building._Building_Street, bean.getStreet());
        values.put(LandingHrModelPaths._Building._Building_Street2, bean.getStreet2());
        values.put(LandingHrModelPaths._Building._Building_PostCode, bean.getPostCode());
        values.put(LandingHrModelPaths._Building._Building_POBox, bean.getPOBox());
        values.put(LandingHrModelPaths._Building._Building_POBoxPostCode, bean.getPOBoxPostCode());
        values.put(LandingHrModelPaths._Building._Building_POBoxTown, bean.getPOBoxTown());
        values.put(LandingHrModelPaths._Building._Building_Town, bean.getTown());
        values.put(LandingHrModelPaths._Building._Building_District, bean.getDistrict());
        values.put(LandingHrModelPaths._Building._Building_StateProvince, bean.getStateProvince());
        values.put(LandingHrModelPaths._Building._Building_Country, bean.getCountry());
        values.put(LandingHrModelPaths._Building._Building_IsActive, bean.getIsActive());
        values.put(LandingHrModelPaths._Building._Control_SourceSystem, bean.getSourceSystem());
        values.put(LandingHrModelPaths._Building._Control_CreatedBy, bean.getCreatedBy());
        values.put(LandingHrModelPaths._Building._Control_CreationTimestamp, bean.getCreationTimeStamp());
        values.put(LandingHrModelPaths._Building._Control_LastUpdatedBy, bean.getLastUpdatedBy());
        values.put(LandingHrModelPaths._Building._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
        
    }

    @Override
    public AbstractTableReader<LandingBuildingBean> getTableReader() {
        return reader;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub
		
	}
}
