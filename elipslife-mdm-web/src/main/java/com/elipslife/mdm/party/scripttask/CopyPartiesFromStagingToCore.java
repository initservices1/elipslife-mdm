package com.elipslife.mdm.party.scripttask;

import java.util.HashMap;
import java.util.Iterator;

import com.elipslife.ebx.data.access.CorePartyBuilder;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

public class CopyPartiesFromStagingToCore extends ScriptTask {
    
    private static AdaptationHome stagingDataSpaceHome;
    private static Adaptation stagingDataSet;

    @Override
    public void executeScript(ScriptTaskContext aContext) throws OperationException {

        HashMap<String, String> contextVariables = getContextVariables(aContext);
        String stagingDataspace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
        String commitSize = contextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);
        
        Repository reposity = aContext.getRepository();
        setStagingDataSpaceHome(reposity.lookupHome(HomeKey.forBranchName(stagingDataspace)));
        setStagingDataSet(getStagingDataSpaceHome());
        
        try {
            CorePartyBuilder.createOrUpdateCorePartyByStagingPartyInBulkProcessing(aContext.getSession(), stagingDataspace, commitSize, getStagingDataSpaceHome(), getStagingDataSet());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static HashMap<String, String> getContextVariables(com.orchestranetworks.workflow.DataContextReadOnly dataContext) {

        HashMap<String, String> hashMap = new HashMap<String, String>();
        Iterator<String> variableNames = dataContext.getVariableNames();
        while (variableNames.hasNext()) {
            String variableName = variableNames.next();
            String variableValue = dataContext.getVariableString(variableName);
            hashMap.put(variableName, variableValue);
        }

        return hashMap;
    }    
    
    public static AdaptationHome getStagingDataSpaceHome() {
        return stagingDataSpaceHome;
    }
    
    public void setStagingDataSpaceHome(AdaptationHome stagingDataSpaceHome) {
        this.stagingDataSpaceHome = stagingDataSpaceHome;
    }
    
    public static Adaptation getStagingDataSet() {
        return stagingDataSet;
    }
    
    public void setStagingDataSet(AdaptationHome stagingDataspace) {
        this.stagingDataSet = stagingDataspace.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.PARTY_MASTER));;
    }
}
