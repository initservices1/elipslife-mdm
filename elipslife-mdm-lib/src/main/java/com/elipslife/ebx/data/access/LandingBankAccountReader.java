package com.elipslife.ebx.data.access;

import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.LandingBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.xpath.filter.XPathPredicate;

public class LandingBankAccountReader extends BaseBankAccountReader<LandingBankAccountBean> {

	public LandingBankAccountReader() {
		this(PartyConstants.DataSpace.LANDING_REFERENCE);
	}
	
	public LandingBankAccountReader(String dataSpace) {
		super(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_BankAccount.getPathInSchema()));
	}
	
	
	public LandingBankAccountReader(AdaptationTable table) {
		super(table);
	}
	

	@Override
	public LandingBankAccountBean createBeanFromRecord(Adaptation record) {
		
		LandingBankAccountBean landingBankAccountBean = new LandingBankAccountBean();
		
		landingBankAccountBean.setId(record.get_int(LandingPaths._LDG_BankAccount._Account_BankAccountId));
		
		landingBankAccountBean.setAccountType(record.getString(LandingPaths._LDG_BankAccount._Account_AccountType));
		landingBankAccountBean.setAccountNumber(record.getString(LandingPaths._LDG_BankAccount._Account_AccountNumber));
		landingBankAccountBean.setIban(record.getString(LandingPaths._LDG_BankAccount._Account_IBAN));
		landingBankAccountBean.setBic(record.getString(LandingPaths._LDG_BankAccount._Account_BIC));
		landingBankAccountBean.setClearingNumber(record.getString(LandingPaths._LDG_BankAccount._Account_ClearingNumber));
		landingBankAccountBean.setBankName(record.getString(LandingPaths._LDG_BankAccount._Account_BankName));
		landingBankAccountBean.setCountry(record.getString(LandingPaths._LDG_BankAccount._Account_Country));
		landingBankAccountBean.setCurrency(record.getString(LandingPaths._LDG_BankAccount._Account_Currency));
		landingBankAccountBean.setIsMainAccount((Boolean)record.get(LandingPaths._LDG_BankAccount._Account_IsMainAccount));
		landingBankAccountBean.setCorrelationId(record.getString(LandingPaths._LDG_BankAccount._Account_CorrelationId)); // Correlation Id
		landingBankAccountBean.setLandingPartyId(record.getString(LandingPaths._LDG_BankAccount._Account_LandingPartyId)); // Landing Party Id
		landingBankAccountBean.setMdmStagingProcessResponse(record.getString(LandingPaths._LDG_BankAccount._Account_MdmStagingProcessResponse)); // MDM Staging Process Response
		landingBankAccountBean.setSourceBankAccountBusId(record.getString(LandingPaths._LDG_BankAccount._Account_SourceBankAccountBusId));
		landingBankAccountBean.setSourceSystem(record.getString(LandingPaths._LDG_BankAccount._Control_SourceSystem));
		landingBankAccountBean.setSourcePartyBusId(record.getString(LandingPaths._LDG_BankAccount._Control_SourcePartyBusId));
		landingBankAccountBean.setValidFrom(record.getDate(LandingPaths._LDG_BankAccount._Control_ValidFrom));
		landingBankAccountBean.setValidTo(record.getDate(LandingPaths._LDG_BankAccount._Control_ValidTo));
		landingBankAccountBean.setCreationTimestamp(record.getDate(LandingPaths._LDG_BankAccount._Control_CreationTimestamp));
		landingBankAccountBean.setLastUpdateTimestamp(record.getDate(LandingPaths._LDG_BankAccount._Control_LastUpdateTimestamp));
		landingBankAccountBean.setAction(record.getString(LandingPaths._LDG_BankAccount._Control_Action));
		landingBankAccountBean.setActionBy(record.getString(LandingPaths._LDG_BankAccount._Control_ActionBy));
		
		return landingBankAccountBean;
	}
	
	/**
	 * Find Landing Bank Account beans by Landing PartyId
	 * 
	 * @param sourcePartyId
	 * @return
	 */
	public List<LandingBankAccountBean> findBeansByLandingPartyId(String sourcePartyId) {
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, LandingPaths._LDG_BankAccount._Account_LandingPartyId, sourcePartyId);
		return readRecordsFor(predicate1.toString()).stream().map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}

	
	@Override
	protected Path[] getPkPaths() {
		
		return new Path[] { LandingPaths._LDG_BankAccount._Account_BankAccountId };
	}

	
	@Override
	protected Object[] retrievePks(LandingBankAccountBean bean) {

		return new Object[] { bean.getId() };
	}
	

	@Override
	protected Path getPathBankAccountId() {

		return LandingPaths._LDG_BankAccount._Account_BankAccountId;
	}
	

	@Override
	protected Path getPathSourceBankAccountBusId() {

		return LandingPaths._LDG_BankAccount._Account_SourceBankAccountBusId;
	}
	

	@Override
	protected Path getPathControlSourceSystem() {

		return LandingPaths._LDG_BankAccount._Control_SourceSystem;
	}
	

	@Override
	protected Path getPathControlSourcePartyBusId() {

		return LandingPaths._LDG_BankAccount._Control_SourcePartyBusId;
	}

	
	@Override
	protected Path getPathControlValidFrom() {

		return LandingPaths._LDG_BankAccount._Control_ValidFrom;
	}
	

	@Override
	protected Path getPathControlValidTo() {

		return LandingPaths._LDG_BankAccount._Control_ValidTo;
	}
}