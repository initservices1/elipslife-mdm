package com.elipslife.ebx.data.access;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public abstract class BaseBeanReader<T> extends AbstractTableReader<T> {

	protected SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	protected abstract Path getPathControlSourceSystem();
	protected abstract Path getPathControlSourcePartyBusId();
	protected abstract Path getPathControlValidFrom();
	protected abstract Path getPathControlValidTo();
	
	protected BaseBeanReader(AdaptationTable table) {
		super(table);
	}
	
	
	/**
	 * Find beans where the actual date is greater or equal to the valid from and less or equal to the valid to date
	 * @return
	 */
	public List<T> findBeansUpToDate() {
		
		Date dateNow = new Date();
		
		return findBeansByDate(dateNow);
	}


	/**
	 * Find beans where the date is greater or equal to the valid from and less or equal to the valid to date
	 * @param date
	 * @return
	 */
	public List<T> findBeansByDate(Date date) {
		
		date = removeTime(date);

		String dateNowString = simpleDateFormat.format(date);
		
		XPathPredicateGroup predicate1 = new XPathPredicateGroup(LinkingType.Or);
		
		XPathPredicate predicate12 = new XPathPredicate(XPathPredicate.Operation.DateEqual, getPathControlValidFrom(), dateNowString);
		predicate1.addPredicate(predicate12);
		
		XPathPredicate predicate13 = new XPathPredicate(XPathPredicate.Operation.IsNull, getPathControlValidFrom(), null);
		predicate1.addPredicate(predicate13);
		
		XPathPredicateGroup predicate2 = new XPathPredicateGroup(LinkingType.Or);
		
		XPathPredicate predicate21 = new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, getPathControlValidTo(), dateNowString);
		predicate2.addPredicate(predicate21);
		
		XPathPredicate predicate22 = new XPathPredicate(XPathPredicate.Operation.DateEqual, getPathControlValidTo(), dateNowString);
		predicate2.addPredicate(predicate22);
		
		XPathPredicate predicate23 = new XPathPredicate(XPathPredicate.Operation.IsNull, getPathControlValidTo(), null);
		predicate2.addPredicate(predicate23);
		
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicateGroup(predicate1);
		group.addPredicateGroup(predicate2);
		
		return readRecordsFor(group.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}

	
	/**
	 * 
	 * @return
	 */
	public List<T> findBeansExpired() {
		
		Date date = new Date();
		
		date = removeTime(date);

		XPathPredicateGroup groupValidDate = new XPathPredicateGroup(LinkingType.And);
		String dateNowString = simpleDateFormat.format(date);
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.DateLessThan, getPathControlValidFrom(), dateNowString);
		groupValidDate.addPredicate(predicate1);
	
		XPathPredicate predicate2 = new XPathPredicate(XPathPredicate.Operation.DateLessThan, getPathControlValidTo(), dateNowString);
		groupValidDate.addPredicate(predicate2);
		
		return readRecordsFor(groupValidDate.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
	
	/**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}
	
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	protected static Date removeTime(Date date) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return calendar.getTime();
	}
}