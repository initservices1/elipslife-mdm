package com.elipslife.ebx.data.access;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class CoreAddressWriterSession extends CoreAddressWriter {

    private final Session session;

    public CoreAddressWriterSession(Session session, boolean useDefaultValues) {
        super(useDefaultValues);
        this.session = session;
    }

    public CoreAddressWriterSession(Session session, boolean useDefaultValues, String dataSpace) {
        super(useDefaultValues, dataSpace);
        this.session = session;
    }

    @Override
    protected void executeProcedure(Procedure procedure) {
        ProcedureResult procedureResult = ProgrammaticService.createForSession(this.session, this.dataSpace).execute(procedure);
        if (procedureResult.hasFailed()) {
            throw new RuntimeException(procedureResult.getException());
        }
    }
}
