package com.butos.ebx.smart.scripttasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.StringUtils;

import ch.butos.ebx.lib.legacy.wrapper.procedure.DeleteRecordsProcedure;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class DeleteTableRecordsTask extends ScriptTaskBean {

	private String dataspace;
	private String dataset;
	private String dataTablePath;
	private String errorMessage;
	private String isDeletion;
	
	public String getDataspace() {
		return dataspace;
	}

	public void setDataspace(String dataspace) {
		this.dataspace = dataspace;
	}

	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getDataTablePath() {
		return dataTablePath;
	}

	public void setDataTablePath(String dataTablePath) {
		this.dataTablePath = dataTablePath;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}	

	public String getIsDeletion() {
		return isDeletion;
	}

	public void setIsDeletion(String isDeletion) {
		this.isDeletion = isDeletion;
	}

	@Override
	public void executeScript(ScriptTaskBeanContext context) throws OperationException {
		
		if(StringUtils.equals(isDeletion, "1")) {
			
			AdaptationHome adaptationHomeDataSpace = RepositoryHelper.getDataSpace(dataspace);
			
			Adaptation adaptationDataSet = RepositoryHelper.getDataSet(adaptationHomeDataSpace, dataset);

			AdaptationTable table = RepositoryHelper.getTable(adaptationDataSet, dataTablePath);

			
			// Workaround because EBX sometimes does not delete all records
			int numberOfRecords = 0;
			
			do {
				
				Request request = table.createRequest();

				RequestResult requestResult = request.execute();

				List<Adaptation> records = new ArrayList<Adaptation>();

				Adaptation adaptation = null;
				while((adaptation = requestResult.nextAdaptation()) != null) {

					records.add(adaptation);
				}
				requestResult.close();
				
				numberOfRecords = records.size();
				
				DeleteRecordsProcedure deleteRecordsProcedure = new DeleteRecordsProcedure(records, true, false);

				ProgrammaticService programmaticService = ProgrammaticService.createForSession(context.getSession(), adaptationHomeDataSpace);
				
				ProcedureResult procedureResult = programmaticService.execute(deleteRecordsProcedure);
				
				if(procedureResult.hasFailed()) {
					errorMessage.concat(procedureResult.getException().getMessage());
					throw new RuntimeException(procedureResult.getException());
				}
			}
			while(numberOfRecords > 0);
		}
	}
}