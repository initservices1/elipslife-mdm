package com.elipslife.mdm.claimcase.landing;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class LandingClaimCaseBean extends Entity<Integer> {
	
	// ClaimCase_General
	private String dataOwner;
	private String claimCaseBusId;
	private String claimCaseExtRef;
	private String contractNumber;
	private String sourceContractBusId;
	private String operatingCountry;
	private String businessScope;
	private String claimType;
	private String claimState;
	private String claimStateDetail;
	private Date claimDate;
	private Date startDate;
	private Date endDate;
	private Date reportingDate;
	private Boolean isActive;
	
	// Party
	private String clientSegment;
	private String policyHolderBusId;
	private String sourcePolicyHolderBusId;
	private String mainAffiliateBusId;
	private String sourceMainAffiliateBusId;
	private String insuredPersonBusId;
	private String insuredPersonType;
	private String sourceInsuredPersonBusId;
	private String claimManagerUserName;
	private String sourceClaimManagerBusId;
	private String claimMgtmTeamBusId;
	private String sourceClaimMgmtTeamBusId;

	// ClaimTypeDisability
	private String assessmentState;
	private Date benefitStartDate;
	private Date benefitEndDate;
	private Date cancellationDate;
	private Date certifiedEndDate;
	private String claimCause;
	private Date diagnosisDate;
	private Date disabilityDate;
	private Date hospitalStartDate;
	private Date lastDayWorked;
	private Date reactivationDate;
	private Date returnToWorkDate;
	private Date symptomsStartDate;
	private Date terminationdate;
	private Date treatmentStartDate;

	// ClaimtypeAbsence_US_AbsenceGeneral
	private String absenceReason;
	private String absenceDurationType;
	private String absenceClaimState;

	// ClaimtypeAbsence_US_AbsenceAdoption
	private String adoptionType;
	private Date adoptionDate;
	private Boolean isAdoptionCourtCase;
	private Date adoptionCourtDate;
	private Integer adoptionCourtHours;

	// ClaimtypeAbsence_US_AbsenceBereavement
	private String bereaverRelationship;
	private String bereaverRelationshipNotes;
	private Date bereavementDeathDate;

	// ClaimtypeAbsence_US_AbsenceBonding
	private String bondingParentType;
	private Date bondingEstimatedDeliveryDate;
	private Integer bondingNumDelivered;
	private Boolean isBondingPartnerEmployerSame;
	private Boolean isPartnerBonding;
	private String bondingPartnerName;
	private Date bondingPartnerStartDate;
	private Date bondingPartnerEndDate;
	private Date bondingLastDisabilityDate;

	// ClaimtypeAbsence_US_AbsenceBoneMarrow
	private Boolean isBoneMarrowMedicalApptCase;
	private Date boneMarrowMedicalApptDate;
	private Integer boneMarrowMedicalApptHours;

	// ClaimtypeAbsence_US_AbsenceChildBirth
	private Date childbirthDate;
	private Date childbirthEstimatedDate;
	private Integer childbirthNumDelivered;
	private String childbirthDeliveryType;
	private Boolean isChildbirthBondingTimeCase;
	private Boolean isChildbirthComplicationCase;
	private Boolean isChildbirthPartnerBonding;
	private Boolean isChildbirthPartnerEmployerSame;
	private String childbirthPartnerName;
	private Date childbirthPartnerBondingStartDate;
	private Date childbirthPartnerBondingEndDate;
	private Date childbirthLastDisabilityDate;
	private String childbirthTreatingDoctorName;

	// ClaimtypeAbsence_US_AbsenceFamilyMember
	private String familyMemberName;
	private String familyMemberRelationship;
	private String familyMemberRelationshipNotes;
	private Boolean isFamilyFollowUpApptCase;
	private Boolean isFamilyHospitalCase;
	private Boolean isFamilyMemberADAQualified;
	private Boolean isFamilyMemberOver18;
	private Boolean isfamilyMemberConditionNonSerious;
	private Date familyChildBirthDate;
	private String familyMemberHealthConditionNotes;
	private Date familyIllnessStartDate;
	private Date familyHospitalStartDate;
	private Date familyHospitalEndDate;
	private Date familyTreatmentStartDate;
	private Date familyFollowUpApptDate;
	private String familyTreatingDoctorName;

	// ClaimtypeAbsence_US_AbsenceMilitaryLeave
	private String militaryLeaveType;
	private String militaryMemberType;
	private String militaryMemberRelationship;
	private String militaryMemberRelationshipNotes;

	// ClaimtypeAbsence_US_AbsenceOrganDonation
	private String organDonating;
	private Boolean isOrganDonorMedicalApptCase;
	private Date organDonorMedicalApptDate;
	private Integer organDonorMedicalApptHours;

	// ClaimtypeAbsence_US_AbsenceOwnIllness
	private String ownIllnessInjuryType;
	private boolean isOwnIllnessHospitalCase;
	private boolean isOwnIllnessFollowUpApptCase;
	private boolean isOwnIllnessWorkRelated;
	private boolean isOwnMentalIllnesCase;
	private Date ownIllnessStartDate;
	private Date ownIllnessHospitalStartDate;
	private Date ownIllnessHospitalEndDate;
	private Date ownIllnessFollowUpApptDate;
	private Date ownIllnessTreatmentStartDate;
	private String ownIllnessTreatingDoctorName;

	// ClaimtypeAbsence_US_AbsencePersonalCrime
	private String personalCrimeType;
	private String personalCrimeTypeNotes;
	private String personalCrimeRelationship;
	private String personalCrimeRelationshipNotes;
	private Boolean isPersonalCrimeCounsellingCase;
	private Boolean isPersonalCrimeCourtCase;
	private Boolean isPersonalCrimeDoctorCase;
	private Boolean isPersonalCrimeHospitalCase;
	private Boolean isPersonalCrimeLegalAssistCase;
	private Boolean isPersonalCrimeRelocationCase;
	private Boolean isPersonalCrimeRestrainingOrderCase;
	private Boolean isPersonalCrimeVictimServicesCase;
	private Date personalCrimeHospitalStartDate;
	private Date personalCrimeHospitalEndDate;
	private String personalCrimeTreatingDoctorName;

	// ClaimtypeAbsence_US_AbsencePersonalLeave
	private Boolean isFMLAQualifyingEvent;

	// ClaimtypeAbsence_US_AbsenceSmallnecessities
	private String smallNecessitiesType;

	// Control
	private String sourceSystem;
	private Date creationTimeStamp;
	private Date lastUpdateTimeStamp;
	private Date lastSyncTimestamp;
	private String action;
	private String actionBy;
	
	public String getDataOwner() {
		return dataOwner;
	}
	public void setDataOwner(String dataOwner) {
		this.dataOwner = dataOwner;
	}
	public String getClaimCaseBusId() {
		return claimCaseBusId;
	}
	public void setClaimCaseBusId(String claimCaseBusId) {
		this.claimCaseBusId = claimCaseBusId;
	}
	public String getClaimCaseExtRef() {
		return claimCaseExtRef;
	}
	public void setClaimCaseExtRef(String claimCaseExtRef) {
		this.claimCaseExtRef = claimCaseExtRef;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	public String getSourceContractBusId() {
		return sourceContractBusId;
	}
	public void setSourceContractBusId(String sourceContractBusId) {
		this.sourceContractBusId = sourceContractBusId;
	}
	public String getOperatingCountry() {
		return operatingCountry;
	}
	public void setOperatingCountry(String operatingCountry) {
		this.operatingCountry = operatingCountry;
	}
	public String getBusinessScope() {
		return businessScope;
	}
	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getClaimState() {
		return claimState;
	}
	public void setClaimState(String claimState) {
		this.claimState = claimState;
	}
	public String getClaimStateDetail() {
		return claimStateDetail;
	}
	public void setClaimStateDetail(String claimStateDetail) {
		this.claimStateDetail = claimStateDetail;
	}
	public Date getClaimDate() {
		return claimDate;
	}
	public void setClaimDate(Date claimDate) {
		this.claimDate = claimDate;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getReportingDate() {
		return reportingDate;
	}
	public void setReportingDate(Date reportingDate) {
		this.reportingDate = reportingDate;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getClientSegment() {
		return clientSegment;
	}
	public void setClientSegment(String clientSegment) {
		this.clientSegment = clientSegment;
	}
	public String getPolicyHolderBusId() {
		return policyHolderBusId;
	}
	public void setPolicyHolderBusId(String policyHolderBusId) {
		this.policyHolderBusId = policyHolderBusId;
	}
	public String getSourcePolicyHolderBusId() {
		return sourcePolicyHolderBusId;
	}
	public void setSourcePolicyHolderBusId(String sourcePolicyHolderBusId) {
		this.sourcePolicyHolderBusId = sourcePolicyHolderBusId;
	}
	public String getMainAffiliateBusId() {
		return mainAffiliateBusId;
	}
	public void setMainAffiliateBusId(String mainAffiliateBusId) {
		this.mainAffiliateBusId = mainAffiliateBusId;
	}
	public String getSourceMainAffiliateBusId() {
		return sourceMainAffiliateBusId;
	}
	public void setSourceMainAffiliateBusId(String sourceMainAffiliateBusId) {
		this.sourceMainAffiliateBusId = sourceMainAffiliateBusId;
	}
	public String getInsuredPersonBusId() {
		return insuredPersonBusId;
	}
	public void setInsuredPersonBusId(String insuredPersonBusId) {
		this.insuredPersonBusId = insuredPersonBusId;
	}
	public String getInsuredPersonType() {
		return insuredPersonType;
	}
	public void setInsuredPersonType(String insuredPersonType) {
		this.insuredPersonType = insuredPersonType;
	}
	public String getSourceInsuredPersonBusId() {
		return sourceInsuredPersonBusId;
	}
	public void setSourceInsuredPersonBusId(String sourceInsuredPersonBusId) {
		this.sourceInsuredPersonBusId = sourceInsuredPersonBusId;
	}
	public String getClaimManagerUserName() {
		return claimManagerUserName;
	}
	public void setClaimManagerUserName(String claimManagerUserName) {
		this.claimManagerUserName = claimManagerUserName;
	}
	public String getSourceClaimManagerBusId() {
		return sourceClaimManagerBusId;
	}
	public void setSourceClaimManagerBusId(String sourceClaimManagerBusId) {
		this.sourceClaimManagerBusId = sourceClaimManagerBusId;
	}
	public String getClaimMgtmTeamBusId() {
		return claimMgtmTeamBusId;
	}
	public void setClaimMgtmTeamBusId(String claimMgtmTeamBusId) {
		this.claimMgtmTeamBusId = claimMgtmTeamBusId;
	}
	public String getSourceClaimMgmtTeamBusId() {
		return sourceClaimMgmtTeamBusId;
	}
	public void setSourceClaimMgmtTeamBusId(String sourceClaimMgmtTeamBusId) {
		this.sourceClaimMgmtTeamBusId = sourceClaimMgmtTeamBusId;
	}
	public String getAssessmentState() {
		return assessmentState;
	}
	public void setAssessmentState(String assessmentState) {
		this.assessmentState = assessmentState;
	}
	public Date getBenefitStartDate() {
		return benefitStartDate;
	}
	public void setBenefitStartDate(Date benefitStartDate) {
		this.benefitStartDate = benefitStartDate;
	}
	public Date getBenefitEndDate() {
		return benefitEndDate;
	}
	public void setBenefitEndDate(Date benefitEndDate) {
		this.benefitEndDate = benefitEndDate;
	}
	public Date getCancellationDate() {
		return cancellationDate;
	}
	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}
	public Date getCertifiedEndDate() {
		return certifiedEndDate;
	}
	public void setCertifiedEndDate(Date certifiedEndDate) {
		this.certifiedEndDate = certifiedEndDate;
	}
	public String getClaimCause() {
		return claimCause;
	}
	public void setClaimCause(String claimCause) {
		this.claimCause = claimCause;
	}
	public Date getDiagnosisDate() {
		return diagnosisDate;
	}
	public void setDiagnosisDate(Date diagnosisDate) {
		this.diagnosisDate = diagnosisDate;
	}
	public Date getDisabilityDate() {
		return disabilityDate;
	}
	public void setDisabilityDate(Date disabilityDate) {
		this.disabilityDate = disabilityDate;
	}
	public Date getHospitalStartDate() {
		return hospitalStartDate;
	}
	public void setHospitalStartDate(Date hospitalStartDate) {
		this.hospitalStartDate = hospitalStartDate;
	}
	public Date getLastDayWorked() {
		return lastDayWorked;
	}
	public void setLastDayWorked(Date lastDayWorked) {
		this.lastDayWorked = lastDayWorked;
	}
	public Date getReactivationDate() {
		return reactivationDate;
	}
	public void setReactivationDate(Date reactivationDate) {
		this.reactivationDate = reactivationDate;
	}
	public Date getReturnToWorkDate() {
		return returnToWorkDate;
	}
	public void setReturnToWorkDate(Date returnToWorkDate) {
		this.returnToWorkDate = returnToWorkDate;
	}
	public Date getSymptomsStartDate() {
		return symptomsStartDate;
	}
	public void setSymptomsStartDate(Date symptomsStartDate) {
		this.symptomsStartDate = symptomsStartDate;
	}
	public Date getTerminationdate() {
		return terminationdate;
	}
	public void setTerminationdate(Date terminationdate) {
		this.terminationdate = terminationdate;
	}
	public Date getTreatmentStartDate() {
		return treatmentStartDate;
	}
	public void setTreatmentStartDate(Date treatmentStartDate) {
		this.treatmentStartDate = treatmentStartDate;
	}
	public String getAbsenceReason() {
		return absenceReason;
	}
	public void setAbsenceReason(String absenceReason) {
		this.absenceReason = absenceReason;
	}
	public String getAbsenceDurationType() {
		return absenceDurationType;
	}
	public void setAbsenceDurationType(String absenceDurationType) {
		this.absenceDurationType = absenceDurationType;
	}
	public String getAbsenceClaimState() {
		return absenceClaimState;
	}
	public void setAbsenceClaimState(String absenceClaimState) {
		this.absenceClaimState = absenceClaimState;
	}
	public String getAdoptionType() {
		return adoptionType;
	}
	public void setAdoptionType(String adoptionType) {
		this.adoptionType = adoptionType;
	}
	public Date getAdoptionDate() {
		return adoptionDate;
	}
	public void setAdoptionDate(Date adoptionDate) {
		this.adoptionDate = adoptionDate;
	}
	public Boolean getIsAdoptionCourtCase() {
		return isAdoptionCourtCase;
	}
	public void setIsAdoptionCourtCase(Boolean isAdoptionCourtCase) {
		this.isAdoptionCourtCase = isAdoptionCourtCase;
	}
	public Date getAdoptionCourtDate() {
		return adoptionCourtDate;
	}
	public void setAdoptionCourtDate(Date adoptionCourtDate) {
		this.adoptionCourtDate = adoptionCourtDate;
	}
	public Integer getAdoptionCourtHours() {
		return adoptionCourtHours;
	}
	public void setAdoptionCourtHours(Integer adoptionCourtHours) {
		this.adoptionCourtHours = adoptionCourtHours;
	}
	public String getBereaverRelationship() {
		return bereaverRelationship;
	}
	public void setBereaverRelationship(String bereaverRelationship) {
		this.bereaverRelationship = bereaverRelationship;
	}
	public String getBereaverRelationshipNotes() {
		return bereaverRelationshipNotes;
	}
	public void setBereaverRelationshipNotes(String bereaverRelationshipNotes) {
		this.bereaverRelationshipNotes = bereaverRelationshipNotes;
	}
	public Date getBereavementDeathDate() {
		return bereavementDeathDate;
	}
	public void setBereavementDeathDate(Date bereavementDeathDate) {
		this.bereavementDeathDate = bereavementDeathDate;
	}
	public String getBondingParentType() {
		return bondingParentType;
	}
	public void setBondingParentType(String bondingParentType) {
		this.bondingParentType = bondingParentType;
	}
	public Date getBondingEstimatedDeliveryDate() {
		return bondingEstimatedDeliveryDate;
	}
	public void setBondingEstimatedDeliveryDate(Date bondingEstimatedDeliveryDate) {
		this.bondingEstimatedDeliveryDate = bondingEstimatedDeliveryDate;
	}
	public Integer getBondingNumDelivered() {
		return bondingNumDelivered;
	}
	public void setBondingNumDelivered(Integer bondingNumDelivered) {
		this.bondingNumDelivered = bondingNumDelivered;
	}
	public Boolean getIsBondingPartnerEmployerSame() {
		return isBondingPartnerEmployerSame;
	}
	public void setIsBondingPartnerEmployerSame(Boolean isBondingPartnerEmployerSame) {
		this.isBondingPartnerEmployerSame = isBondingPartnerEmployerSame;
	}
	public Boolean getIsPartnerBonding() {
		return isPartnerBonding;
	}
	public void setIsPartnerBonding(Boolean isPartnerBonding) {
		this.isPartnerBonding = isPartnerBonding;
	}
	public String getBondingPartnerName() {
		return bondingPartnerName;
	}
	public void setBondingPartnerName(String bondingPartnerName) {
		this.bondingPartnerName = bondingPartnerName;
	}
	public Date getBondingPartnerStartDate() {
		return bondingPartnerStartDate;
	}
	public void setBondingPartnerStartDate(Date bondingPartnerStartDate) {
		this.bondingPartnerStartDate = bondingPartnerStartDate;
	}
	public Date getBondingPartnerEndDate() {
		return bondingPartnerEndDate;
	}
	public void setBondingPartnerEndDate(Date bondingPartnerEndDate) {
		this.bondingPartnerEndDate = bondingPartnerEndDate;
	}
	public Date getBondingLastDisabilityDate() {
		return bondingLastDisabilityDate;
	}
	public void setBondingLastDisabilityDate(Date bondingLastDisabilityDate) {
		this.bondingLastDisabilityDate = bondingLastDisabilityDate;
	}
	public Boolean getIsBoneMarrowMedicalApptCase() {
		return isBoneMarrowMedicalApptCase;
	}
	public void setIsBoneMarrowMedicalApptCase(Boolean isBoneMarrowMedicalApptCase) {
		this.isBoneMarrowMedicalApptCase = isBoneMarrowMedicalApptCase;
	}
	public Date getBoneMarrowMedicalApptDate() {
		return boneMarrowMedicalApptDate;
	}
	public void setBoneMarrowMedicalApptDate(Date boneMarrowMedicalApptDate) {
		this.boneMarrowMedicalApptDate = boneMarrowMedicalApptDate;
	}
	public Integer getBoneMarrowMedicalApptHours() {
		return boneMarrowMedicalApptHours;
	}
	public void setBoneMarrowMedicalApptHours(Integer boneMarrowMedicalApptHours) {
		this.boneMarrowMedicalApptHours = boneMarrowMedicalApptHours;
	}
	public Date getChildbirthDate() {
		return childbirthDate;
	}
	public void setChildbirthDate(Date childbirthDate) {
		this.childbirthDate = childbirthDate;
	}
	public Date getChildbirthEstimatedDate() {
		return childbirthEstimatedDate;
	}
	public void setChildbirthEstimatedDate(Date childbirthEstimatedDate) {
		this.childbirthEstimatedDate = childbirthEstimatedDate;
	}
	public Integer getChildbirthNumDelivered() {
		return childbirthNumDelivered;
	}
	public void setChildbirthNumDelivered(Integer childbirthNumDelivered) {
		this.childbirthNumDelivered = childbirthNumDelivered;
	}
	public String getChildbirthDeliveryType() {
		return childbirthDeliveryType;
	}
	public void setChildbirthDeliveryType(String childbirthDeliveryType) {
		this.childbirthDeliveryType = childbirthDeliveryType;
	}
	public Boolean getIsChildbirthBondingTimeCase() {
		return isChildbirthBondingTimeCase;
	}
	public void setIsChildbirthBondingTimeCase(Boolean isChildbirthBondingTimeCase) {
		this.isChildbirthBondingTimeCase = isChildbirthBondingTimeCase;
	}
	public Boolean getIsChildbirthComplicationCase() {
		return isChildbirthComplicationCase;
	}
	public void setIsChildbirthComplicationCase(Boolean isChildbirthComplicationCase) {
		this.isChildbirthComplicationCase = isChildbirthComplicationCase;
	}
	public Boolean getIsChildbirthPartnerBonding() {
		return isChildbirthPartnerBonding;
	}
	public void setIsChildbirthPartnerBonding(Boolean isChildbirthPartnerBonding) {
		this.isChildbirthPartnerBonding = isChildbirthPartnerBonding;
	}
	public Boolean getIsChildbirthPartnerEmployerSame() {
		return isChildbirthPartnerEmployerSame;
	}
	public void setIsChildbirthPartnerEmployerSame(Boolean isChildbirthPartnerEmployerSame) {
		this.isChildbirthPartnerEmployerSame = isChildbirthPartnerEmployerSame;
	}
	public String getChildbirthPartnerName() {
		return childbirthPartnerName;
	}
	public void setChildbirthPartnerName(String childbirthPartnerName) {
		this.childbirthPartnerName = childbirthPartnerName;
	}
	public Date getChildbirthPartnerBondingStartDate() {
		return childbirthPartnerBondingStartDate;
	}
	public void setChildbirthPartnerBondingStartDate(Date childbirthPartnerBondingStartDate) {
		this.childbirthPartnerBondingStartDate = childbirthPartnerBondingStartDate;
	}
	public Date getChildbirthPartnerBondingEndDate() {
		return childbirthPartnerBondingEndDate;
	}
	public void setChildbirthPartnerBondingEndDate(Date childbirthPartnerBondingEndDate) {
		this.childbirthPartnerBondingEndDate = childbirthPartnerBondingEndDate;
	}
	public Date getChildbirthLastDisabilityDate() {
		return childbirthLastDisabilityDate;
	}
	public void setChildbirthLastDisabilityDate(Date childbirthLastDisabilityDate) {
		this.childbirthLastDisabilityDate = childbirthLastDisabilityDate;
	}
	public String getChildbirthTreatingDoctorName() {
		return childbirthTreatingDoctorName;
	}
	public void setChildbirthTreatingDoctorName(String childbirthTreatingDoctorName) {
		this.childbirthTreatingDoctorName = childbirthTreatingDoctorName;
	}
	public String getFamilyMemberName() {
		return familyMemberName;
	}
	public void setFamilyMemberName(String familyMemberName) {
		this.familyMemberName = familyMemberName;
	}
	public String getFamilyMemberRelationship() {
		return familyMemberRelationship;
	}
	public void setFamilyMemberRelationship(String familyMemberRelationship) {
		this.familyMemberRelationship = familyMemberRelationship;
	}
	public String getFamilyMemberRelationshipNotes() {
		return familyMemberRelationshipNotes;
	}
	public void setFamilyMemberRelationshipNotes(String familyMemberRelationshipNotes) {
		this.familyMemberRelationshipNotes = familyMemberRelationshipNotes;
	}
	public Boolean getIsFamilyFollowUpApptCase() {
		return isFamilyFollowUpApptCase;
	}
	public void setIsFamilyFollowUpApptCase(Boolean isFamilyFollowUpApptCase) {
		this.isFamilyFollowUpApptCase = isFamilyFollowUpApptCase;
	}
	public Boolean getIsFamilyHospitalCase() {
		return isFamilyHospitalCase;
	}
	public void setIsFamilyHospitalCase(Boolean isFamilyHospitalCase) {
		this.isFamilyHospitalCase = isFamilyHospitalCase;
	}
	public Boolean getIsFamilyMemberADAQualified() {
		return isFamilyMemberADAQualified;
	}
	public void setIsFamilyMemberADAQualified(Boolean isFamilyMemberADAQualified) {
		this.isFamilyMemberADAQualified = isFamilyMemberADAQualified;
	}
	public Boolean getIsFamilyMemberOver18() {
		return isFamilyMemberOver18;
	}
	public void setIsFamilyMemberOver18(Boolean isFamilyMemberOver18) {
		this.isFamilyMemberOver18 = isFamilyMemberOver18;
	}
	public Boolean getIsfamilyMemberConditionNonSerious() {
		return isfamilyMemberConditionNonSerious;
	}
	public void setIsfamilyMemberConditionNonSerious(Boolean isfamilyMemberConditionNonSerious) {
		this.isfamilyMemberConditionNonSerious = isfamilyMemberConditionNonSerious;
	}
	public Date getFamilyChildBirthDate() {
		return familyChildBirthDate;
	}
	public void setFamilyChildBirthDate(Date familyChildBirthDate) {
		this.familyChildBirthDate = familyChildBirthDate;
	}
	public String getFamilyMemberHealthConditionNotes() {
		return familyMemberHealthConditionNotes;
	}
	public void setFamilyMemberHealthConditionNotes(String familyMemberHealthConditionNotes) {
		this.familyMemberHealthConditionNotes = familyMemberHealthConditionNotes;
	}
	public Date getFamilyIllnessStartDate() {
		return familyIllnessStartDate;
	}
	public void setFamilyIllnessStartDate(Date familyIllnessStartDate) {
		this.familyIllnessStartDate = familyIllnessStartDate;
	}
	public Date getFamilyHospitalStartDate() {
		return familyHospitalStartDate;
	}
	public void setFamilyHospitalStartDate(Date familyHospitalStartDate) {
		this.familyHospitalStartDate = familyHospitalStartDate;
	}
	public Date getFamilyHospitalEndDate() {
		return familyHospitalEndDate;
	}
	public void setFamilyHospitalEndDate(Date familyHospitalEndDate) {
		this.familyHospitalEndDate = familyHospitalEndDate;
	}
	public Date getFamilyTreatmentStartDate() {
		return familyTreatmentStartDate;
	}
	public void setFamilyTreatmentStartDate(Date familyTreatmentStartDate) {
		this.familyTreatmentStartDate = familyTreatmentStartDate;
	}
	public Date getFamilyFollowUpApptDate() {
		return familyFollowUpApptDate;
	}
	public void setFamilyFollowUpApptDate(Date familyFollowUpApptDate) {
		this.familyFollowUpApptDate = familyFollowUpApptDate;
	}
	public String getFamilyTreatingDoctorName() {
		return familyTreatingDoctorName;
	}
	public void setFamilyTreatingDoctorName(String familyTreatingDoctorName) {
		this.familyTreatingDoctorName = familyTreatingDoctorName;
	}
	public String getMilitaryLeaveType() {
		return militaryLeaveType;
	}
	public void setMilitaryLeaveType(String militaryLeaveType) {
		this.militaryLeaveType = militaryLeaveType;
	}
	public String getMilitaryMemberType() {
		return militaryMemberType;
	}
	public void setMilitaryMemberType(String militaryMemberType) {
		this.militaryMemberType = militaryMemberType;
	}
	public String getMilitaryMemberRelationship() {
		return militaryMemberRelationship;
	}
	public void setMilitaryMemberRelationship(String militaryMemberRelationship) {
		this.militaryMemberRelationship = militaryMemberRelationship;
	}
	public String getMilitaryMemberRelationshipNotes() {
		return militaryMemberRelationshipNotes;
	}
	public void setMilitaryMemberRelationshipNotes(String militaryMemberRelationshipNotes) {
		this.militaryMemberRelationshipNotes = militaryMemberRelationshipNotes;
	}
	public String getOrganDonating() {
		return organDonating;
	}
	public void setOrganDonating(String organDonating) {
		this.organDonating = organDonating;
	}
	public Boolean getIsOrganDonorMedicalApptCase() {
		return isOrganDonorMedicalApptCase;
	}
	public void setIsOrganDonorMedicalApptCase(Boolean isOrganDonorMedicalApptCase) {
		this.isOrganDonorMedicalApptCase = isOrganDonorMedicalApptCase;
	}
	public Date getOrganDonorMedicalApptDate() {
		return organDonorMedicalApptDate;
	}
	public void setOrganDonorMedicalApptDate(Date organDonorMedicalApptDate) {
		this.organDonorMedicalApptDate = organDonorMedicalApptDate;
	}
	public Integer getOrganDonorMedicalApptHours() {
		return organDonorMedicalApptHours;
	}
	public void setOrganDonorMedicalApptHours(Integer organDonorMedicalApptHours) {
		this.organDonorMedicalApptHours = organDonorMedicalApptHours;
	}
	public String getOwnIllnessInjuryType() {
		return ownIllnessInjuryType;
	}
	public void setOwnIllnessInjuryType(String ownIllnessInjuryType) {
		this.ownIllnessInjuryType = ownIllnessInjuryType;
	}
	public boolean isOwnIllnessHospitalCase() {
		return isOwnIllnessHospitalCase;
	}
	public void setOwnIllnessHospitalCase(boolean isOwnIllnessHospitalCase) {
		this.isOwnIllnessHospitalCase = isOwnIllnessHospitalCase;
	}
	public boolean isOwnIllnessFollowUpApptCase() {
		return isOwnIllnessFollowUpApptCase;
	}
	public void setOwnIllnessFollowUpApptCase(boolean isOwnIllnessFollowUpApptCase) {
		this.isOwnIllnessFollowUpApptCase = isOwnIllnessFollowUpApptCase;
	}
	public boolean isOwnIllnessWorkRelated() {
		return isOwnIllnessWorkRelated;
	}
	public void setOwnIllnessWorkRelated(boolean isOwnIllnessWorkRelated) {
		this.isOwnIllnessWorkRelated = isOwnIllnessWorkRelated;
	}
	public boolean isOwnMentalIllnesCase() {
		return isOwnMentalIllnesCase;
	}
	public void setOwnMentalIllnesCase(boolean isOwnMentalIllnesCase) {
		this.isOwnMentalIllnesCase = isOwnMentalIllnesCase;
	}
	public Date getOwnIllnessStartDate() {
		return ownIllnessStartDate;
	}
	public void setOwnIllnessStartDate(Date ownIllnessStartDate) {
		this.ownIllnessStartDate = ownIllnessStartDate;
	}
	public Date getOwnIllnessHospitalStartDate() {
		return ownIllnessHospitalStartDate;
	}
	public void setOwnIllnessHospitalStartDate(Date ownIllnessHospitalStartDate) {
		this.ownIllnessHospitalStartDate = ownIllnessHospitalStartDate;
	}
	public Date getOwnIllnessHospitalEndDate() {
		return ownIllnessHospitalEndDate;
	}
	public void setOwnIllnessHospitalEndDate(Date ownIllnessHospitalEndDate) {
		this.ownIllnessHospitalEndDate = ownIllnessHospitalEndDate;
	}
	public Date getOwnIllnessFollowUpApptDate() {
		return ownIllnessFollowUpApptDate;
	}
	public void setOwnIllnessFollowUpApptDate(Date ownIllnessFollowUpApptDate) {
		this.ownIllnessFollowUpApptDate = ownIllnessFollowUpApptDate;
	}
	public Date getOwnIllnessTreatmentStartDate() {
		return ownIllnessTreatmentStartDate;
	}
	public void setOwnIllnessTreatmentStartDate(Date ownIllnessTreatmentStartDate) {
		this.ownIllnessTreatmentStartDate = ownIllnessTreatmentStartDate;
	}
	public String getOwnIllnessTreatingDoctorName() {
		return ownIllnessTreatingDoctorName;
	}
	public void setOwnIllnessTreatingDoctorName(String ownIllnessTreatingDoctorName) {
		this.ownIllnessTreatingDoctorName = ownIllnessTreatingDoctorName;
	}
	public String getPersonalCrimeType() {
		return personalCrimeType;
	}
	public void setPersonalCrimeType(String personalCrimeType) {
		this.personalCrimeType = personalCrimeType;
	}
	public String getPersonalCrimeTypeNotes() {
		return personalCrimeTypeNotes;
	}
	public void setPersonalCrimeTypeNotes(String personalCrimeTypeNotes) {
		this.personalCrimeTypeNotes = personalCrimeTypeNotes;
	}
	public String getPersonalCrimeRelationship() {
		return personalCrimeRelationship;
	}
	public void setPersonalCrimeRelationship(String personalCrimeRelationship) {
		this.personalCrimeRelationship = personalCrimeRelationship;
	}
	public String getPersonalCrimeRelationshipNotes() {
		return personalCrimeRelationshipNotes;
	}
	public void setPersonalCrimeRelationshipNotes(String personalCrimeRelationshipNotes) {
		this.personalCrimeRelationshipNotes = personalCrimeRelationshipNotes;
	}
	public Boolean getIsPersonalCrimeCounsellingCase() {
		return isPersonalCrimeCounsellingCase;
	}
	public void setIsPersonalCrimeCounsellingCase(Boolean isPersonalCrimeCounsellingCase) {
		this.isPersonalCrimeCounsellingCase = isPersonalCrimeCounsellingCase;
	}
	public Boolean getIsPersonalCrimeCourtCase() {
		return isPersonalCrimeCourtCase;
	}
	public void setIsPersonalCrimeCourtCase(Boolean isPersonalCrimeCourtCase) {
		this.isPersonalCrimeCourtCase = isPersonalCrimeCourtCase;
	}
	public Boolean getIsPersonalCrimeDoctorCase() {
		return isPersonalCrimeDoctorCase;
	}
	public void setIsPersonalCrimeDoctorCase(Boolean isPersonalCrimeDoctorCase) {
		this.isPersonalCrimeDoctorCase = isPersonalCrimeDoctorCase;
	}
	public Boolean getIsPersonalCrimeHospitalCase() {
		return isPersonalCrimeHospitalCase;
	}
	public void setIsPersonalCrimeHospitalCase(Boolean isPersonalCrimeHospitalCase) {
		this.isPersonalCrimeHospitalCase = isPersonalCrimeHospitalCase;
	}
	public Boolean getIsPersonalCrimeLegalAssistCase() {
		return isPersonalCrimeLegalAssistCase;
	}
	public void setIsPersonalCrimeLegalAssistCase(Boolean isPersonalCrimeLegalAssistCase) {
		this.isPersonalCrimeLegalAssistCase = isPersonalCrimeLegalAssistCase;
	}
	public Boolean getIsPersonalCrimeRelocationCase() {
		return isPersonalCrimeRelocationCase;
	}
	public void setIsPersonalCrimeRelocationCase(Boolean isPersonalCrimeRelocationCase) {
		this.isPersonalCrimeRelocationCase = isPersonalCrimeRelocationCase;
	}
	public Boolean getIsPersonalCrimeRestrainingOrderCase() {
		return isPersonalCrimeRestrainingOrderCase;
	}
	public void setIsPersonalCrimeRestrainingOrderCase(Boolean isPersonalCrimeRestrainingOrderCase) {
		this.isPersonalCrimeRestrainingOrderCase = isPersonalCrimeRestrainingOrderCase;
	}
	public Boolean getIsPersonalCrimeVictimServicesCase() {
		return isPersonalCrimeVictimServicesCase;
	}
	public void setIsPersonalCrimeVictimServicesCase(Boolean isPersonalCrimeVictimServicesCase) {
		this.isPersonalCrimeVictimServicesCase = isPersonalCrimeVictimServicesCase;
	}
	public Date getPersonalCrimeHospitalStartDate() {
		return personalCrimeHospitalStartDate;
	}
	public void setPersonalCrimeHospitalStartDate(Date personalCrimeHospitalStartDate) {
		this.personalCrimeHospitalStartDate = personalCrimeHospitalStartDate;
	}
	public Date getPersonalCrimeHospitalEndDate() {
		return personalCrimeHospitalEndDate;
	}
	public void setPersonalCrimeHospitalEndDate(Date personalCrimeHospitalEndDate) {
		this.personalCrimeHospitalEndDate = personalCrimeHospitalEndDate;
	}
	public String getPersonalCrimeTreatingDoctorName() {
		return personalCrimeTreatingDoctorName;
	}
	public void setPersonalCrimeTreatingDoctorName(String personalCrimeTreatingDoctorName) {
		this.personalCrimeTreatingDoctorName = personalCrimeTreatingDoctorName;
	}
	public Boolean getIsFMLAQualifyingEvent() {
		return isFMLAQualifyingEvent;
	}
	public void setIsFMLAQualifyingEvent(Boolean isFMLAQualifyingEvent) {
		this.isFMLAQualifyingEvent = isFMLAQualifyingEvent;
	}
	public String getSmallNecessitiesType() {
		return smallNecessitiesType;
	}
	public void setSmallNecessitiesType(String smallNecessitiesType) {
		this.smallNecessitiesType = smallNecessitiesType;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}
	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	public Date getLastUpdateTimeStamp() {
		return lastUpdateTimeStamp;
	}
	public void setLastUpdateTimeStamp(Date lastUpdateTimeStamp) {
		this.lastUpdateTimeStamp = lastUpdateTimeStamp;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActionBy() {
		return actionBy;
	}
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	public Date getLastSyncTimestamp() {
		return lastSyncTimestamp;
	}
	public void setLastSyncTimestamp(Date lastSyncTimestamp) {
		this.lastSyncTimestamp = lastSyncTimestamp;
	}
	
}