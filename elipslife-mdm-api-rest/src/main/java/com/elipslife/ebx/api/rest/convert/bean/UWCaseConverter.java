package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.lang3.StringUtils;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.SynchroniseUWApplication;
import com.elipslife.ebx.api.rest.models.UWApplication;
import com.elipslife.ebx.api.rest.models.UWCases;
import com.elipslife.mdm.uw.landing.LandingUWCaseBean;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class UWCaseConverter extends AbstractConverter<LandingUWCaseBean[], SynchroniseUWApplication> {

	@Override
	public LandingUWCaseBean[] convert(SynchroniseUWApplication synchroniseUWApplication)throws DatatypeConfigurationException {

		ArrayList<LandingUWCaseBean> landingUWCaseBeans = new ArrayList<>();
		UWApplication uwApplication = synchroniseUWApplication.data;

		for (UWCases uwCase : (uwApplication.uwCases == null ? new ArrayList<UWCases>() : uwApplication.uwCases)) {

			LandingUWCaseBean uwCaseBean = new LandingUWCaseBean();
			landingUWCaseBeans.add(uwCaseBean);
			
			// Format Date values
			Date applicationDateOfBirth = uwCase.applicantDateOfBirth == null ? null : DateHelper.convertToDate(uwCase.applicantDateOfBirth);
			Date eligibilityDate = uwCase.eligibilityDate == null ? null : DateHelper.convertToDate(uwCase.eligibilityDate);
			Date creationTimeStamp = uwCase.creationTimestamp == null ? null : Date.from(uwCase.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
			Date lastUpdateTimeStamp = uwCase.lastUpdateTimestamp == null ? null : Date.from(uwCase.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
			Date deletionTimeStamp = uwCase.deletionTimestamp == null ? null : Date.from(uwCase.deletionTimestamp.atZone(ZoneId.systemDefault()).toInstant());
			
			if(StringUtils.isBlank(uwCase.dataOwner)) {
				uwCaseBean.setDataOwner("0");
			} else {
				uwCaseBean.setDataOwner(uwCase.dataOwner);	}
			uwCaseBean.setUWCaseBusId(uwCase.UWCaseBusId);
			uwCaseBean.setUWApplicationBusId(uwCase.uwApplicationBusId);
			uwCaseBean.setContractNumber(uwCase.contractNumber);
			uwCaseBean.setApplicantBusId(uwCase.applicantBusId);
			uwCaseBean.setApplicantRelationship(uwCase.applicantRelationship);
			uwCaseBean.setApplicantFirstName(uwCase.applicantFirstName);
			uwCaseBean.setApplicantLastName(uwCase.applicantLastName);
			uwCaseBean.setApplicantDateOfBirth(applicationDateOfBirth);
			uwCaseBean.setApplicantSSN(uwCase.applicantSSN);
			uwCaseBean.setApplicantTown(uwCase.applicantTown);
			uwCaseBean.setApplicantPostCode(uwCase.applicantPostCode);
			uwCaseBean.setApplicantCountry(uwCase.applicantCountry);
			uwCaseBean.setApplicantClass(uwCase.applicantClass);
			uwCaseBean.setProductType(uwCase.productType);
			uwCaseBean.setEligiblityDate(eligibilityDate);
			uwCaseBean.setApplicationReason(uwCase.applicationReason);
			uwCaseBean.setApplicationReasonText(uwCase.applicationReasonText);
			uwCaseBean.setInforceCoverage(uwCase.inforceCoverage);
			uwCaseBean.setAdditionalCoverage(uwCase.additionalCoverage);
			if(uwCase.isActive == null) {
				uwCaseBean.setIsActive(true);
			} else {
				uwCaseBean.setIsActive(uwCase.isActive);	}
			uwCaseBean.setSourceSystem(synchroniseUWApplication.context.sourceSystemId);
			uwCaseBean.setCaseState(uwCase.caseState);
			uwCaseBean.setCreatedBy(uwCase.createdBy);
			uwCaseBean.setCreationTimeStamp(creationTimeStamp);
			uwCaseBean.setLastUpdatedBy(uwCase.lastUpdatedBy);
			uwCaseBean.setLastUpdateTimeStamp(lastUpdateTimeStamp);
			uwCaseBean.setDeletionTimeStamp(deletionTimeStamp);
			uwCaseBean.setAction(uwCase.action);

		}
		return landingUWCaseBeans.toArray(new LandingUWCaseBean[0]);
	}
}
