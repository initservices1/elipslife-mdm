package com.elipslife.mdm.party;

import java.util.HashMap;

import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.Path;

public class MapStageToCoreTable {

	
	public HashMap<Path, Object> buildPartyRecordFromStageRecord(ValueContext stageRecord) {/*
	
		HashMap<Path, Object> pathToValueMap = new HashMap<>();
		
		String stgSrcSystem = String.valueOf(stageRecord.getValue(PartyPaths._STG_Party._SourceSystem));
		
		if(stgSrcSystem.equalsIgnoreCase(PartyConstants.SourceSystems.CRM)) {
			pathToValueMap.put(
					PartyPaths._Party._PartyBusId,
					stageRecord.getValue(PartyPaths._STG_Party._PartyBusId));
		}
		else if(stgSrcSystem.equalsIgnoreCase(PartyConstants.SourceSystems.BBTL)) {
			pathToValueMap.put(
					PartyPaths._Party._BBTLPartyBusId,
					stageRecord.getValue(PartyPaths._STG_Party._PartyBusId));
		}
		
		pathToValueMap.put(
				PartyPaths._Party._PartyBusId,
				stageRecord.getValue(PartyPaths._STG_Party._PartyBusId));
		
		pathToValueMap.put(
				PartyPaths._Party._DataOwner,
				stageRecord.getValue(PartyPaths._STG_Party._DataOwner));

			pathToValueMap.put(
				PartyPaths._Party._PartyType,
				stageRecord.getValue(PartyPaths._STG_Party._PartyType));

			pathToValueMap.put(
				PartyPaths._Party._RelationshipOwner,
				stageRecord.getValue(PartyPaths._STG_Party._RelationshipOwner));

			pathToValueMap.put(
				PartyPaths._Party._DataResponsible,
				stageRecord.getValue(PartyPaths._STG_Party._DataResponsible));

			pathToValueMap.put(
				PartyPaths._Party._PartnerState,
				stageRecord.getValue(PartyPaths._STG_Party._PartnerState));

			pathToValueMap.put(
				PartyPaths._Party._ValidFrom,
				stageRecord.getValue(PartyPaths._STG_Party._ValidFrom));

			pathToValueMap.put(
				PartyPaths._Party._ValidTo,
				stageRecord.getValue(PartyPaths._STG_Party._ValidTo));

			String orgName = String
				.valueOf(stageRecord.getValue(PartyPaths._STG_Party._OrgName));
			
			if (!"null".equalsIgnoreCase(orgName) && !orgName.isEmpty())
			{
				pathToValueMap.put(
					PartyPaths._Party._OrgName,
					stageRecord.getValue(PartyPaths._STG_Party._OrgName));

				pathToValueMap.put(
					PartyPaths._Party._PartyName,
					stageRecord.getValue(PartyPaths._STG_Party._OrgName));
				
				pathToValueMap.put(PartyPaths._Party._PartyNameSearchText, PartyFunction.replaceUmlaut(orgName));

				pathToValueMap.put(
					PartyPaths._Party._ValidTo,
					stageRecord.getValue(PartyPaths._STG_Party._ValidTo));

				pathToValueMap.put(
					PartyPaths._Party._OrgName2,
					stageRecord.getValue(PartyPaths._STG_Party._OrgName2));

				pathToValueMap.put(
					PartyPaths._Party._CompanyType,
					stageRecord.getValue(PartyPaths._STG_Party._CompanyType));

				pathToValueMap.put(
					PartyPaths._Party._CommercialRegNo,
					stageRecord.getValue(PartyPaths._STG_Party._CommercialRegNo));

				pathToValueMap.put(
					PartyPaths._Party._UIDNumber,
					stageRecord.getValue(PartyPaths._STG_Party._UIDNumber));

				pathToValueMap.put(
					PartyPaths._Party._Industry,
					stageRecord.getValue(PartyPaths._STG_Party._Industry));

			}

			else
			{

				String partyName = String
					.valueOf(stageRecord.getValue(PartyPaths._STG_Party._FirstName))
					.concat(" ")
					.concat(
						String.valueOf(
							stageRecord.getValue(PartyPaths._STG_Party._LastName)));

				partyName = partyName.replace("null", "");

				pathToValueMap.put(PartyPaths._Party._PartyName, partyName);

				pathToValueMap.put(PartyPaths._Party._PartyNameSearchText, PartyFunction.replaceUmlaut(partyName));

				pathToValueMap.put(
					PartyPaths._Party._LastName,
					stageRecord.getValue(PartyPaths._STG_Party._LastName));

				pathToValueMap.put(
					PartyPaths._Party._FirstName,
					stageRecord.getValue(PartyPaths._STG_Party._FirstName));

				pathToValueMap.put(
					PartyPaths._Party._DateOfBirth,
					stageRecord.getValue(PartyPaths._STG_Party._DateOfBirth));

				pathToValueMap.put(
					PartyPaths._Party._Gender,
					stageRecord.getValue(PartyPaths._STG_Party._Gender));

				pathToValueMap.put(
					PartyPaths._Party._Title,
					stageRecord.getValue(PartyPaths._STG_Party._Title));

				pathToValueMap.put(
					PartyPaths._Party._Salutation,
					stageRecord.getValue(PartyPaths._STG_Party._Salutation));

				pathToValueMap.put(
					PartyPaths._Party._LetterSalutation,
					stageRecord.getValue(PartyPaths._STG_Party._LetterSalutation));

				pathToValueMap.put(
					PartyPaths._Party._CivilStatus,
					stageRecord.getValue(PartyPaths._STG_Party._CivilStatus));

				pathToValueMap.put(
					PartyPaths._Party._LetterSalutation,
					stageRecord.getValue(PartyPaths._STG_Party._LetterSalutation));

			}

			// Add Roles //

			pathToValueMap.put(
				PartyPaths._Party._Roles,
				stageRecord.getValue(PartyPaths._STG_Party._Roles));

			pathToValueMap.put(
				PartyPaths._Party._IsVIPClient,
				stageRecord.getValue(PartyPaths._STG_Party._IsVIPClient));

			pathToValueMap.put(
				PartyPaths._Party._Blacklist,
				stageRecord.getValue(PartyPaths._STG_Party._Blacklist));

			pathToValueMap.put(
				PartyPaths._Party._PartnerState,
				stageRecord.getValue(PartyPaths._STG_Party._PartnerState));

			pathToValueMap.put(
				PartyPaths._Party._CompanyPersonalNr,
				stageRecord.getValue(PartyPaths._STG_Party._CompanyPersonalNr));

			pathToValueMap.put(
				PartyPaths._Party._SocialSecurityNr,
				stageRecord.getValue(PartyPaths._STG_Party._SocialSecurityNr));

			pathToValueMap.put(
				PartyPaths._Party._SocialSecurityNrType,
				stageRecord.getValue(PartyPaths._STG_Party._SocialSecurityNrType));

			pathToValueMap.put(
				PartyPaths._Party._BrokerRegNo,
				stageRecord.getValue(PartyPaths._STG_Party._BrokerRegNo));

			pathToValueMap.put(
				PartyPaths._Party._LineOfBusiness,
				stageRecord.getValue(PartyPaths._STG_Party._LineOfBusiness));

			// Financial //

			pathToValueMap.put(
				PartyPaths._Party._FinancePartyBusId,
				stageRecord.getValue(PartyPaths._STG_Party._FinancePartyBusId));

			pathToValueMap.put(
				PartyPaths._Party._PaymentTerms,
				stageRecord.getValue(PartyPaths._STG_Party._PaymentTerms));

			pathToValueMap.put(
				PartyPaths._Party._FinanceClients,
				stageRecord.getValue(PartyPaths._STG_Party._FinanceClients));

			pathToValueMap.put(
				PartyPaths._Party._CreditorClients,
				stageRecord.getValue(PartyPaths._STG_Party._CreditorClients));

			pathToValueMap.put(
				PartyPaths._Party._BankAccountType,
				stageRecord.getValue(PartyPaths._STG_Party._BankAccountType));

			pathToValueMap.put(
				PartyPaths._Party._BankAccountNumber,
				stageRecord.getValue(PartyPaths._STG_Party._BankAccountNumber));

			pathToValueMap.put(
				PartyPaths._Party._BankClearingNumber,
				stageRecord.getValue(PartyPaths._STG_Party._BankClearingNumber));

			pathToValueMap.put(
				PartyPaths._Party._BIC,
				stageRecord.getValue(PartyPaths._STG_Party._BIC));

			pathToValueMap.put(
				PartyPaths._Party._IBAN,
				stageRecord.getValue(PartyPaths._STG_Party._IBAN));

			pathToValueMap.put(
				PartyPaths._Party._BankName,
				stageRecord.getValue(PartyPaths._STG_Party._BankName));

			pathToValueMap.put(
				PartyPaths._Party._BankCountry,
				stageRecord.getValue(PartyPaths._STG_Party._BankCountry));

			pathToValueMap.put(
				PartyPaths._Party._BankAccountCurrency,
				stageRecord.getValue(PartyPaths._STG_Party._BankAccountCurrency));

			// Commuication //

			pathToValueMap.put(
				PartyPaths._Party._Language,
				stageRecord.getValue(PartyPaths._STG_Party._Language));

			pathToValueMap.put(
				PartyPaths._Party._Legal_Street,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_Street));

			pathToValueMap.put(
				PartyPaths._Party._Legal_Street2,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_Street2));

			pathToValueMap.put(
				PartyPaths._Party._Legal_Town,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_Town));

			pathToValueMap.put(
				PartyPaths._Party._Legal_PostCode,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_PostCode));

			pathToValueMap.put(
				PartyPaths._Party._Legal_POBox,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_POBox));

			pathToValueMap.put(
				PartyPaths._Party._Legal_POBoxPostCode,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_POBoxPostCode));

			pathToValueMap.put(
				PartyPaths._Party._Legal_POBoxTown,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_POBoxTown));

			pathToValueMap.put(
				PartyPaths._Party._Legal_District,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_District));

			pathToValueMap.put(
				PartyPaths._Party._Legal_StateProvince,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_StateProvince));

			pathToValueMap.put(
				PartyPaths._Party._Legal_Country,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_Country));

			pathToValueMap.put(
				PartyPaths._Party._Corr_Street,
				stageRecord.getValue(PartyPaths._STG_Party._Corr_Street));

			pathToValueMap.put(
				PartyPaths._Party._Corr_Street2,
				stageRecord.getValue(PartyPaths._STG_Party._Corr_Street2));

			pathToValueMap.put(
				PartyPaths._Party._Corr_Town,
				stageRecord.getValue(PartyPaths._STG_Party._Corr_Town));

			pathToValueMap.put(
				PartyPaths._Party._Corr_PostCode,
				stageRecord.getValue(PartyPaths._STG_Party._Corr_PostCode));

			pathToValueMap.put(
				PartyPaths._Party._Corr_POBox,
				stageRecord.getValue(PartyPaths._STG_Party._Corr_POBox));

			pathToValueMap.put(
				PartyPaths._Party._Corr_POBoxPostCode,
				stageRecord.getValue(PartyPaths._STG_Party._Corr_POBoxPostCode));

			pathToValueMap.put(
				PartyPaths._Party._Corr_POBoxTown,
				stageRecord.getValue(PartyPaths._STG_Party._Corr_POBoxTown));

			pathToValueMap.put(
				PartyPaths._Party._Corr_District,
				stageRecord.getValue(PartyPaths._STG_Party._Corr_District));

			pathToValueMap.put(
				PartyPaths._Party._Corr_StateProvince,
				stageRecord.getValue(PartyPaths._STG_Party._Corr_StateProvince));

			pathToValueMap.put(
				PartyPaths._Party._Email,
				stageRecord.getValue(PartyPaths._STG_Party._Email));

			pathToValueMap.put(
				PartyPaths._Party._Telephone,
				stageRecord.getValue(PartyPaths._STG_Party._Telephone));

			pathToValueMap.put(
				PartyPaths._Party._Mobile,
				stageRecord.getValue(PartyPaths._STG_Party._Mobile));

			pathToValueMap.put(
				PartyPaths._Party._Fax,
				stageRecord.getValue(PartyPaths._STG_Party._Fax));

			pathToValueMap.put(
				PartyPaths._Party._URL,
				stageRecord.getValue(PartyPaths._STG_Party._URL));

			// Control //
			pathToValueMap.put(
					PartyPaths._Party._SourceSystem,
					stageRecord.getValue(PartyPaths._STG_Party._SourceSystem));*/
			
		
		return null;
	}
	
	public HashMap<Path, Object> buildContactPersonRecordFromStageRecord(ValueContext stageRecord) {/*
		
		HashMap<Path, Object> pathToValueMap = new HashMap<>();
		pathToValueMap.put(
				PartyPaths._ContactPerson._PartyBusId,
				stageRecord.getValue(PartyPaths._STG_Party._PartyBusId));
		
		pathToValueMap.put(
				PartyPaths._ContactPerson._DataOwner,
				stageRecord.getValue(PartyPaths._STG_Party._DataOwner));

			pathToValueMap.put(
				PartyPaths._ContactPerson._PartyType,
				stageRecord.getValue(PartyPaths._STG_Party._PartyType));

			pathToValueMap.put(
				PartyPaths._ContactPerson._RelationshipOwner,
				stageRecord.getValue(PartyPaths._STG_Party._RelationshipOwner));

			pathToValueMap.put(
				PartyPaths._ContactPerson._DataResponsible,
				stageRecord.getValue(PartyPaths._STG_Party._DataResponsible));

			pathToValueMap.put(
				PartyPaths._ContactPerson._ValidFrom,
				stageRecord.getValue(PartyPaths._STG_Party._ValidFrom));

			pathToValueMap.put(
				PartyPaths._ContactPerson._ValidTo,
				stageRecord.getValue(PartyPaths._STG_Party._ValidTo));

				pathToValueMap.put(
					PartyPaths._ContactPerson._PartyName,
					stageRecord.getValue(PartyPaths._STG_Party._OrgName));
				
				pathToValueMap.put(
					PartyPaths._ContactPerson._ValidTo,
					stageRecord.getValue(PartyPaths._STG_Party._ValidTo));


				String partyName = String
					.valueOf(stageRecord.getValue(PartyPaths._STG_Party._FirstName))
					.concat(" ")
					.concat(
						String.valueOf(
							stageRecord.getValue(PartyPaths._STG_Party._LastName)));

				partyName = partyName.replace("null", "");

				pathToValueMap.put(PartyPaths._ContactPerson._PartyName, partyName);

				pathToValueMap.put(PartyPaths._ContactPerson._PartyNameSearchText, PartyFunction.replaceUmlaut(partyName));

				pathToValueMap.put(
					PartyPaths._ContactPerson._LastName,
					stageRecord.getValue(PartyPaths._STG_Party._LastName));

				pathToValueMap.put(
					PartyPaths._ContactPerson._FirstName,
					stageRecord.getValue(PartyPaths._STG_Party._FirstName));

				pathToValueMap.put(
					PartyPaths._ContactPerson._DateOfBirth,
					stageRecord.getValue(PartyPaths._STG_Party._DateOfBirth));

				pathToValueMap.put(
					PartyPaths._ContactPerson._Gender,
					stageRecord.getValue(PartyPaths._STG_Party._Gender));

				pathToValueMap.put(
					PartyPaths._ContactPerson._Title,
					stageRecord.getValue(PartyPaths._STG_Party._Title));

				pathToValueMap.put(
					PartyPaths._ContactPerson._Salutation,
					stageRecord.getValue(PartyPaths._STG_Party._Salutation));

				pathToValueMap.put(
					PartyPaths._ContactPerson._LetterSalutation,
					stageRecord.getValue(PartyPaths._STG_Party._LetterSalutation));

				pathToValueMap.put(
					PartyPaths._ContactPerson._CivilStatus,
					stageRecord.getValue(PartyPaths._STG_Party._CivilStatus));

				pathToValueMap.put(
					PartyPaths._ContactPerson._LetterSalutation,
					stageRecord.getValue(PartyPaths._STG_Party._LetterSalutation));

			// Add Roles //

			pathToValueMap.put(
				PartyPaths._ContactPerson._Blacklist,
				stageRecord.getValue(PartyPaths._STG_Party._Blacklist));


			// Commuication //

			pathToValueMap.put(
				PartyPaths._ContactPerson._Language,
				stageRecord.getValue(PartyPaths._STG_Party._Language));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_Street,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_Street));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_Street2,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_Street2));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_Town,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_Town));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_PostCode,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_PostCode));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_POBox,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_POBox));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_POBoxPostCode,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_POBoxPostCode));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_POBoxTown,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_POBoxTown));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_District,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_District));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_StateProvince,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_StateProvince));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Legal_Country,
				stageRecord.getValue(PartyPaths._STG_Party._Legal_Country));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Email,
				stageRecord.getValue(PartyPaths._STG_Party._Email));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Telephone,
				stageRecord.getValue(PartyPaths._STG_Party._Telephone));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Mobile,
				stageRecord.getValue(PartyPaths._STG_Party._Mobile));

			pathToValueMap.put(
				PartyPaths._ContactPerson._Fax,
				stageRecord.getValue(PartyPaths._STG_Party._Fax));

			pathToValueMap.put(
				PartyPaths._ContactPerson._URL,
				stageRecord.getValue(PartyPaths._STG_Party._URL));

			// Control //
			pathToValueMap.put(
					PartyPaths._ContactPerson._SourceSystem,
					stageRecord.getValue(PartyPaths._STG_Party._SourceSystem));
		*/
		return null;
	}
	
	public HashMap<Path, Object> buildPartyIdentifierRecordFromStageRecord(ValueContext stageRecord) {
		
		HashMap<Path, Object> pathToValueMap = new HashMap<>();
		
		/*pathToValueMap.put(
				PartyPaths._PartyIdentifier._SourceSystem,
				stageRecord.getValue(PartyPaths._STG_Party._SourceSystem));
		
		pathToValueMap.put(
				PartyPaths._PartyIdentifier._SourcePartyBusId,
				stageRecord.getValue(PartyPaths._STG_Party._SourcePartyBusId));*/
		
		return pathToValueMap;
	}
	
}
