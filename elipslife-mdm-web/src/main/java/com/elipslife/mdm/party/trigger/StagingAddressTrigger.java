package com.elipslife.mdm.party.trigger;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import com.elipslife.ebx.data.access.CoreAddressBuilder;
import com.elipslife.ebx.data.access.StagingAddressReader;
import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public class StagingAddressTrigger extends TableTrigger {

	@Override
	public void setup(TriggerSetupContext aContext) {

	}

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		super.handleBeforeCreate(aContext);

		aContext.getOccurrenceContextForUpdate().setValue(new Date(),
				PartyPaths._STG_Address._Control_LastSyncTimestamp);

	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
		super.handleBeforeModify(aContext);

		aContext.getOccurrenceContextForUpdate().setValue(new Date(),
				PartyPaths._STG_Address._Control_LastSyncTimestamp);
	}

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {

		super.handleAfterCreate(aContext);

		boolean rootDataSpace = Objects.equals(PartyConstants.DataSpace.PARTY_REFERENCE,
				aContext.getAdaptationHome().getKey().getName());

		if (rootDataSpace) {
			StagingAddressBean stagingAddressBean = new StagingAddressReader()
					.createBeanFromRecord(aContext.getAdaptationOccurrence());

			boolean isWithinRange = dateWithinRange(stagingAddressBean.getValidFrom(), stagingAddressBean.getValidTo());

			if (!isWithinRange) {
				return;
			}

			boolean hasValidationErrors = hasValidationErrors(stagingAddressBean);
			if (hasValidationErrors) {
				return;
			}
			/*
			CoreAddressBuilder.createOrUpdateCoreAddressByStagingAddress(aContext.getProcedureContext(),
					stagingAddressBean);
				*/	
		}

	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext aContext) throws OperationException {

		super.handleAfterModify(aContext);

		boolean rootDataSpace = Objects.equals(PartyConstants.DataSpace.PARTY_REFERENCE,
				aContext.getAdaptationHome().getKey().getName());

		if (rootDataSpace) {

			StagingAddressBean stagingAddressBean = new StagingAddressReader()
					.createBeanFromRecord(aContext.getAdaptationOccurrence());

			boolean isWithinRange = dateWithinRange(stagingAddressBean.getValidFrom(), stagingAddressBean.getValidTo());

			if (!isWithinRange) {
				return;
			}

			boolean hasValidationErrors = hasValidationErrors(stagingAddressBean);
			if (hasValidationErrors) {
				return;
			}
			/*
			CoreAddressBuilder.createOrUpdateCoreAddressByStagingAddress(aContext.getProcedureContext(),
					stagingAddressBean);
				*/	
		}
	}

	StagingAddressBean stagingAddressBeanToDelete;

	@Override
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext aContext) throws OperationException {

		super.handleBeforeDelete(aContext);

		this.stagingAddressBeanToDelete = new StagingAddressReader()
				.createBeanFromRecord(aContext.getAdaptationOccurrence());
	}

	@Override
	public void handleAfterDelete(AfterDeleteOccurrenceContext aContext) throws OperationException {

		boolean rootDataSpace = Objects.equals(PartyConstants.DataSpace.PARTY_REFERENCE,
				aContext.getAdaptationHome().getKey().getName());

		if (rootDataSpace) {

			super.handleAfterDelete(aContext);

			boolean isWithinRange = dateWithinRange(this.stagingAddressBeanToDelete.getValidFrom(),
					this.stagingAddressBeanToDelete.getValidTo());

			if (!isWithinRange) {
				return;
			}
			
			
			CoreAddressBuilder.deleteCoreAddressByStagingAddress(aContext.getProcedureContext(),
					stagingAddressBeanToDelete);
					
		}
	}

	/**
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @return
	 */
	private boolean dateWithinRange(Date dateFrom, Date dateTo) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		Date dateNow = calendar.getTime();

		Date dateMinimum = new Date(Long.MIN_VALUE);
		Date dateMaximum = new Date(Long.MAX_VALUE);

		return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0
				&& dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
	}

	/**
	 * 
	 * @param stagingPartyBean
	 * @return
	 */
	private static boolean hasValidationErrors(StagingAddressBean stagingAddressBean) {

		StagingAddressReader stagingAddressReader = new StagingAddressReader();

		boolean hasValidationErrors = stagingAddressReader
				.hasValidationErrors(new Object[] { stagingAddressBean.getId() });

		return hasValidationErrors;
	}
}
