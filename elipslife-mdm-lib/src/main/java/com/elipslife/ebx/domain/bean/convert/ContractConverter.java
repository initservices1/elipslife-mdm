package com.elipslife.ebx.domain.bean.convert;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

import com.elipslife.mdm.contract.constants.ContractConstants.RecordOperations;
import com.elipslife.mdm.contract.core.CoreContractCoverBean;
import com.elipslife.mdm.contract.core.CoreExposureBean;
import com.elipslife.mdm.contract.core.CoreInsuranceContractBean;
import com.elipslife.mdm.contract.landing.LandingContractCoverBean;
import com.elipslife.mdm.contract.landing.LandingExposureBean;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractBean;
import com.elipslife.mdm.contract.staging.StagingContractCoverBean;
import com.elipslife.mdm.contract.staging.StagingExposureBean;
import com.elipslife.mdm.contract.staging.StagingInsuranceContractBean;
import com.elipslife.mdm.path.ContractPaths;
import com.orchestranetworks.schema.Path;

public class ContractConverter {
	
	/**
	 * 
	 * @param landingInsuranceContractBean
	 * @return
	 */
	public static StagingInsuranceContractBean createStagingInsuranceContractBeanFromLandingInsuranceContractBean(LandingInsuranceContractBean landingInsuranceContractBean) {
		
		StagingInsuranceContractBean stagingInsuranceContractBean = new StagingInsuranceContractBean();
		fillStagingInsuranceContractBeanWithLandingInsuranceContractBean(landingInsuranceContractBean, stagingInsuranceContractBean);
		return stagingInsuranceContractBean;
	}
	
	
	/**
	 * 
	 * @param landingInsuranceContractBean
	 * @param stagingInsuranceContractBean
	 */
	public static void fillStagingInsuranceContractBeanWithLandingInsuranceContractBean(LandingInsuranceContractBean landingInsuranceContractBean, StagingInsuranceContractBean stagingInsuranceContractBean) {
		
		stagingInsuranceContractBean.setAction(landingInsuranceContractBean.getAction());
		stagingInsuranceContractBean.setActionBy(landingInsuranceContractBean.getActionBy());
		stagingInsuranceContractBean.setAdminResponsibleUserName(landingInsuranceContractBean.getAdminResponsibleUserName());
		stagingInsuranceContractBean.setAlternativeContractName(landingInsuranceContractBean.getAlternativeContractName());
		stagingInsuranceContractBean.setApplicationNumber(landingInsuranceContractBean.getApplicationNumber());
		stagingInsuranceContractBean.setApplicationState(landingInsuranceContractBean.getApplicationState());
		stagingInsuranceContractBean.setAttachedPartnerBusId(landingInsuranceContractBean.getAttachedPartnerBusId());
		stagingInsuranceContractBean.setBrokerBusId(landingInsuranceContractBean.getBrokerBusId());
		stagingInsuranceContractBean.setBusinessContractBeginDate(landingInsuranceContractBean.getBusinessContractBeginDate());
		stagingInsuranceContractBean.setBusinessScope(landingInsuranceContractBean.getBusinessScope());
		stagingInsuranceContractBean.setCaseMgmtResponsibleUserName(landingInsuranceContractBean.getCaseMgmtResponsibleUserName());
		stagingInsuranceContractBean.setClaimResponsibleUserName(landingInsuranceContractBean.getClaimResponsibleUserName());
		stagingInsuranceContractBean.setClientSegment(landingInsuranceContractBean.getClientSegment());
		stagingInsuranceContractBean.setContractBeginDate(landingInsuranceContractBean.getContractBeginDate());
		stagingInsuranceContractBean.setContractBusId2(landingInsuranceContractBean.getContractBusId2());
		stagingInsuranceContractBean.setContractClass(landingInsuranceContractBean.getContractClass());
		stagingInsuranceContractBean.setContractEndDate(landingInsuranceContractBean.getContractEndDate());
		stagingInsuranceContractBean.setContractExpiryDate(landingInsuranceContractBean.getContractExpiryDate());
		stagingInsuranceContractBean.setContractName(landingInsuranceContractBean.getContractName());
		stagingInsuranceContractBean.setContractNumber(landingInsuranceContractBean.getContractNumber());
		stagingInsuranceContractBean.setContractStage(landingInsuranceContractBean.getContractStage());
		stagingInsuranceContractBean.setContractStageBusId(landingInsuranceContractBean.getContractStageBusId());
		stagingInsuranceContractBean.setContractState(landingInsuranceContractBean.getContractState());
		stagingInsuranceContractBean.setCreationTimestamp(landingInsuranceContractBean.getCreationTimestamp());
		stagingInsuranceContractBean.setCustomerRefNumber(landingInsuranceContractBean.getCustomerRefNumber());
		stagingInsuranceContractBean.setDataOwner(landingInsuranceContractBean.getDataOwner());
		stagingInsuranceContractBean.setDistributionAgreementBusId(landingInsuranceContractBean.getDistributionAgreementBusId());
		stagingInsuranceContractBean.setIsActive(landingInsuranceContractBean.getIsActive());
		stagingInsuranceContractBean.setIsProfitParticipation(landingInsuranceContractBean.getIsProfitParticipation());
		stagingInsuranceContractBean.setLastUpdateTimestamp(landingInsuranceContractBean.getLastUpdateTimestamp());
		stagingInsuranceContractBean.setLineOfBusiness(landingInsuranceContractBean.getLineOfBusiness());
		stagingInsuranceContractBean.setOfferNumber(landingInsuranceContractBean.getOfferNumber());
		stagingInsuranceContractBean.setOfferState(landingInsuranceContractBean.getOfferState());
		stagingInsuranceContractBean.setOperatingCountry(landingInsuranceContractBean.getOperatingCountry());
		stagingInsuranceContractBean.setPolicyholderBusId(landingInsuranceContractBean.getPolicyholderBusId());
		stagingInsuranceContractBean.setSalesResponsibleUserName(landingInsuranceContractBean.getSalesResponsibleUserName());
		stagingInsuranceContractBean.setSourceAdminResponsibleBusId(landingInsuranceContractBean.getSourceAdminResponsibleBusId());
		stagingInsuranceContractBean.setSourceAttachedPartnerBusId(landingInsuranceContractBean.getSourceAttachedPartnerBusId());
		stagingInsuranceContractBean.setSourceBrokerBusId(landingInsuranceContractBean.getSourceBrokerBusId());
		stagingInsuranceContractBean.setSourceCaseMgmtResponsibleBusId(landingInsuranceContractBean.getSourceCaseMgmtResponsibleBusId());
		stagingInsuranceContractBean.setSourceClaimResponsibleBusId(landingInsuranceContractBean.getSourceClaimResponsibleBusId());
		stagingInsuranceContractBean.setSourceContractBusId(landingInsuranceContractBean.getSourceContractBusId());
		stagingInsuranceContractBean.setSourcePolicyholderBusId(landingInsuranceContractBean.getSourcePolicyholderBusId());
		stagingInsuranceContractBean.setSourceSalesResponsibleBusId(landingInsuranceContractBean.getSourceSalesResponsibleBusId());
		stagingInsuranceContractBean.setSourceSystem(landingInsuranceContractBean.getSourceSystem());
	}
	
	public static HashMap<Path, Object> fillStagingInsuranceContractWithLandingInsuranceContractBean(LandingInsuranceContractBean landingInsuranceContractBean, boolean useDefaultValues) {
	    
	    HashMap<Path, Object> stagingInsuranceContractValues = new HashMap<Path, Object>();
        
        if(!useDefaultValues || (landingInsuranceContractBean.getDataOwner() != null && !landingInsuranceContractBean.getDataOwner().isEmpty())) {
            stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractIds_DataOwner, landingInsuranceContractBean.getDataOwner());
        }
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractIds_ContractStageBusId, landingInsuranceContractBean.getContractStageBusId());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractIds_ContractNumber, landingInsuranceContractBean.getContractNumber());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractIds_OfferNumber, landingInsuranceContractBean.getOfferNumber());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractIds_ApplicationNumber, landingInsuranceContractBean.getApplicationNumber());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractIds_ContractBusId2, landingInsuranceContractBean.getContractBusId2());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractIds_CustomerRefNumber, landingInsuranceContractBean.getCustomerRefNumber());
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractStage, landingInsuranceContractBean.getContractStage());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractState, landingInsuranceContractBean.getContractState());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_OfferState, landingInsuranceContractBean.getOfferState());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_ApplicationState, landingInsuranceContractBean.getApplicationState());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractClass, landingInsuranceContractBean.getContractClass());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_BusinessContractBeginDate, landingInsuranceContractBean.getBusinessContractBeginDate());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractExpiryDate, landingInsuranceContractBean.getContractExpiryDate());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractBeginDate, landingInsuranceContractBean.getContractBeginDate());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractEndDate, landingInsuranceContractBean.getContractEndDate());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_BusinessScope, landingInsuranceContractBean.getBusinessScope());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractName, landingInsuranceContractBean.getContractName());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_AlternativeContractName, landingInsuranceContractBean.getAlternativeContractName());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_ClientSegment, landingInsuranceContractBean.getClientSegment());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_DistributionAgreementBusId, landingInsuranceContractBean.getDistributionAgreementBusId());
        
        if(!useDefaultValues || landingInsuranceContractBean.getIsActive() != null) {
            stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_IsActive, landingInsuranceContractBean.getIsActive());
        }
        
        if(!useDefaultValues || landingInsuranceContractBean.getIsProfitParticipation() != null) {
            stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_IsProfitParticipation, landingInsuranceContractBean.getIsProfitParticipation());
        }
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_LineOfBusiness, landingInsuranceContractBean.getLineOfBusiness());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._ContractCore_OperatingCountry, landingInsuranceContractBean.getOperatingCountry());
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_Policyholder_SourcePolicyholderBusId, landingInsuranceContractBean.getSourcePolicyholderBusId());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_Policyholder_PolicyholderBusId, landingInsuranceContractBean.getPolicyholderBusId());
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_Broker_SourceBrokerBusId, landingInsuranceContractBean.getSourceBrokerBusId());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_Broker_BrokerBusId, landingInsuranceContractBean.getBrokerBusId());
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_AttachedPartner_SourceAttachedPartnerBusId, landingInsuranceContractBean.getSourceAttachedPartnerBusId());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_AttachedPartner_AttachedPartnerBusId, landingInsuranceContractBean.getAttachedPartnerBusId());
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_AdminResponsible_SourceAdminResponsibleBusId, landingInsuranceContractBean.getSourceAdminResponsibleBusId());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_AdminResponsible_AdminResponsibleUserName, landingInsuranceContractBean.getAdminResponsibleUserName());
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_ClaimResponsible_SourceClaimResponsibleBusId, landingInsuranceContractBean.getSourceClaimResponsibleBusId());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleUserName, landingInsuranceContractBean.getClaimResponsibleUserName());
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_CaseMgmtResponsible_SourceCaseMgmtResponsibleBusId, landingInsuranceContractBean.getSourceCaseMgmtResponsibleBusId());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleUserName, landingInsuranceContractBean.getCaseMgmtResponsibleUserName());
        
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_SalesResponsible_SourceSalesResponsibleBusId, landingInsuranceContractBean.getSourceSalesResponsibleBusId());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Parties_SalesResponsible_SalesResponsibleUserName, landingInsuranceContractBean.getSalesResponsibleUserName());
        // Control
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Control_SourceSystem, landingInsuranceContractBean.getSourceSystem());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Control_SourceContractBusId, landingInsuranceContractBean.getSourceContractBusId());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Control_CreationTimestamp, landingInsuranceContractBean.getCreationTimestamp());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Control_LastUpdateTimestamp, landingInsuranceContractBean.getLastUpdateTimestamp());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Control_LastSyncTimestamp, new Date());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Control_Action, landingInsuranceContractBean.getAction());
        stagingInsuranceContractValues.put(ContractPaths._STG_InsuranceContract._Control_ActionBy, landingInsuranceContractBean.getActionBy());
        
        return stagingInsuranceContractValues;
	}
	
	
	/**
	 * 
	 * @param stagingInsuranceContractBean
	 * @return
	 */
	public static CoreInsuranceContractBean createCoreInsuranceContractBeanFromStagingInsuranceContractBean(StagingInsuranceContractBean stagingInsuranceContractBean) {
		CoreInsuranceContractBean coreInsuranceContractBean = new CoreInsuranceContractBean();
		fillCoreInsuranceContractBeanWithStagingInsuranceContractBeanBean(stagingInsuranceContractBean, coreInsuranceContractBean);
		return coreInsuranceContractBean;
	}
	
	
	/**
	 * 
	 * @param stagingInsuranceContractBean
	 * @param coreInsuranceContractBean
	 */
	public static void fillCoreInsuranceContractBeanWithStagingInsuranceContractBeanBean(StagingInsuranceContractBean stagingInsuranceContractBean, CoreInsuranceContractBean coreInsuranceContractBean) {
		
		coreInsuranceContractBean.setAdminResponsibleUserName(stagingInsuranceContractBean.getAdminResponsibleUserName());
		coreInsuranceContractBean.setAlternativeContractName(stagingInsuranceContractBean.getAlternativeContractName());
		coreInsuranceContractBean.setApplicationNumber(stagingInsuranceContractBean.getApplicationNumber());
		coreInsuranceContractBean.setApplicationState(stagingInsuranceContractBean.getApplicationState());
		coreInsuranceContractBean.setAttachedPartnerBusId(stagingInsuranceContractBean.getAttachedPartnerBusId());
		coreInsuranceContractBean.setBrokerBusId(stagingInsuranceContractBean.getBrokerBusId());
		coreInsuranceContractBean.setBusinessContractBeginDate(stagingInsuranceContractBean.getBusinessContractBeginDate());
		coreInsuranceContractBean.setBusinessScope(stagingInsuranceContractBean.getBusinessScope());
		coreInsuranceContractBean.setCaseMgmtResponsibleUserName(stagingInsuranceContractBean.getCaseMgmtResponsibleUserName());
		coreInsuranceContractBean.setClaimResponsibleUserName(stagingInsuranceContractBean.getClaimResponsibleUserName());
		coreInsuranceContractBean.setClientSegment(stagingInsuranceContractBean.getClientSegment());
		coreInsuranceContractBean.setContractBeginDate(stagingInsuranceContractBean.getContractBeginDate());
		coreInsuranceContractBean.setContractBusId2(stagingInsuranceContractBean.getContractBusId2());
		coreInsuranceContractBean.setContractClass(stagingInsuranceContractBean.getContractClass());
		coreInsuranceContractBean.setContractEndDate(stagingInsuranceContractBean.getContractEndDate());
		coreInsuranceContractBean.setContractExpiryDate(stagingInsuranceContractBean.getContractExpiryDate());
		coreInsuranceContractBean.setContractName(stagingInsuranceContractBean.getContractName());
		coreInsuranceContractBean.setContractNumber(stagingInsuranceContractBean.getContractNumber());
		coreInsuranceContractBean.setContractStage(stagingInsuranceContractBean.getContractStage());
		coreInsuranceContractBean.setContractStageBusId(stagingInsuranceContractBean.getContractStageBusId());
		coreInsuranceContractBean.setContractState(stagingInsuranceContractBean.getContractState());
		coreInsuranceContractBean.setCreationTimestamp(stagingInsuranceContractBean.getCreationTimestamp());
		coreInsuranceContractBean.setCustomerRefNumber(stagingInsuranceContractBean.getCustomerRefNumber());
		coreInsuranceContractBean.setDataOwner(stagingInsuranceContractBean.getDataOwner());
		coreInsuranceContractBean.setDistributionAgreementBusId(stagingInsuranceContractBean.getDistributionAgreementBusId());
		coreInsuranceContractBean.setIsActive(stagingInsuranceContractBean.getIsActive());
		coreInsuranceContractBean.setIsProfitParticipation(stagingInsuranceContractBean.getIsProfitParticipation());
		coreInsuranceContractBean.setLastUpdateTimestamp(stagingInsuranceContractBean.getLastUpdateTimestamp());
		coreInsuranceContractBean.setLastSyncAction(stagingInsuranceContractBean.getAction());
		coreInsuranceContractBean.setLineOfBusiness(stagingInsuranceContractBean.getLineOfBusiness());
		coreInsuranceContractBean.setOfferNumber(stagingInsuranceContractBean.getOfferNumber());
		coreInsuranceContractBean.setOfferState(stagingInsuranceContractBean.getOfferState());
		coreInsuranceContractBean.setOperatingCountry(stagingInsuranceContractBean.getOperatingCountry());
		coreInsuranceContractBean.setPolicyholderBusId(stagingInsuranceContractBean.getPolicyholderBusId());
		coreInsuranceContractBean.setSalesResponsibleUserName(stagingInsuranceContractBean.getSalesResponsibleUserName());
		coreInsuranceContractBean.setSourceAdminResponsibleBusId(stagingInsuranceContractBean.getSourceAdminResponsibleBusId());
		coreInsuranceContractBean.setSourceAttachedPartnerBusId(stagingInsuranceContractBean.getSourceAttachedPartnerBusId());
		coreInsuranceContractBean.setSourceBrokerBusId(stagingInsuranceContractBean.getSourceBrokerBusId());
		coreInsuranceContractBean.setSourceCaseMgmtResponsibleBusId(stagingInsuranceContractBean.getSourceCaseMgmtResponsibleBusId());
		coreInsuranceContractBean.setSourceClaimResponsibleBusId(stagingInsuranceContractBean.getSourceClaimResponsibleBusId());
		coreInsuranceContractBean.setSourceContractBusId(stagingInsuranceContractBean.getSourceContractBusId());
		coreInsuranceContractBean.setSourcePolicyholderBusId(stagingInsuranceContractBean.getSourcePolicyholderBusId());
		coreInsuranceContractBean.setSourceSalesResponsibleBusId(stagingInsuranceContractBean.getSourceSalesResponsibleBusId());
		coreInsuranceContractBean.setSourceSystem(stagingInsuranceContractBean.getSourceSystem());
		
		if(Objects.equals(RecordOperations.CREATE, stagingInsuranceContractBean.getAction())) {
			coreInsuranceContractBean.setCreatedBy(stagingInsuranceContractBean.getActionBy());
		}
		
		if(Objects.equals(RecordOperations.UPDATE, stagingInsuranceContractBean.getAction())) {
			coreInsuranceContractBean.setLastUpdatedBy(stagingInsuranceContractBean.getActionBy());
		}
	}
	
	
	/**
	 * 
	 * @param landingExposureBean
	 * @return
	 */
	public static StagingExposureBean createStagingExposureBeanFromLandingExposureBean(LandingExposureBean landingExposureBean) {
		
		StagingExposureBean stagingExposureBean = new StagingExposureBean();
		fillStagingExposureBeanWithLandingExposureBean(landingExposureBean, stagingExposureBean);
		return stagingExposureBean;
	}
	
	
	/**
	 * 
	 * @param landingExposureBean
	 * @param stagingExposureBean
	 */
	public static void fillStagingExposureBeanWithLandingExposureBean(LandingExposureBean landingExposureBean, StagingExposureBean stagingExposureBean) {
		
		stagingExposureBean.setAction(landingExposureBean.getAction());
		stagingExposureBean.setActionBy(landingExposureBean.getActionBy());
		stagingExposureBean.setCreationTimestamp(landingExposureBean.getCreationTimestamp());
		stagingExposureBean.setExposureBusId(landingExposureBean.getExposureBusId());
		stagingExposureBean.setLastUpdateTimestamp(landingExposureBean.getLastUpdateTimestamp());
		stagingExposureBean.setName(landingExposureBean.getName());
		stagingExposureBean.setProductBusId(landingExposureBean.getProductBusId());
		stagingExposureBean.setProductGroup(landingExposureBean.getProductGroup());
		stagingExposureBean.setProductName(landingExposureBean.getProductName());
		stagingExposureBean.setSourceContractBusId(landingExposureBean.getSourceContractBusId());
		stagingExposureBean.setSourceProductBusId(landingExposureBean.getSourceProductBusId());
		stagingExposureBean.setSourceSystem(landingExposureBean.getSourceSystem());
	}
	
	/**
	 * 
	 * @param stagingExposureBean
	 * @return
	 */
	public static CoreExposureBean createCoreExposureBeanFromStagingExposureBean(StagingExposureBean stagingExposureBean) {
		
		CoreExposureBean coreExposureBean = new CoreExposureBean();
		fillCoreExposureBeanWithStagingExposureBean(stagingExposureBean, coreExposureBean);
		return coreExposureBean;
	}
	
	
	/**
	 * 
	 * @param stagingExposureBean
	 * @param coreExposureBean
	 */
	public static void fillCoreExposureBeanWithStagingExposureBean(StagingExposureBean stagingExposureBean, CoreExposureBean coreExposureBean) {
		
		coreExposureBean.setCreationTimestamp(stagingExposureBean.getCreationTimestamp());
		coreExposureBean.setExposureBusId(stagingExposureBean.getExposureBusId());
		coreExposureBean.setLastUpdateTimestamp(stagingExposureBean.getLastUpdateTimestamp());
		coreExposureBean.setLastSyncAction(stagingExposureBean.getAction());
		coreExposureBean.setName(stagingExposureBean.getName());
		coreExposureBean.setProductBusId(stagingExposureBean.getProductBusId());
		coreExposureBean.setProductGroup(stagingExposureBean.getProductGroup());
		coreExposureBean.setProductName(stagingExposureBean.getProductName());
		coreExposureBean.setSourceSystem(stagingExposureBean.getSourceSystem());
		
		if(Objects.equals(RecordOperations.CREATE, stagingExposureBean.getAction())) {
			coreExposureBean.setCreatedBy(stagingExposureBean.getActionBy());
		}
		
		if(Objects.equals(RecordOperations.UPDATE, stagingExposureBean.getAction())) {
			coreExposureBean.setLastUpdatedBy(stagingExposureBean.getActionBy());
		}
	}
	
	/**
	 * 
	 * @param landingContractCoverBean
	 * @return
	 */
	public static StagingContractCoverBean createStagingContractCoverBeanFromLandingContractCoverBean(LandingContractCoverBean landingContractCoverBean) {
		
		StagingContractCoverBean stagingContractCoverBean = new StagingContractCoverBean();
		fillStagingContractCoverBeanWithLandingContractCoverBean(landingContractCoverBean, stagingContractCoverBean);
		return stagingContractCoverBean;
	}
	
	
	/**
	 * 
	 * @param landingContractCoverBean
	 * @param stagingContractCoverBean
	 */
	public static void fillStagingContractCoverBeanWithLandingContractCoverBean(LandingContractCoverBean landingContractCoverBean, StagingContractCoverBean stagingContractCoverBean) {
		
		stagingContractCoverBean.setAction(landingContractCoverBean.getAction());
		stagingContractCoverBean.setActionBy(landingContractCoverBean.getActionBy());
		stagingContractCoverBean.setBeneficiaryType(landingContractCoverBean.getBeneficiaryType());
		stagingContractCoverBean.setCoverBusId(landingContractCoverBean.getCoverBusId());
		stagingContractCoverBean.setCoverGroupType(landingContractCoverBean.getCoverGroupType());
		stagingContractCoverBean.setCoverGroupTypeName(landingContractCoverBean.getCoverGroupTypeName());
		stagingContractCoverBean.setCoverName(landingContractCoverBean.getCoverName());
		stagingContractCoverBean.setCoverTemplateBusId(landingContractCoverBean.getCoverTemplateBusId());
		stagingContractCoverBean.setCreationTimestamp(landingContractCoverBean.getCreationTimestamp());
		stagingContractCoverBean.setExposureBusId(landingContractCoverBean.getExposureBusId());
		stagingContractCoverBean.setLastUpdateTimestamp(landingContractCoverBean.getLastUpdateTimestamp());
		stagingContractCoverBean.setSourceSystem(landingContractCoverBean.getSourceSystem());
	}
	
	
	/**
	 * 
	 * @param stagingContractCoverBean
	 * @return
	 */
	public static CoreContractCoverBean createCoreContractCoverBeanFromStagingContractCoverBean(StagingContractCoverBean stagingContractCoverBean) {
		
		CoreContractCoverBean coreContractCoverBean = new CoreContractCoverBean();
		fillCoreContractCoverBeanWithStagingContractCoverBean(stagingContractCoverBean, coreContractCoverBean);
		return coreContractCoverBean;
	}
	
	
	/**
	 * 
	 * @param stagingContractCoverBean
	 * @param coreContractCoverBean
	 */
	public static void fillCoreContractCoverBeanWithStagingContractCoverBean(StagingContractCoverBean stagingContractCoverBean, CoreContractCoverBean coreContractCoverBean) {
		
		coreContractCoverBean.setBeneficiaryType(stagingContractCoverBean.getBeneficiaryType());
		coreContractCoverBean.setCoverBusId(stagingContractCoverBean.getCoverBusId());
		coreContractCoverBean.setCoverGroupType(stagingContractCoverBean.getCoverGroupType());
		coreContractCoverBean.setCoverGroupTypeName(stagingContractCoverBean.getCoverGroupTypeName());
		coreContractCoverBean.setCoverName(stagingContractCoverBean.getCoverName());
		coreContractCoverBean.setCoverTemplateBusId(stagingContractCoverBean.getCoverTemplateBusId());
		coreContractCoverBean.setCreationTimestamp(stagingContractCoverBean.getCreationTimestamp());
		coreContractCoverBean.setExposureBusId(stagingContractCoverBean.getExposureBusId());
		coreContractCoverBean.setLastUpdateTimestamp(stagingContractCoverBean.getLastUpdateTimestamp());
		coreContractCoverBean.setLastSyncAction(stagingContractCoverBean.getAction());
		coreContractCoverBean.setSourceSystem(stagingContractCoverBean.getSourceSystem());
		
		if(Objects.equals(RecordOperations.CREATE, stagingContractCoverBean.getAction())) {
			coreContractCoverBean.setCreatedBy(stagingContractCoverBean.getActionBy());
		}
		
		if(Objects.equals(RecordOperations.UPDATE, stagingContractCoverBean.getAction())) {
			coreContractCoverBean.setLastUpdatedBy(stagingContractCoverBean.getActionBy());
		}
	}
	
	public static HashMap<Path, Object> fillStagingContractCoverWithLandingContractCoverBean(LandingContractCoverBean coverBean) {
	    
	    HashMap<Path, Object> stagingContractCoverValues = new HashMap<Path, Object>();
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Exposure_ExposureBusId, coverBean.getExposureBusId());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Cover_CoverBusId, coverBean.getCoverBusId());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Cover_CoverGroupType, coverBean.getCoverGroupType());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Cover_CoverGroupTypeName, coverBean.getCoverGroupTypeName());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Cover_CoverName, coverBean.getCoverName());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Cover_CoverTemplateBusId, coverBean.getCoverTemplateBusId());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Cover_BeneficiaryType, coverBean.getBeneficiaryType());
        
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Control_SourceSystem, coverBean.getSourceSystem());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Control_CreationTimestamp, coverBean.getCreationTimestamp());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Control_LastUpdateTimestamp, coverBean.getLastUpdateTimestamp());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Control_LastSyncTimestamp, new Date());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Control_Action, coverBean.getAction());
        stagingContractCoverValues.put(ContractPaths._STG_ContractCover._Control_ActionBy, coverBean.getActionBy());
        
	    return stagingContractCoverValues;
	}
	
	public static HashMap<Path, Object> fillStagingExposureWithLandingExposureBean(LandingExposureBean bean) {
        
        HashMap<Path, Object> stagingExposureValues = new HashMap<Path, Object>();
        stagingExposureValues.put(ContractPaths._STG_Exposure._Exposure_ExposureBusId, bean.getExposureBusId());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Exposure_Name, bean.getName());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Exposure_ProductBusId, bean.getProductBusId());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Exposure_ProductGroup, bean.getProductGroup());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Exposure_ProductName, bean.getProductName());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Exposure_SourceProductBusId, bean.getSourceProductBusId());
        
        stagingExposureValues.put(ContractPaths._STG_Exposure._Control_SourceSystem, bean.getSourceSystem());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Control_SourceContractBusId, bean.getSourceContractBusId());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Control_CreationTimestamp, bean.getCreationTimestamp());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Control_LastSyncTimestamp, new Date());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Control_Action, bean.getAction());
        stagingExposureValues.put(ContractPaths._STG_Exposure._Control_ActionBy, bean.getActionBy());
        
        return stagingExposureValues;
    }
	
	public static HashMap<Path, Object> fillCoreInsuranceContractBeanWithStagingInsuranceContractBean(CoreInsuranceContractBean coreInsuranceContractBean, boolean useDefaultValues) {
	    HashMap<Path, Object> coreInsuranceContractValues = new HashMap<Path, Object>();
	    
	    coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractIds_InsuranceContractId, coreInsuranceContractBean.getId());
        
        if(!useDefaultValues || (coreInsuranceContractBean.getDataOwner() != null && !coreInsuranceContractBean.getDataOwner().isEmpty())) {
            coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractIds_DataOwner, coreInsuranceContractBean.getDataOwner());
        }
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractIds_InsuranceContractUUID, coreInsuranceContractBean.getInsuranceContractUUID());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractIds_ContractStageBusId, coreInsuranceContractBean.getContractStageBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractIds_ContractNumber, coreInsuranceContractBean.getContractNumber());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractIds_OfferNumber, coreInsuranceContractBean.getOfferNumber());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractIds_ApplicationNumber, coreInsuranceContractBean.getApplicationNumber());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractIds_ContractBusId2, coreInsuranceContractBean.getContractBusId2());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractIds_CustomerRefNumber, coreInsuranceContractBean.getCustomerRefNumber());
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_ContractStage, coreInsuranceContractBean.getContractStage());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_ContractState, coreInsuranceContractBean.getContractState());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_OfferState, coreInsuranceContractBean.getOfferState());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_ApplicationState, coreInsuranceContractBean.getApplicationState());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_ContractClass, coreInsuranceContractBean.getContractClass());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_BusinessContractBeginDate, coreInsuranceContractBean.getBusinessContractBeginDate());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_ContractExpiryDate, coreInsuranceContractBean.getContractExpiryDate());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_ContractBeginDate, coreInsuranceContractBean.getContractBeginDate());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_ContractEndDate, coreInsuranceContractBean.getContractEndDate());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_BusinessScope, coreInsuranceContractBean.getBusinessScope());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_ContractName, coreInsuranceContractBean.getContractName());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_AlternativeContractName, coreInsuranceContractBean.getAlternativeContractName());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_ClientSegment, coreInsuranceContractBean.getClientSegment());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_DistributionAgreementBusId, coreInsuranceContractBean.getDistributionAgreementBusId());
        
        if(!useDefaultValues || coreInsuranceContractBean.getIsActive() != null) {
            coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_IsActive, coreInsuranceContractBean.getIsActive());
        }
        
        if(!useDefaultValues || coreInsuranceContractBean.getIsProfitParticipation() != null) {
            coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_IsProfitParticipation, coreInsuranceContractBean.getIsProfitParticipation());
        }
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_LineOfBusiness, coreInsuranceContractBean.getLineOfBusiness());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._ContractCore_OperatingCountry, coreInsuranceContractBean.getOperatingCountry());
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_Policyholder_SourcePolicyholderBusId, coreInsuranceContractBean.getSourcePolicyholderBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_Policyholder_PolicyholderBusId, coreInsuranceContractBean.getPolicyholderBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_Policyholder_PolicyholderId, coreInsuranceContractBean.getPolicyholderId());
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_Broker_SourceBrokerBusId, coreInsuranceContractBean.getSourceBrokerBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_Broker_BrokerBusId, coreInsuranceContractBean.getBrokerBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_Broker_BrokerId, coreInsuranceContractBean.getBrokerId());
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_AttachedPartner_SourceAttachedPartnerBusId, coreInsuranceContractBean.getSourceAttachedPartnerBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_AttachedPartner_AttachedPartnerBusId, coreInsuranceContractBean.getAttachedPartnerBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_AttachedPartner_AttachedPartnerId, coreInsuranceContractBean.getAttachedPartnerId());
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_AdminResponsible_SourceAdminResponsibleBusId, coreInsuranceContractBean.getSourceAdminResponsibleBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_AdminResponsible_AdminResponsibleUserName, coreInsuranceContractBean.getAdminResponsibleUserName());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_AdminResponsible_AdminResponsibleBusId, coreInsuranceContractBean.getAdminResponsibleBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_AdminResponsible_AdminResponsibleId, coreInsuranceContractBean.getAdminResponsibleId());
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_ClaimResponsible_SourceClaimResponsibleBusId, coreInsuranceContractBean.getSourceClaimResponsibleBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleUserName, coreInsuranceContractBean.getClaimResponsibleUserName());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleBusId, coreInsuranceContractBean.getClaimResponsibleBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleId, coreInsuranceContractBean.getClaimResponsibleId());
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_SourceCaseMgmtResponsibleBusId, coreInsuranceContractBean.getSourceCaseMgmtResponsibleBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleUserName, coreInsuranceContractBean.getCaseMgmtResponsibleUserName());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleBusId, coreInsuranceContractBean.getCaseMgmtResponsibleBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleId, coreInsuranceContractBean.getCaseMgmtResponsibleId());
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_SalesResponsible_SourceSalesResponsibleBusId, coreInsuranceContractBean.getSourceSalesResponsibleBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_SalesResponsible_SalesResponsibleUserName, coreInsuranceContractBean.getSalesResponsibleUserName());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_SalesResponsible_SalesResponsibleBusId, coreInsuranceContractBean.getSalesResponsibleBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Parties_SalesResponsible_SalesResponsibleId, coreInsuranceContractBean.getSalesResponsibleId());
        
        if(!useDefaultValues || coreInsuranceContractBean.getIsExpired() != null) {
            coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Control_IsExpired, coreInsuranceContractBean.getIsExpired());
        }
        
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Control_SourceSystem, coreInsuranceContractBean.getSourceSystem());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Control_SourceContractBusId, coreInsuranceContractBean.getSourceContractBusId());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Control_CreatedBy, coreInsuranceContractBean.getCreatedBy());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Control_CreationTimestamp, coreInsuranceContractBean.getCreationTimestamp());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Control_LastUpdatedBy, coreInsuranceContractBean.getLastUpdatedBy());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Control_LastUpdateTimestamp, coreInsuranceContractBean.getLastUpdateTimestamp());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Control_LastSyncAction, coreInsuranceContractBean.getLastSyncAction());
        coreInsuranceContractValues.put(ContractPaths._InsuranceContract._Control_LastSyncTimestamp, coreInsuranceContractBean.getLastSyncTimestamp());
        return coreInsuranceContractValues;
	}
	
	/**
	 * Fill Contract Cover Bean values
	 * 
	 * @param coreContractCoverBean
	 * @return
	 */
	public static HashMap<Path, Object> fillContractCoverBeanValues(CoreContractCoverBean coreContractCoverBean) {
		
		HashMap<Path, Object> coreContractCoverMap = new HashMap<Path, Object>();
		coreContractCoverMap.put(ContractPaths._ContractCover._Exposure_ExposureId, coreContractCoverBean.getExposureId());
		coreContractCoverMap.put(ContractPaths._ContractCover._Exposure_ExposureBusId, coreContractCoverBean.getExposureBusId());
		
		coreContractCoverMap.put(ContractPaths._ContractCover._Cover_ContractCoverId, coreContractCoverBean.getId());
		coreContractCoverMap.put(ContractPaths._ContractCover._Cover_CoverBusId, coreContractCoverBean.getCoverBusId());
		coreContractCoverMap.put(ContractPaths._ContractCover._Cover_CoverGroupType, coreContractCoverBean.getCoverGroupType());
		coreContractCoverMap.put(ContractPaths._ContractCover._Cover_CoverGroupTypeName, coreContractCoverBean.getCoverGroupTypeName());
		coreContractCoverMap.put(ContractPaths._ContractCover._Cover_CoverName, coreContractCoverBean.getCoverName());
		coreContractCoverMap.put(ContractPaths._ContractCover._Cover_CoverTemplateBusId, coreContractCoverBean.getCoverTemplateBusId());
		coreContractCoverMap.put(ContractPaths._ContractCover._Cover_BeneficiaryType, coreContractCoverBean.getBeneficiaryType());
		
		coreContractCoverMap.put(ContractPaths._ContractCover._Control_SourceSystem, coreContractCoverBean.getSourceSystem());
		coreContractCoverMap.put(ContractPaths._ContractCover._Control_CreatedBy, coreContractCoverBean.getCreatedBy());
		coreContractCoverMap.put(ContractPaths._ContractCover._Control_CreationTimestamp, coreContractCoverBean.getCreationTimestamp());
		coreContractCoverMap.put(ContractPaths._ContractCover._Control_LastUpdatedBy, coreContractCoverBean.getLastUpdatedBy());
		coreContractCoverMap.put(ContractPaths._ContractCover._Control_LastUpdateTimestamp, coreContractCoverBean.getLastUpdateTimestamp());
		coreContractCoverMap.put(ContractPaths._ContractCover._Control_LastSyncAction, coreContractCoverBean.getLastSyncAction());
		coreContractCoverMap.put(ContractPaths._ContractCover._Control_LastSyncTimestamp, coreContractCoverBean.getLastSyncTimestamp());
		
		return coreContractCoverMap;
	}
	
	/**
	 * Return dataContext variables from workflow
	 * 
	 * @param dataContext
	 * @return
	 */
	public static HashMap<String, String> getContextVariables(com.orchestranetworks.workflow.DataContextReadOnly dataContext) {

        HashMap<String, String> hashMap = new HashMap<String, String>();
        Iterator<String> variableNames = dataContext.getVariableNames();
        while (variableNames.hasNext()) {
            String variableName = variableNames.next();
            String variableValue = dataContext.getVariableString(variableName);
            hashMap.put(variableName, variableValue);
        }
        return hashMap;
    }
}