package com.elipslife.ebx.data.access;

import com.onwbp.adaptation.AdaptationTable;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;

public abstract class BaseBankAccountWriter<T> extends AbstractTableWriter<T>  {

	private BaseBankAccountReader<T> baseBankAccountReader;
	
	public BaseBankAccountWriter(BaseBankAccountReader<T> baseBankAccountReader, AdaptationTable table) {
		super(table);
		
		this.baseBankAccountReader = baseBankAccountReader;
	}

	
	@Override
	public AbstractTableReader<T> getTableReader() {

		return this.baseBankAccountReader;
	}
}