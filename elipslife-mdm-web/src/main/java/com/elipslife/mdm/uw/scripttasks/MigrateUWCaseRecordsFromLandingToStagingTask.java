package com.elipslife.mdm.uw.scripttasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.elipslife.mdm.uw.landing.LandingUWApplicationBean;
import com.elipslife.mdm.uw.landing.LandingUWApplicationReader;
import com.elipslife.mdm.uw.landing.LandingUWApplicationWriter;
import com.elipslife.mdm.uw.landing.LandingUWApplicationWriterSession;
import com.elipslife.mdm.uw.landing.LandingUWCaseBean;
import com.elipslife.mdm.uw.landing.LandingUWCaseReader;
import com.elipslife.mdm.uw.landing.LandingUWCaseWriter;
import com.elipslife.mdm.uw.landing.LandingUWCaseWriterSession;
import com.elipslife.mdm.uw.staging.StagingUWCaseBean;
import com.elipslife.mdm.uw.staging.StagingUWCaseWriter;
import com.elipslife.mdm.uw.staging.StagingUWCaseWriterSession;
import com.onwbp.boot.VM;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class MigrateUWCaseRecordsFromLandingToStagingTask extends ScriptTaskBean {

    public static final int BATCH_SIZE = 1000;

    @Override
    public void executeScript(ScriptTaskBeanContext aContext) throws OperationException {

        // migrate UW Case records
        migrateUWCaseFromLandingToStaging(aContext);
    }

    /**
     * 
     * Migrate UWCase records from Landing to Staging
     * 
     * @param aContext
     */
    private void migrateUWCaseFromLandingToStaging(ScriptTaskBeanContext aContext) {

        LandingUWCaseWriter landingUWCaseWriter = new LandingUWCaseWriterSession(aContext.getSession());
        LandingUWCaseReader landingUWCaseReader = (LandingUWCaseReader) landingUWCaseWriter.getTableReader();
        List<LandingUWCaseBean> landingUWCaseBeans = landingUWCaseReader.readAllBeans();

        StagingUWCaseWriter stagingUWCaseWriter = new StagingUWCaseWriterSession(aContext.getSession());

        migrateUWCasesInBatchProcessing(landingUWCaseBeans, landingUWCaseWriter, stagingUWCaseWriter, landingUWCaseReader, aContext.getSession());
        // Clear the Landing UWCase beans
        landingUWCaseBeans.clear();

    }

    /**
     * 
     * Migrate UW Cases records in Batch processing
     * 
     * @param landingUWCaseBeans
     * @param landingUWCaseWriter
     * @param stagingUWCaseWriter
     */
    private void migrateUWCasesInBatchProcessing(List<LandingUWCaseBean> landingUWCaseBeans, LandingUWCaseWriter landingUWCaseWriter,
        StagingUWCaseWriter stagingUWCaseWriter, LandingUWCaseReader uwCaseReader, Session session) {

        List<LandingUWCaseBean> landingUWCaseBatch = new ArrayList<LandingUWCaseBean>();
        for (LandingUWCaseBean luwcb : landingUWCaseBeans) {
            // If landing record has any validation errors , it should stay in Landing
            boolean hasUWCaseValidationErrors = uwCaseReader.hasValidationErrors(new Object[] {luwcb.getUWCaseId()});
            if (hasUWCaseValidationErrors) {
                VM.log.kernelWarn("Migration UW Case error with UW Case id " + luwcb.getUWCaseId());
                continue;
            }
            
            // If Parent UWApplication has any validation errors, it should not be processed
            boolean hasUWApplicationExists = findLandingUWApplicationByUWApplicationId(session, luwcb);
            if (hasUWApplicationExists) {
                VM.log.kernelWarn("Migration UW Case error with UW Case id " + luwcb.getUWCaseId());
                continue;
            }

            if (landingUWCaseBatch.size() == BATCH_SIZE) {
                for (LandingUWCaseBean luwc : landingUWCaseBatch) {
                    try {
                        StagingUWCaseBean stagingUWCaseBean = createStagingUWCaseBeanFromLandingUWCaseBean(luwc);
                        stagingUWCaseWriter.createBeanOrThrow(stagingUWCaseBean);

                        // Delete Landing UW Application record
                        landingUWCaseWriter.delete(luwc);
                    } catch (Exception ex) {
                        VM.log.kernelInfo(String.format("Migration error for landing UW Case with ID %s:\r\n%s",
                            luwc.getUWCaseId(), ex.toString()));
                    }
                }
                landingUWCaseBatch.clear();
            }
            landingUWCaseBatch.add(luwcb);
        }

        if (landingUWCaseBatch.size() != 0) {

            for (LandingUWCaseBean luwc : landingUWCaseBatch) {
                try {
                    StagingUWCaseBean stagingUWCaseBean = createStagingUWCaseBeanFromLandingUWCaseBean(luwc);
                    stagingUWCaseWriter.createBeanOrThrow(stagingUWCaseBean);

                    // Delete Landing UW Application record
                    landingUWCaseWriter.delete(luwc);
                } catch (Exception ex) {
                    VM.log.kernelInfo(String.format("Migration error for landing UW Case with ID %s:\r\n%s",
                        luwc.getUWCaseId(), ex.toString()));
                }
            }
            landingUWCaseBatch.clear();
        }
    }

    private static StagingUWCaseBean createStagingUWCaseBeanFromLandingUWCaseBean(LandingUWCaseBean landingUWCaseBean) {

        StagingUWCaseBean stagingUWCaseBean = new StagingUWCaseBean();
        fillStagingUWCaseWithLandingUWCase(stagingUWCaseBean, landingUWCaseBean);
        return stagingUWCaseBean;
    }

    public static void fillStagingUWCaseWithLandingUWCase(
        StagingUWCaseBean stagingUWCaseBean, LandingUWCaseBean landingUWCaseBean) {

        stagingUWCaseBean.setUWCaseBusId(landingUWCaseBean.getUWCaseBusId());
        stagingUWCaseBean.setSourceSystem(landingUWCaseBean.getSourceSystem());
        if (landingUWCaseBean.getDataOwner() == null || landingUWCaseBean.getDataOwner() == "") {
            stagingUWCaseBean.setDataOwner("0");
        } else {
            stagingUWCaseBean.setDataOwner(landingUWCaseBean.getDataOwner());
        }

        stagingUWCaseBean.setUWApplicationBusId(landingUWCaseBean.getUWApplicationBusId());
        stagingUWCaseBean.setContractNumber(landingUWCaseBean.getContractNumber());
        stagingUWCaseBean.setApplicantBusId(landingUWCaseBean.getApplicantBusId());
        stagingUWCaseBean.setApplicantRelationship(landingUWCaseBean.getApplicantRelationship());
        stagingUWCaseBean.setApplicantFirstName(landingUWCaseBean.getApplicantFirstName());
        stagingUWCaseBean.setApplicantLastName(landingUWCaseBean.getApplicantLastName());
        stagingUWCaseBean.setApplicantDateOfBirth(landingUWCaseBean.getApplicantDateOfBirth());
        stagingUWCaseBean.setApplicantSSN(landingUWCaseBean.getApplicantSSN());
        stagingUWCaseBean.setApplicantTown(landingUWCaseBean.getApplicantTown());
        stagingUWCaseBean.setApplicantPostCode(landingUWCaseBean.getApplicantPostCode());
        stagingUWCaseBean.setApplicantCountry(landingUWCaseBean.getApplicantCountry());
        stagingUWCaseBean.setApplicantClass(landingUWCaseBean.getApplicantClass());
        stagingUWCaseBean.setProductType(landingUWCaseBean.getProductType());
        stagingUWCaseBean.setEligiblityDate(landingUWCaseBean.getEligiblityDate());
        stagingUWCaseBean.setApplicationReason(landingUWCaseBean.getApplicationReason());
        stagingUWCaseBean.setApplicationReasonText(landingUWCaseBean.getApplicationReasonText());
        if (landingUWCaseBean.getIsActive() == null) {
            stagingUWCaseBean.setIsActive(true);
        } else {
            stagingUWCaseBean.setIsActive(landingUWCaseBean.getIsActive());
        }
        stagingUWCaseBean.setCaseState(landingUWCaseBean.getCaseState());
        stagingUWCaseBean.setCreatedBy(landingUWCaseBean.getCreatedBy());
        stagingUWCaseBean.setCreationTimeStamp(landingUWCaseBean.getCreationTimeStamp());
        stagingUWCaseBean.setLastUpdatedBy(landingUWCaseBean.getLastUpdatedBy());
        stagingUWCaseBean.setLastUpdateTimeStamp(landingUWCaseBean.getLastUpdateTimeStamp());
        stagingUWCaseBean.setDeletionTimeStamp(landingUWCaseBean.getDeletionTimeStamp());
        stagingUWCaseBean.setLastSyncTimeStamp(new Date());
        stagingUWCaseBean.setAction(landingUWCaseBean.getAction());
    }

    /**
     * Check If Parent Landing UWApplication available or not
     * 
     * @param session
     * @param landingUWCaseBean
     * @return
     */
    private static boolean findLandingUWApplicationByUWApplicationId(Session session, LandingUWCaseBean landingUWCaseBean) {

        LandingUWApplicationWriter landingUWApplicationWriter = new LandingUWApplicationWriterSession(session);
        LandingUWApplicationReader landingUWApplicationReader = (LandingUWApplicationReader) landingUWApplicationWriter.getTableReader();
        List<LandingUWApplicationBean> landingUWApplicationBeans = landingUWApplicationReader.findBeanByUWApplicationId(landingUWCaseBean.getUWApplicationId());
        if (!landingUWApplicationBeans.isEmpty()) {
            return true;
        }

        return false;
    }
}
