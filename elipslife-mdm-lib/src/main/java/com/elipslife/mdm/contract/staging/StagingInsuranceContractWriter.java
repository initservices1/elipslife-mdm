package com.elipslife.mdm.contract.staging;

import java.util.Date;
import java.util.HashMap;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class StagingInsuranceContractWriter extends AbstractTableWriter<StagingInsuranceContractBean> {

	private StagingInsuranceContractReader stagingInsuranceContractReader;
	private boolean useDefaultValues;
	
	public StagingInsuranceContractWriter(boolean useDefaultValues) {
		this(ContractConstants.DataSpace.CONTRACT_REF, useDefaultValues);
	}
	
	public StagingInsuranceContractWriter(String dataSpace, boolean useDefaultValues) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._STG_InsuranceContract.getPathInSchema()),
				useDefaultValues);
	}
	
	public StagingInsuranceContractWriter(AdaptationTable table, boolean useDefaultValues) {
		super(table);
		
		this.stagingInsuranceContractReader = new StagingInsuranceContractReader(table);
		this.useDefaultValues = useDefaultValues;
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, StagingInsuranceContractBean bean) {
		
		values.put(ContractPaths._STG_InsuranceContract._ContractIds_InsuranceContractId, bean.getId());
		
		if(!useDefaultValues || (bean.getDataOwner() != null && !bean.getDataOwner().isEmpty())) {
			values.put(ContractPaths._STG_InsuranceContract._ContractIds_DataOwner, bean.getDataOwner());
		}
		
		values.put(ContractPaths._STG_InsuranceContract._ContractIds_ContractStageBusId, bean.getContractStageBusId());
		values.put(ContractPaths._STG_InsuranceContract._ContractIds_ContractNumber, bean.getContractNumber());
		values.put(ContractPaths._STG_InsuranceContract._ContractIds_OfferNumber, bean.getOfferNumber());
		values.put(ContractPaths._STG_InsuranceContract._ContractIds_ApplicationNumber, bean.getApplicationNumber());
		values.put(ContractPaths._STG_InsuranceContract._ContractIds_ContractBusId2, bean.getContractBusId2());
		values.put(ContractPaths._STG_InsuranceContract._ContractIds_CustomerRefNumber, bean.getCustomerRefNumber());
		values.put(ContractPaths._STG_InsuranceContract._ContractIds_InsuranceContractMaster, bean.getInsuranceContractMaster());
		
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractStage, bean.getContractStage());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractState, bean.getContractState());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_OfferState, bean.getOfferState());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_ApplicationState, bean.getApplicationState());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractClass, bean.getContractClass());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_BusinessContractBeginDate, bean.getBusinessContractBeginDate());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractExpiryDate, bean.getContractExpiryDate());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractBeginDate, bean.getContractBeginDate());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractEndDate, bean.getContractEndDate());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_BusinessScope, bean.getBusinessScope());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_ContractName, bean.getContractName());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_AlternativeContractName, bean.getAlternativeContractName());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_ClientSegment, bean.getClientSegment());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_DistributionAgreementBusId, bean.getDistributionAgreementBusId());
		
		if(!useDefaultValues || bean.getIsActive() != null) {
			values.put(ContractPaths._STG_InsuranceContract._ContractCore_IsActive, bean.getIsActive());
		}
		
		if(!useDefaultValues || bean.getIsProfitParticipation() != null) {
			values.put(ContractPaths._STG_InsuranceContract._ContractCore_IsProfitParticipation, bean.getIsProfitParticipation());
		}
		
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_LineOfBusiness, bean.getLineOfBusiness());
		values.put(ContractPaths._STG_InsuranceContract._ContractCore_OperatingCountry, bean.getOperatingCountry());
		
		values.put(ContractPaths._STG_InsuranceContract._Parties_Policyholder_SourcePolicyholderBusId, bean.getSourcePolicyholderBusId());
		values.put(ContractPaths._STG_InsuranceContract._Parties_Policyholder_PolicyholderBusId, bean.getPolicyholderBusId());
		
		values.put(ContractPaths._STG_InsuranceContract._Parties_Broker_SourceBrokerBusId, bean.getSourceBrokerBusId());
		values.put(ContractPaths._STG_InsuranceContract._Parties_Broker_BrokerBusId, bean.getBrokerBusId());
		
		values.put(ContractPaths._STG_InsuranceContract._Parties_AttachedPartner_SourceAttachedPartnerBusId, bean.getSourceAttachedPartnerBusId());
		values.put(ContractPaths._STG_InsuranceContract._Parties_AttachedPartner_AttachedPartnerBusId, bean.getAttachedPartnerBusId());
		
		values.put(ContractPaths._STG_InsuranceContract._Parties_AdminResponsible_SourceAdminResponsibleBusId, bean.getSourceAdminResponsibleBusId());
		values.put(ContractPaths._STG_InsuranceContract._Parties_AdminResponsible_AdminResponsibleUserName, bean.getAdminResponsibleUserName());
		
		values.put(ContractPaths._STG_InsuranceContract._Parties_ClaimResponsible_SourceClaimResponsibleBusId, bean.getSourceClaimResponsibleBusId());
		values.put(ContractPaths._STG_InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleUserName, bean.getClaimResponsibleUserName());
		
		values.put(ContractPaths._STG_InsuranceContract._Parties_CaseMgmtResponsible_SourceCaseMgmtResponsibleBusId, bean.getSourceCaseMgmtResponsibleBusId());
		values.put(ContractPaths._STG_InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleUserName, bean.getCaseMgmtResponsibleUserName());
		
		values.put(ContractPaths._STG_InsuranceContract._Parties_SalesResponsible_SourceSalesResponsibleBusId, bean.getSourceSalesResponsibleBusId());
		values.put(ContractPaths._STG_InsuranceContract._Parties_SalesResponsible_SalesResponsibleUserName, bean.getSalesResponsibleUserName());
		
		values.put(ContractPaths._STG_InsuranceContract._Control_SourceSystem, bean.getSourceSystem());
		values.put(ContractPaths._STG_InsuranceContract._Control_SourceContractBusId, bean.getSourceContractBusId());
		values.put(ContractPaths._STG_InsuranceContract._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(ContractPaths._STG_InsuranceContract._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(ContractPaths._STG_InsuranceContract._Control_LastSyncTimestamp, new Date());
		values.put(ContractPaths._STG_InsuranceContract._Control_Action, bean.getAction());
		values.put(ContractPaths._STG_InsuranceContract._Control_ActionBy, bean.getActionBy());
	}

	@Override
	public AbstractTableReader<StagingInsuranceContractBean> getTableReader() {
		return this.stagingInsuranceContractReader;
	}
}