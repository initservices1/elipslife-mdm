package com.elipslife.ebx.data.access;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.StagingBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class StagingBankAccountWriter extends BaseBankAccountWriter<StagingBankAccountBean> {

	private boolean useDefaultValues;
	
	public StagingBankAccountWriter(boolean useDefaultValues) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
				PartyPaths._STG_BankAccount.getPathInSchema()),
			useDefaultValues);
	}
	
	public StagingBankAccountWriter(boolean useDefaultValues, String dataspace) {
        this(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataspace, PartyConstants.DataSet.PARTY_MASTER),
                PartyPaths._STG_BankAccount.getPathInSchema()),
            useDefaultValues);
    }
	
	
	public StagingBankAccountWriter(AdaptationTable adaptationTable, boolean useDefaultValues) {
		super(new StagingBankAccountReader(adaptationTable), adaptationTable);
		
		this.useDefaultValues = useDefaultValues;
	}

	
	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, StagingBankAccountBean bean) {

		values.put(PartyPaths._STG_BankAccount._Account_BankAccountId, bean.getId());
		
		values.put(PartyPaths._STG_BankAccount._Account_AccountType, bean.getAccountType());
		values.put(PartyPaths._STG_BankAccount._Account_AccountNumber, bean.getAccountNumber());
		values.put(PartyPaths._STG_BankAccount._Account_IBAN, bean.getIban());
		values.put(PartyPaths._STG_BankAccount._Account_BIC, bean.getBic());
		values.put(PartyPaths._STG_BankAccount._Account_ClearingNumber, bean.getClearingNumber());
		values.put(PartyPaths._STG_BankAccount._Account_BankName, bean.getBankName());
		values.put(PartyPaths._STG_BankAccount._Account_Country, bean.getCountry());
		values.put(PartyPaths._STG_BankAccount._Account_Currency, bean.getCurrency());
		
		if(!useDefaultValues || bean.getIsMainAccount() != null) {
			values.put(PartyPaths._STG_BankAccount._Account_IsMainAccount, bean.getIsMainAccount());
		}
		values.put(PartyPaths._STG_BankAccount._Account_CorrelationId, bean.getCorrelationId());
		values.put(PartyPaths._STG_BankAccount._Account_SourceBankAccountBusId, bean.getSourceBankAccountBusId());
		values.put(PartyPaths._STG_BankAccount._Account_CoreBankAccountId, bean.getCoreBankAccountId());
		values.put(PartyPaths._STG_BankAccount._Control_SourceSystem, bean.getSourceSystem());
		values.put(PartyPaths._STG_BankAccount._Control_SourcePartyBusId, bean.getSourcePartyBusId());
		values.put(PartyPaths._STG_BankAccount._Control_ValidFrom, bean.getValidFrom());
		values.put(PartyPaths._STG_BankAccount._Control_ValidTo, bean.getValidTo());
		values.put(PartyPaths._STG_BankAccount._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(PartyPaths._STG_BankAccount._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(PartyPaths._STG_BankAccount._Control_Action, bean.getAction());
		values.put(PartyPaths._STG_BankAccount._Control_ActionBy, bean.getActionBy());
	}
}