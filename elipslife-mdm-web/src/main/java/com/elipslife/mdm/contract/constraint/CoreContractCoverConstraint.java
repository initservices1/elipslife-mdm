package com.elipslife.mdm.contract.constraint;

import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import com.butos.ebx.smart.validation.SmartRecordAndTableLevelValidationCheck;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class CoreContractCoverConstraint extends SmartRecordAndTableLevelValidationCheck{

	

	@Override
	protected void executeBusinessValidations(ValueContext recordContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {
		
		UserMessage userMsg = null;
		String exposureId = (String) recordContext.getValue(ContractPaths._ContractCover._Exposure_ExposureId);
		if(exposureId == null || exposureId == "") {
			String exposureBusId = (String) recordContext.getValue(ContractPaths._ContractCover._Exposure_ExposureBusId);
			if(exposureBusId != null && !exposureBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Exposure Id not found by Exposure Bus Id");
				this.insertUserMsg(
					validationInfoMap,
					recordContext.getNode(ContractPaths._ContractCover._Exposure_ExposureId),
					userMsg);
			}
		}
	}
	
	@Override
	public void setup(ConstraintContextOnTable aContext) {
		
	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		return null;
	}

}
