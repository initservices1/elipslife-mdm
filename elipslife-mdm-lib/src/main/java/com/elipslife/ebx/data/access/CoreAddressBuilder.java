package com.elipslife.ebx.data.access;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.elipslife.ebx.domain.bean.CoreAddressBean;
import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.ebx.domain.bean.convert.AddressConverter;
import com.elipslife.ebx.domain.bean.convert.CoreAddressConverter;
import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.boot.VM;
import com.orchestranetworks.presales.toolbox.procedure.RecordValuesBean;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class CoreAddressBuilder {

	public static List<CoreAddressBean> coreAddressBeansToCreate = new ArrayList<CoreAddressBean>();
	public static List<StagingAddressBean> stagingAddressChunks = new ArrayList<StagingAddressBean>();
	public static List<StagingAddressBean> stageAddressesUpdateList = new ArrayList<StagingAddressBean>();
	public static int recordCount = 0;
	public static Integer commitSize = 0;

	public static AdaptationHome stagingDataSpaceHome;
	public static Adaptation stagingDataSet;

	public static Integer getCommitSize() {
		return commitSize;
	}

	public static void setCommitSize(Integer commitSize) {
		CoreAddressBuilder.commitSize = commitSize;
	}

	/**
	 * Links a staging address to a core address
	 * 
	 * @param stagingAddressBean
	 * @return
	 */
	public static CoreAddressBean linkStagingAddressToCoreAddress(StagingAddressBean stagingAddressBean, String dataSpace, CorePartyBean corePartyBean) {

		// Get CoreAddresses by Party
		CoreAddressReader coreAddressReader = new CoreAddressReader(dataSpace);
		List<CoreAddressBean> coreAddressBeans = coreAddressReader.findBeansByPartyId(corePartyBean.getId());

		// Check by SourceAddressBusId
		List<CoreAddressBean> coreAddressBeansBySourceAddressBusId = coreAddressBeans.stream()
				.filter(cab -> stagingAddressBean.getSourceAddressBusId() != null
						&& Objects.equals(stagingAddressBean.getSourceSystem(), cab.getSourceSystem())
						&& Objects.equals(stagingAddressBean.getSourceAddressBusId(), cab.getSourceAddressBusId())
						&& Objects.equals(stagingAddressBean.getSourcePartyBusId(), cab.getSourcePartyBusId()))
				.collect(Collectors.toList());

		if (!coreAddressBeansBySourceAddressBusId.isEmpty()) {
			return coreAddressBeansBySourceAddressBusId.get(0);
		}

		// Check rest by SourceSystem, SourcePartyBusId, AddressType, AddressUsage, ElectronicAddressType, ValidFrom, ValidTo
		Stream<CoreAddressBean> coreAddressBeansByAddressType = coreAddressBeans.stream()
				.filter(cab -> (stagingAddressBean.getSourceAddressBusId() == null
						|| stagingAddressBean.getSourceAddressBusId().isEmpty())
						&& Objects.equals(stagingAddressBean.getSourceSystem(), cab.getSourceSystem())
						&& Objects.equals(stagingAddressBean.getSourcePartyBusId(), cab.getSourcePartyBusId())
						&& Objects.equals(stagingAddressBean.getAddressType(), cab.getAddressType())
						&& Objects.equals(stagingAddressBean.getAddressUsage(), cab.getAddressUsage())
						&& Objects.equals(stagingAddressBean.getElectronicAddressType(), cab.getElectronicAddressType())
						&& Objects.equals(stagingAddressBean.getValidFrom(), cab.getValidFrom())
						&& Objects.equals(stagingAddressBean.getValidTo(), cab.getValidTo()));

		// Sort CoreAddresses by LastSyncTimestamp
		List<CoreAddressBean> coreAddressBeansSorted = coreAddressBeansByAddressType
				.sorted((cab1, cab2) -> cab1.getLastSyncTimestamp().compareTo(cab2.getLastSyncTimestamp()))
				.collect(Collectors.toList());

		// Get CoreAddress with oldest LastSyncTimestamp
		return coreAddressBeansSorted.isEmpty() ? null : coreAddressBeansSorted.get(0);
	}

	/**
	 * 
	 * @param procedureContext
	 * @param stagingAddressBean
	 */
	public static void createOrUpdateCoreAddressByStagingAddress(ProcedureContext procedureContext,
			StagingAddressBean stagingAddressBean) {

		StagingAddressWriter stagingAddressWriter = new StagingAddressWriterProcedureContext(procedureContext);

		CoreAddressWriter coreAddressWriter = new CoreAddressWriterProcedureContext(procedureContext, true);

		createOrUpdateCoreAddressByStagingAddress(stagingAddressWriter, coreAddressWriter, stagingAddressBean,
				PartyConstants.DataSpace.PARTY_REFERENCE);
	}

	public static void createOrUpdateCoreAddressByStagingAddressInBulkProcess(Session session, String dataspace,
			AdaptationHome stagingDataSpace, Adaptation stagingDataset, String commitSize) {

		stagingDataSpaceHome = stagingDataSpace;
		stagingDataSet = stagingDataset;

		Date dataspaceCreationTimestamp = stagingDataSpaceHome.getCreationDate();
		setCommitSize(Integer.parseInt(commitSize));

		StagingAddressWriter stagingAddressWriter = new StagingAddressWriterSession(session, dataspace);
		StagingAddressReader stagingAddressReader = new StagingAddressReader(dataspace);
		List<StagingAddressBean> stagingAddressBeans = stagingAddressReader.getStagingAddressesBeansByLastSyncTimeStamp(dataspaceCreationTimestamp);

		CoreAddressWriter coreAddressWriter = new CoreAddressWriterSession(session, true, dataspace);
		try {
			for (StagingAddressBean stagingAddressBean : stagingAddressBeans) {
				boolean isWithinRange = dateWithinRange(stagingAddressBean.getValidFrom(), stagingAddressBean.getValidTo());
				if (!isWithinRange) {
					continue;
				}

				boolean hasValidationErrors = hasValidationErrors(stagingAddressBean, dataspace);
				if (hasValidationErrors) {
					continue;
				}
				recordCount++;
				createOrUpdateCoreAddressByStagingAddress(stagingAddressWriter, coreAddressWriter, stagingAddressBean, dataspace);
				if (recordCount > getCommitSize()) {
					createCoreAddresses(coreAddressBeansToCreate, session, dataspace);
					coreAddressBeansToCreate.clear();
					recordCount = 0;
				}
				
			}
			if (!coreAddressBeansToCreate.isEmpty() || !stageAddressesUpdateList.isEmpty()) {

				createCoreAddresses(coreAddressBeansToCreate, session, dataspace);
				updateStagingAddress(stageAddressesUpdateList, session, dataspace);
				coreAddressBeansToCreate.clear();
				stageAddressesUpdateList.clear();
				stagingAddressBeans.clear();
				recordCount = 0;
			}

		} catch (Exception e) {

		}

	}

	/**
	 * 
	 * @param session
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @param validFrom
	 * @param validTo
	 */
	public static void createOrUpdateCoreAddressByStagingAddress(Session session, StagingAddressBean stagingAddressBean) {

		StagingAddressWriter stagingAddressWriter = new StagingAddressWriterSession(session);
		CoreAddressWriter coreAddressWriter = new CoreAddressWriterSession(session, true);
		createOrUpdateCoreAddressByStagingAddress(stagingAddressWriter, coreAddressWriter, stagingAddressBean, PartyConstants.DataSpace.PARTY_REFERENCE);
	}

	/**
	 * Create or Update core addresses by staging addresses
	 * 
	 * @param stagingAddressWriter
	 * @param coreAddressWriter
	 * @param stagingAddressBean
	 */
	public static void createOrUpdateCoreAddressByStagingAddress(StagingAddressWriter stagingAddressWriter,
			CoreAddressWriter coreAddressWriter, StagingAddressBean stagingAddressBean, String dataSpace) {

		Date dateMinimum = new Date(Long.MIN_VALUE);
		boolean isInBulkProcess = CorePartyBuilder.isBulkProcess();
		// CorePartyId must be set from staging address SourceSystem, SourcePartyBusId
		CorePartyBean corePartyBean = CorePartyBuilder.getPartyBySourceSystem(stagingAddressBean.getSourceSystem(), stagingAddressBean.getSourcePartyBusId(), dataSpace);
		CoreAddressBean coreAddressBean = null;
		if (corePartyBean == null) {
			VM.log.kernelInfo(String.format("Could not create or update core address for staging address %s, because no core party identifier or party found with SourceSystem %s and SourcePartyBusId %s",
					stagingAddressBean.getId(), stagingAddressBean.getSourceSystem(),stagingAddressBean.getSourcePartyBusId()));
		} else {
			if(isInBulkProcess) {
				stagingAddressChunks.add(stagingAddressBean);
				// Create a core address
				coreAddressBean = AddressConverter.createCoreAddressBeanFromStagingAddressBean(stagingAddressBean);
				coreAddressBean.setPartyId(String.valueOf(corePartyBean.getId()));
				coreAddressBean.setLastSyncAction(Constants.RecordOperations.CREATE);
				coreAddressBeansToCreate.add(coreAddressBean);
				
			} else {
				coreAddressBean = CoreAddressBuilder.linkStagingAddressToCoreAddress(stagingAddressBean, dataSpace, corePartyBean);
				if (coreAddressBean == null) {
					// Create a core address
					coreAddressBean = AddressConverter.createCoreAddressBeanFromStagingAddressBean(stagingAddressBean);
					coreAddressBean.setPartyId(String.valueOf(corePartyBean.getId()));
					coreAddressBean.setLastSyncAction(Constants.RecordOperations.CREATE);
					Optional<CoreAddressBean> coreAddressBeanOptional = coreAddressWriter.createBeanOrThrow(coreAddressBean);
					coreAddressBean = coreAddressBeanOptional.get();
					stagingAddressBean.setCoreAddressId(String.valueOf(coreAddressBean.getId())); // Update staging address with core address ID
				} else {
					Date coreLastUpdatedTimeStamp = coreAddressBean.getLastUpdateTimestamp() == null ? dateMinimum : coreAddressBean.getLastUpdateTimestamp();
					Date stagingLastUpdatedTimestamp = stagingAddressBean.getLastUpdateTimestamp() == null ? dateMinimum : stagingAddressBean.getLastUpdateTimestamp();
					if (stagingLastUpdatedTimestamp.compareTo(coreLastUpdatedTimeStamp) >= 0) {
						AddressConverter.fillCoreAddressBeanWithStagingAddressBean(stagingAddressBean, coreAddressBean);
						if (!(stagingAddressBean.getAction() != null && stagingAddressBean.getAction().equals(Constants.RecordOperations.DELETE))) {
							coreAddressBean.setLastSyncAction(Constants.RecordOperations.UPDATE);
						}
						coreAddressWriter.updateBean(coreAddressBean);
					}
					stagingAddressBean.setCoreAddressId(String.valueOf(coreAddressBean.getId())); // Update staging address with core address ID
				}
			}
		}
		if (!isInBulkProcess) {
			stagingAddressWriter.updateBean(stagingAddressBean);
		} else {
			stageAddressesUpdateList.add(stagingAddressBean);
		}

	}

	/**
	 * 
	 * @param procedureContext
	 * @param stagingAddressBean
	 */
	public static void deleteCoreAddressByStagingAddress(ProcedureContext procedureContext,
			StagingAddressBean stagingAddressBean) {

		StagingAddressWriter stagingAddressWriter = new StagingAddressWriterProcedureContext(procedureContext);

		CoreAddressWriter coreAddressWriter = new CoreAddressWriterProcedureContext(procedureContext, false);

		deleteCoreAddressByStagingAddress(stagingAddressWriter, coreAddressWriter, stagingAddressBean);
	}

	/**
	 * 
	 * @param session
	 * @param stagingAddressBean
	 */
	public static void deleteCoreAddressByStagingAddress(Session session, StagingAddressBean stagingAddressBean) {

		StagingAddressWriter stagingAddressWriter = new StagingAddressWriterSession(session);

		CoreAddressWriter coreAddressWriter = new CoreAddressWriterSession(session, false);

		deleteCoreAddressByStagingAddress(stagingAddressWriter, coreAddressWriter, stagingAddressBean);
	}

	/**
	 * 
	 * @param stagingAddressWriter
	 * @param coreAddressWriter
	 * @param stagingAddressBean
	 */
	public static void deleteCoreAddressByStagingAddress(StagingAddressWriter stagingAddressWriter,
			CoreAddressWriter coreAddressWriter, StagingAddressBean stagingAddressBean) {

		StagingAddressReader stagingAddressReader = (StagingAddressReader) stagingAddressWriter.getTableReader();
		CoreAddressReader coreAddressReader = (CoreAddressReader) coreAddressWriter.getTableReader();

		// Get core address bean referenced by staging address bean
		if (stagingAddressBean.getCoreAddressId() == null || stagingAddressBean.getCoreAddressId().isEmpty()) {
			return;
		}

		CoreAddressBean coreAddressBean = coreAddressReader
				.findBeanByAddressId(Integer.valueOf(stagingAddressBean.getCoreAddressId()));

		if (coreAddressBean == null) {
			return;
		}

		// Get staging address beans referenced to core address bean
		List<StagingAddressBean> stagingAddressBeans = stagingAddressReader
				.findBeansByCoreAddressId(String.valueOf(coreAddressBean.getId()));

		// Unlink all staging address beans referenced to core address bean
		stagingAddressBeans.forEach(sab -> {
			sab.setCoreAddressId(null);
			stagingAddressWriter.updateBean(sab);
		});

		// Delete core address bean referenced by staging address bean
		coreAddressWriter.delete(coreAddressBean);
	}

	/**
	 * 
	 * @param coreAddressWriter
	 */
	public static void markExpiredAddresses(CoreAddressWriter coreAddressWriter) {

		CoreAddressReader coreAddressReader = (CoreAddressReader) coreAddressWriter.getTableReader();
		List<CoreAddressBean> coreAddressBeans = coreAddressReader.findBeansExpired();
		coreAddressBeans.forEach(cab -> {
			cab.setIsExpired(true);
			coreAddressWriter.updateBean(cab);
		});
	}

	/**
	 * 
	 * @param coreAddressWriter
	 * @param partyId
	 */
	public static void markExpiredAddressesByParty(CoreAddressWriter coreAddressWriter, int partyId) {

		CoreAddressReader coreAddressReader = (CoreAddressReader) coreAddressWriter.getTableReader();
		List<CoreAddressBean> coreAddressBeans = coreAddressReader.findBeansByPartyId(partyId);
		coreAddressBeans.forEach(cab -> {
			cab.setIsExpired(true);
			coreAddressWriter.updateBean(cab);
		});
	}

	public static void createCoreAddresses(List<CoreAddressBean> addressBean, Session session, String dataSpace) throws OperationException {

		AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._Address.getPathInSchema());
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		for (CoreAddressBean coreAddressBean : addressBean) {
			HashMap<Path, Object> values = CoreAddressConverter.fillCoreAddressValuesFromStagingAddressBean(coreAddressBean);
			RecordValuesBean rvb = new RecordValuesBean(aTable, values);
			recordValuesBeanList.add(rvb);
		}

		final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);
		CreateRecords createRecordsProcedure = new CreateRecords(aTable, recordValuesBeanList);
		ProcedureResult result = service.execute(createRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}

		List<Adaptation> createdAddressRecords = createRecordsProcedure.getCreateRecordList();
		updateStagingAddressRecords(createdAddressRecords, session, dataSpace);

	}

	public static void updateStagingAddressRecords(List<Adaptation> createdAddressRecords, Session session, String dataSpace) {

		if(!createdAddressRecords.isEmpty()) {
			Map<String, List<Adaptation>> corePartyBeansGroupedBySourceSystem = createdAddressRecords.stream()
					.collect(Collectors.groupingBy(cpb -> cpb.getString(PartyPaths._Address._Control_SourceSystem) + cpb.getString(PartyPaths._Address._Control_SourcePartyBusId)));
			List<StagingAddressBean> stagingAddressList = new ArrayList<StagingAddressBean>();
			//CoreAddressReader coreAddressReader = new CoreAddressReader(dataSpace);
			for (StagingAddressBean stagingAddressBean : stagingAddressChunks) {
				//List<CoreAddressBean> coreAddressList = coreAddressReader.findBeansBySourceSystemAndPartyBusId(stagingAddressBean.getSourceSystem(), stagingAddressBean.getSourcePartyBusId());
				List<Adaptation> coreAddressList = corePartyBeansGroupedBySourceSystem.get(stagingAddressBean.getSourceSystem()+stagingAddressBean.getSourcePartyBusId());
				for(Adaptation coreAddressBean : coreAddressList) {
					boolean isTrue = Objects.equals(stagingAddressBean.getAddressType(), coreAddressBean.getString(PartyPaths._Address._AddressInfo_AddressType))
							&& Objects.equals(stagingAddressBean.getAddressUsage(), coreAddressBean.getString(PartyPaths._Address._AddressInfo_AddressUse))
							&& Objects.equals(stagingAddressBean.getElectronicAddressType(), coreAddressBean.getString(PartyPaths._Address._ElectronicAddress_ElectronicAddressType))
							&& Objects.equals(stagingAddressBean.getValidFrom(), coreAddressBean.getDate(PartyPaths._Address._Control_ValidFrom))
							&& Objects.equals(stagingAddressBean.getValidTo(), coreAddressBean.getDate(PartyPaths._Address._Control_ValidTo));
					if (isTrue) {
						stagingAddressBean.setCoreAddressId(String.valueOf(coreAddressBean.get(PartyPaths._Address._AddressInfo_AddressId)));
						stagingAddressList.add(stagingAddressBean);
					}
				}
			}
			stagingAddressChunks.clear();
			updateStagingAddress(stagingAddressList, session, dataSpace);
		}
	}

	public static void updateStagingAddress(List<StagingAddressBean> addressBeanList, Session session, String dataSpace) {

		if(!addressBeanList.isEmpty()) {
			AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._STG_Address.getPathInSchema());
			List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
			HashMap<Path, Object> values = null;
			RecordValuesBean rvb = null;
			for (StagingAddressBean stagingAddressBean : addressBeanList) {
				values = new HashMap<>();
				values.put(PartyPaths._STG_Address._Address_CoreAddressId, stagingAddressBean.getCoreAddressId());
				rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(stagingAddressBean.getId().toString())), values);
				recordValuesBeanList.add(rvb);
			}
			
			final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);
			UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
			ProcedureResult result = service.execute(updateRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
			}
			addressBeanList.clear();
		}
	}

	public static void updateAddressList(List<CoreAddressBean> addressBeans, Session session, String dataSpace) {

		AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._Address.getPathInSchema());
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();

		final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);

		HashMap<Path, Object> values = null;
		RecordValuesBean rvb = null;
		for (CoreAddressBean coreAddressBean : addressBeans) {
			values = CoreAddressConverter.fillCoreAddressValuesFromStagingAddressBean(coreAddressBean);
			rvb = new RecordValuesBean( aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(coreAddressBean.getId().toString())), values);
			recordValuesBeanList.add(rvb);
		}

		UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
		}
		
	}

	private static boolean dateWithinRange(Date dateFrom, Date dateTo) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		Date dateNow = calendar.getTime();

		Date dateMinimum = new Date(Long.MIN_VALUE);
		Date dateMaximum = new Date(Long.MAX_VALUE);

		return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0
				&& dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
	}

	private static boolean hasValidationErrors(StagingAddressBean stagingAddressBean, String dataSpace) {

		StagingAddressReader stagingAddressReader = new StagingAddressReader(dataSpace);
		boolean hasValidationErrors = stagingAddressReader.hasValidationErrors(new Object[] { stagingAddressBean.getId() });
		return hasValidationErrors;
	}

	public AdaptationHome getStagingDataSpaceHome() {
		return stagingDataSpaceHome;
	}

	public void setStagingDataSpaceHome(AdaptationHome stagingDataSpaceHome) {
		CoreAddressBuilder.stagingDataSpaceHome = stagingDataSpaceHome;
	}

	public Adaptation getStagingDataSet() {
		return stagingDataSet;
	}

	public void setStagingDataSet(Adaptation stagingDataset) {
		CoreAddressBuilder.stagingDataSet = stagingDataset;
	}
}
