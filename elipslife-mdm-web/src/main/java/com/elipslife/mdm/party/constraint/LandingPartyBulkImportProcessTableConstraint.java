package com.elipslife.mdm.party.constraint;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;

import ch.butos.ebx.lib.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.xpath.filter.XPathPredicateGroup;

public class LandingPartyBulkImportProcessTableConstraint implements ConstraintOnTableWithRecordLevelCheck {

    @Override
    public void checkRecord(ValueContextForValidationOnRecord aContext) {

    	// Check the combination of SourceSystem, Source Party Busid, Valid From and Valid To 
        ValueContext valueContext = aContext.getRecord();
        Integer partyId = (Integer) valueContext.getValue(LandingPaths._LDG_Party_BulkProcess._Party_PartyId);
        if (partyId != null) {
            RequestResult requestResult = findDuplicateRecords(valueContext);
            for(Adaptation partyRecord ; (partyRecord = requestResult.nextAdaptation()) != null;) {
            	Integer corePartyRecordId = (Integer) partyRecord.get_int(LandingPaths._LDG_Party_BulkProcess._Party_PartyId);
            	if(!Objects.equals(partyId, corePartyRecordId) ) {
            		UserMessage userMsg = UserMessage.createError("Combination of SourceSystem, Source PartyId, Valid From and Valid To already exists.");
                    aContext.addMessage(valueContext.getNode(LandingPaths._LDG_Party_BulkProcess._Party), userMsg);
            	}
            }
            requestResult.close();
        }
    }

    private boolean isDuplicate(List<Adaptation> adaptations, Adaptation adaptation) {
        return adaptations.stream().filter(record -> checkDuplicates(record, adaptation)).count() > 1;
    }

    private boolean checkDuplicates(Adaptation record, Adaptation adaptation) {

        return record.get(LandingPaths._LDG_Party_BulkProcess._Control_SourceSystem)
            .equals(adaptation.get(LandingPaths._LDG_Party_BulkProcess._Control_SourceSystem)) &&
            record.get(LandingPaths._LDG_Party_BulkProcess._Control_SourcePartyBusId)
                .equals(adaptation.get(LandingPaths._LDG_Party_BulkProcess._Control_SourcePartyBusId))
            &&
            record.get(LandingPaths._LDG_Party_BulkProcess._Control_ValidFrom)
                .equals(adaptation.get(LandingPaths._LDG_Party_BulkProcess._Control_ValidFrom))
            &&
            record.get(LandingPaths._LDG_Party_BulkProcess._Control_ValidTo)
                .equals(adaptation.get(LandingPaths._LDG_Party_BulkProcess._Control_ValidTo));
    }

    @Override
    public void setup(ConstraintContextOnTable aContext) {}

    @Override
    public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
        return null;
    }

    public RequestResult findDuplicateRecords(ValueContext aRecord) {

        AdaptationTable aTable = aRecord.getAdaptationTable();
        String sourceSystem = (String) aRecord.getValue(LandingPaths._LDG_Party_BulkProcess._Control_SourceSystem);
        String sourcePartyBusId = (String) aRecord.getValue(LandingPaths._LDG_Party_BulkProcess._Control_SourcePartyBusId);
        Date validFrom = (Date) aRecord.getValue(LandingPaths._LDG_Party_BulkProcess._Control_ValidFrom);
        Date validTo = (Date) aRecord.getValue(LandingPaths._LDG_Party_BulkProcess._Control_ValidTo);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);

        XPathPredicate predicateSourceSystem = new XPathPredicate(sourceSystem != null ? Operation.Equals : Operation.IsNull,
            LandingPaths._LDG_Party_BulkProcess._Control_SourceSystem, sourceSystem);
        XPathPredicate predicateSourcePartyId = new XPathPredicate(sourcePartyBusId != null ? Operation.Equals : Operation.IsNull,
            LandingPaths._LDG_Party_BulkProcess._Control_SourcePartyBusId, sourcePartyBusId);
        String validFromString = validFrom != null ? simpleDateFormat.format(validFrom) : null;
        XPathPredicate predicateValidFrom = new XPathPredicate(validFromString != null ? Operation.DateEqual : Operation.IsNull,
            LandingPaths._LDG_Party_BulkProcess._Control_ValidFrom, validFromString);
        String validToString = validTo != null ? simpleDateFormat.format(validTo) : null;
        XPathPredicate predicateValidTo = new XPathPredicate(validToString != null ? Operation.DateEqual : Operation.IsNull,
            LandingPaths._LDG_Party_BulkProcess._Control_ValidTo, validToString);

        group.addPredicate(predicateSourceSystem);
        group.addPredicate(predicateSourcePartyId);
        group.addPredicate(predicateValidFrom);
        group.addPredicate(predicateValidTo);

        RequestResult requestResult = aTable.createRequestResult(group.toString());
        return requestResult;
    }

	@Override
	public void checkTable(ValueContextForValidationOnTable aContext) {
		
	}

}
