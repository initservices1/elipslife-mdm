package com.elipslife.mdm.claimcase.staging;

import java.util.HashMap;

import com.elipslife.mdm.claimcase.constants.ClaimCaseConstants;
import com.elipslife.mdm.claimcase.constants.ClaimCasePaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class StagingClaimCaseWriter extends AbstractTableWriter<StagingClaimCaseBean> {
	private StagingClaimCaseReader stagingClaimCaseReader;

	public StagingClaimCaseWriter() {
		this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(ClaimCaseConstants.DataSpace.CLAIMCASE_REF, ClaimCaseConstants.DataSet.CLAIMCASE_MASTER),
				ClaimCasePaths._STG_ClaimCase.getPathInSchema()));
	}

	public StagingClaimCaseWriter(AdaptationTable table) {
		super(table);

		this.stagingClaimCaseReader = new StagingClaimCaseReader(super.table);
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, StagingClaimCaseBean bean) {

		// ClaimCase_General
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ClaimCaseId, bean.getId());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_DataOwner, bean.getDataOwner());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ClaimCaseBusId, bean.getClaimCaseBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ClaimCaseExtRef, bean.getClaimCaseExtRef());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ContractNumber, bean.getContractNumber());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_SourceContractBusId, bean.getSourceContractBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_OperatingCountry, bean.getOperatingCountry());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_BusinessScope, bean.getBusinessScope());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ClaimType, bean.getClaimType());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ClaimState, bean.getClaimState());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ClaimStateDetail, bean.getClaimStateDetail());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ClaimDate, bean.getClaimDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_StartDate, bean.getStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_EndDate, bean.getEndDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ReportingDate, bean.getReportingDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_IsActive, bean.getIsActive());
		
		// Party
		values.put(ClaimCasePaths._STG_ClaimCase._Party_ClientSegment, bean.getClientSegment());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_PolicyholderBusId, bean.getPolicyHolderBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_SourcePolicyholderBusId, bean.getSourcePolicyHolderBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_MainAffiliateBusId, bean.getMainAffiliateBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_SourceMainAffiliateBusId, bean.getSourceMainAffiliateBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_InsuredPersonBusId, bean.getInsuredPersonBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_InsuredPersonType, bean.getInsuredPersonType());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_SourceInsuredPersonBusId, bean.getSourceInsuredPersonBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_ClaimManagerUserName, bean.getClaimManagerUserName());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_SourceClaimManagerBusId, bean.getSourceClaimManagerBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_ClaimMgtmTeamBusId, bean.getClaimMgtmTeamBusId());
		values.put(ClaimCasePaths._STG_ClaimCase._Party_SourceClaimMgmtTeamBusId, bean.getSourceClaimMgmtTeamBusId());

		// ClaimTypeDisability
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_AssessmentState, bean.getAssessmentState());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_BenefitStartDate, bean.getBenefitStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_BenefitEndDate, bean.getBenefitEndDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_CancellationDate, bean.getCancellationDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_CertifiedEndDate, bean.getCertifiedEndDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_ClaimCause, bean.getClaimCause());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_DiagnosisDate, bean.getDiagnosisDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_DisabilityDate, bean.getDisabilityDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_HospitalStartDate, bean.getHospitalStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_LastDayWorked, bean.getLastDayWorked());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_ReactivationDate, bean.getReactivationDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_ReturnToWorkDate, bean.getReturnToWorkDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_SymptomsStartDate, bean.getSymptomsStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_TerminationDate, bean.getTerminationdate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeDisability_TreatmentStartDate, bean.getTreatmentStartDate());

		// ClaimtypeAbsence_US_AbsenceGeneral
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceGeneral_AbsenceReason, bean.getAbsenceReason());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceGeneral_AbsenceDurationType, bean.getAbsenceDurationType());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceGeneral_AbsenceClaimState, bean.getAbsenceClaimState());

		// ClaimtypeAbsence_US_AbsenceAdoption
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceAdoption_AdoptionType, bean.getAdoptionType());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceAdoption_AdoptionDate, bean.getAdoptionDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceAdoption_IsAdoptionCourtCase, bean.getIsAdoptionCourtCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceAdoption_AdoptionCourtDate, bean.getAdoptionCourtDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceAdoption_AdoptionCourtHours, bean.getAdoptionCourtHours());

		// ClaimtypeAbsence_US_AbsenceBereavement
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBereavement_BereaverRelationship, bean.getBereaverRelationship());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBereavement_BereaverRelationshipNotes, bean.getBereaverRelationshipNotes());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBereavement_BereavementDeathDate, bean.getBereavementDeathDate());

		// ClaimtypeAbsence_US_AbsenceBonding
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBonding_BondingParentType, bean.getBondingParentType());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBonding_BondingEstimatedDeliveryDate, bean.getBondingEstimatedDeliveryDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBonding_BondingNumDelivered, bean.getBondingNumDelivered());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBonding_IsBondingPartnerEmployerSame, bean.getIsBondingPartnerEmployerSame());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBonding_IsPartnerBonding, bean.getIsPartnerBonding());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBonding_BondingPartnerName, bean.getBondingPartnerName());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBonding_BondingPartnerStartDate, bean.getBondingPartnerStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBonding_BondingPartnerEndDate, bean.getBondingPartnerEndDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBonding_BondingLastDisabilityDate, bean.getBondingLastDisabilityDate());

		// ClaimtypeAbsence_US_AbsenceBoneMarrow
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBoneMarrow_IsBoneMarrowMedicalApptCase, bean.getIsBoneMarrowMedicalApptCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBoneMarrow_BoneMarrowMedicalApptDate, bean.getBoneMarrowMedicalApptDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceBoneMarrow_BoneMarrowMedicalApptHours, bean.getBoneMarrowMedicalApptHours());

		// ClaimtypeAbsence_US_AbsenceChildBirth
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_ChildbirthDate, bean.getChildbirthDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_ChildbirthEstimatedDate, bean.getChildbirthEstimatedDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_ChildbirthNumDelivered, bean.getChildbirthNumDelivered());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_ChildbirthDeliveryType, bean.getChildbirthDeliveryType());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_IsChildbirthBondingTimeCase, bean.getIsChildbirthBondingTimeCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_IsChildbirthComplicationCase, bean.getIsChildbirthComplicationCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_IsChildbirthPartnerBonding, bean.getIsChildbirthPartnerBonding());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_IsChildbirthPartnerEmployerSame, bean.getIsChildbirthPartnerEmployerSame());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_ChildbirthPartnerName, bean.getChildbirthPartnerName());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_ChildbirthPartnerBondingStartDate, bean.getChildbirthPartnerBondingStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_ChildbirthPartnerBondingEndDate, bean.getChildbirthPartnerBondingEndDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_ChildbirthLastDisabilityDate, bean.getChildbirthLastDisabilityDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceChildbirth_ChildbirthTreatingDoctorName, bean.getChildbirthTreatingDoctorName());

		// ClaimtypeAbsence_US_AbsenceFamilyMember
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyMemberName, bean.getFamilyMemberName());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyMemberRelationship, bean.getFamilyMemberRelationship());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyMemberRelationshipNotes, bean.getFamilyMemberRelationshipNotes());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_IsFamilyFollowUpApptCase, bean.getIsFamilyFollowUpApptCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_IsFamilyHospitalCase, bean.getIsFamilyHospitalCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_IsFamilyMemberADAQualified, bean.getIsFamilyMemberADAQualified());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_IsFamilyMemberOver18, bean.getIsFamilyMemberOver18());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_IsFamilyMemberConditionNonSerious, bean.getIsfamilyMemberConditionNonSerious());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyChildBirthDate, bean.getFamilyChildBirthDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyMemberHealthConditionNotes, bean.getFamilyMemberHealthConditionNotes());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyIllnessStartDate, bean.getFamilyIllnessStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyHospitalStartDate, bean.getFamilyHospitalStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyHospitalEndDate, bean.getFamilyHospitalEndDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyTreatmentStartDate, bean.getFamilyTreatmentStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyFollowUpApptDate, bean.getFamilyFollowUpApptDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceFamilyMember_FamilyTreatingDoctorName, bean.getFamilyTreatingDoctorName());

		// ClaimtypeAbsence_US_AbsenceMilitaryLeave
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceMilitaryLeave_MilitaryLeaveType, bean.getMilitaryLeaveType());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceMilitaryLeave_MilitaryMemberType, bean.getMilitaryMemberType());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceMilitaryLeave_MilitaryMemberRelationship, bean.getMilitaryMemberRelationship());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceMilitaryLeave_MilitaryMemberRelationshipNotes, bean.getMilitaryMemberRelationshipNotes());

		// ClaimtypeAbsence_US_AbsenceOrganDonation
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOrganDonation_OrganDonating, bean.getOrganDonating());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOrganDonation_IsOrganDonorMedicalApptCase, bean.getIsOrganDonorMedicalApptCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOrganDonation_OrganDonorMedicalApptDate, bean.getOrganDonorMedicalApptDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOrganDonation_OrganDonorMedicalApptHours, bean.getOrganDonorMedicalApptHours());

		// ClaimtypeAbsence_US_AbsenceOwnIllness
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_OwnIllnessInjuryType, bean.getOwnIllnessInjuryType());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_IsOwnIllnessHospitalCase, bean.isOwnIllnessHospitalCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_IsOwnIllnessFollowUpApptCase, bean.isOwnIllnessFollowUpApptCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_IsOwnIllnessWorkRelated, bean.isOwnIllnessWorkRelated());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_IsOwnMentalIllnessCase, bean.isOwnMentalIllnesCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_OwnIllnessStartDate, bean.getOwnIllnessStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_OwnIllnessHospitalStartDate, bean.getOwnIllnessHospitalStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_OwnIllnessHospitalEndDate, bean.getOwnIllnessHospitalEndDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_OwnIllnessFollowUpApptDate, bean.getOwnIllnessFollowUpApptDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_OwnIllnessTreatmentStartDate, bean.getOwnIllnessTreatmentStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceOwnIllness_OwnIllnessTreatingDoctorName, bean.getOwnIllnessTreatingDoctorName());

		// ClaimtypeAbsence_US_AbsencePersonalCrime
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_PersonalCrimeType, bean.getPersonalCrimeType());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_PersonalCrimeTypeNotes, bean.getPersonalCrimeTypeNotes());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_PersonalCrimeRelationship, bean.getPersonalCrimeRelationship());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_PersonalCrimeRelationshipNotes, bean.getPersonalCrimeRelationshipNotes());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_IsPersonalCrimeCounsellingCase, bean.getIsPersonalCrimeCounsellingCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_IsPersonalCrimeCourtCase, bean.getIsPersonalCrimeCourtCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_IsPersonalCrimeDoctorCase, bean.getIsPersonalCrimeDoctorCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_IsPersonalCrimeHospitalCase, bean.getIsPersonalCrimeHospitalCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_IsPersonalCrimeLegalAssistCase, bean.getIsPersonalCrimeLegalAssistCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_IsPersonalCrimeRelocationCase, bean.getIsPersonalCrimeRelocationCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_IsPersonalCrimeRestrainingOrderCase, bean.getIsPersonalCrimeRestrainingOrderCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_IsPersonalCrimeVictimServicesCase, bean.getIsPersonalCrimeVictimServicesCase());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_PersonalCrimeHospitalStartDate, bean.getPersonalCrimeHospitalStartDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_PersonalCrimeHospitalEndDate, bean.getPersonalCrimeHospitalEndDate());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalCrime_PersonalCrimeTreatingDoctorName, bean.getPersonalCrimeTreatingDoctorName());

		// ClaimtypeAbsence_US_AbsencePersonalLeave
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsencePersonalLeave_IsFMLAQualifyingEvent, bean.getIsFMLAQualifyingEvent());

		// ClaimtypeAbsence_US_AbsenceSmallnecessities
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimTypeAbsence_US_AbsenceSmallNecessities_SmallNecessitiesType, bean.getSmallNecessitiesType());

		// Control
		values.put(ClaimCasePaths._STG_ClaimCase._Control_SourceSystem, bean.getSourceSystem());
		values.put(ClaimCasePaths._STG_ClaimCase._Control_CreationTimestamp, bean.getCreationTimeStamp());
		values.put(ClaimCasePaths._STG_ClaimCase._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
		values.put(ClaimCasePaths._STG_ClaimCase._Control_LastSyncTimestamp, bean.getLastSyncTimestamp());
		values.put(ClaimCasePaths._STG_ClaimCase._Control_Action, bean.getAction());
		values.put(ClaimCasePaths._STG_ClaimCase._Control_ActionBy, bean.getActionBy());
		values.put(ClaimCasePaths._STG_ClaimCase._ClaimCase_General_ClaimsMaster, bean.getClaimsMaster());
	}

	@Override
	protected AbstractTableReader<StagingClaimCaseBean> getTableReader() {
		return this.stagingClaimCaseReader;
	}
}