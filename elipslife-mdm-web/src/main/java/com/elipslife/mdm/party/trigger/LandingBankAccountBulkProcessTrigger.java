package com.elipslife.mdm.party.trigger;

import java.util.Date;

import com.elipslife.mdm.path.LandingPaths;
import com.elipslife.mdm.utilities.TriggerUtilities;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class LandingBankAccountBulkProcessTrigger extends LandingBulkProcessTriggerBase {

    @Override
    public void setup(TriggerSetupContext context) {

    }

    @Override
    public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException {
        super.handleBeforeCreate(context);

        ValueContextForUpdate valueContextForUpdate = context.getOccurrenceContextForUpdate();
        valueContextForUpdate.setValue(new Date(), LandingPaths._LDG_BankAccount_BulkProcess._Control_LastSyncTimestamp);
        TriggerUtilities.setDefaultValueIfNull(valueContextForUpdate, LandingPaths._LDG_BankAccount_BulkProcess._Account_IsMainAccount);

        setLogicalDefaultValues(valueContextForUpdate);
    }

    @Override
    protected Path getPathControlValidFrom() {
        return LandingPaths._LDG_BankAccount_BulkProcess._Control_ValidFrom;
    }

    @Override
    protected Path getPathControlValidTo() {
        return LandingPaths._LDG_BankAccount_BulkProcess._Control_ValidTo;
    }

    @Override
    protected Path getPathCreationTimestamp() {
        return LandingPaths._LDG_BankAccount_BulkProcess._Control_CreationTimestamp;
    }

    @Override
    protected Path getPathLastUpdateTimestamp() {
        return LandingPaths._LDG_BankAccount_BulkProcess._Control_LastUpdateTimestamp;
    }

    @Override
    protected Path getPathAction() {
        return LandingPaths._LDG_BankAccount_BulkProcess._Control_Action;
    }

	@Override
	protected Path getPathLandingPartyId() {
		return LandingPaths._LDG_BankAccount_BulkProcess._Account_LandingPartyId;
	}

	@Override
	protected Path getPathMdmStagingProcessResponse() {
		return LandingPaths._LDG_BankAccount_BulkProcess._Account_MdmStagingProcessResponse;
	}

}
