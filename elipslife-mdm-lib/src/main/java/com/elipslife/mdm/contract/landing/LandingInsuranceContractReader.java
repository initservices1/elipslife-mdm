package com.elipslife.mdm.contract.landing;

import java.util.List;

import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;

public class LandingInsuranceContractReader extends AbstractTableReader<LandingInsuranceContractBean> {

	public LandingInsuranceContractReader() {
		this(PartyConstants.DataSpace.LANDING_REFERENCE);
	}
	
	public LandingInsuranceContractReader(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_InsuranceContract.getPathInSchema()));
	}
	
	public LandingInsuranceContractReader(AdaptationTable table) {
		super(table);
	}

	@Override
	protected LandingInsuranceContractBean createBeanFromRecord(Adaptation record) {
		LandingInsuranceContractBean bean = new LandingInsuranceContractBean();
		
		bean.setId(record.get_int(LandingPaths._LDG_InsuranceContract._ContractIds_InsuranceContractId));
		bean.setDataOwner(record.getString(LandingPaths._LDG_InsuranceContract._ContractIds_DataOwner));
		bean.setContractStageBusId(record.getString(LandingPaths._LDG_InsuranceContract._ContractIds_ContractStageBusId));
		bean.setContractNumber(record.getString(LandingPaths._LDG_InsuranceContract._ContractIds_ContractNumber));
		bean.setOfferNumber(record.getString(LandingPaths._LDG_InsuranceContract._ContractIds_OfferNumber));
		bean.setApplicationNumber(record.getString(LandingPaths._LDG_InsuranceContract._ContractIds_ApplicationNumber));
		bean.setContractBusId2(record.getString(LandingPaths._LDG_InsuranceContract._ContractIds_ContractBusId2));
		bean.setCustomerRefNumber(record.getString(LandingPaths._LDG_InsuranceContract._ContractIds_CustomerRefNumber));
		
		bean.setContractStage(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_ContractStage));
		bean.setContractState(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_ContractState));
		bean.setOfferState(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_OfferState));
		bean.setApplicationState(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_ApplicationState));
		bean.setContractClass(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_ContractClass));
		bean.setBusinessContractBeginDate(record.getDate(LandingPaths._LDG_InsuranceContract._ContractCore_BusinessContractBeginDate));
		bean.setContractExpiryDate(record.getDate(LandingPaths._LDG_InsuranceContract._ContractCore_ContractExpiryDate));
		bean.setContractBeginDate(record.getDate(LandingPaths._LDG_InsuranceContract._ContractCore_ContractBeginDate));
		bean.setContractEndDate(record.getDate(LandingPaths._LDG_InsuranceContract._ContractCore_ContractEndDate));
		bean.setBusinessScope(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_BusinessScope));
		bean.setContractName(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_ContractName));
		bean.setAlternativeContractName(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_AlternativeContractName));
		bean.setClientSegment(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_ClientSegment));
		bean.setDistributionAgreementBusId(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_DistributionAgreementBusId));
		bean.setIsActive((Boolean)record.get(LandingPaths._LDG_InsuranceContract._ContractCore_IsActive));
		bean.setIsProfitParticipation((Boolean)record.get(LandingPaths._LDG_InsuranceContract._ContractCore_IsProfitParticipation));
		bean.setLineOfBusiness(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_LineOfBusiness));
		bean.setOperatingCountry(record.getString(LandingPaths._LDG_InsuranceContract._ContractCore_OperatingCountry));
		
		bean.setSourcePolicyholderBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_Policyholder_SourcePolicyholderBusId));
		bean.setPolicyholderBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_Policyholder_PolicyholderBusId));
		
		bean.setSourceBrokerBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_Broker_SourceBrokerBusId));
		bean.setBrokerBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_Broker_BrokerBusId));
		
		bean.setSourceAttachedPartnerBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_AttachedPartner_SourceAttachedPartnerBusId));
		bean.setAttachedPartnerBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_AttachedPartner_AttachedPartnerBusId));
		
		bean.setSourceAdminResponsibleBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_AdminResponsible_SourceAdminResponsibleBusId));
		bean.setAdminResponsibleUserName(record.getString(LandingPaths._LDG_InsuranceContract._Parties_AdminResponsible_AdminResponsibleUserName));
		
		bean.setSourceClaimResponsibleBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_ClaimResponsible_SourceClaimResponsibleBusId));
		bean.setClaimResponsibleUserName(record.getString(LandingPaths._LDG_InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleUserName));
		
		bean.setSourceCaseMgmtResponsibleBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_CaseMgmtResponsible_SourceCaseMgmtResponsibleBusId));
		bean.setCaseMgmtResponsibleUserName(record.getString(LandingPaths._LDG_InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleUserName));
		
		bean.setSourceSalesResponsibleBusId(record.getString(LandingPaths._LDG_InsuranceContract._Parties_SalesResponsible_SourceSalesResponsibleBusId));
		bean.setSalesResponsibleUserName(record.getString(LandingPaths._LDG_InsuranceContract._Parties_SalesResponsible_SalesResponsibleUserName));
		
		bean.setSourceSystem(record.getString(LandingPaths._LDG_InsuranceContract._Control_SourceSystem));
		bean.setSourceContractBusId(record.getString(LandingPaths._LDG_InsuranceContract._Control_SourceContractBusId));
		bean.setCreationTimestamp(record.getDate(LandingPaths._LDG_InsuranceContract._Control_CreationTimestamp));
		bean.setLastUpdateTimestamp(record.getDate(LandingPaths._LDG_InsuranceContract._Control_LastUpdateTimestamp));
		bean.setLastSyncTimeStamp(record.getDate(LandingPaths._LDG_InsuranceContract._Control_LastSyncTimestamp));
		bean.setAction(record.getString(LandingPaths._LDG_InsuranceContract._Control_Action));
		bean.setActionBy(record.getString(LandingPaths._LDG_InsuranceContract._Control_ActionBy));
		
		return bean;
	}

	@Override
	protected Path[] getPkPaths() {
		return new Path[] { LandingPaths._LDG_InsuranceContract._ContractIds_InsuranceContractId };
	}

	@Override
	protected Object[] retrievePks(LandingInsuranceContractBean bean) {
		return new Object[] { bean.getId() };
	}
	
	/**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}
	
	/**
	 * 
	 * @param sourceSystem
	 * @param exposureBusId
	 * @return
	 */
	public List<LandingInsuranceContractBean> findBeansByInsuranceContractId(int insuranceContractId) {
		
		XPathPredicate predicate1 = new XPathPredicate(Operation.Equals, LandingPaths._LDG_InsuranceContract._ContractIds_InsuranceContractId, String.valueOf(insuranceContractId));
		
		return super.readBeansFor(predicate1.toString());
	}
}