package com.elipslife.ebx.data.access;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.CoreBankAccountBean;
import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.ebx.domain.bean.StagingBankAccountBean;
import com.elipslife.ebx.domain.bean.convert.BankAccountConverter;
import com.elipslife.ebx.domain.bean.convert.CoreBankAccountConverter;
import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.boot.VM;
import com.orchestranetworks.presales.toolbox.procedure.RecordValuesBean;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class CoreBankAccountBuilder {

	public static List<CoreBankAccountBean> coreBankAccountBeansToCreate = new ArrayList<CoreBankAccountBean>();
	public static List<StagingBankAccountBean> stagingBankAccountChunks = new ArrayList<StagingBankAccountBean>();
	public static List<StagingBankAccountBean> stageBankAccountsUpdate = new ArrayList<StagingBankAccountBean>();
	public static Map<String, List<StagingBankAccountBean>> stagingBankAccountsGroupedBySourceSystem = null;
	public static int recordCount = 0;
	public static Integer commitSize = 0;

	public static Integer getCommitSize() {
		return commitSize;
	}

	public static AdaptationHome stagingDataSpaceHome;
	public static Adaptation stagingDataSet;

	public static void setCommitSize(Integer commitSize) {
		CoreBankAccountBuilder.commitSize = commitSize;
	}

	/**
	 * Links a staging bankAccount to a core bankAccount
	 * 
	 * @param stagingBankAccountBean
	 * @return
	 */
	public static CoreBankAccountBean linkStagingBankAccountToCoreBankAccount(
			StagingBankAccountBean stagingBankAccountBean, CorePartyBean corePartyBean, String dataSpace) {
		
		// Get CoreBankAccountes by Party
		CoreBankAccountReader coreBankAccountReader = new CoreBankAccountReader(dataSpace);
		List<CoreBankAccountBean> coreBankAccountBeans = coreBankAccountReader
				.findBeansByPartyId(corePartyBean.getId());

		// Check by SourceAddressBusId
		List<CoreBankAccountBean> coreBankAccountsBySourceAccountBusId = coreBankAccountBeans.stream()
				.filter(cab -> stagingBankAccountBean.getSourceBankAccountBusId() != null
						&& Objects.equals(stagingBankAccountBean.getSourceSystem(), cab.getSourceSystem())
						&& Objects.equals(stagingBankAccountBean.getSourceBankAccountBusId(),
								cab.getSourceBankAccountBusId())
						&& Objects.equals(stagingBankAccountBean.getSourcePartyBusId(), cab.getSourcePartyBusId()))
				.collect(Collectors.toList());

		if (!coreBankAccountsBySourceAccountBusId.isEmpty()) {
			return coreBankAccountsBySourceAccountBusId.get(0);
		}

		// Get CoreBankAccount from CoreBankAccountes by valid dates
		List<CoreBankAccountBean> coreBankAccountBeansByValidDate = coreBankAccountBeans.stream()
				.filter(cab -> checkIbanOrAccountNumberIfSourceBankAccountIsNull(stagingBankAccountBean, cab.getSourceBankAccountBusId(), cab.getAccountNumber(), cab.getIban())
						&& Objects.equals(stagingBankAccountBean.getSourceSystem(), cab.getSourceSystem())
						&& Objects.equals(stagingBankAccountBean.getSourcePartyBusId(), cab.getSourcePartyBusId()))
				.collect(Collectors.toList());

		return coreBankAccountBeansByValidDate.isEmpty() ? null : coreBankAccountBeansByValidDate.get(0);
	}

	/**
	 * 
	 * @param procedureContext
	 * @param stagingBankAccountBean
	 */
	public static void createOrUpdateCoreBankAccountByStagingBankAccount(ProcedureContext procedureContext, StagingBankAccountBean stagingBankAccountBean) {

		StagingBankAccountWriter stagingBankAccountWriter = new StagingBankAccountWriterProcedureContext(procedureContext, true);
		CoreBankAccountWriter coreBankAccountWriter = new CoreBankAccountWriterProcedureContext(procedureContext, true);
		createOrUpdateCoreBankAccountByStagingBankAccount(stagingBankAccountWriter, coreBankAccountWriter, stagingBankAccountBean, PartyConstants.DataSpace.PARTY_REFERENCE);
	}

	/**
	 * 
	 * @param session
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @param validFrom
	 * @param validTo
	 */
	public static void createOrUpdateCoreBankAccountByStagingBankAccount(Session session, StagingBankAccountBean stagingBankAccountBean, String dataSpace) {

		StagingBankAccountWriter stagingBankAccountWriter = new StagingBankAccountWriterSession(session, true, dataSpace);
		CoreBankAccountWriter coreBankAccountWriter = new CoreBankAccountWriterSession(session, true, dataSpace);
		createOrUpdateCoreBankAccountByStagingBankAccount(stagingBankAccountWriter, coreBankAccountWriter, stagingBankAccountBean, dataSpace);
	}

	public static void createOrUpdateCoreBankAccountByStagingBankAccountInBulkProcessing(Session session, String dataSpace, AdaptationHome stagingDataSpace, Adaptation stagingDataset, String commitSize) {

		stagingDataSpaceHome = stagingDataSpace;
		stagingDataSet = stagingDataset;
		Date dataspaceCreationTimestamp = stagingDataSpaceHome.getCreationDate();
		StagingBankAccountReader stagingBankAccountReader = new StagingBankAccountReader(dataSpace);
		List<StagingBankAccountBean> stagingBankAccountBeans = stagingBankAccountReader.getStagingBankAccountBeansByLastSyncTimeStamp(dataspaceCreationTimestamp);
		setCommitSize(Integer.parseInt(commitSize));
		StagingBankAccountWriter stagingBankAccountWriter = new StagingBankAccountWriterSession(session, true, dataSpace);
		stagingBankAccountsGroupedBySourceSystem = stagingBankAccountBeans.stream().collect(Collectors.groupingBy(cpb -> cpb.getSourceSystem() + cpb.getSourcePartyBusId()));

		CoreBankAccountWriter coreBankAccountWriter = new CoreBankAccountWriterSession(session, true, dataSpace);
		try {
			for (StagingBankAccountBean stagingBankAccountBean : stagingBankAccountBeans) {

				boolean isWithinRange = dateWithinRange(stagingBankAccountBean.getValidFrom(), stagingBankAccountBean.getValidTo());
				if (!isWithinRange) {
					continue;
				}

				boolean hasValidationErrors = hasValidationErrors(stagingBankAccountBean, dataSpace);
				if (hasValidationErrors) {
					continue;
				}

				recordCount++;
				createOrUpdateCoreBankAccountByStagingBankAccount(stagingBankAccountWriter, coreBankAccountWriter, stagingBankAccountBean, dataSpace);
				if (recordCount > getCommitSize()) {
					createCoreBankAccounts(coreBankAccountBeansToCreate, session, dataSpace);
					coreBankAccountBeansToCreate.clear();
					recordCount = 0;
				}
			}

			if (!coreBankAccountBeansToCreate.isEmpty() || !stageBankAccountsUpdate.isEmpty()) { 
				createCoreBankAccounts(coreBankAccountBeansToCreate, session, dataSpace);
				updateStagingBankAccounts(stageBankAccountsUpdate, session, dataSpace);
				coreBankAccountBeansToCreate.clear();
				recordCount = 0;
				stagingBankAccountBeans.clear();
				stagingBankAccountsGroupedBySourceSystem.clear();
			}
		} catch (Exception e) {

		}

	}

	/**
	 * Create or Update core bank accounts with Staging Bank accounts
	 * 
	 * @param stagingBankAccountWriter
	 * @param coreBankAccountWriter
	 * @param stagingBankAccountBean
	 */
	public static void createOrUpdateCoreBankAccountByStagingBankAccount(StagingBankAccountWriter stagingBankAccountWriter, CoreBankAccountWriter coreBankAccountWriter,
			StagingBankAccountBean stagingBankAccountBean, String dataSpace) {

		Date dateMinimum = new Date(Long.MIN_VALUE);
		// CorePartyId must be set from staging bankAccount SourceSystem, SourcePartyBusId
		CorePartyBean corePartyBean = CorePartyBuilder.getPartyBySourceSystem(stagingBankAccountBean.getSourceSystem(), stagingBankAccountBean.getSourcePartyBusId(), dataSpace);
		if (corePartyBean == null) {
			VM.log.kernelInfo(String.format("Could not create or update core bank account for staging bank account %s, because no core party identifier or party found with SourceSystem %s and SourcePartyBusId %s",
					stagingBankAccountBean.getId(), stagingBankAccountBean.getSourceSystem(), stagingBankAccountBean.getSourcePartyBusId()));
		} else {
			boolean isInBulkProcess = CorePartyBuilder.isBulkProcess();
			StagingBankAccountBean bankAccountBean = null;
			CoreBankAccountBean coreBankAccountBean = null;
			if(isInBulkProcess) {
				stagingBankAccountChunks.add(stagingBankAccountBean);
				List<StagingBankAccountBean> stagingBankAccountsBySourceSystem = stagingBankAccountsGroupedBySourceSystem.get(stagingBankAccountBean.getSourceSystem() + stagingBankAccountBean.getSourcePartyBusId());
				// Check by SourceBankAccountBusId
				List<StagingBankAccountBean> stagingBankAccountsBySourceAccountBusId = stagingBankAccountsBySourceSystem.stream().filter(cab -> stagingBankAccountBean.getSourceBankAccountBusId() != null
								&& Objects.equals(stagingBankAccountBean.getSourceSystem(), cab.getSourceSystem())
								&& Objects.equals(stagingBankAccountBean.getSourceBankAccountBusId(), cab.getSourceBankAccountBusId())
								&& Objects.equals(stagingBankAccountBean.getSourcePartyBusId(), cab.getSourcePartyBusId())).collect(Collectors.toList());
				
				if (!stagingBankAccountsBySourceAccountBusId.isEmpty()) {
					// Sort CoreBankAccount by LastSyncTimestamp
					bankAccountBean = !stagingBankAccountsBySourceAccountBusId.isEmpty() ? stagingBankAccountsBySourceAccountBusId.stream()
							.sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getLastUpdateTimestamp(), a.getLastUpdateTimestamp())).findFirst().get() : null;
				} else {
					// Get CoreBankAccount from CoreBankAccountes by valid dates
					List<StagingBankAccountBean> stagingBankAccountBeansByValidDate = stagingBankAccountsBySourceSystem.stream().filter(cab -> checkIbanOrAccountNumberIfSourceBankAccountIsNullforStaging(stagingBankAccountBean, cab)
									&& Objects.equals(stagingBankAccountBean.getSourceSystem(), cab.getSourceSystem())
									&& Objects.equals(stagingBankAccountBean.getSourcePartyBusId(), cab.getSourcePartyBusId())).collect(Collectors.toList());
					
					bankAccountBean = !stagingBankAccountBeansByValidDate.isEmpty() ? stagingBankAccountBeansByValidDate.stream()
									.sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getLastUpdateTimestamp(), a.getLastUpdateTimestamp())).findFirst().get() : null;
				}
				
				// Create a core bankAccount
				coreBankAccountBean = BankAccountConverter.createCoreBankAccountBeanFromStagingBankAccountBean(stagingBankAccountBean);
				coreBankAccountBean.setPartyId(String.valueOf(corePartyBean.getId()));
				coreBankAccountBean.setLastSyncAction(Constants.RecordOperations.CREATE);
				if (bankAccountBean != null && !bankAccountBean.getId().equals(stagingBankAccountBean.getId())) {
					if (stagingBankAccountBean.getLastUpdateTimestamp() != null && stagingBankAccountBean.getLastUpdateTimestamp().compareTo(bankAccountBean.getLastUpdateTimestamp()) > 0) {
						coreBankAccountBeansToCreate.add(coreBankAccountBean);
					}
				} else {
					coreBankAccountBeansToCreate.add(coreBankAccountBean);
				}
				
				stageBankAccountsUpdate.add(stagingBankAccountBean);
				
			} else {
				coreBankAccountBean = CoreBankAccountBuilder.linkStagingBankAccountToCoreBankAccount(stagingBankAccountBean, corePartyBean, dataSpace);
				if(coreBankAccountBean == null) {
					// Create a core bankAccount
					coreBankAccountBean = BankAccountConverter.createCoreBankAccountBeanFromStagingBankAccountBean(stagingBankAccountBean);
					coreBankAccountBean.setPartyId(String.valueOf(corePartyBean.getId()));
					coreBankAccountBean.setLastSyncAction(Constants.RecordOperations.CREATE);
					Optional<CoreBankAccountBean> coreBankAccountBeanOptional = coreBankAccountWriter.createBeanOrThrow(coreBankAccountBean);
					coreBankAccountBean = coreBankAccountBeanOptional.get();
					stagingBankAccountBean.setCoreBankAccountId(String.valueOf(coreBankAccountBean.getId()));
				} else {

					Date coreLastUpdatedTimeStamp = coreBankAccountBean.getLastUpdateTimestamp() == null ? dateMinimum : coreBankAccountBean.getLastUpdateTimestamp();
					Date stagingLastUpdatedTimestamp = stagingBankAccountBean.getLastUpdateTimestamp() == null ? dateMinimum : stagingBankAccountBean.getLastUpdateTimestamp();
					if (stagingLastUpdatedTimestamp.compareTo(coreLastUpdatedTimeStamp) >= 0) {

						BankAccountConverter.fillCoreBankAccountBeanWithStagingBankAccountBean(stagingBankAccountBean, coreBankAccountBean);
						if (!(stagingBankAccountBean.getAction() != null && stagingBankAccountBean.getAction().equals(Constants.RecordOperations.DELETE))) {
							coreBankAccountBean.setLastSyncAction(Constants.RecordOperations.UPDATE);
						}
						coreBankAccountWriter.updateBean(coreBankAccountBean);
					}
					stagingBankAccountBean.setCoreBankAccountId(String.valueOf(coreBankAccountBean.getId()));
				}
				stagingBankAccountWriter.updateBean(stagingBankAccountBean);
			} 
		}
	}

	/**
	 * 
	 * @param procedureContext
	 * @param stagingBankAccountBean
	 */
	public static void deleteCoreBankAccountByStagingBankAccount(ProcedureContext procedureContext, StagingBankAccountBean stagingBankAccountBean) {

		StagingBankAccountWriter stagingBankAccountWriter = new StagingBankAccountWriterProcedureContext(procedureContext, false);
		CoreBankAccountWriter coreBankAccountWriter = new CoreBankAccountWriterProcedureContext(procedureContext, false);
		deleteCoreBankAccountByStagingBankAccount(stagingBankAccountWriter, coreBankAccountWriter, stagingBankAccountBean);
	}

	/**
	 * 
	 * @param session
	 * @param stagingBankAccountBean
	 */
	public static void deleteCoreBankAccountByStagingBankAccount(Session session, StagingBankAccountBean stagingBankAccountBean) {

		StagingBankAccountWriter stagingBankAccountWriter = new StagingBankAccountWriterSession(session, false);
		CoreBankAccountWriter coreBankAccountWriter = new CoreBankAccountWriterSession(session, false);
		deleteCoreBankAccountByStagingBankAccount(stagingBankAccountWriter, coreBankAccountWriter, stagingBankAccountBean);
	}

	/**
	 * 
	 * @param stagingBankAccountWriter
	 * @param coreBankAccountWriter
	 * @param stagingBankAccountBean
	 */
	public static void deleteCoreBankAccountByStagingBankAccount(StagingBankAccountWriter stagingBankAccountWriter,
			CoreBankAccountWriter coreBankAccountWriter, StagingBankAccountBean stagingBankAccountBean) {

		StagingBankAccountReader stagingBankAccountReader = (StagingBankAccountReader) stagingBankAccountWriter.getTableReader();
		CoreBankAccountReader coreBankAccountReader = (CoreBankAccountReader) coreBankAccountWriter.getTableReader();

		// Get core bank account bean referenced by staging bank account bean
		if (stagingBankAccountBean.getCoreBankAccountId() == null || stagingBankAccountBean.getCoreBankAccountId().isEmpty()) {
			return;
		}

		CoreBankAccountBean coreBankAccountBean = coreBankAccountReader.findBeanByBankAccountId(Integer.valueOf(stagingBankAccountBean.getCoreBankAccountId()));
		if (coreBankAccountBean == null) {
			return;
		}

		// Get staging bankAccount beans referenced to core bankAccount bean
		List<StagingBankAccountBean> stagingBankAccountBeans = stagingBankAccountReader.findBeansByCoreBankAccountId(String.valueOf(coreBankAccountBean.getId()));

		// Unlink all staging bankAccount beans referenced to core bankAccount bean
		stagingBankAccountBeans.forEach(sab -> {
			sab.setCoreBankAccountId(null);
			stagingBankAccountWriter.updateBean(sab);
		});

		// Delete core bankAccount bean referenced by staging bankAccount bean
		coreBankAccountWriter.delete(coreBankAccountBean);
	}

	/**
	 * 
	 * @param coreBankAccountWriter
	 */
	public static void markExpiredBankAccounts(CoreBankAccountWriter coreBankAccountWriter) {

		CoreBankAccountReader coreBankAccountReader = (CoreBankAccountReader) coreBankAccountWriter.getTableReader();
		List<CoreBankAccountBean> coreBankAccountBeans = coreBankAccountReader.findBeansExpired();
		coreBankAccountBeans.forEach(cbab -> {
			cbab.setIsExpired(true);
			coreBankAccountWriter.updateBean(cbab);
		});
	}

	/**
	 * 
	 * @param coreBankAccountWriter
	 * @param partyId
	 */
	public static void markExpiredBankAccountsByParty(CoreBankAccountWriter coreBankAccountWriter, int partyId) {

		CoreBankAccountReader coreBankAccountReader = (CoreBankAccountReader) coreBankAccountWriter.getTableReader();
		List<CoreBankAccountBean> coreBankAccountBeans = coreBankAccountReader.findBeansByPartyId(partyId);
		coreBankAccountBeans.forEach(cbab -> {
			cbab.setIsExpired(true);
			coreBankAccountWriter.updateBean(cbab);
		});
	}

	private static Boolean checkIbanOrAccountNumberIfSourceBankAccountIsNull(
			StagingBankAccountBean stagingBankAccountBean, String coreSourceBankAccountBusId, String accountNum, String coreIban) {
		String sourceBankAccount = stagingBankAccountBean.getSourceBankAccountBusId();
		if (sourceBankAccount != null) {
			return Objects.equals(stagingBankAccountBean.getSourceBankAccountBusId(),
					coreSourceBankAccountBusId);
		}

		String accountNumber = stagingBankAccountBean.getAccountNumber();
		if (accountNumber != null) {
			return Objects.equals(stagingBankAccountBean.getAccountNumber(), accountNum);
		}

		String iban = stagingBankAccountBean.getIban();
		if (iban != null) {
			return Objects.equals(stagingBankAccountBean.getIban(), coreIban);
		}

		return false;
	}

	private static boolean checkIbanOrAccountNumberIfSourceBankAccountIsNullforStaging(
			StagingBankAccountBean bankAccountBean1, StagingBankAccountBean bankAccountBean2) {

		String sourceBankAccount = bankAccountBean1.getSourceBankAccountBusId();
		if (sourceBankAccount != null) {
			return Objects.equals(bankAccountBean1.getSourceBankAccountBusId(),
					bankAccountBean2.getSourceBankAccountBusId());
		}

		String accountNumber = bankAccountBean1.getAccountNumber();
		if (accountNumber != null) {
			return Objects.equals(bankAccountBean1.getAccountNumber(), bankAccountBean2.getAccountNumber());
		}

		String iban = bankAccountBean1.getIban();
		if (iban != null) {
			return Objects.equals(bankAccountBean1.getIban(), bankAccountBean2.getIban());
		}

		return false;
	}

	public static void createCoreBankAccounts(List<CoreBankAccountBean> bankAccountBeans, Session session, String dataSpace) throws OperationException {

		if(!bankAccountBeans.isEmpty()) {
			AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._BankAccount.getPathInSchema());
			List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
			for (CoreBankAccountBean coreBankAccount : bankAccountBeans) {
				HashMap<Path, Object> values = CoreBankAccountConverter.fillCoreBankAccountValuesFromStagingBankAccountBean(coreBankAccount, true);
				RecordValuesBean rvb = new RecordValuesBean(aTable, values);
				recordValuesBeanList.add(rvb);
			}

			final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);
			CreateRecords createRecordsProcedure = new CreateRecords(aTable, recordValuesBeanList);
			ProcedureResult result = service.execute(createRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
				throw exception;
			}
			List<Adaptation> createdBankAccounts = createRecordsProcedure.getCreateRecordList();

			updateStagingBankAccountRecords(createdBankAccounts, session, dataSpace);
		}
	}

	public static void updateStagingBankAccountRecords(List<Adaptation> createdBankAccounts, Session session, String dataspace) {

		List<StagingBankAccountBean> stagingBankAccountList = new ArrayList<StagingBankAccountBean>();
		if(!createdBankAccounts.isEmpty()) {
			Map<String, List<Adaptation>> coreBankAccountsGroupedBySourceSystem = createdBankAccounts.stream()
					.collect(Collectors.groupingBy(cpb -> cpb.getString(PartyPaths._BankAccount._Control_SourceSystem) + cpb.getString(PartyPaths._BankAccount._Control_SourcePartyBusId)));
			for (StagingBankAccountBean stagingBankAccountBean : stagingBankAccountChunks) {
				List<Adaptation> coreBankAccountsList = coreBankAccountsGroupedBySourceSystem.get(stagingBankAccountBean.getSourceSystem()+stagingBankAccountBean.getSourcePartyBusId());
				// List<CoreBankAccountBean> coreBankAccountsList = accountReader.findBeansBySourceSystemAndPartyBusId(stagingBankAccountBean.getSourceSystem(), stagingBankAccountBean.getSourceBankAccountBusId());
				for(Adaptation coreBankAccountBean : coreBankAccountsList) {
					String sourceBankAccountBusId = coreBankAccountBean.getString(PartyPaths._BankAccount._Account_SourceBankAccountBusId);
					if (sourceBankAccountBusId != null && stagingBankAccountBean.getSourceBankAccountBusId() != null) {
						if (sourceBankAccountBusId.equals(stagingBankAccountBean.getSourceBankAccountBusId())) {
							stagingBankAccountBean.setCoreBankAccountId(String.valueOf(coreBankAccountBean.get_int(PartyPaths._BankAccount._Account_BankAccountId)));
						}
					} else {
						if (checkIbanOrAccountNumberIfSourceBankAccountIsNull(stagingBankAccountBean, coreBankAccountBean.getString(PartyPaths._BankAccount._Account_SourceBankAccountBusId),
								coreBankAccountBean.getString(PartyPaths._BankAccount._Account_AccountNumber), coreBankAccountBean.getString(PartyPaths._BankAccount._Account_IBAN))) {
							stagingBankAccountBean.setCoreBankAccountId(String.valueOf(coreBankAccountBean.get_int(PartyPaths._BankAccount._Account_BankAccountId)));
						}
					}
					stagingBankAccountList.add(stagingBankAccountBean);
				}
			}
			stagingBankAccountChunks.clear();
			updateStagingBankAccounts(stagingBankAccountList, session, dataspace);
		}
	}

	public static void updateStagingBankAccounts(List<StagingBankAccountBean> bankAccountBeans, Session session, String dataSpace) {

		AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._STG_BankAccount.getPathInSchema());
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();

		final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);

		HashMap<Path, Object> values = null;
		RecordValuesBean rvb = null;
		for (StagingBankAccountBean stagingBankAccountBean : bankAccountBeans) {
			values = new HashMap<Path, Object>();
			values.put(PartyPaths._STG_BankAccount._Account_CoreBankAccountId, stagingBankAccountBean.getCoreBankAccountId());
			rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(stagingBankAccountBean.getId().toString())), values);
			recordValuesBeanList.add(rvb);
		}

		UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
		}
	}

	public static void updateBankAccountList(List<CoreBankAccountBean> bankAccountBeans, Session session,
			String dataSpace) {

		AdaptationTable aTable = stagingDataSet.getTable(PartyPaths._BankAccount.getPathInSchema());
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();

		final ProgrammaticService service = ProgrammaticService.createForSession(session, stagingDataSpaceHome);

		HashMap<Path, Object> values = null;
		RecordValuesBean rvb = null;
		for (CoreBankAccountBean coreBankAccountBean : bankAccountBeans) {
			values = CoreBankAccountConverter.fillCoreBankAccountValuesFromStagingBankAccountBean(coreBankAccountBean,
					true);
			rvb = new RecordValuesBean(
					aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(coreBankAccountBean.getId().toString())),
					values);
			recordValuesBeanList.add(rvb);
		}

		UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
		}
	}

	private static boolean dateWithinRange(Date dateFrom, Date dateTo) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		Date dateNow = calendar.getTime();

		Date dateMinimum = new Date(Long.MIN_VALUE);
		Date dateMaximum = new Date(Long.MAX_VALUE);

		return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0
				&& dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
	}

	private static boolean hasValidationErrors(StagingBankAccountBean stagingAddressBean, String dataSpace) {

		StagingBankAccountReader stagingAddressReader = new StagingBankAccountReader(dataSpace);

		boolean hasValidationErrors = stagingAddressReader
				.hasValidationErrors(new Object[] { stagingAddressBean.getId() });

		return hasValidationErrors;
	}

	public AdaptationHome getStagingDataSpaceHome() {
		return stagingDataSpaceHome;
	}

	public void setStagingDataSpaceHome(AdaptationHome stagingDataSpaceHome) {
		CoreAddressBuilder.stagingDataSpaceHome = stagingDataSpaceHome;
	}

	public Adaptation getStagingDataSet() {
		return stagingDataSet;
	}

	public void setStagingDataSet(Adaptation stagingDataset) {
		CoreAddressBuilder.stagingDataSet = stagingDataset;
	}
}
