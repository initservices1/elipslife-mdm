package com.elipslife.mdm.uw.staging;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class StagingUWCaseWriterProcedureContext extends StagingUWCaseWriter{

	private final ProcedureContext procedureContext;

	public StagingUWCaseWriterProcedureContext(ProcedureContext procedureContext) {
		super();
		this.procedureContext = procedureContext;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
