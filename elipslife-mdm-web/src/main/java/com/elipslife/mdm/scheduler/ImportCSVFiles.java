package com.elipslife.mdm.scheduler;

import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.WorkflowEngine;

public class ImportCSVFiles extends ScheduledTask {
	String inputDirectory;
	String processedDirectory;
	String errorDirectory;
	String errorLogDirectory;
	String dataSetName;
	String initialDataSpace;
	String tableName;
	String filePattern;
	String workflowToLaunch;

	@Override
	public void execute(ScheduledExecutionContext aContext) throws OperationException, ScheduledTaskInterruption {
		WorkflowEngine workflowEngine = WorkflowEngine.getFromRepository(aContext.getRepository(), aContext.getSession());
		ProcessLauncher processLauncher = workflowEngine.getProcessLauncher(PublishedProcessKey.forName("ImportCsvFiles"));
		processLauncher.setInputParameter("inputDirectory", inputDirectory);
		processLauncher.setInputParameter("processedDirectory", processedDirectory);
		processLauncher.setInputParameter("errorDirectory", errorDirectory);
		processLauncher.setInputParameter("errorLogDirectory", errorLogDirectory);
		processLauncher.setInputParameter("dataSetName", dataSetName);
		processLauncher.setInputParameter("initialDataSpace", initialDataSpace);
		processLauncher.setInputParameter("tableName", tableName);
		processLauncher.setInputParameter("filePattern", filePattern);
		processLauncher.setInputParameter("workflowToLaunch", workflowToLaunch);
		processLauncher.launchProcess();
	}

	public String getInputDirectory() {
		return inputDirectory;
	}

	public void setInputDirectory(String inputDirectory) {
		this.inputDirectory = inputDirectory;
	}

	public String getProcessedDirectory() {
		return processedDirectory;
	}

	public void setProcessedDirectory(String processedDirectory) {
		this.processedDirectory = processedDirectory;
	}

	public String getErrorDirectory() {
		return errorDirectory;
	}

	public void setErrorDirectory(String errorDirectory) {
		this.errorDirectory = errorDirectory;
	}

	public String getErrorLogDirectory() {
		return errorLogDirectory;
	}

	public void setErrorLogDirectory(String errorLogDirectory) {
		this.errorLogDirectory = errorLogDirectory;
	}

	public String getDataSetName() {
		return dataSetName;
	}

	public void setDataSetName(String dataSetName) {
		this.dataSetName = dataSetName;
	}

	public String getInitialDataSpace() {
		return initialDataSpace;
	}

	public void setInitialDataSpace(String initialDataSpace) {
		this.initialDataSpace = initialDataSpace;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public void setFilePattern(String filePattern) {
		this.filePattern = filePattern;
	}
	
	public String getFilePattern() {
		return filePattern;
	}

	public String getWorkflowToLaunch() {
		return workflowToLaunch;
	}

	public void setWorkflowToLaunch(String workflowToLaunch) {
		this.workflowToLaunch = workflowToLaunch;
	}
}