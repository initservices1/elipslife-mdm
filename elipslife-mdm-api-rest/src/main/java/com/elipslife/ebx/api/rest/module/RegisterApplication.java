package com.elipslife.ebx.api.rest.module;

import javax.servlet.annotation.WebListener;

import com.elipslife.ebx.api.rest.rest.RESTApplication;
import com.elipslife.ebx.domain.bean.convert.ReadPartyBulkProcesserProperty;
import com.orchestranetworks.module.ModuleInitializedContext;
import com.orchestranetworks.module.ModuleRegistrationListener;

@WebListener
public class RegisterApplication extends ModuleRegistrationListener {


    @Override
    public void handleContextInitialized(ModuleInitializedContext aContext) {
        aContext.registerRESTApplication(RESTApplication.class);
        ReadPartyBulkProcesserProperty bulkProcesserProperty = ReadPartyBulkProcesserProperty.getInstance();
    }
}
