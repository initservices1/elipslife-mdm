package com.elipslife.mdm.hr.core;

import java.util.List;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;


public class EmployeeReader extends AbstractTableReader<EmployeeBean> {

    public EmployeeReader() {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._Employee.getPathInSchema()));
    }
    
    public EmployeeReader(String dataspace) {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._Employee.getPathInSchema()));
    }

    public EmployeeReader(AdaptationTable table) {
    	super(table);
    }

    @Override
    public EmployeeBean createBeanFromRecord(Adaptation record) {
        EmployeeBean bean = new EmployeeBean();
        //bean.setId(record.getString(HrModelPaths._Employee._Employee_EmployeeId));
        bean.setEmployeeId(record.getString(HrModelPaths._Employee._Employee_EmployeeId));
        bean.setDataOwner(record.getString(HrModelPaths._Employee._Employee_DataOwner));
        bean.setEmployeeBusId(record.getString(HrModelPaths._Employee._Employee_EmployeeBusId));
        bean.setUserName(record.getString(HrModelPaths._Employee._Employee_UserName));
        bean.setFirstName(record.getString(HrModelPaths._Employee._Employee_FirstName));
        bean.setPreferredFirstName(record.getString(HrModelPaths._Employee._Employee_PreferredFirstName));
        bean.setMiddleName(record.getString(HrModelPaths._Employee._Employee_MiddleName));
        bean.setLastName(record.getString(HrModelPaths._Employee._Employee_LastName));
       // bean.setFullName(record.getString(HrModelPaths._Employee._Employee_FullName));
        bean.setGender(record.getString(HrModelPaths._Employee._Employee_Gender));
        bean.setSalutation(record.getString(HrModelPaths._Employee._Employee_Salutation));
        bean.setLanguage(record.getString(HrModelPaths._Employee._Employee_Language));
        bean.setEmail(record.getString(HrModelPaths._Employee._Employee_Email));
        bean.setTelephone(record.getString(HrModelPaths._Employee._Employee_Telephone));
        bean.setMobile(record.getString(HrModelPaths._Employee._Employee_Mobile));
        bean.setFax(record.getString(HrModelPaths._Employee._Employee_Fax));
        bean.setIsActive(record.get_boolean(HrModelPaths._Employee._Employee_IsActive));
        bean.setIsSystemEnabled(record.get_boolean(HrModelPaths._Employee._Employee_IsSystemEnabled));
        bean.setEmploymentCategory(record.getString(HrModelPaths._Employee._Employment_EmploymentCategory));
        bean.setManagementLevel(record.getString(HrModelPaths._Employee._Employment_ManagementLevel));
        bean.setSignOffRole(record.getString(HrModelPaths._Employee._Employment_SignOffRole));
        bean.setFunctionalTitle(record.getString(HrModelPaths._Employee._JobPosition_FunctionalTitle));
        bean.setPositionName(record.getString(HrModelPaths._Employee._JobPosition_PositionName));
        bean.setJobProfileName(record.getString(HrModelPaths._Employee._JobPosition_JobProfileName));
        bean.setIsLineManager(record.get_boolean(HrModelPaths._Employee._JobPosition_IsLineManager));
        bean.setOrganisationId(record.getString(HrModelPaths._Employee._JobPosition_OrganisationId));
        bean.setOrganisationBusId(record.getString(HrModelPaths._Employee._JobPosition_OrganisationBusId));
        bean.setLineManagerId(record.getString(HrModelPaths._Employee._JobPosition_LineManagerId));
        bean.setLineManagerBusId(record.getString(HrModelPaths._Employee._JobPosition_LineManagerBusId));
        bean.setDottedLineManagerId(record.getString(HrModelPaths._Employee._JobPosition_DottedLineManagerId));
        bean.setDottedLineManagerBusId(record.getString(HrModelPaths._Employee._JobPosition_DottedLineManagerBusId));
        bean.setBuildingId(record.getString(HrModelPaths._Employee._WorkplaceLocation_BuildingId));
        bean.setBuildingBusId(record.getString(HrModelPaths._Employee._WorkplaceLocation_BuildingBusId));
        
        //Employee Private Address
        bean.setPrivateStreet(record.getString(HrModelPaths._Employee._EmployeePrivateAddress_PrivateStreet));
        bean.setPrivatePostCode(record.getString(HrModelPaths._Employee._EmployeePrivateAddress_PrivatePostCode));
        bean.setPrivateTown(record.getString(HrModelPaths._Employee._EmployeePrivateAddress_PrivateTown));
        bean.setPrivateStateProvince(record.getString(HrModelPaths._Employee._EmployeePrivateAddress_PrivateStateProvince));
        bean.setPrivateCountryCode(record.getString(HrModelPaths._Employee._EmployeePrivateAddress_PrivateCountryCode));
        
        bean.setEmployerId(record.getString(HrModelPaths._Employee._EmployerCompany_EmployerId));
        bean.setEmployerBusId(record.getString(HrModelPaths._Employee._EmployerCompany_EmployerBusId));
        bean.setSourceSystem(record.getString(HrModelPaths._Employee._Control_SourceSystem));
        bean.setCreatedBy(record.getString(HrModelPaths._Employee._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(HrModelPaths._Employee._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(HrModelPaths._Employee._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(HrModelPaths._Employee._Control_LastUpdateTimestamp));
        
      
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{HrModelPaths._Employee._Employee_EmployeeId};
    }

    @Override
    public Object[] retrievePks(EmployeeBean bean) {
        return new Object[]{bean.getEmployeeId()};
    }
    
    /**
     * find employee records by username
     * 
     * @param userName
     * @return
     */
    public List<EmployeeBean> findBeansByUserName(String userName) {
    	
    	String predicateString = constructPredicate(HrModelPaths._Employee._Employee_UserName.format(), userName);
    	
    	return super.readBeansFor(predicateString);
    }
    
    /**
     * Find employee records by username and employee bus id
     * 
     * @param userName
     * @param employeeBusId
     * @return
     */
    public List<EmployeeBean> findBeansByEmployeeBusIdAndUserName(String userName, String employeeBusId) {
    	
    	StringBuffer predicate = new StringBuffer();
    	predicate.append(constructPredicate(HrModelPaths._Employee._Employee_UserName.format(), userName));
    	predicate.append(" and ");
    	XPathPredicate predicate2 = new XPathPredicate(Operation.Equals, HrModelPaths._Employee._Employee_EmployeeBusId, employeeBusId);
    	predicate.append(predicate2.toString());
    	
    	return super.readBeansFor(predicate.toString());
    }
	
	public List<EmployeeBean> findBeansByEmployeeBusId(String employeeBusId) {
    	
    	XPathPredicate predicate = new XPathPredicate(Operation.Equals, HrModelPaths._Employee._Employee_EmployeeBusId, employeeBusId);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate);
		
		return super.readBeansFor(group.toString());
	}
	
	public List<EmployeeBean> findUnresolvedLineMngsAndDottedLineMngs() {
		
		XPathPredicate predicate1 = new XPathPredicate(Operation.IsNull, HrModelPaths._Employee._JobPosition_LineManagerId, null);
		XPathPredicate predicate2 = new XPathPredicate(Operation.IsNull, HrModelPaths._Employee._JobPosition_DottedLineManagerId, null);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.Or);
		group.addPredicate(predicate1);
		group.addPredicate(predicate2);
		
		return super.readBeansFor(group.toString());
	}
	
	private static String constructPredicate(String fieldPath, String fieldValue) {
		
		StringBuffer predicate = new StringBuffer();
		predicate.append("osd:is-equal-case-insensitive(");
		predicate.append(fieldPath+",'");
		predicate.append(fieldValue+"')");
		return predicate.toString();
	}
}
