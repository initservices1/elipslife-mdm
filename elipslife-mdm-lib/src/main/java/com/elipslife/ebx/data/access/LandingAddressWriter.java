package com.elipslife.ebx.data.access;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.LandingAddressBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class LandingAddressWriter extends BaseAddressWriter<LandingAddressBean> {
	
	private boolean useDefaultValues;
	
	public LandingAddressWriter(boolean useDefaultValues) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.LANDING_REFERENCE, PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_Address.getPathInSchema()),
			useDefaultValues);
	}
	
	
	public LandingAddressWriter(AdaptationTable adaptationTable, boolean useDefaultValues) {
		super(new LandingAddressReader(adaptationTable), adaptationTable);
		
		this.useDefaultValues = useDefaultValues;
	}

	
	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, LandingAddressBean bean) {

		values.put(LandingPaths._LDG_Address._Address_AddressId, bean.getId());
		
		if(!useDefaultValues || bean.getAddressType() != null) {
			values.put(LandingPaths._LDG_Address._Address_AddressType, bean.getAddressType());
		}
		
		values.put(LandingPaths._LDG_Address._Address_AddressUse, bean.getAddressUsage());
		values.put(LandingPaths._LDG_Address._Address_SourceAddressBusId, bean.getSourceAddressBusId());
		values.put(LandingPaths._LDG_Address._PostalAddress_Street, bean.getStreet());
		values.put(LandingPaths._LDG_Address._PostalAddress_Street2, bean.getStreet2());
		values.put(LandingPaths._LDG_Address._PostalAddress_PostCode, bean.getPostCode());
		values.put(LandingPaths._LDG_Address._PostalAddress_POBox, bean.getPoBox());
		values.put(LandingPaths._LDG_Address._PostalAddress_POBoxPostCode, bean.getPoBoxPostCode());
		values.put(LandingPaths._LDG_Address._PostalAddress_POBoxTown, bean.getPoBoxTown());
		values.put(LandingPaths._LDG_Address._PostalAddress_District, bean.getDistrict());
		values.put(LandingPaths._LDG_Address._PostalAddress_Town, bean.getTown());
		values.put(LandingPaths._LDG_Address._PostalAddress_StateProvince, bean.getStateProvince());
		values.put(LandingPaths._LDG_Address._PostalAddress_Country, bean.getCountry());
		values.put(LandingPaths._LDG_Address._Address_CorrelationId, bean.getCorrelationId()); // Correlation Id
		values.put(LandingPaths._LDG_Address._Address_LandingPartyId, bean.getLandingPartyId()); // Landing Party Id
		values.put(LandingPaths._LDG_Address._Address_MdmStagingProcessResponse, bean.getMdmStagingProcessResponse()); // MDM Staging Process Response
		values.put(LandingPaths._LDG_Address._ElectronicAddress_ElectronicAddressType, bean.getElectronicAddressType());
		values.put(LandingPaths._LDG_Address._ElectronicAddress_Address, bean.getAddress());
		values.put(LandingPaths._LDG_Address._Control_SourceSystem, bean.getSourceSystem());
		values.put(LandingPaths._LDG_Address._Control_SourcePartyBusId, bean.getSourcePartyBusId());
		values.put(LandingPaths._LDG_Address._Control_ValidFrom, bean.getValidFrom());
		values.put(LandingPaths._LDG_Address._Control_ValidTo, bean.getValidTo());
		values.put(LandingPaths._LDG_Address._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(LandingPaths._LDG_Address._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(LandingPaths._LDG_Address._Control_Action, bean.getAction());
		values.put(LandingPaths._LDG_Address._Control_ActionBy, bean.getActionBy());
	}
}