package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;

import javax.json.bind.annotation.JsonbProperty;

public class LegalContract {
	
	@JsonbProperty("ContractEndDate")
	public LocalDate contractEndDate;
	
	@JsonbProperty("ContractState")
	public String contractState;
	
	@JsonbProperty("CustomerRefNumber")
	public String customerRefNumber;
	
	@JsonbProperty("IsActive")
	public String isActive;
	
	@JsonbProperty("IsReplaced")
	public String isReplaced;
	
	@JsonbProperty("VersionNumber")
	public Integer versionNumber;
}