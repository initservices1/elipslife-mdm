package com.elipslife.mdm.contract.core;

import java.util.HashMap;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class CoreContractCoverWriter extends AbstractTableWriter<CoreContractCoverBean> {

	private CoreContractCoverReader coreContractCoverReader;
	
	public CoreContractCoverWriter() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public CoreContractCoverWriter(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._ContractCover.getPathInSchema()));
	}
	
	public CoreContractCoverWriter(AdaptationTable table) {
		super(table);
		
		this.coreContractCoverReader = new CoreContractCoverReader(table);
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, CoreContractCoverBean bean) {
		
		values.put(ContractPaths._ContractCover._Exposure_ExposureId, bean.getExposureId());
		values.put(ContractPaths._ContractCover._Exposure_ExposureBusId, bean.getExposureBusId());
		
		values.put(ContractPaths._ContractCover._Cover_ContractCoverId, bean.getId());
		values.put(ContractPaths._ContractCover._Cover_CoverBusId, bean.getCoverBusId());
		values.put(ContractPaths._ContractCover._Cover_CoverGroupType, bean.getCoverGroupType());
		values.put(ContractPaths._ContractCover._Cover_CoverGroupTypeName, bean.getCoverGroupTypeName());
		values.put(ContractPaths._ContractCover._Cover_CoverName, bean.getCoverName());
		values.put(ContractPaths._ContractCover._Cover_CoverTemplateBusId, bean.getCoverTemplateBusId());
		values.put(ContractPaths._ContractCover._Cover_BeneficiaryType, bean.getBeneficiaryType());
		
		values.put(ContractPaths._ContractCover._Control_SourceSystem, bean.getSourceSystem());
		values.put(ContractPaths._ContractCover._Control_CreatedBy, bean.getCreatedBy());
		values.put(ContractPaths._ContractCover._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(ContractPaths._ContractCover._Control_LastUpdatedBy, bean.getLastUpdatedBy());
		values.put(ContractPaths._ContractCover._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(ContractPaths._ContractCover._Control_LastSyncAction, bean.getLastSyncAction());
		values.put(ContractPaths._ContractCover._Control_LastSyncTimestamp, bean.getLastSyncTimestamp());
	}

	@Override
	public AbstractTableReader<CoreContractCoverBean> getTableReader() {
		return this.coreContractCoverReader;
	}
}