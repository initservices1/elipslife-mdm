package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.json.bind.annotation.JsonbProperty;

public class Party {
	
	@JsonbProperty("DataOwner")
	public String dataOwner;
	
	@JsonbProperty("OperatingCountry")
	public String operatingCountry;
	
	@JsonbProperty("PartyBusId")
	public String partyBusId;
	
	@JsonbProperty("CRMId")
	public String crmId;
	
	@JsonbProperty("PartyType")
	public String partyType;
	
	@JsonbProperty("RelationshipOwner")
	public String relationshipOwner;
	
	@JsonbProperty("DataResponsible")
	public String dataResponsible;
	
	@JsonbProperty("Language")
	public String language;
	
	@JsonbProperty("IsActive")
	public Boolean isActive;
	
	// Person
	@JsonbProperty("LastName")
	public String lastName;
	
	@JsonbProperty("MiddleName")
	public String middleName;
	
	@JsonbProperty("FirstName")
	public String firstName;
	
	@JsonbProperty("DateOfBirth")
	public LocalDate dateOfBirth;
	
	@JsonbProperty("Gender")
	public String gender;
	
	@JsonbProperty("Title")
	public String title;
	
	@JsonbProperty("Salutation")
	public String salutation;
	
	@JsonbProperty("LetterSalutation")
	public String letterSalutation;
	
	@JsonbProperty("CivilStatus")
	public String civilStatus;
	
	@JsonbProperty("BirthCity")
	public String birthCity;
	
	@JsonbProperty("BirthCountry")
	public String birthCountry;
	
	@JsonbProperty("IdentityDocument")
	public IdentityDocument identityDocument;
	
	// Organization/Company
	@JsonbProperty("ParentBusId")
	public String parentBusId;
	
	@JsonbProperty("OrgName")
	public String orgName;
	
	@JsonbProperty("OrgName2")
	public String orgName2;
	
	@JsonbProperty("CompanyType")
	public String companyType;
	
	@JsonbProperty("CommercialRegNo")
	public String commercialRegNo;
	
	@JsonbProperty("UIDNumber")
	public String uidNumber;
	
	@JsonbProperty("VATNumber")
	public String vatNumber ;
	
	@JsonbProperty("Industry")
	public String industry;
	
	@JsonbProperty("SubIndustry")
	public String subIndustry;
	
	@JsonbProperty("PreferredEmail")
	public String preferredEmail;
	
	@JsonbProperty("PreferredCommChannel")
	public String preferredCommChannel;
	
	// Contact person
	@JsonbProperty("OrganisationBusId")
	public String organisationBusId;
	
	@JsonbProperty("LineManagerBusId")
	public String lineManagerBusId;
	
	@JsonbProperty("PartnerBusId")
	public String partnerBusId;
	
	@JsonbProperty("Department")
	public String department;
	
	@JsonbProperty("Function")
	public String function;
	
	@JsonbProperty("IsExecutor")
	public Boolean isExecutor;
	
	@JsonbProperty("IsLegalRepresentative")
    public Boolean isLegalRepresentative;
	
	@JsonbProperty("IsSeniorManager")
	public Boolean isSeniorManager;
	
	@JsonbProperty("IsUltimateBeneficialOwner")
	public Boolean isUltimateBeneficialOwner;
	
	@JsonbProperty("BORelationship")
	public String boRelationship;
	
	@JsonbProperty("ContactPersonState")
	public String contactPersonState;
	
	// Roles
	@JsonbProperty("Roles")
	public String roles;
	
	@JsonbProperty("IsVIPClient")
	public Boolean isVipClient;
	
	@JsonbProperty("Blacklist")
	public String blacklist;
	
	@JsonbProperty("PartnerState")
	public String partnerState;
	
	@JsonbProperty("CompanyPersonalNr")
	public String companyPersonalNr;
	
	@JsonbProperty("SocialSecurityNr")
	public String socialSecurityNr;
	
	@JsonbProperty("SocialSecurityNrType")
	public String socialSecurityNrType;
	
	@JsonbProperty("BrokerRegNo")
	public String brokerRegNo;
	
	@JsonbProperty("BrokerTiering")
	public String brokerTiering;
	
	@JsonbProperty("LineOfBusiness")
	public String lineOfBusiness;
	
	// Financials
	@JsonbProperty("FinancePartyBusId")
	public String financePartyBusId;
	
	@JsonbProperty("PaymentTerms")
	public String paymentTerms;
	
	@JsonbProperty("FinanceClients")
	public String financeClients;
	
	@JsonbProperty("CreditorClients")
	public String creditorClients;
	
	@JsonbProperty("BankAccounts")
	public Collection<BankAccount> bankAccounts;
	
	// Communication
	
	@JsonbProperty("Addresses")
	public Collection<Address> addresses;
	
	// Control
	@JsonbProperty("ValidFrom")
	public LocalDate validFrom;
	
	@JsonbProperty("ValidTo")
	public LocalDate validTo;
	
	@JsonbProperty("CreationTimestamp")
	public LocalDateTime creationTimestamp;
	
	@JsonbProperty("LastUpdateTimestamp")
	public LocalDateTime lastUpdateTimestamp;
	
	@JsonbProperty("Action")
	public String action;
}