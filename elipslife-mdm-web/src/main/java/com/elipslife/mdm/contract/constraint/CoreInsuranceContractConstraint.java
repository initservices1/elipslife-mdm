package com.elipslife.mdm.contract.constraint;

import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import com.butos.ebx.smart.validation.SmartRecordAndTableLevelValidationCheck;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaNode;

public class CoreInsuranceContractConstraint extends SmartRecordAndTableLevelValidationCheck {

	@Override
	public void setup(ConstraintContextOnTable aContext) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	protected void executeBusinessValidations(
			ValueContext valueContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap,
			boolean isFstRec) {

		validateReferencedParty(
				valueContext,
				validationInfoMap,
				"Policyholder",
				ContractPaths._InsuranceContract._Parties_Policyholder_PolicyholderId,
				ContractPaths._InsuranceContract._Parties_Policyholder_PolicyholderBusId,
				ContractPaths._InsuranceContract._Control_SourceSystem,
				ContractPaths._InsuranceContract._Parties_Policyholder_SourcePolicyholderBusId);
		
		validateReferencedParty(
				valueContext,
				validationInfoMap,
				"Broker",
				ContractPaths._InsuranceContract._Parties_Broker_BrokerId,
				ContractPaths._InsuranceContract._Parties_Broker_BrokerBusId,
				ContractPaths._InsuranceContract._Control_SourceSystem,
				ContractPaths._InsuranceContract._Parties_Broker_SourceBrokerBusId);
		
		validateReferencedParty(
				valueContext,
				validationInfoMap,
				"AttachedPartner",
				ContractPaths._InsuranceContract._Parties_AttachedPartner_AttachedPartnerId,
				ContractPaths._InsuranceContract._Parties_AttachedPartner_AttachedPartnerBusId,
				ContractPaths._InsuranceContract._Control_SourceSystem,
				ContractPaths._InsuranceContract._Parties_AttachedPartner_SourceAttachedPartnerBusId);
		
		validateReferencedEmployee(
				valueContext,
				validationInfoMap,
				"AdminResponsible",
				ContractPaths._InsuranceContract._Parties_AdminResponsible_AdminResponsibleId,
				ContractPaths._InsuranceContract._Parties_AdminResponsible_AdminResponsibleUserName);
		
		validateReferencedEmployee(
				valueContext,
				validationInfoMap,
				"ClaimResponsible",
				ContractPaths._InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleId,
				ContractPaths._InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleUserName);
		
		validateReferencedEmployee(
				valueContext,
				validationInfoMap,
				"CaseMgmtResponsible",
				ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleId,
				ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleUserName);
		
		validateReferencedEmployee(
				valueContext,
				validationInfoMap,
				"SalesResponsible",
				ContractPaths._InsuranceContract._Parties_SalesResponsible_SalesResponsibleId,
				ContractPaths._InsuranceContract._Parties_SalesResponsible_SalesResponsibleUserName);
		
		validateMandatoryCheck(
				valueContext,
				validationInfoMap);
		
	}

	
	/**
	 * 
	 * @param context
	 * @param businessObject
	 * @param pathId
	 * @param pathBusId
	 * @param pathSourceSystem
	 * @param pathSourceBusId
	 */
	private void validateReferencedParty(
			ValueContext valueContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap,
			String businessObject,
			Path pathId,
			Path pathBusId,
			Path pathSourceSystem,
			Path pathSourceBusId) {

		String valueId = (String)valueContext.getValue(pathId);
		
		if(valueId == null || valueId.isEmpty()) {
			
			String valueBusId = (String)valueContext.getValue(pathBusId);
			
			if(valueBusId != null && !valueBusId.isEmpty()) {
				UserMessage userMessage = UserMessage.createWarning(String.format("%s not found by %sBusId %s", businessObject, businessObject, valueBusId));
				super.insertUserMsg(validationInfoMap, valueContext.getNode(pathId), userMessage);
				return;
			}
			
			String sourceSystem = (String)valueContext.getValue(pathSourceSystem);
			String valueSourceBusId = (String)valueContext.getValue(pathSourceBusId);
			
			if(sourceSystem != null && !sourceSystem.isEmpty() && valueSourceBusId != null && !valueSourceBusId.isEmpty()) {
				UserMessage userMessage = UserMessage.createWarning(String.format("%s not found by SourceSystem %s and Source%sBusId %s", businessObject, sourceSystem, businessObject, valueSourceBusId));
				super.insertUserMsg(validationInfoMap, valueContext.getNode(pathId), userMessage);
				return;
			}
		}
	}
	
	
	/**
	 * 
	 * @param valueContext
	 * @param validationInfoMap
	 * @param businessObject
	 * @param pathId
	 * @param pathUserName
	 */
	private void validateReferencedEmployee(
			ValueContext valueContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap,
			String businessObject,
			Path pathId,
			Path pathUserName) {
		
		String valueId = (String)valueContext.getValue(pathId);
		
		if(valueId == null || valueId.isEmpty()) {
			
			String valueUserName = (String) valueContext.getValue(pathUserName);
			
			if(valueUserName != null && !valueUserName.isEmpty()) {
				UserMessage userMessage = UserMessage.createWarning(String.format("%s not found by UserName %s", businessObject, valueUserName));
				super.insertUserMsg(validationInfoMap, valueContext.getNode(pathId), userMessage);
				return;
			}
		}
	}
	
	/**
	 * Application Number, Offer Number, Contract Number : Any of these fields are mandatory
	 * 
	 * @param valueContext
	 * @param validationInfoMap
	 */
	private void validateMandatoryCheck(
			ValueContext valueContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap) {
		
		if(!(isFilledWithAnyValue(valueContext, ContractPaths._InsuranceContract._ContractIds_ApplicationNumber) ||
				isFilledWithAnyValue(valueContext, ContractPaths._InsuranceContract._ContractIds_OfferNumber)  ||
				isFilledWithAnyValue(valueContext, ContractPaths._InsuranceContract._ContractIds_ContractNumber))) {
			
			UserMessage userMessage = UserMessage.createWarning(String.format("Application Number, Offer Number, Contract Number : Any of these fields are mandatory"));
			super.insertUserMsg(validationInfoMap, valueContext.getNode(ContractPaths._InsuranceContract._ContractIds_ApplicationNumber), userMessage);
			return;
		}
		
	}
	
	/**
	 * Check the field have some value
	 * 
	 * @param valueContext
	 * @param pathId
	 * @return
	 */
	private boolean isFilledWithAnyValue(ValueContext valueContext, Path pathId) {
		
		String valueId = (String) valueContext.getValue(pathId);
		if(valueId == null || valueId.isEmpty()) {
			return false;
		}
		
		return true;
	}
}