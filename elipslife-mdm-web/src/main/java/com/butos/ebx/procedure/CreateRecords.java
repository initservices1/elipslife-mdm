package com.butos.ebx.procedure;

import java.util.ArrayList;
import java.util.List;

import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ValueContextForUpdate;

/**
 * <h1>CreateRecords</h1>
 * 
 * Class for bulk record insert.
 * 
 * 
 * @author Butos AG
 * @version 1.0
 * @since 2019-10-01
 * 
 */
public class CreateRecords implements Procedure
{

	private List<RecordValuesBean> insertRecordList;
	private AdaptationTable insertTable;
	private Adaptation record;
	private List<Adaptation> recordsList = new ArrayList<Adaptation>();

	/**
	 * Instantiates a new update records procedure.
	 */
	public CreateRecords()
	{
		super();
	}

	/**
	 * Instantiates a new update records procedure.
	 *
	 * @param recordDefinitions the record definitions
	 */
	public CreateRecords(AdaptationTable insertTable
			,List<RecordValuesBean> insertRecordList )
	{
		super();
		this.insertRecordList = insertRecordList;
		this.insertTable = insertTable;
	}
  
	/**
	 * Execute.
	 *
	 * @param pContext the context
	 * @throws Exception the exception
	 */
	@Override
	public void execute(final ProcedureContext context) throws OperationException
	{
		if (this.insertRecordList == null || this.insertTable == null ) {
			OperationException exception = OperationException
					.createError(this.getClass()+", Initialization error.");
			throw exception;
		}
		
		for (RecordValuesBean insertRecord : this.insertRecordList)
		{
			ValueContextForUpdate valueContextInsertRecord = context
					.getContextForNewOccurrence(this.insertTable);
			
			insertRecord.getValues().forEach((key,value)->
					valueContextInsertRecord.setValue(value, key));
			this.record = context.doCreateOccurrence(valueContextInsertRecord, this.insertTable);
			recordsList.add(record);
		}
	}
	
	public List<Adaptation> getCreateRecordList() {
	    
	    return this.recordsList;
	}

}
