package com.elipslife.mdm.party.constraint;

import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import com.butos.ebx.smart.validation.SmartRecordAndTableLevelValidationCheck;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class LandingAddressBulkProcessConstraint extends SmartRecordAndTableLevelValidationCheck {

    @Override
    public void setup(ConstraintContextOnTable aContext) {
        
    }

    @Override
    public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
        return null;
    }

    @Override
    protected void executeBusinessValidations(ValueContext recordContext,
            Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {

    }
    
    

}
