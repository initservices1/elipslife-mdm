package com.elipslife.mdm.party.scripttask;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.CoreAddressBean;
import com.elipslife.mdm.path.PartyPaths;
import com.orchestranetworks.schema.Path;

public class CoreAddressConverter {

    public static HashMap<Path, Object> fillCoreAddressValuesFromStagingAddressBean(CoreAddressBean bean) {

        HashMap<Path, Object> values = new HashMap<Path, Object>();
        
        values.put(PartyPaths._Address._AddressInfo_AddressId, bean.getId());

        values.put(PartyPaths._Address._AddressInfo_PartyId, bean.getPartyId());
        values.put(PartyPaths._Address._AddressInfo_AddressType, bean.getAddressType());
        values.put(PartyPaths._Address._AddressInfo_AddressUse, bean.getAddressUsage());
        values.put(PartyPaths._Address._AddressInfo_SourceAddressBusId, bean.getSourceAddressBusId());
        values.put(PartyPaths._Address._PostalAddress_Street, bean.getStreet());
        values.put(PartyPaths._Address._PostalAddress_Street2, bean.getStreet2());
        values.put(PartyPaths._Address._PostalAddress_Town, bean.getTown());
        values.put(PartyPaths._Address._PostalAddress_PostCode, bean.getPostCode());
        values.put(PartyPaths._Address._PostalAddress_POBox, bean.getPoBox());
        values.put(PartyPaths._Address._PostalAddress_POBoxPostCode, bean.getPoBoxPostCode());
        values.put(PartyPaths._Address._PostalAddress_POBoxTown, bean.getPoBoxTown());
        values.put(PartyPaths._Address._PostalAddress_District, bean.getDistrict());
        values.put(PartyPaths._Address._PostalAddress_StateProvince, bean.getStateProvince());
        values.put(PartyPaths._Address._PostalAddress_Country, bean.getCountry());
        values.put(PartyPaths._Address._ElectronicAddress_ElectronicAddressType, bean.getElectronicAddressType());
        values.put(PartyPaths._Address._ElectronicAddress_Address, bean.getAddress());
        values.put(PartyPaths._Address._Control_SourceSystem, bean.getSourceSystem());
        values.put(PartyPaths._Address._Control_SourcePartyBusId, bean.getSourcePartyBusId());
        values.put(PartyPaths._Address._Control_CreatedBy, bean.getCreatedBy());
        values.put(PartyPaths._Address._Control_CreationTimestamp, bean.getCreationTimestamp());
        values.put(PartyPaths._Address._Control_LastUpdatedBy, bean.getLastUpdatedBy());
        values.put(PartyPaths._Address._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
        values.put(PartyPaths._Address._Control_LastSyncAction, bean.getLastSyncAction());
        values.put(PartyPaths._Address._Control_LastSyncTimestamp, bean.getLastSyncTimestamp());
        values.put(PartyPaths._Address._Control_ValidFrom, bean.getValidFrom());
        values.put(PartyPaths._Address._Control_ValidTo, bean.getValidTo());

        if (bean.getIsExpired() != null) {
            values.put(PartyPaths._Address._Control_IsExpired, bean.getIsExpired());
        }
        return values;

    }

}
