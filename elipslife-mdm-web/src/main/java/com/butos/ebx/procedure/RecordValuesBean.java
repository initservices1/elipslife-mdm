package com.butos.ebx.procedure;

import java.util.*;

import com.onwbp.adaptation.*;
import com.orchestranetworks.schema.*;

/**
 * The Class RecordValuesBean.
 *
 * @author ATI
 */
public class RecordValuesBean
{
	private Adaptation record;
	private AdaptationTable table;
	private HashMap<Path, Object> values;

	/**
	 * Instantiates a new record values bean.
	 */
	public RecordValuesBean()
	{
	}

	/**
	 * Instantiates a new record values bean.
	 *
	 * @param record the record
	 * @param values the values
	 */
	public RecordValuesBean(final Adaptation record, final HashMap<Path, Object> values)
	{
		this.record = record;
		this.values = values;
	}
	
	public RecordValuesBean(final HashMap<Path, Object> values)
    {
        // this.record = record;
        this.values = values;
    }

	/**
	 * Instantiates a new record values bean.
	 *
	 * @param table the table
	 * @param record the record
	 * @param values the values
	 */
	public RecordValuesBean(
		final AdaptationTable table,
		final Adaptation record,
		final HashMap<Path, Object> values)
	{
		this.table = table;
		this.record = record;
		this.values = values;
	}

	/**
	 * Instantiates a new record values bean.
	 *
	 * @param table the table
	 * @param values the values
	 */
	public RecordValuesBean(final AdaptationTable table, final HashMap<Path, Object> values)
	{
		this.table = table;
		this.values = values;
	}

	/**
	 * Gets the record.
	 *
	 * @return the record
	 */
	public Adaptation getRecord()
	{
		return this.record;
	}

	/**
	 * Gets the table.
	 *
	 * @return the table
	 */
	public AdaptationTable getTable()
	{
		return this.table;
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public HashMap<Path, Object> getValues()
	{
		return this.values;
	}

	/**
	 * Sets the record.
	 *
	 * @param record the new record
	 */
	public void setRecord(final Adaptation record)
	{
		this.record = record;
	}

	/**
	 * Sets the table.
	 *
	 * @param table the new table
	 */
	public void setTable(final AdaptationTable table)
	{
		this.table = table;
	}

	/**
	 * Sets the values.
	 *
	 * @param values the values
	 */
	public void setValues(final HashMap<Path, Object> values)
	{
		this.values = values;
	}

}
