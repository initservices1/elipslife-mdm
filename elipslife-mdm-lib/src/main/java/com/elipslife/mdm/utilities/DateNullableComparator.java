package com.elipslife.mdm.utilities;

import java.util.Calendar;
import java.util.Date;

public class DateNullableComparator implements java.util.Comparator<Date> {

	private DateNullableComparator() {
		
	}
	
	@Override
	public int compare(Date date1, Date date2) {
		if(date1 == null && date2 == null) {
			return 0;
		}
		
		if(date1 == null && date2 != null) {
			return -1;
		}
		
		if(date1 != null && date2 == null) {
			return 1;
		}
		
		return date1.compareTo(date2);
	}
	
	private static DateNullableComparator dateNullableComparator = null;
	
	public static DateNullableComparator getInstance() {
		
		if(dateNullableComparator == null) {
			dateNullableComparator = new DateNullableComparator();
		}
		
		return dateNullableComparator;
	}
	
	
	/**
	 * 
	 * @param dateFrom
	 * @param dateTo
	 * @return
	 */
	public static boolean dateWithinRange(Date dateFrom, Date dateTo) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		Date dateNow = calendar.getTime();
		
		Date dateMinimum = new Date(Long.MIN_VALUE);
		Date dateMaximum = new Date(Long.MAX_VALUE);
		
		return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0 &&
				dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
	}
	
	
	/**
	 * 
	 * @param date
	 * @return
	 */
	public static Date removeTime(Date date) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return calendar.getTime();
	}
}
