package com.elipslife.mdm.contract.core;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class CoreInsuranceContractWriterSession extends CoreInsuranceContractWriter {

	private Session session;
	
	public CoreInsuranceContractWriterSession(Session session) {
		this(session, false);
	}
	
	public CoreInsuranceContractWriterSession(Session session, boolean useDefaultValues) {
		super(useDefaultValues);
		this.session = session;
	}
		
	@Override
	protected void executeProcedure(Procedure procedure) {
		ProcedureResult procedureResult = ProgrammaticService.createForSession(this.session,this.dataSpace).execute(procedure);
		if(procedureResult.hasFailed()) {
			throw new RuntimeException(procedureResult.getException());
		}
	}
}