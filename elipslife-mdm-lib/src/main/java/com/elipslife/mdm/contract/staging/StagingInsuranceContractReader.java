package com.elipslife.mdm.contract.staging;

import java.util.List;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;

public class StagingInsuranceContractReader extends AbstractTableReader<StagingInsuranceContractBean> {

	public StagingInsuranceContractReader() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public StagingInsuranceContractReader(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._STG_InsuranceContract.getPathInSchema()));
	}
	
	public StagingInsuranceContractReader(AdaptationTable table) {
		super(table);
	}

	@Override
	protected StagingInsuranceContractBean createBeanFromRecord(Adaptation record) {
		StagingInsuranceContractBean bean = new StagingInsuranceContractBean();
		
		bean.setId(record.get_int(ContractPaths._STG_InsuranceContract._ContractIds_InsuranceContractId));
		bean.setDataOwner(record.getString(ContractPaths._STG_InsuranceContract._ContractIds_DataOwner));
		bean.setContractStageBusId(record.getString(ContractPaths._STG_InsuranceContract._ContractIds_ContractStageBusId));
		bean.setContractNumber(record.getString(ContractPaths._STG_InsuranceContract._ContractIds_ContractNumber));
		bean.setOfferNumber(record.getString(ContractPaths._STG_InsuranceContract._ContractIds_OfferNumber));
		bean.setApplicationNumber(record.getString(ContractPaths._STG_InsuranceContract._ContractIds_ApplicationNumber));
		bean.setContractBusId2(record.getString(ContractPaths._STG_InsuranceContract._ContractIds_ContractBusId2));
		bean.setCustomerRefNumber(record.getString(ContractPaths._STG_InsuranceContract._ContractIds_CustomerRefNumber));
		bean.setInsuranceContractMaster(record.getString(ContractPaths._STG_InsuranceContract._ContractIds_InsuranceContractMaster));
		
		bean.setContractStage(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_ContractStage));
		bean.setContractState(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_ContractState));
		bean.setOfferState(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_OfferState));
		bean.setApplicationState(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_ApplicationState));
		bean.setContractClass(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_ContractClass));
		bean.setBusinessContractBeginDate(record.getDate(ContractPaths._STG_InsuranceContract._ContractCore_BusinessContractBeginDate));
		bean.setContractExpiryDate(record.getDate(ContractPaths._STG_InsuranceContract._ContractCore_ContractExpiryDate));
		bean.setContractBeginDate(record.getDate(ContractPaths._STG_InsuranceContract._ContractCore_ContractBeginDate));
		bean.setContractEndDate(record.getDate(ContractPaths._STG_InsuranceContract._ContractCore_ContractEndDate));
		bean.setBusinessScope(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_BusinessScope));
		bean.setContractName(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_ContractName));
		bean.setAlternativeContractName(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_AlternativeContractName));
		bean.setClientSegment(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_ClientSegment));
		bean.setDistributionAgreementBusId(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_DistributionAgreementBusId));
		bean.setIsActive((Boolean)record.get(ContractPaths._STG_InsuranceContract._ContractCore_IsActive));
		bean.setIsProfitParticipation((Boolean)record.get(ContractPaths._STG_InsuranceContract._ContractCore_IsProfitParticipation));
		bean.setLineOfBusiness(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_LineOfBusiness));
		bean.setOperatingCountry(record.getString(ContractPaths._STG_InsuranceContract._ContractCore_OperatingCountry));
		
		bean.setSourcePolicyholderBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_Policyholder_SourcePolicyholderBusId));
		bean.setPolicyholderBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_Policyholder_PolicyholderBusId));
		
		bean.setSourceBrokerBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_Broker_SourceBrokerBusId));
		bean.setBrokerBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_Broker_BrokerBusId));
		
		bean.setSourceAttachedPartnerBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_AttachedPartner_SourceAttachedPartnerBusId));
		bean.setAttachedPartnerBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_AttachedPartner_AttachedPartnerBusId));
		
		bean.setSourceAdminResponsibleBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_AdminResponsible_SourceAdminResponsibleBusId));
		bean.setAdminResponsibleUserName(record.getString(ContractPaths._STG_InsuranceContract._Parties_AdminResponsible_AdminResponsibleUserName));
		
		bean.setSourceClaimResponsibleBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_ClaimResponsible_SourceClaimResponsibleBusId));
		bean.setClaimResponsibleUserName(record.getString(ContractPaths._STG_InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleUserName));
		
		bean.setSourceCaseMgmtResponsibleBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_CaseMgmtResponsible_SourceCaseMgmtResponsibleBusId));
		bean.setCaseMgmtResponsibleUserName(record.getString(ContractPaths._STG_InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleUserName));
		
		bean.setSourceSalesResponsibleBusId(record.getString(ContractPaths._STG_InsuranceContract._Parties_SalesResponsible_SourceSalesResponsibleBusId));
		bean.setSalesResponsibleUserName(record.getString(ContractPaths._STG_InsuranceContract._Parties_SalesResponsible_SalesResponsibleUserName));
		
		bean.setSourceSystem(record.getString(ContractPaths._STG_InsuranceContract._Control_SourceSystem));
		bean.setSourceContractBusId(record.getString(ContractPaths._STG_InsuranceContract._Control_SourceContractBusId));
		bean.setCreationTimestamp(record.getDate(ContractPaths._STG_InsuranceContract._Control_CreationTimestamp));
		bean.setLastUpdateTimestamp(record.getDate(ContractPaths._STG_InsuranceContract._Control_LastUpdateTimestamp));
		bean.setLastSyncTimeStamp(record.getDate(ContractPaths._STG_InsuranceContract._Control_LastSyncTimestamp));
		bean.setAction(record.getString(ContractPaths._STG_InsuranceContract._Control_Action));
		bean.setActionBy(record.getString(ContractPaths._STG_InsuranceContract._Control_ActionBy));
		
		return bean;
	}

	@Override
	protected Path[] getPkPaths() {
		return new Path[] { ContractPaths._STG_InsuranceContract._ContractIds_InsuranceContractId };
	}

	@Override
	protected Object[] retrievePks(StagingInsuranceContractBean bean) {
		return new Object[] { bean.getId() };
	}
	
	
	/**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}
	
	/**
	 * Get all staging records with Insurance Contract Master is null
	 * 
	 * @return
	 */
	public List<StagingInsuranceContractBean> getAllBeansWithInsuranceContractMasterIsEmpty() {
		
		XPathPredicate predicate1 = new XPathPredicate(Operation.IsNull, ContractPaths._STG_InsuranceContract._ContractIds_InsuranceContractMaster, null);
		
		return super.readBeansFor(predicate1.toString());
	}
}