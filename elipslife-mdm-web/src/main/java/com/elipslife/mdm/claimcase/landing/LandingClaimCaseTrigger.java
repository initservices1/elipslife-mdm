package com.elipslife.mdm.claimcase.landing;

import java.util.Date;

import com.elipslife.mdm.claimcase.constants.ClaimCaseConstants;
import com.elipslife.mdm.claimcase.staging.StagingClaimCaseBean;
import com.elipslife.mdm.claimcase.staging.StagingClaimCaseWriter;
import com.elipslife.mdm.claimcase.staging.StagingClaimCaseWriterProcedureContext;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;

public class LandingClaimCaseTrigger extends TableTrigger {

	@Override
	public void setup(TriggerSetupContext aContext) {

	}
	
	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {
		// only allow DataSpace "ClaimCaseReference" to move
		if (aContext.getAdaptationHome().getKey().getName().equals(ClaimCaseConstants.DataSpace.CLAIMCASE_REF)) {
			moveClaimCaseFromLandingToStaging(aContext.getSession(), aContext.getProcedureContext(), aContext.getAdaptationOccurrence());
		}
	}

	public void moveClaimCaseFromLandingToStaging(Session session, ProcedureContext context, Adaptation adaptation) {
		// 1. Read landing ClaimCase		
		LandingClaimCaseReader landingClaimCaseReader = new LandingClaimCaseReader();
		LandingClaimCaseBean landingClaimCaseBean = landingClaimCaseReader.createBeanFromRecord(adaptation);
		
		// 2. Copy values into new StagingClaimCase
		StagingClaimCaseBean stagingClaimCaseBean = new StagingClaimCaseBean();
		transferbean(landingClaimCaseBean, stagingClaimCaseBean);

		// 3. Create StagingClaimCase
		StagingClaimCaseWriter stagingClaimCaseWriter = new StagingClaimCaseWriterProcedureContext(context);
		stagingClaimCaseWriter.createBeanOrThrow(stagingClaimCaseBean);

		// 4. Delete LandingClaimCase
		LandingClaimCaseWriter landingClaimCaseWriter = new LandingClaimCaseWriterProcedureContext(context);
		landingClaimCaseWriter.delete(landingClaimCaseBean);
	}
	
	public void transferbean(LandingClaimCaseBean landingClaimCaseBean, StagingClaimCaseBean stagingClaimCaseBean) {
		// ClaimCase_General
		String dataOwner = landingClaimCaseBean.getDataOwner() != null ? landingClaimCaseBean.getDataOwner() : "0";
		stagingClaimCaseBean.setDataOwner(dataOwner);
		stagingClaimCaseBean.setClaimCaseBusId(landingClaimCaseBean.getClaimCaseBusId());
		stagingClaimCaseBean.setClaimCaseExtRef(landingClaimCaseBean.getClaimCaseExtRef());
		stagingClaimCaseBean.setContractNumber(landingClaimCaseBean.getContractNumber());
		stagingClaimCaseBean.setSourceContractBusId(landingClaimCaseBean.getSourceContractBusId());
		stagingClaimCaseBean.setOperatingCountry(landingClaimCaseBean.getOperatingCountry());
		stagingClaimCaseBean.setBusinessScope(landingClaimCaseBean.getBusinessScope());
		stagingClaimCaseBean.setStartDate(landingClaimCaseBean.getStartDate());
		stagingClaimCaseBean.setEndDate(landingClaimCaseBean.getEndDate());
		stagingClaimCaseBean.setReportingDate(landingClaimCaseBean.getReportingDate());
		Boolean isActive = landingClaimCaseBean.getIsActive() != null ? landingClaimCaseBean.getIsActive() : true;
		stagingClaimCaseBean.setIsActive(isActive);
		
		// Party
		stagingClaimCaseBean.setClientSegment(landingClaimCaseBean.getClientSegment());
		stagingClaimCaseBean.setPolicyHolderBusId(landingClaimCaseBean.getPolicyHolderBusId());
		stagingClaimCaseBean.setSourcePolicyHolderBusId(landingClaimCaseBean.getSourcePolicyHolderBusId());
		stagingClaimCaseBean.setMainAffiliateBusId(landingClaimCaseBean.getMainAffiliateBusId());
		stagingClaimCaseBean.setSourceMainAffiliateBusId(landingClaimCaseBean.getSourceMainAffiliateBusId());
		stagingClaimCaseBean.setInsuredPersonBusId(landingClaimCaseBean.getInsuredPersonBusId());
		stagingClaimCaseBean.setInsuredPersonType(landingClaimCaseBean.getInsuredPersonType());
		stagingClaimCaseBean.setSourceInsuredPersonBusId(landingClaimCaseBean.getSourceInsuredPersonBusId());
		stagingClaimCaseBean.setClaimManagerUserName(landingClaimCaseBean.getClaimManagerUserName());
		stagingClaimCaseBean.setSourceClaimManagerBusId(landingClaimCaseBean.getSourceClaimManagerBusId());
		stagingClaimCaseBean.setClaimMgtmTeamBusId(landingClaimCaseBean.getClaimMgtmTeamBusId());
		stagingClaimCaseBean.setSourceClaimMgmtTeamBusId(landingClaimCaseBean.getSourceClaimMgmtTeamBusId());
		
		// ClaimTypeDisability
		stagingClaimCaseBean.setAssessmentState(landingClaimCaseBean.getAssessmentState());
		stagingClaimCaseBean.setBenefitStartDate(landingClaimCaseBean.getBenefitStartDate());
		stagingClaimCaseBean.setBenefitEndDate(landingClaimCaseBean.getBenefitEndDate());
		stagingClaimCaseBean.setCancellationDate(landingClaimCaseBean.getCancellationDate());
		stagingClaimCaseBean.setCertifiedEndDate(landingClaimCaseBean.getCertifiedEndDate());
		stagingClaimCaseBean.setClaimCause(landingClaimCaseBean.getClaimCause());
		stagingClaimCaseBean.setDiagnosisDate(landingClaimCaseBean.getDiagnosisDate());
		stagingClaimCaseBean.setDisabilityDate(landingClaimCaseBean.getDisabilityDate());
		stagingClaimCaseBean.setHospitalStartDate(landingClaimCaseBean.getHospitalStartDate());
		stagingClaimCaseBean.setLastDayWorked(landingClaimCaseBean.getLastDayWorked());
		stagingClaimCaseBean.setReactivationDate(landingClaimCaseBean.getReactivationDate());
		stagingClaimCaseBean.setReturnToWorkDate(landingClaimCaseBean.getReturnToWorkDate());
		stagingClaimCaseBean.setSymptomsStartDate(landingClaimCaseBean.getSymptomsStartDate());
		stagingClaimCaseBean.setTerminationdate(landingClaimCaseBean.getTerminationdate());
		stagingClaimCaseBean.setTreatmentStartDate(landingClaimCaseBean.getTreatmentStartDate());
		
		// ClaimtypeAbsence_US_AbsenceGeneral
		stagingClaimCaseBean.setAbsenceReason(landingClaimCaseBean.getAbsenceReason());
		stagingClaimCaseBean.setAbsenceDurationType(landingClaimCaseBean.getAbsenceDurationType());
		stagingClaimCaseBean.setAbsenceClaimState(landingClaimCaseBean.getAbsenceClaimState());
		stagingClaimCaseBean.setClaimState(landingClaimCaseBean.getClaimState());
		stagingClaimCaseBean.setClaimType(landingClaimCaseBean.getClaimType());
		stagingClaimCaseBean.setClaimStateDetail(landingClaimCaseBean.getClaimStateDetail());
		stagingClaimCaseBean.setClaimDate(landingClaimCaseBean.getClaimDate());
		
		// ClaimtypeAbsence_US_AbsenceAdoption
		stagingClaimCaseBean.setAdoptionType(landingClaimCaseBean.getAdoptionType());
		stagingClaimCaseBean.setAdoptionDate(landingClaimCaseBean.getAdoptionDate());
		stagingClaimCaseBean.setIsAdoptionCourtCase(landingClaimCaseBean.getIsAdoptionCourtCase());
		stagingClaimCaseBean.setAdoptionCourtDate(landingClaimCaseBean.getAdoptionCourtDate());
		stagingClaimCaseBean.setAdoptionCourtHours(landingClaimCaseBean.getAdoptionCourtHours());
		
		// ClaimtypeAbsence_US_AbsenceBereavement
		stagingClaimCaseBean.setBereaverRelationship(landingClaimCaseBean.getBereaverRelationship());
		stagingClaimCaseBean.setBereaverRelationshipNotes(landingClaimCaseBean.getBereaverRelationshipNotes());
		stagingClaimCaseBean.setBereavementDeathDate(landingClaimCaseBean.getBereavementDeathDate());
		
		// ClaimtypeAbsence_US_AbsenceBonding
		stagingClaimCaseBean.setBondingParentType(landingClaimCaseBean.getBondingParentType());
		stagingClaimCaseBean.setBondingEstimatedDeliveryDate(landingClaimCaseBean.getBondingEstimatedDeliveryDate());
		stagingClaimCaseBean.setBondingNumDelivered(landingClaimCaseBean.getBondingNumDelivered());
		stagingClaimCaseBean.setIsBondingPartnerEmployerSame(landingClaimCaseBean.getIsBondingPartnerEmployerSame());
		stagingClaimCaseBean.setIsPartnerBonding(landingClaimCaseBean.getIsPartnerBonding());
		stagingClaimCaseBean.setBondingPartnerName(landingClaimCaseBean.getBondingPartnerName());
		stagingClaimCaseBean.setBondingPartnerStartDate(landingClaimCaseBean.getBondingPartnerStartDate());
		stagingClaimCaseBean.setBondingPartnerEndDate(landingClaimCaseBean.getBondingPartnerEndDate());
		stagingClaimCaseBean.setBondingLastDisabilityDate(landingClaimCaseBean.getBondingLastDisabilityDate());
		
		// ClaimtypeAbsence_US_AbsenceBoneMarrow
		stagingClaimCaseBean.setIsBoneMarrowMedicalApptCase(landingClaimCaseBean.getIsBoneMarrowMedicalApptCase());
		stagingClaimCaseBean.setBoneMarrowMedicalApptDate(landingClaimCaseBean.getBoneMarrowMedicalApptDate());
		stagingClaimCaseBean.setBoneMarrowMedicalApptHours(landingClaimCaseBean.getBoneMarrowMedicalApptHours());
		
		// ClaimtypeAbsence_US_AbsenceChildBirth
		stagingClaimCaseBean.setChildbirthDate(landingClaimCaseBean.getChildbirthDate());
		stagingClaimCaseBean.setChildbirthEstimatedDate(landingClaimCaseBean.getChildbirthEstimatedDate());
		stagingClaimCaseBean.setChildbirthNumDelivered(landingClaimCaseBean.getChildbirthNumDelivered());
		stagingClaimCaseBean.setChildbirthDeliveryType(landingClaimCaseBean.getChildbirthDeliveryType());
		stagingClaimCaseBean.setIsChildbirthBondingTimeCase(landingClaimCaseBean.getIsChildbirthBondingTimeCase());
		stagingClaimCaseBean.setIsChildbirthComplicationCase(landingClaimCaseBean.getIsChildbirthComplicationCase());
		stagingClaimCaseBean.setIsChildbirthPartnerBonding(landingClaimCaseBean.getIsChildbirthPartnerBonding());
		stagingClaimCaseBean.setIsChildbirthPartnerEmployerSame(landingClaimCaseBean.getIsChildbirthPartnerEmployerSame());
		stagingClaimCaseBean.setChildbirthPartnerName(landingClaimCaseBean.getChildbirthPartnerName());
		stagingClaimCaseBean.setChildbirthPartnerBondingStartDate(landingClaimCaseBean.getChildbirthPartnerBondingStartDate());
		stagingClaimCaseBean.setChildbirthPartnerBondingEndDate(landingClaimCaseBean.getChildbirthPartnerBondingEndDate());
		stagingClaimCaseBean.setChildbirthLastDisabilityDate(landingClaimCaseBean.getChildbirthLastDisabilityDate());
		stagingClaimCaseBean.setChildbirthTreatingDoctorName(landingClaimCaseBean.getChildbirthTreatingDoctorName());
		stagingClaimCaseBean.setMilitaryLeaveType(landingClaimCaseBean.getMilitaryLeaveType());
		stagingClaimCaseBean.setMilitaryMemberType(landingClaimCaseBean.getMilitaryMemberType());
		
		// ClaimtypeAbsence_US_AbsenceFamilyMember
		stagingClaimCaseBean.setFamilyMemberName(landingClaimCaseBean.getFamilyMemberName());
		stagingClaimCaseBean.setFamilyMemberRelationship(landingClaimCaseBean.getFamilyMemberRelationship());
		stagingClaimCaseBean.setFamilyMemberRelationshipNotes(landingClaimCaseBean.getFamilyMemberRelationshipNotes());
		stagingClaimCaseBean.setIsFamilyFollowUpApptCase(landingClaimCaseBean.getIsFamilyFollowUpApptCase());
		stagingClaimCaseBean.setIsFamilyHospitalCase(landingClaimCaseBean.getIsFamilyHospitalCase());
		stagingClaimCaseBean.setIsFamilyMemberADAQualified(landingClaimCaseBean.getIsFamilyMemberADAQualified());
		stagingClaimCaseBean.setIsFamilyMemberOver18(landingClaimCaseBean.getIsFamilyMemberOver18());
		stagingClaimCaseBean.setIsfamilyMemberConditionNonSerious(landingClaimCaseBean.getIsfamilyMemberConditionNonSerious());
		stagingClaimCaseBean.setFamilyChildBirthDate(landingClaimCaseBean.getFamilyChildBirthDate());
		stagingClaimCaseBean.setFamilyMemberHealthConditionNotes(landingClaimCaseBean.getFamilyMemberHealthConditionNotes());
		stagingClaimCaseBean.setFamilyIllnessStartDate(landingClaimCaseBean.getFamilyIllnessStartDate());
		stagingClaimCaseBean.setFamilyHospitalStartDate(landingClaimCaseBean.getFamilyHospitalStartDate());
		stagingClaimCaseBean.setFamilyHospitalEndDate(landingClaimCaseBean.getFamilyHospitalEndDate());
		stagingClaimCaseBean.setFamilyTreatmentStartDate(landingClaimCaseBean.getFamilyTreatmentStartDate());
		stagingClaimCaseBean.setFamilyFollowUpApptDate(landingClaimCaseBean.getFamilyFollowUpApptDate());
		stagingClaimCaseBean.setFamilyTreatingDoctorName(landingClaimCaseBean.getFamilyTreatingDoctorName());
		
		// ClaimtypeAbsence_US_AbsenceMilitaryLeave
		stagingClaimCaseBean.setMilitaryLeaveType(landingClaimCaseBean.getMilitaryLeaveType());
		stagingClaimCaseBean.setMilitaryMemberType(landingClaimCaseBean.getMilitaryMemberType());
		stagingClaimCaseBean.setMilitaryMemberRelationship(landingClaimCaseBean.getMilitaryMemberRelationship());
		stagingClaimCaseBean.setMilitaryMemberRelationshipNotes(landingClaimCaseBean.getMilitaryMemberRelationshipNotes());
		
		// ClaimtypeAbsence_US_AbsenceOrganDonation
		stagingClaimCaseBean.setOrganDonating(landingClaimCaseBean.getOrganDonating());
		stagingClaimCaseBean.setIsOrganDonorMedicalApptCase(landingClaimCaseBean.getIsOrganDonorMedicalApptCase());
		stagingClaimCaseBean.setOrganDonorMedicalApptDate(landingClaimCaseBean.getOrganDonorMedicalApptDate());
		stagingClaimCaseBean.setOrganDonorMedicalApptHours(landingClaimCaseBean.getOrganDonorMedicalApptHours());
		
		// ClaimtypeAbsence_US_AbsenceOwnIllness
		stagingClaimCaseBean.setOwnIllnessInjuryType(landingClaimCaseBean.getOwnIllnessInjuryType());
		stagingClaimCaseBean.setOwnIllnessHospitalCase(landingClaimCaseBean.isOwnIllnessHospitalCase());
		stagingClaimCaseBean.setOwnIllnessFollowUpApptCase(landingClaimCaseBean.isOwnIllnessFollowUpApptCase());
		stagingClaimCaseBean.setOwnIllnessWorkRelated(landingClaimCaseBean.isOwnIllnessWorkRelated());
		stagingClaimCaseBean.setOwnMentalIllnesCase(landingClaimCaseBean.isOwnMentalIllnesCase());
		stagingClaimCaseBean.setOwnIllnessStartDate(landingClaimCaseBean.getOwnIllnessStartDate());
		stagingClaimCaseBean.setOwnIllnessHospitalStartDate(landingClaimCaseBean.getOwnIllnessHospitalStartDate());
		stagingClaimCaseBean.setOwnIllnessHospitalEndDate(landingClaimCaseBean.getOwnIllnessHospitalEndDate());
		stagingClaimCaseBean.setOwnIllnessFollowUpApptDate(landingClaimCaseBean.getOwnIllnessFollowUpApptDate());
		stagingClaimCaseBean.setOwnIllnessTreatmentStartDate(landingClaimCaseBean.getOwnIllnessTreatmentStartDate());
		stagingClaimCaseBean.setOwnIllnessTreatingDoctorName(landingClaimCaseBean.getOwnIllnessTreatingDoctorName());
		
		// ClaimtypeAbsence_US_AbsencePersonalCrime
		stagingClaimCaseBean.setPersonalCrimeType(landingClaimCaseBean.getPersonalCrimeType());
		stagingClaimCaseBean.setPersonalCrimeTypeNotes(landingClaimCaseBean.getPersonalCrimeTypeNotes());
		stagingClaimCaseBean.setPersonalCrimeRelationship(landingClaimCaseBean.getPersonalCrimeRelationship());
		stagingClaimCaseBean.setPersonalCrimeRelationshipNotes(landingClaimCaseBean.getPersonalCrimeRelationshipNotes());
		stagingClaimCaseBean.setIsPersonalCrimeCounsellingCase(landingClaimCaseBean.getIsPersonalCrimeCounsellingCase());
		stagingClaimCaseBean.setIsPersonalCrimeCourtCase(landingClaimCaseBean.getIsPersonalCrimeCourtCase());
		stagingClaimCaseBean.setIsPersonalCrimeDoctorCase(landingClaimCaseBean.getIsPersonalCrimeDoctorCase());
		stagingClaimCaseBean.setIsPersonalCrimeHospitalCase(landingClaimCaseBean.getIsPersonalCrimeHospitalCase());
		stagingClaimCaseBean.setIsPersonalCrimeLegalAssistCase(landingClaimCaseBean.getIsPersonalCrimeLegalAssistCase());
		stagingClaimCaseBean.setIsPersonalCrimeRelocationCase(landingClaimCaseBean.getIsPersonalCrimeRelocationCase());
		stagingClaimCaseBean.setIsPersonalCrimeRestrainingOrderCase(landingClaimCaseBean.getIsPersonalCrimeRestrainingOrderCase());
		stagingClaimCaseBean.setIsPersonalCrimeVictimServicesCase(landingClaimCaseBean.getIsPersonalCrimeVictimServicesCase());
		stagingClaimCaseBean.setPersonalCrimeHospitalStartDate(landingClaimCaseBean.getPersonalCrimeHospitalStartDate());
		stagingClaimCaseBean.setPersonalCrimeHospitalEndDate(landingClaimCaseBean.getPersonalCrimeHospitalEndDate());
		stagingClaimCaseBean.setPersonalCrimeTreatingDoctorName(landingClaimCaseBean.getPersonalCrimeTreatingDoctorName());
		
		// ClaimtypeAbsence_US_AbsencePersonalLeave
		stagingClaimCaseBean.setIsFMLAQualifyingEvent(landingClaimCaseBean.getIsFMLAQualifyingEvent());
		
		// ClaimtypeAbsence_US_AbsenceSmallnecessities
		stagingClaimCaseBean.setSmallNecessitiesType(landingClaimCaseBean.getSmallNecessitiesType());
		
		// Control
		stagingClaimCaseBean.setSourceSystem(landingClaimCaseBean.getSourceSystem());
		stagingClaimCaseBean.setCreationTimeStamp(landingClaimCaseBean.getCreationTimeStamp());
		stagingClaimCaseBean.setLastUpdateTimeStamp(landingClaimCaseBean.getLastUpdateTimeStamp());
		stagingClaimCaseBean.setLastSyncTimestamp(new Date());
		stagingClaimCaseBean.setAction(landingClaimCaseBean.getAction());
		stagingClaimCaseBean.setActionBy(landingClaimCaseBean.getActionBy());
	}
}