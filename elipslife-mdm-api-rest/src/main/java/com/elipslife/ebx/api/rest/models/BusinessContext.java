package com.elipslife.ebx.api.rest.models;

import javax.json.bind.annotation.JsonbProperty;

public class BusinessContext {
	
	@JsonbProperty("UserId")
	public String userId;
	
	@JsonbProperty("SourceSystemId")
	public String sourceSystemId;
}