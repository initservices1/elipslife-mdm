package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;

import javax.json.bind.annotation.JsonbProperty;

public class Offer {
	
	@JsonbProperty("AcceptedDate")
	public String acceptedDate;
	
	@JsonbProperty("IsNonBinding")
	public Boolean isNonBinding;
	
	@JsonbProperty("OfferDate")
	public LocalDate offerDate;
	
	@JsonbProperty("OfferEndDate")
	public LocalDate offerEndDate;
	
	@JsonbProperty("OfferNumber")
	public Integer offerNumber;
	
	@JsonbProperty("OfferState")
	public Integer offerState;
	
	@JsonbProperty("OfferType")
	public Integer offerType;
}
