package com.elipslife.mdm.party.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.butos.ebx.procedure.DeleteRecords;
import com.elipslife.mdm.common.rules.WorkflowRules;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

import ch.butos.ebx.lib.legacy.wrapper.procedure.DeleteRecordsProcedure;

public class DeleteRecordsFromLandingPartyTable extends ScriptTask {

    @Override
    public void executeScript(ScriptTaskContext aContext) throws OperationException {

        HashMap<String, String> contextVariables = WorkflowRules.getContextVariables(aContext);
        String dataspace = contextVariables.get("tempDataspace");
        String dataset = contextVariables.get("dataset");
        String sourceSystem = contextVariables.get("sourceSystem");

        AdaptationHome dataspaceHome = aContext.getRepository().lookupHome(HomeKey.forBranchName(dataspace));
        Adaptation dataSet = dataspaceHome.findAdaptationOrNull(AdaptationName.forName(dataset));
        AdaptationTable aTable = dataSet.getTable(LandingPaths._LDG_Party.getPathInSchema());
        ArrayList<Adaptation> deleteRecords = new ArrayList<Adaptation>();
        RequestResult requestResult = null;
        if (sourceSystem != null && !sourceSystem.equalsIgnoreCase("null")) {

            requestResult = WorkflowRules.getRecordsByFieldValue(aTable, LandingPaths._LDG_Party._Control_SourceSystem, sourceSystem);
            for (Adaptation record;(record = requestResult.nextAdaptation()) != null;) {
                deleteRecords.add(record);
            }

        } else {
            requestResult = aTable.createRequestResult(null);
            for (Adaptation record;(record = requestResult.nextAdaptation()) != null;) {
                deleteRecords.add(record);
            }
        }

        if (!deleteRecords.isEmpty()) {
            
            DeleteRecords deleteRecordsProcedure = new DeleteRecords(deleteRecords);
            // DeleteRecordsProcedure deleteRecordsProcedure = new DeleteRecordsProcedure(deleteRecords, true, false);
            ProgrammaticService programmaticService = ProgrammaticService.createForSession(aContext.getSession(), dataspaceHome);
            ProcedureResult procedureResult = programmaticService.execute(deleteRecordsProcedure);
            if (procedureResult.hasFailed()) {
                //errorMessage.concat(procedureResult.getException().getMessage());
                throw new RuntimeException(procedureResult.getException());
            }
        }


    }

}
