package com.elipslife.mdm.contract.constants;

import com.orchestranetworks.service.ServiceKey;

public class ContractConstants {
	
	public static final ServiceKey USER_SERVICE_EXPORT_DATAMODEL_TO_EXCEL = ServiceKey.forModuleServiceName("elipslife-web","Export_data_model_to_EXCEL");;
	
	public static class DataSpace {
		public static final String PARTY_REF = "PartyReference";
		public static final String CONTRACT_REF = "ContractReference";
	}

	public static final class DataSet {
		public static final String CONTRACT_MASTER = "ContractMaster";
		public static final String PARTY_MASTER = "PartyMaster";
	}
	
	public static final class RecordOperations {
		public static final String CREATE = "C";
		public static final String UPDATE = "U";
		public static final String DELETE = "D";
	}
}