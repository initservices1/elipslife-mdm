package com.elipslife.mdm.contract;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.codec.binary.StringUtils;

import com.elipslife.ebx.domain.bean.convert.ContractConverter;
import com.elipslife.mdm.contract.constants.ContractConstants.RecordOperations;
import com.elipslife.mdm.contract.core.CoreExposureBean;
import com.elipslife.mdm.contract.core.CoreExposureReader;
import com.elipslife.mdm.contract.core.CoreExposureWriter;
import com.elipslife.mdm.contract.core.CoreExposureWriterSession;
import com.elipslife.mdm.contract.core.CoreInsuranceContractBean;
import com.elipslife.mdm.contract.core.CoreInsuranceContractReader;
import com.elipslife.mdm.contract.staging.StagingExposureBean;
import com.elipslife.mdm.contract.staging.StagingExposureWriter;
import com.elipslife.mdm.contract.staging.StagingExposureWriterSession;
import com.elipslife.mdm.path.ContractPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Session;

public class ExposureBuilder {
	
	/**
	 * 
	 * @param session
	 * @param stagingExposureBean
	 */
	public static void createOrUpdateCoreExposureByStagingExposure(Session session, StagingExposureBean stagingExposureBean) {
		
		StagingExposureWriter stagingExposureWriter = new StagingExposureWriterSession(session);
		CoreExposureWriter coreExposureWriter = new CoreExposureWriterSession(session);
		
		createOrUpdateCoreExposureByStagingExposure(stagingExposureWriter, coreExposureWriter, stagingExposureBean);
	}
	
	
	/**
	 * 
	 * @param stagingExposureWriter
	 * @param coreExposureWriter
	 * @param stagingExposureBean
	 */
	public static void createOrUpdateCoreExposureByStagingExposure(StagingExposureWriter stagingExposureWriter, CoreExposureWriter coreExposureWriter,
			StagingExposureBean stagingExposureBean) {
		
		CoreExposureReader coreExposureReader = (CoreExposureReader) coreExposureWriter.getTableReader();
		
		List<CoreExposureBean> coreExposureBeans = coreExposureReader.findBeansBySourceSystemAndExposureBusId(
				stagingExposureBean.getSourceSystem(), stagingExposureBean.getExposureBusId());
		
		if(coreExposureBeans.size() > 1) {
			throw new RuntimeException(String.format("More than one exposure found with source system %s and exposure business ID %s",
							stagingExposureBean.getSourceSystem(), stagingExposureBean.getExposureBusId()));
		}
		
		CoreExposureBean coreExposureBean = coreExposureBeans.isEmpty() ? null : coreExposureBeans.get(0);
		
		if(coreExposureBean != null) {
			
			// Update
			
			boolean updateCoreExposureBean = DateNullableComparator.getInstance().compare(stagingExposureBean.getLastUpdateTimestamp(), coreExposureBean.getLastUpdateTimestamp()) > 0;
			if(!updateCoreExposureBean) {
				stagingExposureBean.setExposureMaster(String.valueOf(coreExposureBean.getId()));
				stagingExposureWriter.updateBean(stagingExposureBean);
				return;
			}
			
			ContractConverter.fillCoreExposureBeanWithStagingExposureBean(stagingExposureBean, coreExposureBean);
			
			linkInsuranceContractReference(coreExposureBean, stagingExposureBean.getSourceContractBusId());
			
			coreExposureWriter.updateBean(coreExposureBean);
			stagingExposureBean.setExposureMaster(String.valueOf(coreExposureBean.getId()));
		}
		else {
			
			// Create
			
			coreExposureBean = ContractConverter.createCoreExposureBeanFromStagingExposureBean(stagingExposureBean);
			
			linkInsuranceContractReference(coreExposureBean, stagingExposureBean.getSourceContractBusId());
			
			Optional<CoreExposureBean> coreExposureBeanOptional  = coreExposureWriter.createBeanOrThrow(coreExposureBean);
			stagingExposureBean.setExposureMaster(String.valueOf(coreExposureBeanOptional.get().getId()));
		}
		stagingExposureWriter.updateBean(stagingExposureBean);
	}
	
	public static void linkInsuranceContractReference(CoreExposureBean coreExposureBean, String sourceContractBusId) {
		
		CoreInsuranceContractReader coreInsuranceContractReader = new CoreInsuranceContractReader();
		
		CoreInsuranceContractBean coreInsuranceContractBean = findInsuranceContractBySourceSystemAndSourceContractBusId(coreInsuranceContractReader, coreExposureBean.getSourceSystem(), sourceContractBusId);
		
		coreExposureBean.setInsuranceContractId(coreInsuranceContractBean != null ? coreInsuranceContractBean.getId().toString() : null);
	}
	
	/**
	 * Link Insurance Contract References
	 * 
	 * @param coreExposureBean
	 * @param sourceContractBusId
	 */
	public static void linkInsuranceContractReference(String dataspace, CoreExposureBean coreExposureBean, String sourceContractBusId) {
		
		CoreInsuranceContractReader coreInsuranceContractReader = new CoreInsuranceContractReader(dataspace);
		
		CoreInsuranceContractBean coreInsuranceContractBean = findInsuranceContractBySourceSystemAndSourceContractBusId(coreInsuranceContractReader, coreExposureBean.getSourceSystem(), sourceContractBusId);
		
		coreExposureBean.setInsuranceContractId(coreInsuranceContractBean != null ? coreInsuranceContractBean.getId().toString() : null);
	}
	
	public static HashMap<Path, Object> fillCoreExposureBean(CoreExposureBean coreExposureBean) {
		
		HashMap<Path, Object> coreExposureRecordMap = new HashMap<Path, Object>();
		coreExposureRecordMap.put(ContractPaths._Exposure._ExposureGeneral_ExposureBusId, coreExposureBean.getExposureBusId());
		coreExposureRecordMap.put(ContractPaths._Exposure._ExposureGeneral_Name, coreExposureBean.getName());
		coreExposureRecordMap.put(ContractPaths._Exposure._ExposureGeneral_ProductBusId, coreExposureBean.getProductBusId());
		coreExposureRecordMap.put(ContractPaths._Exposure._ExposureGeneral_ProductGroup, coreExposureBean.getProductGroup());
		coreExposureRecordMap.put(ContractPaths._Exposure._ExposureGeneral_ProductName, coreExposureBean.getProductName());
		coreExposureRecordMap.put(ContractPaths._Exposure._InsuranceContract_InsuranceContractId, coreExposureBean.getInsuranceContractId());
		coreExposureRecordMap.put(ContractPaths._Exposure._Control_SourceSystem, coreExposureBean.getSourceSystem());
		coreExposureRecordMap.put(ContractPaths._Exposure._Control_LastUpdateTimestamp, coreExposureBean.getLastUpdateTimestamp());
		coreExposureRecordMap.put(ContractPaths._Exposure._Control_LastSyncAction, coreExposureBean.getLastSyncAction());
		coreExposureRecordMap.put(ContractPaths._Exposure._Control_CreationTimestamp, coreExposureBean.getCreationTimestamp());
		
		if(StringUtils.equals(RecordOperations.CREATE, coreExposureBean.getLastSyncAction())) {
			coreExposureRecordMap.put(ContractPaths._Exposure._Control_CreatedBy, coreExposureBean.getCreatedBy());
		}
		if(Objects.equals(RecordOperations.UPDATE, coreExposureBean.getLastSyncAction())) {
			coreExposureRecordMap.put(ContractPaths._Exposure._Control_LastUpdatedBy, coreExposureBean.getLastUpdatedBy());
		}
		
		return coreExposureRecordMap;
	}
	
	/**
	 * Get Insurance Contract By Source System and Source Contract BusId
	 * 
	 * @param coreInsuranceContractReader
	 * @param sourceSystem
	 * @param sourceContractBusId
	 * @return
	 */
	public static CoreInsuranceContractBean findInsuranceContractBySourceSystemAndSourceContractBusId(CoreInsuranceContractReader coreInsuranceContractReader, String sourceSystem, String sourceContractBusId) {

		List<CoreInsuranceContractBean> coreInsuranceContractBeans = coreInsuranceContractReader.findBeansBySourceSystemAndSourceContractBusId(
				sourceSystem, sourceContractBusId);
		
		if(coreInsuranceContractBeans.size() > 1) {
			throw new RuntimeException(String.format("More than one insurance contract found with source system %s and source contract bus ID %s",
							sourceSystem, sourceContractBusId));
		}
		
		return coreInsuranceContractBeans.isEmpty() ? null : coreInsuranceContractBeans.get(0);
	}
}