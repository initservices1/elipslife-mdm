package com.elipslife.mdm.contract.staging;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class StagingInsuranceContractWriterProcedureContext extends StagingInsuranceContractWriter {

	private ProcedureContext procedureContext;
	
	public StagingInsuranceContractWriterProcedureContext(ProcedureContext procedureContext) {
		this(procedureContext, false);
	}
	
	public StagingInsuranceContractWriterProcedureContext(ProcedureContext procedureContext, boolean useDefaultValues) {
		super(useDefaultValues);
		this.procedureContext = procedureContext;
	}
	
	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}