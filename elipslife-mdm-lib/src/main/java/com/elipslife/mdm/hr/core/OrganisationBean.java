package com.elipslife.mdm.hr.core;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class OrganisationBean extends Entity<Integer>{
    private String OrganisationId;
    private String DataOwner;
    private String OrganisationBusId;
    private String OrganisationType;
    private String ParentId;
    private String ParentBusId;
    private String Name;
    private String ManagerId;
    private String ManagerBusId;
    private String BuildingId;
    private String BuildingBusId;
    private Boolean IsActive;
    private String OrganisationMaster;
    private String Control;
    
    // Control
    private String sourceSystem;
	private String createdBy;
	private Date creationTimeStamp;
	private String lastUpdatedBy;
	private Date lastUpdateTimeStamp;
	private String lastSyncAction;
	private Date lastSyncTimeStamp;

    public String getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(String OrganisationId) {
        this.OrganisationId = OrganisationId;
    }

    public String getDataOwner() {
        return DataOwner;
    }

    public void setDataOwner(String DataOwner) {
        this.DataOwner = DataOwner;
    }

    public String getOrganisationBusId() {
        return OrganisationBusId;
    }

    public void setOrganisationBusId(String OrganisationBusId) {
        this.OrganisationBusId = OrganisationBusId;
    }

    public String getOrganisationType() {
        return OrganisationType;
    }

    public void setOrganisationType(String OrganisationType) {
        this.OrganisationType = OrganisationType;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String ParentId) {
        this.ParentId = ParentId;
    }

    public String getParentBusId() {
        return ParentBusId;
    }

    public void setParentBusId(String ParentBusId) {
        this.ParentBusId = ParentBusId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getManagerId() {
        return ManagerId;
    }

    public void setManagerId(String ManagerId) {
        this.ManagerId = ManagerId;
    }

    public String getManagerBusId() {
        return ManagerBusId;
    }

    public void setManagerBusId(String ManagerBusId) {
        this.ManagerBusId = ManagerBusId;
    }

    public String getBuildingId() {
        return BuildingId;
    }

    public void setBuildingId(String BuildingId) {
        this.BuildingId = BuildingId;
    }

    public String getBuildingBusId() {
        return BuildingBusId;
    }

    public void setBuildingBusId(String BuildingBusId) {
        this.BuildingBusId = BuildingBusId;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    public String getControl() {
        return Control;
    }

    public void setControl(String Control) {
        this.Control = Control;
    }

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdateTimeStamp() {
		return lastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(Date lastUpdateTimeStamp) {
		this.lastUpdateTimeStamp = lastUpdateTimeStamp;
	}

	public String getLastSyncAction() {
		return lastSyncAction;
	}

	public void setLastSyncAction(String lastSyncAction) {
		this.lastSyncAction = lastSyncAction;
	}

	public Date getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}

	public void setLastSyncTimeStamp(Date lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}

	public String getOrganisationMaster() {
		return OrganisationMaster;
	}

	public void setOrganisationMaster(String organisationMaster) {
		OrganisationMaster = organisationMaster;
	}
    
	
    
}
