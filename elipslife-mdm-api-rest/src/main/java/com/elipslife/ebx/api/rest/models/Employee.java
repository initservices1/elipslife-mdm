package com.elipslife.ebx.api.rest.models;

import javax.json.bind.annotation.JsonbProperty;

public class Employee {
	
	@JsonbProperty("PartyBusId")
	public String partyBusId;
	
	@JsonbProperty("UserName")
	public String userName;
}