package com.elipslife.mdm.contract.core;

import java.util.List;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class CoreInsuranceContractReader extends AbstractTableReader<CoreInsuranceContractBean> {

	public CoreInsuranceContractReader() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public CoreInsuranceContractReader(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._InsuranceContract.getPathInSchema()));
	}
	
	public CoreInsuranceContractReader(AdaptationTable table) {
		super(table);
	}

	@Override
	public CoreInsuranceContractBean createBeanFromRecord(Adaptation record) {
		CoreInsuranceContractBean bean = new CoreInsuranceContractBean();
		
		bean.setId(record.get_int(ContractPaths._InsuranceContract._ContractIds_InsuranceContractId));
		bean.setDataOwner(record.getString(ContractPaths._InsuranceContract._ContractIds_DataOwner));
		bean.setInsuranceContractUUID(record.getString(ContractPaths._InsuranceContract._ContractIds_DataOwner));
		bean.setContractStageBusId(record.getString(ContractPaths._InsuranceContract._ContractIds_ContractStageBusId));
		bean.setContractNumber(record.getString(ContractPaths._InsuranceContract._ContractIds_ContractNumber));
		bean.setOfferNumber(record.getString(ContractPaths._InsuranceContract._ContractIds_OfferNumber));
		bean.setApplicationNumber(record.getString(ContractPaths._InsuranceContract._ContractIds_ApplicationNumber));
		bean.setContractBusId2(record.getString(ContractPaths._InsuranceContract._ContractIds_ContractBusId2));
		bean.setCustomerRefNumber(record.getString(ContractPaths._InsuranceContract._ContractIds_CustomerRefNumber));
		
		bean.setContractStage(record.getString(ContractPaths._InsuranceContract._ContractCore_ContractStage));
		bean.setContractState(record.getString(ContractPaths._InsuranceContract._ContractCore_ContractState));
		bean.setOfferState(record.getString(ContractPaths._InsuranceContract._ContractCore_OfferState));
		bean.setApplicationState(record.getString(ContractPaths._InsuranceContract._ContractCore_ApplicationState));
		bean.setContractClass(record.getString(ContractPaths._InsuranceContract._ContractCore_ContractClass));
		bean.setBusinessContractBeginDate(record.getDate(ContractPaths._InsuranceContract._ContractCore_BusinessContractBeginDate));
		bean.setContractExpiryDate(record.getDate(ContractPaths._InsuranceContract._ContractCore_ContractExpiryDate));
		bean.setContractBeginDate(record.getDate(ContractPaths._InsuranceContract._ContractCore_ContractBeginDate));
		bean.setContractEndDate(record.getDate(ContractPaths._InsuranceContract._ContractCore_ContractEndDate));
		bean.setBusinessScope(record.getString(ContractPaths._InsuranceContract._ContractCore_BusinessScope));
		bean.setContractName(record.getString(ContractPaths._InsuranceContract._ContractCore_ContractName));
		bean.setAlternativeContractName(record.getString(ContractPaths._InsuranceContract._ContractCore_AlternativeContractName));
		bean.setClientSegment(record.getString(ContractPaths._InsuranceContract._ContractCore_ClientSegment));
		bean.setDistributionAgreementBusId(record.getString(ContractPaths._InsuranceContract._ContractCore_DistributionAgreementBusId));
		bean.setIsActive((Boolean)record.get(ContractPaths._InsuranceContract._ContractCore_IsActive));
		bean.setIsProfitParticipation((Boolean)record.get(ContractPaths._InsuranceContract._ContractCore_IsProfitParticipation));
		bean.setLineOfBusiness(record.getString(ContractPaths._InsuranceContract._ContractCore_LineOfBusiness));
		bean.setOperatingCountry(record.getString(ContractPaths._InsuranceContract._ContractCore_OperatingCountry));
		
		bean.setSourcePolicyholderBusId(record.getString(ContractPaths._InsuranceContract._Parties_Policyholder_SourcePolicyholderBusId));
		bean.setPolicyholderBusId(record.getString(ContractPaths._InsuranceContract._Parties_Policyholder_PolicyholderBusId));
		bean.setPolicyholderId(record.getString(ContractPaths._InsuranceContract._Parties_Policyholder_PolicyholderId));
		
		bean.setSourceBrokerBusId(record.getString(ContractPaths._InsuranceContract._Parties_Broker_SourceBrokerBusId));
		bean.setBrokerBusId(record.getString(ContractPaths._InsuranceContract._Parties_Broker_BrokerBusId));
		bean.setBrokerId(record.getString(ContractPaths._InsuranceContract._Parties_Broker_BrokerId));
		
		bean.setSourceAttachedPartnerBusId(record.getString(ContractPaths._InsuranceContract._Parties_AttachedPartner_SourceAttachedPartnerBusId));
		bean.setAttachedPartnerBusId(record.getString(ContractPaths._InsuranceContract._Parties_AttachedPartner_AttachedPartnerBusId));
		bean.setAttachedPartnerId(record.getString(ContractPaths._InsuranceContract._Parties_AttachedPartner_AttachedPartnerId));
		
		bean.setSourceAdminResponsibleBusId(record.getString(ContractPaths._InsuranceContract._Parties_AdminResponsible_SourceAdminResponsibleBusId));
		bean.setAdminResponsibleUserName(record.getString(ContractPaths._InsuranceContract._Parties_AdminResponsible_AdminResponsibleUserName));
		bean.setAdminResponsibleBusId(record.getString(ContractPaths._InsuranceContract._Parties_AdminResponsible_AdminResponsibleBusId));
		bean.setAdminResponsibleId(record.getString(ContractPaths._InsuranceContract._Parties_AdminResponsible_AdminResponsibleId));
		
		bean.setSourceClaimResponsibleBusId(record.getString(ContractPaths._InsuranceContract._Parties_ClaimResponsible_SourceClaimResponsibleBusId));
		bean.setClaimResponsibleUserName(record.getString(ContractPaths._InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleUserName));
		bean.setClaimResponsibleBusId(record.getString(ContractPaths._InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleBusId));
		bean.setClaimResponsibleId(record.getString(ContractPaths._InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleId));
		
		bean.setSourceCaseMgmtResponsibleBusId(record.getString(ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_SourceCaseMgmtResponsibleBusId));
		bean.setCaseMgmtResponsibleUserName(record.getString(ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleUserName));
		bean.setCaseMgmtResponsibleBusId(record.getString(ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleBusId));
		bean.setCaseMgmtResponsibleId(record.getString(ContractPaths._InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleId));
		
		bean.setSourceSalesResponsibleBusId(record.getString(ContractPaths._InsuranceContract._Parties_SalesResponsible_SourceSalesResponsibleBusId));
		bean.setSalesResponsibleUserName(record.getString(ContractPaths._InsuranceContract._Parties_SalesResponsible_SalesResponsibleUserName));
		bean.setSalesResponsibleBusId(record.getString(ContractPaths._InsuranceContract._Parties_SalesResponsible_SalesResponsibleBusId));
		bean.setSalesResponsibleId(record.getString(ContractPaths._InsuranceContract._Parties_SalesResponsible_SalesResponsibleId));
		
		bean.setIsExpired((Boolean)record.get(ContractPaths._InsuranceContract._Control_IsExpired));
		bean.setSourceSystem(record.getString(ContractPaths._InsuranceContract._Control_SourceSystem));
		bean.setSourceContractBusId(record.getString(ContractPaths._InsuranceContract._Control_SourceContractBusId));
		bean.setCreatedBy(record.getString(ContractPaths._InsuranceContract._Control_CreatedBy));
		bean.setCreationTimestamp(record.getDate(ContractPaths._InsuranceContract._Control_CreationTimestamp));
		bean.setLastUpdatedBy(record.getString(ContractPaths._InsuranceContract._Control_LastUpdatedBy));
		bean.setLastUpdateTimestamp(record.getDate(ContractPaths._InsuranceContract._Control_LastUpdateTimestamp));
		bean.setLastSyncAction(record.getString(ContractPaths._InsuranceContract._Control_LastSyncAction));
		bean.setLastSyncTimestamp(record.getDate(ContractPaths._InsuranceContract._Control_LastSyncTimestamp));
		// bean.setValidFrom(record.getDate(ContractPaths._InsuranceContract._Control_ValidFrom));
		// bean.setValidTo(record.getDate(ContractPaths._InsuranceContract._Control_ValidTo));
		
		return bean;
	}

	@Override
	protected Path[] getPkPaths() {
		return new Path[] { ContractPaths._InsuranceContract._ContractIds_InsuranceContractId };
	}

	@Override
	protected Object[] retrievePks(CoreInsuranceContractBean bean) {
		return new Object[] { bean.getId() };
	}
	
	
	/**
	 * 
	 * @param sourceSystem
	 * @param sourceContractBusId
	 */
	public List<CoreInsuranceContractBean> findBeansBySourceSystemAndSourceContractBusId(String sourceSystem, String sourceContractBusId) {
		
		XPathPredicate predicate1 = new XPathPredicate(Operation.Equals, ContractPaths._InsuranceContract._Control_SourceSystem, sourceSystem);
		XPathPredicate predicate2 = new XPathPredicate(Operation.Equals, ContractPaths._InsuranceContract._Control_SourceContractBusId, sourceContractBusId);
		
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate1);
		group.addPredicate(predicate2);
		
		return super.readBeansFor(group.toString());
	}
	
	public List<CoreInsuranceContractBean> findBeansByContractNumber(String contractNumber){
		
		XPathPredicate predicate = new XPathPredicate(Operation.Equals, ContractPaths._InsuranceContract._ContractIds_ContractNumber, contractNumber);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate);
		
		return super.readBeansFor(group.toString());
	}
}