package com.elipslife.mdm.contract.core;

import java.util.List;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class CoreContractCoverReader extends AbstractTableReader<CoreContractCoverBean> {

	public CoreContractCoverReader() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public CoreContractCoverReader(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._ContractCover.getPathInSchema()));
	}
	
	public CoreContractCoverReader(AdaptationTable table) {
		super(table);
	}

	@Override
	public CoreContractCoverBean createBeanFromRecord(Adaptation record) {
		
		CoreContractCoverBean bean = new CoreContractCoverBean();
		
		bean.setExposureId(record.getString(ContractPaths._ContractCover._Exposure_ExposureId));
		bean.setExposureBusId(record.getString(ContractPaths._ContractCover._Exposure_ExposureBusId));
		
		bean.setId(record.get_int(ContractPaths._ContractCover._Cover_ContractCoverId));
		bean.setCoverBusId(record.getString(ContractPaths._ContractCover._Cover_CoverBusId));
		bean.setCoverGroupType(record.getString(ContractPaths._ContractCover._Cover_CoverGroupType));
		bean.setCoverGroupTypeName(record.getString(ContractPaths._ContractCover._Cover_CoverGroupTypeName));
		bean.setCoverName(record.getString(ContractPaths._ContractCover._Cover_CoverName));
		bean.setCoverTemplateBusId(record.getString(ContractPaths._ContractCover._Cover_CoverTemplateBusId));
		bean.setBeneficiaryType(record.getString(ContractPaths._ContractCover._Cover_BeneficiaryType));
		
		bean.setSourceSystem(record.getString(ContractPaths._ContractCover._Control_SourceSystem));
		bean.setCreatedBy(record.getString(ContractPaths._ContractCover._Control_CreatedBy));
		bean.setCreationTimestamp(record.getDate(ContractPaths._ContractCover._Control_CreationTimestamp));
		bean.setLastUpdatedBy(record.getString(ContractPaths._ContractCover._Control_LastUpdatedBy));
		bean.setLastUpdateTimestamp(record.getDate(ContractPaths._ContractCover._Control_LastUpdateTimestamp));
		bean.setLastSyncAction(record.getString(ContractPaths._ContractCover._Control_LastSyncAction));
		bean.setLastSyncTimestamp(record.getDate(ContractPaths._ContractCover._Control_LastSyncTimestamp));
		
		return bean;
	}

	@Override
	protected Path[] getPkPaths() {
		return new Path[] { ContractPaths._ContractCover._Cover_ContractCoverId };
	}

	@Override
	protected Object[] retrievePks(CoreContractCoverBean bean) {
		return new Object[] { bean.getId() };
	}
	
	
	/**
	 * 
	 * @param sourceSystem
	 * @param coverBusId
	 * @return
	 */
	public List<CoreContractCoverBean> findBeansBySourceSystemAndCoverBusId(String sourceSystem, String coverBusId) {
		
		XPathPredicate predicate1 = new XPathPredicate(Operation.Equals, ContractPaths._ContractCover._Control_SourceSystem, sourceSystem);
		XPathPredicate predicate2 = new XPathPredicate(Operation.Equals, ContractPaths._ContractCover._Cover_CoverBusId, coverBusId);
		
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate1);
		group.addPredicate(predicate2);
		
		return super.readBeansFor(group.toString());
	}
	
	public List<CoreContractCoverBean> findContractCoversByInsuranceContracts(String insuranceContractId) {
    	XPathPredicate insuranceContractPredicate = new XPathPredicate(XPathPredicate.Operation.Equals, ContractPaths._ContractCover._Exposure_ExposureId, insuranceContractId);
		return super.readBeansFor(insuranceContractPredicate.toString());
	}
}