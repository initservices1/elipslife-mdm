package com.elipslife.mdm.hr.staging;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class STG_OrganisationBean extends Entity<Integer>{
    private Integer OrganisationId;
    private String DataOwner;
    private String OrganisationBusId;
    private String OrganisationType;
    private String ParentBusId;
    private String Name;
    private String ManagerBusId;
    private String BuildingBusId;
    private Boolean IsActive;
    private String OrganisationMaster;
    private String STG_Control;
    
    // Control
    private String sourceSystem;
	private String createdBy;
	private Date creationTimeStamp;
	private String lastUpdatedBy;
	private Date lastUpdateTimeStamp;
	private Date lastSyncTimestamp;
	
    public Integer getOrganisationId() {
        return OrganisationId;
    }

    public void setOrganisationId(Integer OrganisationId) {
        this.OrganisationId = OrganisationId;
    }

    public String getDataOwner() {
        return DataOwner;
    }

    public void setDataOwner(String DataOwner) {
        this.DataOwner = DataOwner;
    }

    public String getOrganisationBusId() {
        return OrganisationBusId;
    }

    public void setOrganisationBusId(String OrganisationBusId) {
        this.OrganisationBusId = OrganisationBusId;
    }

    public String getOrganisationType() {
        return OrganisationType;
    }

    public void setOrganisationType(String OrganisationType) {
        this.OrganisationType = OrganisationType;
    }

    public String getParentBusId() {
        return ParentBusId;
    }

    public void setParentBusId(String ParentBusId) {
        this.ParentBusId = ParentBusId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getManagerBusId() {
        return ManagerBusId;
    }

    public void setManagerBusId(String ManagerBusId) {
        this.ManagerBusId = ManagerBusId;
    }

    public String getBuildingBusId() {
        return BuildingBusId;
    }

    public void setBuildingBusId(String BuildingBusId) {
        this.BuildingBusId = BuildingBusId;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    public String getSTG_Control() {
        return STG_Control;
    }

    public void setSTG_Control(String STG_Control) {
        this.STG_Control = STG_Control;
    }

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdateTimeStamp() {
		return lastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(Date lastUpdateTimeStamp) {
		this.lastUpdateTimeStamp = lastUpdateTimeStamp;
	}

	public String getOrganisationMaster() {
		return OrganisationMaster;
	}

	public void setOrganisationMaster(String organisationMaster) {
		OrganisationMaster = organisationMaster;
	}

	public Date getLastSyncTimestamp() {
		return lastSyncTimestamp;
	}

	public void setLastSyncTimestamp(Date lastSyncTimestamp) {
		this.lastSyncTimestamp = lastSyncTimestamp;
	}
	
}
