package com.elipslife.mdm.uw.staging;

import java.util.Date;

public class StagingUWApplicationBean {
    private Integer UWApplicationId;
    private String UWApplicationBusId;
    private String UWApplicationMaster;
    private String SourceSystem;
    private String DataOwner;
    private String OperatingCountry;
    private String PolicyholderId;
    private String PolicyholderBusId;
    private String PolicyholderName;
    private String MainAffiliateId;
    private String MainAffiliateBusId;
    private String MainAffiliateFirstName;
    private String MainAffiliateLastName;
    private Date MainAffiliateDateOfBirth;
    private String MainAffiliateSSN;
    private String MainAffiliateTown;
    private String MainAffiliatePostCode;
    private String MainAffiliateCountry;
    private Date SubmissionDate;
    private Boolean IsActive;
    private String DocSignId;
    private Date SFSignedDate;
    private String Control;
    private String CreatedBy;
    private Date CreationTimeStamp;
	private String LastUpdatedBy;
	private Date LastUpdateTimeStamp;
	private Date DeletionTimeStamp;
	private Date lastSyncTimeStamp;
	private String Action;

    public Integer getUWApplicationId() {
        return UWApplicationId;
    }

    public void setUWApplicationId(Integer UWApplicationId) {
        this.UWApplicationId = UWApplicationId;
    }

    public String getUWApplicationBusId() {
        return UWApplicationBusId;
    }

    public void setUWApplicationBusId(String UWApplicationBusId) {
        this.UWApplicationBusId = UWApplicationBusId;
    }
    
    

    public String getUWApplicationMaster() {
		return UWApplicationMaster;
	}

	public void setUWApplicationMaster(String uWApplicationMaster) {
		UWApplicationMaster = uWApplicationMaster;
	}

	public String getSourceSystem() {
        return SourceSystem;
    }

    public void setSourceSystem(String SourceSystem) {
        this.SourceSystem = SourceSystem;
    }

    public String getDataOwner() {
        return DataOwner;
    }

    public void setDataOwner(String DataOwner) {
        this.DataOwner = DataOwner;
    }

    public String getOperatingCountry() {
        return OperatingCountry;
    }

    public void setOperatingCountry(String OperatingCountry) {
        this.OperatingCountry = OperatingCountry;
    }

    public String getPolicyholderId() {
        return PolicyholderId;
    }

    public void setPolicyholderId(String PolicyholderId) {
        this.PolicyholderId = PolicyholderId;
    }

    public String getPolicyholderBusId() {
        return PolicyholderBusId;
    }

    public void setPolicyholderBusId(String PolicyholderBusId) {
        this.PolicyholderBusId = PolicyholderBusId;
    }

    public String getPolicyholderName() {
        return PolicyholderName;
    }

    public void setPolicyholderName(String PolicyholderName) {
        this.PolicyholderName = PolicyholderName;
    }

    public String getMainAffiliateId() {
        return MainAffiliateId;
    }

    public void setMainAffiliateId(String MainAffiliateId) {
        this.MainAffiliateId = MainAffiliateId;
    }

    public String getMainAffiliateBusId() {
        return MainAffiliateBusId;
    }

    public void setMainAffiliateBusId(String MainAffiliateBusId) {
        this.MainAffiliateBusId = MainAffiliateBusId;
    }

    public String getMainAffiliateFirstName() {
        return MainAffiliateFirstName;
    }

    public void setMainAffiliateFirstName(String MainAffiliateFirstName) {
        this.MainAffiliateFirstName = MainAffiliateFirstName;
    }

    public String getMainAffiliateLastName() {
        return MainAffiliateLastName;
    }

    public void setMainAffiliateLastName(String MainAffiliateLastName) {
        this.MainAffiliateLastName = MainAffiliateLastName;
    }

    public Date getMainAffiliateDateOfBirth() {
        return MainAffiliateDateOfBirth;
    }

    public void setMainAffiliateDateOfBirth(Date MainAffiliateDateOfBirth) {
        this.MainAffiliateDateOfBirth = MainAffiliateDateOfBirth;
    }

    public String getMainAffiliateSSN() {
        return MainAffiliateSSN;
    }

    public void setMainAffiliateSSN(String MainAffiliateSSN) {
        this.MainAffiliateSSN = MainAffiliateSSN;
    }

    public String getMainAffiliateTown() {
        return MainAffiliateTown;
    }

    public void setMainAffiliateTown(String MainAffiliateTown) {
        this.MainAffiliateTown = MainAffiliateTown;
    }

    public String getMainAffiliatePostCode() {
        return MainAffiliatePostCode;
    }

    public void setMainAffiliatePostCode(String MainAffiliatePostCode) {
        this.MainAffiliatePostCode = MainAffiliatePostCode;
    }

    public String getMainAffiliateCountry() {
        return MainAffiliateCountry;
    }

    public void setMainAffiliateCountry(String MainAffiliateCountry) {
        this.MainAffiliateCountry = MainAffiliateCountry;
    }

    public Date getSubmissionDate() {
        return SubmissionDate;
    }

    public void setSubmissionDate(Date SubmissionDate) {
        this.SubmissionDate = SubmissionDate;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    public String getDocSignId() {
        return DocSignId;
    }

    public void setDocSignId(String DocSignId) {
        this.DocSignId = DocSignId;
    }

    public Date getSFSignedDate() {
        return SFSignedDate;
    }

    public void setSFSignedDate(Date SFSignedDate) {
        this.SFSignedDate = SFSignedDate;
    }

    public String getControl() {
        return Control;
    }

    public void setControl(String Control) {
        this.Control = Control;
    }

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public Date getCreationTimeStamp() {
		return CreationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		CreationTimeStamp = creationTimeStamp;
	}

	public String getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	public Date getDeletionTimeStamp() {
		return DeletionTimeStamp;
	}

	public void setDeletionTimeStamp(Date deletionTimeStamp) {
		DeletionTimeStamp = deletionTimeStamp;
	}

	public Date getLastUpdateTimeStamp() {
		return LastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(Date lastUpdateTimeStamp) {
		LastUpdateTimeStamp = lastUpdateTimeStamp;
	}
	
	public Date getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}

	public void setLastSyncTimeStamp(Date lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}

	public String getAction() {
		return Action;
	}

	public void setAction(String action) {
		Action = action;
	}
    
}
