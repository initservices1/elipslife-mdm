package com.elipslife.mdm.contract.staging;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class StagingContractCoverWriterSession extends StagingContractCoverWriter {

	private Session session;
	
	public StagingContractCoverWriterSession(Session session) {
		this.session = session;
	}
	
	@Override
	protected void executeProcedure(Procedure procedure) {
		ProcedureResult procedureResult = ProgrammaticService.createForSession(this.session,this.dataSpace).execute(procedure);
		if(procedureResult.hasFailed()) {
			throw new RuntimeException(procedureResult.getException());
		}
	}
}