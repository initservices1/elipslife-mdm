package com.elipslife.mdm.party.scheduler;

import com.elipslife.ebx.data.access.CoreAddressBuilder;
import com.elipslife.ebx.data.access.CoreAddressWriterSession;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;

public class MarkExpiredAddressesTask extends ScheduledTask {

	@Override
	public void execute(ScheduledExecutionContext scheduledExecutionContext) {
		
		CoreAddressBuilder.markExpiredAddresses(new CoreAddressWriterSession(scheduledExecutionContext.getSession(), true));
	}
}