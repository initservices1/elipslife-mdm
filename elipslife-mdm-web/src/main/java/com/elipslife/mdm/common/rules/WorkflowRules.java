package com.elipslife.mdm.common.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.butos.ebx.procedure.DeleteRecords;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

import ch.butos.ebx.lib.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.xpath.filter.XPathPredicateGroup;

public class WorkflowRules {

    public static HashMap<String, String> getContextVariables(com.orchestranetworks.workflow.DataContextReadOnly dataContext) {

        HashMap<String, String> hashMap = new HashMap<String, String>();
        Iterator<String> variableNames = dataContext.getVariableNames();
        while (variableNames.hasNext()) {
            String variableName = variableNames.next();
            String variableValue = dataContext.getVariableString(variableName);
            hashMap.put(variableName, variableValue);
        }
        return hashMap;
    }

    public static RequestResult getRecordsByFieldValue(AdaptationTable aTable, Path aTablePath, String value) {

        XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
        XPathPredicate predicateSourceSystem = new XPathPredicate(Operation.Equals, aTablePath, value);
        group.addPredicate(predicateSourceSystem);

        RequestResult requestResult = aTable.createRequestResult(group.toString());
        return requestResult;
    }

    public static RequestResult getStagingPartiesByCorePartyId(Adaptation dataset, String fieldValue) {

        AdaptationTable stagingPartyTable = dataset.getTable(PartyPaths._STG_Party.getPathInSchema());
        Path aCorePartyIdPath = PartyPaths._STG_Party._Party_CorePartyId;

        /*
         * XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And); XPathPredicate predicateSourceSystem = new
         * XPathPredicate(Operation.Equals, aCorePartyIdPath, fieldValue); group.addPredicate(predicateSourceSystem);
         */

        RequestResult requestResult = getRecordsByFieldValue(stagingPartyTable, aCorePartyIdPath, fieldValue);
        return requestResult;
    }

    public static RequestResult getStagingAddressesByCoreAddressId(Adaptation dataset, String fieldValue) {

        AdaptationTable stagingPartyTable = dataset.getTable(PartyPaths._STG_Party.getPathInSchema());
        Path aCorePartyIdPath = PartyPaths._STG_Party._Party_CorePartyId;

        /*
         * XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And); XPathPredicate predicateSourceSystem = new
         * XPathPredicate(Operation.Equals, aCorePartyIdPath, fieldValue); group.addPredicate(predicateSourceSystem);
         */

        RequestResult requestResult = getRecordsByFieldValue(stagingPartyTable, aCorePartyIdPath, fieldValue);
        return requestResult;
    }
    
    public static RequestResult getStagingBankAccountsByCoreBankAccountId(Adaptation dataset, String fieldValue) {

        AdaptationTable stagingPartyTable = dataset.getTable(PartyPaths._STG_Party.getPathInSchema());
        Path aCorePartyIdPath = PartyPaths._STG_Party._Party_CorePartyId;

        /*
         * XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And); XPathPredicate predicateSourceSystem = new
         * XPathPredicate(Operation.Equals, aCorePartyIdPath, fieldValue); group.addPredicate(predicateSourceSystem);
         */

        RequestResult requestResult = getRecordsByFieldValue(stagingPartyTable, aCorePartyIdPath, fieldValue);
        return requestResult;
    }
    
    public static void deleteRecords(AdaptationHome dataspace, Session session, ArrayList<Adaptation> recordValuesBeanList) throws OperationException {

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
		DeleteRecords deleteRecordsProcedure = new DeleteRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(deleteRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}
}
