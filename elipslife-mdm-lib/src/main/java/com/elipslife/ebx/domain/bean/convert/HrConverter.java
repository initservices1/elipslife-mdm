package com.elipslife.ebx.domain.bean.convert;

import java.util.Date;

import com.elipslife.mdm.hr.core.BuildingBean;
import com.elipslife.mdm.hr.core.EmployeeBean;
import com.elipslife.mdm.hr.core.OrganisationBean;
import com.elipslife.mdm.hr.landing.LandingBuildingBean;
import com.elipslife.mdm.hr.landing.LandingEmployeeBean;
import com.elipslife.mdm.hr.landing.LandingOrganisationBean;
import com.elipslife.mdm.hr.staging.STG_BuildingBean;
import com.elipslife.mdm.hr.staging.STG_EmployeeBean;
import com.elipslife.mdm.hr.staging.STG_OrganisationBean;

public class HrConverter {

	public static BuildingBean fillBuildingBeanWithStagingBuildingBean(STG_BuildingBean stgBuildingBean) {

		BuildingBean buildingBean = new BuildingBean();
		fillBuildingBeanWithStagingBuildingBean(buildingBean, stgBuildingBean);
		return buildingBean;
	}

	public static void fillBuildingBeanWithStagingBuildingBean(BuildingBean updateBuildingBean,
			STG_BuildingBean stgBuildingBean) {

		updateBuildingBean.setDataOwner(stgBuildingBean.getDataOwner());
		updateBuildingBean.setBuildingBusId(stgBuildingBean.getBuildingBusId());
		updateBuildingBean.setName(stgBuildingBean.getName());
		updateBuildingBean.setStreet(stgBuildingBean.getStreet());
		updateBuildingBean.setStreet2(stgBuildingBean.getStreet2());
		updateBuildingBean.setPostCode(stgBuildingBean.getPostCode());
		updateBuildingBean.setPOBox(stgBuildingBean.getPOBox());
		updateBuildingBean.setPOBoxPostCode(stgBuildingBean.getPOBoxPostCode());
		updateBuildingBean.setPOBoxTown(stgBuildingBean.getPOBoxTown());
		updateBuildingBean.setTown(stgBuildingBean.getTown());
		updateBuildingBean.setDistrict(stgBuildingBean.getDistrict());
		updateBuildingBean.setStateProvince(stgBuildingBean.getStateProvince());
		updateBuildingBean.setCountry(stgBuildingBean.getCountry());
		updateBuildingBean.setIsActive(stgBuildingBean.getIsActive());
		updateBuildingBean.setSourceSystem(stgBuildingBean.getSourceSystem());
		updateBuildingBean.setCreatedBy(stgBuildingBean.getCreatedBy());
		updateBuildingBean.setCreationTimeStamp(stgBuildingBean.getCreationTimeStamp());
		updateBuildingBean.setLastUpdatedBy(stgBuildingBean.getLastUpdatedBy());
		updateBuildingBean.setLastUpdateTimeStamp(stgBuildingBean.getLastUpdateTimeStamp());
	}

	public static OrganisationBean fillOrganisationBeanWithStagingOrganisationBean(
			STG_OrganisationBean stgOrganisationBean) {

		OrganisationBean organisationBean = new OrganisationBean();
		fillOrganisationBeanWithStagingOrganisationBean(organisationBean, stgOrganisationBean);
		return organisationBean;
	}

	public static void fillOrganisationBeanWithStagingOrganisationBean(OrganisationBean updateOrganisationBean,
			STG_OrganisationBean stgOrganisationBean) {

		updateOrganisationBean.setDataOwner(stgOrganisationBean.getDataOwner());
		updateOrganisationBean.setOrganisationBusId(stgOrganisationBean.getOrganisationBusId());
		updateOrganisationBean.setOrganisationType(stgOrganisationBean.getOrganisationType());
		updateOrganisationBean.setParentBusId(stgOrganisationBean.getParentBusId());
		updateOrganisationBean.setName(stgOrganisationBean.getName());
		updateOrganisationBean.setManagerBusId(stgOrganisationBean.getManagerBusId());
		updateOrganisationBean.setBuildingBusId(stgOrganisationBean.getBuildingBusId());
		updateOrganisationBean.setIsActive(stgOrganisationBean.getIsActive());
		updateOrganisationBean.setSourceSystem(stgOrganisationBean.getSourceSystem());
		updateOrganisationBean.setCreatedBy(stgOrganisationBean.getCreatedBy());
		updateOrganisationBean.setCreationTimeStamp(stgOrganisationBean.getCreationTimeStamp());
		updateOrganisationBean.setLastUpdatedBy(stgOrganisationBean.getLastUpdatedBy());
		updateOrganisationBean.setLastUpdateTimeStamp(stgOrganisationBean.getLastUpdateTimeStamp());
	}

	public static EmployeeBean fillEmployeeBeanWithStagingEmployeeBean(STG_EmployeeBean stgEmployeeBean) {

		EmployeeBean employeeBean = new EmployeeBean();
		fillEmployeeBeanWithStagingEmployeeBean(employeeBean, stgEmployeeBean);
		return employeeBean;
	}

	public static void fillEmployeeBeanWithStagingEmployeeBean(EmployeeBean updateEmployeeBean,
			STG_EmployeeBean stgEmployeeBean) {

		updateEmployeeBean.setDataOwner(stgEmployeeBean.getDataOwner());
		updateEmployeeBean.setEmployeeBusId(stgEmployeeBean.getEmployeeBusId());
		updateEmployeeBean.setUserName(stgEmployeeBean.getUserName());
		updateEmployeeBean.setFirstName(stgEmployeeBean.getFirstName());
		updateEmployeeBean.setPreferredFirstName(stgEmployeeBean.getPreferredFirstName());
		updateEmployeeBean.setMiddleName(stgEmployeeBean.getMiddleName());
		updateEmployeeBean.setLastName(stgEmployeeBean.getLastName());
		updateEmployeeBean.setGender(stgEmployeeBean.getGender());
		updateEmployeeBean.setSalutation(stgEmployeeBean.getSalutation());
		updateEmployeeBean.setLanguage(stgEmployeeBean.getLanguage());
		updateEmployeeBean.setEmail(stgEmployeeBean.getEmail());
		updateEmployeeBean.setTelephone(stgEmployeeBean.getTelephone());
		updateEmployeeBean.setMobile(stgEmployeeBean.getMobile());
		updateEmployeeBean.setFax(stgEmployeeBean.getFax());
		updateEmployeeBean.setIsActive(stgEmployeeBean.getIsActive());
		updateEmployeeBean.setIsSystemEnabled(stgEmployeeBean.getIsSystemEnabled());
		updateEmployeeBean.setEmploymentCategory(stgEmployeeBean.getEmploymentCategory());
		updateEmployeeBean.setManagementLevel(stgEmployeeBean.getManagementLevel());
		updateEmployeeBean.setSignOffRole(stgEmployeeBean.getSignOffRole());
		updateEmployeeBean.setFunctionalTitle(stgEmployeeBean.getFunctionalTitle());
		updateEmployeeBean.setJobProfileName(stgEmployeeBean.getJobProfileName());
		updateEmployeeBean.setPositionName(stgEmployeeBean.getPositionName());
		updateEmployeeBean.setIsLineManager(stgEmployeeBean.getIsLineManager());
		updateEmployeeBean.setOrganisationBusId(stgEmployeeBean.getOrganisationBusId());
		updateEmployeeBean.setLineManagerBusId(stgEmployeeBean.getLineManagerBusId());
		updateEmployeeBean.setDottedLineManagerBusId(stgEmployeeBean.getDottedLineManagerBusId());
		updateEmployeeBean.setBuildingBusId(stgEmployeeBean.getBuildingBusId());
		
		//Employee Private Address
		updateEmployeeBean.setPrivateStreet(stgEmployeeBean.getPrivateStreet());
		updateEmployeeBean.setPrivatePostCode(stgEmployeeBean.getPrivatePostCode());
		updateEmployeeBean.setPrivateTown(stgEmployeeBean.getPrivateTown());
		updateEmployeeBean.setPrivateStateProvince(stgEmployeeBean.getPrivateStateProvince());
		updateEmployeeBean.setPrivateCountryCode(stgEmployeeBean.getPrivateCountryCode());
		
		updateEmployeeBean.setEmployerBusId(stgEmployeeBean.getEmployerBusId());
		updateEmployeeBean.setSourceSystem(stgEmployeeBean.getSourceSystem());
		updateEmployeeBean.setCreatedBy(stgEmployeeBean.getCreatedBy());
		updateEmployeeBean.setCreationTimeStamp(stgEmployeeBean.getCreationTimeStamp());
		updateEmployeeBean.setLastUpdatedBy(stgEmployeeBean.getLastUpdatedBy());
		updateEmployeeBean.setLastUpdateTimeStamp(stgEmployeeBean.getLastUpdateTimeStamp());

	}

	public static STG_BuildingBean createStagingBuildingBeanFromLandingBuildingBean(
			LandingBuildingBean landingBuildingBean) {

		STG_BuildingBean buildingBean = new STG_BuildingBean();
		fillStagingBuildingBeanWithLandingBuildingBean(buildingBean, landingBuildingBean);
		return buildingBean;

	}

	private static void fillStagingBuildingBeanWithLandingBuildingBean(STG_BuildingBean stgBuildingBean,
			LandingBuildingBean landingBuildingBean) {
		if (landingBuildingBean.getDataOwner() != null) {
			stgBuildingBean.setDataOwner(landingBuildingBean.getDataOwner());
		} else {
			stgBuildingBean.setDataOwner("0");
		}
		stgBuildingBean.setBuildingBusId(landingBuildingBean.getBuildingBusId());
		stgBuildingBean.setName(landingBuildingBean.getName());
		stgBuildingBean.setStreet(landingBuildingBean.getStreet());
		stgBuildingBean.setStreet2(landingBuildingBean.getStreet2());
		stgBuildingBean.setPostCode(landingBuildingBean.getPostCode());
		stgBuildingBean.setPOBox(landingBuildingBean.getPOBox());
		stgBuildingBean.setPOBoxPostCode(landingBuildingBean.getPOBoxPostCode());
		stgBuildingBean.setPOBoxTown(landingBuildingBean.getPOBoxTown());
		stgBuildingBean.setTown(landingBuildingBean.getTown());
		stgBuildingBean.setDistrict(landingBuildingBean.getDistrict());
		stgBuildingBean.setStateProvince(landingBuildingBean.getStateProvince());
		stgBuildingBean.setCountry(landingBuildingBean.getCountry());
		if (landingBuildingBean.getIsActive() != null) {
			stgBuildingBean.setIsActive(landingBuildingBean.getIsActive());
		} else {
			stgBuildingBean.setIsActive(true);
		}
		stgBuildingBean.setSourceSystem(landingBuildingBean.getSourceSystem());
		stgBuildingBean.setCreatedBy(landingBuildingBean.getCreatedBy());
		stgBuildingBean.setCreationTimeStamp(landingBuildingBean.getCreationTimeStamp());
		stgBuildingBean.setLastUpdatedBy(landingBuildingBean.getLastUpdatedBy());
		stgBuildingBean.setLastUpdateTimeStamp(landingBuildingBean.getLastUpdateTimeStamp());
		stgBuildingBean.setLastSyncTimestamp(new Date());
	}

	public static STG_OrganisationBean createStagingOrganisationBeanFromLandingOrganisationBean(
			LandingOrganisationBean landingOrganisationBean) {
		STG_OrganisationBean organisationBean = new STG_OrganisationBean();
		fillStagingOrganisationBeanWithLandingOrganisationBean(organisationBean, landingOrganisationBean);
		return organisationBean;
	}

	private static void fillStagingOrganisationBeanWithLandingOrganisationBean(STG_OrganisationBean organisationBean,
			LandingOrganisationBean landingOrganisationBean) {

		if (landingOrganisationBean.getDataOwner() != null) {
			organisationBean.setDataOwner(landingOrganisationBean.getDataOwner());
		} else {
			organisationBean.setDataOwner("0");
		}
		organisationBean.setOrganisationBusId(landingOrganisationBean.getOrganisationBusId());
		organisationBean.setOrganisationType(landingOrganisationBean.getOrganisationType());
		organisationBean.setParentBusId(landingOrganisationBean.getParentBusId());
		organisationBean.setName(landingOrganisationBean.getName());
		organisationBean.setManagerBusId(landingOrganisationBean.getManagerBusId());
		organisationBean.setBuildingBusId(landingOrganisationBean.getBuildingBusId());
		if (landingOrganisationBean.getIsActive() != null) {
			organisationBean.setIsActive(landingOrganisationBean.getIsActive());
		} else {
			organisationBean.setIsActive(true);
		}

		// organisationBean.setOrganisationMaster(landingOrganisationBean.getOrganisationMaster());
		organisationBean.setSourceSystem(landingOrganisationBean.getSourceSystem());
		organisationBean.setCreatedBy(landingOrganisationBean.getCreatedBy());
		organisationBean.setCreationTimeStamp(landingOrganisationBean.getCreationTimeStamp());
		organisationBean.setLastUpdatedBy(landingOrganisationBean.getLastUpdatedBy());
		organisationBean.setLastUpdateTimeStamp(landingOrganisationBean.getLastUpdateTimeStamp());
		organisationBean.setLastSyncTimestamp(new Date());

	}

	public static STG_EmployeeBean createStagingEmployeeBeanFromLandingEmployeeBean(
			LandingEmployeeBean landingEmployeeBean) {
		STG_EmployeeBean employeeBean = new STG_EmployeeBean();
		fillStagingEmployeeBeanWithLandingEmployeeBean(employeeBean, landingEmployeeBean);
		return employeeBean;

	}

	private static void fillStagingEmployeeBeanWithLandingEmployeeBean(STG_EmployeeBean employeeBean,
			LandingEmployeeBean landingEmployeeBean) {

		if (landingEmployeeBean.getDataOwner() != null) {
			employeeBean.setDataOwner(landingEmployeeBean.getDataOwner());
		} else {
			employeeBean.setDataOwner("0");
		}

		employeeBean.setEmployeeBusId(landingEmployeeBean.getEmployeeBusId());
		employeeBean.setUserName(landingEmployeeBean.getUserName());
		employeeBean.setFirstName(landingEmployeeBean.getFirstName());
		employeeBean.setPreferredFirstName(landingEmployeeBean.getPreferredFirstName());
		employeeBean.setMiddleName(landingEmployeeBean.getMiddleName());
		employeeBean.setLastName(landingEmployeeBean.getLastName());
		// employeeBean.setFullName(landingEmployeeBean.getFullName());
		employeeBean.setGender(landingEmployeeBean.getGender());
		employeeBean.setSalutation(landingEmployeeBean.getSalutation());
		employeeBean.setLanguage(landingEmployeeBean.getLanguage());
		employeeBean.setEmail(landingEmployeeBean.getEmail());
		employeeBean.setTelephone(landingEmployeeBean.getTelephone());
		employeeBean.setMobile(landingEmployeeBean.getMobile());
		employeeBean.setFax(landingEmployeeBean.getFax());
		if (landingEmployeeBean.getIsActive() != null) {
			employeeBean.setIsActive(landingEmployeeBean.getIsActive());
		} else {
			employeeBean.setIsActive(true);
		}

		if (landingEmployeeBean.getIsSystemEnabled() != null) {
			employeeBean.setIsSystemEnabled(landingEmployeeBean.getIsSystemEnabled());
		} else {
			employeeBean.setIsSystemEnabled(true);
		}
		employeeBean.setEmploymentCategory(landingEmployeeBean.getEmploymentCategory());
		employeeBean.setManagementLevel(landingEmployeeBean.getManagementLevel());
		employeeBean.setSignOffRole(landingEmployeeBean.getSignOffRole());
		employeeBean.setFunctionalTitle(landingEmployeeBean.getFunctionalTitle());
		employeeBean.setJobProfileName(landingEmployeeBean.getJobProfileName());
		employeeBean.setPositionName(landingEmployeeBean.getPositionName());
		employeeBean.setIsLineManager(landingEmployeeBean.getIsLineManager());
		employeeBean.setOrganisationBusId(landingEmployeeBean.getOrganisationBusId());
		employeeBean.setLineManagerBusId(landingEmployeeBean.getLineManagerBusId());
		employeeBean.setDottedLineManagerBusId(landingEmployeeBean.getDottedLineManagerBusId());
		employeeBean.setBuildingBusId(landingEmployeeBean.getBuildingBusId());
		
		//Employee Private Address
		employeeBean.setPrivateStreet(landingEmployeeBean.getPrivateStreet());
		employeeBean.setPrivatePostCode(landingEmployeeBean.getPrivatePostCode());
		employeeBean.setPrivateTown(landingEmployeeBean.getPrivateTown());
		employeeBean.setPrivateStateProvince(landingEmployeeBean.getPrivateStateProvince());
		employeeBean.setPrivateCountryCode(landingEmployeeBean.getPrivateCountryCode());
		
		employeeBean.setEmployerBusId(landingEmployeeBean.getEmployerBusId());
		employeeBean.setSourceSystem(landingEmployeeBean.getSourceSystem());
		employeeBean.setCreatedBy(landingEmployeeBean.getCreatedBy());
		employeeBean.setCreationTimeStamp(landingEmployeeBean.getCreationTimeStamp());
		employeeBean.setLastUpdatedBy(landingEmployeeBean.getLastUpdatedBy());
		employeeBean.setLastUpdateTimeStamp(landingEmployeeBean.getLastUpdateTimeStamp());
		employeeBean.setLastSyncTimestamp(new Date());
	}

}
