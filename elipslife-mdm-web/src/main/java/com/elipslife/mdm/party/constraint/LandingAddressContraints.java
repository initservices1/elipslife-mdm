package com.elipslife.mdm.party.constraint;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.butos.ebx.smart.validation.SmartRecordAndTableLevelValidationCheck;
import com.elipslife.ebx.data.access.LandingAddressReader;
import com.elipslife.ebx.data.access.LandingPartyReader;
import com.elipslife.ebx.data.access.StagingPartyReader;
import com.elipslife.ebx.domain.bean.LandingPartyBean;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

/**
 * 1. Check if any validation rules found in current record
 * 2. Check if associated party exits with source system and source party bus id in staging
 * 
 * @author VenkateswarluJetti
 *
 */
public class LandingAddressContraints extends SmartRecordAndTableLevelValidationCheck {

	@Override
	protected void executeBusinessValidations(ValueContext aValueContext, Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {
		
		// check if any validation rules found in current record
		// checkIfAnyValidationError(aValueContext, validationInfoMap, true);
		
		// Check if associated party exits with source system and source party bus id in staging
		checkLinkedPartyInLandingOrStaging(aValueContext, validationInfoMap, true);

		
	}
	
	/**
	 * Check if associated party exits with source system and source party bus id in staging or not
	 * 
	 * @param aValueContext
	 * @param validationInfoMap
	 * @param isFstRec
	 */
	public void checkLinkedPartyInLandingOrStaging(ValueContext aValueContext, Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {
		
		String sourceSystem = (String) aValueContext.getValue(LandingPaths._LDG_Address._Control_SourceSystem);
		String sourcePartyBusId = (String) aValueContext.getValue(LandingPaths._LDG_Address._Control_SourcePartyBusId);
		
		LandingPartyReader landingPartyReader = new LandingPartyReader();
        List<LandingPartyBean> landingPartyBeans = landingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId);
        if (landingPartyBeans.isEmpty()) {
        	
        	// check linked party in staging as well
        	StagingPartyReader stagingPartyReader = new StagingPartyReader();
            List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId);
            if(stagingPartyBeans.isEmpty()) {
            	
            	String message = String.format("Landing Party or Staging party with %s %s and %s %s not found", LandingPaths._LDG_Address._Control_SourceSystem.getLastStep().format(), sourceSystem,
        				LandingPaths._LDG_Address._Control_SourcePartyBusId.getLastStep().format(), sourcePartyBusId);
        		
        		super.insertUserMsg(validationInfoMap, aValueContext.getNode(LandingPaths._LDG_Address._Control_SourceSystem), UserMessage.createError(message));
        	
        		super.insertUserMsg(validationInfoMap, aValueContext.getNode(LandingPaths._LDG_Address._Control_SourcePartyBusId), UserMessage.createError(message));
            }   
        } else {
        	LandingPartyBean landingPartyBean = landingPartyBeans.get(0);
        	// check if landing party has any validation errors or not
        	boolean isAnyValidationErrorsFound = landingPartyReader.hasValidationErrors(new Object[] { landingPartyBean.getId() });
			if(isAnyValidationErrorsFound) {
				// add the error message if there is any errors.
				UserMessage userMessage = UserMessage.createError("Linked landing party has validation errors");
				this.insertUserMsg(validationInfoMap, aValueContext.getNode(LandingPaths._LDG_Address._Address_AddressId), userMessage);
			}
        }
	}
	
	/**
	 * check if validation errors found in current record or not
	 * 
	 * @param aValueContext
	 * @param validationInfoMap
	 * @param isFstRec
	 */
	public void checkIfAnyValidationError(ValueContext aValueContext, Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {
		
		Integer landingAddressId = (Integer) aValueContext.getValue(LandingPaths._LDG_Address._Address_AddressId);
		
		// check it only for created records
		if(landingAddressId != null) {
			UserMessage userMessage = null;
			LandingAddressReader landingAddressReader = new LandingAddressReader();
			
			//check if we have any validation errors found in current selected record
			boolean isAnyValidationErrorsFound = landingAddressReader.hasValidationErrors(new Object[] { landingAddressId.intValue() });
			if(isAnyValidationErrorsFound) {
				// add the error message if there is any errors.
				userMessage = UserMessage.createError("Please correct the below validations to process this record to staging");
				this.insertUserMsg(validationInfoMap, aValueContext.getNode(LandingPaths._LDG_Address._Address_AddressId), userMessage);
			}
		}
	}
	
	@Override
	public void setup(ConstraintContextOnTable aContext) {
	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		return null;
	}
}