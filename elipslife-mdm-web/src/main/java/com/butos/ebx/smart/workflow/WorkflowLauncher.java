package com.butos.ebx.smart.workflow;

import java.util.HashMap;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.ProcessLauncherResult;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.WorkflowEngine;

public class WorkflowLauncher {
	
	public ProcessLauncherResult launchWorkflow(Repository repository, Session session, String workflowName, String launchWorkflowLabelName) throws OperationException {
		
		WorkflowEngine wfEngine = WorkflowEngine.getFromRepository(repository,
				session);

		PublishedProcessKey ppk = PublishedProcessKey.forName(workflowName);

		ProcessLauncher pl = wfEngine.getProcessLauncher(ppk);
		
		if(launchWorkflowLabelName != null && !launchWorkflowLabelName.trim().isEmpty())
			pl.setLabel(UserMessage.createInfo(launchWorkflowLabelName));

		ProcessLauncherResult wfLaunchResult = pl.launchProcessWithResult();
		
		return wfLaunchResult;
		
	}

	public ProcessLauncherResult launchWorkflowWithInputParameters(Repository repository, Session session, String workflowName, HashMap<String, String> inputParameters, String launchWorkflowLabelName) throws OperationException {
		
		WorkflowEngine wfEngine = WorkflowEngine.getFromRepository(repository,
				session);

		PublishedProcessKey ppk = PublishedProcessKey.forName(workflowName);

		ProcessLauncher pl = wfEngine.getProcessLauncher(ppk);
		
		if(inputParameters != null && !inputParameters.isEmpty()) {
			inputParameters.forEach((variableName,value)->{
				pl.setInputParameter(variableName, value);
			});
		}
			
		
		if(launchWorkflowLabelName != null && !launchWorkflowLabelName.trim().isEmpty())
			pl.setLabel(UserMessage.createInfo(launchWorkflowLabelName));

		ProcessLauncherResult wfLaunchResult = pl.launchProcessWithResult();
		
		return wfLaunchResult;
		
	}

}
