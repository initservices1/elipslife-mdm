package com.elipslife.mdm.hr.trigger;

import java.util.Date;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.common.HrCommonRules;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public class EmployeeTableTrigger extends TableTrigger {

	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		super.handleBeforeCreate(aContext);

		aContext.getOccurrenceContextForUpdate().setValue(Constants.RecordOperations.CREATE,
				HrModelPaths._Employee._Control_LastSyncAction);
		aContext.getOccurrenceContextForUpdate().setValue(new Date(),
				HrModelPaths._Employee._Control_LastSyncTimestamp);
		
		String employeeBusId = (String)aContext.getOccurrenceContext().getValue(HrModelPaths._Employee._Employee_EmployeeBusId);
		if(employeeBusId != null) {
			aContext.getOccurrenceContextForUpdate().setValue(employeeBusId, HrModelPaths._Employee._Employee_EmployeeId);
		}

		// Link Organisation Id By OrganisationBusId
		String organisationBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._JobPosition_OrganisationBusId);
		if (organisationBusId != null) {
			HrCommonRules.setOrganisationIdByParentBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Employee._JobPosition_OrganisationId, organisationBusId);
		}

		// Link LineManagerId By LineManagerBusId
		String lineMangerBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._JobPosition_LineManagerBusId);
		if (lineMangerBusId != null) {
			HrCommonRules.setManagerIdByManagerBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Employee._JobPosition_LineManagerId, lineMangerBusId);
		}

		// Link DottedLineManagerId by DottedLineManagerBusId
		String dottedLineManagerBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._JobPosition_DottedLineManagerBusId);
		if (dottedLineManagerBusId != null) {
			HrCommonRules.setManagerIdByManagerBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Employee._JobPosition_DottedLineManagerId, dottedLineManagerBusId);
		}

		// Link BuidlingId by BuildingBusId
		String buildingBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._WorkplaceLocation_BuildingBusId);
		if (buildingBusId != null) {
			HrCommonRules.setBuildingIdByBuildingBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Employee._WorkplaceLocation_BuildingId, buildingBusId);
		}

		// Link EmployeerId by EmployeerBusId
		String employeerBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._EmployerCompany_EmployerBusId);
		if (employeerBusId != null) {
			HrCommonRules.setPartyIdByPartyBusId(aContext, aContext.getOccurrenceContextForUpdate(), employeerBusId);
		}

	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
		super.handleBeforeModify(aContext);

		aContext.getOccurrenceContextForUpdate().setValue(Constants.RecordOperations.UPDATE,
				HrModelPaths._Building._Control_LastSyncAction);
		aContext.getOccurrenceContextForUpdate().setValue(new Date(),
				HrModelPaths._Building._Control_LastSyncTimestamp);

		// Link Organisation Id By OrganisationBusId
		String organisationBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._JobPosition_OrganisationBusId);
		if (organisationBusId != null) {
			HrCommonRules.setOrganisationIdByParentBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Employee._JobPosition_OrganisationId, organisationBusId);
		}

		// Link LineManagerId By LineManagerBusId
		String lineMangerBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._JobPosition_LineManagerBusId);
		if (lineMangerBusId != null) {
			HrCommonRules.setManagerIdByManagerBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Employee._JobPosition_LineManagerId, lineMangerBusId);
		}

		// Link DottedLineManagerId by DottedLineManagerBusId
		String dottedLineManagerBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._JobPosition_DottedLineManagerBusId);
		if (dottedLineManagerBusId != null) {
			HrCommonRules.setManagerIdByManagerBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Employee._JobPosition_DottedLineManagerId, dottedLineManagerBusId);
		}

		// Link BuidlingId by BuildingBusId
		String buildingBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._WorkplaceLocation_BuildingBusId);
		if (buildingBusId != null) {
			HrCommonRules.setBuildingIdByBuildingBusId(aContext, aContext.getOccurrenceContextForUpdate(),
					HrModelPaths._Employee._WorkplaceLocation_BuildingId, buildingBusId);
		}

		// Link EmployeerId by EmployeerBusId
		String employeerBusId = (String) aContext.getOccurrenceContext()
				.getValue(HrModelPaths._Employee._EmployerCompany_EmployerBusId);
		if (employeerBusId != null) {
			HrCommonRules.setPartyIdByPartyBusId(aContext, aContext.getOccurrenceContextForUpdate(), employeerBusId);
		}

	}

	@Override
	public void setup(TriggerSetupContext aContext) {
		// TODO Auto-generated method stub

	}

}
