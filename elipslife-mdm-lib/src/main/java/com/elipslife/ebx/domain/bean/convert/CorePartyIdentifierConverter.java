package com.elipslife.ebx.domain.bean.convert;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;

public class CorePartyIdentifierConverter {

	public static HashMap<Path, Object> fillCorePartyIdentifierValues(CorePartyIdentifierBean bean) {

		HashMap<Path, Object> values = new HashMap<Path, Object>();

		values.put(PartyPaths._PartyIdentifier._SourceSystem, bean.getSourceSystem());
		values.put(PartyPaths._PartyIdentifier._SourcePartyBusId, bean.getSourcePartyBusId());
		values.put(PartyPaths._PartyIdentifier._PartyId, bean.getPartyId());

		return values;
	}

	public static HashMap<Path, Object> fillCorePartyIdentifierValuesByRecord(Adaptation corePartyRecord) {

		HashMap<Path, Object> values = new HashMap<Path, Object>();

		values.put(PartyPaths._PartyIdentifier._SourceSystem, corePartyRecord.get(PartyPaths._Party._Control_SourceSystem));
		values.put(PartyPaths._PartyIdentifier._SourcePartyBusId, corePartyRecord.get(PartyPaths._Party._Control_SourcePartyBusId));
		values.put(PartyPaths._PartyIdentifier._PartyId, String.valueOf(corePartyRecord.get(PartyPaths._Party._PartyInfo_PartyId)));

		return values;
	}

}
