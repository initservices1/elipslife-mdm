package com.elipslife.ebx;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DummyTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    public void dummyTest() {
        // arrange

        // act
        String result = "dosomething()";

        // verify
        assertEquals(result, "dosomething()");
    }
}