package com.elipslife.mdm.uw.constraint;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.elipslife.mdm.path.UWPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

import ch.butos.ebx.lib.validation.SmartRecordAndTableLevelValidationCheck;

public class UWCaseTableConstraint extends SmartRecordAndTableLevelValidationCheck {

	@Override
	protected void executeBusinessValidations(ValueContext recordContext,
			Map<SchemaNode, Set<UserMessage>> validationMessages, boolean firstRecord) {
		
		// String contractNumber = (String)recordContext.getValue(UWPaths._UWCase._Context_ContractNumber);
		// String insuranceContractId = (String)recordContext.getValue(UWPaths._UWCase._Context_InsuranceContractId);
		String applicantId = (String)recordContext.getValue(UWPaths._UWCase._Applicant_ApplicantId);
		String applicantBusId = (String)recordContext.getValue(UWPaths._UWCase._Applicant_ApplicantBusId);
		String uwApplicationId = (String)recordContext.getValue(UWPaths._UWCase._Context_UWApplicationId);
		String uwApplicationBusId = (String)recordContext.getValue(UWPaths._UWCase._Context_UWApplicationBusId);
		
		UserMessage userMsg = null;
		
		/*
		if(contractNumber != null && insuranceContractId == null) {
			if(!contractNumber.isEmpty()) {
				userMsg = UserMessage.createWarning("Insurance Contract Id ' " + insuranceContractId + " ' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(UWPaths._UWCase._Context_InsuranceContractId), userMsg);
			}
		}
		*/
		
		if(applicantBusId != null && applicantId == null) {
			if(!applicantBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("Applicant Bus Id ' " + applicantBusId + " ' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(UWPaths._UWCase._Applicant_ApplicantId), userMsg);
			}
		}
		
		if(uwApplicationBusId != null && uwApplicationId == null) {
			if(!uwApplicationBusId.isEmpty()) {
				userMsg = UserMessage.createWarning("UW Application Bus Id ' " + uwApplicationBusId + " ' is missing");
				this.insertUserMsg(validationMessages, recordContext.getNode(UWPaths._UWCase._Context_UWApplicationId), userMsg);
			}
		}
		
	}

	@Override
	public void setup(ConstraintContextOnTable aContext) {
		
	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		return null;
	}

}
