package com.elipslife.mdm.uw.trigger;

import java.util.Date;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.uw.landing.LandingUWApplicationBean;
import com.elipslife.mdm.uw.landing.LandingUWApplicationReader;
import com.elipslife.mdm.uw.landing.LandingUWApplicationWriter;
import com.elipslife.mdm.uw.landing.LandingUWApplicationWriterProcedureContext;
import com.elipslife.mdm.uw.staging.StagingUWApplicationBean;
import com.elipslife.mdm.uw.staging.StagingUWApplicationWriter;
import com.elipslife.mdm.uw.staging.StagingUWApplicationWriterSession;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;

public class LandingUWApplicationTableTrigger extends TableTrigger {

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {
		super.handleAfterCreate(aContext);

		// only allow DataSpace "Landing UW" to move
		if (aContext.getAdaptationHome().getKey().getName().equals(Constants.DataSpace.LANDING_UW_REFERENCE)) {
			moveUWApplicationFromLandingToStaging(aContext.getSession(), aContext.getProcedureContext(),
					aContext.getAdaptationOccurrence());
		}
	}
	
	public void moveUWApplicationFromLandingToStaging(Session session, ProcedureContext context, Adaptation adaptation) {
		// 1. Read landing UWApplication	
		LandingUWApplicationReader landingUWApplicationReader = new LandingUWApplicationReader();
		LandingUWApplicationBean landingUWApplicationBean = landingUWApplicationReader.createBeanFromRecord(adaptation);
		
		// 2. Copy values into new Staging UWApplication
		StagingUWApplicationBean stagingUWApplicationBean = new StagingUWApplicationBean();
		transferbean(landingUWApplicationBean, stagingUWApplicationBean);
		
		// 3. Create Staging UWApplication
		StagingUWApplicationWriter stagingUWApplicationWriter = new StagingUWApplicationWriterSession(session, Constants.DataSpace.UW_REFERENCE);
		stagingUWApplicationWriter.createBeanOrThrow(stagingUWApplicationBean);
		
		// 4. Delete Landing UWApplication
		LandingUWApplicationWriter landingUWApplicationWriter = new LandingUWApplicationWriterProcedureContext(context);
		landingUWApplicationWriter.delete(landingUWApplicationBean);
	}
	
	public void transferbean(LandingUWApplicationBean applicationBean, StagingUWApplicationBean bean) {
		
		bean.setUWApplicationBusId(applicationBean.getUWApplicationBusId());
        bean.setSourceSystem(applicationBean.getSourceSystem());
        bean.setDataOwner(applicationBean.getDataOwner());
        bean.setOperatingCountry(applicationBean.getOperatingCountry());
        bean.setPolicyholderBusId(applicationBean.getPolicyholderBusId());
        bean.setPolicyholderName(applicationBean.getPolicyholderName());
        bean.setMainAffiliateBusId(applicationBean.getMainAffiliateBusId());
        bean.setMainAffiliateFirstName(applicationBean.getMainAffiliateFirstName());
        bean.setMainAffiliateLastName(applicationBean.getMainAffiliateLastName());
        bean.setMainAffiliateDateOfBirth(applicationBean.getMainAffiliateDateOfBirth());
        bean.setMainAffiliateSSN(applicationBean.getMainAffiliateSSN());
        bean.setMainAffiliateTown(applicationBean.getMainAffiliateTown());
        bean.setMainAffiliatePostCode(applicationBean.getMainAffiliatePostCode());
        bean.setMainAffiliateCountry(applicationBean.getMainAffiliateCountry());
        bean.setSubmissionDate(applicationBean.getSubmissionDate());
        bean.setIsActive(applicationBean.getIsActive());
        bean.setDocSignId(applicationBean.getDocSignId());
        bean.setSFSignedDate(applicationBean.getSFSignedDate());
        bean.setCreatedBy(applicationBean.getCreatedBy());
		bean.setCreationTimeStamp(applicationBean.getCreationTimeStamp());
		bean.setLastUpdatedBy(applicationBean.getLastUpdatedBy());
		bean.setLastUpdateTimeStamp(applicationBean.getLastUpdateTimeStamp());
		bean.setDeletionTimeStamp(applicationBean.getDeletionTimeStamp());
		bean.setLastSyncTimeStamp(new Date());
		bean.setAction(applicationBean.getAction());
		
	}
	
	@Override
	public void setup(TriggerSetupContext aContext) {

	}

}
