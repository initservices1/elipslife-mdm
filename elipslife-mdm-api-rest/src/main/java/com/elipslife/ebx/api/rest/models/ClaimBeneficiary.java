package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.json.bind.annotation.JsonbProperty;

public class ClaimBeneficiary {
	
	// Beneficiary
	@JsonbProperty("ClaimBeneficiaryBusId")
	public String claimBeneficiaryBusId;
	
	@JsonbProperty("BeneficiaryType")
	public String beneficiaryType;
	
	@JsonbProperty("BeneficiarySubtype")
	public String beneficiarySubtype;
	
	// Party ID's
	@JsonbProperty("PartyBusId")
	public String partyBusId;
	
	@JsonbProperty("PartyBusId2")
	public String partyBusId2;
	
	//Person details
	@JsonbProperty("FirstName")
	public String firstName;
	
	@JsonbProperty("LastName")
	public String lastName;
	
	@JsonbProperty("DateOfBirth")
	public LocalDate dateOfBirth;
	
	@JsonbProperty("Gender")
	public String gender;
	
	// Valid from
	@JsonbProperty("ValidFrom")
	public LocalDate validFrom;
	
	@JsonbProperty("ValidTo")
	public LocalDate validTo;
	
	@JsonbProperty("CreationTimestamplidTo")
	public LocalDateTime creationTimestamp;
	
	@JsonbProperty("LastUpdateTimestamp")
	public LocalDateTime lastUpdateTimestamp;
	
	@JsonbProperty("Action")
	public String action;
	
	@JsonbProperty("ActionTimestamp")
	public LocalDateTime actionTimestamp;
	
}