package com.elipslife.mdm.hr.core;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class BuildingBean extends Entity<Integer>{
    private String BuildingId;
    private String DataOwner;
    private String BuildingBusId;
    private String Name;
    private String Street;
    private String Street2;
    private String PostCode;
    private String POBox;
    private String POBoxPostCode;
    private String POBoxTown;
    private String Town;
    private String District;
    private String StateProvince;
    private String Country;
    private Boolean IsActive;
    private String Control;
    
    // Control
    private String sourceSystem;
	private String createdBy;
	private Date creationTimeStamp;
	private String lastUpdatedBy;
	private Date lastUpdateTimeStamp;
	private String lastSyncAction;
	private Date lastSyncTimeStamp;

    public String getBuildingId() {
        return BuildingId;
    }

    public void setBuildingId(String BuildingId) {
        this.BuildingId = BuildingId;
    }

    public String getDataOwner() {
        return DataOwner;
    }

    public void setDataOwner(String DataOwner) {
        this.DataOwner = DataOwner;
    }

    public String getBuildingBusId() {
        return BuildingBusId;
    }

    public void setBuildingBusId(String BuildingBusId) {
        this.BuildingBusId = BuildingBusId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String Street) {
        this.Street = Street;
    }

    public String getStreet2() {
        return Street2;
    }

    public void setStreet2(String Street2) {
        this.Street2 = Street2;
    }

    public String getPostCode() {
        return PostCode;
    }

    public void setPostCode(String PostCode) {
        this.PostCode = PostCode;
    }

    public String getPOBox() {
        return POBox;
    }

    public void setPOBox(String POBox) {
        this.POBox = POBox;
    }

    public String getPOBoxPostCode() {
        return POBoxPostCode;
    }

    public void setPOBoxPostCode(String POBoxPostCode) {
        this.POBoxPostCode = POBoxPostCode;
    }

    public String getPOBoxTown() {
        return POBoxTown;
    }

    public void setPOBoxTown(String POBoxTown) {
        this.POBoxTown = POBoxTown;
    }

    public String getTown() {
        return Town;
    }

    public void setTown(String Town) {
        this.Town = Town;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String District) {
        this.District = District;
    }

    public String getStateProvince() {
        return StateProvince;
    }

    public void setStateProvince(String StateProvince) {
        this.StateProvince = StateProvince;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public Boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(Boolean IsActive) {
        this.IsActive = IsActive;
    }

    public String getControl() {
        return Control;
    }

    public void setControl(String Control) {
        this.Control = Control;
    }

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdateTimeStamp() {
		return lastUpdateTimeStamp;
	}

	public void setLastUpdateTimeStamp(Date lastUpdateTimeStamp) {
		this.lastUpdateTimeStamp = lastUpdateTimeStamp;
	}

	public String getLastSyncAction() {
		return lastSyncAction;
	}

	public void setLastSyncAction(String lastSyncAction) {
		this.lastSyncAction = lastSyncAction;
	}

	public Date getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}

	public void setLastSyncTimeStamp(Date lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}
}
