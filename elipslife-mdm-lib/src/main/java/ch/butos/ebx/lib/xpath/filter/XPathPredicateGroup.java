package ch.butos.ebx.lib.xpath.filter;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * https://dl.orchestranetworks.com/restricted/documentation/en/advanced/references/xpath_ref.html
 *
 */
public class XPathPredicateGroup {

    public enum LinkingType {
        And, Or
    }

    private LinkingType linkingType;
    private Collection<XPathPredicateGroup> xPathPredicateGroups;
    private Collection<XPathPredicate> xPathPredicates;

    public XPathPredicateGroup(LinkingType linkingType) {
        this.linkingType = linkingType;
        this.xPathPredicateGroups = new ArrayList<>();
        this.xPathPredicates = new ArrayList<>();
    }

    public void addPredicateGroup(XPathPredicateGroup xPathPredicateGroup) {
        this.xPathPredicateGroups.add(xPathPredicateGroup);
    }

    public void addPredicate(XPathPredicate xPathPredicate) {
        this.xPathPredicates.add(xPathPredicate);
    }

    @Override
    public String toString() {

        String linkingType = this.linkingType == LinkingType.And ? " and " : " or ";

        List<String> predicateGroups = this.xPathPredicateGroups.stream().map(xppg -> xppg.toString()).collect(Collectors.toList());
        List<String> predicates = this.xPathPredicates.stream().map(xpp -> xpp.toString()).collect(Collectors.toList());

        String queryPredicateGroups = String.join(linkingType, predicateGroups);
        String queryPredicates = String.join(linkingType, predicates);

        boolean hasContent =
            (queryPredicateGroups != null && !queryPredicateGroups.isEmpty()) || (queryPredicates != null && !queryPredicates.isEmpty());
        boolean hasSubcontentAndLeafs =
            (queryPredicateGroups != null && !queryPredicateGroups.isEmpty()) && (queryPredicates != null && !queryPredicates.isEmpty());

        String query =
            (hasContent ? "(" : "") +
                queryPredicateGroups +
                (hasSubcontentAndLeafs ? linkingType : "") +
                queryPredicates +
                (hasContent ? ")" : "");

        return query;
    }
}
