package com.elipslife.ebx.data.access;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class CorePartyWriter extends BasePartyWriter<CorePartyBean> {

	private boolean useDefaultValues;
	
	public CorePartyWriter(boolean useDefaultValues) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
				PartyPaths._Party.getPathInSchema()),
			useDefaultValues);
	}
	
	public CorePartyWriter(boolean useDefaultValues, String dataspace) {
        this(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataspace, PartyConstants.DataSet.PARTY_MASTER),
                PartyPaths._Party.getPathInSchema()),
            useDefaultValues);
    }
	
	public CorePartyWriter(AdaptationTable adaptationTable, boolean useDefaultValues) {
		super(new CorePartyReader(adaptationTable), adaptationTable);
		this.useDefaultValues = useDefaultValues;
	}

	
	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, CorePartyBean bean) {

		values.put(PartyPaths._Party._PartyInfo_PartyId, bean.getId());
		
		// values.put(PartyPaths._Party._PartyInfo_MasterRecord, bean.getMasterRecord());
		
		if(!useDefaultValues || bean.getDataOwner() != null) {
			values.put(PartyPaths._Party._PartyInfo_DataOwner, bean.getDataOwner());
		}
		
		values.put(PartyPaths._Party._PartyInfo_OperatingCountry, bean.getOperatingCountry());
		values.put(PartyPaths._Party._PartyInfo_PartyBusId, bean.getPartyBusId());
		values.put(PartyPaths._Party._PartyInfo_CRMId, bean.getCrmId());
		values.put(PartyPaths._Party._PartyInfo_PartyType, bean.getPartyType());
		values.put(PartyPaths._Party._PartyInfo_PartyName, bean.getPartyName());
		values.put(PartyPaths._Party._PartyInfo_PartyNameSearchText, bean.getPartyNameSearchText());
		values.put(PartyPaths._Party._PartyInfo_RelationshipOwner, bean.getRelationshipOwner());
		values.put(PartyPaths._Party._PartyInfo_DataResponsible, bean.getDataResponsible());
		values.put(PartyPaths._Party._PartyInfo_Language, bean.getLanguage());
		values.put(PartyPaths._Party._PartyInfo_LegalPostalAddress, bean.getLegalAddressId());
		
		if(!useDefaultValues || bean.getIsActive() != null) {
			values.put(PartyPaths._Party._PartyInfo_IsActive, bean.getIsActive());
		}
		
		values.put(PartyPaths._Party._Person_FirstName, bean.getFirstName());
		values.put(PartyPaths._Party._Person_MiddleName, bean.getMiddleName());
		values.put(PartyPaths._Party._Person_LastName, bean.getLastName());
		values.put(PartyPaths._Party._Person_DateOfBirth, bean.getDateOfBirth());
		values.put(PartyPaths._Party._Person_Gender, bean.getGender());
		values.put(PartyPaths._Party._Person_Title, bean.getTitle());
		values.put(PartyPaths._Party._Person_Salutation, bean.getSalutation());
		values.put(PartyPaths._Party._Person_LetterSalutation, bean.getLetterSalutation());
		values.put(PartyPaths._Party._Person_CivilStatus, bean.getCivilStatus());
		values.put(PartyPaths._Party._Person_BirthCity, bean.getBirthCity());
		values.put(PartyPaths._Party._Person_BirthCountry, bean.getBirthCountry());
		values.put(PartyPaths._Party._IdDocument_ID_DocumentNumber, bean.getIdDocumentNumber());
		values.put(PartyPaths._Party._IdDocument_ID_DocumentType, bean.getIdDocumentType());
		values.put(PartyPaths._Party._IdDocument_ID_IssueDate, bean.getIdIssueDate());
		values.put(PartyPaths._Party._IdDocument_ID_ExpiryDate, bean.getIdExpiryDate());
		values.put(PartyPaths._Party._IdDocument_ID_IssuingAuthority, bean.getIdIssuingAuthority());
		values.put(PartyPaths._Party._Organisation_ParentId, bean.getParentId());
		values.put(PartyPaths._Party._Organisation_SourceParentBusId, bean.getSourceParentBusId());
		values.put(PartyPaths._Party._Organisation_OrgName, bean.getOrgName());
		values.put(PartyPaths._Party._Organisation_OrgName2, bean.getOrgName2());
		values.put(PartyPaths._Party._Organisation_CompanyType, bean.getCompanyType());
		values.put(PartyPaths._Party._Organisation_CommercialRegNo, bean.getCommercialRegNo());
		values.put(PartyPaths._Party._Organisation_UIDNumber, bean.getUidNumber());
		values.put(PartyPaths._Party._Organisation_VATNumber, bean.getVatNumber());
		values.put(PartyPaths._Party._Organisation_Industry, bean.getIndustry());
		values.put(PartyPaths._Party._Organisation_SubIndustry, bean.getSubIndustry());
		values.put(PartyPaths._Party._Organisation_PreferredEmail, bean.getPreferredEmail());
		values.put(PartyPaths._Party._Organisation_PreferredCommChannel, bean.getPreferredCommChannel());
		values.put(PartyPaths._Party._ContactPerson_OrganisationId, bean.getOrganisationId());
		values.put(PartyPaths._Party._ContactPerson_SourceOrganisationBusId, bean.getSourceOrganisationBusId());
		values.put(PartyPaths._Party._ContactPerson_LineManagerId, bean.getLineManagerId());
		values.put(PartyPaths._Party._ContactPerson_SourceLineManagerBusId, bean.getSourceLineManagerBusId());
		values.put(PartyPaths._Party._ContactPerson_PartnerId, bean.getPartnerId());
		values.put(PartyPaths._Party._ContactPerson_SourcePartnerBusId, bean.getSourcePartnerBusId());
		values.put(PartyPaths._Party._ContactPerson_Department, bean.getDepartment());
		values.put(PartyPaths._Party._ContactPerson_Function, bean.getFunction());
		values.put(PartyPaths._Party._ContactPerson_IsExecutor, bean.getIsExecutor());
        values.put(PartyPaths._Party._ContactPerson_IsLegalRepresentative, bean.getIsLegalRepresentative());
		
		if(!useDefaultValues || bean.getIsSeniorManager() != null) {
			values.put(PartyPaths._Party._ContactPerson_IsSeniorManager, bean.getIsSeniorManager());
		}
		
		if(!useDefaultValues || bean.getIsUltimateBeneficialOwner() != null) {
			values.put(PartyPaths._Party._ContactPerson_IsUltimateBeneficialOwner, bean.getIsUltimateBeneficialOwner());
		}
		
		values.put(PartyPaths._Party._ContactPerson_BORelationship, bean.getBoRelationship());
		values.put(PartyPaths._Party._ContactPerson_ContactPersonState, bean.getContactPersonState());
		values.put(PartyPaths._Party._Role_Roles, bean.getRoles());
		values.put(PartyPaths._Party._Role_IsBeneficiary, bean.getIsBeneficiary());
		values.put(PartyPaths._Party._Role_IsInsuredPerson, bean.getIsInsuredPerson());
		values.put(PartyPaths._Party._Role_IsInjuredPerson, bean.getIsInjuredPerson());
		values.put(PartyPaths._Party._Role_IsElipsLifeLegalEntity, bean.getIsElipsLifeLegalEntity());
		values.put(PartyPaths._Party._Role_IsBusinessPartner, bean.getIsBusinessPartner());
		values.put(PartyPaths._Party._Role_IsPolicyholder, bean.getIsPolicyholder());
		values.put(PartyPaths._Party._Role_IsReCoInsurer, bean.getIsReCoInsurer());
		values.put(PartyPaths._Party._Role_IsVendor, bean.getIsVendor());
		values.put(PartyPaths._Party._Role_IsMedicalServiceProvider, bean.getIsMedicalServiceProvider());
		values.put(PartyPaths._Party._Role_IsStateRegulatoryAuthority, bean.getIsStateRegulatoryAuthority());
		values.put(PartyPaths._Party._Role_IsDistributionPartner, bean.getIsDistributionPartner());
		values.put(PartyPaths._Party._Role_IsBroker, bean.getIsBroker());
		values.put(PartyPaths._Party._Role_IsAssociation, bean.getIsAssociation());
		values.put(PartyPaths._Party._Role_IsBank, bean.getIsBank());
		values.put(PartyPaths._Party._Role_IsConsultant, bean.getIsConsultant());
		values.put(PartyPaths._Party._Role_IsTPA, bean.getIsTpa());
		values.put(PartyPaths._Party._Role_IsInsurance, bean.getIsInsurance());
		
		if(!useDefaultValues || bean.getIsVipClient() != null) {
			values.put(PartyPaths._Party._Role_IsVIPClient, bean.getIsVipClient());
		}
		
		values.put(PartyPaths._Party._Role_Blacklist, bean.getBlacklist());
		values.put(PartyPaths._Party._Role_PartnerState, bean.getPartnerState());
		values.put(PartyPaths._Party._Role_CompanyPersonalNr, bean.getCompanyPersonalNr());
		values.put(PartyPaths._Party._Role_SocialSecurityNr, bean.getSocialSecurityNr());
		values.put(PartyPaths._Party._Role_SocialSecurityNrType, bean.getSocialSecurityNrType());
		values.put(PartyPaths._Party._Role_BrokerRegNo, bean.getBrokerRegNo());
		values.put(PartyPaths._Party._Role_BrokerTiering, bean.getBrokerTiering());
		values.put(PartyPaths._Party._Role_LineOfBusiness, bean.getLineOfBusiness());
		values.put(PartyPaths._Party._Role_IsLHLOB, bean.getIsLhlob());
		values.put(PartyPaths._Party._Role_IsAHLOB, bean.getIsAhlob());
		values.put(PartyPaths._Party._Financial_FinancePartyBusId, bean.getFinancePartyBusId());
		values.put(PartyPaths._Party._Financial_PaymentTerms, bean.getPaymentTerms());
		values.put(PartyPaths._Party._Financial_FinanceClients, bean.getFinanceClients());
		values.put(PartyPaths._Party._Financial_CreditorClients, bean.getCreditorClients());
		values.put(PartyPaths._Party._Control_ValidFrom, bean.getValidFrom());
		values.put(PartyPaths._Party._Control_ValidTo, bean.getValidTo());
		
		if(!useDefaultValues || bean.getIsExpired() != null) {
			values.put(PartyPaths._Party._Control_IsExpired, bean.getIsExpired());
		}
		
		values.put(PartyPaths._Party._Control_SourceSystem, bean.getSourceSystem());
		values.put(PartyPaths._Party._Control_SourcePartyBusId, bean.getSourcePartyBusId());
		values.put(PartyPaths._Party._Control_CreatedBy, bean.getCreatedBy());
		values.put(PartyPaths._Party._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(PartyPaths._Party._Control_LastUpdatedBy, bean.getLastUpdatedBy());
		values.put(PartyPaths._Party._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(PartyPaths._Party._Control_LastSyncAction, bean.getLastSyncAction());
		values.put(PartyPaths._Party._Control_LastSyncTimestamp, bean.getLastSyncTimestamp());
	}
}