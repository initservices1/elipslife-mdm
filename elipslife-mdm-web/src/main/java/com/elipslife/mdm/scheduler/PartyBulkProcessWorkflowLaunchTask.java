package com.elipslife.mdm.scheduler;

import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.WorkflowEngine;

public class PartyBulkProcessWorkflowLaunchTask extends ScheduledTask {

    String workflowToLaunch;
    String initialLandingDataSpace;
    String initialMasterDataSpace;
    String commitSize;

    @Override
    public void execute(ScheduledExecutionContext aContext) throws OperationException, ScheduledTaskInterruption {

        WorkflowEngine workflowEngine = WorkflowEngine.getFromRepository(aContext.getRepository(), aContext.getSession());
        ProcessLauncher processLauncher = workflowEngine.getProcessLauncher(PublishedProcessKey.forName(getWorkflowToLaunch()));
        processLauncher.setInputParameter("initialLandingDataSpace", getInitialLandingDataSpace());
        processLauncher.setInputParameter("initialMasterDataSpace", getInitialMasterDataSpace());
        processLauncher.setInputParameter("commitSize", getCommitSize());
        processLauncher.setInputParameter("workflowToLaunch", workflowToLaunch);
        processLauncher.launchProcess();
    }

    public String getWorkflowToLaunch() {
        return workflowToLaunch;
    }


    public void setWorkflowToLaunch(String workflowToLaunch) {
        this.workflowToLaunch = workflowToLaunch;
    }


    public String getInitialLandingDataSpace() {
        return initialLandingDataSpace;
    }


    public void setInitialLandingDataSpace(String initialLandingDataSpace) {
        this.initialLandingDataSpace = initialLandingDataSpace;
    }


    public String getInitialMasterDataSpace() {
        return initialMasterDataSpace;
    }


    public void setInitialMasterDataSpace(String initialMasterDataSpace) {
        this.initialMasterDataSpace = initialMasterDataSpace;
    }

    
    public String getCommitSize() {
        return commitSize;
    }

    
    public void setCommitSize(String commitSize) {
        this.commitSize = commitSize;
    }

}
