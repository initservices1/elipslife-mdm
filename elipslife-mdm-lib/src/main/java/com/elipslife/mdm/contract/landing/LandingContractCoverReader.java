package com.elipslife.mdm.contract.landing;

import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class LandingContractCoverReader extends AbstractTableReader<LandingContractCoverBean> {

	public LandingContractCoverReader() {
		this(PartyConstants.DataSpace.LANDING_REFERENCE);
	}
	
	public LandingContractCoverReader(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_ContractCover.getPathInSchema()));
	}
	
	public LandingContractCoverReader(AdaptationTable table) {
		super(table);
	}

	@Override
	protected LandingContractCoverBean createBeanFromRecord(Adaptation record) {
		LandingContractCoverBean bean = new LandingContractCoverBean();
		
		bean.setId(record.get_int(LandingPaths._LDG_ContractCover._Cover_ContractCoverId));
		bean.setExposureBusId(record.getString(LandingPaths._LDG_ContractCover._Exposure_ExposureBusId));
		bean.setCoverBusId(record.getString(LandingPaths._LDG_ContractCover._Cover_CoverBusId));
		bean.setCoverGroupType(record.getString(LandingPaths._LDG_ContractCover._Cover_CoverGroupType));
		bean.setCoverGroupTypeName(record.getString(LandingPaths._LDG_ContractCover._Cover_CoverGroupTypeName));
		bean.setCoverName(record.getString(LandingPaths._LDG_ContractCover._Cover_CoverName));
		bean.setCoverTemplateBusId(record.getString(LandingPaths._LDG_ContractCover._Cover_CoverTemplateBusId));
		bean.setBeneficiaryType(record.getString(LandingPaths._LDG_ContractCover._Cover_BeneficiaryType));
		bean.setExposureId(record.get_int(LandingPaths._LDG_ContractCover._Exposure_ExposureId));
		bean.setSourceSystem(record.getString(LandingPaths._LDG_ContractCover._Control_SourceSystem));
		bean.setCreationTimestamp(record.getDate(LandingPaths._LDG_ContractCover._Control_CreationTimestamp));
		bean.setLastUpdateTimestamp(record.getDate(LandingPaths._LDG_ContractCover._Control_LastUpdateTimestamp));
		bean.setLastSyncTimeStamp(record.getDate(LandingPaths._LDG_ContractCover._Control_LastSyncTimestamp));
		bean.setAction(record.getString(LandingPaths._LDG_ContractCover._Control_Action));
		bean.setActionBy(record.getString(LandingPaths._LDG_ContractCover._Control_ActionBy));
		
		return bean;
	}

	@Override
	protected Path[] getPkPaths() {
		return new Path[] { LandingPaths._LDG_ContractCover._Cover_ContractCoverId };
	}

	@Override
	protected Object[] retrievePks(LandingContractCoverBean bean) {
		return new Object[] { bean.getId() };
	}
	
	/**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}
}