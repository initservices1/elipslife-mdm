package com.elipslife.mdm.party.scheduler;

import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.WorkflowEngine;

public class DeletePartyRecordsFromLandingTablesTask extends ScheduledTask {

    private String dataspace;
    private String dataset;
    private String sourceSystem;
    private String workflowToLaunch;

    @Override
    public void execute(ScheduledExecutionContext aContext) throws OperationException, ScheduledTaskInterruption {

        aContext.addExecutionInformation("****** Party Landing Deletion workflow scheduled task started *****");
        WorkflowEngine workflowEngine = WorkflowEngine.getFromRepository(aContext.getRepository(), aContext.getSession());
        ProcessLauncher processLauncher = workflowEngine.getProcessLauncher(PublishedProcessKey.forName(workflowToLaunch));
        processLauncher.setInputParameter("dataspace", getDataspace());
        processLauncher.setInputParameter("dataset", getDataset());
        processLauncher.setInputParameter("sourceSystem", getSourceSystem());
        processLauncher.launchProcess();
        aContext.addExecutionInformation("****** Party Landing Deletion workflow scheduled task ended *****");
    }

    public String getDataspace() {
        return dataspace;
    }

    public void setDataspace(String dataspace) {
        this.dataspace = dataspace;
    }

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getWorkflowToLaunch() {
        return workflowToLaunch;
    }

    public void setWorkflowToLaunch(String workflowToLaunch) {
        this.workflowToLaunch = workflowToLaunch;
    }
}
