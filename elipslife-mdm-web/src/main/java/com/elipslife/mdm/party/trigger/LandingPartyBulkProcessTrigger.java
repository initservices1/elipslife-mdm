package com.elipslife.mdm.party.trigger;

import java.util.Date;

import com.elipslife.mdm.path.LandingPaths;
import com.elipslife.mdm.utilities.TriggerUtilities;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class LandingPartyBulkProcessTrigger extends TableTrigger {

    @Override
    public void setup(TriggerSetupContext context) {
        
    }

    @Override
    public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException {
        super.handleBeforeCreate(context);
        
        ValueContextForUpdate valueContextForUpdate = context.getOccurrenceContextForUpdate();
        valueContextForUpdate.setValue(new Date(), LandingPaths._LDG_Party_BulkProcess._Control_LastSyncTimestamp);
        
        TriggerUtilities.setDefaultValueIfNull(valueContextForUpdate, LandingPaths._LDG_Party_BulkProcess._Party_DataOwner);
        TriggerUtilities.setDefaultValueIfNull(valueContextForUpdate, LandingPaths._LDG_Party_BulkProcess._Party_IsActive);
        TriggerUtilities.setDefaultValueIfNull(valueContextForUpdate, LandingPaths._LDG_Party_BulkProcess._ContactPerson_IsExecutor);
        TriggerUtilities.setDefaultValueIfNull(valueContextForUpdate, LandingPaths._LDG_Party_BulkProcess._ContactPerson_IsLegalRepresentative);
        TriggerUtilities.setDefaultValueIfNull(valueContextForUpdate, LandingPaths._LDG_Party_BulkProcess._ContactPerson_IsSeniorManager);
        TriggerUtilities.setDefaultValueIfNull(valueContextForUpdate, LandingPaths._LDG_Party_BulkProcess._ContactPerson_IsUltimateBeneficialOwner);
        TriggerUtilities.setDefaultValueIfNull(valueContextForUpdate, LandingPaths._LDG_Party_BulkProcess._Role_IsVIPClient);
        
        TriggerUtilities.setMinimumDateIfNull(valueContextForUpdate, LandingPaths._LDG_Party_BulkProcess._Control_ValidFrom);
        TriggerUtilities.setMaximumDateIfNull(valueContextForUpdate, LandingPaths._LDG_Party_BulkProcess._Control_ValidTo);
    }
}
