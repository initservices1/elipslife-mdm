package com.elipslife.ebx.data.access;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.CoreBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class CoreBankAccountWriter extends BaseBankAccountWriter<CoreBankAccountBean> {
	
	private boolean useDefaultValues;
	
	public CoreBankAccountWriter(boolean useDefaultValues) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
				PartyPaths._BankAccount.getPathInSchema()),
			useDefaultValues);
	}
	
	public CoreBankAccountWriter(boolean useDefaultValues, String dataSpace) {
        this(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.PARTY_MASTER),
                PartyPaths._BankAccount.getPathInSchema()),
            useDefaultValues);
    }
	
	public CoreBankAccountWriter(AdaptationTable adaptationTable, boolean useDefaultValues) {
		super(new CoreBankAccountReader(adaptationTable), adaptationTable);
		
		this.useDefaultValues = useDefaultValues;
	}
	

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, CoreBankAccountBean bean) {

		values.put(PartyPaths._BankAccount._Account_BankAccountId, bean.getId());
		
		values.put(PartyPaths._BankAccount._Account_PartyId, bean.getPartyId());
		values.put(PartyPaths._BankAccount._Account_AccountType, bean.getAccountType());
		values.put(PartyPaths._BankAccount._Account_AccountNumber, bean.getAccountNumber());
		values.put(PartyPaths._BankAccount._Account_IBAN, bean.getIban());
		values.put(PartyPaths._BankAccount._Account_BIC, bean.getBic());
		values.put(PartyPaths._BankAccount._Account_ClearingNumber, bean.getClearingNumber());
		values.put(PartyPaths._BankAccount._Account_BankName, bean.getBankName());
		values.put(PartyPaths._BankAccount._Account_Country, bean.getCountry());
		values.put(PartyPaths._BankAccount._Account_Currency, bean.getCurrency());
		
		if(!useDefaultValues || bean.getIsExpired() != null) {
			values.put(PartyPaths._BankAccount._Account_IsMainAccount, bean.getIsMainAccount());
		}
		
		values.put(PartyPaths._BankAccount._Account_SourceBankAccountBusId, bean.getSourceBankAccountBusId());
		values.put(PartyPaths._BankAccount._Control_SourceSystem, bean.getSourceSystem());
		values.put(PartyPaths._BankAccount._Control_SourcePartyBusId, bean.getSourcePartyBusId());
		values.put(PartyPaths._BankAccount._Control_CreatedBy, bean.getCreatedBy());
		values.put(PartyPaths._BankAccount._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(PartyPaths._BankAccount._Control_LastUpdatedBy, bean.getLastUpdatedBy());
		values.put(PartyPaths._BankAccount._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(PartyPaths._BankAccount._Control_LastSyncAction, bean.getLastSyncAction());
		values.put(PartyPaths._BankAccount._Control_LastSyncTimestamp, bean.getLastSyncTimestamp());
		values.put(PartyPaths._BankAccount._Control_ValidFrom, bean.getValidFrom());
		values.put(PartyPaths._BankAccount._Control_ValidTo, bean.getValidTo());
		
		if(!useDefaultValues || bean.getIsExpired() != null) {
			values.put(PartyPaths._BankAccount._Control_IsExpired, bean.getIsExpired());
		}
	}
}