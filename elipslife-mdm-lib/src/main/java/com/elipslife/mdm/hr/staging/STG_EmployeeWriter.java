package com.elipslife.mdm.hr.staging;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.elipslife.mdm.hr.constants.LandingHrModelPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class STG_EmployeeWriter extends AbstractTableWriter<STG_EmployeeBean> {
    private STG_EmployeeReader stgEmployeereader;

    public STG_EmployeeWriter() {
        this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Employee.getPathInSchema()));
    }
    
    public STG_EmployeeWriter(String dataspace) {
        this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Employee.getPathInSchema()));
    }

    public STG_EmployeeWriter(AdaptationTable table) {
    	super(table);
        this.stgEmployeereader = new STG_EmployeeReader(table);
    }

        @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, STG_EmployeeBean bean) {
        values.put(HrModelPaths._STG_Employee._Employee_EmployeeId, bean.getEmployeeId());
        values.put(HrModelPaths._STG_Employee._Employee_DataOwner, bean.getDataOwner());
        values.put(HrModelPaths._STG_Employee._Employee_EmployeeBusId, bean.getEmployeeBusId());
        values.put(HrModelPaths._STG_Employee._Employee_UserName, bean.getUserName());
        values.put(HrModelPaths._STG_Employee._Employee_FirstName, bean.getFirstName());
        values.put(HrModelPaths._STG_Employee._Employee_PreferredFirstName, bean.getPreferredFirstName());
        values.put(HrModelPaths._STG_Employee._Employee_MiddleName, bean.getMiddleName());
        values.put(HrModelPaths._STG_Employee._Employee_LastName, bean.getLastName());
        values.put(HrModelPaths._STG_Employee._Employee_Gender, bean.getGender());
        values.put(HrModelPaths._STG_Employee._Employee_Salutation, bean.getSalutation());
        values.put(HrModelPaths._STG_Employee._Employee_Language, bean.getLanguage());
        values.put(HrModelPaths._STG_Employee._Employee_Email, bean.getEmail());
        values.put(HrModelPaths._STG_Employee._Employee_Telephone, bean.getTelephone());
        values.put(HrModelPaths._STG_Employee._Employee_Mobile, bean.getMobile());
        values.put(HrModelPaths._STG_Employee._Employee_Fax, bean.getFax());
        values.put(HrModelPaths._STG_Employee._Employee_IsActive, bean.getIsActive());
        values.put(HrModelPaths._STG_Employee._Employee_EmployeeMaster, bean.getEmployeeMaster());
        values.put(HrModelPaths._STG_Employee._Employee_IsSystemEnabled, bean.getIsSystemEnabled());
        values.put(HrModelPaths._STG_Employee._Employment_EmploymentCategory, bean.getEmploymentCategory());
        values.put(HrModelPaths._STG_Employee._Employment_ManagementLevel, bean.getManagementLevel());
        values.put(HrModelPaths._STG_Employee._Employment_SignOffRole, bean.getSignOffRole());
        values.put(HrModelPaths._STG_Employee._JobPosition_FunctionalTitle, bean.getFunctionalTitle());
        values.put(HrModelPaths._STG_Employee._JobPosition_PositionName, bean.getPositionName());
        values.put(HrModelPaths._STG_Employee._JobPosition_JobProfileName, bean.getJobProfileName());
        values.put(HrModelPaths._STG_Employee._JobPosition_IsLineManager, bean.getIsLineManager());
        values.put(HrModelPaths._STG_Employee._JobPosition_OrganisationBusId, bean.getOrganisationBusId());
        values.put(HrModelPaths._STG_Employee._JobPosition_LineManagerBusId, bean.getLineManagerBusId());
        values.put(HrModelPaths._STG_Employee._JobPosition_DottedLineManagerBusId, bean.getDottedLineManagerBusId());
        values.put(HrModelPaths._STG_Employee._WorkplaceLocation_BuildingBusId, bean.getBuildingBusId());
        
        //Employee Private Address
        values.put(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivateStreet, bean.getPrivateStreet());
        values.put(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivatePostCode, bean.getPrivatePostCode());
        values.put(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivateTown, bean.getPrivateTown());
        values.put(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivateStateProvince, bean.getPrivateStateProvince());
        values.put(HrModelPaths._STG_Employee._EmployeePrivateAddress_PrivateCountryCode, bean.getPrivateCountryCode());
        
        values.put(HrModelPaths._STG_Employee._EmployerCompany_EmployerBusId, bean.getEmployerBusId());
        values.put(HrModelPaths._STG_Employee._Control_SourceSystem, bean.getSourceSystem());
		values.put(HrModelPaths._STG_Employee._Control_CreatedBy, bean.getCreatedBy());
		values.put(HrModelPaths._STG_Employee._Control_CreationTimestamp, bean.getCreationTimeStamp());
		values.put(HrModelPaths._STG_Employee._Control_LastUpdatedBy, bean.getLastUpdatedBy());
		values.put(HrModelPaths._STG_Employee._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
		values.put(HrModelPaths._STG_Employee._Control_LastSyncTimestamp, bean.getLastSyncTimestamp());
    }

    @Override
    public AbstractTableReader<STG_EmployeeBean> getTableReader() {
        return stgEmployeereader;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub
		
	}
}
