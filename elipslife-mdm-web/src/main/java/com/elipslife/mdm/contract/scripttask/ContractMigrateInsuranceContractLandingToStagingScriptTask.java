package com.elipslife.mdm.contract.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.butos.ebx.procedure.CreateRecords;
import com.butos.ebx.procedure.DeleteRecords;
import com.butos.ebx.procedure.RecordValuesBean;
import com.elipslife.ebx.domain.bean.convert.ContractConverter;
import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractBean;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractReader;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractWriter;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractWriterSession;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class ContractMigrateInsuranceContractLandingToStagingScriptTask extends ScriptTask {

	private static int COMMITSIZE = 1;
	private static int recordCount = 0;

	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException {

		Map<String, String> dataContextVariables = ContractConverter.getContextVariables(context);
		String commitSize = dataContextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);
		String dataSpace = dataContextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		String landingDataspace = dataContextVariables.get(PartyConstants.DataContextVariables.TEMP_LANDING_DATASPACE);
		if (commitSize != null) {
			COMMITSIZE = Integer.parseInt(commitSize);
		}
		// AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(ContractConstants.DataSpace.CONTRACT_REF);
		AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(dataSpace);
		LandingInsuranceContractWriter landingInsuranceContractWriter = new LandingInsuranceContractWriterSession(
				context.getSession());
		LandingInsuranceContractReader landingInsuranceContractReader = (LandingInsuranceContractReader) landingInsuranceContractWriter
				.getTableReader();
		
		List<LandingInsuranceContractBean> landingInsuranceContractBean = landingInsuranceContractReader.readAllBeans();

		StringBuilder stringBuilderErrorMessage = new StringBuilder();
		List<RecordValuesBean> stagingContractsListToCreate = new ArrayList<RecordValuesBean>();
		List<LandingInsuranceContractBean> landingInusranceContractsToDelete = new ArrayList<LandingInsuranceContractBean>();
		landingInsuranceContractBean.forEach(licb -> {
			try {

				boolean hasValidationErrors = landingInsuranceContractReader
						.hasValidationErrors(new Object[] { licb.getId() });
				if (hasValidationErrors) {
					return;
				}
				recordCount++;
				HashMap<Path, Object> stagingInsuranceContractValues = ContractConverter
						.fillStagingInsuranceContractWithLandingInsuranceContractBean(licb, false);
				RecordValuesBean recordValuesBean = new RecordValuesBean(stagingInsuranceContractValues);
				stagingContractsListToCreate.add(recordValuesBean);

				if (recordCount > COMMITSIZE) {
					createStagingInsuranceContracts(dataspaceName, context.getSession(), stagingContractsListToCreate);
					stagingContractsListToCreate.clear();
					recordCount = 0;
				}

				landingInusranceContractsToDelete.add(licb);

			} catch (Exception ex) {
				stringBuilderErrorMessage.append(String.format("Migration error for landing insurance contract with ID %s:\r\n%s",
								licb.getId(), ex.toString()));
			}
		});

		if (!stagingContractsListToCreate.isEmpty()) { // create staging records out of batch size
			createStagingInsuranceContracts(dataspaceName, context.getSession(), stagingContractsListToCreate);
			stagingContractsListToCreate.clear();
			recordCount = 0;
		}

		// Delete Landing Insurance Contract Table records
		deleteLandingContractInsuranceRecords(landingDataspace, context.getSession(), landingInusranceContractsToDelete);

		context.setVariableString("errorMessage", stringBuilderErrorMessage.toString());
	}

	/**
	 * Create staging insurance contracts
	 * 
	 * @param dataspace
	 * @param session
	 * @param stagingInsuranceContractsList
	 * @throws OperationException
	 */

	private static void createStagingInsuranceContracts(AdaptationHome dataspace, Session session,
			List<RecordValuesBean> stagingInsuranceContractsList) throws OperationException {

		Adaptation dataset = dataspace
				.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
		AdaptationTable aTable = dataset.getTable(ContractPaths._STG_InsuranceContract.getPathInSchema());

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);

		CreateRecords createRecordsProcedure = new CreateRecords(aTable, stagingInsuranceContractsList);
		ProcedureResult result = service.execute(createRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}

	/**
	 * Delete Landing Insurance Contract tables records based on commit size
	 * 
	 * @param dataspace
	 * @param session
	 * @param landingInsuranceContractBeans
	 * @throws OperationException
	 */
	private static void deleteLandingContractInsuranceRecords(String dataspace, Session session,
			List<LandingInsuranceContractBean> landingInsuranceContractBeans) throws OperationException {

		int deleteRecordCount = 0;
		AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(dataspace);
		Adaptation dataset = dataspaceName.findAdaptationOrNull(AdaptationName.forName(PartyConstants.DataSet.LANDING_MASTER));
		AdaptationTable aTable = dataset.getTable(LandingPaths._LDG_InsuranceContract.getPathInSchema());
		ArrayList<Adaptation> recordValuesBeanList = new ArrayList<Adaptation>();
		for (LandingInsuranceContractBean landingInsuranceContractBean : landingInsuranceContractBeans) {
			deleteRecordCount++;
			recordValuesBeanList.add(aTable.lookupAdaptationByPrimaryKey(
					PrimaryKey.parseString(landingInsuranceContractBean.getId().toString())));
			if (deleteRecordCount > 1000) {
				deleteRecords(dataspaceName, session, recordValuesBeanList);
				recordValuesBeanList.clear();
				deleteRecordCount = 0;
			}
		}

		if (!recordValuesBeanList.isEmpty()) {
			deleteRecords(dataspaceName, session, recordValuesBeanList);
			recordValuesBeanList.clear();
			deleteRecordCount = 0;
		}
	}

	/**
	 * Delete table records
	 * 
	 * @param dataspace
	 * @param session
	 * @param recordValuesBeanList
	 * @throws OperationException
	 */
	private static void deleteRecords(AdaptationHome dataspace, Session session,
			ArrayList<Adaptation> recordValuesBeanList) throws OperationException {

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
		DeleteRecords deleteRecordsProcedure = new DeleteRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(deleteRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}
}