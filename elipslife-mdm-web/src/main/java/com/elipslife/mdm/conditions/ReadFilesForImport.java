package com.elipslife.mdm.conditions;

import java.io.File;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class ReadFilesForImport extends ConditionBean {
	private String inputDirectory;
	private String fileList;
	private String filePattern;

	@Override
	public boolean evaluateCondition(ConditionBeanContext aContext) throws OperationException {
		File[] files = new File(inputDirectory).listFiles();
		if (files.length > 0) {
			for (File file : files) {
				if (file.isFile()) {
					if (fileList == null) {
						fileList = "";
					}
					if(file.getAbsolutePath().contains(filePattern)) {
						if (!fileList.isEmpty()) {
							fileList += ",";
						}
						fileList += file.getAbsolutePath();
					}
				}
			}
			return true;
		} else {
			// empty inputdir
			// terminate and make email notification (next step in WF
			// "CheckAndSortCSVFilesForImport")
			return false;
		}
	}

	public String getInputDirectory() {
		return inputDirectory;
	}

	public void setInputDirectory(String inputDirectory) {
		this.inputDirectory = inputDirectory;
	}

	public void setFileList(String fileList) {
		this.fileList = fileList;
	}

	public String getFileList() {
		return fileList;
	}

	public String getFilePattern() {
		return filePattern;
	}

	public void setFilePattern(String filePattern) {
		this.filePattern = filePattern;
	}
}
