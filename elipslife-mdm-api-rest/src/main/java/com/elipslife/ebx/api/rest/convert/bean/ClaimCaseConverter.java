package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.ClaimCase;
import com.elipslife.ebx.api.rest.models.SynchroniseClaimCase;
import com.elipslife.mdm.claimcase.landing.LandingClaimCaseBean;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class ClaimCaseConverter extends AbstractConverter<LandingClaimCaseBean, SynchroniseClaimCase> {

	@Override
	public LandingClaimCaseBean convert(SynchroniseClaimCase synchroniseClaimCase) throws DatatypeConfigurationException {
		
		LandingClaimCaseBean landingClaimCaseBean = new LandingClaimCaseBean();
		
		ClaimCase claimCase = synchroniseClaimCase.data;
		
		landingClaimCaseBean.setDataOwner(claimCase.dataOwner);
		landingClaimCaseBean.setClaimCaseBusId(claimCase.claimCaseBusId);
		landingClaimCaseBean.setClaimCaseExtRef(claimCase.claimCaseExtRef);
		landingClaimCaseBean.setContractNumber(claimCase.contractNumber);
		landingClaimCaseBean.setSourceContractBusId(claimCase.contractExtRef);
		//landingClaimCaseBean.setContractVersionBusId(claimCase.contractExtRef);
		landingClaimCaseBean.setIsActive(claimCase.isActive);
		
		landingClaimCaseBean.setAssessmentState(claimCase.assessmentState);
		landingClaimCaseBean.setBusinessScope(claimCase.businessScope);
		landingClaimCaseBean.setClaimCause(claimCase.claimCause);
		landingClaimCaseBean.setClaimState(claimCase.claimState);
		landingClaimCaseBean.setClaimStateDetail(claimCase.claimStateDetail);
		landingClaimCaseBean.setClaimType(claimCase.claimType);
		landingClaimCaseBean.setClientSegment(claimCase.clientSegment);
		landingClaimCaseBean.setOperatingCountry(claimCase.operatingCountry);
		
		Date benefitStartDate = claimCase.benefitStartDate == null ? null : DateHelper.convertToDate(claimCase.benefitStartDate);
		Date benefitEndDate = claimCase.benefitEndDate == null ? null : DateHelper.convertToDate(claimCase.benefitEndDate);
		Date cancellationDate = claimCase.cancellationDate == null ? null : DateHelper.convertToDate(claimCase.cancellationDate);
		Date claimDate = claimCase.claimDate == null ? null :  DateHelper.convertToDate(claimCase.claimDate);
		Date diagnosisDate = claimCase.diagnosisDate == null ? null : DateHelper.convertToDate(claimCase.diagnosisDate);
		Date disabilityDate = claimCase.disabilityDate == null ? null : DateHelper.convertToDate(claimCase.disabilityDate);
		Date hospitalStartDate = claimCase.hospitalStartDate == null ? null : DateHelper.convertToDate(claimCase.hospitalStartDate);
		Date lastDayWorked = claimCase.lastDayWorked == null ? null : DateHelper.convertToDate(claimCase.lastDayWorked);
		Date reactivationDate = claimCase.reactivationDate == null ? null : DateHelper.convertToDate(claimCase.reactivationDate);
		Date reportingDate = claimCase.reportingDate == null ? null : DateHelper.convertToDate(claimCase.reportingDate);
		Date returnToWorkDate = claimCase.returnToWorkDate == null ? null : DateHelper.convertToDate(claimCase.returnToWorkDate);
		Date symptomsStartDate = claimCase.symptomsStartDate == null ? null : DateHelper.convertToDate(claimCase.symptomsStartDate);
		Date terminationDate = claimCase.terminationDate == null ? null : DateHelper.convertToDate(claimCase.terminationDate);
		Date treatmentStartDate = claimCase.treatmentStartDate == null ? null : DateHelper.convertToDate(claimCase.treatmentStartDate);
		
		landingClaimCaseBean.setBenefitStartDate(benefitStartDate);
		landingClaimCaseBean.setBenefitEndDate(benefitEndDate);
		landingClaimCaseBean.setCancellationDate(cancellationDate);
		landingClaimCaseBean.setClaimDate(claimDate);
		landingClaimCaseBean.setDiagnosisDate(diagnosisDate);
		landingClaimCaseBean.setDisabilityDate(disabilityDate);
		landingClaimCaseBean.setHospitalStartDate(hospitalStartDate);
		landingClaimCaseBean.setLastDayWorked(lastDayWorked);
		landingClaimCaseBean.setReactivationDate(reactivationDate);
		landingClaimCaseBean.setReportingDate(reportingDate);
		landingClaimCaseBean.setReturnToWorkDate(returnToWorkDate);
		landingClaimCaseBean.setSymptomsStartDate(symptomsStartDate);
		landingClaimCaseBean.setTerminationdate(terminationDate);
		landingClaimCaseBean.setTreatmentStartDate(treatmentStartDate);
		
		if(claimCase.claimManager != null) {
			landingClaimCaseBean.setClaimManagerUserName(claimCase.claimManager.userName);
			landingClaimCaseBean.setSourceClaimManagerBusId(claimCase.claimManager.partyBusId);
		}
		
		if(claimCase.claimMgmtTeam != null) {
			landingClaimCaseBean.setSourceClaimMgmtTeamBusId(claimCase.claimMgmtTeam.partyBusId);
			landingClaimCaseBean.setClaimMgtmTeamBusId(claimCase.claimMgmtTeam.crmId);
		}
		
		if(claimCase.mainAffiliate != null) {
			landingClaimCaseBean.setSourceMainAffiliateBusId(claimCase.mainAffiliate.partyBusId);
			landingClaimCaseBean.setMainAffiliateBusId(claimCase.mainAffiliate.crmId);
		}
		
		if(claimCase.injuredPerson != null) {
			landingClaimCaseBean.setSourceInsuredPersonBusId(claimCase.injuredPerson.partyBusId);
			landingClaimCaseBean.setInsuredPersonBusId(claimCase.injuredPerson.crmId);
		}
		
		landingClaimCaseBean.setInsuredPersonType(claimCase.insuredPersonType);
		
		Date creationTimeStamp = claimCase.creationTimestamp == null ? null : Date.from(claimCase.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
		Date lastUpdateTimeStamp = claimCase.lastUpdateTimestamp == null ? null : Date.from(claimCase.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
		
		landingClaimCaseBean.setCreationTimeStamp(creationTimeStamp);
		landingClaimCaseBean.setLastUpdateTimeStamp(lastUpdateTimeStamp);
		landingClaimCaseBean.setAction(claimCase.action);
		landingClaimCaseBean.setActionBy(synchroniseClaimCase.context.userId);
		landingClaimCaseBean.setSourceSystem(synchroniseClaimCase.context.sourceSystemId);
		
		return landingClaimCaseBean;
	}
}