package com.elipslife.mdm.contract.staging;

import java.util.Date;
import java.util.HashMap;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class StagingExposureWriter extends AbstractTableWriter<StagingExposureBean> {

	private StagingExposureReader stagingExposureReader;
	
	public StagingExposureWriter() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public StagingExposureWriter(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._STG_Exposure.getPathInSchema()));
	}
	
	public StagingExposureWriter(AdaptationTable table) {
		super(table);
		
		this.stagingExposureReader = new StagingExposureReader(table);
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, StagingExposureBean bean) {
		
		values.put(ContractPaths._STG_Exposure._Exposure_ExposureId, bean.getId());
		values.put(ContractPaths._STG_Exposure._Exposure_ExposureBusId, bean.getExposureBusId());
		values.put(ContractPaths._STG_Exposure._Exposure_Name, bean.getName());
		values.put(ContractPaths._STG_Exposure._Exposure_ExposureMaster, bean.getExposureMaster());
		values.put(ContractPaths._STG_Exposure._Exposure_ProductBusId, bean.getProductBusId());
		values.put(ContractPaths._STG_Exposure._Exposure_ProductGroup, bean.getProductGroup());
		values.put(ContractPaths._STG_Exposure._Exposure_ProductName, bean.getProductName());
		values.put(ContractPaths._STG_Exposure._Exposure_SourceProductBusId, bean.getSourceProductBusId());
		
		values.put(ContractPaths._STG_Exposure._Control_SourceSystem, bean.getSourceSystem());
		values.put(ContractPaths._STG_Exposure._Control_SourceContractBusId, bean.getSourceContractBusId());
		values.put(ContractPaths._STG_Exposure._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(ContractPaths._STG_Exposure._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(ContractPaths._STG_Exposure._Control_LastSyncTimestamp, new Date());
		values.put(ContractPaths._STG_Exposure._Control_Action, bean.getAction());
		values.put(ContractPaths._STG_Exposure._Control_ActionBy, bean.getActionBy());
	}

	@Override
	public AbstractTableReader<StagingExposureBean> getTableReader() {
		return this.stagingExposureReader;
	}
}