package com.elipslife.ebx.data.access;

import java.util.*;
import java.util.Map.*;

import com.onwbp.adaptation.*;
import com.orchestranetworks.presales.toolbox.procedure.RecordValuesBean;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;

/**
 * The Class UpdateRecordsProcedure.
 *
 *
 */
public class UpdateRecords implements Procedure
{
	private List<RecordValuesBean> recordDefinitions;

	/**
	 * Instantiates a new update records procedure.
	 */
	public UpdateRecords()
	{
		super();
	}

	/**
	 * Instantiates a new update records procedure.
	 *
	 * @param recordDefinitions the record definitions
	 */
	public UpdateRecords(final List<RecordValuesBean> recordDefinitions)
	{
		super();
		this.recordDefinitions = recordDefinitions;
	}

	/**
	 * Instantiates a new update records procedure.
	 *
	 * @param recordDefinition the record definition
	 */
	public UpdateRecords(final RecordValuesBean recordDefinition)
	{
		super();
		this.recordDefinitions = new ArrayList<RecordValuesBean>();
		this.recordDefinitions.add(recordDefinition);
	}

	/**
	 * Execute.
	 *
	 * @param pContext the context
	 * @throws Exception the exception
	 */
	@Override
	public void execute(final ProcedureContext pContext) throws Exception
	{
		if (this.recordDefinitions == null)
		{
			return;
		}

		for (RecordValuesBean recordDefinition : this.recordDefinitions)
		{
			Adaptation record = recordDefinition.getRecord();
			if (record == null)
			{
				continue;
			}
			HashMap<Path, Object> values = recordDefinition.getValues();
			ValueContextForUpdate valueContext = pContext.getContext(record.getAdaptationName());
			this.setValues(valueContext, values);
			pContext.setAllPrivileges(true);
			pContext.doModifyContent(record, valueContext);
			pContext.setAllPrivileges(false);
		}
	}

	/**
	 * Gets the record definitions.
	 *
	 * @return the record definitions
	 */
	public List<RecordValuesBean> getRecordDefinitions()
	{
		return this.recordDefinitions;
	}

	/**
	 * Sets the record definition.
	 *
	 * @param recordDefinition the new record definitions
	 */
	public void setRecordDefinition(final RecordValuesBean recordDefinition)
	{
		this.recordDefinitions = new ArrayList<RecordValuesBean>();
		this.recordDefinitions.add(recordDefinition);
	}

	/**
	 * Sets the record definitions.
	 *
	 * @param recordDefinitions the new record definitions
	 */
	public void setRecordDefinitions(final List<RecordValuesBean> recordDefinitions)
	{
		this.recordDefinitions = recordDefinitions;
	}

	/**
	 * Sets the values.
	 *
	 * @param pContext the context
	 * @param pValues the values
	 */
	private void setValues(
		final ValueContextForUpdate pContext,
		final HashMap<Path, Object> pValues)
	{
		if (pValues != null)
		{
			Iterator<Entry<Path, Object>> iterator = pValues.entrySet().iterator();
			while (iterator.hasNext())
			{
				Entry<Path, Object> valueDef = iterator.next();
				Path path = valueDef.getKey();
				Object value = valueDef.getValue();
				pContext.setValue(value, path);
			}
		}
	}

}
