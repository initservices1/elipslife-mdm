package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.BankAccount;
import com.elipslife.ebx.api.rest.models.Party;
import com.elipslife.ebx.api.rest.models.SynchroniseParty;
import com.elipslife.ebx.domain.bean.LandingBankAccountBulkProcessBean;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class BankAccountBulkProcessConverter extends AbstractConverter<LandingBankAccountBulkProcessBean[], SynchroniseParty> {

    @Override
    public LandingBankAccountBulkProcessBean[] convert(SynchroniseParty synchroniseParty) throws DatatypeConfigurationException {

        ArrayList<LandingBankAccountBulkProcessBean> landingBankAccountBeans = new ArrayList<>();

        for (Party party : synchroniseParty.data) {

            for (BankAccount bankAccount : (party.bankAccounts == null ? new ArrayList<BankAccount>() : party.bankAccounts)) {

                LandingBankAccountBulkProcessBean landingBankAccountBean = new LandingBankAccountBulkProcessBean();
                landingBankAccountBeans.add(landingBankAccountBean);

                Date creationTimeStampBankAccount = bankAccount.creationTimestamp == null ? null
                    : Date.from(bankAccount.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
                Date lastUpdateTimestampBankAccount = bankAccount.lastUpdateTimestamp == null ? null
                    : Date.from(bankAccount.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
                Date validFromBankAccount = bankAccount.validFrom == null ? null : DateHelper.convertToDate(bankAccount.validFrom);
                Date validToBankAccount = bankAccount.validTo == null ? null : DateHelper.convertToDate(bankAccount.validTo);

                landingBankAccountBean.setAccountType(bankAccount.accountType);
                landingBankAccountBean.setAccountNumber(bankAccount.accountNumber);
                landingBankAccountBean.setIban(bankAccount.iban);
                landingBankAccountBean.setBic(bankAccount.bic);
                landingBankAccountBean.setClearingNumber(bankAccount.clearingNumber);
                landingBankAccountBean.setBankName(bankAccount.bankName);
                landingBankAccountBean.setCountry(bankAccount.country);
                landingBankAccountBean.setCurrency(bankAccount.currency);
                landingBankAccountBean.setIsMainAccount(bankAccount.isMainAccount);
                landingBankAccountBean.setSourceBankAccountBusId(bankAccount.sourceBankAccountBusId);
                landingBankAccountBean.setSourceSystem(synchroniseParty.context.sourceSystemId);
                landingBankAccountBean.setSourcePartyBusId(party.partyBusId);
                landingBankAccountBean.setValidFrom(validFromBankAccount);
                landingBankAccountBean.setValidTo(validToBankAccount);
                landingBankAccountBean.setCreationTimestamp(creationTimeStampBankAccount);
                landingBankAccountBean.setLastUpdateTimestamp(lastUpdateTimestampBankAccount);
                landingBankAccountBean.setAction(bankAccount.action);
                landingBankAccountBean.setActionBy(synchroniseParty.context.userId);
            }
        }

        return landingBankAccountBeans.toArray(new LandingBankAccountBulkProcessBean[0]);
    }
}
