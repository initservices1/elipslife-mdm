package com.elipslife.ebx.api.rest.models;

import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {
	
	@JsonbProperty("ProductBusId")
	@JsonProperty("ProductBusId")
	public String productBusId;
	
	@JsonbProperty("ProductGroup")
	@JsonProperty("ProductGroup")
	public String productGroup;
	
	@JsonbProperty("ProductName")
	@JsonProperty("ProductName")
	public String productName;
}