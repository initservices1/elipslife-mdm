package com.elipslife.mdm.contract.core;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class CoreContractCoverBean extends Entity<Integer> {
	
	private String exposureId;
	private String exposureBusId;
	private String coverBusId;
	private String coverGroupType;
	private String coverGroupTypeName;
	private String coverName;
	private String coverTemplateBusId;
	private String beneficiaryType;
	
	private String sourceSystem;
	private String createdBy;
	private Date creationTimestamp;
	private String lastUpdatedBy;
	private Date lastUpdateTimestamp;
	private String lastSyncAction;
	private Date lastSyncTimestamp;
	
	
	public String getExposureId() {
		return exposureId;
	}
	public void setExposureId(String exposureId) {
		this.exposureId = exposureId;
	}
	public String getExposureBusId() {
		return exposureBusId;
	}
	public void setExposureBusId(String exposureBusId) {
		this.exposureBusId = exposureBusId;
	}
	public String getCoverBusId() {
		return coverBusId;
	}
	public void setCoverBusId(String coverBusId) {
		this.coverBusId = coverBusId;
	}
	public String getCoverGroupType() {
		return coverGroupType;
	}
	public void setCoverGroupType(String coverGroupType) {
		this.coverGroupType = coverGroupType;
	}
	public String getCoverGroupTypeName() {
		return coverGroupTypeName;
	}
	public void setCoverGroupTypeName(String coverGroupTypeName) {
		this.coverGroupTypeName = coverGroupTypeName;
	}
	public String getCoverName() {
		return coverName;
	}
	public void setCoverName(String coverName) {
		this.coverName = coverName;
	}
	public String getCoverTemplateBusId() {
		return coverTemplateBusId;
	}
	public void setCoverTemplateBusId(String coverTemplateBusId) {
		this.coverTemplateBusId = coverTemplateBusId;
	}
	public String getBeneficiaryType() {
		return beneficiaryType;
	}
	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getLastSyncAction() {
		return lastSyncAction;
	}
	public void setLastSyncAction(String lastSyncAction) {
		this.lastSyncAction = lastSyncAction;
	}
	public Date getLastSyncTimestamp() {
		return lastSyncTimestamp;
	}
	public void setLastSyncTimestamp(Date lastSyncTimestamp) {
		this.lastSyncTimestamp = lastSyncTimestamp;
	}
}