package com.elipslife.mdm.party.trigger;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import com.elipslife.ebx.data.access.CorePartyBuilder;
import com.elipslife.ebx.data.access.StagingPartyReader;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.boot.VM;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.schema.trigger.ValueChange;
import com.orchestranetworks.service.OperationException;

public class StagingPartyTrigger extends TableTrigger {

    @Override
    public void setup(TriggerSetupContext aContext) {

    }

    @Override
    public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
        super.handleBeforeCreate(aContext);

        aContext.getOccurrenceContextForUpdate().setValue(new Date(), PartyPaths._STG_Party._Control_LastSyncTimestamp);

    }
    
    @Override
    public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
        super.handleBeforeModify(aContext);
        
        aContext.getOccurrenceContextForUpdate().setValue(new Date(), PartyPaths._STG_Party._Control_LastSyncTimestamp);
    }

    @Override
    public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {

        super.handleAfterCreate(aContext);

        boolean rootDataSpace = Objects.equals(PartyConstants.DataSpace.PARTY_REFERENCE, aContext.getAdaptationHome().getKey().getName());
        if (rootDataSpace) {
        	
        	StagingPartyBean stagingPartyBean = new StagingPartyReader().createBeanFromRecord(aContext.getAdaptationOccurrence());
            boolean isWithinRange = dateWithinRange(stagingPartyBean.getValidFrom(), stagingPartyBean.getValidTo());
            if (!isWithinRange) {  // check if staging party is valid or not, If not valid, stop the process here
                VM.log.kernelDebug("Staging Party is either future party or a not valid party. Party sourceSystem: "+stagingPartyBean.getSourceSystem() +
                		" sourcePartyBusId: "+stagingPartyBean.getSourcePartyBusId()+ " validFrom: "+stagingPartyBean.getValidFrom()+ " validTo: "+stagingPartyBean.getValidTo());
            	return;
            }
            
            // Create or Update staging addresses by landing addresses by party
            PartyBuilder.createOrUpdateStagingAddressesByLandingAddresses(aContext.getProcedureContext(), stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId());
            
            // Create or Update staging bank accounts by landing bank accounts by party
            PartyBuilder.createOrUpdateStagingBankAccountsByLandingBankAccounts(aContext.getProcedureContext(), stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId());
            
            boolean hasValidationErrors = hasValidationErrors(stagingPartyBean);
            if (hasValidationErrors) {
                return;
            }

            try {
                CorePartyBuilder.createOrUpdateCorePartyByStagingParty(aContext.getProcedureContext(), stagingPartyBean);
                
                // Create or Update core addresses by staging addresses by party
                PartyBuilder.createOrUpdateCoreAddressByStagingAddress(aContext.getProcedureContext(), stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId());
                
                // Create or Update staging bank accounts by landing bank accounts by party
                PartyBuilder.createOrUpdateCoreBankAccountByStagingBankAccount(aContext.getProcedureContext(), stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId());
                
            } catch (Exception e) {
                throw OperationException.createError(e);
            }
        }
    }

    @Override
    public void handleAfterModify(AfterModifyOccurrenceContext aContext) throws OperationException {

        super.handleAfterModify(aContext);

        boolean rootDataSpace = Objects.equals(PartyConstants.DataSpace.PARTY_REFERENCE, aContext.getAdaptationHome().getKey().getName());
        
        if (rootDataSpace) {
        	
            // if the core party value changes only, then skip remaining migration process
        	ValueChange valueChange = aContext.getChanges().getChange(PartyPaths._STG_Party._Party_CorePartyId);
        	if(valueChange != null) {
        		return;
        	}
        	
            StagingPartyBean stagingPartyBean = new StagingPartyReader().createBeanFromRecord(aContext.getAdaptationOccurrence());
            boolean isWithinRange = dateWithinRange(stagingPartyBean.getValidFrom(), stagingPartyBean.getValidTo());
            if (!isWithinRange) {
            	VM.log.kernelDebug("Staging Party is either future party or a not valid party. Party sourceSystem: "+stagingPartyBean.getSourceSystem() +
                		" sourcePartyBusId: "+stagingPartyBean.getSourcePartyBusId()+ " validFrom: "+stagingPartyBean.getValidFrom()+ " validTo: "+stagingPartyBean.getValidTo());
                return;
            }
                        
            // Create or Update staging addresses by landing addresses by party
            PartyBuilder.createOrUpdateStagingAddressesByLandingAddresses(aContext.getProcedureContext(), stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId());
            
            // Create or Update staging bank accounts by landing bank accounts by party
            PartyBuilder.createOrUpdateStagingBankAccountsByLandingBankAccounts(aContext.getProcedureContext(), stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId());

            boolean hasValidationErrors = hasValidationErrors(stagingPartyBean);
            if (hasValidationErrors) {
                return;
            }

            try {
                CorePartyBuilder.createOrUpdateCorePartyByStagingParty(aContext.getProcedureContext(), stagingPartyBean);
                
                // Create or Update core addresses by staging addresses by party
                PartyBuilder.createOrUpdateCoreAddressByStagingAddress(aContext.getProcedureContext(), stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId());
                
                // Create or Update staging bank accounts by landing bank accounts by party
                PartyBuilder.createOrUpdateCoreBankAccountByStagingBankAccount(aContext.getProcedureContext(), stagingPartyBean.getSourceSystem(), stagingPartyBean.getSourcePartyBusId());
                
            } catch (Exception e) {
                throw OperationException.createError(e);
            }
        }
    }

    StagingPartyBean stagingPartyBeanToDelete;

    @Override
    public void handleBeforeDelete(BeforeDeleteOccurrenceContext aContext) throws OperationException {

        super.handleBeforeDelete(aContext);

        this.stagingPartyBeanToDelete = new StagingPartyReader().createBeanFromRecord(aContext.getAdaptationOccurrence());
    }


    @Override
    public void handleAfterDelete(AfterDeleteOccurrenceContext aContext) throws OperationException {

        super.handleAfterDelete(aContext);

        boolean rootDataSpace = Objects.equals(PartyConstants.DataSpace.PARTY_REFERENCE, aContext.getAdaptationHome().getKey().getName());

        if (rootDataSpace) {
        	boolean isWithinRange = dateWithinRange(this.stagingPartyBeanToDelete.getValidFrom(), this.stagingPartyBeanToDelete.getValidTo());

            if (!isWithinRange) {
                return;
            }

            CorePartyBuilder.deleteCorePartyByStagingParty(aContext.getProcedureContext(), stagingPartyBeanToDelete);
        }
        
    }

    /**
     * 
     * @param dateFrom
     * @param dateTo
     * @return
     */
    private static boolean dateWithinRange(Date dateFrom, Date dateTo) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date dateNow = calendar.getTime();

        Date dateMinimum = new Date(Long.MIN_VALUE);
        Date dateMaximum = new Date(Long.MAX_VALUE);

        return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0 &&
            dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
    }


    /**
     * 
     * @param stagingPartyBean
     * @return
     */
    private static boolean hasValidationErrors(StagingPartyBean stagingPartyBean) {

        StagingPartyReader stagingPartyReader = new StagingPartyReader();

        boolean hasValidationErrors = stagingPartyReader.hasValidationErrors(new Object[] {stagingPartyBean.getId()});

        return hasValidationErrors;
    }
}
