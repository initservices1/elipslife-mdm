package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.json.bind.annotation.JsonbProperty;

public class BankAccount {
	
	@JsonbProperty("AccountType")
	public String accountType;

	@JsonbProperty("AccountNumber")
	public String accountNumber;
	
	@JsonbProperty("IBAN")
	public String iban;
	
	@JsonbProperty("BIC")
	public String bic;
	
	@JsonbProperty("ClearingNumber")
	public String clearingNumber;
	
	@JsonbProperty("BankName")
	public String bankName;
	
	@JsonbProperty("Country")
	public String country;
	
	@JsonbProperty("Currency")
	public String currency;
	
	@JsonbProperty("IsMainAccount")
	public Boolean isMainAccount;
	
	@JsonbProperty("SourceBankAccountBusId")
	public String sourceBankAccountBusId;
	
	@JsonbProperty("ValidFrom")
	public LocalDate validFrom;
	
	@JsonbProperty("ValidTo")
	public LocalDate validTo;
	
	@JsonbProperty("CreationTimestamp")
	public LocalDateTime creationTimestamp;
	
	@JsonbProperty("LastUpdateTimestamp")
	public LocalDateTime lastUpdateTimestamp;
	
	@JsonbProperty("Action")
	public String action;
}