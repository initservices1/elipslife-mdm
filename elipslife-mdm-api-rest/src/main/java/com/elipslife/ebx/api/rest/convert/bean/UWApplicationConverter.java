package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.lang3.StringUtils;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.SynchroniseUWApplication;
import com.elipslife.ebx.api.rest.models.UWApplication;
import com.elipslife.mdm.uw.landing.LandingUWApplicationBean;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class UWApplicationConverter extends AbstractConverter<LandingUWApplicationBean, SynchroniseUWApplication> {

	@Override
	public LandingUWApplicationBean convert(SynchroniseUWApplication synchroniseObject)throws DatatypeConfigurationException {

		ArrayList<LandingUWApplicationBean> landingUWApplicationBeans = new ArrayList<>();

		UWApplication uwApplication = synchroniseObject.data;
		LandingUWApplicationBean applicationBean = new LandingUWApplicationBean();
		
		// format Date values
		Date mainAffiliateDateOfBirth = uwApplication.mainAffiliateDateOfBirth == null ? null : DateHelper.convertToDate(uwApplication.mainAffiliateDateOfBirth);
		Date submissionDate = uwApplication.submissionDate == null ? null : DateHelper.convertToDate(uwApplication.submissionDate);
		Date creationTimeStamp = uwApplication.creationTimestamp == null ? null : Date.from(uwApplication.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
		Date lastUpdateTimeStamp = uwApplication.lastUpdateTimestamp == null ? null : Date.from(uwApplication.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
		Date deletionTimeStamp = uwApplication.deletionTimestamp == null ? null : Date.from(uwApplication.deletionTimestamp.atZone(ZoneId.systemDefault()).toInstant());
		Date sfSignedDate = uwApplication.sfSignedDate == null ? null : DateHelper.convertToDate(uwApplication.sfSignedDate);
		
		if(StringUtils.isBlank(uwApplication.dataOwner)) {
			applicationBean.setDataOwner("0");
		} else {
			applicationBean.setDataOwner(uwApplication.dataOwner);	}
		applicationBean.setUWApplicationBusId(uwApplication.uwApplicationBusId);
		applicationBean.setSourceSystem(synchroniseObject.context.sourceSystemId);
		applicationBean.setOperatingCountry(uwApplication.operatingCountry);
		applicationBean.setPolicyholderBusId(uwApplication.policyholderBusId);
		applicationBean.setPolicyholderName(uwApplication.policyholderName);
		applicationBean.setMainAffiliateBusId(uwApplication.mainAffiliateBusId);
		applicationBean.setMainAffiliateFirstName(uwApplication.mainAffiliateFirstName);
		applicationBean.setMainAffiliateLastName(uwApplication.mainAffiliateLastName);
		applicationBean.setMainAffiliateDateOfBirth(mainAffiliateDateOfBirth);
		applicationBean.setMainAffiliateSSN(uwApplication.mainAffiliateSSN);
		applicationBean.setMainAffiliateTown(uwApplication.mainAffiliateTown);
		applicationBean.setMainAffiliatePostCode(uwApplication.mainAffiliatePostCode);
		applicationBean.setMainAffiliateCountry(uwApplication.mainAffiliateCountry);
		applicationBean.setSubmissionDate(submissionDate);
		if(uwApplication.isActive == null) {
			applicationBean.setIsActive(true);
		}else {
		applicationBean.setIsActive(uwApplication.isActive);	}
		applicationBean.setDocSignId(uwApplication.docSignId);
		applicationBean.setSFSignedDate(sfSignedDate);
		applicationBean.setCreatedBy(uwApplication.createdBy);
		applicationBean.setCreationTimeStamp(creationTimeStamp);
		applicationBean.setLastUpdatedBy(uwApplication.lastUpdatedBy);
		applicationBean.setLastUpdateTimeStamp(lastUpdateTimeStamp);
		applicationBean.setDeletionTimeStamp(deletionTimeStamp);
		applicationBean.setAction(uwApplication.action);
		landingUWApplicationBeans.add(applicationBean);

		return applicationBean;
	}

}
