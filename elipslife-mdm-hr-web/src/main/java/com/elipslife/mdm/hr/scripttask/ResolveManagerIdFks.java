package com.elipslife.mdm.hr.scripttask;

import java.util.List;

import com.elipslife.mdm.hr.common.HrCommonRules;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.elipslife.mdm.hr.core.EmployeeBean;
import com.elipslife.mdm.hr.core.EmployeeReader;
import com.elipslife.mdm.hr.core.EmployeeWriter;
import com.elipslife.mdm.hr.core.EmployeeWriterSession;
import com.elipslife.mdm.hr.core.OrganisationBean;
import com.elipslife.mdm.hr.core.OrganisationReader;
import com.elipslife.mdm.hr.core.OrganisationWriter;
import com.elipslife.mdm.hr.core.OrganisationWriterSession;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class ResolveManagerIdFks extends ScriptTaskBean {

	private String tempDataspace;
	private String tablePath;

	@Override
	public void executeScript(ScriptTaskBeanContext aContext) throws OperationException {
		
		// Organisation table FKs
		if(tablePath != null && tablePath.equals(HrModelPaths._Organisation.getPathInSchema().format()))
		{
			OrganisationWriter organisationWriter = new OrganisationWriterSession(aContext.getSession(), tempDataspace);
			OrganisationReader oranisationReader = (OrganisationReader) organisationWriter.getTableReader();
			List<OrganisationBean> organisationBeans = oranisationReader.findEmptyManagerIdsOrEmptyParentIds();
			for (OrganisationBean organisationRecord : organisationBeans) {
				if(organisationRecord.getManagerBusId() != null && organisationRecord.getManagerId() == null) {
					String managerBusId = organisationRecord.getManagerBusId();
					String employeeId = HrCommonRules.findManagerIdFromEmployee(managerBusId , tempDataspace);
					organisationRecord.setManagerId(employeeId);
					
				}
				
				// Update missing parent ids from parent busid
				if(organisationRecord.getParentBusId() != null && organisationRecord.getParentId() == null) {
					String parentId = HrCommonRules.findOrganisationParentIdByParentBusId(organisationRecord.getParentBusId(), tempDataspace);
					organisationRecord.setParentId(parentId);
				}
				organisationWriter.updateBean(organisationRecord);
			}
		} 
		
		// Employee table FKs
		else if(tablePath != null && tablePath.equals(HrModelPaths._Employee.getPathInSchema().format())) {
			
			EmployeeWriter employeeWriter = new EmployeeWriterSession(aContext.getSession(), tempDataspace);
			EmployeeReader employeeReader = (EmployeeReader) employeeWriter.getTableReader();
			List<EmployeeBean> unresolvedEmps = employeeReader.findUnresolvedLineMngsAndDottedLineMngs();
			for(EmployeeBean employeeBean : unresolvedEmps)
			{
				// Employee Line Manager id
				if(employeeBean.getLineManagerBusId() != null && employeeBean.getLineManagerId() == null)
				{
					String lineManagerBusId = employeeBean.getLineManagerBusId();
					List<EmployeeBean> employeeBeans = employeeReader.findBeansByEmployeeBusId(lineManagerBusId);
					String employeeId = employeeBeans.isEmpty() ? null : employeeBeans.get(0).getEmployeeId();
					employeeBean.setLineManagerId(employeeId);
				}
				
				// Employee Dotted Line Manager id
				if(employeeBean.getDottedLineManagerBusId() != null && employeeBean.getDottedLineManagerId() == null)
				{
					String dottedLineManagerBusId = employeeBean.getDottedLineManagerBusId();
					List<EmployeeBean> employeeBeans = employeeReader.findBeansByEmployeeBusId(dottedLineManagerBusId);
					String employeeId = employeeBeans.isEmpty() ? null : employeeBeans.get(0).getEmployeeId();
					employeeBean.setDottedLineManagerId(employeeId);
				}
				employeeWriter.updateBean(employeeBean);
			}
		}
	}

	public String getTempDataspace() {
		return tempDataspace;
	}

	public void setTempDataspace(String tempDataspace) {
		this.tempDataspace = tempDataspace;
	}

	public String getTablePath() {
		return tablePath;
	}

	public void setTablePath(String tablePath) {
		this.tablePath = tablePath;
	}

}
