package com.elipslife.mdm.hr.core;

import java.util.List;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;


public class OrganisationReader extends AbstractTableReader<OrganisationBean> {

    public OrganisationReader() {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._Organisation.getPathInSchema()));
    }
    
    public OrganisationReader(String dataspace) {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._Organisation.getPathInSchema()));
    }

    public OrganisationReader(AdaptationTable table) {
    	super(table);
    }

    @Override
    public OrganisationBean createBeanFromRecord(Adaptation record) {
        OrganisationBean bean = new OrganisationBean();
        bean.setOrganisationId(record.getString(HrModelPaths._Organisation._Organisation_OrganisationId));
        bean.setDataOwner(record.getString(HrModelPaths._Organisation._Organisation_DataOwner));
        bean.setOrganisationBusId(record.getString(HrModelPaths._Organisation._Organisation_OrganisationBusId));
        bean.setOrganisationType(record.getString(HrModelPaths._Organisation._Organisation_OrganisationType));
        bean.setParentId(record.getString(HrModelPaths._Organisation._Organisation_ParentId));
        bean.setParentBusId(record.getString(HrModelPaths._Organisation._Organisation_ParentBusId));
        bean.setName(record.getString(HrModelPaths._Organisation._Organisation_Name));
        bean.setManagerId(record.getString(HrModelPaths._Organisation._Organisation_ManagerId));
        bean.setManagerBusId(record.getString(HrModelPaths._Organisation._Organisation_ManagerBusId));
        bean.setBuildingId(record.getString(HrModelPaths._Organisation._Organisation_BuildingId));
        bean.setBuildingBusId(record.getString(HrModelPaths._Organisation._Organisation_BuildingBusId));
        bean.setIsActive(record.get_boolean(HrModelPaths._Organisation._Organisation_IsActive));
        bean.setSourceSystem(record.getString(HrModelPaths._Organisation._Control_SourceSystem));
        bean.setCreatedBy(record.getString(HrModelPaths._Organisation._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(HrModelPaths._Organisation._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(HrModelPaths._Organisation._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(HrModelPaths._Organisation._Control_LastUpdateTimestamp));
        
		return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{HrModelPaths._Organisation._Organisation_OrganisationId};
    }

    @Override
    public Object[] retrievePks(OrganisationBean bean) {
        return new Object[]{bean.getOrganisationId()};
    }
    
    public List<OrganisationBean> findBeansByOrganisationBusIdAndName(String organisationBusId, String name) {
    	
    	XPathPredicate predicate1 = new XPathPredicate(Operation.Equals, HrModelPaths._Organisation._Organisation_OrganisationBusId, organisationBusId);
    	XPathPredicate predicate2 = new XPathPredicate(Operation.Equals, HrModelPaths._Organisation._Organisation_Name, name);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate1);
		group.addPredicate(predicate2);
		
		return super.readBeansFor(group.toString());
	}
    
    public List<OrganisationBean> findEmptyManagerIdsOrEmptyParentIds() {
		XPathPredicate predicateForEmptyManagerIds = new XPathPredicate(XPathPredicate.Operation.IsNotNull,
				HrModelPaths._Organisation._Organisation_ManagerBusId, null);
		XPathPredicate predicateForEmptyParentIds = new XPathPredicate(XPathPredicate.Operation.IsNull,
				HrModelPaths._Organisation._Organisation_ParentId, null);
		
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.Or);
		group.addPredicate(predicateForEmptyManagerIds);
		group.addPredicate(predicateForEmptyParentIds);
		
		return super.readBeansFor(group.toString());
	}
    
    public List<OrganisationBean> findBeansByOrganisationBusId(String organisationBusId) {
    	
    	XPathPredicate predicate = new XPathPredicate(Operation.Equals, HrModelPaths._Organisation._Organisation_OrganisationBusId, organisationBusId);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate);
		
		return super.readBeansFor(group.toString());
	}
}
