package com.elipslife.mdm.uw.core;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class UWApplicationWriterProcedureContext extends UWApplicationWriter{
	
	private final ProcedureContext procedureContext;

	public UWApplicationWriterProcedureContext(ProcedureContext procedureContext) {
		super();
		this.procedureContext = procedureContext;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
