package com.elipslife.mdm.party.scheduler;

import java.util.List;

import com.elipslife.ebx.data.access.CorePartyBuilder;
import com.elipslife.ebx.data.access.StagingPartyReader;
import com.elipslife.ebx.data.access.StagingPartyWriter;
import com.elipslife.ebx.data.access.StagingPartyWriterSession;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;

public class MigratePartyFromStagingToCoreTask extends MigrateFromStagingToCoreTaskBase {

	@Override
	public void execute(ScheduledExecutionContext scheduledExecutionContext) {

  		StagingPartyWriter stagingPartyWriter = new StagingPartyWriterSession(scheduledExecutionContext.getSession(), true);
		StagingPartyReader stagingPartyReader = (StagingPartyReader) stagingPartyWriter.getTableReader();

		List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansUpToDate();
		
		stagingPartyBeans.forEach(spb ->
		{
			try {
				CorePartyBuilder.createOrUpdateCorePartyByStagingParty(scheduledExecutionContext.getSession(), spb);
			}
			catch(Exception ex) {
				scheduledExecutionContext.addExecutionInformation(String.format("Migration error for staging party with ID %s:\r\n%s", spb.getId(), ex.toString()));
			}
		});
	}
}