package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.Address;
import com.elipslife.ebx.api.rest.models.Party;
import com.elipslife.ebx.api.rest.models.SynchroniseParty;
import com.elipslife.ebx.domain.bean.LandingAddressBulkProcessBean;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class AddressBulkProcessConverter extends AbstractConverter<LandingAddressBulkProcessBean[], SynchroniseParty> {

    @Override
    public LandingAddressBulkProcessBean[] convert(SynchroniseParty synchroniseParty) throws DatatypeConfigurationException {

        ArrayList<LandingAddressBulkProcessBean> landingAddressBeans = new ArrayList<>();

        for (Party party : synchroniseParty.data) {

            for (Address address : (party.addresses == null ? new ArrayList<Address>() : party.addresses)) {

                LandingAddressBulkProcessBean landingAddressBean = new LandingAddressBulkProcessBean();
                landingAddressBeans.add(landingAddressBean);

                Date creationTimeStampAddress =
                    address.creationTimestamp == null ? null : Date.from(address.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
                Date lastUpdateTimestampAddress =
                    address.lastUpdateTimestamp == null ? null : Date.from(address.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
                Date validFromAddress =
                    address.validFrom == null ? null : DateHelper.convertToDate(address.validFrom);
                Date validToAddress = address.validTo == null ? null : DateHelper.convertToDate(address.validTo);

                landingAddressBean.setAddressType(address.addressType);
                landingAddressBean.setAddressUsage(address.addressUse);
                landingAddressBean.setSourceAddressBusId(address.sourceAddressBusId);
                landingAddressBean.setStreet(address.street);
                landingAddressBean.setStreet2(address.street2);
                landingAddressBean.setPostCode(address.postCode);
                landingAddressBean.setPoBox(address.poBox);
                landingAddressBean.setPoBoxPostCode(address.poBoxPostCode);
                landingAddressBean.setPoBoxTown(address.poBoxTown);
                landingAddressBean.setDistrict(address.district);
                landingAddressBean.setTown(address.town);
                landingAddressBean.setStateProvince(address.stateProvince);
                landingAddressBean.setCountry(address.country);
                landingAddressBean.setElectronicAddressType(address.electronicAddressType);
                landingAddressBean.setAddress(address.address);
                landingAddressBean.setSourceSystem(synchroniseParty.context.sourceSystemId);
                landingAddressBean.setSourcePartyBusId(party.partyBusId);
                landingAddressBean.setValidFrom(validFromAddress);
                landingAddressBean.setValidTo(validToAddress);
                landingAddressBean.setCreationTimestamp(creationTimeStampAddress);
                landingAddressBean.setLastUpdateTimestamp(lastUpdateTimestampAddress);
                landingAddressBean.setAction(address.action);
                landingAddressBean.setActionBy(synchroniseParty.context.userId);
            }
        }

        return landingAddressBeans.toArray(new LandingAddressBulkProcessBean[0]);
    }
}
