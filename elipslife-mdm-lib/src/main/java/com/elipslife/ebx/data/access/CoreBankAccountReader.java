package com.elipslife.ebx.data.access;

import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.CoreBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.xpath.filter.XPathPredicateGroup;

public class CoreBankAccountReader extends BaseBankAccountReader<CoreBankAccountBean> {

	public CoreBankAccountReader() {
		super(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
				PartyPaths._BankAccount.getPathInSchema()));
	}
	
	public CoreBankAccountReader(String dataSpace) {
        super(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.PARTY_MASTER),
                PartyPaths._BankAccount.getPathInSchema()));
    }
	
	public CoreBankAccountReader(AdaptationTable table) {
		super(table);
	}

	
	@Override
	public CoreBankAccountBean createBeanFromRecord(Adaptation record) {
		
		CoreBankAccountBean coreBankAccountBean = new CoreBankAccountBean();
		
		coreBankAccountBean.setId(record.get_int(PartyPaths._BankAccount._Account_BankAccountId));
		
		coreBankAccountBean.setPartyId(record.getString(PartyPaths._BankAccount._Account_PartyId));
		coreBankAccountBean.setAccountType(record.getString(PartyPaths._BankAccount._Account_AccountType));
		coreBankAccountBean.setAccountNumber(record.getString(PartyPaths._BankAccount._Account_AccountNumber));
		coreBankAccountBean.setIban(record.getString(PartyPaths._BankAccount._Account_IBAN));
		coreBankAccountBean.setBic(record.getString(PartyPaths._BankAccount._Account_BIC));
		coreBankAccountBean.setClearingNumber(record.getString(PartyPaths._BankAccount._Account_ClearingNumber));
		coreBankAccountBean.setBankName(record.getString(PartyPaths._BankAccount._Account_BankName));
		coreBankAccountBean.setCountry(record.getString(PartyPaths._BankAccount._Account_Country));
		coreBankAccountBean.setCurrency(record.getString(PartyPaths._BankAccount._Account_Currency));
		coreBankAccountBean.setIsMainAccount((Boolean)record.get(PartyPaths._BankAccount._Account_IsMainAccount));
		coreBankAccountBean.setSourceBankAccountBusId(record.getString(PartyPaths._BankAccount._Account_SourceBankAccountBusId));
		coreBankAccountBean.setSourceSystem(record.getString(PartyPaths._BankAccount._Control_SourceSystem));
		coreBankAccountBean.setSourcePartyBusId(record.getString(PartyPaths._BankAccount._Control_SourcePartyBusId));
		coreBankAccountBean.setCreatedBy(record.getString(PartyPaths._BankAccount._Control_CreatedBy));
		coreBankAccountBean.setCreationTimestamp(record.getDate(PartyPaths._BankAccount._Control_CreationTimestamp));
		coreBankAccountBean.setLastUpdatedBy(record.getString(PartyPaths._BankAccount._Control_LastUpdatedBy));
		coreBankAccountBean.setLastUpdateTimestamp(record.getDate(PartyPaths._BankAccount._Control_LastUpdateTimestamp));
		coreBankAccountBean.setLastSyncAction(record.getString(PartyPaths._BankAccount._Control_LastSyncAction));
		coreBankAccountBean.setLastSyncTimestamp(record.getDate(PartyPaths._BankAccount._Control_LastSyncTimestamp));
		coreBankAccountBean.setValidFrom(record.getDate(PartyPaths._BankAccount._Control_ValidFrom));
		coreBankAccountBean.setValidTo(record.getDate(PartyPaths._BankAccount._Control_ValidTo));
		coreBankAccountBean.setIsExpired((Boolean)record.get(PartyPaths._BankAccount._Control_IsExpired));
		
		return coreBankAccountBean;
	}
	

	@Override
	protected Path[] getPkPaths() {
		
		return new Path[] { PartyPaths._STG_BankAccount._Account_BankAccountId };
	}
	

	@Override
	protected Object[] retrievePks(CoreBankAccountBean bean) {

		return new Object[] { bean.getId() };
	}
	
	
	/**
	 * Finds beans by party ID
	 * @param partyId
	 * @return
	 */
	public List<CoreBankAccountBean> findBeansByPartyId(int partyId) {
		
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._BankAccount._Account_PartyId, String.valueOf(partyId));
		
		return readRecordsFor(predicate1.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
	/**
	 * Find Core BankAccounts by Sourcesystem and SourcePartyBusid
	 * 
	 */
	public List<CoreBankAccountBean> findBeansBySourceSystemAndPartyBusId(String sourceSystem, String sourceSystemPartyBusId) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._Address._Control_SourceSystem, sourceSystem);
		group.addPredicate(predicate1);
		XPathPredicate predicate2 = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._Address._Control_SourcePartyBusId, sourceSystemPartyBusId);
		group.addPredicate(predicate2);
		
		return readRecordsFor(group.toString()).stream().map(this::createBeanFromRecord).collect(Collectors.toList());
	}

	@Override
	protected Path getPathBankAccountId() {

		return PartyPaths._BankAccount._Account_BankAccountId;
	}
	

	@Override
	protected Path getPathSourceBankAccountBusId() {

		return PartyPaths._BankAccount._Account_SourceBankAccountBusId;
	}

	
	@Override
	protected Path getPathControlSourceSystem() {

		return PartyPaths._BankAccount._Control_SourceSystem;
	}
	

	@Override
	protected Path getPathControlSourcePartyBusId() {

		return null;
	}
	

	@Override
	protected Path getPathControlValidFrom() {

		return PartyPaths._BankAccount._Control_ValidFrom;
	}

	
	@Override
	protected Path getPathControlValidTo() {

		return PartyPaths._BankAccount._Control_ValidTo;
	}
}