Deployment
==========

Precondition
------------
**Azure Client** `az` and **Kubernetes Controller** `kubectl` must be installed.

See [Prepare Local Development Environment](https://butos1.atlassian.net/wiki/spaces/BUT/pages/636190773/AKS+Cluster+Kubernetes)

Install
-------
```bash
chmod +x deployment/etc/*
deployment/etc/install-deployment.sh
```

Uninstall
---------
```bash
deployment/etc/uninstall-deployment.sh
```

Manual Installation
===================

Create new K8s namespace
```bash
kubectl create namespace elipslife-mdm
kubectl config set-context elipslife-mdm --cluster butosAKSCluster --namespace elipslife-mdm --user clusterUser_butos_butosAKSCluster
kubectl config use-context elipslife-mdm
```

Prepare Azure SQL Database
```bash
az sql db create -g butos -s butos-dev \
	--name elipslife-mdm \
	--collation SQL_Latin1_General_CP1_CS_AS \
	--max-size 500MB \
	-c 5 \
	-e Basic \
	--service-objective Basic
```

Initial create ConfigMap
```bash
kubectl create configmap ebx-config --from-file=deployment/target/dockerfile-context/ebx-home/aks/
```

Set dns-name for public-ip
```bash
publicIpId=$(az network public-ip list --query "[?tags.service=='elipslife-mdm/ebx']".id -o tsv)
az network public-ip update --ids ${publicIpId} --dns-name elipslife-mdm-ebx-butos
```

Copy LDAP configuration to EBX repository
```bash
container=$(kubectl get pods -l app=ebx -o jsonpath="{.items[*].metadata.name}")
kubectl cp deployment/target/dockerfile-context/ebx-archives/_ldap-configuration.ebx ${container}:/app/ebx-home/repository/archives/_ldap-configuration.ebx
```

Troubleshooting
---------------

Always set the proper K8s context
```bash
kubectl config use-context elipslife-mdm
```

Check the logs
```bash
kubectl get pods
kubectl logs -f <pod-name>
```

Check the current ebx.properties file
```bash
kubectl get configmap ebx-config -o jsonpath='{.data.ebx\.properties}'
```

Manualy make changes to the ebx.properties
```bash
kubectl create configmap ebx-config --from-file=<ebx-home-folder-containing-the-ebx-properties-file> -o yaml --dry-run | kubectl replace -f -

#e.g. kubectl create configmap ebx-config --from-file=deployment/target/docker-context/ebx-home/aks/ -o yaml --dry-run | kubectl replace -f -
```