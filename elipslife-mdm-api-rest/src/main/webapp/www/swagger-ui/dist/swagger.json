{
	"swagger": "2.0",
	"info": {
		"version": "2.0.0",
		"title": "elipslife-api-rest"
	},
	"host": "localhost:8080",
	"basePath": "/rest/",
	"tags": [
		{
			"name": "v2"
		}
	],
	"schemes": [
		"http"
	],
	"securityDefinitions": {
		"basicAuth": {
			"type": "basic"
		}
	},
	"paths": {
		"/api/v2/synchroniseParty": {
			"post": {
				"tags": [
					"v2"
				],
				"description": "Synchronises changes (i.e. creation and updates) to a master system party entity, in order to ensure that the redundant MDM data is consistent. Parties may be persons, organisations/companies or contact persons covering all party role types (e.g. business partners, insured persons and beneficiaries).",
				"operationId": "synchroniseParty",
				"consumes": [
					"application/json"
				],
				"produces": [
					"application/json"
				],
				"parameters": [
					{
						"in": "body",
						"name": "body",
						"required": true,
						"schema": {
							"type": "object",
							"$ref": "#/definitions/Synchronize party model"
						}
					}
				],
				"responses": {
					"200": {
						"description": "Successful operation"
					},
					"401": {
						"description": "Authentication information is missing or invalid"
					},
					"500": {
						"description": "Internal server error"
					}
				}
			}
		},
		"/api/v2/synchroniseClaimCase": {
			"post": {
				"tags": [
					"v2"
				],
				"description": "This service is used to load and synchronise insurance claim case master data changes (i.e. claim case entity creations and updates) with the MDM by means of a standard service.",
				"operationId": "synchroniseClaimCase",
				"consumes": [
					"application/json"
				],
				"produces": [
					"application/json"
				],
				"parameters": [
					{
						"in": "body",
						"name": "body",
						"required": true,
						"schema": {
							"type": "object",
							"$ref": "#/definitions/Synchronize claim case model"
						}
					}
				],
				"responses": {
					"200": {
						"description": "Successful operation"
					},
					"401": {
						"description": "Authentication information is missing or invalid"
					},
					"500": {
						"description": "Internal server error"
					}
				}
			}
		},
		"/api/v2/synchroniseInsuranceContract": {
			"post": {
				"tags": [
					"v2"
				],
				"description": "This service is used to load and synchronise insurance contract master data changes with the MDM. Insurance Contract data includes contract property attributes, as well as the contract’s related Exposures and their related Contract Covers.",
				"operationId": "synchroniseInsuranceContract",
				"consumes": [
					"application/json"
				],
				"produces": [
					"application/json"
				],
				"parameters": [
					{
						"in": "body",
						"name": "body",
						"required": true,
						"schema": {
							"type": "object",
							"$ref": "#/definitions/Synchronize insurance contract model"
						}
					}
				],
				"responses": {
					"200": {
						"description": "Successful operation"
					},
					"401": {
						"description": "Authentication information is missing or invalid"
					},
					"500": {
						"description": "Internal server error"
					}
				}
			}
		},
		"/api/v2/synchroniseUWApplication": {
			"post": {
				"tags": [
					"v2"
				],
				"description": "Synchronises changes (i.e. creation and updates) to a master system UW Application entity and its component UW Case(s), in order to ensure that the redundant MDM data is consistent. All the defined UW input data attributes should be sent if available (i.e. not just the changed attribute values).",
				"operationId": "synchroniseUWApplication",
				"consumes": [
					"application/json"
				],
				"produces": [
					"application/json"
				],
				"parameters": [
					{
						"in": "body",
						"name": "body",
						"required": true,
						"schema": {
							"type": "object",
							"$ref": "#/definitions/Synchronize UWApplication model"
						}
					}
				],
				"responses": {
					"200": {
						"description": "Successful operation"
					},
					"401": {
						"description": "Authentication information is missing or invalid"
					},
					"500": {
						"description": "Internal server error"
					}
				}
			}
		}
	},
	"definitions": {
		"Synchronize party model": {
			"type": "object",
			"properties": {
				"Context": {
					"type": "object",
					"properties": {
						"BusinessContext": {
							"type": "object",
							"$ref": "#/definitions/Business context model"
						},
						"Data": {
							"type": "array",
							"items": {
								"type": "object",
								"$ref": "#/definitions/Party model"
							}
						}
					}
				}
			},
			"description": "This model contains all necessary fields for request object in REST service"
		},
		"Synchronize claim case model": {
			"type": "object",
			"properties": {
				"Context": {
					"type": "object",
					"properties": {
						"BusinessContext": {
							"type": "object",
							"$ref": "#/definitions/Business context model"
						},
						"Data": {
							"type": "object",
							"$ref": "#/definitions/Claim case model"
						}
					}
				}
			},
			"description": "This model contains all necessary fields for request object in REST service"
		},
		"Synchronize insurance contract model": {
			"type": "object",
			"properties": {
				"Context": {
					"type": "object",
					"properties": {
						"BusinessContext": {
							"type": "object",
							"$ref": "#/definitions/Business context model"
						},
						"Data": {
							"type": "object",
							"$ref": "#/definitions/Insurance contract model"
						}
					}
				}
			},
			"description": "This model contains all necessary fields for request object in REST service"
		},
		"Synchronize UWApplication model": {
			"type": "object",
			"properties": {
				"Context": {
					"type": "object",
					"properties": {
						"BusinessContext": {
							"type": "object",
							"$ref": "#/definitions/Business context model"
						},
						"Data": {
							"type": "object",
							"$ref": "#/definitions/UWApplication model"
						}
					}
				}
			},
			"description": "This model contains all necessary fields for request object in REST service"
		},
		"Business context model": {
			"type": "object",
			"properties": {
				"UserId": {
					"type": "string"
				},
				"SourceSystemId": {
					"type": "string"
				}
			}
		},
		"Party model": {
			"type": "object",
			"properties": {
				"DataOwner": {
					"type": "string"
				},
				"OperatingCountry": {
					"type": "string"
				},
				"PartyBusId": {
					"type": "string"
				},
				"CRMId": {
					"type": "string"
				},
				"PartyType": {
					"type": "string"
				},
				"RelationshipOwner": {
					"type": "string"
				},
				"DataResponsible": {
					"type": "string"
				},
				"Language": {
					"type": "string"
				},
				"IsActive": {
					"type": "string"
				},
				"LastName": {
					"type": "string"
				},
				"MiddleName": {
					"type": "string"
				},
				"FirstName": {
					"type": "string"
				},
				"DateOfBirth": {
					"type": "string",
					"format": "date"
				},
				"Gender": {
					"type": "string"
				},
				"Title": {
					"type": "string"
				},
				"Salutation": {
					"type": "string"
				},
				"LetterSalutation": {
					"type": "string"
				},
				"CivilStatus": {
					"type": "string"
				},
				"BirthCity": {
					"type": "string"
				},
				"BirthCountry": {
					"type": "string"
				},
				"IdentityDocument": {
					"type": "string"
				},
				"ParentBusId": {
					"type": "string"
				},
				"OrgName": {
					"type": "string"
				},
				"OrgName2": {
					"type": "string"
				},
				"CompanyType": {
					"type": "string"
				},
				"CommercialRegNo": {
					"type": "string"
				},
				"UIDNumber": {
					"type": "string"
				},
				"VATNumber": {
					"type": "string"
				},
				"Industry": {
					"type": "string"
				},
				"SubIndustry": {
					"type": "string"
				},
				"PreferredEmail": {
					"type": "string"
				},
				"PreferredCommChannel": {
					"type": "string"
				},
				"Department": {
					"type": "string"
				},
				"Function": {
					"type": "string"
				},
				"IsSeniorManager": {
					"type": "boolean"
				},
				"IsUltimateBeneficialOwner": {
					"type": "boolean"
				},
				"BORelationship": {
					"type": "string"
				},
				"ContactPersonState": {
					"type": "string"
				},
				"Roles": {
					"type": "string"
				},
				"IsVIPClient": {
					"type": "boolean"
				},
				"Blacklist": {
					"type": "string"
				},
				"PartnerState": {
					"type": "string"
				},
				"CompanyPersonalNr": {
					"type": "string"
				},
				"SocialSecurityNr": {
					"type": "string"
				},
				"SocialSecurityNrType": {
					"type": "string"
				},
				"BrokerRegNo": {
					"type": "string"
				},
				"LineOfBusiness": {
					"type": "string"
				},
				"FinancePartyBusId": {
					"type": "string"
				},
				"PaymentTerms": {
					"type": "string"
				},
				"FinanceClients": {
					"type": "string"
				},
				"CreditorClients": {
					"type": "string"
				},
				"BankAccounts": {
					"type": "array",
					"items": {
						"$ref": "#/definitions/Bank account model"
					}
				},
				"Addresses": {
					"type": "array",
					"items": {
						"$ref": "#/definitions/Address model"
					}
				},
				"ValidFrom": {
					"type": "string",
					"format": "date"
				},
				"ValidTo": {
					"type": "string",
					"format": "date"
				},
				"CreationTimestamp": {
					"type": "string",
					"format": "date"
				},
				"LastUpdateTimestamp": {
					"type": "string",
					"format": "date"
				},
				"Action": {
					"type": "string"
				}
			}
		},
		"Bank account model": {
			"type": "object",
			"properties": {
				"AccountType": {
					"type": "string"
				},
				"AccountNumber": {
					"type": "string"
				},
				"IBAN": {
					"type": "string"
				},
				"BIC": {
					"type": "string"
				},
				"ClearingNumber": {
					"type": "string"
				},
				"BankName": {
					"type": "string"
				},
				"Country": {
					"type": "string"
				},
				"Currency": {
					"type": "string"
				},
				"IsMainAccount": {
					"type": "boolean"
				},
				"SourceBankAccountBusId": {
					"type": "string"
				},
				"ValidFrom": {
					"type": "string",
					"format": "date"
				},
				"ValidTo": {
					"type": "string",
					"format": "date"
				},
				"CreationTimestamp": {
					"type": "string",
					"format": "date"
				},
				"LastUpdateTimestamp": {
					"type": "string",
					"format": "date"
				},
				"Action": {
					"type": "string"
				}
			}
		},
		"Address model": {
			"type": "object",
			"properties": {
				"AddressType": {
					"type": "string"
				},
				"AddressUse": {
					"type": "string"
				},
				"SourceAddressBusId": {
					"type": "string"
				},
				"Street": {
					"type": "string"
				},
				"Street2": {
					"type": "string"
				},
				"POBox": {
					"type": "string"
				},
				"POBoxPostCode": {
					"type": "string"
				},
				"POBoxTown": {
					"type": "string"
				},
				"PostCode": {
					"type": "string"
				},
				"District": {
					"type": "string"
				},
				"Town": {
					"type": "string"
				},
				"StateProvince": {
					"type": "string"
				},
				"Country": {
					"type": "string"
				},
				"ElectronicAddressType": {
					"type": "string"
				},
				"Address": {
					"type": "string"
				},
				"ValidFrom": {
					"type": "string",
					"format": "date"
				},
				"ValidTo": {
					"type": "string",
					"format": "date"
				},
				"CreationTimestamp": {
					"type": "string",
					"format": "date"
				},
				"LastUpdateTimestamp": {
					"type": "string",
					"format": "date"
				},
				"Action": {
					"type": "string"
				}
			}
		},
		"Employee model": {
			"type": "object",
			"properties": {
				"PartyBusId": {
					"type": "string"
				},
				"UserName": {
					"type": "string"
				}
			}
		},
		"Claim case model": {
			"type": "object",
			"properties": {
				"DataOwner": {
					"type": "string"
				},
				"ClaimCaseBusId": {
					"type": "string"
				},
				"ClaimCaseExtRef": {
					"type": "string"
				},
				"ContractNumber": {
					"type": "string"
				},
				"ContractExtRef": {
					"type": "string"
				},
				"IsActive": {
					"type": "boolean"
				},
				"AssessmentState": {
					"type": "string"
				},
				"BusinessScope": {
					"type": "string"
				},
				"ClaimCause": {
					"type": "string"
				},
				"ClaimState": {
					"type": "string"
				},
				"ClaimStateDetail": {
					"type": "string"
				},
				"ClaimType": {
					"type": "string"
				},
				"ClientSegment": {
					"type": "string"
				},
				"OperatingCountry": {
					"type": "string"
				},
				"BenefitStartDate": {
					"type": "string",
					"format": "date"
				},
				"BenefitEndDate": {
					"type": "string",
					"format": "date"
				},
				"CancellationDate": {
					"type": "string",
					"format": "date"
				},
				"ClaimDate": {
					"type": "string",
					"format": "date"
				},
				"DiagnosisDate": {
					"type": "string",
					"format": "date"
				},
				"DisabilityDate": {
					"type": "string",
					"format": "date"
				},
				"HospitalStartDate": {
					"type": "string",
					"format": "date"
				},
				"LastDayWorked": {
					"type": "string",
					"format": "date"
				},
				"ReactivationDate": {
					"type": "string",
					"format": "date"
				},
				"ReportingDate": {
					"type": "string",
					"format": "date"
				},
				"ReturnToWorkDate": {
					"type": "string",
					"format": "date"
				},
				"SymptomsStartDate": {
					"type": "string",
					"format": "date"
				},
				"TerminationDate": {
					"type": "string",
					"format": "date"
				},
				"TreatmentStartDate": {
					"type": "string",
					"format": "date"
				},
				"ClaimManager": {
					"type": "object",
					"$ref": "#/definitions/Employee model"
				},
				"ClaimMgmtTeam": {
					"type": "object",
					"$ref": "#/definitions/Party model"
				},
				"MainAffiliate": {
					"type": "object",
					"$ref": "#/definitions/Party model"
				},
				"InjuredPerson": {
					"type": "object",
					"$ref": "#/definitions/Party model"
				},
				"InsuredPersonType": {
					"type": "string"
				},
				"CreationTimestamp": {
					"type": "string",
					"format": "date"
				},
				"LastUpdateTimestamp": {
					"type": "string",
					"format": "date"
				},
				"Action": {
					"type": "string"
				}
			}
		},
		"Cover model": {
			"type": "object",
			"properties": {
				"CoverBusId": {
					"type": "string"
				},
				"CoverGroupType": {
					"type": "string"
				},
				"CoverGroupTypeName": {
					"type": "string"
				},
				"CoverName": {
					"type": "string"
				},
				"CoverTemplateBusId": {
					"type": "string"
				},
				"BeneficiaryType": {
					"type": "string"
				}
			}
		},
		"Product model": {
			"type": "object",
			"properties": {
				"ProductBusId": {
					"type": "string"
				},
				"ProductGroup": {
					"type": "string"
				},
				"ProductName": {
					"type": "string"
				}
			}
		},
		"Exposure model": {
			"type": "object",
			"properties": {
				"ExposureBusId": {
					"type": "string"
				},
				"Name": {
					"type": "string"
				},
				"Product": {
					"type": "object",
					"$ref": "#/definitions/Product model"
				},
				"Covers": {
					"type": "array",
					"items": {
						"type": "object",
						"$ref": "#/definitions/Cover model"
					}
				}
			}
		},
		"Insurance contract model": {
			"type": "object",
			"properties": {
				"DataOwner": {
					"type": "string"
				},
				"ContractNumber": {
					"type": "string"
				},
				"ContractStageBusId": {
					"type": "string"
				},
				"OfferNumber": {
					"type": "string"
				},
				"ApplicationNumber": {
					"type": "string"
				},
				"ContractBusId2": {
					"type": "string"
				},
				"SourceContractBusId": {
					"type": "string"
				},
				"CustomerRefNumber": {
					"type": "string"
				},
				"ContractStage": {
					"type": "string"
				},
				"ContractState": {
					"type": "string"
				},
				"OfferState": {
					"type": "string"
				},
				"ApplicationState": {
					"type": "string"
				},
				"ContractClass": {
					"type": "string"
				},
				"BusinessContractBeginDate": {
					"type": "string",
					"format": "date"
				},
				"ContractExpiryDate": {
					"type": "string",
					"format": "date"
				},
				"ContractBeginDate": {
					"type": "string",
					"format": "date"
				},
				"ContractEndDate": {
					"type": "string",
					"format": "date"
				},
				"BusinessScope": {
					"type": "string"
				},
				"ContractName": {
					"type": "string"
				},
				"AlternativeContractName": {
					"type": "string"
				},
				"ClientSegment": {
					"type": "string"
				},
				"DistributionAgreementBusId": {
					"type": "string"
				},
				"IsActive": {
					"type": "boolean"
				},
				"IsProfitParticipation": {
					"type": "boolean"
				},
				"LineOfBusiness": {
					"type": "string"
				},
				"OperatingCountry": {
					"type": "string"
				},
				"Exposures": {
					"type": "array",
					"items": {
						"type": "object",
						"$ref": "#/definitions/Exposure model"
					}
				},
				"AdminResponsible": {
					"type": "object",
					"$ref": "#/definitions/Employee model"
				},
				"ClaimResponsible": {
					"type": "object",
					"$ref": "#/definitions/Employee model"
				},
				"CaseMgmtResponsible": {
					"type": "object",
					"$ref": "#/definitions/Employee model"
				},
				"SalesResponsible": {
					"type": "object",
					"$ref": "#/definitions/Employee model"
				},
				"Policyholder": {
					"type": "object",
					"$ref": "#/definitions/Party model"
				},
				"Broker": {
					"type": "object",
					"$ref": "#/definitions/Party model"
				},
				"AttachedPartner": {
					"type": "object",
					"$ref": "#/definitions/Party model"
				},
				"CreationTimestamp": {
					"type": "string",
					"format": "date"
				},
				"LastUpdateTimestamp": {
					"type": "string",
					"format": "date"
				},
				"Action": {
					"type": "string"
				}
			}
		},
		"UWCase model": {
			"type": "object",
			"properties": {
				"DataOwner": {
					"type": "string"
				},
				"UWCaseBusId": {
					"type": "string"
				},
				"UWApplicationBusId": {
					"type": "string"
				},
				"ContractNumber": {
					"type": "string"
				},
				"ApplicantBusId": {
					"type": "string"
				},
				"ApplicantRelationship": {
					"type": "string"
				},
				"ApplicantFirstName": {
					"type": "string"
				},
				"ApplicantLastName": {
					"type": "string"
				},
				"ApplicantDateOfBirth": {
					"type": "string",
					"format": "date"
				},
				"ApplicantSSN": {
					"type": "string"
				},
				"ApplicantTown": {
					"type": "string"
				},
				"ApplicantPostCode": {
					"type": "string"
				},
				"ApplicantCountry": {
					"type": "string"
				},
				"ApplicantClass": {
					"type": "string"
				},
				"ProductType": {
					"type": "string"
				},
				"EligiblityDate": {
					"type": "string",
					"format": "date"
				},
				"ApplicationReason": {
					"type": "string"
				},
				"ApplicationReasonText": {
					"type": "string"
				},
				"InforceCoverage": {
					"type": "decimal"
				},
				"AdditionalCoverage": {
					"type": "decimal"
				},
				"IsActive": {
					"type": "boolean"
				},
				"CaseState": {
					"type": "string"
				},
				"CreatedBy": {
					"type": "string"
				},
				"CreationTimestamp": {
					"type": "string",
					"format": "date"
				},
				"LastUpdatedBy": {
					"type": "string"
				},
				"LastUpdateTimestamp": {
					"type": "string",
					"format": "date"
				},
				"Action": {
					"type": "string"
				}
			}
		},
		"UWApplication model": {
			"type": "object",
			"properties": {
				"DataOwner": {
					"type": "string"
				},
				"UWApplicationBusId": {
					"type": "string"
				},
				"OperatingCountry": {
					"type": "string"
				},
				"PolicyholderBusId": {
					"type": "string"
				},
				"PolicyholderName": {
					"type": "string"
				},
				"MainAffiliateBusId": {
					"type": "string"
				},
				"MainAffiliateFirstName": {
					"type": "string"
				},
				"MainAffiliateLastName": {
					"type": "string"
				},
				"MainAffiliateDateOfBirth": {
					"type": "string",
					"format": "date"
				},
				"MainAffiliateSSN": {
					"type": "string"
				},
				"MainAffiliateTown": {
					"type": "string"
				},
				"MainAffiliatePostCode": {
					"type": "string"
				},
				"MainAffiliateCountry": {
					"type": "string"
				},
				"SubmissionDate": {
					"type": "string",
					"format": "date"
				},
				"IsActive": {
					"type": "boolean"
				},
				"DocSignId": {
					"type": "string"
				},
				"SFSignedDate": {
					"type": "string",
					"format": "date"
				},
				"UWCases": {
					"type": "array",
					"items": {
						"type": "object",
						"$ref": "#/definitions/UWCase model"
					}
				},
				"CreatedBy": {
					"type": "string"
				},
				"CreationTimestamp": {
					"type": "string",
					"format": "date"
				},
				"LastUpdatedBy": {
					"type": "string"
				},
				"LastUpdateTimestamp": {
					"type": "string",
					"format": "date"
				},
				"Action": {
					"type": "string"
				}
			}
		}
	}
}