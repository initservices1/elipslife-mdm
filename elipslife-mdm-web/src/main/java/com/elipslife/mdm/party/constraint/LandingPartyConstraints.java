package com.elipslife.mdm.party.constraint;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.elipslife.ebx.data.access.StagingPartyReader;
import com.elipslife.ebx.domain.bean.StagingPartyBean;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.base.text.Severity;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidationOnRecord;
import com.orchestranetworks.instance.ValueContextForValidationOnTable;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.ConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.ValidationReport;

/**
 * 1. check if any validation rules found in current record
 * 
 * 2. Check if more than one party found in staging area with the combination of source system, Source PartyBusId, Valid From and Valid To
 * 
 * @author VenkateswarluJetti
 *
 */
public class LandingPartyConstraints implements ConstraintOnTableWithRecordLevelCheck {

	@Override
	public void checkRecord(ValueContextForValidationOnRecord aValueContext) {
		
		Map<SchemaNode, HashSet<UserMessage>> validationInfoMap = new HashMap<SchemaNode, HashSet<UserMessage>>();
		
		// check if any validation rules found in current record
		// checkIfAnyValidationError(aValueContext.getRecord(), validationInfoMap, true);
		
		// Check if more than one party found in staging area with the combination of source system, Source PartyBusId, Valid From and Valid To
		checkIfDuplicatePartiesFoundInStaging(aValueContext.getRecord(), validationInfoMap, true);
	}
	
	/**
	 * Check if more than one party found in staging area with the combination of source system, Source PartyBusId, Valid From and Valid To
	 * 
	 * @param aValueContext
	 * @param validationInfoMap
	 * @param isFstRec
	 */
	public void checkIfDuplicatePartiesFoundInStaging(ValueContext aValueContext, Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {
		
		Integer landingPartyId = (Integer) aValueContext.getValue(LandingPaths._LDG_Party._Party_PartyId);
		
		// check it only for created records
		if(landingPartyId != null) {
			
			UserMessage userMessage = null;
			String sourceSystem = (String) aValueContext.getValue(LandingPaths._LDG_Party._Control_SourceSystem);
			String sourcePartyBusId = (String) aValueContext.getValue(LandingPaths._LDG_Party._Control_SourcePartyBusId);
			Date validFrom = (Date) aValueContext.getValue(LandingPaths._LDG_Party._Control_ValidFrom);
			Date validTo = (Date) aValueContext.getValue(LandingPaths._LDG_Party._Control_ValidTo);
			
			// get staging parties
			StagingPartyReader stagingPartyReader = new StagingPartyReader();
			List<StagingPartyBean> stagingPartyBeans = stagingPartyReader.findBeansBySourceSystem(sourceSystem, sourcePartyBusId, validFrom, validTo);
			if(!stagingPartyBeans.isEmpty()) {
				if(stagingPartyBeans.size() > 1) {
					// add the error message if there is any errors.
					userMessage = UserMessage.createError("More than one party found in staging with the combination of Source System, Source Party BusId, Valid From and Valid To");
					this.insertUserMsg(validationInfoMap, aValueContext.getNode(LandingPaths._LDG_Party._Party_PartyId), userMessage);
				}
			}
			
			
		}
	}
	
	/**
	 * check if validation errors found in current record or not
	 * 
	 * @param aValueContext
	 * @param validationInfoMap
	 * @param isFstRec
	 */
	public void checkIfAnyValidationError(ValueContext aValueContext, Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {
		
		Integer landingPartyId = (Integer) aValueContext.getValue(LandingPaths._LDG_Party._Party_PartyId);
		
		// check it only for created records
		if(landingPartyId != null) {
			UserMessage userMessage = null;
			Adaptation landingPartyRecord = aValueContext.getAdaptationTable().lookupAdaptationByPrimaryKey(aValueContext);
			ValidationReport validationReport = landingPartyRecord.getValidationReport();
			if(validationReport != null) {
				//check if we have any validation errors found in current selected record
				boolean isAnyValidationErrorsFound = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
				if(isAnyValidationErrorsFound) {
					// add the error message if there is any errors.
					userMessage = UserMessage.createError("Please correct the below validations to process this record to staging");
					this.insertUserMsg(validationInfoMap, aValueContext.getNode(LandingPaths._LDG_Party._Party_PartyId), userMessage);
				}
			}
		}
	}
	public void insertUserMsg(final Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, final SchemaNode sn, final UserMessage userMsg) {
			HashSet<UserMessage> listOfUserMsg = null;
			if (validationInfoMap.containsKey(sn)) {
				listOfUserMsg = validationInfoMap.get(sn);
				listOfUserMsg.add(userMsg);
			} else {
				listOfUserMsg = new HashSet<UserMessage>();
				listOfUserMsg.add(userMsg);
				validationInfoMap.put(sn, listOfUserMsg);
			}
	}
	
	
	@Override
	public void checkTable(ValueContextForValidationOnTable aTable) {
		
	}
	
	@Override
	public void setup(ConstraintContextOnTable arg0) {
	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1) throws InvalidSchemaException {
		return null;
	}

	

}
