package com.elipslife.ebx.domain.bean.convert;

import java.util.Objects;

import com.elipslife.ebx.domain.bean.CoreAddressBean;
import com.elipslife.ebx.domain.bean.LandingAddressBean;
import com.elipslife.ebx.domain.bean.LandingAddressBulkProcessBean;
import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.mdm.party.constants.PartyConstants;

public class AddressConverter {

    /**
     * 
     * @param landingAddressBean
     * @return
     */
    public static StagingAddressBean createStagingAddressBeanFromLandingAddressBean(LandingAddressBean landingAddressBean) {

        StagingAddressBean stagingAddressBean = new StagingAddressBean();
        fillStagingAddressBeanWithLandingAddressBean(landingAddressBean, stagingAddressBean);
        return stagingAddressBean;
    }


    /**
     * 
     * @param landingAddressBean
     * @param stagingAddressBean
     */
    public static void fillStagingAddressBeanWithLandingAddressBean(LandingAddressBean landingAddressBean, StagingAddressBean stagingAddressBean) {

        stagingAddressBean.setAddressType(landingAddressBean.getAddressType());
        stagingAddressBean.setAddressUsage(landingAddressBean.getAddressUsage());
        stagingAddressBean.setSourceAddressBusId(landingAddressBean.getSourceAddressBusId());
        stagingAddressBean.setStreet(landingAddressBean.getStreet());
        stagingAddressBean.setStreet2(landingAddressBean.getStreet2());
        stagingAddressBean.setPostCode(landingAddressBean.getPostCode());
        stagingAddressBean.setPoBox(landingAddressBean.getPoBox());
        stagingAddressBean.setPoBoxPostCode(landingAddressBean.getPoBoxPostCode());
        stagingAddressBean.setPoBoxTown(landingAddressBean.getPoBoxTown());
        stagingAddressBean.setDistrict(landingAddressBean.getDistrict());
        stagingAddressBean.setTown(landingAddressBean.getTown());
        stagingAddressBean.setStateProvince(landingAddressBean.getStateProvince());
        stagingAddressBean.setCountry(landingAddressBean.getCountry());
        stagingAddressBean.setCorrelationId(landingAddressBean.getCorrelationId()); // set correlation id
        stagingAddressBean.setSourceSystem(landingAddressBean.getSourceSystem());
        stagingAddressBean.setSourcePartyBusId(landingAddressBean.getSourcePartyBusId());
        stagingAddressBean.setValidFrom(landingAddressBean.getValidFrom());
        stagingAddressBean.setValidTo(landingAddressBean.getValidTo());
        stagingAddressBean.setCreationTimestamp(landingAddressBean.getCreationTimestamp());
        stagingAddressBean.setLastUpdateTimestamp(landingAddressBean.getLastUpdateTimestamp());
        stagingAddressBean.setAction(landingAddressBean.getAction());
        stagingAddressBean.setActionBy(landingAddressBean.getActionBy());
        stagingAddressBean.setElectronicAddressType(landingAddressBean.getElectronicAddressType());
        stagingAddressBean.setAddress(landingAddressBean.getAddress());
    }
    
    /**
     * Create Staging Address Bean from Landing Address Bulk Process Bean
     * 
     * @param landingAddressBulkProcessBean
     * @return
     */
    public static StagingAddressBean createStagingAddressBeanFromLandingAddressBean(LandingAddressBulkProcessBean landingAddressBulkProcessBean) {

        StagingAddressBean stagingAddressBean = new StagingAddressBean();
        fillStagingAddressBeanWithLandingAddressBulkProcessBean(landingAddressBulkProcessBean, stagingAddressBean);
        return stagingAddressBean;
    }

    /**
     * Fill staging party bean with Landing Party Bulk Process Bean
     * 
     * @param landingAddressBulkProcessBean
     * @param stagingAddressBean
     */
    public static void fillStagingAddressBeanWithLandingAddressBulkProcessBean(LandingAddressBulkProcessBean landingAddressBulkProcessBean,
        StagingAddressBean stagingAddressBean) {

        stagingAddressBean.setAddressType(landingAddressBulkProcessBean.getAddressType());
        stagingAddressBean.setAddressUsage(landingAddressBulkProcessBean.getAddressUsage());
        stagingAddressBean.setSourceAddressBusId(landingAddressBulkProcessBean.getSourceAddressBusId());
        stagingAddressBean.setStreet(landingAddressBulkProcessBean.getStreet());
        stagingAddressBean.setStreet2(landingAddressBulkProcessBean.getStreet2());
        stagingAddressBean.setPostCode(landingAddressBulkProcessBean.getPostCode());
        stagingAddressBean.setPoBox(landingAddressBulkProcessBean.getPoBox());
        stagingAddressBean.setPoBoxPostCode(landingAddressBulkProcessBean.getPoBoxPostCode());
        stagingAddressBean.setPoBoxTown(landingAddressBulkProcessBean.getPoBoxTown());
        stagingAddressBean.setDistrict(landingAddressBulkProcessBean.getDistrict());
        stagingAddressBean.setTown(landingAddressBulkProcessBean.getTown());
        stagingAddressBean.setStateProvince(landingAddressBulkProcessBean.getStateProvince());
        stagingAddressBean.setCountry(landingAddressBulkProcessBean.getCountry());
        stagingAddressBean.setCorrelationId(landingAddressBulkProcessBean.getCorrelationId()); // set correlation id
        stagingAddressBean.setSourceSystem(landingAddressBulkProcessBean.getSourceSystem());
        stagingAddressBean.setSourcePartyBusId(landingAddressBulkProcessBean.getSourcePartyBusId());
        stagingAddressBean.setValidFrom(landingAddressBulkProcessBean.getValidFrom());
        stagingAddressBean.setValidTo(landingAddressBulkProcessBean.getValidTo());
        stagingAddressBean.setCreationTimestamp(landingAddressBulkProcessBean.getCreationTimestamp());
        stagingAddressBean.setLastUpdateTimestamp(landingAddressBulkProcessBean.getLastUpdateTimestamp());
        stagingAddressBean.setAction(landingAddressBulkProcessBean.getAction());
        stagingAddressBean.setActionBy(landingAddressBulkProcessBean.getActionBy());
        stagingAddressBean.setElectronicAddressType(landingAddressBulkProcessBean.getElectronicAddressType());
        stagingAddressBean.setAddress(landingAddressBulkProcessBean.getAddress());
    }


    /**
     * 
     * @param stagingAddressBean
     * @return
     */
    public static CoreAddressBean createCoreAddressBeanFromStagingAddressBean(StagingAddressBean stagingAddressBean) {

        CoreAddressBean coreAddressBean = new CoreAddressBean();
        fillCoreAddressBeanWithStagingAddressBean(stagingAddressBean, coreAddressBean);
        return coreAddressBean;
    }


    /**
     * 
     * @param stagingAddressBean
     * @param coreAddressBean
     */
    public static void fillCoreAddressBeanWithStagingAddressBean(StagingAddressBean stagingAddressBean, CoreAddressBean coreAddressBean) {

//		coreAddressBean.setPartyId(stagingAddressBean.getPartyId());
        coreAddressBean.setAddressType(stagingAddressBean.getAddressType());
        coreAddressBean.setAddressUsage(stagingAddressBean.getAddressUsage());
        coreAddressBean.setSourceAddressBusId(stagingAddressBean.getSourceAddressBusId());
        coreAddressBean.setStreet(stagingAddressBean.getStreet());
        coreAddressBean.setStreet2(stagingAddressBean.getStreet2());
        coreAddressBean.setTown(stagingAddressBean.getTown());
        coreAddressBean.setPostCode(stagingAddressBean.getPostCode());
        coreAddressBean.setPoBox(stagingAddressBean.getPoBox());
        coreAddressBean.setPoBoxPostCode(stagingAddressBean.getPoBoxPostCode());
        coreAddressBean.setPoBoxTown(stagingAddressBean.getPoBoxTown());
        coreAddressBean.setDistrict(stagingAddressBean.getDistrict());
        coreAddressBean.setStateProvince(stagingAddressBean.getStateProvince());
        coreAddressBean.setCountry(stagingAddressBean.getCountry());
        coreAddressBean.setElectronicAddressType(stagingAddressBean.getElectronicAddressType());
        coreAddressBean.setAddress(stagingAddressBean.getAddress());
        coreAddressBean.setValidFrom(stagingAddressBean.getValidFrom());
        coreAddressBean.setValidTo(stagingAddressBean.getValidTo());
//		coreAddressBean.setIsExpired(stagingAddressBean.getIsExpired());
        coreAddressBean.setSourceSystem(stagingAddressBean.getSourceSystem());
        coreAddressBean.setSourcePartyBusId(stagingAddressBean.getSourcePartyBusId());

        if (Objects.equals(stagingAddressBean.getAction(), PartyConstants.RecordOperations.CREATE)) {
            coreAddressBean.setCreatedBy(stagingAddressBean.getActionBy());
        }

        coreAddressBean.setCreationTimestamp(stagingAddressBean.getCreationTimestamp());

        if (Objects.equals(stagingAddressBean.getAction(), PartyConstants.RecordOperations.UPDATE)) {
            coreAddressBean.setLastUpdatedBy(stagingAddressBean.getActionBy());
        }


        coreAddressBean.setLastUpdateTimestamp(stagingAddressBean.getLastUpdateTimestamp());
        coreAddressBean.setLastSyncAction(stagingAddressBean.getAction());
//		coreAddressBean.setLastSyncTimestamp(stagingAddressBean.getLastSyncTimestamp());
    }
}
