package com.elipslife.ebx.domain.bean;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class LandingBankAccountBean extends Entity<Integer> {
	
	private String accountType;
	private String accountNumber;
	private String iban;
	private String bic;
	private String clearingNumber;
	private String bankName;
	private String country;
	private String currency;
	private String correlationId;
	private String landingPartyId;
	private String mdmStagingProcessResponse;
	private Boolean isMainAccount;
	private String sourceBankAccountBusId;
	private String sourceSystem;
	private String sourcePartyBusId;
	private Date validFrom;
	private Date validTo;
	private Date creationTimestamp;
	private Date lastUpdateTimestamp;
	private Date lastSyncTimeStamp;
	private String action;
	private String actionBy;
	
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getBic() {
		return bic;
	}
	public void setBic(String bic) {
		this.bic = bic;
	}
	public String getClearingNumber() {
		return clearingNumber;
	}
	public void setClearingNumber(String clearingNumber) {
		this.clearingNumber = clearingNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	public String getLandingPartyId() {
		return landingPartyId;
	}
	public void setLandingPartyId(String landingPartyId) {
		this.landingPartyId = landingPartyId;
	}
	public String getMdmStagingProcessResponse() {
		return mdmStagingProcessResponse;
	}
	public void setMdmStagingProcessResponse(String mdmStagingProcessResponse) {
		this.mdmStagingProcessResponse = mdmStagingProcessResponse;
	}
	public Boolean getIsMainAccount() {
		return isMainAccount;
	}
	public void setIsMainAccount(Boolean isMainAccount) {
		this.isMainAccount = isMainAccount;
	}
	public String getSourceBankAccountBusId() {
		return sourceBankAccountBusId;
	}
	public void setSourceBankAccountBusId(String sourceBankAccountBusId) {
		this.sourceBankAccountBusId = sourceBankAccountBusId;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getSourcePartyBusId() {
		return sourcePartyBusId;
	}
	public void setSourcePartyBusId(String sourcePartyBusId) {
		this.sourcePartyBusId = sourcePartyBusId;
	}
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActionBy() {
		return actionBy;
	}
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	
	public Date getUpdateTimestamp() {
		return getLastUpdateTimestamp() == null ? getCreationTimestamp() : getLastUpdateTimestamp();
	}
	public Date getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}
	public void setLastSyncTimeStamp(Date lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}
	
	
}