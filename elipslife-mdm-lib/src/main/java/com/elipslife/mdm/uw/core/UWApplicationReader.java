package com.elipslife.mdm.uw.core;

import java.util.List;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.UWPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;


public class UWApplicationReader extends AbstractTableReader<UWApplicationBean> {

    public UWApplicationReader() {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._UWApplication.getPathInSchema()));
    }

    public UWApplicationReader(String dataspace) {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._UWApplication.getPathInSchema()));
    }

    public UWApplicationReader(AdaptationTable table) {
		super(table);
	}

	@Override
    public UWApplicationBean createBeanFromRecord(Adaptation record) {
        UWApplicationBean bean = new UWApplicationBean();
        bean.setUWApplicationId(record.get_int(UWPaths._UWApplication._UWApplicationId));
        bean.setUWApplicationBusId(record.getString(UWPaths._UWApplication._UWApplicationBusId));
        bean.setSourceSystem(record.getString(UWPaths._UWApplication._Context_SourceSystem));
        bean.setDataOwner(record.getString(UWPaths._UWApplication._Context_DataOwner));
        bean.setOperatingCountry(record.getString(UWPaths._UWApplication._Context_OperatingCountry));
        bean.setPolicyholderId(record.getString(UWPaths._UWApplication._Policyholder_PolicyholderId));
        bean.setPolicyholderBusId(record.getString(UWPaths._UWApplication._Policyholder_PolicyholderBusId));
        bean.setPolicyholderName(record.getString(UWPaths._UWApplication._Policyholder_PolicyholderName));
        bean.setMainAffiliateId(record.getString(UWPaths._UWApplication._MainAffiliate_MainAffiliateId));
        bean.setMainAffiliateBusId(record.getString(UWPaths._UWApplication._MainAffiliate_MainAffiliateBusId));
        bean.setMainAffiliateFirstName(record.getString(UWPaths._UWApplication._MainAffiliate_MainAffiliateFirstName));
        bean.setMainAffiliateLastName(record.getString(UWPaths._UWApplication._MainAffiliate_MainAffiliateLastName));
        bean.setMainAffiliateDateOfBirth(record.getDate(UWPaths._UWApplication._MainAffiliate_MainAffiliateDateOfBirth));
        bean.setMainAffiliateSSN(record.getString(UWPaths._UWApplication._MainAffiliate_MainAffiliateSSN));
        bean.setMainAffiliateTown(record.getString(UWPaths._UWApplication._MainAffiliate_MainAffiliateTown));
        bean.setMainAffiliatePostCode(record.getString(UWPaths._UWApplication._MainAffiliate_MainAffiliatePostCode));
        bean.setMainAffiliateCountry(record.getString(UWPaths._UWApplication._MainAffiliate_MainAffiliateCountry));
        bean.setSubmissionDate(record.getDate(UWPaths._UWApplication._Details_SubmissionDate));
        bean.setIsActive(record.get_boolean(UWPaths._UWApplication._Details_IsActive));
        bean.setDocSignId(record.getString(UWPaths._UWApplication._DocumentSigning_DocSignId));
        bean.setSFSignedDate(record.getDate(UWPaths._UWApplication._DocumentSigning_SFSignedDate));
        bean.setCreatedBy(record.getString(UWPaths._UWApplication._Control_CreatedBy));
		bean.setCreationTimeStamp(record.getDate(UWPaths._UWApplication._Control_CreationTimestamp));
		bean.setLastUpdatedBy(record.getString(UWPaths._UWApplication._Control_LastUpdatedBy));
		bean.setLastUpdateTimeStamp(record.getDate(UWPaths._UWApplication._Control_LastUpdateTimestamp));
		bean.setDeletionTimeStamp(record.getDate(UWPaths._STG_UWApplication._Control_DeletionTimestamp));
		bean.setLastSyncAction(record.getString(UWPaths._UWApplication._Control_LastSyncAction));
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{UWPaths._UWApplication._UWApplicationId};
    }

    @Override
    public Object[] retrievePks(UWApplicationBean bean) {
        return new Object[]{bean.getUWApplicationId()};
    }
    
    public List<UWApplicationBean> findBeansByApplicationBusId(String applicationBusId) {
    	XPathPredicate predicate = new XPathPredicate(Operation.Equals, UWPaths._UWApplication._UWApplicationBusId, applicationBusId);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate);
		
		return super.readBeansFor(group.toString());
    }

}
