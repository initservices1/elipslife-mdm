package com.elipslife.mdm.party;

import static com.elipslife.mdm.party.constants.PartyConstants.Entities.Party.AddressType.ELECTRONIC_ADDRESS;
import static com.elipslife.mdm.party.constants.PartyConstants.Entities.Party.AddressType.POSTAL_ADDRESS;
import static com.elipslife.mdm.party.constants.PartyConstants.Entities.Party.PartyType.CONTACT;
import static com.elipslife.mdm.party.constants.PartyConstants.Entities.Party.PartyType.INDIVIDUAL;
import static com.elipslife.mdm.party.constants.PartyConstants.Entities.Party.PartyType.COMPANY;
import static com.elipslife.mdm.party.constants.PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT;

import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Session;

public class PartySchemaExtensions  implements SchemaExtensions {

	private static final Path PATH_TO_PERSON = PartyPaths._Party
			.getPathInSchema()
			.add(PartyPaths._Party._Person);
	
	private static final Path PATH_TO_CONTACTPERSON = PartyPaths._Party
			.getPathInSchema()
			.add(PartyPaths._Party._ContactPerson);
					
	private static final Path PATH_TO_ORGANISATION = PartyPaths._Party
			.getPathInSchema()
			.add(PartyPaths._Party._Organisation);

	private static final Path PATH_TO_POSTAL_ADRRESS = PartyPaths._Address
			.getPathInSchema()
			.add(PartyPaths._Address._PostalAddress);

	private static final Path PATH_TO_ELETRONIC_ADRRESS = PartyPaths._Address
			.getPathInSchema()
			.add(PartyPaths._Address._ElectronicAddress);

	/*private static final Path PATH_TO_PARTY_DAQA_DUPLICATES = PartyPaths._Party
			.getPathInSchema()
			.add(PartyPaths._Party._Duplicates);*/
	
	
	@Override
	public void defineExtensions(SchemaExtensionsContext aContext) {

		aContext.setAccessRuleOnNodeAndAllDescendants(PATH_TO_PERSON, true,	new AccessRuleForFieldPerson());
		
		aContext.setAccessRuleOnNodeAndAllDescendants(PATH_TO_ORGANISATION, true, new AccessRuleForFieldOrganisation());
		
		aContext.setAccessRuleOnNodeAndAllDescendants(PATH_TO_CONTACTPERSON, true, new AccessRuleForFieldContactPerson());
		
		aContext.setAccessRuleOnNodeAndAllDescendants(PATH_TO_POSTAL_ADRRESS, true,	new AccessRuleForPostalAddress());
		
		aContext.setAccessRuleOnNodeAndAllDescendants(PATH_TO_ELETRONIC_ADRRESS, true, new AccessRuleForElectronicAddress());
		
		//aContext.setAccessRuleOnNode(PATH_TO_PARTY_DAQA_DUPLICATES, new AccessRuleForPartyDAQADuplicates());
		
		
	}
	
	
	private static class AccessRuleForFieldPerson implements AccessRule {

		private static final Path PATH_TO_PARTYTYPE = PartyPaths._Party._PartyInfo_PartyType;

		@Override
		public AccessPermission getPermission(Adaptation adaptation, Session sess, SchemaNode schemaNode) {
			
			if (adaptation.isSchemaInstance()) {
				return AccessPermission.getReadWrite();
			}
			
			final String partyType = String.valueOf(adaptation.get(PATH_TO_PARTYTYPE));
			
			if (partyType.equalsIgnoreCase(INDIVIDUAL) || partyType.equalsIgnoreCase(CONTACT)) {
				return AccessPermission.getReadWrite();
			} else {
				return AccessPermission.getHidden();
			}
		}
	}
	

	private static class AccessRuleForFieldContactPerson implements AccessRule {

		private static final Path PATH_TO_PARTYTYPE = PartyPaths._Party._PartyInfo_PartyType;

		@Override
		public AccessPermission getPermission(Adaptation adaptation, Session sess, SchemaNode schemaNode) {
				
			if (adaptation.isSchemaInstance()) {
				return AccessPermission.getReadWrite();
			}
			
			final String partyType = String.valueOf(adaptation.get(PATH_TO_PARTYTYPE));
			
			if (partyType.equalsIgnoreCase(CONTACT) || partyType.equalsIgnoreCase(INDIVIDUAL)) {
				return AccessPermission.getReadWrite();
			} else {
				return AccessPermission.getHidden();
			}
		}
	}
	
	
	private static class AccessRuleForFieldOrganisation implements AccessRule {

		private static final Path PATH_TO_PARTYTYPE = PartyPaths._Party._PartyInfo_PartyType;

		@Override
		public AccessPermission getPermission(Adaptation adaptation, Session sess, SchemaNode schemaNode) {
			
			if (adaptation.isSchemaInstance()) {
				return AccessPermission.getReadWrite();
			}
			
			final String partyType = String.valueOf(adaptation.get(PATH_TO_PARTYTYPE));
			
			if (partyType.equalsIgnoreCase(COMPANY) || partyType.equalsIgnoreCase(ORGANISATION_UNIT)) {
				return AccessPermission.getReadWrite();
			} else {
				return AccessPermission.getHidden();
			}
		}
	}
	
	
	private static class AccessRuleForPostalAddress implements AccessRule {

		private static final Path PATH_TO_ADDRESS_TYPE = PartyPaths._Address._AddressInfo_AddressType;

		@Override
		public AccessPermission getPermission(Adaptation adaptation, Session sess, SchemaNode schemaNode) {
			
			if (adaptation.isSchemaInstance()) {
				return AccessPermission.getReadWrite();
			}
			
			final String partyType = String.valueOf(adaptation.get(PATH_TO_ADDRESS_TYPE));
			
			if (partyType.equalsIgnoreCase(POSTAL_ADDRESS)) {
				return AccessPermission.getReadWrite();
			} else {
				return AccessPermission.getHidden();
			}
		}
	}

	
	private static class AccessRuleForElectronicAddress implements AccessRule {

		private static final Path PATH_TO_ADDRESS_TYPE = PartyPaths._Address._AddressInfo_AddressType;
		
		@Override
		public AccessPermission getPermission(Adaptation adaptation, Session sess, SchemaNode schemaNode) {
			
			if (adaptation.isSchemaInstance()) {
				return AccessPermission.getReadWrite();
			}
			
			final String partyType = String.valueOf(adaptation.get(PATH_TO_ADDRESS_TYPE));

			if (partyType.equalsIgnoreCase(ELECTRONIC_ADDRESS)) {
				return AccessPermission.getReadWrite();
			} else {
				return AccessPermission.getHidden();
			}
		}
	}
	
	/*private static class AccessRuleForPartyDAQADuplicates implements AccessRule {

		private static final Path PATH_TO_PARTY_DAQA_STATE = PartyPaths._Party._DaqaMetaData_State;


		@Override
		public AccessPermission getPermission(Adaptation adaptation, Session sess, SchemaNode schemaNode) {
		
		if(adaptation.getOccurrencePrimaryKey() == null) {
			return AccessPermission.getReadWrite();
		}	
		
		final String daqaState = String.valueOf(adaptation.get(PATH_TO_PARTY_DAQA_STATE));
			
			if (daqaState.equalsIgnoreCase("Golden") || daqaState.equalsIgnoreCase("Pivot")) {
				return AccessPermission.getReadWrite();
			} else {
				return AccessPermission.getHidden();
			}
		}
	}*/
}