package com.elipslife.mdm.uw.landing;

import java.util.Date;
import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.LandingUWPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class LandingUWCaseWriter extends AbstractTableWriter<LandingUWCaseBean> {

    private LandingUWCaseReader reader;

    public LandingUWCaseWriter() {
        this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.LANDING_UW_REFERENCE,
            Constants.DataSet.LANDING_UW_MASTER), LandingUWPaths._UWCase.getPathInSchema()));
    }

    public LandingUWCaseWriter(String dataspace) {
        this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace,
            Constants.DataSet.LANDING_UW_MASTER), LandingUWPaths._UWCase.getPathInSchema()));
    }

    public LandingUWCaseWriter(AdaptationTable table) {
        super(table);
        reader = new LandingUWCaseReader(table);
    }

    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, LandingUWCaseBean bean) {
        values.put(LandingUWPaths._UWCase._UWCaseId, bean.getUWCaseId());
        values.put(LandingUWPaths._UWCase._UWCaseBusId, bean.getUWCaseBusId());
        values.put(LandingUWPaths._UWCase._UWApplicationId, bean.getUWApplicationId());
        values.put(LandingUWPaths._UWCase._Context_SourceSystem, bean.getSourceSystem());
        values.put(LandingUWPaths._UWCase._Context_DataOwner, bean.getDataOwner());
        values.put(LandingUWPaths._UWCase._Context_UWApplicationBusId, bean.getUWApplicationBusId());
        values.put(LandingUWPaths._UWCase._Context_ContractNumber, bean.getContractNumber());
        values.put(LandingUWPaths._UWCase._Applicant_ApplicantBusId, bean.getApplicantBusId());
        values.put(LandingUWPaths._UWCase._Applicant_ApplicantRelationship, bean.getApplicantRelationship());
        values.put(LandingUWPaths._UWCase._Applicant_ApplicantFirstName, bean.getApplicantFirstName());
        values.put(LandingUWPaths._UWCase._Applicant_ApplicantLastName, bean.getApplicantLastName());
        values.put(LandingUWPaths._UWCase._Applicant_ApplicantDateOfBirth, bean.getApplicantDateOfBirth());
        values.put(LandingUWPaths._UWCase._Applicant_ApplicantSSN, bean.getApplicantSSN());
        values.put(LandingUWPaths._UWCase._Applicant_ApplicantTown, bean.getApplicantTown());
        values.put(LandingUWPaths._UWCase._Applicant_ApplicantPostCode, bean.getApplicantPostCode());
        values.put(LandingUWPaths._UWCase._Applicant_ApplicantCountry, bean.getApplicantCountry());
        values.put(LandingUWPaths._UWCase._Details_ApplicantClass, bean.getApplicantClass());
        values.put(LandingUWPaths._UWCase._Details_ProductType, bean.getProductType());
        values.put(LandingUWPaths._UWCase._Details_EligiblityDate, bean.getEligiblityDate());
        values.put(LandingUWPaths._UWCase._Details_ApplicationReason, bean.getApplicationReason());
        values.put(LandingUWPaths._UWCase._Details_ApplicationReasonText, bean.getApplicationReasonText());
        values.put(LandingUWPaths._UWCase._Details_InforceCoverage, bean.getInforceCoverage());
        values.put(LandingUWPaths._UWCase._Details_AdditionalCoverage, bean.getAdditionalCoverage());
        values.put(LandingUWPaths._UWCase._State_IsActive, bean.getIsActive());
        values.put(LandingUWPaths._UWCase._State_CaseState, bean.getCaseState());
        values.put(LandingUWPaths._UWCase._Control_CreatedBy, bean.getCreatedBy());
        values.put(LandingUWPaths._UWCase._Control_CreationTimestamp, bean.getCreationTimeStamp());
        values.put(LandingUWPaths._UWCase._Control_LastUpdatedBy, bean.getLastUpdatedBy());
        values.put(LandingUWPaths._UWCase._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
        values.put(LandingUWPaths._UWCase._Control_DeletionTimestamp, bean.getDeletionTimeStamp());
        values.put(LandingUWPaths._UWCase._Control_LastSyncTimestamp, new Date());
        values.put(LandingUWPaths._UWCase._Control_Action, bean.getAction());
    }

    @Override
    public AbstractTableReader<LandingUWCaseBean> getTableReader() {
        return reader;
    }

    @Override
    protected void executeProcedure(Procedure procedure) {
        // TODO Auto-generated method stub

    }
}
