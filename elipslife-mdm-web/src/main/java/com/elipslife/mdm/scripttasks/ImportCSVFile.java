package com.elipslife.mdm.scripttasks;

import java.io.File;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ExportImportCSVSpec;
import com.orchestranetworks.service.ExportImportCSVSpec.Header;
import com.orchestranetworks.service.ImportSpec;
import com.orchestranetworks.service.ImportSpecMode;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class ImportCSVFile extends ScriptTaskBean {
	private String fieldSeparator;
	private String inputDirectory;
	private String fileName;
	private String errorMessage;
	private String errorLogDirectory;
	private String tableName;
	private String dataSetName;
	private String dataSpaceName;

	@Override
	public void executeScript(ScriptTaskBeanContext aContext) throws OperationException {
		
		File file = new File(inputDirectory, fileName);

		// import CSV Specs
		ExportImportCSVSpec csvSpec = new ExportImportCSVSpec();
		csvSpec.setEncoding("UTF-8");
		csvSpec.setFieldSeparator(fieldSeparator.charAt(0));
		csvSpec.setHeader(Header.LABEL);
		csvSpec.setListSeparator("\r\n");

		// import specs
		ImportSpec importSpec = new ImportSpec();
		importSpec.setImportMode(ImportSpecMode.UPDATE_OR_INSERT);
		importSpec.setDetailedResult(true);
		importSpec.setSourceFile(file);
		importSpec.setCSVSpec(csvSpec);

		// set target table
		AdaptationTable landingClaimCase = RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(RepositoryHelper.getDataSpace(dataSpaceName), dataSetName),
				Path.parse(tableName));
		importSpec.setTargetAdaptationTable(landingClaimCase);
		
		// start import and catch errors
		Procedure proc = new Procedure() {
			@Override
			public void execute(ProcedureContext aContext) throws Exception {
				aContext.doImport(importSpec);
			}
		};

		ProcedureResult result = ProgrammaticService.createForSession(aContext.getSession(), RepositoryHelper.getDataSpace(dataSpaceName)).execute(proc);
		if (result.hasFailed()) {

			String message = result.getException().toString();
			setErrorMessage(message);
		}
	}

	public String getFieldSeparator() {
		return fieldSeparator;
	}

	public void setFieldSeparator(String fieldSeparator) {
		this.fieldSeparator = fieldSeparator;
	}

	public String getErrorLogDirectory() {
		return errorLogDirectory;
	}

	public void setErrorLogDirectory(String errorLogDirectory) {
		this.errorLogDirectory = errorLogDirectory;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getDataSetName() {
		return dataSetName;
	}

	public void setDataSetName(String dataSet) {
		this.dataSetName = dataSet;
	}

	public String getDataSpaceName() {
		return dataSpaceName;
	}

	public void setDataSpaceName(String dataSpaceName) {
		this.dataSpaceName = dataSpaceName;
	}

	public String getInputDirectory() {
		return inputDirectory;
	}

	public void setInputDirectory(String inpuDirectory) {
		this.inputDirectory = inpuDirectory;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}