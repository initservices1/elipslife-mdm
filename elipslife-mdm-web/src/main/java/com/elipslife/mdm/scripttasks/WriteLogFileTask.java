package com.elipslife.mdm.scripttasks;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

public class WriteLogFileTask extends ScriptTaskBean {
	
	private String directory;
	private String fileName;
	private String encoding;
	private String content;
	
	@Override
	public void executeScript(ScriptTaskBeanContext context) throws OperationException {

		try {
			
			File file = new File(directory, fileName + ".log");
			file.createNewFile();
			
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), this.encoding));
			writer.write(this.content);
			writer.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}