package com.elipslife.mdm.party.scheduler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.boot.VM;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProgrammaticService;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class UpdateCoreRecords_Bkp extends ScheduledTask{

	public String	fromDataSpace;
	public String	toDataSpace;

	public String	fromDataSet;
	public String	toDataSet;
	
	public String	fromTableXpath;
	public String	toTableXpath;
	
	public String getFromDataSpace() {
		return fromDataSpace;
	}


	public void setFromDataSpace(String fromDataSpace) {
		this.fromDataSpace = fromDataSpace;
	}


	public String getToDataSpace() {
		return toDataSpace;
	}


	public void setToDataSpace(String toDataSpace) {
		this.toDataSpace = toDataSpace;
	}


	public String getFromDataSet() {
		return fromDataSet;
	}


	public void setFromDataSet(String fromDataSet) {
		this.fromDataSet = fromDataSet;
	}


	public String getToDataSet() {
		return toDataSet;
	}


	public void setToDataSet(String toDataSet) {
		this.toDataSet = toDataSet;
	}


	

	public String getFromTableXpath() {
		return fromTableXpath;
	}


	public void setFromTableXpath(String fromTableXpath) {
		this.fromTableXpath = fromTableXpath;
	}


	public String getToTableXpath() {
		return toTableXpath;
	}


	public void setToTableXpath(String toTableXpath) {
		this.toTableXpath = toTableXpath;
	}

	Adaptation sourceDataSet = null;
	Adaptation targetDataSet = null;
	
	
	AdaptationTable sourceTable = null;
	AdaptationTable targetTable = null;
	AdaptationTable coreTable = null;
	
	private static final String stgPartyXpath = PartyPaths._STG_Party.getPathInSchema().format();
	private static final String stgAddressXpath = PartyPaths._STG_Address.getPathInSchema().format();
	private static final String stgBankAccountXpath = PartyPaths._STG_BankAccount.getPathInSchema().format();
	
	boolean isParty = false;
	boolean isAddress = false;
	boolean isBankAccount = false;
	Path stgCoreIdPath = null;
	Path srcSystemPath = null;
	
	Path partyIdPath = null;
	
	HashMap<String, ArrayList<Adaptation>> partyIdentifierMap = null;
	
	@Override
	public void execute(ScheduledExecutionContext arg0) throws OperationException, ScheduledTaskInterruption {

		Path srcPartyBusIdPath = null;
		
//		Path childTablePath = null;
//		Path corePartyBusIdPath = null;
//		Path coreSystemPath = null;
//		Path coreTablePath = null;
		
		Repository repo = arg0.getRepository();
		
		AdaptationHome srcDataSpace = RepositoryHelper.getDataSpace(repo, this.fromDataSpace);
		AdaptationHome targetDataSpace = RepositoryHelper.getDataSpace(repo, this.toDataSpace);
		
		
		sourceDataSet = RepositoryHelper.getDataSet(srcDataSpace, this.fromDataSet);
		targetDataSet = RepositoryHelper.getDataSet(targetDataSpace, this.toDataSet);
		
		
		sourceTable = RepositoryHelper.getTable(sourceDataSet, this.fromTableXpath);
		targetTable = RepositoryHelper.getTable(targetDataSet, this.toTableXpath);
		coreTable = targetTable;
		
			Date date = new Date();  
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
		    String curDate = formatter.format(date); 
		    
		String predicateCoreTable = "osd:is-not-null(./Control/ValidTo) and date-less-than(./Control/ValidFrom, '"
				.concat(curDate).concat("') ");
		
		VM.log.kernelInfo("Predicate -----------> ".concat(predicateCoreTable));
		VM.log.kernelInfo("source table -----------> ".concat(sourceTable.toPublicReference()));
		VM.log.kernelInfo("target table -----------> ".concat(targetTable.toPublicReference()));
		
//		String predicateStageTable = "";
		
		
		if(fromTableXpath.equalsIgnoreCase(stgPartyXpath)) {
			
			isParty = true;
//			predicateStageTable = "osd:is-not-null(./Party/CorePartyId) and osd:is-null(./Control/ValidTo)";
			srcPartyBusIdPath = PartyPaths._STG_Party._Control_SourcePartyBusId;
			srcSystemPath = PartyPaths._STG_Party._Control_SourceSystem;
			stgCoreIdPath = PartyPaths._STG_Party._Party_CorePartyId;
			//corePartyBusIdPath = PartyPaths._STG_Party._Control_SourcePartyBusId;
			//coreSystemPath = PartyPaths._STG_Party._Control_SourceSystem;
			
			
		}
			
		else if(fromTableXpath.equalsIgnoreCase(stgAddressXpath)) {
			
			isAddress = true;
			//predicate = predicate.concat("and osd:is-null(./Address/CoreAddressId))");
//			predicateStageTable = "osd:is-not-null(./Address/CoreAddressId) and osd:is-null(./Control/ValidTo)";
			
			srcPartyBusIdPath = PartyPaths._STG_Address._Control_SourcePartyBusId;
			srcSystemPath = PartyPaths._STG_Address._Control_SourceSystem;
//			childTablePath = PartyPaths._Party._Addresses;
			stgCoreIdPath = PartyPaths._STG_Address._Address_CoreAddressId;
			partyIdPath = PartyPaths._Address._AddressInfo_PartyId;
		}
		
		else if(fromTableXpath.equalsIgnoreCase(stgBankAccountXpath)) {
			
			isBankAccount = true;
			//predicate = predicate.concat("and osd:is-null(./Account/CoreBankAccountId))");
//			predicateStageTable = "osd:is-not-null(./Account/CoreBankAccountId) and osd:is-null(./Control/ValidTo)";
			
			srcPartyBusIdPath = PartyPaths._STG_BankAccount._Control_SourcePartyBusId;
			srcSystemPath = PartyPaths._STG_BankAccount._Control_SourceSystem;
//			childTablePath = PartyPaths._Party._BankAccounts;
			stgCoreIdPath = PartyPaths._STG_BankAccount._Account_CoreBankAccountId;
			partyIdPath = PartyPaths._BankAccount._Account_PartyId;
		}
		
		RequestResult expiredRecordsResult = coreTable.createRequestResult(predicateCoreTable);
		
		if(!expiredRecordsResult.isEmpty()) {
			
			
			ArrayList <Adaptation> listOfCoreRecordsToBeDeleted = new ArrayList<>();
			
			ProgrammaticService programmaticService = ProgrammaticService.createForSession(arg0.getSession(), targetDataSpace);
			
			for(Adaptation recordExpired; (recordExpired = expiredRecordsResult.nextAdaptation()) != null;) {
			
				listOfCoreRecordsToBeDeleted.add(recordExpired);
			}
			
			expiredRecordsResult.close();
			
			RequestResult stageFutureRecordsResult = sourceTable.createRequestResult(predicateCoreTable);
			
			if(stageFutureRecordsResult.isEmpty()) {
				
				//Perform delete on all the records.
				deleteExpiredRecords(listOfCoreRecordsToBeDeleted);
			}
			
			else {
				
				HashMap<String, ArrayList<Adaptation>> stageFutureRecordsMap = new HashMap<>();
				ArrayList <Adaptation> adapList = null;
			
				for(Adaptation stageFutureRecord; (stageFutureRecord = stageFutureRecordsResult.nextAdaptation()) != null;) {
					
					String stgSrcBusId = stageFutureRecord.getString(srcPartyBusIdPath);
					
					if(!stageFutureRecordsMap.containsKey(stgSrcBusId)) {
						
						adapList = new ArrayList<>();
						adapList.add(stageFutureRecord);
						
						stageFutureRecordsMap.put(stgSrcBusId, adapList);
					}
					
					else {
						
						stageFutureRecordsMap.get(stgSrcBusId).add(stageFutureRecord);
					}
					
				}
				stageFutureRecordsResult.close();
				
//				buildPartyIdentifiers();
				
				partyIdentifierMap = buildPartyIdentifierMap(targetDataSet);
				
				actOnExpiredRecords(listOfCoreRecordsToBeDeleted, stageFutureRecordsMap, programmaticService);
				
			}
			
		}
	}


	private HashMap<String, ArrayList<Adaptation>> buildPartyIdentifierMap(Adaptation dataset) {
		
		HashMap<String, ArrayList<Adaptation>> partyIdentifiersMap = new HashMap<>();
		
		AdaptationTable partyIdentifierTable = dataset.getTable(PartyPaths._PartyIdentifier.getPathInSchema());
		
		String partyIdentifiersPredicate = "osd:is-not-null(./PartyIdentifierId)";
		
		RequestResult partyIdentifierReqReslt = partyIdentifierTable.createRequestResult(partyIdentifiersPredicate);
		
		ArrayList <Adaptation> adapList = null;
		
		for(Adaptation partyIdentifier; (partyIdentifier = partyIdentifierReqReslt.nextAdaptation()) != null;) {
			
			String partySrcBusId = partyIdentifier.getString(PartyPaths._PartyIdentifier._SourcePartyBusId);
			
			if(!partyIdentifiersMap.containsKey(partySrcBusId)) {
				
				adapList = new ArrayList<>();
				adapList.add(partyIdentifier);
				
				partyIdentifiersMap.put(partySrcBusId, adapList);
			}
			
			else {
				
				partyIdentifiersMap.get(partySrcBusId).add(partyIdentifier);
			}
			
		}
		partyIdentifierReqReslt.close();
		return partyIdentifiersMap;
	}
	
	private void actOnExpiredRecords(ArrayList<Adaptation> listOfCoreRecordsToBeDeleted, HashMap<String, ArrayList<Adaptation>> stageFutureRecordsMap, ProgrammaticService programmaticService) {
		
		ArrayList<Adaptation> deleteRecordList = new ArrayList<>();
		
		
		
		for(Adaptation coreRecord : listOfCoreRecordsToBeDeleted) {
			
			Adaptation coreParty = coreRecord;
			
			if(isAddress) {
				
				coreParty = coreRecord.getSchemaNode().getNode(partyIdPath).getFacetOnTableReference().getLinkedRecord(coreRecord);
				
			}
			
			if(isBankAccount) {
				
				coreParty = coreRecord.getSchemaNode().getNode(partyIdPath).getFacetOnTableReference().getLinkedRecord(coreRecord);
			}
			
			//Update with PartyIdentifiers
			RequestResult partyIdentifiers = coreParty.getSchemaNode().getNode(PartyPaths._Party._Addresses).getAssociationLink().getAssociationResult(coreParty);
			
			for(Adaptation partyIdentifier; (partyIdentifier = partyIdentifiers.nextAdaptation()) != null;) {
				
				if(stageFutureRecordsMap.containsKey(partyIdentifier.getString(PartyPaths._PartyIdentifier._SourcePartyBusId))) {
					
				}
				
			}
			
			/*for(Adaptation partyIdentifier; (partyIdentifier = partyIdentifiers.nextAdaptation()) != null;) {
				
				if(stageFutureRecordsMap.containsKey(partyIdentifier.getString(PartyPaths._PartyIdentifier._SourcePartyBusId))) {
					
					ArrayList<Adaptation> listOfStgRecords = stageFutureRecordsMap.get(partyIdentifier.getString(PartyPaths._PartyIdentifier._SourcePartyBusId));
					
					for(Adaptation stgRecord : listOfStgRecords) {
						
						if(stgRecord.getString(srcSystemPath).equalsIgnoreCase(partyIdentifier.getString(PartyPaths._PartyIdentifier._SourceSystem))) {
							
							migrateRecordToCore(stgRecord, coreRecord, coreParty, programmaticService);
						}
						else {
							deleteRecordList.add(coreRecord);
						}
					}
				}
				
			}*/
		}
		
		deleteExpiredRecords(deleteRecordList);
		
	}
	
	private void deleteExpiredRecords(ArrayList<Adaptation> listOfRecordsToBeDeleted) {
		
		
	}
	
//	private void migrateRecordToCore(Adaptation stgRecord, Adaptation coreRecordToBeUpdated, Adaptation corePartyId, ProgrammaticService programmaticService) {
//		
//		RecordValuesBean rvb = null;
//		
//		ProcedureResult procedureResult = null;
//		
//		Object coreIdUpdated = null;
//
//		HashMap<Path, Object> pathValueMap = null;
//		
//		String stgSrcSystem = ""; 
//		String stgSrcSystemId = ""; 
//		
//		
//		if(isParty)	{	
//			
//			VM.log.kernelInfo("************************************* Detected as Party *************************************");
//			pathValueMap = mapStageToCorePartyData(stgRecord);
//			stgSrcSystem = stgRecord.getString(PartyPaths._STG_Party._Control_SourceSystem);
//			stgSrcSystemId = stgRecord.getString(PartyPaths._STG_Party._Control_SourcePartyBusId);
//		
//		}
//		
//		else if(isAddress) {
//		
//			VM.log.kernelInfo("************************************* Detected as Address *************************************");
//			pathValueMap = mapStageToCoreAddressData(stgRecord);
//			VM.log.kernelInfo("************************************* Respective Party Id *************************************".concat(corePartyId.getOccurrencePrimaryKey().format()));
//			pathValueMap.put(PartyPaths._Address._AddressInfo_PartyId, corePartyId.getOccurrencePrimaryKey().format());
//			
//			
//			stgSrcSystem = stgRecord.getString(PartyPaths._STG_Party._Control_SourceSystem);
//			stgSrcSystemId = stgRecord.getString(PartyPaths._STG_Party._Control_SourcePartyBusId);
//			
//		}
//		
//		else if(isBankAccount) {
//			
//			VM.log.kernelInfo("************************************* Detected as Bank Account *************************************");
//			pathValueMap = mapStageToCoreAccountData(stgRecord);
//			VM.log.kernelInfo("************************************* Respective Party Id *************************************".concat(corePartyId.getOccurrencePrimaryKey().format()));
//			pathValueMap.put(PartyPaths._BankAccount._Account_PartyId, corePartyId.getOccurrencePrimaryKey().format());
//			
//			stgSrcSystem = stgRecord.getString(PartyPaths._STG_Party._Control_SourceSystem);
//			stgSrcSystemId = stgRecord.getString(PartyPaths._STG_Party._Control_SourcePartyBusId);
//			
//			
//		}
//		
//		
//			VM.log.kernelInfo("************************************* Update Create *************************************");
//			
//			rvb = new RecordValuesBean();
//			rvb.setValues(pathValueMap);
//			rvb.setTable(targetTable);
//			rvb.setRecord(coreRecordToBeUpdated);
//			
//			UpdateRecords updateCoreRecords = new UpdateRecords(rvb);
//			procedureResult = programmaticService.execute(updateCoreRecords);
//			coreIdUpdated  = coreRecordToBeUpdated.getOccurrencePrimaryKey().format();
//		
//	
//		//Update Stage Table with the Core Id//
//		if(!procedureResult.hasFailed()) {
//			
//			rvb = new RecordValuesBean();
//			
//			pathValueMap.clear();
//			
//			pathValueMap.put(stgCoreIdPath, coreIdUpdated);
//			
//			rvb.setValues(pathValueMap);
//			rvb.setRecord(stgRecord);
//			rvb.setTable(stgRecord.getContainerTable());
//			
//			UpdateRecords updateRecords = new UpdateRecords(rvb);
//			
//			procedureResult = programmaticService.execute(updateRecords);
//			
//		}
//		
//	}
	
//	private HashMap<Path, Object> mapStageToCorePartyData(Adaptation recordToBeMigrated)
//	{
//
//		HashMap<Path, Object> pathValueMap = new HashMap<>();
//
//		pathValueMap.put(
//				PartyPaths._Party._PartyInfo_DataOwner,
//			recordToBeMigrated.get(PartyPaths._STG_Party._Party_DataOwner));
//
//		/*pathValueMap.put(
//				PartyPaths._Party._PartyInfo_PartyBusId,
//				recordToBeMigrated.get(PartyPaths._STG_Party._Party_PartyBusId));*/
//
//			pathValueMap.put(
//				PartyPaths._Party._PartyInfo_PartyType,
//				recordToBeMigrated.get(PartyPaths._STG_Party._Party_PartyType));
//
//			pathValueMap.put(
//				PartyPaths._Party._PartyInfo_RelationshipOwner,
//				recordToBeMigrated.get(PartyPaths._STG_Party._Party_RelationshipOwner));
//
//			pathValueMap.put(
//				PartyPaths._Party._PartyInfo_DataResponsible,
//				recordToBeMigrated.get(PartyPaths._STG_Party._Party_DataResponsible));
//
//			pathValueMap.put(
//				PartyPaths._Party._PartyInfo_Language,
//				recordToBeMigrated.get(PartyPaths._STG_Party._Party_Language));
//
//			pathValueMap.put(
//				PartyPaths._Party._PartyInfo_IsActive,
//				getIsActive(recordToBeMigrated));
//			
//			
//			//switch(recordToBeMigrated.getString(PartyPaths._STG_Party._Party_IsActive)) {
//	
//			String partyName = "";
//			
//			switch(recordToBeMigrated.getString(PartyPaths._STG_Party._Party_PartyType)) {
//			
//		
//			case Party.PartyType.PERSON://person
//
//				//For Person Information//
//				String lastName = recordToBeMigrated.getString(PartyPaths._STG_Party._Person_LastName);
//				String firstName = recordToBeMigrated.getString(PartyPaths._STG_Party._Person_FirstName);
//				
//				pathValueMap.put(
//					PartyPaths._Party._Person_LastName,
//					lastName);
//
//				pathValueMap.put(
//					PartyPaths._Party._Person_FirstName,
//					firstName);
//				
//				lastName = lastName != null ? lastName : "";
//				firstName = firstName != null ? firstName : "";
//				
//				partyName = firstName.concat(" ").concat(lastName);
//
//				pathValueMap.put(
//					PartyPaths._Party._Person_DateOfBirth,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Person_DateOfBirth));
//
//				pathValueMap.put(
//					PartyPaths._Party._Person_Gender,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Person_Gender));
//
//				pathValueMap.put(
//					PartyPaths._Party._Person_Title,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Person_Title));
//
//				pathValueMap.put(
//					PartyPaths._Party._Person_Salutation,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Person_Salutation));
//
//				pathValueMap.put(
//					PartyPaths._Party._Person_LetterSalutation,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Person_LetterSalutation));
//
//				pathValueMap.put(
//					PartyPaths._Party._Person_CivilStatus,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Person_CivilStatus));				
//				break;
//				
//			
//			case Party.PartyType.CONTACT_PERSON://contact person
//				//Contact Person//
//				pathValueMap.put(
//					PartyPaths._Party._ContactPerson_Department,
//					recordToBeMigrated.get(PartyPaths._STG_Party._ContactPerson_Department));
//				pathValueMap.put(
//					PartyPaths._Party._ContactPerson_Function,
//					recordToBeMigrated.get(PartyPaths._STG_Party._ContactPerson_Function));
//
//				pathValueMap.put(
//					PartyPaths._Party._ContactPerson_IsSeniorManager,
//					recordToBeMigrated.get(PartyPaths._STG_Party._ContactPerson_IsSeniorManager));
//				pathValueMap.put(
//					PartyPaths._Party._ContactPerson_IsUltimateBeneficialOwner,
//					recordToBeMigrated.get(PartyPaths._STG_Party._ContactPerson_IsUltimateBeneficialOwner));
//				pathValueMap.put(
//					PartyPaths._Party._ContactPerson_ContactPersonState,
//					recordToBeMigrated.get(PartyPaths._STG_Party._ContactPerson_ContactPersonState));
//				
//				String srcOrgBusId = recordToBeMigrated.getString(PartyPaths._STG_Party._ContactPerson_SourceOrganisationBusId);
//				String orgSrcSystem = recordToBeMigrated.getString(PartyPaths._STG_Party._Control_SourceSystem);
//				
//				//Add Organization Id//
//				if(srcOrgBusId != null && !srcOrgBusId.isEmpty()) {
//				
//					String contactOrgId = getCorePartyIdIfExists(srcOrgBusId, orgSrcSystem);
//				
//					if(contactOrgId != null) {
//						pathValueMap.put(PartyPaths._Party._ContactPerson_OrganisationId,
//							contactOrgId);
//					}
//				}
//				
//				break;
//				
//				
//			case Party.PartyType.ORGANIZTION://organization
//				//Organization//
//				pathValueMap.put(
//					PartyPaths._Party._Organisation_OrgName,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Organisation_OrgName));
//				
//				partyName = recordToBeMigrated.getString(PartyPaths._STG_Party._Organisation_OrgName);
//				
//					pathValueMap.put(
//					PartyPaths._Party._Organisation_OrgName2,
//				recordToBeMigrated.get(PartyPaths._STG_Party._Organisation_OrgName2));
//
//				pathValueMap.put(
//					PartyPaths._Party._Organisation_CompanyType,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Organisation_CompanyType));
//				pathValueMap.put(
//					PartyPaths._Party._Organisation_CommercialRegNo,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Organisation_CommercialRegNo));
//				pathValueMap.put(
//					PartyPaths._Party._Organisation_UIDNumber,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Organisation_UIDNumber));
//					
//				pathValueMap.put(
//					PartyPaths._Party._Organisation_Industry,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Organisation_Industry));
//				
//				String srcParentBusId = recordToBeMigrated.getString(PartyPaths._STG_Party._Organisation_SourceParentBusId);
//				String orgParentSrcSystem = recordToBeMigrated.getString(PartyPaths._STG_Party._Control_SourceSystem);
//				
//				//Add Organization Id//
//				if(srcParentBusId != null && !srcParentBusId.isEmpty()) {
//				
//					String parentOrgId = getCorePartyIdIfExists(srcParentBusId, orgParentSrcSystem);
//				
//					if(parentOrgId != null) {
//						pathValueMap.put(PartyPaths._Party._Organisation_ParentBusId,
//								parentOrgId);
//					}
//				}
//				
//				break;
//				
//			
//			default:
//				break;
//			
//			
//			}
//			
//			partyName = partyName != null ? partyName : "";
//			pathValueMap.put(PartyPaths._Party._PartyInfo_PartyName,
//					partyName);
//			
//			pathValueMap.put(PartyPaths._Party._PartyInfo_PartyNameSearchText,
//					standardizeSeaarchText(partyName));
//			
//		//Role//
//		
//			String roles = recordToBeMigrated.getString(PartyPaths._STG_Party._Role_Roles);
//			
//			pathValueMap.put(
//					PartyPaths._Party._Role_Roles,
//					roles);
//			
//				pathValueMap = deriveRolesForParty(pathValueMap, roles, ",");
//			
//				pathValueMap.put(
//					PartyPaths._Party._Role_IsVIPClient,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Role_IsVIPClient));
//				pathValueMap.put(
//					PartyPaths._Party._Role_Blacklist,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Role_Blacklist));
//				pathValueMap.put(
//					PartyPaths._Party._Role_PartnerState,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Role_PartnerState));
//				pathValueMap.put(
//					PartyPaths._Party._Role_CompanyPersonalNr,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Role_CompanyPersonalNr));
//				pathValueMap.put(
//					PartyPaths._Party._Role_SocialSecurityNr,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Role_SocialSecurityNr));
//				pathValueMap.put(
//					PartyPaths._Party._Role_SocialSecurityNrType,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Role_SocialSecurityNrType));
//				pathValueMap.put(
//					PartyPaths._Party._Role_BrokerRegNo,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Role_BrokerRegNo));
//				
//				String lineOfBusinessRoles = recordToBeMigrated.getString(PartyPaths._STG_Party._Role_LineOfBusiness);
//				
//				pathValueMap.put(
//					PartyPaths._Party._Role_LineOfBusiness,
//					lineOfBusinessRoles);
//				
//					pathValueMap = deriveLineOfBusinessRolesForParty(pathValueMap, lineOfBusinessRoles, ",");
//				
//				//Financial//
//				
//				pathValueMap.put(
//					PartyPaths._Party._Financial_FinancePartyBusId,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Financial_FinancePartyBusId));
//				pathValueMap.put(
//					PartyPaths._Party._Financial_PaymentTerms,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Financial_PaymentTerms));
//				pathValueMap.put(
//					PartyPaths._Party._Financial_FinanceClients,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Financial_FinanceClients));
//				pathValueMap.put(
//					PartyPaths._Party._Financial_CreditorClients,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Financial_CreditorClients));
//
//				// Control//
//				
//				/*pathValueMap.put(
//					PartyPaths._Party._Control_ValidFrom,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Control_ValidFrom));
//				pathValueMap.put(
//					PartyPaths._Party._Control_ValidTo,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Control_ValidTo));
//*/
//				pathValueMap.put(
//					PartyPaths._Party._Control_SourceSystem,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Control_SourceSystem));
//				/*pathValueMap.put(
//					PartyPaths._Party._Control_SourcePartyBusId,
//					recordToBeMigrated.get(PartyPaths._STG_Party._Control_SourcePartyBusId));
//				*/
//				
//					pathValueMap.put(
//							PartyPaths._Party._Control_LastUpdateTimestamp,
//							recordToBeMigrated.get(PartyPaths._STG_Party._Control_LastUpdateTimestamp));
//					pathValueMap.put(
//							PartyPaths._Party._Control_LastUpdatedBy,
//							recordToBeMigrated.get(PartyPaths._STG_Party._Control_ActionBy));
//					
//					pathValueMap.put(
//							PartyPaths._Party._Control_ValidFrom,
//							recordToBeMigrated.get(PartyPaths._STG_Party._Control_ValidFrom));
//					
//					pathValueMap.put(
//							PartyPaths._Party._Control_ValidTo,
//							recordToBeMigrated.get(PartyPaths._STG_Party._Control_ValidTo));
//		
//		return pathValueMap;
//	}


//	private boolean getIsActive(Adaptation recordToBeMigrated) {
//
//		boolean isActive = false;
//		
//		String partyType = String.valueOf(recordToBeMigrated.getString(PartyPaths._STG_Party._Party_PartyType));
//		String rolePartntState = String.valueOf(recordToBeMigrated.getString(PartyPaths._STG_Party._Role_PartnerState));
//		
//		if(partyType.equalsIgnoreCase("3")) {
//			
//			String cntctPerState = String.valueOf(recordToBeMigrated.getString(PartyPaths._STG_Party._ContactPerson_ContactPersonState));
//			
//			if(cntctPerState.equalsIgnoreCase("0"))
//				isActive =  false;
//			
//			else if(cntctPerState.equalsIgnoreCase("1"))
//				isActive = true;
//		}
//		
//		else if(!"null".equalsIgnoreCase(rolePartntState) && rolePartntState.equalsIgnoreCase("0")) {
//			
//			isActive = true;
//		}
//		
//		else {
//			
//			isActive =  recordToBeMigrated.get_boolean(PartyPaths._STG_Party._Party_IsActive);
//		}
//		
//		return isActive;
//		
//	}


//	private HashMap<Path, Object> mapStageToCoreAddressData(Adaptation recordToBeMigrated)
//	{
//		HashMap<Path, Object> pathValueMap = new HashMap<>();
//		
//		pathValueMap.put(
//				PartyPaths._Address._AddressInfo_DataOwner,
//			recordToBeMigrated.get(PartyPaths._STG_Address._Address_DataOwner));
//		
//		pathValueMap.put(
//				PartyPaths._Address._AddressInfo_AddressBusId,
//			recordToBeMigrated.get(PartyPaths._STG_Address._Address_AddressBusId));
//		
//		pathValueMap.put(
//				PartyPaths._Address._AddressInfo_AddressType,
//			recordToBeMigrated.get(PartyPaths._STG_Address._Address_AddressType));
//		
//		pathValueMap.put(
//				PartyPaths._Address._AddressInfo_AddressUsage,
//			recordToBeMigrated.get(PartyPaths._STG_Address._Address_AddressUsage));
//	
//		switch(recordToBeMigrated.getString(PartyPaths._STG_Address._Address_AddressType)) {
//		
//		case Party.AddressType.POSTAL_ADDRESS:
//			pathValueMap.put(
//					PartyPaths._Address._PostalAddress_Street,
//				recordToBeMigrated.get(PartyPaths._STG_Address._PostalAddress_Street));
//			pathValueMap.put(
//					PartyPaths._Address._PostalAddress_Street2,
//				recordToBeMigrated.get(PartyPaths._STG_Address._PostalAddress_Street2));
//			pathValueMap.put(
//					PartyPaths._Address._PostalAddress_Town,
//				recordToBeMigrated.get(PartyPaths._STG_Address._PostalAddress_Town));
//			pathValueMap.put(
//					PartyPaths._Address._PostalAddress_PostCode,
//				recordToBeMigrated.get(PartyPaths._STG_Address._PostalAddress_PostCode));
//			pathValueMap.put(
//					PartyPaths._Address._PostalAddress_POBoxPostCode,
//				recordToBeMigrated.get(PartyPaths._STG_Address._PostalAddress_POBoxPostCode));
//			pathValueMap.put(
//					PartyPaths._Address._PostalAddress_POBoxTown,
//				recordToBeMigrated.get(PartyPaths._STG_Address._PostalAddress_POBoxTown));
//			pathValueMap.put(
//					PartyPaths._Address._PostalAddress_District,
//				recordToBeMigrated.get(PartyPaths._STG_Address._PostalAddress_District));
//			pathValueMap.put(
//					PartyPaths._Address._PostalAddress_StateProvince,
//				recordToBeMigrated.get(PartyPaths._STG_Address._PostalAddress_StateProvince));
//			pathValueMap.put(
//					PartyPaths._Address._PostalAddress_Country,
//				recordToBeMigrated.get(PartyPaths._STG_Address._PostalAddress_Country));
//			break;
//			
//		case Party.AddressType.ELECTRONIC_ADDRESS:
//			
//			pathValueMap.put(
//					PartyPaths._Address._ElectronicAddress_ElectronicAddressType,
//				recordToBeMigrated.get(PartyPaths._STG_Address._ElectronicAddress_ElectronicAddressType));
//			
//			pathValueMap.put(
//					PartyPaths._Address._ElectronicAddress_Address,
//				recordToBeMigrated.get(PartyPaths._STG_Address._ElectronicAddress_Address));
//			
//			break;
//			
//			default:
//				break;
//		
//		}
//		
//	//Control//
//		
//		pathValueMap.put(
//				PartyPaths._Address._Control_SourceSystem,
//				recordToBeMigrated.get(PartyPaths._STG_Address._Control_SourceSystem));
//		
//		pathValueMap.put(
//					PartyPaths._Address._Control_LastUpdateTimestamp,
//					recordToBeMigrated.get(PartyPaths._STG_Address._Control_LastUpdateTimestamp));
//			pathValueMap.put(
//					PartyPaths._Address._Control_LastUpdatedBy,
//					recordToBeMigrated.get(PartyPaths._STG_Address._Control_ActionBy));
//		
//		pathValueMap.put(
//				PartyPaths._Address._Control_ValidFrom,
//				recordToBeMigrated.get(PartyPaths._STG_Address._Control_ValidFrom));
//		
//		pathValueMap.put(
//				PartyPaths._Address._Control_ValidTo,
//				recordToBeMigrated.get(PartyPaths._STG_Address._Control_ValidTo));
//		
//		return pathValueMap;
//	}
	
//	private HashMap<Path, Object> mapStageToCoreAccountData(Adaptation recordToBeMigrated)
//	{
//		HashMap<Path, Object> pathValueMap = new HashMap<>();
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_DataOwner,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_DataOwner));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_BankAccountBusId,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_BankAccountBusId));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_AccountType,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_AccountType));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_AccountNumber,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_AccountNumber));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_IBAN,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_IBAN));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_BIC,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_BIC));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_ClearingNumber,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_ClearingNumber));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_BankName,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_BankName));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_BankName,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_BankName));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_Country,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_Country));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_Currency,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_Currency));
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Account_IsMainAccount,
//			recordToBeMigrated.get(PartyPaths._STG_BankAccount._Account_IsMainAccount));
//		
//		//Control//
//		
//		pathValueMap.put(
//				PartyPaths._BankAccount._Control_SourceSystem,
//				recordToBeMigrated.get(PartyPaths._STG_BankAccount._Control_SourceSystem));
//			
//			pathValueMap.put(
//					PartyPaths._BankAccount._Control_CreationTimestamp,
//					recordToBeMigrated.get(PartyPaths._STG_BankAccount._Control_CreationTimestamp));
//			
//			pathValueMap.put(
//					PartyPaths._BankAccount._Control_LastUpdateTimestamp,
//					recordToBeMigrated.get(PartyPaths._STG_BankAccount._Control_CreationTimestamp));
//			
//			pathValueMap.put(
//					PartyPaths._BankAccount._Control_CreatedBy,
//					recordToBeMigrated.get(PartyPaths._STG_BankAccount._Control_ActionBy));
//
//			pathValueMap.put(
//					PartyPaths._BankAccount._Control_LastUpdateTimestamp,
//					recordToBeMigrated.get(PartyPaths._STG_BankAccount._Control_LastUpdateTimestamp));
//			pathValueMap.put(
//					PartyPaths._BankAccount._Control_LastUpdatedBy,
//					recordToBeMigrated.get(PartyPaths._STG_BankAccount._Control_ActionBy));
//			
//			pathValueMap.put(
//					PartyPaths._BankAccount._Control_ValidFrom,
//					recordToBeMigrated.get(PartyPaths._STG_BankAccount._Control_ValidFrom));
//			
//			pathValueMap.put(
//					PartyPaths._BankAccount._Control_ValidTo,
//					recordToBeMigrated.get(PartyPaths._STG_BankAccount._Control_ValidTo));		
//		
//		return pathValueMap;
//	}
	
//	private HashMap<Path, Object> deriveRolesForParty(HashMap<Path, Object> partyPathToValueMap, String roles, String delimiter){
//		
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsBeneficiary,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsInjuredPerson,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsInsuredPerson,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsElipsLifeLegalEntity,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsPolicyholder,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsReCoInsurer,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsVendor,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsStateRegulatoryAuthority,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsDistributionPartner,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsBroker,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsAssociation,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsBank,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsConsultant,
//				false);
//				
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsTPA,
//				false);
//		
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsInsurance,
//				false);
//		
//		if(roles != null && !roles.isEmpty()) {
//		
//		
//		List<String> listOfRoles = Arrays.asList(roles.split(delimiter));
//		
//		for(String role : listOfRoles) {
//			
//			
//			switch(role.trim()) {
//			
//			case Party.PartyRole.BENEFICIARY:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsBeneficiary,
//	 					true);
//				break;
//
//			case Party.PartyRole.INJURED_PERSON:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsInjuredPerson,
//	 					true);
//				break;
//
//			case Party.PartyRole.INSURED_PERSON:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsInsuredPerson,
//	 					true);
//				break;
//
//			case Party.PartyRole.ELIPSLIFE_LEGALENTITY:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsElipsLifeLegalEntity,
//	 					true);
//				break;
//
//			case Party.PartyRole.POLICY_HOLDER:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsPolicyholder,
//	 					true);
//				break;
//
//			case Party.PartyRole.RE_COINSURER:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsReCoInsurer,
//	 					true);
//				break;
//
//			case Party.PartyRole.VENDOR:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsVendor,
//	 					true);
//				break;
//
//			case Party.PartyRole.STATE_REGULATORY_AUTHORITY:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsStateRegulatoryAuthority,
//	 					true);
//				break;
//
//			case Party.PartyRole.DISTRIBUTION_PARTNER:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsDistributionPartner,
//	 					true);
//				break;
//
//			case Party.PartyRole.BROKER:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsBroker,
//	 					true);
//				break;
//
//			case Party.PartyRole.ASSOCIATION:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsAssociation,
//	 					true);
//				break;
//
//			case Party.PartyRole.BANK:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsBank,
//	 					true);
//				break;
//
//			case Party.PartyRole.CONSULLTANT:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsConsultant,
//	 					true);
//				break;
//
//			case Party.PartyRole.TPA:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsTPA,
//	 					true);
//				break;
//
//			case Party.PartyRole.INSURANCE:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsInsurance,
//	 					true);
//				break;
//			}
//			
//		}
//			}
//		return partyPathToValueMap;
//	}
		
//	private HashMap<Path, Object> deriveLineOfBusinessRolesForParty(HashMap<Path, Object> partyPathToValueMap, String linneOfBusRoles, String delimiter){
//
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsLHLOB,
//				false);
//		partyPathToValueMap.put(
//				PartyPaths._Party._Role_IsAHLOB,
//				false);
//		
//		if(linneOfBusRoles != null && !linneOfBusRoles.trim().isEmpty()) {
//		
//		List<String> listOfLOBRoles = Arrays.asList(linneOfBusRoles.split(delimiter));
//		
//		for(String lobRole : listOfLOBRoles) {
//			
//			
//			switch(lobRole.trim()) {
//			
//			case Party.LineOfBusiness.LHLOB:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsLHLOB,
//						true);
//				break;
//				
//			case Party.LineOfBusiness.AHLOB:
//				partyPathToValueMap.put(
//						PartyPaths._Party._Role_IsAHLOB,
//						true);
//				break;
//				
//			}
//			
//			}
//		}
//		return partyPathToValueMap;
//	}

//	private String standardizeSeaarchText(String input)	{
//	VM.log.kernelInfo("In Standardize Function --> ".concat(input));
//		//replace all lower Umlauts
//		String output = input.replace("ü", "ue")
//			.replace("ö", "oe")
//			.replace("ä", "ae")
//			.replace("ß", "ss");
//
//		//first replace all capital umlaute in a non-capitalized context (e.g. Übung)
//		output = output.replace("Ü(?=[a-zäöüß ])", "Ue")
//			.replace("Ö(?=[a-zäöüß ])", "Oe")
//			.replace("Ä(?=[a-zäöüß ])", "Ae");
//
//		//now replace all the other capital umlaute
//		output = output.replace("Ü", "UE").replace("Ö", "OE").replace("Ä", "AE");
//
//		VM.log.kernelInfo("After Umlauts--> ".concat(output));
//		
//		String regExReplaceSplChar = "[^a-zA-Z0-9]+";
//		
//		output = output.replaceAll(regExReplaceSplChar,"");
//		
//		VM.log.kernelInfo("After Reg Expression - Final Output--> ".concat(output));
//		
//		return output;
//	}
	
//	private String getCorePartyIdIfExists(String partyBusId, String srcSystem) {
//		
//		String partyId = null;
//		
//		if(partyBusId != null && partyIdentifierMap.containsKey(partyBusId)) {
//			
//			ArrayList<Adaptation> coreRecords = partyIdentifierMap.get(partyBusId);
//			
//			//String orgSrcSystem = recordToBeMigrated.getString(PartyPaths._STG_Party._Control_SourceSystem);
//			
//			for(Adaptation partyIdentifier : coreRecords) {
//				
//				if( srcSystem != null && 
//						srcSystem.equalsIgnoreCase(partyIdentifier.getString(PartyPaths._PartyIdentifier._SourceSystem))
//						) {
//					partyId = partyIdentifier.getString(PartyPaths._PartyIdentifier._PartyId);
//					break;
//				}
//			}
//		
//		}
//		
//		return partyId;
//	}

}
