package com.elipslife.ebx.data.access;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.LandingBankAccountBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class LandingBankAccountWriter extends BaseBankAccountWriter<LandingBankAccountBean> {

	private boolean useDefaultValues;
	
	public LandingBankAccountWriter(boolean useDefaultValues) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.LANDING_REFERENCE, PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_BankAccount.getPathInSchema()),
			useDefaultValues);
	}
	
	
	public LandingBankAccountWriter(AdaptationTable adaptationTable, boolean useDefaultValues) {
		super(new LandingBankAccountReader(adaptationTable), adaptationTable);
		
		this.useDefaultValues = useDefaultValues;
	}

	
	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, LandingBankAccountBean bean) {

		values.put(LandingPaths._LDG_BankAccount._Account_BankAccountId, bean.getId());
		
		values.put(LandingPaths._LDG_BankAccount._Account_AccountType, bean.getAccountType());
		values.put(LandingPaths._LDG_BankAccount._Account_AccountNumber, bean.getAccountNumber());
		values.put(LandingPaths._LDG_BankAccount._Account_IBAN, bean.getIban());
		values.put(LandingPaths._LDG_BankAccount._Account_BIC, bean.getBic());
		values.put(LandingPaths._LDG_BankAccount._Account_ClearingNumber, bean.getClearingNumber());
		values.put(LandingPaths._LDG_BankAccount._Account_BankName, bean.getBankName());
		values.put(LandingPaths._LDG_BankAccount._Account_Country, bean.getCountry());
		values.put(LandingPaths._LDG_BankAccount._Account_Currency, bean.getCurrency());
		values.put(LandingPaths._LDG_BankAccount._Account_CorrelationId, bean.getCorrelationId()); // Correlation Id
		values.put(LandingPaths._LDG_BankAccount._Account_LandingPartyId, bean.getLandingPartyId()); // Landing Party Id
		values.put(LandingPaths._LDG_BankAccount._Account_MdmStagingProcessResponse, bean.getMdmStagingProcessResponse()); // MDM Staging Process Response
		
		if(!useDefaultValues || bean.getIsMainAccount() != null) {
			values.put(LandingPaths._LDG_BankAccount._Account_IsMainAccount, bean.getIsMainAccount());
		}
		
		values.put(LandingPaths._LDG_BankAccount._Account_SourceBankAccountBusId, bean.getSourceBankAccountBusId());
		values.put(LandingPaths._LDG_BankAccount._Control_SourceSystem, bean.getSourceSystem());
		values.put(LandingPaths._LDG_BankAccount._Control_SourcePartyBusId, bean.getSourcePartyBusId());
		values.put(LandingPaths._LDG_BankAccount._Control_ValidFrom, bean.getValidFrom());
		values.put(LandingPaths._LDG_BankAccount._Control_ValidTo, bean.getValidTo());
		values.put(LandingPaths._LDG_BankAccount._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(LandingPaths._LDG_BankAccount._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(LandingPaths._LDG_BankAccount._Control_Action, bean.getAction());
		values.put(LandingPaths._LDG_BankAccount._Control_ActionBy, bean.getActionBy());
	}
}