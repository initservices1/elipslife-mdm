package com.elipslife.mdm.contract.landing;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class LandingExposureBean extends Entity<Integer> {
	
	private String exposureBusId;
	private String name;
	private String productBusId;
	private String productGroup;
	private String productName;
	private String sourceProductBusId;
	private Integer insuranceContractId;
	
	private String sourceSystem;
	private String sourceContractBusId;
	private Date creationTimestamp;
	private Date lastUpdateTimestamp;
	private Date lastSyncTimeStamp;
	private String action;
	private String actionBy;
	
	public String getExposureBusId() {
		return exposureBusId;
	}
	public void setExposureBusId(String exposureBusId) {
		this.exposureBusId = exposureBusId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProductBusId() {
		return productBusId;
	}
	public void setProductBusId(String productBusId) {
		this.productBusId = productBusId;
	}
	public String getProductGroup() {
		return productGroup;
	}
	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSourceProductBusId() {
		return sourceProductBusId;
	}
	public void setSourceProductBusId(String sourceProductBusId) {
		this.sourceProductBusId = sourceProductBusId;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getSourceContractBusId() {
		return sourceContractBusId;
	}
	public void setSourceContractBusId(String sourceContractBusId) {
		this.sourceContractBusId = sourceContractBusId;
	}
	
	
	public Integer getInsuranceContractId() {
		return insuranceContractId;
	}
	public void setInsuranceContractId(Integer insuranceContractId) {
		this.insuranceContractId = insuranceContractId;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActionBy() {
		return actionBy;
	}
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	public Date getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}
	public void setLastSyncTimeStamp(Date lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}
}