package com.elipslife.mdm.hr.landing;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.LandingHrModelPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class LandingOrganisationReader extends AbstractTableReader<LandingOrganisationBean> {

    public LandingOrganisationReader() {
        this(Constants.DataSpace.LANDING_HR_REFERENCE);
    }

    public LandingOrganisationReader(String dataspace) {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.LANDING_HR_MASTER),
				LandingHrModelPaths._Organisation.getPathInSchema()));
    }

    public LandingOrganisationReader(AdaptationTable table) {
		super(table);
	}

	@Override
    public LandingOrganisationBean createBeanFromRecord(Adaptation record) {
        LandingOrganisationBean bean = new LandingOrganisationBean();
        bean.setOrganisationId(record.get_int(LandingHrModelPaths._Organisation._Organisation_OrganisationId));
        bean.setDataOwner(record.getString(LandingHrModelPaths._Organisation._Organisation_DataOwner));
        bean.setOrganisationBusId(record.getString(LandingHrModelPaths._Organisation._Organisation_OrganisationBusId));
        bean.setOrganisationType(record.getString(LandingHrModelPaths._Organisation._Organisation_OrganisationType));
        bean.setParentBusId(record.getString(LandingHrModelPaths._Organisation._Organisation_ParentBusId));
        bean.setName(record.getString(LandingHrModelPaths._Organisation._Organisation_Name));
        bean.setManagerBusId(record.getString(LandingHrModelPaths._Organisation._Organisation_ManagerBusId));
        bean.setBuildingBusId(record.getString(LandingHrModelPaths._Organisation._Organisation_BuildingBusId));
        bean.setIsActive((Boolean)record.get(LandingHrModelPaths._Organisation._Organisation_IsActive));
        //Control
        bean.setSourceSystem(record.getString(LandingHrModelPaths._Organisation._Control_SourceSystem));
        bean.setCreatedBy(record.getString(LandingHrModelPaths._Organisation._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(LandingHrModelPaths._Organisation._Control_CreationTimestamp));
        bean.setLastUpdatedBy(record.getString(LandingHrModelPaths._Organisation._Control_LastUpdatedBy));
        bean.setLastUpdateTimeStamp(record.getDate(LandingHrModelPaths._Organisation._Control_LastUpdateTimestamp));
        
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{LandingHrModelPaths._Organisation._Organisation_OrganisationId};
    }

    @Override
    public Object[] retrievePks(LandingOrganisationBean bean) {
        return new Object[]{bean.getOrganisationId()};
    }

}
