package com.elipslife.ebx.data.access;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class LandingPartyBulkProcessWriterProcedureContext extends LandingPartyBulkProcessWriter {

    private final ProcedureContext procedureContext;

    public LandingPartyBulkProcessWriterProcedureContext(ProcedureContext procedureContext, boolean useDefaultValues) {
        super(useDefaultValues);
        this.procedureContext = procedureContext;
    }

    @Override
    protected void executeProcedure(Procedure procedure) {

        try {
            procedure.execute(this.procedureContext);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
