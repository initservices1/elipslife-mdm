package com.elipslife.ebx.data.access;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.LandingBankAccountBulkProcessBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class LandingBankAccountBulkProcessWriter extends BaseBankAccountWriter<LandingBankAccountBulkProcessBean> {

    private boolean useDefaultValues;

    public LandingBankAccountBulkProcessWriter(boolean useDefaultValues) {

        this(RepositoryHelper.getTable(
            RepositoryHelper.getDataSet(PartyConstants.DataSpace.LANDING_REFERENCE, PartyConstants.DataSet.LANDING_MASTER),
            LandingPaths._LDG_BankAccount_BulkProcess.getPathInSchema()),
            useDefaultValues);
    }

    
    public LandingBankAccountBulkProcessWriter(boolean useDefaultValues, String dataspace) {

        this(RepositoryHelper.getTable(
            RepositoryHelper.getDataSet(dataspace, PartyConstants.DataSet.LANDING_MASTER),
            LandingPaths._LDG_BankAccount_BulkProcess.getPathInSchema()),
            useDefaultValues);
    }
    
    public LandingBankAccountBulkProcessWriter(AdaptationTable adaptationTable, boolean useDefaultValues) {
        super(new LandingBankAccountBulkProcessReader(adaptationTable), adaptationTable);

        this.useDefaultValues = useDefaultValues;
    }


    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, LandingBankAccountBulkProcessBean bean) {

        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_BankAccountId, bean.getId());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_AccountType, bean.getAccountType());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_AccountNumber, bean.getAccountNumber());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_IBAN, bean.getIban());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_BIC, bean.getBic());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_ClearingNumber, bean.getClearingNumber());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_BankName, bean.getBankName());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_Country, bean.getCountry());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_Currency, bean.getCurrency());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_CorrelationId, bean.getCorrelationId()); // set correlation id
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_LandingPartyId, bean.getLandingPartyId()); // Landing Party Id
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_MdmStagingProcessResponse, bean.getMdmStagingProcessResponse()); // MDM Staging Process Response
        
        if (!useDefaultValues || bean.getIsMainAccount() != null) {
            values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_IsMainAccount, bean.getIsMainAccount());
        }
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Account_SourceBankAccountBusId, bean.getSourceBankAccountBusId());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Control_SourceSystem, bean.getSourceSystem());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Control_SourcePartyBusId, bean.getSourcePartyBusId());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Control_ValidFrom, bean.getValidFrom());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Control_ValidTo, bean.getValidTo());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Control_CreationTimestamp, bean.getCreationTimestamp());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Control_Action, bean.getAction());
        values.put(LandingPaths._LDG_BankAccount_BulkProcess._Control_ActionBy, bean.getActionBy());
    }
}
