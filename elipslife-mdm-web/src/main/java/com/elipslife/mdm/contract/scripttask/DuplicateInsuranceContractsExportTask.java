package com.elipslife.mdm.contract.scripttask;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.elipslife.mdm.claimcase.core.ClaimCaseBean;
import com.elipslife.mdm.claimcase.core.ClaimCaseReader;
import com.elipslife.mdm.contract.core.CoreInsuranceContractBean;
import com.elipslife.mdm.contract.core.CoreInsuranceContractReader;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.elipslife.mdm.uw.core.UWCaseBean;
import com.elipslife.mdm.uw.core.UWCaseReader;
import com.onwbp.boot.VM;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.OperationException;

public class DuplicateInsuranceContractsExportTask extends ScheduledTask{
	
	List<CoreInsuranceContractBean> duplicateInsuranceContracts = new ArrayList<CoreInsuranceContractBean>();
	List<CoreInsuranceContractBean> nonDuplicateInsuranceContracts = new ArrayList<CoreInsuranceContractBean>();
	List<ClaimCaseBean> claimCaseBeans = new ArrayList<ClaimCaseBean>();
	List<UWCaseBean> uwCaseBeans = new ArrayList<UWCaseBean>();

	@Override
	public void execute(ScheduledExecutionContext aContext) throws OperationException, ScheduledTaskInterruption {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
		String currentDate = dateFormat.format(new Date());
		String ebxHomePath = System.getProperty("ebx.home");
		String excelFilePath = ebxHomePath.concat("/").concat("InsuranceContracts").concat("_").concat(currentDate).concat(".xlsx");
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			
			aContext.addExecutionInformation(String.format("Start : Export duplicate insurance contract in excel "+excelFilePath));
			VM.log.kernelDebug("Duplicate insurnace contract export start in "+excelFilePath);
			
			XSSFSheet sheet = workbook.createSheet("Insurance Contracts");
			headerLinesForTableInfo(sheet, workbook);
			dataLinesTableForTableInfo(workbook, sheet);
			
			XSSFSheet sheet1 = workbook.createSheet("ClaimCase References");
			headerLinesForClainCaseInfo(sheet1, workbook);
			dataLinesTableForClaimCaseInfo(workbook, sheet1);
			
			XSSFSheet sheet2 = workbook.createSheet("UWCase References");
			headerLinesForUWCaswInfo(sheet2, workbook);
			dataLinesTableForUWCaseInfo(workbook, sheet2);
			
			FileOutputStream outputStream = new FileOutputStream(excelFilePath);
			
			try {
				workbook.write(outputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				workbook.close();
				// outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			aContext.addExecutionInformation(String.format("End : Exporting duplicate insurance contract in excel "+excelFilePath));
			VM.log.kernelDebug("Duplicate insurnace contract export completed in "+ebxHomePath.concat(excelFilePath) +" path");
			// workbook.write(outputStream);

		} catch (IOException e) {
			e.getMessage();
		}
		
	}
	
	/**
	 * Worksheet headers for duplicate and non duplicate insurance contracts
	 * 
	 * @param sheet
	 * @param workbook
	 */
	private void headerLinesForTableInfo(XSSFSheet sheet, XSSFWorkbook workbook) {
		int headerCellNumber = 0;
		Row headerRow = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = workbook.createFont();
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		style.setFont(font);

		Cell headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Insurance Contract Id");
		headerCell.setCellStyle(style);

		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Source System");
		headerCell.setCellStyle(style);

		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Source Contract Bus Id");
		headerCell.setCellStyle(style);
		
		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("To be deleted");
		headerCell.setCellStyle(style);

	}
	
	/**
	 * worksheet headers for claimcase 
	 * 
	 * @param sheet
	 * @param workbook
	 */
	
	private void headerLinesForClainCaseInfo(XSSFSheet sheet, XSSFWorkbook workbook) {
		int headerCellNumber = 0;
		Row headerRow = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = workbook.createFont();
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		style.setFont(font);

		Cell headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Claim Case Id");
		headerCell.setCellStyle(style);
		
		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Insurance Contract Id");
		headerCell.setCellStyle(style);

		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Source System");
		headerCell.setCellStyle(style);

		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Source Contract Bus Id");
		headerCell.setCellStyle(style);

	}
	
	/**
	 * headers for uwcase 
	 * 
	 * @param sheet
	 * @param workbook
	 */
	private void headerLinesForUWCaswInfo(XSSFSheet sheet, XSSFWorkbook workbook) {
		int headerCellNumber = 0;
		Row headerRow = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = workbook.createFont();
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		style.setFont(font);

		Cell headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("UWCase Id");
		headerCell.setCellStyle(style);
		
		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Insurance Contract Id");
		headerCell.setCellStyle(style);

		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Source System");
		headerCell.setCellStyle(style);

		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Source Contract Bus Id");
		headerCell.setCellStyle(style);

	}

	/**
	 * write duplicate and non duplicate insurance contracts
	 * 
	 * @param workbook
	 * @param sheet
	 */
	private void dataLinesTableForTableInfo(XSSFWorkbook workbook, XSSFSheet sheet) {
		
		getDuplicateInsuranceContractIds(); // get duplicate and non duplicate insurance contracts
		int rowCount = 1;
		for (CoreInsuranceContractBean coreInsuranceContractBean : duplicateInsuranceContracts) {
			
			int columnCount = 0;
			int columnNumber = 0;
			Row row = sheet.createRow(rowCount++);

			sheet.autoSizeColumn(columnNumber++);
			Cell cell = row.createCell(columnCount++);
			cell.setCellValue(coreInsuranceContractBean.getId());

			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(coreInsuranceContractBean.getSourceSystem());
			
			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(coreInsuranceContractBean.getSourceContractBusId());
			
			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue("Yes");

		}
		
		for (CoreInsuranceContractBean coreInsuranceContractBean : nonDuplicateInsuranceContracts) {
			
			int columnCount = 0;
			int columnNumber = 0;
			Row row = sheet.createRow(rowCount++);

			sheet.autoSizeColumn(columnNumber++);
			Cell cell = row.createCell(columnCount++);
			cell.setCellValue(coreInsuranceContractBean.getId());

			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(coreInsuranceContractBean.getSourceSystem());
			
			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(coreInsuranceContractBean.getSourceContractBusId());
			
			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue("No");

		}
		
		nonDuplicateInsuranceContracts.clear();
		duplicateInsuranceContracts.clear();
	}
	
	/**
	 * Write claim case references
	 * 
	 * @param workbook
	 * @param sheet
	 */
	private void dataLinesTableForClaimCaseInfo(XSSFWorkbook workbook, XSSFSheet sheet) {

		int rowCount = 1;
		for (ClaimCaseBean claimCaseBean : claimCaseBeans) {
			
			int columnCount = 0;
			int columnNumber = 0;
			Row row = sheet.createRow(rowCount++);

			sheet.autoSizeColumn(columnNumber++);
			Cell cell = row.createCell(columnCount++);
			cell.setCellValue(claimCaseBean.getId());
			
			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(claimCaseBean.getInsuranceContractId());

			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(claimCaseBean.getSourceSystem());
			
			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(claimCaseBean.getClaimCaseBusId());
		}
	}
	
	/**
	 * Write UWCase references in sheet
	 * 
	 * @param workbook
	 * @param sheet
	 */
	private void dataLinesTableForUWCaseInfo(XSSFWorkbook workbook, XSSFSheet sheet) {
		
		int rowCount = 1;
		for (UWCaseBean uwCaseBean : uwCaseBeans) {
			
			int columnCount = 0;
			int columnNumber = 0;
			Row row = sheet.createRow(rowCount++);

			sheet.autoSizeColumn(columnNumber++);
			Cell cell = row.createCell(columnCount++);
			cell.setCellValue(uwCaseBean.getUWCaseId());
			
			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(uwCaseBean.getInsuranceContractId());

			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(uwCaseBean.getSourceSystem());
			
			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(uwCaseBean.getUWCaseBusId());
		}
	}

	/**
	 * Prepare duplicate insurance contracts, claim case references and uw references
	 */
	private void getDuplicateInsuranceContractIds() {
		
		CoreInsuranceContractReader insuranceContractReader = new CoreInsuranceContractReader();
		ClaimCaseReader claimCaseReader = new ClaimCaseReader();
    	UWCaseReader uwCaseReader = new UWCaseReader();
    	
		List<CoreInsuranceContractBean> insuranceContractsList = insuranceContractReader.readAllBeans();
		
		Map<String, List<CoreInsuranceContractBean>> insuranceContractsGroupedBySourceSystem = insuranceContractsList.stream().collect(Collectors.groupingBy(cpb -> cpb.getSourceSystem() + cpb.getSourceContractBusId()));
		for (Map.Entry<String, List<CoreInsuranceContractBean>> sourceSysAndBusIdKey : insuranceContractsGroupedBySourceSystem.entrySet()) {
			List<CoreInsuranceContractBean> coreInsuranceContractsList = sourceSysAndBusIdKey.getValue();
			if (coreInsuranceContractsList.size() == 1) {
				// VM.log.kernelInfo("Single entry found for insurance contract source id "+coreInsuranceContractsList.get(0).getSourceContractBusId());
			} else {
				// Latest insurance contract based on last updated timestamp
				CoreInsuranceContractBean coreInsuranceContractBean = coreInsuranceContractsList.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getLastUpdateTimestamp(), a.getLastUpdateTimestamp())).findFirst().get();
				
				// remove latest insurance contract
				coreInsuranceContractsList.remove(coreInsuranceContractBean);
				duplicateInsuranceContracts.addAll(coreInsuranceContractsList);
				nonDuplicateInsuranceContracts.add(coreInsuranceContractBean);
				for(CoreInsuranceContractBean insuranceContractBean : coreInsuranceContractsList) {
					VM.log.kernelDebug("Duplicate entry found for insurance contract source id "+coreInsuranceContractsList.get(0).getSourceContractBusId());
					
					// find the references in Claim Case
					List<ClaimCaseBean> claimCaseList = claimCaseReader.findClaimCasesByInsuranceContracts(String.valueOf(insuranceContractBean.getId()));
					claimCaseBeans.addAll(claimCaseList);
					
					// find the references in UW
					List<UWCaseBean> uwCaseList = uwCaseReader.findBeansByInsuranceContractId(String.valueOf(insuranceContractBean.getId()));
					uwCaseBeans.addAll(uwCaseList);	
				}			
			}
		}
	}
}
