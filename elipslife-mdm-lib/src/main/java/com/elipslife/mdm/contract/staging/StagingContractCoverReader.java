package com.elipslife.mdm.contract.staging;

import java.util.List;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.text.Severity;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.ValidationReport;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate.Operation;

public class StagingContractCoverReader extends AbstractTableReader<StagingContractCoverBean> {

	public StagingContractCoverReader() {
		this(ContractConstants.DataSpace.CONTRACT_REF);
	}
	
	public StagingContractCoverReader(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						ContractConstants.DataSet.CONTRACT_MASTER),
				ContractPaths._STG_ContractCover.getPathInSchema()));
	}
	
	public StagingContractCoverReader(AdaptationTable table) {
		super(table);
	}

	@Override
	protected StagingContractCoverBean createBeanFromRecord(Adaptation record) {
		StagingContractCoverBean bean = new StagingContractCoverBean();
		
		bean.setId(record.get_int(ContractPaths._STG_ContractCover._Cover_ContractCoverId));
		bean.setExposureBusId(record.getString(ContractPaths._STG_ContractCover._Exposure_ExposureBusId));
		bean.setCoverBusId(record.getString(ContractPaths._STG_ContractCover._Cover_CoverBusId));
		bean.setCoverGroupType(record.getString(ContractPaths._STG_ContractCover._Cover_CoverGroupType));
		bean.setCoverGroupTypeName(record.getString(ContractPaths._STG_ContractCover._Cover_CoverGroupTypeName));
		bean.setCoverName(record.getString(ContractPaths._STG_ContractCover._Cover_CoverName));
		bean.setCoverTemplateBusId(record.getString(ContractPaths._STG_ContractCover._Cover_CoverTemplateBusId));
		bean.setBeneficiaryType(record.getString(ContractPaths._STG_ContractCover._Cover_BeneficiaryType));
		bean.setContractCoverMaster(record.getString(ContractPaths._STG_ContractCover._Exposure_ContractCoverMaster));
		bean.setSourceSystem(record.getString(ContractPaths._STG_ContractCover._Control_SourceSystem));
		bean.setCreationTimestamp(record.getDate(ContractPaths._STG_ContractCover._Control_CreationTimestamp));
		bean.setLastUpdateTimestamp(record.getDate(ContractPaths._STG_ContractCover._Control_LastUpdateTimestamp));
		bean.setLastSyncTimeStamp(record.getDate(ContractPaths._STG_ContractCover._Control_LastSyncTimestamp));
		bean.setAction(record.getString(ContractPaths._STG_ContractCover._Control_Action));
		bean.setActionBy(record.getString(ContractPaths._STG_ContractCover._Control_ActionBy));
		
		return bean;
	}

	@Override
	protected Path[] getPkPaths() {
		return new Path[] { ContractPaths._STG_ContractCover._Cover_ContractCoverId };
	}

	@Override
	protected Object[] retrievePks(StagingContractCoverBean bean) {
		return new Object[] { bean.getId() };
	}
	
	
	/**
	 * Checks for validation errors with severity error or fatal
	 * @param primaryKeys
	 * @return
	 */
	public boolean hasValidationErrors(Object[] primaryKeys) {
		
		Adaptation adaptation = super.findRecordByPks(primaryKeys);
		
		ValidationReport validationReport = adaptation.getValidationReport();
		
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		
		return hasErrors;
	}
	
	/**
	 * Get all staging records with Contract Cover Master is null
	 * 
	 * @return
	 */
	public List<StagingContractCoverBean> getAllBeansWithContractCoverMasterIsEmpty() {
		
		XPathPredicate predicate1 = new XPathPredicate(Operation.IsNull, ContractPaths._STG_ContractCover._Exposure_ContractCoverMaster, null);
		
		return super.readBeansFor(predicate1.toString());
	}
}