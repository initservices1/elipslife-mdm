/*

 */
package com.butos.ebx.smart.validation;

import java.util.*;

import com.onwbp.adaptation.*;
import com.onwbp.base.text.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.schema.*;

/**
 */
public abstract class SmartRecordAndTableLevelValidationCheck
	implements ConstraintOnTableWithRecordLevelCheck
{

	@Override
	public void checkRecord(final ValueContextForValidationOnRecord arg0)
	{

		ValueContext vc = arg0.getRecord();
		arg0.removeRecordFromMessages(vc);

		boolean isFstRec = true;
		Map<SchemaNode, HashSet<UserMessage>> validationInfoMap = new HashMap<SchemaNode, HashSet<UserMessage>>();
		try
		{
			this.executeBusinessValidations(vc, validationInfoMap, isFstRec);

			if ((validationInfoMap != null) && !validationInfoMap.isEmpty())
			{

				for (SchemaNode sn : validationInfoMap.keySet())
				{

					for (UserMessage um : validationInfoMap.get(sn))
					{

						arg0.addMessage(sn, um);
					}
				}
			}
		}

		finally
		{

			if ((validationInfoMap != null) && !validationInfoMap.isEmpty())
			{
				validationInfoMap.clear();
			}

		}
	}

	@Override
	public void checkTable(final ValueContextForValidationOnTable arg0)
	{

		AdaptationTable adapTable = arg0.getTable();

		ValueContext vc = null;

		RequestResult reqRes = adapTable.createRequestResult(null);

		Map<SchemaNode, HashSet<UserMessage>> validationInfoMap = new HashMap<SchemaNode, HashSet<UserMessage>>();
		boolean isFstRec = true;

		try
		{

			for (Adaptation adap; (adap = reqRes.nextAdaptation()) != null;)
			{

				vc = adap.createValueContext();

				this.executeBusinessValidations(vc, validationInfoMap, isFstRec);

				if ((validationInfoMap != null) && !validationInfoMap.isEmpty())
				{

					for (SchemaNode sn : validationInfoMap.keySet())
					{

						for (UserMessage um : validationInfoMap.get(sn))
						{

							arg0.addMessage(adap, sn, um);
						}
					}
				}

				isFstRec = false;
				if ((validationInfoMap != null) && !validationInfoMap.isEmpty())
				{
					validationInfoMap.clear();
				}
			}
		}

		finally
		{

			if (reqRes != null)
			{
				reqRes.close();
			}
		}

	}

	public void insertUserMsg(
		final Map<SchemaNode, HashSet<UserMessage>> validationInfoMap,
		final SchemaNode sn,
		final UserMessage userMsg)
	{

		HashSet<UserMessage> listOfUserMsg = null;

		if (validationInfoMap.containsKey(sn))
		{

			listOfUserMsg = validationInfoMap.get(sn);
			listOfUserMsg.add(userMsg);

		}

		else
		{

			listOfUserMsg = new HashSet<UserMessage>();
			listOfUserMsg.add(userMsg);
			validationInfoMap.put(sn, listOfUserMsg);
		}

	}

	protected abstract void executeBusinessValidations(
		ValueContext recordContext,
		Map<SchemaNode, HashSet<UserMessage>> validationInfoMap,
		boolean isFstRec);

}
