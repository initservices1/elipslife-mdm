package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;

import javax.json.bind.annotation.JsonbProperty;

public class Application {
	
	@JsonbProperty("AcceptedDate")
	public LocalDate acceptedDate;
	
	@JsonbProperty("ApplicationDate")
	public LocalDate applicationDate;
	
	@JsonbProperty("ApplicationNumber")
	public String applicationNumber;
	
	@JsonbProperty("ApplicationStateCode")
	public String applicationStateCode;
}