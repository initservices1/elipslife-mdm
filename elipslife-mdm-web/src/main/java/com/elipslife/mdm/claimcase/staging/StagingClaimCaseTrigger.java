package com.elipslife.mdm.claimcase.staging;

import java.util.Date;
import java.util.Optional;

import com.elipslife.mdm.claimcase.constants.ClaimCaseConstants;
import com.elipslife.mdm.claimcase.constants.ClaimCasePaths;
import com.elipslife.mdm.claimcase.core.ClaimCaseBean;
import com.elipslife.mdm.claimcase.core.ClaimCaseWriter;
import com.elipslife.mdm.claimcase.core.ClaimCaseWriterProcedureContext;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.Request;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.adaptation.XPathFilter;
import com.onwbp.boot.VM;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class StagingClaimCaseTrigger extends TableTrigger {

	@Override
	public void setup(TriggerSetupContext aContext) {

	}

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {
		StagingClaimCaseReader claimCaseReader = new StagingClaimCaseReader();
		StagingClaimCaseBean stagingClaimCaseBean = claimCaseReader.createBeanFromRecord(aContext.getAdaptationOccurrence());

		// only update if sourceSystem and claimCaseBusId are set, else do nothing
		//if (!stagingClaimCaseBean.getSourceSystem().isEmpty() && !stagingClaimCaseBean.getClaimCaseBusId().isEmpty()) {
		if((stagingClaimCaseBean.getSourceSystem() != null && stagingClaimCaseBean.getClaimCaseBusId() != null) && (!stagingClaimCaseBean.getSourceSystem().isEmpty() && !stagingClaimCaseBean.getClaimCaseBusId().isEmpty())) {	
			moveClaimCaseFromStagingToMaster(aContext.getSession(), aContext.getProcedureContext(), aContext.getAdaptationOccurrence());
		}
	}

	public void moveClaimCaseFromStagingToMaster(Session session, ProcedureContext context, Adaptation adaptation) {
		// instantiate bean, reader and writer
		ClaimCaseBean claimCaseBean = new ClaimCaseBean();
		StagingClaimCaseReader claimCaseReader = new StagingClaimCaseReader();
		StagingClaimCaseBean stagingClaimCaseBean = claimCaseReader.createBeanFromRecord(adaptation);
		StagingClaimCaseWriter stagingClaimCaseWriter = new StagingClaimCaseWriterProcedureContext(context);
		ClaimCaseWriter claimCaseWriter = new ClaimCaseWriterProcedureContext(context);

		// check if update or not
		Adaptation claimCaseSet = RepositoryHelper.getDataSet(RepositoryHelper.getDataSpace(ClaimCaseConstants.DataSpace.CLAIMCASE_REF), ClaimCaseConstants.DataSet.CLAIMCASE_MASTER);
		AdaptationTable claimCaseTable = RepositoryHelper.getTable(claimCaseSet, ClaimCasePaths._ClaimCase.getPathInSchema());
		XPathFilter claimCaseFilter = XPathFilter.newFilter("./Control/SourceSystem=$sourceSytem and ./ClaimCase_General/ClaimCaseBusId=$claimCaseBusId");
		Request isUpdateRequest = claimCaseTable.createRequest();
		isUpdateRequest.setXPathFilter(claimCaseFilter);
		isUpdateRequest.setXPathParameter("sourceSytem", stagingClaimCaseBean.getSourceSystem());
		isUpdateRequest.setXPathParameter("claimCaseBusId", stagingClaimCaseBean.getClaimCaseBusId());
		RequestResult isUpdateRequestResult = isUpdateRequest.execute();

		if (!isUpdateRequestResult.isEmpty()) {
			for (Adaptation adap; (adap = isUpdateRequestResult.nextAdaptation()) != null;) {
				Integer adapKey = adap.get_int(ClaimCasePaths._ClaimCase._ClaimCase_General_ClaimCaseId);
				Adaptation record = claimCaseTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(String.valueOf(adapKey)));
				Date coreRecordLastUpdateTimeStamp = record.getDate(ClaimCasePaths._ClaimCase._Control_LastUpdateTimestamp);
				Date stagingUpdateTimeStamp = stagingClaimCaseBean.getLastUpdateTimeStamp();

				if (stagingUpdateTimeStamp != null
						&& (coreRecordLastUpdateTimeStamp == null || stagingUpdateTimeStamp.after(coreRecordLastUpdateTimeStamp) || stagingUpdateTimeStamp.equals(coreRecordLastUpdateTimeStamp))) {
					// update ClaimCase in Core
					transferBean(claimCaseBean, stagingClaimCaseBean);
					// claimCaseBean.setLastUpdatedBy(stagingClaimCaseBean.getActionBy());
					// claimCaseBean.setLastUpdateTimeStamp(stagingClaimCaseBean.getLastUpdateTimeStamp());
					claimCaseBean.setLastSyncTimeStamp(new Date());
					claimCaseBean.setLastSyncAction(ClaimCaseConstants.RecordOperations.UPDATE);
					claimCaseBean.setId(record.get_int(ClaimCasePaths._ClaimCase._ClaimCase_General_ClaimCaseId));
					
					// set createdBy and creationTimeStamp again
					// claimCaseBean.setCreatedBy(record.getString(ClaimCasePaths._ClaimCase._Control_CreatedBy));
					// claimCaseBean.setCreationTimeStamp(record.getDate(ClaimCasePaths._ClaimCase._Control_CreationTimestamp));
					claimCaseWriter.updateBean(claimCaseBean);

					stagingClaimCaseBean.setClaimsMaster(String.valueOf(record.get_int(ClaimCasePaths._ClaimCase._ClaimCase_General_ClaimCaseId)));
					stagingClaimCaseWriter.updateBean(stagingClaimCaseBean);

				} else {
					// Old update
					VM.log.kernelWarn("*************************************************************************************************");
					VM.log.kernelWarn("Ignoring Record Update To Claim Case Master For Claim Case Stage Recod --> ".concat(stagingClaimCaseBean.getSourceSystem()).concat(" && ")
							.concat(stagingClaimCaseBean.getClaimCaseBusId()));
					VM.log.kernelWarn("*************************************************************************************************");
					stagingClaimCaseBean.setClaimsMaster(String.valueOf(record.get_int(ClaimCasePaths._ClaimCase._ClaimCase_General_ClaimCaseId)));
					// stagingClaimCaseBean.setLastUpdateTimeStamp(new Date());
					stagingClaimCaseBean.setLastSyncTimestamp(new Date());
					stagingClaimCaseWriter.updateBean(stagingClaimCaseBean);
				}
			}
			isUpdateRequestResult.close();
		} else {
			// create ClaimCase in Core
			transferBean(claimCaseBean, stagingClaimCaseBean);
			/// claimCaseBean.setCreatedBy(stagingClaimCaseBean.getActionBy());
			// claimCaseBean.setCreationTimeStamp(stagingClaimCaseBean.getCreationTimeStamp());
			claimCaseBean.setLastSyncTimeStamp(new Date());
			claimCaseBean.setLastSyncAction(ClaimCaseConstants.RecordOperations.CREATE);
			Optional<ClaimCaseBean> createdBeanOptional = claimCaseWriter.createBeanOrThrow(claimCaseBean);
			ClaimCaseBean createdBean =	createdBeanOptional.get();
			stagingClaimCaseBean.setClaimsMaster(String.valueOf(createdBean.getId()));
			stagingClaimCaseWriter.updateBean(stagingClaimCaseBean);
		}
	}

	public RequestResult getRequestResultFromXPathAndFilterForPartyIdentifier(XPathFilter filter, String crm, String busId, Request request) {
		request.setXPathFilter(filter);
		request.setXPathParameter("sourceSystem", crm);
		request.setXPathParameter("busId", busId);
		return request.execute();
	}

	public void transferBean(ClaimCaseBean claimCaseBean, StagingClaimCaseBean stagingClaimCaseBean) {
		// ignore this field for now
		// claimCaseBean.setInsuranceContractId();

		// ClaimCase_General
		claimCaseBean.setDataOwner(stagingClaimCaseBean.getDataOwner());
		claimCaseBean.setClaimCaseBusId(stagingClaimCaseBean.getClaimCaseBusId());
		claimCaseBean.setClaimCaseExtRef(stagingClaimCaseBean.getClaimCaseExtRef());
		claimCaseBean.setContractNumber(stagingClaimCaseBean.getContractNumber());
		claimCaseBean.setSourceContractBusId(stagingClaimCaseBean.getSourceContractBusId());
		claimCaseBean.setStartDate(stagingClaimCaseBean.getStartDate());
		claimCaseBean.setEndDate(stagingClaimCaseBean.getEndDate());
		claimCaseBean.setReportingDate(stagingClaimCaseBean.getReportingDate());
		claimCaseBean.setIsActive(stagingClaimCaseBean.getIsActive());
		claimCaseBean.setBusinessScope(stagingClaimCaseBean.getBusinessScope());
		
		// Party
		claimCaseBean.setClientSegment(stagingClaimCaseBean.getClientSegment());
		claimCaseBean.setPersonalCrimeTreatingDoctorName(stagingClaimCaseBean.getPersonalCrimeTreatingDoctorName());
		claimCaseBean.setPolicyHolderBusId(stagingClaimCaseBean.getPolicyHolderBusId());
		claimCaseBean.setSourcePolicyholderBusId(stagingClaimCaseBean.getSourcePolicyHolderBusId());
		claimCaseBean.setClaimDate(stagingClaimCaseBean.getClaimDate());
		claimCaseBean.setOperatingCountry(stagingClaimCaseBean.getOperatingCountry());
		claimCaseBean.setMainAffiliateBusId(stagingClaimCaseBean.getMainAffiliateBusId());
		claimCaseBean.setSourceMainAffiliateBusId(stagingClaimCaseBean.getSourceMainAffiliateBusId());
		claimCaseBean.setInsuredPersonBusId(stagingClaimCaseBean.getInsuredPersonBusId());
		claimCaseBean.setClaimStateDetail(stagingClaimCaseBean.getClaimStateDetail());
		claimCaseBean.setInsuredPersonType(stagingClaimCaseBean.getInsuredPersonType());
		claimCaseBean.setSourceInsuredPersonBusId(stagingClaimCaseBean.getSourceInsuredPersonBusId());
		claimCaseBean.setClaimManagerUserName(stagingClaimCaseBean.getClaimManagerUserName());
		claimCaseBean.setSourceClaimManagerBusId(stagingClaimCaseBean.getSourceClaimManagerBusId());
		claimCaseBean.setClaimMgtmTeamBusId(stagingClaimCaseBean.getClaimMgtmTeamBusId());
		claimCaseBean.setSourceClaimMgmtTeamBusId(stagingClaimCaseBean.getSourceClaimMgmtTeamBusId());

		// ClaimTypeDisability
		claimCaseBean.setAssessmentState(stagingClaimCaseBean.getAssessmentState());
		claimCaseBean.setBenefitStartDate(stagingClaimCaseBean.getBenefitStartDate());
		claimCaseBean.setBenefitEndDate(stagingClaimCaseBean.getBenefitEndDate());
		claimCaseBean.setCancellationDate(stagingClaimCaseBean.getCancellationDate());
		claimCaseBean.setCertifiedEndDate(stagingClaimCaseBean.getCertifiedEndDate());
		claimCaseBean.setClaimCause(stagingClaimCaseBean.getClaimCause());
		claimCaseBean.setDiagnosisDate(stagingClaimCaseBean.getDiagnosisDate());
		claimCaseBean.setDisabilityDate(stagingClaimCaseBean.getDisabilityDate());
		claimCaseBean.setHospitalStartDate(stagingClaimCaseBean.getHospitalStartDate());
		claimCaseBean.setLastDayWorked(stagingClaimCaseBean.getLastDayWorked());
		claimCaseBean.setReactivationDate(stagingClaimCaseBean.getReactivationDate());
		claimCaseBean.setReturnToWorkDate(stagingClaimCaseBean.getReturnToWorkDate());
		claimCaseBean.setSymptomsStartDate(stagingClaimCaseBean.getSymptomsStartDate());
		claimCaseBean.setTerminationdate(stagingClaimCaseBean.getTerminationdate());
		claimCaseBean.setTreatmentStartDate(stagingClaimCaseBean.getTreatmentStartDate());

		// ClaimtypeAbsence_US_AbsenceGeneral
		claimCaseBean.setAbsenceReason(stagingClaimCaseBean.getAbsenceReason());
		claimCaseBean.setAbsenceDurationType(stagingClaimCaseBean.getAbsenceDurationType());
		claimCaseBean.setAbsenceClaimState(stagingClaimCaseBean.getAbsenceClaimState());
		claimCaseBean.setClaimState(stagingClaimCaseBean.getClaimState());
		claimCaseBean.setClaimType(stagingClaimCaseBean.getClaimType());

		// ClaimtypeAbsence_US_AbsenceAdoption
		claimCaseBean.setAdoptionType(stagingClaimCaseBean.getAdoptionType());
		claimCaseBean.setAdoptionDate(stagingClaimCaseBean.getAdoptionDate());
		claimCaseBean.setIsAdoptionCourtCase(stagingClaimCaseBean.getIsAdoptionCourtCase());
		claimCaseBean.setAdoptionCourtDate(stagingClaimCaseBean.getAdoptionCourtDate());
		claimCaseBean.setAdoptionCourtHours(stagingClaimCaseBean.getAdoptionCourtHours());

		// ClaimtypeAbsence_US_AbsenceBereavement
		claimCaseBean.setBereaverRelationship(stagingClaimCaseBean.getBereaverRelationship());
		claimCaseBean.setBereaverRelationshipNotes(stagingClaimCaseBean.getBereaverRelationshipNotes());
		claimCaseBean.setBereavementDeathDate(stagingClaimCaseBean.getBereavementDeathDate());

		// ClaimtypeAbsence_US_AbsenceBonding
		claimCaseBean.setBondingParentType(stagingClaimCaseBean.getBondingParentType());
		claimCaseBean.setBondingEstimatedDeliveryDate(stagingClaimCaseBean.getBondingEstimatedDeliveryDate());
		claimCaseBean.setBondingNumDelivered(stagingClaimCaseBean.getBondingNumDelivered());
		claimCaseBean.setIsBondingPartnerEmployerSame(stagingClaimCaseBean.getIsBondingPartnerEmployerSame());
		claimCaseBean.setIsPartnerBonding(stagingClaimCaseBean.getIsPartnerBonding());
		claimCaseBean.setBondingPartnerName(stagingClaimCaseBean.getBondingPartnerName());
		claimCaseBean.setBondingPartnerStartDate(stagingClaimCaseBean.getBondingPartnerStartDate());
		claimCaseBean.setBondingPartnerEndDate(stagingClaimCaseBean.getBondingPartnerEndDate());
		claimCaseBean.setBondingLastDisabilityDate(stagingClaimCaseBean.getBondingLastDisabilityDate());

		// ClaimtypeAbsence_US_AbsenceBoneMarrow
		claimCaseBean.setIsBoneMarrowMedicalApptCase(stagingClaimCaseBean.getIsBoneMarrowMedicalApptCase());
		claimCaseBean.setBoneMarrowMedicalApptDate(stagingClaimCaseBean.getBoneMarrowMedicalApptDate());
		claimCaseBean.setBoneMarrowMedicalApptHours(stagingClaimCaseBean.getBoneMarrowMedicalApptHours());

		// ClaimtypeAbsence_US_AbsenceChildBirth
		claimCaseBean.setChildbirthDate(stagingClaimCaseBean.getChildbirthDate());
		claimCaseBean.setChildbirthEstimatedDate(stagingClaimCaseBean.getChildbirthEstimatedDate());
		claimCaseBean.setChildbirthNumDelivered(stagingClaimCaseBean.getChildbirthNumDelivered());
		claimCaseBean.setChildbirthDeliveryType(stagingClaimCaseBean.getChildbirthDeliveryType());
		claimCaseBean.setIsChildbirthBondingTimeCase(stagingClaimCaseBean.getIsChildbirthBondingTimeCase());
		claimCaseBean.setIsChildbirthComplicationCase(stagingClaimCaseBean.getIsChildbirthComplicationCase());
		claimCaseBean.setIsChildbirthPartnerBonding(stagingClaimCaseBean.getIsChildbirthPartnerBonding());
		claimCaseBean.setIsChildbirthPartnerEmployerSame(stagingClaimCaseBean.getIsChildbirthPartnerEmployerSame());
		claimCaseBean.setChildbirthPartnerName(stagingClaimCaseBean.getChildbirthPartnerName());
		claimCaseBean.setChildbirthPartnerBondingStartDate(stagingClaimCaseBean.getChildbirthPartnerBondingStartDate());
		claimCaseBean.setChildbirthPartnerBondingEndDate(stagingClaimCaseBean.getChildbirthPartnerBondingEndDate());
		claimCaseBean.setChildbirthLastDisabilityDate(stagingClaimCaseBean.getChildbirthLastDisabilityDate());
		claimCaseBean.setChildbirthTreatingDoctorName(stagingClaimCaseBean.getChildbirthTreatingDoctorName());
		claimCaseBean.setMilitaryLeaveType(stagingClaimCaseBean.getMilitaryLeaveType());
		claimCaseBean.setMilitaryMemberType(stagingClaimCaseBean.getMilitaryMemberType());

		// ClaimtypeAbsence_US_AbsenceFamilyMember
		claimCaseBean.setFamilyMemberName(stagingClaimCaseBean.getFamilyMemberName());
		claimCaseBean.setFamilyMemberRelationship(stagingClaimCaseBean.getFamilyMemberRelationship());
		claimCaseBean.setFamilyMemberRelationshipNotes(stagingClaimCaseBean.getFamilyMemberRelationshipNotes());
		claimCaseBean.setIsFamilyFollowUpApptCase(stagingClaimCaseBean.getIsFamilyFollowUpApptCase());
		claimCaseBean.setIsFamilyHospitalCase(stagingClaimCaseBean.getIsFamilyHospitalCase());
		claimCaseBean.setIsFamilyMemberADAQualified(stagingClaimCaseBean.getIsFamilyMemberADAQualified());
		claimCaseBean.setIsFamilyMemberOver18(stagingClaimCaseBean.getIsFamilyMemberOver18());
		claimCaseBean.setIsfamilyMemberConditionNonSerious(stagingClaimCaseBean.getIsfamilyMemberConditionNonSerious());
		claimCaseBean.setFamilyChildBirthDate(stagingClaimCaseBean.getFamilyChildBirthDate());
		claimCaseBean.setFamilyMemberHealthConditionNotes(stagingClaimCaseBean.getFamilyMemberHealthConditionNotes());
		claimCaseBean.setFamilyIllnessStartDate(stagingClaimCaseBean.getFamilyIllnessStartDate());
		claimCaseBean.setFamilyHospitalStartDate(stagingClaimCaseBean.getFamilyHospitalStartDate());
		claimCaseBean.setFamilyHospitalEndDate(stagingClaimCaseBean.getFamilyHospitalEndDate());
		claimCaseBean.setFamilyTreatmentStartDate(stagingClaimCaseBean.getFamilyTreatmentStartDate());
		claimCaseBean.setFamilyFollowUpApptDate(stagingClaimCaseBean.getFamilyFollowUpApptDate());
		claimCaseBean.setFamilyTreatingDoctorName(stagingClaimCaseBean.getFamilyTreatingDoctorName());

		// ClaimtypeAbsence_US_AbsenceMilitaryLeave
		claimCaseBean.setMilitaryLeaveType(stagingClaimCaseBean.getMilitaryLeaveType());
		claimCaseBean.setMilitaryMemberType(stagingClaimCaseBean.getMilitaryMemberType());
		claimCaseBean.setMilitaryMemberRelationship(stagingClaimCaseBean.getMilitaryMemberRelationship());
		claimCaseBean.setMilitaryMemberRelationshipNotes(stagingClaimCaseBean.getMilitaryMemberRelationshipNotes());

		// ClaimtypeAbsence_US_AbsenceOrganDonation
		claimCaseBean.setOrganDonating(stagingClaimCaseBean.getOrganDonating());
		claimCaseBean.setIsOrganDonorMedicalApptCase(stagingClaimCaseBean.getIsOrganDonorMedicalApptCase());
		claimCaseBean.setOrganDonorMedicalApptDate(stagingClaimCaseBean.getOrganDonorMedicalApptDate());
		claimCaseBean.setOrganDonorMedicalApptHours(stagingClaimCaseBean.getOrganDonorMedicalApptHours());

		// ClaimtypeAbsence_US_AbsenceOwnIllness
		claimCaseBean.setOwnIllnessInjuryType(stagingClaimCaseBean.getOwnIllnessInjuryType());
		claimCaseBean.setOwnIllnessHospitalCase(stagingClaimCaseBean.isOwnIllnessHospitalCase());
		claimCaseBean.setOwnIllnessFollowUpApptCase(stagingClaimCaseBean.isOwnIllnessFollowUpApptCase());
		claimCaseBean.setOwnIllnessWorkRelated(stagingClaimCaseBean.isOwnIllnessWorkRelated());
		claimCaseBean.setOwnMentalIllnesCase(stagingClaimCaseBean.isOwnMentalIllnesCase());
		claimCaseBean.setOwnIllnessStartDate(stagingClaimCaseBean.getOwnIllnessStartDate());
		claimCaseBean.setOwnIllnessHospitalStartDate(stagingClaimCaseBean.getOwnIllnessHospitalStartDate());
		claimCaseBean.setOwnIllnessHospitalEndDate(stagingClaimCaseBean.getOwnIllnessHospitalEndDate());
		claimCaseBean.setOwnIllnessFollowUpApptDate(stagingClaimCaseBean.getOwnIllnessFollowUpApptDate());
		claimCaseBean.setOwnIllnessTreatmentStartDate(stagingClaimCaseBean.getOwnIllnessTreatmentStartDate());
		claimCaseBean.setOwnIllnessTreatingDoctorName(stagingClaimCaseBean.getOwnIllnessTreatingDoctorName());

		// ClaimtypeAbsence_US_AbsencePersonalCrime
		claimCaseBean.setPersonalCrimeType(stagingClaimCaseBean.getPersonalCrimeType());
		claimCaseBean.setPersonalCrimeTypeNotes(stagingClaimCaseBean.getPersonalCrimeTypeNotes());
		claimCaseBean.setPersonalCrimeRelationship(stagingClaimCaseBean.getPersonalCrimeRelationship());
		claimCaseBean.setPersonalCrimeRelationshipNotes(stagingClaimCaseBean.getPersonalCrimeRelationshipNotes());
		claimCaseBean.setIsPersonalCrimeCounsellingCase(stagingClaimCaseBean.getIsPersonalCrimeCounsellingCase());
		claimCaseBean.setIsPersonalCrimeRestrainingOrderCase(stagingClaimCaseBean.getIsPersonalCrimeRestrainingOrderCase());
		claimCaseBean.setIsPersonalCrimeCourtCase(stagingClaimCaseBean.getIsPersonalCrimeCourtCase());
		claimCaseBean.setIsPersonalCrimeDoctorCase(stagingClaimCaseBean.getIsPersonalCrimeDoctorCase());
		claimCaseBean.setIsPersonalCrimeHospitalCase(stagingClaimCaseBean.getIsPersonalCrimeHospitalCase());
		claimCaseBean.setIsPersonalCrimeLegalAssistCase(stagingClaimCaseBean.getIsPersonalCrimeLegalAssistCase());
		claimCaseBean.setIsPersonalCrimeRelocationCase(stagingClaimCaseBean.getIsPersonalCrimeRelocationCase());
		claimCaseBean.setIsPersonalCrimeVictimServicesCase(stagingClaimCaseBean.getIsPersonalCrimeVictimServicesCase());
		claimCaseBean.setPersonalCrimeHospitalStartDate(stagingClaimCaseBean.getPersonalCrimeHospitalStartDate());
		claimCaseBean.setPersonalCrimeHospitalEndDate(stagingClaimCaseBean.getPersonalCrimeHospitalEndDate());

		// ClaimtypeAbsence_US_AbsencePersonalLeave
		claimCaseBean.setIsFMLAQualifyingEvent(stagingClaimCaseBean.getIsFMLAQualifyingEvent());

		// ClaimtypeAbsence_US_AbsenceSmallnecessities
		claimCaseBean.setSmallNecessitiesType(stagingClaimCaseBean.getSmallNecessitiesType());
		claimCaseBean.setCreationTimeStamp(stagingClaimCaseBean.getCreationTimeStamp());
		claimCaseBean.setLastUpdateTimeStamp(stagingClaimCaseBean.getLastUpdateTimeStamp());
		
		// Control
		claimCaseBean.setSourceSystem(stagingClaimCaseBean.getSourceSystem());
		if(stagingClaimCaseBean.getAction() != null && stagingClaimCaseBean.getAction().equals(PartyConstants.RecordOperations.CREATE)) {
		    claimCaseBean.setCreatedBy(stagingClaimCaseBean.getActionBy()); 
		}
		
		if(stagingClaimCaseBean.getAction() != null && stagingClaimCaseBean.getAction().equals(PartyConstants.RecordOperations.UPDATE)) {
            claimCaseBean.setLastUpdatedBy(stagingClaimCaseBean.getActionBy()); 
        }
		
	}
}