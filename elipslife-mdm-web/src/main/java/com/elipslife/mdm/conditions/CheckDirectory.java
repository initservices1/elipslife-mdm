package com.elipslife.mdm.conditions;

import java.io.File;

import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class CheckDirectory extends ConditionBean {
	private String inputDirectory;
	private String processedDirectory;
	private String errorDirectory;
	private String errorLogDirectory;
	private String accessErrorDirectory;

	@Override
	public boolean evaluateCondition(ConditionBeanContext aContext) throws OperationException {
		Boolean inputAccessible = checkIfDirectoryFromPathIsAccessible(inputDirectory);
		Boolean processedAccessible = checkIfDirectoryFromPathIsAccessible(processedDirectory);
		Boolean errorAccessible = checkIfDirectoryFromPathIsAccessible(errorDirectory);
		Boolean errorLogAccessible = checkIfDirectoryFromPathIsAccessible(errorLogDirectory);

		if (inputAccessible && processedAccessible && errorAccessible && errorLogAccessible) {
			// everything accessible
			return true;
				
		} else {
			// TODO: only email notification -> remove this, send paths which are configured
			if (!inputAccessible) {
				setAccessErrorDirectory(inputDirectory);
				//createLogFile("Error: Input Directory: Wrong path or directory not accessible.");
			}
			if (!processedAccessible) {
				setAccessErrorDirectory(processedDirectory);
				//createLogFile("Error: Directory: Wrong path or directory not accessible.");
			}
			if (!errorAccessible) {
				setAccessErrorDirectory(errorDirectory);
				//createLogFile("Error: Error Directory: Wrong path or directory not accessible.");
			}
			// TODO: can't create log if errorLog not accessible: Send Email Notification -> unable to access directory for every caseb
			if (!errorLogAccessible) {
				setAccessErrorDirectory(errorLogDirectory);
				//createLogFile("Error: Error Log Directory: Wrong path or directory not accessible.");
			}
			return false;
		}
	}

	public Boolean checkIfDirectoryFromPathIsAccessible(String path) {
		try {
			File file = new File(path);
			if (file.isDirectory()) {
				if (file.canWrite()) {
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	public String getErrorLogDirectory() {
		return errorLogDirectory;
	}

	public void setErrorLogDirectory(String errorLogDirectory) {
		this.errorLogDirectory = errorLogDirectory;
	}

	public String getInputDirectory() {
		return inputDirectory;
	}

	public void setInputDirectory(String inputDirectory) {
		this.inputDirectory = inputDirectory;
	}

	public String getProcessedDirectory() {
		return processedDirectory;
	}

	public void setProcessedDirectory(String processedDirectory) {
		this.processedDirectory = processedDirectory;
	}

	public String getErrorDirectory() {
		return errorDirectory;
	}

	public void setErrorDirectory(String outputDirectory) {
		this.errorDirectory = outputDirectory;
	}

	public String getAccessErrorDirectory() {
		return accessErrorDirectory;
	}

	public void setAccessErrorDirectory(String accessErrorDirectory) {
		this.accessErrorDirectory = accessErrorDirectory;
	}
}