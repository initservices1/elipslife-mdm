package com.elipslife.mdm.contract.core;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class CoreInsuranceContractBean extends Entity<Integer> {
	
	private String dataOwner;
	
	private String contractStageBusId;
	private String contractNumber;
	private String offerNumber;
	private String applicationNumber;
	private String contractBusId2;
	private String customerRefNumber;
	private String insuranceContractUUID;
	private String contractStage;
	private String contractState;
	private String offerState;
	private String applicationState;
	private String contractClass;
	private Date businessContractBeginDate;
	private Date contractExpiryDate;
	private Date contractBeginDate;
	private Date contractEndDate;
	private String businessScope;
	private String contractName;
	private String alternativeContractName;
	private String clientSegment;
	private String distributionAgreementBusId;
	private Boolean isActive;
	private Boolean isProfitParticipation;
	private String lineOfBusiness;
	private String operatingCountry;
	
	private String sourcePolicyholderBusId;
	private String policyholderBusId;
	private String policyholderId;
	private String sourceBrokerBusId;
	private String brokerBusId;
	private String brokerId;
	private String sourceAttachedPartnerBusId;
	private String attachedPartnerBusId;
	private String attachedPartnerId;
	private String sourceAdminResponsibleBusId;
	private String adminResponsibleUserName;
	private String adminResponsibleBusId;
	private String adminResponsibleId;
	private String sourceClaimResponsibleBusId;
	private String claimResponsibleUserName;
	private String claimResponsibleBusId;
	private String claimResponsibleId;
	private String sourceCaseMgmtResponsibleBusId;
	private String caseMgmtResponsibleUserName;
	private String caseMgmtResponsibleBusId;
	private String caseMgmtResponsibleId;
	private String sourceSalesResponsibleBusId;
	private String salesResponsibleUserName;
	private String salesResponsibleBusId;
	private String salesResponsibleId;
	
	private Boolean isExpired;
	private String sourceSystem;
	private String sourceContractBusId;
	private String createdBy;
	private Date creationTimestamp;
	private String lastUpdatedBy;
	private Date lastUpdateTimestamp;
	private String lastSyncAction;
	private Date lastSyncTimestamp;
	private Date validFrom;
	private Date validTo;
	
	public String getDataOwner() {
		return dataOwner;
	}
	public void setDataOwner(String dataOwner) {
		this.dataOwner = dataOwner;
	}
	public String getContractStageBusId() {
		return contractStageBusId;
	}
	public void setContractStageBusId(String contractStageBusId) {
		this.contractStageBusId = contractStageBusId;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getOfferNumber() {
		return offerNumber;
	}
	public void setOfferNumber(String offerNumber) {
		this.offerNumber = offerNumber;
	}
	public String getApplicationNumber() {
		return applicationNumber;
	}
	public void setApplicationNumber(String applicationNumber) {
		this.applicationNumber = applicationNumber;
	}
	public String getContractBusId2() {
		return contractBusId2;
	}
	public void setContractBusId2(String contractBusId2) {
		this.contractBusId2 = contractBusId2;
	}
	public String getCustomerRefNumber() {
		return customerRefNumber;
	}
	public void setCustomerRefNumber(String customerRefNumber) {
		this.customerRefNumber = customerRefNumber;
	}
	public String getInsuranceContractUUID() {
		return insuranceContractUUID;
	}
	public void setInsuranceContractUUID(String insuranceContractUUID) {
		this.insuranceContractUUID = insuranceContractUUID;
	}
	public String getContractStage() {
		return contractStage;
	}
	public void setContractStage(String contractStage) {
		this.contractStage = contractStage;
	}
	public String getContractState() {
		return contractState;
	}
	public void setContractState(String contractState) {
		this.contractState = contractState;
	}
	public String getOfferState() {
		return offerState;
	}
	public void setOfferState(String offerState) {
		this.offerState = offerState;
	}
	public String getApplicationState() {
		return applicationState;
	}
	public void setApplicationState(String applicationState) {
		this.applicationState = applicationState;
	}
	public String getContractClass() {
		return contractClass;
	}
	public void setContractClass(String contractClass) {
		this.contractClass = contractClass;
	}
	public Date getBusinessContractBeginDate() {
		return businessContractBeginDate;
	}
	public void setBusinessContractBeginDate(Date businessContractBeginDate) {
		this.businessContractBeginDate = businessContractBeginDate;
	}
	public Date getContractExpiryDate() {
		return contractExpiryDate;
	}
	public void setContractExpiryDate(Date contractExpiryDate) {
		this.contractExpiryDate = contractExpiryDate;
	}
	public Date getContractBeginDate() {
		return contractBeginDate;
	}
	public void setContractBeginDate(Date contractBeginDate) {
		this.contractBeginDate = contractBeginDate;
	}
	public Date getContractEndDate() {
		return contractEndDate;
	}
	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getBusinessScope() {
		return businessScope;
	}
	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}
	public String getContractName() {
		return contractName;
	}
	public void setContractName(String contractName) {
		this.contractName = contractName;
	}
	public String getAlternativeContractName() {
		return alternativeContractName;
	}
	public void setAlternativeContractName(String alternativeContractName) {
		this.alternativeContractName = alternativeContractName;
	}
	public String getClientSegment() {
		return clientSegment;
	}
	public void setClientSegment(String clientSegment) {
		this.clientSegment = clientSegment;
	}
	public String getDistributionAgreementBusId() {
		return distributionAgreementBusId;
	}
	public void setDistributionAgreementBusId(String distributionAgreementBusId) {
		this.distributionAgreementBusId = distributionAgreementBusId;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public Boolean getIsProfitParticipation() {
		return isProfitParticipation;
	}
	public void setIsProfitParticipation(Boolean isProfitParticipation) {
		this.isProfitParticipation = isProfitParticipation;
	}
	public String getLineOfBusiness() {
		return lineOfBusiness;
	}
	public void setLineOfBusiness(String lineOfBusiness) {
		this.lineOfBusiness = lineOfBusiness;
	}
	public String getOperatingCountry() {
		return operatingCountry;
	}
	public void setOperatingCountry(String operatingCountry) {
		this.operatingCountry = operatingCountry;
	}
	public String getSourcePolicyholderBusId() {
		return sourcePolicyholderBusId;
	}
	public void setSourcePolicyholderBusId(String sourcePolicyholderBusId) {
		this.sourcePolicyholderBusId = sourcePolicyholderBusId;
	}
	public String getPolicyholderBusId() {
		return policyholderBusId;
	}
	public void setPolicyholderBusId(String policyholderBusId) {
		this.policyholderBusId = policyholderBusId;
	}
	public String getPolicyholderId() {
		return policyholderId;
	}
	public void setPolicyholderId(String policyholderId) {
		this.policyholderId = policyholderId;
	}
	public String getSourceBrokerBusId() {
		return sourceBrokerBusId;
	}
	public void setSourceBrokerBusId(String sourceBrokerBusId) {
		this.sourceBrokerBusId = sourceBrokerBusId;
	}
	public String getBrokerBusId() {
		return brokerBusId;
	}
	public void setBrokerBusId(String brokerBusId) {
		this.brokerBusId = brokerBusId;
	}
	public String getBrokerId() {
		return brokerId;
	}
	public void setBrokerId(String brokerId) {
		this.brokerId = brokerId;
	}
	public String getSourceAttachedPartnerBusId() {
		return sourceAttachedPartnerBusId;
	}
	public void setSourceAttachedPartnerBusId(String sourceAttachedPartnerBusId) {
		this.sourceAttachedPartnerBusId = sourceAttachedPartnerBusId;
	}
	public String getAttachedPartnerBusId() {
		return attachedPartnerBusId;
	}
	public void setAttachedPartnerBusId(String attachedPartnerBusId) {
		this.attachedPartnerBusId = attachedPartnerBusId;
	}
	public String getAttachedPartnerId() {
		return attachedPartnerId;
	}
	public void setAttachedPartnerId(String attachedPartnerId) {
		this.attachedPartnerId = attachedPartnerId;
	}
	public String getSourceAdminResponsibleBusId() {
		return sourceAdminResponsibleBusId;
	}
	public void setSourceAdminResponsibleBusId(String sourceAdminResponsibleBusId) {
		this.sourceAdminResponsibleBusId = sourceAdminResponsibleBusId;
	}
	public String getAdminResponsibleUserName() {
		return adminResponsibleUserName;
	}
	public void setAdminResponsibleUserName(String adminResponsibleUserName) {
		this.adminResponsibleUserName = adminResponsibleUserName;
	}
	public String getAdminResponsibleBusId() {
		return adminResponsibleBusId;
	}
	public void setAdminResponsibleBusId(String adminResponsibleBusId) {
		this.adminResponsibleBusId = adminResponsibleBusId;
	}
	public String getAdminResponsibleId() {
		return adminResponsibleId;
	}
	public void setAdminResponsibleId(String adminResponsibleId) {
		this.adminResponsibleId = adminResponsibleId;
	}
	public String getSourceClaimResponsibleBusId() {
		return sourceClaimResponsibleBusId;
	}
	public void setSourceClaimResponsibleBusId(String sourceClaimResponsibleBusId) {
		this.sourceClaimResponsibleBusId = sourceClaimResponsibleBusId;
	}
	public String getClaimResponsibleUserName() {
		return claimResponsibleUserName;
	}
	public void setClaimResponsibleUserName(String claimResponsibleUserName) {
		this.claimResponsibleUserName = claimResponsibleUserName;
	}
	public String getClaimResponsibleBusId() {
		return claimResponsibleBusId;
	}
	public void setClaimResponsibleBusId(String claimResponsibleBusId) {
		this.claimResponsibleBusId = claimResponsibleBusId;
	}
	public String getClaimResponsibleId() {
		return claimResponsibleId;
	}
	public void setClaimResponsibleId(String claimResponsibleId) {
		this.claimResponsibleId = claimResponsibleId;
	}
	public String getSourceCaseMgmtResponsibleBusId() {
		return sourceCaseMgmtResponsibleBusId;
	}
	public void setSourceCaseMgmtResponsibleBusId(String sourceCaseMgmtResponsibleBusId) {
		this.sourceCaseMgmtResponsibleBusId = sourceCaseMgmtResponsibleBusId;
	}
	public String getCaseMgmtResponsibleUserName() {
		return caseMgmtResponsibleUserName;
	}
	public void setCaseMgmtResponsibleUserName(String caseMgmtResponsibleUserName) {
		this.caseMgmtResponsibleUserName = caseMgmtResponsibleUserName;
	}
	public String getCaseMgmtResponsibleBusId() {
		return caseMgmtResponsibleBusId;
	}
	public void setCaseMgmtResponsibleBusId(String caseMgmtResponsibleBusId) {
		this.caseMgmtResponsibleBusId = caseMgmtResponsibleBusId;
	}
	public String getCaseMgmtResponsibleId() {
		return caseMgmtResponsibleId;
	}
	public void setCaseMgmtResponsibleId(String caseMgmtResponsibleId) {
		this.caseMgmtResponsibleId = caseMgmtResponsibleId;
	}
	public String getSourceSalesResponsibleBusId() {
		return sourceSalesResponsibleBusId;
	}
	public void setSourceSalesResponsibleBusId(String sourceSalesResponsibleBusId) {
		this.sourceSalesResponsibleBusId = sourceSalesResponsibleBusId;
	}
	public String getSalesResponsibleUserName() {
		return salesResponsibleUserName;
	}
	public void setSalesResponsibleUserName(String salesResponsibleUserName) {
		this.salesResponsibleUserName = salesResponsibleUserName;
	}
	public String getSalesResponsibleBusId() {
		return salesResponsibleBusId;
	}
	public void setSalesResponsibleBusId(String salesResponsibleBusId) {
		this.salesResponsibleBusId = salesResponsibleBusId;
	}
	public String getSalesResponsibleId() {
		return salesResponsibleId;
	}
	public void setSalesResponsibleId(String salesResponsibleId) {
		this.salesResponsibleId = salesResponsibleId;
	}
	public Boolean getIsExpired() {
		return isExpired;
	}
	public void setIsExpired(Boolean isExpired) {
		this.isExpired = isExpired;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getSourceContractBusId() {
		return sourceContractBusId;
	}
	public void setSourceContractBusId(String sourceContractBusId) {
		this.sourceContractBusId = sourceContractBusId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getLastSyncAction() {
		return lastSyncAction;
	}
	public void setLastSyncAction(String lastSyncAction) {
		this.lastSyncAction = lastSyncAction;
	}
	public Date getLastSyncTimestamp() {
		return lastSyncTimestamp;
	}
	public void setLastSyncTimestamp(Date lastSyncTimestamp) {
		this.lastSyncTimestamp = lastSyncTimestamp;
	}
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	
}