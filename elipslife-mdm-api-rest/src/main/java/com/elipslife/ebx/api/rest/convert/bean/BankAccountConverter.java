package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.BankAccount;
import com.elipslife.ebx.api.rest.models.Party;
import com.elipslife.ebx.api.rest.models.SynchroniseParty;
import com.elipslife.ebx.domain.bean.LandingBankAccountBean;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class BankAccountConverter extends AbstractConverter<LandingBankAccountBean[], SynchroniseParty> {

	@Override
	public LandingBankAccountBean[] convert(SynchroniseParty synchroniseParty) throws DatatypeConfigurationException {

		ArrayList<LandingBankAccountBean> landingBankAccountBeans = new ArrayList<>();
		
		for(Party party : synchroniseParty.data ) {
			
			Date partyValidFrom = party.validFrom == null ? null :DateHelper.convertToDate(party.validFrom);
			Date partyValidTo = party.validTo == null ? null : DateHelper.convertToDate(party.validTo);
			// Don't process future party bank accounts
			boolean isWithinRange = dateWithinRange(partyValidFrom, partyValidTo);
            if (!isWithinRange) {
                continue;
            }
			
			for(BankAccount bankAccount : (party.bankAccounts == null ? new ArrayList<BankAccount>() : party.bankAccounts)) {
					
				LandingBankAccountBean landingBankAccountBean = new LandingBankAccountBean();
				landingBankAccountBeans.add(landingBankAccountBean);
				
				Date creationTimeStampBankAccount = bankAccount.creationTimestamp == null ? null : Date.from(bankAccount.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
				Date lastUpdateTimestampBankAccount = bankAccount.lastUpdateTimestamp == null ? null : Date.from(bankAccount.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
				Date validFromBankAccount = bankAccount.validFrom == null ? null : DateHelper.convertToDate(bankAccount.validFrom);
				Date validToBankAccount = bankAccount.validTo == null ? null : DateHelper.convertToDate(bankAccount.validTo);
				
				landingBankAccountBean.setAccountType(bankAccount.accountType);
				landingBankAccountBean.setAccountNumber(bankAccount.accountNumber);
				landingBankAccountBean.setIban(bankAccount.iban);
				landingBankAccountBean.setBic(bankAccount.bic);
				landingBankAccountBean.setClearingNumber(bankAccount.clearingNumber);
				landingBankAccountBean.setBankName(bankAccount.bankName);
				landingBankAccountBean.setCountry(bankAccount.country);
				landingBankAccountBean.setCurrency(bankAccount.currency);
				landingBankAccountBean.setIsMainAccount(bankAccount.isMainAccount);
				landingBankAccountBean.setCorrelationId(synchroniseParty.correlationId);
				landingBankAccountBean.setSourceBankAccountBusId(bankAccount.sourceBankAccountBusId);
				landingBankAccountBean.setSourceSystem(synchroniseParty.context.sourceSystemId);
				landingBankAccountBean.setSourcePartyBusId(party.partyBusId);
				landingBankAccountBean.setValidFrom(validFromBankAccount);
				landingBankAccountBean.setValidTo(validToBankAccount);
				landingBankAccountBean.setCreationTimestamp(creationTimeStampBankAccount);
				landingBankAccountBean.setLastUpdateTimestamp(lastUpdateTimestampBankAccount);
				landingBankAccountBean.setAction(bankAccount.action);
				landingBankAccountBean.setActionBy(synchroniseParty.context.userId);
			}
		}
		
		return landingBankAccountBeans.toArray(new LandingBankAccountBean[0]);
	}
	
	/**
     * 
     * @param dateFrom
     * @param dateTo
     * @return
     */
    private static boolean dateWithinRange(Date dateFrom, Date dateTo) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date dateNow = calendar.getTime();

        Date dateMinimum = new Date(Long.MIN_VALUE);
        Date dateMaximum = new Date(Long.MAX_VALUE);

        return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0 &&
            dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
    }
}