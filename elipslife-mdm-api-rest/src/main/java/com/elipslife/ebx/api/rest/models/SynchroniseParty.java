package com.elipslife.ebx.api.rest.models;

import java.util.Collection;

import javax.json.bind.annotation.JsonbProperty;

public class SynchroniseParty {
	
	@JsonbProperty("Context")
	public BusinessContext context;
	
	@JsonbProperty("Data")
	public Collection<Party> data;
	
	@JsonbProperty("CorrelationId")
	public String correlationId;
}