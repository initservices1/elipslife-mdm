package com.elipslife.mdm.common.trigger;

import com.elipslife.mdm.common.path.CommonReferencePaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.onwbp.base.text.UserMessageLocalized;
import com.onwbp.boot.VM;
import com.orchestranetworks.schema.trigger.BeforeDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public class AccountHierarchyGroupTrigger extends TableTrigger {

	@Override
	public void handleBeforeDelete(BeforeDeleteOccurrenceContext aContext) throws OperationException {
		
		VM.log.kernelDebug("[AccountHierarchyGroupTrigger][handleBeforeDelete] -- entry point");
		
		// dataset name
		Adaptation dataset = aContext.getAdaptationOccurrence().getContainer();
		
		// Group id
		Integer groupId = (Integer) aContext.getOccurrenceContext().getValue(CommonReferencePaths._NonGeneric_AccountHierarchyGroup._AccountHierarchyGroupId);
		
		// Account HierarchyLinks table
		AdaptationTable accountHierarchyLinkTable = dataset.getTable(CommonReferencePaths._NonGeneric_AccountHierarchyLink.getPathInSchema());
		
		// get all account hierarchy groups by account hierarchy group id
		RequestResult accountHierarchyGroups = accountHierarchyLinkTable.createRequestResult("./accountHierarchyGroupId='"+ String.valueOf(groupId) +"'");
		
		// if reference hierarchy groups are found, show the error messages
		if(!accountHierarchyGroups.isEmpty()) {
			// error messages
			UserMessageLocalized userMessage = new UserMessageLocalized();
			userMessage.setMessage("Please remove the referencing AccountHierarchyLinks, and try again!");
			throw OperationException.createError(userMessage);
		}
	}
	
	@Override
	public void setup(TriggerSetupContext arg0) { }

}
