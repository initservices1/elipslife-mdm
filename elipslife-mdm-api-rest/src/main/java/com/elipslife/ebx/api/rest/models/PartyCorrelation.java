package com.elipslife.ebx.api.rest.models;

import javax.json.bind.annotation.JsonbProperty;

public class PartyCorrelation {
	
	@JsonbProperty("CorrelationId")
	public String correlationId;

}
