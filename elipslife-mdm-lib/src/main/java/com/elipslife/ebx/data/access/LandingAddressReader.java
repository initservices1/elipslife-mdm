package com.elipslife.ebx.data.access;

import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.LandingAddressBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.xpath.filter.XPathPredicateGroup.LinkingType;

public class LandingAddressReader extends BaseAddressReader<LandingAddressBean> {

	public LandingAddressReader() {
		this(PartyConstants.DataSpace.LANDING_REFERENCE);
	}
	
	public LandingAddressReader(String dataSpace) {
		super(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_Address.getPathInSchema()));
	}
	
	public LandingAddressReader(AdaptationTable table) {
		super(table);
	}

	
	@Override
	public LandingAddressBean createBeanFromRecord(Adaptation record) {
		
		LandingAddressBean landingAddressBean = new LandingAddressBean();
		
		landingAddressBean.setId(record.get_int(LandingPaths._LDG_Address._Address_AddressId));
		
		landingAddressBean.setAddressType(record.getString(LandingPaths._LDG_Address._Address_AddressType));
		landingAddressBean.setAddressUsage(record.getString(LandingPaths._LDG_Address._Address_AddressUse));
		landingAddressBean.setSourceAddressBusId(record.getString(LandingPaths._LDG_Address._Address_SourceAddressBusId));
		landingAddressBean.setStreet(record.getString(LandingPaths._LDG_Address._PostalAddress_Street));
		landingAddressBean.setStreet2(record.getString(LandingPaths._LDG_Address._PostalAddress_Street2));
		landingAddressBean.setPostCode(record.getString(LandingPaths._LDG_Address._PostalAddress_PostCode));
		landingAddressBean.setPoBox(record.getString(LandingPaths._LDG_Address._PostalAddress_POBox));
		landingAddressBean.setPoBoxPostCode(record.getString(LandingPaths._LDG_Address._PostalAddress_POBoxPostCode));
		landingAddressBean.setPoBoxTown(record.getString(LandingPaths._LDG_Address._PostalAddress_POBoxTown));
		landingAddressBean.setDistrict(record.getString(LandingPaths._LDG_Address._PostalAddress_District));
		landingAddressBean.setTown(record.getString(LandingPaths._LDG_Address._PostalAddress_Town));
		landingAddressBean.setStateProvince(record.getString(LandingPaths._LDG_Address._PostalAddress_StateProvince));
		landingAddressBean.setCountry(record.getString(LandingPaths._LDG_Address._PostalAddress_Country));
		landingAddressBean.setCorrelationId(record.getString(LandingPaths._LDG_Address._Address_CorrelationId)); // Correlation Id
		landingAddressBean.setLandingPartyId(record.getString(LandingPaths._LDG_Address._Address_LandingPartyId)); // Landing Party Id
		landingAddressBean.setMdmStagingProcessResponse(record.getString(LandingPaths._LDG_Address._Address_MdmStagingProcessResponse)); // MDM Staging Process Response
		landingAddressBean.setSourceSystem(record.getString(LandingPaths._LDG_Address._Control_SourceSystem));
		landingAddressBean.setSourcePartyBusId(record.getString(LandingPaths._LDG_Address._Control_SourcePartyBusId));
		landingAddressBean.setValidFrom(record.getDate(LandingPaths._LDG_Address._Control_ValidFrom));
		landingAddressBean.setValidTo(record.getDate(LandingPaths._LDG_Address._Control_ValidTo));
		landingAddressBean.setCreationTimestamp(record.getDate(LandingPaths._LDG_Address._Control_CreationTimestamp));
		landingAddressBean.setLastUpdateTimestamp(record.getDate(LandingPaths._LDG_Address._Control_LastUpdateTimestamp));
		landingAddressBean.setAction(record.getString(LandingPaths._LDG_Address._Control_Action));
		landingAddressBean.setActionBy(record.getString(LandingPaths._LDG_Address._Control_ActionBy));
		landingAddressBean.setElectronicAddressType(record.getString(LandingPaths._LDG_Address._ElectronicAddress_ElectronicAddressType));
		landingAddressBean.setAddress(record.getString(LandingPaths._LDG_Address._ElectronicAddress_Address));
		
		return landingAddressBean;
	}
	
	/**
	 * Get all Landing Addresses by Source System and Source Party Busid
	 * 
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @return
	 */
	public List<LandingAddressBean> findLandingAddressesBySourceSystemAndSourcePartyBusId(String sourceSystem, String sourcePartyBusId) {
      
      XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, LandingPaths._LDG_Address._Control_SourceSystem, sourceSystem);
      XPathPredicate predicate2 = new XPathPredicate(XPathPredicate.Operation.Equals, LandingPaths._LDG_Address._Control_SourcePartyBusId, sourcePartyBusId);
      XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
      group.addPredicate(predicate1);
      group.addPredicate(predicate2);
      return super.readBeansFor(group.toString());
	}

	/**
	 * Find Landing Address beans by Landing PartyId
	 * 
	 * @param sourcePartyId
	 * @return
	 */
	public List<LandingAddressBean> findBeansByLandingPartyId(String sourcePartyId) {
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, LandingPaths._LDG_Address._Address_LandingPartyId, sourcePartyId);
		return readRecordsFor(predicate1.toString()).stream().map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
	@Override
	protected Path[] getPkPaths() {
		return new Path[] { LandingPaths._LDG_Address._Address_AddressId };
	}
	

	@Override
	protected Object[] retrievePks(LandingAddressBean bean) {

		return new Object[] { bean.getId() };
	}
	

	@Override
	protected Path getPathAddressId() {

		return LandingPaths._LDG_Address._Address_AddressId;
	}
	
	@Override
	protected Path getPathSourceAddressBusId() {

		return LandingPaths._LDG_Address._Address_SourceAddressBusId;
	}
	

	@Override
	protected Path getPathControlSourceSystem() {

		return LandingPaths._LDG_Address._Control_SourceSystem;
	}

	
	@Override
	protected Path getPathControlSourcePartyBusId() {

		return LandingPaths._LDG_Address._Control_SourcePartyBusId;
	}
	

	@Override
	protected Path getPathControlValidFrom() {

		return LandingPaths._LDG_Address._Control_ValidFrom;
	}
	

	@Override
	protected Path getPathControlValidTo() {

		return LandingPaths._LDG_Address._Control_ValidTo;
	}
}