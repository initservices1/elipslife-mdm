package com.elipslife.ebx.data.access;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.LandingAddressBulkProcessBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class LandingAddressBulkProcessReader extends BaseAddressReader<LandingAddressBulkProcessBean> {

    public LandingAddressBulkProcessReader() {
        this(PartyConstants.DataSpace.LANDING_REFERENCE);
    }

    public LandingAddressBulkProcessReader(String dataSpace) {

        super(RepositoryHelper.getTable(
            RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.LANDING_MASTER),
            LandingPaths._LDG_Address_BulkProcess.getPathInSchema()));
    }

    public LandingAddressBulkProcessReader(AdaptationTable table) {
        super(table);
    }


    @Override
    public LandingAddressBulkProcessBean createBeanFromRecord(Adaptation record) {

        LandingAddressBulkProcessBean landingAddressBulkProcessBean = new LandingAddressBulkProcessBean();

        landingAddressBulkProcessBean.setId(record.get_int(LandingPaths._LDG_Address_BulkProcess._Address_AddressId));

        landingAddressBulkProcessBean.setAddressType(record.getString(LandingPaths._LDG_Address_BulkProcess._Address_AddressType));
        landingAddressBulkProcessBean.setAddressUsage(record.getString(LandingPaths._LDG_Address_BulkProcess._Address_AddressUse));
        landingAddressBulkProcessBean.setSourceAddressBusId(record.getString(LandingPaths._LDG_Address_BulkProcess._Address_SourceAddressBusId));
        landingAddressBulkProcessBean.setLandingPartyId(record.getString(LandingPaths._LDG_Address_BulkProcess._Address_LandingPartyId)); // Landing Party Id
        landingAddressBulkProcessBean.setMdmStagingProcessResponse(record.getString(LandingPaths._LDG_Address_BulkProcess._Address_MdmStagingProcessResponse)); // MDM Staging Process Response
        landingAddressBulkProcessBean.setStreet(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_Street));
        landingAddressBulkProcessBean.setStreet2(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_Street2));
        landingAddressBulkProcessBean.setPostCode(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_PostCode));
        landingAddressBulkProcessBean.setPoBox(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_POBox));
        landingAddressBulkProcessBean.setPoBoxPostCode(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_POBoxPostCode));
        landingAddressBulkProcessBean.setPoBoxTown(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_POBoxTown));
        landingAddressBulkProcessBean.setDistrict(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_District));
        landingAddressBulkProcessBean.setTown(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_Town));
        landingAddressBulkProcessBean.setStateProvince(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_StateProvince));
        landingAddressBulkProcessBean.setCountry(record.getString(LandingPaths._LDG_Address_BulkProcess._PostalAddress_Country));
        landingAddressBulkProcessBean.setCorrelationId(record.getString(LandingPaths._LDG_Address_BulkProcess._Address_CorrelationId)); // set correlation id
        landingAddressBulkProcessBean.setSourceSystem(record.getString(LandingPaths._LDG_Address_BulkProcess._Control_SourceSystem));
        landingAddressBulkProcessBean.setSourcePartyBusId(record.getString(LandingPaths._LDG_Address_BulkProcess._Control_SourcePartyBusId));
        landingAddressBulkProcessBean.setValidFrom(record.getDate(LandingPaths._LDG_Address_BulkProcess._Control_ValidFrom));
        landingAddressBulkProcessBean.setValidTo(record.getDate(LandingPaths._LDG_Address_BulkProcess._Control_ValidTo));
        landingAddressBulkProcessBean.setCreationTimestamp(record.getDate(LandingPaths._LDG_Address_BulkProcess._Control_CreationTimestamp));
        landingAddressBulkProcessBean.setLastUpdateTimestamp(record.getDate(LandingPaths._LDG_Address_BulkProcess._Control_LastUpdateTimestamp));
        landingAddressBulkProcessBean.setAction(record.getString(LandingPaths._LDG_Address_BulkProcess._Control_Action));
        landingAddressBulkProcessBean.setActionBy(record.getString(LandingPaths._LDG_Address_BulkProcess._Control_ActionBy));
        landingAddressBulkProcessBean.setElectronicAddressType(record.getString(LandingPaths._LDG_Address_BulkProcess._ElectronicAddress_ElectronicAddressType));
        landingAddressBulkProcessBean.setAddress(record.getString(LandingPaths._LDG_Address_BulkProcess._ElectronicAddress_Address));

        return landingAddressBulkProcessBean;
    }
    
    /**
     * Get Landing Addresses which are created after the mentioned lastSyncTimestamp
     * 
     * @param lastSyncTimestamp
     * @return
     */
    public List<LandingAddressBulkProcessBean> getLandingAddressBulkProcessBeansByLastSyncTimeStamp(Date lastSyncTimestamp) {

        String dateNowString = DateHelper.convertToXpathFormat(lastSyncTimestamp);

        XPathPredicate predicate1 =
            new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, LandingPaths._LDG_Address._Control_LastSyncTimestamp, dateNowString);
        XPathPredicate predicate2 =
                new XPathPredicate(XPathPredicate.Operation.DateEqual, LandingPaths._LDG_Address._Control_LastSyncTimestamp, dateNowString);
        XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.Or);
        group.addPredicate(predicate1);
        group.addPredicate(predicate2);
        
        return readRecordsFor(group.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }


    @Override
    protected Path[] getPkPaths() {

        return new Path[] {LandingPaths._LDG_Address_BulkProcess._Address_AddressId};
    }


    @Override
    protected Object[] retrievePks(LandingAddressBulkProcessBean bean) {

        return new Object[] {bean.getId()};
    }


    @Override
    protected Path getPathAddressId() {

        return LandingPaths._LDG_Address_BulkProcess._Address_AddressId;
    }

    @Override
    protected Path getPathSourceAddressBusId() {

        return LandingPaths._LDG_Address_BulkProcess._Address_SourceAddressBusId;
    }


    @Override
    protected Path getPathControlSourceSystem() {

        return LandingPaths._LDG_Address_BulkProcess._Control_SourceSystem;
    }


    @Override
    protected Path getPathControlSourcePartyBusId() {

        return LandingPaths._LDG_Address_BulkProcess._Control_SourcePartyBusId;
    }


    @Override
    protected Path getPathControlValidFrom() {

        return LandingPaths._LDG_Address_BulkProcess._Control_ValidFrom;
    }


    @Override
    protected Path getPathControlValidTo() {

        return LandingPaths._LDG_Address_BulkProcess._Control_ValidTo;
    }
}
