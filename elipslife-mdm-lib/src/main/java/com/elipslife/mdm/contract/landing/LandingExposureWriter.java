package com.elipslife.mdm.contract.landing;

import java.util.Date;
import java.util.HashMap;

import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class LandingExposureWriter extends AbstractTableWriter<LandingExposureBean> {

	private LandingExposureReader landingExposureReader;
	
	public LandingExposureWriter() {
		this(PartyConstants.DataSpace.LANDING_REFERENCE);
	}
	
	public LandingExposureWriter(String dataSpace) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_Exposure.getPathInSchema()));
	}
	
	public LandingExposureWriter(AdaptationTable table) {
		super(table);
		
		this.landingExposureReader = new LandingExposureReader(table);
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, LandingExposureBean bean) {
		
		values.put(LandingPaths._LDG_Exposure._Exposure_ExposureId, bean.getId());
		values.put(LandingPaths._LDG_Exposure._Exposure_ExposureBusId, bean.getExposureBusId());
		values.put(LandingPaths._LDG_Exposure._Exposure_Name, bean.getName());
		values.put(LandingPaths._LDG_Exposure._Exposure_ProductBusId, bean.getProductBusId());
		values.put(LandingPaths._LDG_Exposure._Exposure_ProductGroup, bean.getProductGroup());
		values.put(LandingPaths._LDG_Exposure._Exposure_ProductName, bean.getProductName());
		values.put(LandingPaths._LDG_Exposure._Exposure_SourceProductBusId, bean.getSourceProductBusId());
		values.put(LandingPaths._LDG_Exposure._Exposure_InsuranceContractId, bean.getInsuranceContractId());
		
		values.put(LandingPaths._LDG_Exposure._Control_SourceSystem, bean.getSourceSystem());
		values.put(LandingPaths._LDG_Exposure._Control_SourceContractBusId, bean.getSourceContractBusId());
		values.put(LandingPaths._LDG_Exposure._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(LandingPaths._LDG_Exposure._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(LandingPaths._LDG_Exposure._Control_LastSyncTimestamp, new Date());
		values.put(LandingPaths._LDG_Exposure._Control_Action, bean.getAction());
		values.put(LandingPaths._LDG_Exposure._Control_ActionBy, bean.getActionBy());
	}

	@Override
	public AbstractTableReader<LandingExposureBean> getTableReader() {
		return this.landingExposureReader;
	}
}