package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.Cover;
import com.elipslife.ebx.api.rest.models.Exposure;
import com.elipslife.ebx.api.rest.models.InsuranceContract;
import com.elipslife.ebx.api.rest.models.SynchroniseInsuranceContract;
import com.elipslife.mdm.contract.landing.LandingContractCoverBean;

public class ContractCoverConverter
		extends AbstractConverter<LandingContractCoverBean[], SynchroniseInsuranceContract> {

	@Override
	public LandingContractCoverBean[] convert(SynchroniseInsuranceContract synchroniseInsuranceContract)
			throws DatatypeConfigurationException {

		ArrayList<LandingContractCoverBean> landingContractCoverBeans = new ArrayList<>();
		InsuranceContract insuranceContract = synchroniseInsuranceContract.data;
		for (Exposure exposure : insuranceContract.exposures == null ? new ArrayList<Exposure>() : insuranceContract.exposures) {
			for (Cover cover : exposure.covers == null ? new ArrayList<Cover>() : exposure.covers) {
				LandingContractCoverBean landingContractCoverBean = new LandingContractCoverBean();
				landingContractCoverBean.setExposureBusId(exposure.exposureBusId);
				landingContractCoverBean.setCoverBusId(cover.coverBusId);
				landingContractCoverBean.setCoverGroupType(cover.coverGroupType);
				landingContractCoverBean.setCoverGroupTypeName(cover.coverGroupTypeName);
				landingContractCoverBean.setCoverName(cover.coverName);
				landingContractCoverBean.setCoverTemplateBusId(cover.coverTemplateBusId);
				String beneficiaryType = cover.beneficiaryType == null ? "1" : cover.beneficiaryType;
				landingContractCoverBean.setBeneficiaryType(beneficiaryType);
				
				Date creationTimestamp = insuranceContract.creationTimestamp == null ? null : Date.from(insuranceContract.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
				Date lastUpdateTimestamp = insuranceContract.lastUpdateTimestamp == null ? null : Date.from(insuranceContract.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
				landingContractCoverBean.setSourceSystem(synchroniseInsuranceContract.context.sourceSystemId);
				landingContractCoverBean.setCreationTimestamp(creationTimestamp);
				landingContractCoverBean.setLastUpdateTimestamp(lastUpdateTimestamp);
				landingContractCoverBean.setAction(insuranceContract.action);
				landingContractCoverBean.setActionBy(synchroniseInsuranceContract.context.userId);

				landingContractCoverBeans.add(landingContractCoverBean);
			}
		}

		return landingContractCoverBeans.toArray(new LandingContractCoverBean[0]);
	}
}