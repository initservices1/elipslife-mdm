package com.elipslife.mdm.contract.landing;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class LandingInsuranceContractWriterProcedureContext extends LandingInsuranceContractWriter {

	private ProcedureContext procedureContext;
	
	public LandingInsuranceContractWriterProcedureContext(ProcedureContext procedureContext) {
		this(procedureContext, false);
	}
	
	public LandingInsuranceContractWriterProcedureContext(ProcedureContext procedureContext, boolean useDefaultValues) {
		super(useDefaultValues);
		this.procedureContext = procedureContext;
	}
	

	
	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}