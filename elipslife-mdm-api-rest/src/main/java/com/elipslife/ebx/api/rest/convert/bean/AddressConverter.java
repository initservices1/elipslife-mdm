package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.Address;
import com.elipslife.ebx.api.rest.models.Party;
import com.elipslife.ebx.api.rest.models.SynchroniseParty;
import com.elipslife.ebx.domain.bean.LandingAddressBean;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

public class AddressConverter extends AbstractConverter<LandingAddressBean[], SynchroniseParty> {

	@Override
	public LandingAddressBean[] convert(SynchroniseParty synchroniseParty) throws DatatypeConfigurationException {

		ArrayList<LandingAddressBean> landingAddressBeans = new ArrayList<>();
		
		for(Party party : synchroniseParty.data ) {
			
			Date partyValidFrom = party.validFrom == null ? null :DateHelper.convertToDate(party.validFrom);
			Date partyValidTo = party.validTo == null ? null : DateHelper.convertToDate(party.validTo);
			// Don't process future party address
			boolean isWithinRange = dateWithinRange(partyValidFrom, partyValidTo);
            if (!isWithinRange) {
                continue;
            }
			
			for(Address address : (party.addresses == null ? new ArrayList<Address>() : party.addresses)) {
					
				LandingAddressBean landingAddressBean = new LandingAddressBean();
				landingAddressBeans.add(landingAddressBean);
				
				Date creationTimeStampAddress = address.creationTimestamp == null ? null : Date.from(address.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
				Date lastUpdateTimestampAddress = address.lastUpdateTimestamp == null ? null : Date.from(address.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
				Date validFromAddress = address.validFrom == null ? null : DateHelper.convertToDate(address.validFrom);
				Date validToAddress = address.validTo == null ? null : DateHelper.convertToDate(address.validTo);
				
				landingAddressBean.setAddressType(address.addressType);
				landingAddressBean.setAddressUsage(address.addressUse);
				landingAddressBean.setSourceAddressBusId(address.sourceAddressBusId);
				landingAddressBean.setStreet(address.street);
				landingAddressBean.setStreet2(address.street2);
				landingAddressBean.setPostCode(address.postCode);
				landingAddressBean.setPoBox(address.poBox);
				landingAddressBean.setPoBoxPostCode(address.poBoxPostCode);
				landingAddressBean.setPoBoxTown(address.poBoxTown);
				landingAddressBean.setDistrict(address.district);
				landingAddressBean.setTown(address.town);
				landingAddressBean.setStateProvince(address.stateProvince);
				landingAddressBean.setCountry(address.country);
				landingAddressBean.setCorrelationId(synchroniseParty.correlationId);
				landingAddressBean.setElectronicAddressType(address.electronicAddressType);
				landingAddressBean.setAddress(address.address);
				landingAddressBean.setSourceSystem(synchroniseParty.context.sourceSystemId);
				landingAddressBean.setSourcePartyBusId(party.partyBusId);
				landingAddressBean.setValidFrom(validFromAddress);
				landingAddressBean.setValidTo(validToAddress);
				landingAddressBean.setCreationTimestamp(creationTimeStampAddress);
				landingAddressBean.setLastUpdateTimestamp(lastUpdateTimestampAddress);
				landingAddressBean.setAction(address.action);
				landingAddressBean.setActionBy(synchroniseParty.context.userId);
				landingAddressBean.setCorrelationId(synchroniseParty.correlationId);
			}
		}
		
		return landingAddressBeans.toArray(new LandingAddressBean[0]);
	}
	
    /**
     * 
     * @param dateFrom
     * @param dateTo
     * @return
     */
    private static boolean dateWithinRange(Date dateFrom, Date dateTo) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        Date dateNow = calendar.getTime();

        Date dateMinimum = new Date(Long.MIN_VALUE);
        Date dateMaximum = new Date(Long.MAX_VALUE);

        return dateNow.compareTo(dateFrom == null ? dateMinimum : dateFrom) >= 0 &&
            dateNow.compareTo(dateTo == null ? dateMaximum : dateTo) <= 0;
    }
}