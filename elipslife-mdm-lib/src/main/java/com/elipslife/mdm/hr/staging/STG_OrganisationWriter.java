package com.elipslife.mdm.hr.staging;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class STG_OrganisationWriter extends AbstractTableWriter<STG_OrganisationBean> {
    private STG_OrganisationReader stgOrganisationreader;

    public STG_OrganisationWriter() {
    	this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Organisation.getPathInSchema()));
    }
    
    public STG_OrganisationWriter(String dataspace) {
    	this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._STG_Organisation.getPathInSchema()));
    }

    public STG_OrganisationWriter(AdaptationTable table) {
    	super(table);
        this.stgOrganisationreader = new STG_OrganisationReader(table);
    }

    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, STG_OrganisationBean bean) {
        values.put(HrModelPaths._STG_Organisation._Organisation_OrganisationId, bean.getOrganisationId());
        values.put(HrModelPaths._STG_Organisation._Organisation_DataOwner, bean.getDataOwner());
        values.put(HrModelPaths._STG_Organisation._Organisation_OrganisationBusId, bean.getOrganisationBusId());
        values.put(HrModelPaths._STG_Organisation._Organisation_OrganisationType, bean.getOrganisationType());
        values.put(HrModelPaths._STG_Organisation._Organisation_ParentBusId, bean.getParentBusId());
        values.put(HrModelPaths._STG_Organisation._Organisation_Name, bean.getName());
        values.put(HrModelPaths._STG_Organisation._Organisation_ManagerBusId, bean.getManagerBusId());
        values.put(HrModelPaths._STG_Organisation._Organisation_BuildingBusId, bean.getBuildingBusId());
        values.put(HrModelPaths._STG_Organisation._Organisation_OrganisationMaster, bean.getOrganisationMaster());
        values.put(HrModelPaths._STG_Organisation._Organisation_IsActive, bean.getIsActive());
        values.put(HrModelPaths._STG_Organisation._Control_SourceSystem, bean.getSourceSystem());
		values.put(HrModelPaths._STG_Organisation._Control_CreatedBy, bean.getCreatedBy());
		values.put(HrModelPaths._STG_Organisation._Control_CreationTimestamp, bean.getCreationTimeStamp());
		values.put(HrModelPaths._STG_Organisation._Control_LastUpdatedBy, bean.getLastUpdatedBy());
		values.put(HrModelPaths._STG_Organisation._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
		values.put(HrModelPaths._STG_Organisation._Control_LastSyncTimestamp, bean.getLastSyncTimestamp());
    }

    @Override
    public AbstractTableReader<STG_OrganisationBean> getTableReader() {
        return stgOrganisationreader;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub
		
	}
}
