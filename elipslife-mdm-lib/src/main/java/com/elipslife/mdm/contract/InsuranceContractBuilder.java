package com.elipslife.mdm.contract;

import java.util.List;
import java.util.Optional;

import com.elipslife.ebx.data.access.CorePartyIdentifierReader;
import com.elipslife.ebx.data.access.CorePartyReader;
import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.ebx.domain.bean.convert.ContractConverter;
import com.elipslife.mdm.contract.core.CoreInsuranceContractBean;
import com.elipslife.mdm.contract.core.CoreInsuranceContractReader;
import com.elipslife.mdm.contract.core.CoreInsuranceContractWriter;
import com.elipslife.mdm.contract.core.CoreInsuranceContractWriterSession;
import com.elipslife.mdm.contract.staging.StagingInsuranceContractBean;
import com.elipslife.mdm.contract.staging.StagingInsuranceContractWriter;
import com.elipslife.mdm.contract.staging.StagingInsuranceContractWriterSession;
import com.elipslife.mdm.hr.core.EmployeeBean;
import com.elipslife.mdm.hr.core.EmployeeReader;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.AdaptationHome;
import com.orchestranetworks.service.Session;

public class InsuranceContractBuilder {
    
    public static void processStagingInsuranceContracts(AdaptationHome dataspaceName, String commitSize, List<StagingInsuranceContractBean> stagingInsuranceContractBeans) {
        
    }
	
	/**
	 * 
	 * @param session
	 * @param stagingInsuranceContractBean
	 */
	public static void createOrUpdateCoreInsuranceContractByStagingInsuranceContract(Session session, StagingInsuranceContractBean stagingInsuranceContractBean, boolean useDefaultValues) {
		
		StagingInsuranceContractWriter stagingInsuranceContractWriter = new StagingInsuranceContractWriterSession(session);
		CoreInsuranceContractWriter coreInsuranceContractWriter = new CoreInsuranceContractWriterSession(session, useDefaultValues);
		
		createOrUpdateCoreInsuranceContractByStagingInsuranceContract(stagingInsuranceContractWriter, coreInsuranceContractWriter, stagingInsuranceContractBean);
	}
	
	
	/**
	 * 
	 * @param stagingInsuranceContractWriter
	 * @param coreInsuranceContractWriter
	 * @param stagingInsuranceContractBean
	 */
	public static void createOrUpdateCoreInsuranceContractByStagingInsuranceContract(
			StagingInsuranceContractWriter stagingInsuranceContractWriter,
			CoreInsuranceContractWriter coreInsuranceContractWriter,
			StagingInsuranceContractBean stagingInsuranceContractBean) {
		
		CoreInsuranceContractReader coreInsuranceContractReader = (CoreInsuranceContractReader) coreInsuranceContractWriter.getTableReader();
		
		List<CoreInsuranceContractBean> coreInsuranceContractBeans = coreInsuranceContractReader.findBeansBySourceSystemAndSourceContractBusId(
				stagingInsuranceContractBean.getSourceSystem(),
				stagingInsuranceContractBean.getSourceContractBusId());
		
		if(coreInsuranceContractBeans.size() > 1) {
			throw new RuntimeException(
					String.format(
							"More than one insurance contract found with source system %s and source contract bus ID %s",
							stagingInsuranceContractBean.getSourceSystem(),
							stagingInsuranceContractBean.getSourceContractBusId()));
		}
		
		CoreInsuranceContractBean coreInsuranceContractBean = coreInsuranceContractBeans.isEmpty() ? null : coreInsuranceContractBeans.get(0);
		
		if(coreInsuranceContractBean != null) {
			
			// Update
			
			boolean updateCoreInsuranceContractBean = DateNullableComparator.getInstance().compare(stagingInsuranceContractBean.getLastUpdateTimestamp(), coreInsuranceContractBean.getLastUpdateTimestamp()) > 0;
			if(!updateCoreInsuranceContractBean) {
				stagingInsuranceContractBean.setInsuranceContractMaster(String.valueOf(coreInsuranceContractBean.getId()));
				stagingInsuranceContractWriter.updateBean(stagingInsuranceContractBean);
				return;
			}
			
			ContractConverter.fillCoreInsuranceContractBeanWithStagingInsuranceContractBeanBean(stagingInsuranceContractBean, coreInsuranceContractBean);
			
			linkReferences(coreInsuranceContractBean);
			
			coreInsuranceContractWriter.updateBean(coreInsuranceContractBean);
			stagingInsuranceContractBean.setInsuranceContractMaster(String.valueOf(coreInsuranceContractBean.getId()));
		}
		else {
			
			// Create
			
			coreInsuranceContractBean = ContractConverter.createCoreInsuranceContractBeanFromStagingInsuranceContractBean(stagingInsuranceContractBean);
			
			linkReferences(coreInsuranceContractBean);
			
			Optional<CoreInsuranceContractBean> coreInsuranceContractBeanOptional  = coreInsuranceContractWriter.createBeanOrThrow(coreInsuranceContractBean);
			stagingInsuranceContractBean.setInsuranceContractMaster(String.valueOf(coreInsuranceContractBeanOptional.get().getId()));
		}
		stagingInsuranceContractWriter.updateBean(stagingInsuranceContractBean);
	}
	
	
	/**
	 * Link Insurance Contract References
	 * 
	 * @param coreInsuranceContractBean
	 */
	public static CoreInsuranceContractBean linkReferences(CoreInsuranceContractBean coreInsuranceContractBean) {
		
		CorePartyIdentifierReader corePartyIdentifierReader = new CorePartyIdentifierReader();
		CorePartyReader corePartyReader = new CorePartyReader();
		
		CorePartyBean corePartyBeanPolicyHolder = findPartyByCrmIdOrSourceSystemAndPartyBusId(
				corePartyIdentifierReader,
				corePartyReader,
				coreInsuranceContractBean.getPolicyholderBusId(),
				coreInsuranceContractBean.getSourceSystem(),
				coreInsuranceContractBean.getSourcePolicyholderBusId());
		
		coreInsuranceContractBean.setPolicyholderId(corePartyBeanPolicyHolder != null ? corePartyBeanPolicyHolder.getId().toString() : null);
		
		CorePartyBean corePartyBeanBroker = findPartyByCrmIdOrSourceSystemAndPartyBusId(
				corePartyIdentifierReader,
				corePartyReader,
				coreInsuranceContractBean.getBrokerBusId(),
				coreInsuranceContractBean.getSourceSystem(),
				coreInsuranceContractBean.getSourceBrokerBusId());
		
		coreInsuranceContractBean.setBrokerId(corePartyBeanBroker != null ? corePartyBeanBroker.getId().toString() : null);
		
		CorePartyBean corePartyBeanAttachedPartner = findPartyByCrmIdOrSourceSystemAndPartyBusId(
				corePartyIdentifierReader,
				corePartyReader,
				coreInsuranceContractBean.getAttachedPartnerBusId(),
				coreInsuranceContractBean.getSourceSystem(),
				coreInsuranceContractBean.getSourceAttachedPartnerBusId());
		
		coreInsuranceContractBean.setAttachedPartnerId(corePartyBeanAttachedPartner != null ? corePartyBeanAttachedPartner.getId().toString() : null);
		
		
		EmployeeReader employeeReader = new EmployeeReader();
		
		EmployeeBean employeeBeanAdminResponsible = findEmployeeByUserName(employeeReader, coreInsuranceContractBean.getAdminResponsibleUserName());
		coreInsuranceContractBean.setAdminResponsibleId(employeeBeanAdminResponsible != null ? employeeBeanAdminResponsible.getEmployeeId() : null);
		coreInsuranceContractBean.setAdminResponsibleBusId(employeeBeanAdminResponsible != null ? employeeBeanAdminResponsible.getEmployeeBusId() : null);
		
		EmployeeBean employeeBeanClaimResponsible = findEmployeeByUserName(employeeReader, coreInsuranceContractBean.getClaimResponsibleUserName());
		coreInsuranceContractBean.setClaimResponsibleId(employeeBeanClaimResponsible != null ? employeeBeanClaimResponsible.getEmployeeId() : null);
		coreInsuranceContractBean.setClaimResponsibleBusId(employeeBeanClaimResponsible != null ? employeeBeanClaimResponsible.getEmployeeBusId() : null);
		
		EmployeeBean employeeBeanCaseMgmtResponsible = findEmployeeByUserName(employeeReader, coreInsuranceContractBean.getCaseMgmtResponsibleUserName());
		coreInsuranceContractBean.setCaseMgmtResponsibleId(employeeBeanCaseMgmtResponsible != null ? employeeBeanCaseMgmtResponsible.getEmployeeId() : null);
		coreInsuranceContractBean.setCaseMgmtResponsibleBusId(employeeBeanCaseMgmtResponsible != null ? employeeBeanCaseMgmtResponsible.getEmployeeBusId() : null);
		
		EmployeeBean employeeBeanSalesResponsible = findEmployeeByUserName(employeeReader, coreInsuranceContractBean.getSalesResponsibleUserName());
		coreInsuranceContractBean.setSalesResponsibleId(employeeBeanSalesResponsible != null ? employeeBeanSalesResponsible.getEmployeeId() : null);
		coreInsuranceContractBean.setSalesResponsibleBusId(employeeBeanSalesResponsible != null ? employeeBeanSalesResponsible.getEmployeeBusId() : null);
		
		return coreInsuranceContractBean;
	}
	

	/**
	 * Find Party by CRMid and SourceSystem and Source Party BusId
	 * 
	 * @param corePartyIdentifierReader
	 * @param corePartyReader
	 * @param crmId
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @return
	 */
	private static CorePartyBean findPartyByCrmIdOrSourceSystemAndPartyBusId(
			CorePartyIdentifierReader corePartyIdentifierReader,
			CorePartyReader corePartyReader,
			String crmId,
			String sourceSystem,
			String sourcePartyBusId) {

		
		List<CorePartyIdentifierBean> corePartyIdentifierBeans = null;
		
		if(crmId != null && !crmId.isEmpty()) {
			
			corePartyIdentifierBeans = corePartyIdentifierReader.getBeansBySourceSystem("CRM", crmId);
		}
		
		if(corePartyIdentifierBeans == null || corePartyIdentifierBeans.isEmpty()) {
			if(sourceSystem != null && !sourceSystem.isEmpty() && sourcePartyBusId != null && !sourcePartyBusId.isEmpty()) {
				
				corePartyIdentifierBeans = corePartyIdentifierReader.getBeansBySourceSystem(sourceSystem, sourcePartyBusId);
			}
		}
		
		if(corePartyIdentifierBeans == null || corePartyIdentifierBeans.isEmpty()) {
			return null;
		}
		
		CorePartyIdentifierBean corePartyIdentifierBean = corePartyIdentifierBeans.get(0);
		
		CorePartyBean corePartyBeans = corePartyReader.findBeanByPartyId(Integer.valueOf(corePartyIdentifierBean.getPartyId()));
		
		return corePartyBeans;
	}
	
	
	/**
	 * Find the employee by username
	 * 
	 * @param employeeReader
	 * @param userName
	 * @return
	 */
	private static EmployeeBean findEmployeeByUserName(EmployeeReader employeeReader, String userName) {
		
		if(userName == null || userName.isEmpty()) {
			return null;
		}
		
		List<EmployeeBean> employees = employeeReader.findBeansByUserName(userName);
		
		return employees.isEmpty() ? null : employees.get(0);
	}
}