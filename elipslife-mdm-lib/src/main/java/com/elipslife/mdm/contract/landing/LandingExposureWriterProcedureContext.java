package com.elipslife.mdm.contract.landing;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class LandingExposureWriterProcedureContext extends LandingExposureWriter {

	private ProcedureContext procedureContext;
	
	public LandingExposureWriterProcedureContext(ProcedureContext procedureContext) {
		this.procedureContext = procedureContext;
	}
	
	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}