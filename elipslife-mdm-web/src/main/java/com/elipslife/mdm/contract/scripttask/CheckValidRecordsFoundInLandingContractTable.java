package com.elipslife.mdm.contract.scripttask;

import java.util.List;

import com.elipslife.mdm.contract.landing.LandingInsuranceContractBean;
import com.elipslife.mdm.contract.landing.LandingInsuranceContractReader;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ConditionBean;
import com.orchestranetworks.workflow.ConditionBeanContext;

public class CheckValidRecordsFoundInLandingContractTable extends ConditionBean {

    @Override
    public boolean evaluateCondition(ConditionBeanContext context) throws OperationException {
        
        LandingInsuranceContractReader landingInsuranceContractReader = new LandingInsuranceContractReader();
        List<LandingInsuranceContractBean> landingInsuranceContractBean = landingInsuranceContractReader.readAllBeans();
        boolean isValid = false;
        for(LandingInsuranceContractBean insuranceContractBean : landingInsuranceContractBean) {
            boolean hasValidationErrors = landingInsuranceContractReader.hasValidationErrors(new Object[] { insuranceContractBean.getId() });
            if(!hasValidationErrors) {
                isValid = true;
                break;
            }
        }
        return isValid;
    }

}
