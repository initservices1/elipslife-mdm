package com.elipslife.ebx.data.access;

import java.util.List;
import java.util.stream.Collectors;

import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public abstract class BaseBankAccountReader<T> extends BaseBeanReader<T> {

	protected abstract Path getPathBankAccountId();
	protected abstract Path getPathSourceBankAccountBusId();
	
	protected BaseBankAccountReader(AdaptationTable table) {
		super(table);
	}

	
	/**
	 * 
	 * @param bankAccountId
	 * @return
	 */
	public T findBeanByBankAccountId(int bankAccountId) {
		
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathBankAccountId(), String.valueOf(bankAccountId));
		
		List<T> coreBankAccountBeans = readRecordsFor(predicate1.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
		
		if(coreBankAccountBeans.isEmpty()) {
			return null;
		}
		
		return coreBankAccountBeans.get(0);
	}
	
	
	/**
	 * Find beans by source system
	 * @param sourceAddressBusId
	 * @return
	 */
	public List<T> findBeansBySourceSystem(String sourceAddressBusId) {
		
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathSourceBankAccountBusId(), sourceAddressBusId);
		
		return readRecordsFor(predicate1.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
	
	/**
	 * Find beans by party
	 * @param sourceSystem
	 * @param sourcePartyBusId
	 * @return
	 */
	public List<T> findBeansByParty(String sourceSystem, String sourcePartyBusId) {
		
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathControlSourceSystem(), sourceSystem);
		XPathPredicate predicate2 = new XPathPredicate(XPathPredicate.Operation.Equals, getPathControlSourcePartyBusId(), sourcePartyBusId);
		
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate1);
		group.addPredicate(predicate2);
		
		return readRecordsFor(group.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
}