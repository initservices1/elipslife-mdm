package com.elipslife.ebx.domain.bean.convert;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import com.elipslife.mdm.party.constants.PartyConstants;
import com.onwbp.boot.VM;

public class ReadPartyBulkProcesserProperty {

    private static final String PROPERTY_FILE_LOCATION = "elipslife.mdm.properties";
    private static final String ELIPSLIFE_MDM_PROPERTIES_FILE = "elipslife_mdm.properties";
    private static ReadPartyBulkProcesserProperty instance = null;
    protected Properties properties;

    private ReadPartyBulkProcesserProperty() {

        String propertiesPath = System.getProperty(PROPERTY_FILE_LOCATION);
        try {

            Properties properties = new Properties();
            try (InputStream is = new FileInputStream(propertiesPath)) {
                properties.load(is);
            }
            this.properties = properties;

        } catch (Exception e) {
            VM.log.kernelError("Exception has occurred while loading the property file. So let's go head with Incremental load ");
            VM.log.kernelError(e.getMessage());
        }
    }

    public static ReadPartyBulkProcesserProperty getInstance() {

        if (instance == null) {
            instance = new ReadPartyBulkProcesserProperty();
        }
        
        return instance;
    }
    
    public void setPartyBulkProcessProperty() {
        this.properties.setProperty(PartyConstants.Entities.Party.RestBulkProcessState.BULKPROCESS_PROPERTY, PartyConstants.Entities.Party.RestBulkProcessState.REST_BULKPROCESS_STATE_INCR);
    }

    public String getPartyBulkProcessProperty() {
        return this.properties.getProperty(PartyConstants.Entities.Party.RestBulkProcessState.BULKPROCESS_PROPERTY);
    }
}
