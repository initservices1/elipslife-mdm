package com.elipslife.ebx.api.rest.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.json.bind.annotation.JsonbProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UWApplication {
	
	@JsonbProperty("DataOwner")
	@JsonProperty("DataOwner")
	public String dataOwner;
	
	@JsonbProperty("UWApplicationBusId")
	@JsonProperty("UWApplicationBusId")
	public String uwApplicationBusId;
	
	@JsonbProperty("OperatingCountry")
	@JsonProperty("OperatingCountry")
	public String operatingCountry;
	
	@JsonbProperty("PolicyholderBusId")
	@JsonProperty("PolicyholderBusId")
	public String policyholderBusId;
	
	@JsonbProperty("PolicyholderName")
	@JsonProperty("PolicyholderName")
	public String policyholderName;
	
	@JsonbProperty("MainAffiliateBusId")
	@JsonProperty("MainAffiliateBusId")
	public String mainAffiliateBusId;
	
	@JsonbProperty("MainAffiliateFirstName")
	@JsonProperty("MainAffiliateFirstName")
	public String mainAffiliateFirstName;
	
	@JsonbProperty("MainAffiliateLastName")
	@JsonProperty("MainAffiliateLastName")
	public String mainAffiliateLastName;
	
	@JsonbProperty("MainAffiliateDateOfBirth")
	@JsonProperty("MainAffiliateDateOfBirth")
	public LocalDate mainAffiliateDateOfBirth;
	
	@JsonbProperty("MainAffiliateSSN")
	@JsonProperty("MainAffiliateSSN")
	public String mainAffiliateSSN;
	
	@JsonbProperty("MainAffiliateTown")
	@JsonProperty("MainAffiliateTown")
	public String mainAffiliateTown;
	
	@JsonbProperty("MainAffiliatePostCode")
	@JsonProperty("MainAffiliatePostCode")
	public String mainAffiliatePostCode;
	
	@JsonbProperty("MainAffiliateCountry")
	@JsonProperty("MainAffiliateCountry")
	public String mainAffiliateCountry;																						
	
	@JsonbProperty("SubmissionDate")
	@JsonProperty("SubmissionDate")
	public LocalDate submissionDate;																								
	
	@JsonbProperty("IsActive")
	@JsonProperty("IsActive")
	public Boolean isActive;
	
	@JsonbProperty("DocSignId")
	@JsonProperty("DocSignId")
	public String docSignId; 
	
	@JsonbProperty("SFSignedDate")
	@JsonProperty("SFSignedDate")
	public LocalDate sfSignedDate;
	
	@JsonbProperty("CreatedBy")
	@JsonProperty("CreatedBy")
	public String createdBy; 	
	
	@JsonbProperty("CreationTimestamp")
	@JsonProperty("CreationTimestamp")
	public LocalDateTime creationTimestamp;
	
	@JsonbProperty("LastUpdatedBy")
	@JsonProperty("LastUpdatedBy")
	public String lastUpdatedBy;
	
	@JsonbProperty("LastUpdateTimestamp")
	@JsonProperty("LastUpdateTimestamp")
	public LocalDateTime lastUpdateTimestamp;
	
	@JsonbProperty("DeletionTimestamp")
	@JsonProperty("DeletionTimestamp")
	public LocalDateTime deletionTimestamp;
	
	@JsonbProperty("Action")
	@JsonProperty("Action")
	public String action;
	
	@JsonbProperty("UWCases")
	@JsonProperty("UWCases")
	public Collection<UWCases> uwCases;

}
