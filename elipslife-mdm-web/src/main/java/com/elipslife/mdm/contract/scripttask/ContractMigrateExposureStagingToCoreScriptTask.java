package com.elipslife.mdm.contract.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.butos.ebx.procedure.CreateRecords;
import com.butos.ebx.procedure.RecordValuesBean;
import com.butos.ebx.procedure.UpdateRecords;
import com.elipslife.ebx.domain.bean.convert.ContractConverter;
import com.elipslife.mdm.contract.ExposureBuilder;
import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.contract.core.CoreExposureBean;
import com.elipslife.mdm.contract.core.CoreExposureReader;
import com.elipslife.mdm.contract.staging.StagingExposureBean;
import com.elipslife.mdm.contract.staging.StagingExposureReader;
import com.elipslife.mdm.contract.staging.StagingExposureWriter;
import com.elipslife.mdm.contract.staging.StagingExposureWriterSession;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class ContractMigrateExposureStagingToCoreScriptTask extends ScriptTask {

	private static int COMMITSIZE = 1;
	private static int recordCount = 0; // record count
	private static List<StagingExposureBean> stagingExposureList = new ArrayList<StagingExposureBean>();
	private static List<CoreExposureBean> coreExposuresToCreate = new ArrayList<CoreExposureBean>();
	private static List<CoreExposureBean> coreExposuresToUpdate = new ArrayList<CoreExposureBean>();
	

	@Override
	public void executeScript(ScriptTaskContext aContext) throws OperationException {
		
		// Migrate staging exposures to core
		migrateStagingExposuresToCore(aContext);
	}
	
	/**
	 * Migrate staging exposures to core
	 * 
	 * @param aContext
	 * @throws OperationException 
	 */
	private void migrateStagingExposuresToCore(ScriptTaskContext aContext) throws OperationException {
		
		Map<String, String> dataContextVariables = ContractConverter.getContextVariables(aContext);
		String commitSize = dataContextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);
		if (commitSize != null) {
			COMMITSIZE = Integer.parseInt(commitSize);
		}
		String errorMessage = dataContextVariables.get("errorMessage");
		StringBuilder stringBuilderErrorMessage = new StringBuilder(errorMessage == null ? "" : errorMessage);
		String dataSpace = dataContextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(dataSpace);
		
		// get staging exposures whose exposure master is empty
		StagingExposureWriter stagingExposureWriter = new StagingExposureWriterSession(aContext.getSession());
		// StagingExposureReader stagingExposureReader = (StagingExposureReader) stagingExposureWriter.getTableReader();
		StagingExposureReader stagingExposureReader = new StagingExposureReader(dataSpace);
		List<StagingExposureBean> stagingExposureBeans = stagingExposureReader.getAllBeansWithExposureMasterIsEmpty();
		
		CoreExposureReader coreExposureReader = new CoreExposureReader(dataSpace);
		stagingExposureBeans.forEach(seb -> {
		// for (StagingExposureBean seb : stagingExposureBeans) {
			try {
				// Check if staging exposure has validations errors
				boolean hasValidationErrors = stagingExposureReader.hasValidationErrors(new Object[] { seb.getId() });
				if (hasValidationErrors) {
					return;
				}
				recordCount++;
				// Get Core exposure beans based on sourceSystem and expsoureBusId
				List<CoreExposureBean> coreExposureBeans = coreExposureReader.findBeansBySourceSystemAndExposureBusId(
						seb.getSourceSystem(), seb.getExposureBusId());
				
				// If it returns more than 1 core exposure
				if(coreExposureBeans.size() > 1) {
					throw new RuntimeException(String.format("More than one exposure found with source system %s and exposure business ID %s",
									seb.getSourceSystem(), seb.getExposureBusId()));
				}
				
				CoreExposureBean coreExposureBean = coreExposureBeans.isEmpty() ? null : coreExposureBeans.get(0);
				stagingExposureList.add(seb);
				if(coreExposureBean != null) {  // update
					boolean updateCoreExposureBean = DateNullableComparator.getInstance().compare(seb.getLastUpdateTimestamp(), 
							coreExposureBean.getLastUpdateTimestamp()) > 0;
					if(!updateCoreExposureBean) {
					// seb.setExposureMaster(String.valueOf(coreExposureBean.getId()));
					// stagingExposureWriter.updateBean(seb);
						return;
					}
					
					ContractConverter.fillCoreExposureBeanWithStagingExposureBean(seb, coreExposureBean);
					// link insurance contract references
					ExposureBuilder.linkInsuranceContractReference(dataSpace, coreExposureBean, seb.getSourceContractBusId());
					coreExposuresToUpdate.add(coreExposureBean);					
				} else { // create
					coreExposureBean = ContractConverter.createCoreExposureBeanFromStagingExposureBean(seb);
					// link insurance contract references
					ExposureBuilder.linkInsuranceContractReference(dataSpace, coreExposureBean, seb.getSourceContractBusId());
					coreExposuresToCreate.add(coreExposureBean);
				}
				
				if(recordCount > COMMITSIZE) 
				{	
					// create exposures in core
					createCoreExposureRecords(dataSpace, coreExposuresToCreate, dataspaceName, aContext.getSession());
					
					// update exposures in core
					updateCoreExposureRecords(dataSpace, coreExposuresToUpdate, dataspaceName, aContext.getSession());
					recordCount = 0;
				}
			} catch (Exception ex) {
				stringBuilderErrorMessage.append(String.format("Migration error for staging exposure with ID %s:\r\n%s",
				        seb.getId(), ex.toString()));
			}
		});
		
		if(!coreExposuresToCreate.isEmpty() || !coreExposuresToUpdate.isEmpty() || !stagingExposureBeans.isEmpty()) {
			// create exposures in core
			createCoreExposureRecords(dataSpace, coreExposuresToCreate, dataspaceName, aContext.getSession());
			
			// update exposures in core
			updateCoreExposureRecords(dataSpace, coreExposuresToUpdate, dataspaceName, aContext.getSession());
			
			updateStagingExposureMaster(dataSpace, dataspaceName, aContext.getSession());
			recordCount = 0;
		}
		
		aContext.setVariableString("errorMessage", stringBuilderErrorMessage.toString());
	}
	
	/**
	 * Create core Exposure record value beans
	 * 
	 * @param createCoreExposures
	 * @throws OperationException 
	 */
	private static void createCoreExposureRecords(String dataspaceName, List<CoreExposureBean> createCoreExposures, AdaptationHome dataspace, Session session) throws OperationException {
		
		List<RecordValuesBean> coreExposureValuesList = new ArrayList<RecordValuesBean>();
		if(!createCoreExposures.isEmpty()) {  // check if create core exposures are not empty
			for (CoreExposureBean coreExposureBean : createCoreExposures) {
				HashMap<Path, Object> values = ExposureBuilder.fillCoreExposureBean(coreExposureBean);
				RecordValuesBean rvb = new RecordValuesBean(values);
				coreExposureValuesList.add(rvb);
			}
			createCoreExposures(dataspaceName, coreExposureValuesList, dataspace, session);
		}
	}
	
	/**
	 * Create core Exposure record value beans
	 * 
	 * @param coreExposures
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void createCoreExposures(String dataspaceName, List<RecordValuesBean> coreExposures, AdaptationHome dataspace, Session session) throws OperationException {
		
		if(!coreExposures.isEmpty()) {
			Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
			AdaptationTable aTable = dataset.getTable(ContractPaths._Exposure.getPathInSchema());
			
			final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
			CreateRecords createRecordsProcedure = new CreateRecords(aTable, coreExposures);
			ProcedureResult result = service.execute(createRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
				throw exception;
			}
			
			coreExposuresToCreate.clear();
			// Update staging exposure master
			updateStagingExposureMaster(dataspaceName, dataspace, session);
		}
	}
	
	/**
	 * Update core exposure records
	 * 
	 * @param updateCoreExposures
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void updateCoreExposureRecords(String dataspaceName, List<CoreExposureBean> updateCoreExposures, AdaptationHome dataspace, Session session) throws OperationException {
		if(!updateCoreExposures.isEmpty()) {  // check if create core exposures are not empty
			Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
			AdaptationTable aTable = dataset.getTable(ContractPaths._Exposure.getPathInSchema());
			List<RecordValuesBean> updateExposureValuesList = new ArrayList<RecordValuesBean>();
			HashMap<Path, Object> values = null;
			RecordValuesBean rvb = null;
			for (CoreExposureBean coreExposureBean : updateCoreExposures) {
				values = ExposureBuilder.fillCoreExposureBean(coreExposureBean);
				rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(coreExposureBean.getId().toString())), values);
				updateExposureValuesList.add(rvb);
			}
			updateCoreExposures(dataspaceName, updateExposureValuesList, dataspace, session);
		}
	}
	
	/**
	 * Update core exposures
	 * 
	 * @param updateExposureValuesList
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void updateCoreExposures(String dataspaceName, List<RecordValuesBean> updateExposureValuesList, AdaptationHome dataspace, Session session) throws OperationException {

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
		UpdateRecords updateRecordsProcedure = new UpdateRecords(updateExposureValuesList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
		}
		coreExposuresToUpdate.clear();
		// Update staging exposure master
		updateStagingExposureMaster(dataspaceName, dataspace, session);
	}
	
	/**
	 * Update staging Exposure Master
	 * 
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void updateStagingExposureMaster(String dataspaceName, AdaptationHome dataspace, Session session) throws OperationException {
		
		Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
		AdaptationTable aTable = dataset.getTable(ContractPaths._STG_Exposure.getPathInSchema());
		CoreExposureReader coreExposureReader = new CoreExposureReader(dataspaceName);
		
		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		List<StagingExposureBean> alreadyProcessedRecords = new ArrayList<StagingExposureBean>();
		HashMap<Path, Object> values = null;
		RecordValuesBean rvb = null;
		for (StagingExposureBean stagingExposureBean : stagingExposureList) {
			alreadyProcessedRecords.add(stagingExposureBean);
			List<CoreExposureBean> exposureRecord = coreExposureReader.findBeansBySourceSystemAndExposureBusId(stagingExposureBean.getSourceSystem(), 
					stagingExposureBean.getExposureBusId()); 
			Integer exposureId = exposureRecord.isEmpty() ? null : exposureRecord.get(0).getId();
			values = new HashMap<Path, Object>();
			values.put(ContractPaths._STG_Exposure._Exposure_ExposureMaster, String.valueOf(exposureId));
			rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(stagingExposureBean.getId().toString())), values);
			recordValuesBeanList.add(rvb);
		}
		stagingExposureList.removeAll(alreadyProcessedRecords);
		
		// For Updates
		UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}

}