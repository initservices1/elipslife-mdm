package com.elipslife.ebx.data.access;

import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class CorePartyIdentifierReader extends AbstractTableReader<CorePartyIdentifierBean> {

    public CorePartyIdentifierReader() {
        super(RepositoryHelper.getTable(
            RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
            PartyPaths._PartyIdentifier.getPathInSchema()));
    }

    public CorePartyIdentifierReader(String dataSpace) {
        this(RepositoryHelper.getTable(
            RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.PARTY_MASTER),
            PartyPaths._PartyIdentifier.getPathInSchema()));
    }

    public CorePartyIdentifierReader(AdaptationTable table) {
        super(table);
    }

    @Override
    public CorePartyIdentifierBean createBeanFromRecord(Adaptation record) {

        CorePartyIdentifierBean corePartyIdentifierBean = new CorePartyIdentifierBean();

        corePartyIdentifierBean.setId(record.get_int(PartyPaths._PartyIdentifier._PartyIdentifierId));

        corePartyIdentifierBean.setPartyId(record.getString(PartyPaths._PartyIdentifier._PartyId));
        corePartyIdentifierBean.setSourceSystem(record.getString(PartyPaths._PartyIdentifier._SourceSystem));
        corePartyIdentifierBean.setSourcePartyBusId(record.getString(PartyPaths._PartyIdentifier._SourcePartyBusId));

        return corePartyIdentifierBean;
    }

    @Override
    protected Path[] getPkPaths() {

        return new Path[] {PartyPaths._PartyIdentifier._PartyIdentifierId};
    }

    @Override
    protected Object[] retrievePks(CorePartyIdentifierBean bean) {

        return new Object[] {bean.getId()};
    }

    /**
     * Gets beans by source system
     * 
     * @param sourceSystem
     * @param sourcePartyBusId
     * @return
     */
    public List<CorePartyIdentifierBean> getBeansBySourceSystem(String sourceSystem, String sourcePartyBusId) {

        XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._PartyIdentifier._SourceSystem, sourceSystem);
        XPathPredicate predicate2 =
            new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._PartyIdentifier._SourcePartyBusId, sourcePartyBusId);

        XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
        group.addPredicate(predicate1);
        group.addPredicate(predicate2);

        return readRecordsFor(group.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }

    /**
     * 
     * @param partyId
     * @return
     */
    public List<CorePartyIdentifierBean> findBeansByPartyId(String partyId) {

        XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._PartyIdentifier._PartyId, partyId);

        return readRecordsFor(predicate1.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }
}
