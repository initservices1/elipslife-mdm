package com.elipslife.ebx.data.access;

import java.util.Date;
import java.util.List;

import com.onwbp.adaptation.AdaptationTable;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;

public abstract class BasePartyWriter<T> extends AbstractTableWriter<T> {

	private BasePartyReader<T> basePartyReader;
	
	public BasePartyWriter(BasePartyReader<T> basePartyReader, AdaptationTable table) {
		super(table);
		
		this.basePartyReader = basePartyReader;
	}
	
	
	/**
	 * 
	 * @param sourceSystem
	 * @param sourceSystemPartyBusId
	 * @param validFrom
	 * @param validTo
	 */
	public void delete(String sourceSystem, String sourceSystemPartyBusId, Date validFrom, Date validTo) {
		
		List<T> basePartyBeans = this.basePartyReader.findBeansBySourceSystem(
				sourceSystem,
				sourceSystemPartyBusId,
				validFrom,
				validTo);
		
		if(basePartyBeans.isEmpty()) {
			throw new IllegalStateException(
					String.format(
							"Landing party not found with source system '%s', source party bus ID '%s', valid from '%s' and valid to '%s'",
							sourceSystem,
							sourceSystemPartyBusId,
							validFrom,
							validTo));
		}
		
		if(basePartyBeans.size() > 1) {
			throw new IllegalStateException(
					String.format(
							"More than one landing party not found with source system '%s', source party bus ID '%s', valid from '%s' and valid to '%s'",
							sourceSystem,
							sourceSystemPartyBusId,
							validFrom,
							validTo));
		}
		
		T landingPartyBean = basePartyBeans.get(0);
		
		delete(landingPartyBean);
	}


	@Override
	public AbstractTableReader<T> getTableReader() {

		return this.basePartyReader;
	}
}