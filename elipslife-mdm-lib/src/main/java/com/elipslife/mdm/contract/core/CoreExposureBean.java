package com.elipslife.mdm.contract.core;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class CoreExposureBean extends Entity<Integer> {
	
	private String insuranceContractId;
	
	private String exposureBusId;
	private String name;
	private String productId;
	private String productBusId;
	private String productGroup;
	private String productName;
	
	private String sourceSystem;
	private String createdBy;
	private Date creationTimestamp;
	private String lastUpdatedBy;
	private Date lastUpdateTimestamp;
	private String lastSyncAction;
	private Date lastSyncTimestamp;
	
	
	public String getInsuranceContractId() {
		return insuranceContractId;
	}
	public void setInsuranceContractId(String insuranceContractId) {
		this.insuranceContractId = insuranceContractId;
	}
	public String getExposureBusId() {
		return exposureBusId;
	}
	public void setExposureBusId(String exposureBusId) {
		this.exposureBusId = exposureBusId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductBusId() {
		return productBusId;
	}
	public void setProductBusId(String productBusId) {
		this.productBusId = productBusId;
	}
	public String getProductGroup() {
		return productGroup;
	}
	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getLastSyncAction() {
		return lastSyncAction;
	}
	public void setLastSyncAction(String lastSyncAction) {
		this.lastSyncAction = lastSyncAction;
	}
	public Date getLastSyncTimestamp() {
		return lastSyncTimestamp;
	}
	public void setLastSyncTimestamp(Date lastSyncTimestamp) {
		this.lastSyncTimestamp = lastSyncTimestamp;
	}
}