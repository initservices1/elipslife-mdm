package com.elipslife.ebx.data.access;

import java.util.HashMap;

import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class CorePartyIdentifierWriter extends AbstractTableWriter<CorePartyIdentifierBean> {

	private CorePartyIdentifierReader corePartyIdentifierReader;
	
	public CorePartyIdentifierWriter() {
		super(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
				PartyPaths._PartyIdentifier.getPathInSchema()));
		
		this.corePartyIdentifierReader = new CorePartyIdentifierReader(super.table);
	}
	
	public CorePartyIdentifierWriter(String dataSpace) {
        super(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.PARTY_MASTER),
                PartyPaths._PartyIdentifier.getPathInSchema()));
        
        this.corePartyIdentifierReader = new CorePartyIdentifierReader(super.table);
    }

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, CorePartyIdentifierBean bean) {

		values.put(PartyPaths._PartyIdentifier._SourceSystem, bean.getSourceSystem());
		values.put(PartyPaths._PartyIdentifier._SourcePartyBusId, bean.getSourcePartyBusId());
		values.put(PartyPaths._PartyIdentifier._PartyId, bean.getPartyId());
	}

	@Override
	public AbstractTableReader<CorePartyIdentifierBean> getTableReader() {
		
		return this.corePartyIdentifierReader;
	}
}