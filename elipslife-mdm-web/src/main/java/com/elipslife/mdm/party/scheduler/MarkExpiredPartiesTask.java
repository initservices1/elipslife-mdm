package com.elipslife.mdm.party.scheduler;

import com.elipslife.ebx.data.access.CoreAddressWriter;
import com.elipslife.ebx.data.access.CoreAddressWriterSession;
import com.elipslife.ebx.data.access.CoreBankAccountWriter;
import com.elipslife.ebx.data.access.CoreBankAccountWriterSession;
import com.elipslife.ebx.data.access.CorePartyBuilder;
import com.elipslife.ebx.data.access.CorePartyWriter;
import com.elipslife.ebx.data.access.CorePartyWriterSession;
import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;

public class MarkExpiredPartiesTask extends ScheduledTask {

	@Override
	public void execute(ScheduledExecutionContext scheduledExecutionContext) {
		
		CorePartyWriter corePartyWriter = new CorePartyWriterSession(scheduledExecutionContext.getSession(), true);
		CoreAddressWriter coreAddressWriter = new CoreAddressWriterSession(scheduledExecutionContext.getSession(), true);
		CoreBankAccountWriter coreBankAccountWriter = new CoreBankAccountWriterSession(scheduledExecutionContext.getSession(), true);
		
		CorePartyBuilder.markExpiredParties(corePartyWriter, coreAddressWriter, coreBankAccountWriter);
	}
}