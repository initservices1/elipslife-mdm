package com.elipslife.mdm.contract.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.contract.core.CoreInsuranceContractBean;
import com.elipslife.mdm.contract.core.CoreInsuranceContractReader;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.boot.VM;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.ui.UIButtonSpecNavigation;
import com.orchestranetworks.ui.UICSSClasses;
import com.orchestranetworks.ui.selection.TableViewEntitySelection;
import com.orchestranetworks.userservice.UserServiceDisplayConfigurator;
import com.orchestranetworks.userservice.UserServiceEventOutcome;
import com.orchestranetworks.userservice.UserServiceExtended;
import com.orchestranetworks.userservice.UserServiceGetContext;
import com.orchestranetworks.userservice.UserServiceGetRequest;
import com.orchestranetworks.userservice.UserServiceGetResponse;
import com.orchestranetworks.userservice.UserServiceInitializeContext;
import com.orchestranetworks.userservice.UserServiceObjectContextBuilder;
import com.orchestranetworks.userservice.UserServicePane;
import com.orchestranetworks.userservice.UserServicePaneContext;
import com.orchestranetworks.userservice.UserServicePaneWriter;
import com.orchestranetworks.userservice.UserServiceProcessEventOutcomeContext;
import com.orchestranetworks.userservice.UserServiceSetupDisplayContext;
import com.orchestranetworks.userservice.UserServiceSetupObjectContext;
import com.orchestranetworks.userservice.UserServiceValidateContext;

public class ExportDuplicateInsuranceContractUserService implements UserServiceExtended<TableViewEntitySelection> {

	AdaptationHome dataspace;
	Adaptation dataset;
	Session session;
	Adaptation result = null;
	Repository repository = null;
	List<Locale> listOfLocales = new ArrayList<Locale>();
	
	@Override
	public void setupDisplay(UserServiceSetupDisplayContext<TableViewEntitySelection> aContext,
			UserServiceDisplayConfigurator aConfigurator) {
		UIButtonSpecNavigation closeButton = aConfigurator.newCloseButton();
		closeButton.setDefaultButton(true);
		aConfigurator.setLeftButtons(closeButton);

		writePane(aConfigurator);
	}
	
	@Override
	public void setupObjectContext(UserServiceSetupObjectContext<TableViewEntitySelection> aContext,
			UserServiceObjectContextBuilder aBuilder) {
		//result = aContext.getEntitySelection().getRecord();
		repository = aContext.getRepository();
		dataspace = repository.lookupHome(HomeKey.forBranchName(ContractConstants.DataSpace.CONTRACT_REF));
		dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
		session = aContext.getSession();
		
	}

	@Override
	public UserServiceEventOutcome processEventOutcome(
			UserServiceProcessEventOutcomeContext<TableViewEntitySelection> aContext,
			UserServiceEventOutcome anEventOutcome) {
		return anEventOutcome;
	}

	@Override
	public void validate(UserServiceValidateContext<TableViewEntitySelection> aContext) {

	}

	@Override
	public UserServiceEventOutcome initialize(UserServiceInitializeContext<TableViewEntitySelection> aContext) {

		return null;
	}

	private void writePane(UserServiceDisplayConfigurator configurator) {

		configurator.setContent(new UserServicePane() {

			@Override
			public void writePane(UserServicePaneContext aPaneContext, UserServicePaneWriter aWriter) {
				aWriter.add("<div ");
				aWriter.addSafeAttribute("class", UICSSClasses.CONTAINER_WITH_TEXT_PADDING);
				aWriter.add(">");

				String downloadURL = aWriter.getURLForGetRequest(new UserServiceGetRequest() {

					@SuppressWarnings("static-access")
					@Override
					public void processRequest(UserServiceGetContext aGetContext, UserServiceGetResponse aGetResponse) {

						SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
						String currentDate = dateFormat.format(new Date());
						aGetResponse.setContentType("application/vnd.ms-excel");

						try {
							XSSFWorkbook workbook = new XSSFWorkbook();
							String prefix = "Insurance Contracts";

							aGetResponse.setHeader("Content-Disposition", "attachment; filename="
									+ prefix.toString().concat("_").concat(currentDate).concat(".xlsx") + "");

							XSSFSheet sheet = workbook.createSheet("Insurance Contracts");
							HeaderLinesForTableInfo(sheet, workbook);
							DataLinesTableForTableInfo(workbook, sheet);

							try {
								workbook.write(aGetResponse.getOutputStream());
							} catch (IOException e) {
								e.printStackTrace();
							}

							try {
								workbook.close();
							} catch (IOException e) {
								e.printStackTrace();
							}

							workbook.write(aGetResponse.getOutputStream());

						} catch (IOException e) {
							e.getMessage();
						}

					}

					@SuppressWarnings("unused")
					private Path parse(String dataModel) {
						// TODO Auto-generated method stub
						return null;
					}
				});
				aWriter.add("<a id=\"downloadLink\"");
				aWriter.addSafeAttribute("href", downloadURL);
				aWriter.add(">Click here If download does not start automatically </a>");
				aWriter.add("</div>");
				aWriter.addJS("document.getElementById('downloadLink').click();");
			}

		});
	}

	private void HeaderLinesForTableInfo(XSSFSheet sheet, XSSFWorkbook workbook) {
		int headerCellNumber = 0;
		Row headerRow = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Font font = workbook.createFont();
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		style.setFont(font);

		Cell headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Insurance Contract Id");
		headerCell.setCellStyle(style);

		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Source System");
		headerCell.setCellStyle(style);

		headerCell = headerRow.createCell(headerCellNumber++);
		headerCell.setCellValue("Source Contract Bus Id");
		headerCell.setCellStyle(style);

	}

	private void DataLinesTableForTableInfo(XSSFWorkbook workbook, XSSFSheet sheet) {
		
		List<CoreInsuranceContractBean> coreInsuranceContractBeans = getDuplicateInsuranceContractIds();
		int rowCount = 1;
		for (CoreInsuranceContractBean coreInsuranceContractBean : coreInsuranceContractBeans) {
			
			int columnCount = 0;
			int columnNumber = 0;
			Row row = sheet.createRow(rowCount++);

			sheet.autoSizeColumn(columnNumber++);
			Cell cell = row.createCell(columnCount++);
			cell.setCellValue(coreInsuranceContractBean.getId());

			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(coreInsuranceContractBean.getSourceSystem());
			
			sheet.autoSizeColumn(columnNumber++);
			cell = row.createCell(columnCount++);
			cell.setCellValue(coreInsuranceContractBean.getSourceContractBusId());

		}
	}

	private List<CoreInsuranceContractBean> getDuplicateInsuranceContractIds() {
		
		CoreInsuranceContractReader insuranceContractReader = new CoreInsuranceContractReader();
		List<CoreInsuranceContractBean> insuranceContractsList = insuranceContractReader.readAllBeans();
		List<CoreInsuranceContractBean> duplicateInsuranceContracts = new ArrayList<CoreInsuranceContractBean>();
		
		Map<String, List<CoreInsuranceContractBean>> insuranceContractsGroupedBySourceSystem = insuranceContractsList.stream().collect(Collectors.groupingBy(cpb -> cpb.getSourceSystem() + cpb.getSourceContractBusId()));
		for (Map.Entry<String, List<CoreInsuranceContractBean>> sourceSysAndBusIdKey : insuranceContractsGroupedBySourceSystem.entrySet()) {
			List<CoreInsuranceContractBean> coreInsuranceContractsList = sourceSysAndBusIdKey.getValue();
			if (coreInsuranceContractsList.size() == 1) {
				VM.log.kernelInfo("Single entry found for insurance contract source id "+coreInsuranceContractsList.get(0).getSourceContractBusId());
			} else {
				// Latest insurance contract based on last updated timestamp
				CoreInsuranceContractBean coreInsuranceContractBean = coreInsuranceContractsList.stream().sorted((a, b) -> DateNullableComparator.getInstance().compare(b.getLastUpdateTimestamp(), a.getLastUpdateTimestamp())).findFirst().get();
				
				// remove latest insurance contract
				coreInsuranceContractsList.remove(coreInsuranceContractBean);
				duplicateInsuranceContracts.addAll(coreInsuranceContractsList);
			}
		}
		return duplicateInsuranceContracts;
	}

}
