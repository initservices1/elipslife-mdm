package com.elipslife.ebx.domain.bean;

import java.util.Date;

import ch.butos.ebx.lib.legacy.wrapper.repository.Entity;

public class LandingPartyBean extends Entity<Integer> {
	
	private String dataOwner;
	private String operatingCountry;
	private String partyBusId;
	private String crmId;
	private String partyType;
	private String relationshipOwner;
	private String dataResponsible;
	private String language;
	private Boolean isActive;	
	private String firstName;
	private String middleName;
	private String lastName;
	private Date dateOfBirth;
	private String gender;
	private String title;
	private String salutation;
	private String letterSalutation;
	private String civilStatus;
	private String birthCity;
	private String birthCountry;
	private String idDocumentNumber;
	private String idDocumentType;
	private Date idIssueDate;
	private Date idExpiryDate;
	private String idIssuingAuthority;
	private String sourceParentBusId;
	private String orgName;
	private String orgName2;
	private String companyType;
	private String commercialRegNo;
	private String uidNumber;
	private String vatNumber;
	private String industry;
	private String subIndustry;
	private String preferredEmail;
	private String preferredCommChannel;
	private String sourceOrganisationBusId;
	private String sourceLineManagerBusId;
	private String sourcePartnerBusId;
	private String department;
	private String function;
	private Boolean isExecutor;
	private Boolean isLegalRepresentative;
	private Boolean isSeniorManager;
	private Boolean isUltimateBeneficialOwner;
	private String boRelationship;
	private String contactPersonState;
	private String roles;
	private Boolean isVipClient;
	private String blacklist;
	private String partnerState;
	private String companyPersonalNr;
	private String socialSecurityNr;
	private String socialSecurityNrType;
	private String brokerRegNo;
	private String brokerTiering;
	private String lineOfBusiness;
	private String financePartyBusId;
	private String paymentTerms;
	private String financeClients;
	private String creditorClients;
	private String correlationId;
	private String mdmStagingProcessResponse;
	
	// control attributes
	private Date validFrom;
	private Date validTo;
	private String sourceSystem;
	private String sourcePartyBusId;
	private Date creationTimestamp;
	private Date lastUpdateTimestamp;
	private Date lastSyncTimeStamp;
	private String action;
	private String actionBy;
	
	public String getDataOwner() {
		return dataOwner;
	}
	public void setDataOwner(String dataOwner) {
		this.dataOwner = dataOwner;
	}
	public String getOperatingCountry() {
		return operatingCountry;
	}
	public void setOperatingCountry(String operatingCountry) {
		this.operatingCountry = operatingCountry;
	}
	public String getPartyBusId() {
		return partyBusId;
	}
	public void setPartyBusId(String partyBusId) {
		this.partyBusId = partyBusId;
	}
	public String getCrmId() {
		return crmId;
	}
	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}
	public String getPartyType() {
		return partyType;
	}
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}
	public String getRelationshipOwner() {
		return relationshipOwner;
	}
	public void setRelationshipOwner(String relationshipOwner) {
		this.relationshipOwner = relationshipOwner;
	}
	public String getDataResponsible() {
		return dataResponsible;
	}
	public void setDataResponsible(String dataResponsible) {
		this.dataResponsible = dataResponsible;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSalutation() {
		return salutation;
	}
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	public String getLetterSalutation() {
		return letterSalutation;
	}
	public void setLetterSalutation(String letterSalutation) {
		this.letterSalutation = letterSalutation;
	}
	public String getCivilStatus() {
		return civilStatus;
	}
	public void setCivilStatus(String civilStatus) {
		this.civilStatus = civilStatus;
	}
	public String getBirthCity() {
		return birthCity;
	}
	public void setBirthCity(String birthCity) {
		this.birthCity = birthCity;
	}
	public String getBirthCountry() {
		return birthCountry;
	}
	public void setBirthCountry(String birthCountry) {
		this.birthCountry = birthCountry;
	}
	public String getIdDocumentNumber() {
		return idDocumentNumber;
	}
	public void setIdDocumentNumber(String idDocumentNumber) {
		this.idDocumentNumber = idDocumentNumber;
	}
	public String getIdDocumentType() {
		return idDocumentType;
	}
	public void setIdDocumentType(String idDocumentType) {
		this.idDocumentType = idDocumentType;
	}
	public Date getIdIssueDate() {
		return idIssueDate;
	}
	public void setIdIssueDate(Date idIssueDate) {
		this.idIssueDate = idIssueDate;
	}
	public Date getIdExpiryDate() {
		return idExpiryDate;
	}
	public void setIdExpiryDate(Date idExpiryDate) {
		this.idExpiryDate = idExpiryDate;
	}
	public String getIdIssuingAuthority() {
		return idIssuingAuthority;
	}
	public void setIdIssuingAuthority(String idIssuingAuthority) {
		this.idIssuingAuthority = idIssuingAuthority;
	}
	public String getSourceParentBusId() {
		return sourceParentBusId;
	}
	public void setSourceParentBusId(String sourceParentBusId) {
		this.sourceParentBusId = sourceParentBusId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getOrgName2() {
		return orgName2;
	}
	public void setOrgName2(String orgName2) {
		this.orgName2 = orgName2;
	}
	public String getCompanyType() {
		return companyType;
	}
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	public String getCommercialRegNo() {
		return commercialRegNo;
	}
	public void setCommercialRegNo(String commercialRegNo) {
		this.commercialRegNo = commercialRegNo;
	}
	public String getUidNumber() {
		return uidNumber;
	}
	public void setUidNumber(String uidNumber) {
		this.uidNumber = uidNumber;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getSubIndustry() {
		return subIndustry;
	}
	public void setSubIndustry(String subIndustry) {
		this.subIndustry = subIndustry;
	}
	public String getPreferredEmail() {
		return preferredEmail;
	}
	public void setPreferredEmail(String preferredEmail) {
		this.preferredEmail = preferredEmail;
	}
	public String getPreferredCommChannel() {
		return preferredCommChannel;
	}
	public void setPreferredCommChannel(String preferredCommChannel) {
		this.preferredCommChannel = preferredCommChannel;
	}
	public String getSourceOrganisationBusId() {
		return sourceOrganisationBusId;
	}
	public void setSourceOrganisationBusId(String sourceOrganisationBusId) {
		this.sourceOrganisationBusId = sourceOrganisationBusId;
	}
	public String getSourceLineManagerBusId() {
		return sourceLineManagerBusId;
	}
	public void setSourceLineManagerBusId(String sourceLineManagerBusId) {
		this.sourceLineManagerBusId = sourceLineManagerBusId;
	}
	public String getSourcePartnerBusId() {
		return sourcePartnerBusId;
	}
	public void setSourcePartnerBusId(String sourcePartnerBusId) {
		this.sourcePartnerBusId = sourcePartnerBusId;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public Boolean getIsExecutor() {
        return isExecutor;
    }
    public void setIsExecutor(Boolean isExecutor) {
        this.isExecutor = isExecutor;
    }
    public Boolean getIsLegalRepresentative() {
        return isLegalRepresentative;
    }
    public void setIsLegalRepresentative(Boolean isLegalRepresentative) {
        this.isLegalRepresentative = isLegalRepresentative;
    }
    public Boolean getIsSeniorManager() {
		return isSeniorManager;
	}
	public void setIsSeniorManager(Boolean isSeniorManager) {
		this.isSeniorManager = isSeniorManager;
	}
	public Boolean getIsUltimateBeneficialOwner() {
		return isUltimateBeneficialOwner;
	}
	public void setIsUltimateBeneficialOwner(Boolean isUltimateBeneficialOwner) {
		this.isUltimateBeneficialOwner = isUltimateBeneficialOwner;
	}
	public String getBoRelationship() {
		return boRelationship;
	}
	public void setBoRelationship(String boRelationship) {
		this.boRelationship = boRelationship;
	}
	public String getContactPersonState() {
		return contactPersonState;
	}
	public void setContactPersonState(String contactPersonState) {
		this.contactPersonState = contactPersonState;
	}
	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}
	public Boolean getIsVipClient() {
		return isVipClient;
	}
	public void setIsVipClient(Boolean isVipClient) {
		this.isVipClient = isVipClient;
	}
	public String getBlacklist() {
		return blacklist;
	}
	public void setBlacklist(String blacklist) {
		this.blacklist = blacklist;
	}
	public String getPartnerState() {
		return partnerState;
	}
	public void setPartnerState(String partnerState) {
		this.partnerState = partnerState;
	}
	public String getCompanyPersonalNr() {
		return companyPersonalNr;
	}
	public void setCompanyPersonalNr(String companyPersonalNr) {
		this.companyPersonalNr = companyPersonalNr;
	}
	public String getSocialSecurityNr() {
		return socialSecurityNr;
	}
	public void setSocialSecurityNr(String socialSecurityNr) {
		this.socialSecurityNr = socialSecurityNr;
	}
	public String getSocialSecurityNrType() {
		return socialSecurityNrType;
	}
	public void setSocialSecurityNrType(String socialSecurityNrType) {
		this.socialSecurityNrType = socialSecurityNrType;
	}
	public String getBrokerRegNo() {
		return brokerRegNo;
	}
	public void setBrokerRegNo(String brokerRegNo) {
		this.brokerRegNo = brokerRegNo;
	}
	public String getBrokerTiering() {
		return brokerTiering;
	}
	public void setBrokerTiering(String brokerTiering) {
		this.brokerTiering = brokerTiering;
	}
	public String getLineOfBusiness() {
		return lineOfBusiness;
	}
	public void setLineOfBusiness(String lineOfBusiness) {
		this.lineOfBusiness = lineOfBusiness;
	}
	public String getFinancePartyBusId() {
		return financePartyBusId;
	}
	public void setFinancePartyBusId(String financePartyBusId) {
		this.financePartyBusId = financePartyBusId;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getFinanceClients() {
		return financeClients;
	}
	public void setFinanceClients(String financeClients) {
		this.financeClients = financeClients;
	}
	public String getCreditorClients() {
		return creditorClients;
	}
	public void setCreditorClients(String creditorClients) {
		this.creditorClients = creditorClients;
	}
	public Date getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	public Date getValidTo() {
		return validTo;
	}
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getSourcePartyBusId() {
		return sourcePartyBusId;
	}
	public void setSourcePartyBusId(String sourcePartyBusId) {
		this.sourcePartyBusId = sourcePartyBusId;
	}
	public Date getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Date creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActionBy() {
		return actionBy;
	}
	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}
	
	public Date getUpdateTimestamp() {
		return getLastUpdateTimestamp() == null ? getCreationTimestamp() : getLastUpdateTimestamp();
	}
	public Date getLastSyncTimeStamp() {
		return lastSyncTimeStamp;
	}
	public void setLastSyncTimeStamp(Date lastSyncTimeStamp) {
		this.lastSyncTimeStamp = lastSyncTimeStamp;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	public String getMdmStagingProcessResponse() {
		return mdmStagingProcessResponse;
	}
	public void setMdmStagingProcessResponse(String mdmStagingProcessResponse) {
		this.mdmStagingProcessResponse = mdmStagingProcessResponse;
	}
	
	
}