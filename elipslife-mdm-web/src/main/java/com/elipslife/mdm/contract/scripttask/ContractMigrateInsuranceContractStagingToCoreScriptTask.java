package com.elipslife.mdm.contract.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.butos.ebx.procedure.CreateRecords;
import com.butos.ebx.procedure.RecordValuesBean;
import com.butos.ebx.procedure.UpdateRecords;
import com.elipslife.ebx.domain.bean.convert.ContractConverter;
import com.elipslife.mdm.contract.InsuranceContractBuilder;
import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.contract.core.CoreInsuranceContractBean;
import com.elipslife.mdm.contract.core.CoreInsuranceContractReader;
import com.elipslife.mdm.contract.staging.StagingInsuranceContractBean;
import com.elipslife.mdm.contract.staging.StagingInsuranceContractReader;
import com.elipslife.mdm.contract.staging.StagingInsuranceContractWriter;
import com.elipslife.mdm.contract.staging.StagingInsuranceContractWriterSession;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class ContractMigrateInsuranceContractStagingToCoreScriptTask extends ScriptTask {

	public static int COMMITSIZE = 1;
	private static int recordCount = 0;
	public static Map<String, List<StagingInsuranceContractBean>> insuranceContractMap = new HashMap<String, List<StagingInsuranceContractBean>>();
	public static List<CoreInsuranceContractBean> coreInsuranceContractBeansToCreate = new ArrayList<CoreInsuranceContractBean>();
	public static List<CoreInsuranceContractBean> coreInsuranceContractBeansToUpdate = new ArrayList<CoreInsuranceContractBean>();
	private static List<StagingInsuranceContractBean> stagingInsuranceContractList = new ArrayList<StagingInsuranceContractBean>();

	@Override
	public void executeScript(ScriptTaskContext aContext) throws OperationException {

		// Migrate staging insurance contracts to core
		migrateStagingInsuranceContractsToCore(aContext);
	}

	/**
	 * Migrate staging Insurance Contracts to Core
	 * 
	 * @param aContext
	 * @throws OperationException
	 */
	private static void migrateStagingInsuranceContractsToCore(ScriptTaskContext aContext) throws OperationException {

		int startPositionAt = 0;
		int endPositionAt = 0;
		Map<String, String> dataContextVariables = ContractConverter.getContextVariables(aContext);
		String commitSize = dataContextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);
		if (commitSize != null) {
			COMMITSIZE = Integer.parseInt(commitSize);
		}
		String dataSpace = dataContextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(dataSpace);
		StringBuilder stringBuilderErrorMessage = new StringBuilder();

		StagingInsuranceContractWriter stagingInsuranceContractWriter = new StagingInsuranceContractWriterSession(aContext.getSession());
		// StagingInsuranceContractReader stagingInsuranceContractReader = (StagingInsuranceContractReader) stagingInsuranceContractWriter
		//        .getTableReader();
		StagingInsuranceContractReader stagingInsuranceContractReader = new StagingInsuranceContractReader(dataSpace);
		List<StagingInsuranceContractBean> stagingInsuranceContractBeans = stagingInsuranceContractReader.getAllBeansWithInsuranceContractMasterIsEmpty();
		CoreInsuranceContractReader coreInsuranceContractReader = new CoreInsuranceContractReader(dataSpace);
		int totalNoOfRecords = stagingInsuranceContractBeans.size();
		float noOfIterations = totalNoOfRecords / COMMITSIZE;
		for (int i = 0; i < noOfIterations + 1; i++) {
			if (COMMITSIZE < totalNoOfRecords) {
				endPositionAt = endPositionAt + COMMITSIZE;
				totalNoOfRecords = totalNoOfRecords - COMMITSIZE;
			} else {
				endPositionAt = endPositionAt + totalNoOfRecords;
				totalNoOfRecords = 0;
			}
			List<StagingInsuranceContractBean> stagingInsuranceContractsBatch = stagingInsuranceContractBeans.subList(startPositionAt, endPositionAt);
			setStagingInsuraceContractsBatachRecords(stagingInsuranceContractsBatch);
			startPositionAt = endPositionAt;
			for (StagingInsuranceContractBean stagingInsuranceContractBean : stagingInsuranceContractsBatch) {
				try {
					boolean hasValidationErrors = stagingInsuranceContractReader.hasValidationErrors(new Object[] { stagingInsuranceContractBean.getId() });
					if (hasValidationErrors) {
						return;
					}
					recordCount++;
					// InsuranceContractBuilder.createOrUpdateCoreInsuranceContractByStagingInsuranceContract(aContext.getSession(),
					// stagingInsuranceContractBean, true);
					createOrUpdateCoreInsuranceContractByStagingInsuranceContract(stagingInsuranceContractWriter, coreInsuranceContractReader,
					        stagingInsuranceContractBean);
					if (recordCount > COMMITSIZE) {
						createInsuranceContractRecords(dataSpace, dataspaceName, aContext.getSession());
						updateInsuranceContractRecords(dataSpace, dataspaceName, aContext.getSession());
						recordCount = 0;
					}
				} catch (Exception ex) {
					stringBuilderErrorMessage
					        .append(String.format("Migration error for staging insurance contract with ID %s:\r\n%s",
					                stagingInsuranceContractBean.getId(), ex.toString()));
				}
			}

			if (!coreInsuranceContractBeansToCreate.isEmpty() || !coreInsuranceContractBeansToCreate.isEmpty() || !stagingInsuranceContractBeans.isEmpty()) {
				createInsuranceContractRecords(dataSpace, dataspaceName, aContext.getSession());
				updateInsuranceContractRecords(dataSpace, dataspaceName, aContext.getSession());
				updateStagingInsuranceContractMaster(dataSpace, dataspaceName, aContext.getSession());
				recordCount = 0;
			}
		}
		aContext.setVariableString("errorMessage", stringBuilderErrorMessage.toString());
	}

	/**
	 * Create or Update core insurance contract by staging records
	 * 
	 * @param coreInsuranceContractReader
	 * @param stagingInsuranceContractBean
	 */
	private static void createOrUpdateCoreInsuranceContractByStagingInsuranceContract(StagingInsuranceContractWriter stagingInsuranceContractWriter, CoreInsuranceContractReader coreInsuranceContractReader,
	        StagingInsuranceContractBean stagingInsuranceContractBean) {

		List<CoreInsuranceContractBean> coreInsuranceContractBeans = coreInsuranceContractReader.findBeansBySourceSystemAndSourceContractBusId(stagingInsuranceContractBean.getSourceSystem(),
		                stagingInsuranceContractBean.getSourceContractBusId());

		if (coreInsuranceContractBeans.size() > 1) {
			throw new RuntimeException(String.format("More than one insurance contract found with source system %s and source contract bus ID %s",
			        stagingInsuranceContractBean.getSourceSystem(), stagingInsuranceContractBean.getSourceContractBusId()));
		}

		CoreInsuranceContractBean coreInsuranceContractBean = coreInsuranceContractBeans.isEmpty() ? null
		        : coreInsuranceContractBeans.get(0);
		stagingInsuranceContractList.add(stagingInsuranceContractBean);
		if (coreInsuranceContractBean != null) {
			boolean updateCoreInsuranceContractBean = DateNullableComparator.getInstance().compare(
			        stagingInsuranceContractBean.getLastUpdateTimestamp(), coreInsuranceContractBean.getLastUpdateTimestamp()) > 0;
			if(!updateCoreInsuranceContractBean) {
				// stagingInsuranceContractBean.setInsuranceContractMaster(String.valueOf(coreInsuranceContractBean.getId()));
				// stagingInsuranceContractWriter.updateBean(stagingInsuranceContractBean);
				return;
			} 
			ContractConverter.fillCoreInsuranceContractBeanWithStagingInsuranceContractBeanBean(
			        stagingInsuranceContractBean, coreInsuranceContractBean);
			InsuranceContractBuilder.linkReferences(coreInsuranceContractBean);
			coreInsuranceContractBeansToUpdate.add(coreInsuranceContractBean);
		} else { // Create Insurance Contract record in Core
			List<StagingInsuranceContractBean> stagingInsuranceList = insuranceContractMap
			        .get(stagingInsuranceContractBean.getSourceSystem()
			                + stagingInsuranceContractBean.getSourceContractBusId());
			StagingInsuranceContractBean duplicateBean = stagingInsuranceList.size() > 1
			        ? stagingInsuranceList.stream().sorted((a, b) -> DateNullableComparator.getInstance()
			                .compare(b.getLastUpdateTimestamp(), a.getLastUpdateTimestamp())).findFirst().get()
			        : stagingInsuranceList.get(0);
			if (duplicateBean != null && Objects.equals(duplicateBean.getId(), stagingInsuranceContractBean.getId())) {
				coreInsuranceContractBean = ContractConverter
				        .createCoreInsuranceContractBeanFromStagingInsuranceContractBean(stagingInsuranceContractBean);
				InsuranceContractBuilder.linkReferences(coreInsuranceContractBean);
				coreInsuranceContractBeansToCreate.add(coreInsuranceContractBean);
			}
		}
	}

	/**
	 * Create core insurance contracts
	 * 
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void createInsuranceContractRecords(String dataspaceName, AdaptationHome dataspace, Session session)
	        throws OperationException {
		List<RecordValuesBean> coreInsuranceContractsValuesList = new ArrayList<RecordValuesBean>();
		if (!coreInsuranceContractBeansToCreate.isEmpty()) {
			for (CoreInsuranceContractBean coreInsuranceContractBean : coreInsuranceContractBeansToCreate) {
				HashMap<Path, Object> values = ContractConverter
				        .fillCoreInsuranceContractBeanWithStagingInsuranceContractBean(coreInsuranceContractBean, true);
				RecordValuesBean rvb = new RecordValuesBean(values);
				coreInsuranceContractsValuesList.add(rvb);
			}
			createCoreInsuranceContracts(dataspaceName, dataspace, session, coreInsuranceContractsValuesList);
			coreInsuranceContractBeansToCreate.clear();
		}
	}

	/**
	 * Update Insurance Contract Records
	 * 
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void updateInsuranceContractRecords(String dataspaceName, AdaptationHome dataspace, Session session) throws OperationException {
		
		if (!coreInsuranceContractBeansToUpdate.isEmpty()) {
			Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
			AdaptationTable aTable = dataset.getTable(ContractPaths._InsuranceContract.getPathInSchema());
			ArrayList<RecordValuesBean> coreInsuranceContractsValuesList = new ArrayList<RecordValuesBean>();
			HashMap<Path, Object> values = null;
			RecordValuesBean rvb = null;
			for (CoreInsuranceContractBean coreInsuranceContractBean : coreInsuranceContractBeansToUpdate) {
				values = ContractConverter.fillCoreInsuranceContractBeanWithStagingInsuranceContractBean(coreInsuranceContractBean, true);
				rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(coreInsuranceContractBean.getId().toString())), values);
				coreInsuranceContractsValuesList.add(rvb);
			}
			updateCoreInsuranceContracts(dataspaceName, dataspace, session, coreInsuranceContractsValuesList);
			coreInsuranceContractBeansToUpdate.clear();
		}
	}

	/**
	 * Create core insurance contracts
	 * 
	 * @param dataspace
	 * @param session
	 * @param stagingInsuranceContractsList
	 * @throws OperationException
	 */

	private static List<Adaptation> createCoreInsuranceContracts(String dataspaceName, AdaptationHome dataspace, Session session,
	        List<RecordValuesBean> coreInsuranceContractsList) throws OperationException {

		Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
		AdaptationTable aTable = dataset.getTable(ContractPaths._InsuranceContract.getPathInSchema());

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);

		CreateRecords createRecordsProcedure = new CreateRecords(aTable, coreInsuranceContractsList);
		ProcedureResult result = service.execute(createRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
		
		// Update staging insurance contract master
		updateStagingInsuranceContractMaster(dataspaceName, dataspace, session);
				
		return createRecordsProcedure.getCreateRecordList();
	}

	private static void updateCoreInsuranceContracts(String dataspaceName, AdaptationHome dataspace, Session session,
	        List<RecordValuesBean> coreInsuranceContractsList) throws OperationException {

		

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);

		UpdateRecords updateRecordsProcedure = new UpdateRecords(coreInsuranceContractsList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
		}
		
		// Update staging insurance contract master
		updateStagingInsuranceContractMaster(dataspaceName, dataspace, session);
	}

	/**
	 * Set the batch of staging insurance contracts by source system and
	 * sourceContractBus id
	 * 
	 * @param stagingInsuranceContractsBatch
	 */
	private static void setStagingInsuraceContractsBatachRecords(
	        List<StagingInsuranceContractBean> stagingInsuranceContractsBatch) {

		insuranceContractMap.clear();
		for (StagingInsuranceContractBean stagingInsuranceContractBean : stagingInsuranceContractsBatch) {
			List<StagingInsuranceContractBean> stagingContractsList = insuranceContractMap
			        .get(stagingInsuranceContractBean.getSourceSystem()
			                + stagingInsuranceContractBean.getSourceContractBusId());
			if (stagingContractsList == null) {
				stagingContractsList = new ArrayList<StagingInsuranceContractBean>();
			}
			stagingContractsList.add(stagingInsuranceContractBean);
			insuranceContractMap.put(stagingInsuranceContractBean.getSourceSystem()
			        + stagingInsuranceContractBean.getSourceContractBusId(), stagingContractsList);
		}
	}
	
	/**
	 * Update staging Contract Insurance Master
	 * 
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void updateStagingInsuranceContractMaster(String dataspaceName, AdaptationHome dataspace, Session session) throws OperationException {
		
		Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
		AdaptationTable aTable = dataset.getTable(ContractPaths._STG_InsuranceContract.getPathInSchema());
		CoreInsuranceContractReader coreInsuranceContractReader = new CoreInsuranceContractReader(dataspaceName);
		
		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		List<StagingInsuranceContractBean> alreadyProcessedRecords = new ArrayList<StagingInsuranceContractBean>();
		HashMap<Path, Object> values = null;
		RecordValuesBean rvb = null;
		for (StagingInsuranceContractBean stagingInsuranceContractBean : stagingInsuranceContractList) {
			alreadyProcessedRecords.add(stagingInsuranceContractBean);
			List<CoreInsuranceContractBean> insuranceContractRecord = coreInsuranceContractReader.findBeansBySourceSystemAndSourceContractBusId(stagingInsuranceContractBean.getSourceSystem(), 
					stagingInsuranceContractBean.getSourceContractBusId());
			Integer insuranceContractId = insuranceContractRecord.isEmpty() ? null : insuranceContractRecord.get(0).getId();
			values = new HashMap<Path, Object>();
			values.put(ContractPaths._STG_InsuranceContract._ContractIds_InsuranceContractMaster, String.valueOf(insuranceContractId));
			rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(stagingInsuranceContractBean.getId().toString())), values);
			recordValuesBeanList.add(rvb);
		}
		stagingInsuranceContractList.removeAll(alreadyProcessedRecords);
		
		// For Updates
		UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}
}