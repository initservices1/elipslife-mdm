package com.elipslife.mdm.contract.constraint;

import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import com.butos.ebx.smart.validation.SmartRecordAndTableLevelValidationCheck;
import com.elipslife.mdm.path.ContractPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class CoreExposureConstraint extends SmartRecordAndTableLevelValidationCheck{

	@Override
	protected void executeBusinessValidations(ValueContext recordContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {
		
		UserMessage userMsg = null;
		String insuranceContractId = (String) recordContext.getValue(ContractPaths._Exposure._InsuranceContract_InsuranceContractId);
		if(insuranceContractId == null || insuranceContractId == "") {
			userMsg = UserMessage.createWarning("Insurance Contract Id not found");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(ContractPaths._Exposure._InsuranceContract_InsuranceContractId),
				userMsg);
		}
		
	}
	
	@Override
	public void setup(ConstraintContextOnTable aContext) {
		
	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		return null;
	}

}
