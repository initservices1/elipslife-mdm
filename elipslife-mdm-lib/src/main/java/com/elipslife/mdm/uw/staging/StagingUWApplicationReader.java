package com.elipslife.mdm.uw.staging;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.UWPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class StagingUWApplicationReader extends AbstractTableReader<StagingUWApplicationBean> {

    public StagingUWApplicationReader() {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._STG_UWApplication.getPathInSchema()));
    }

    public StagingUWApplicationReader(String dataspace) {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(dataspace, Constants.DataSet.UW_MASTER),
				UWPaths._STG_UWApplication.getPathInSchema()));
    }

    public StagingUWApplicationReader(AdaptationTable table) {
		super(table);
	}

	@Override
    public StagingUWApplicationBean createBeanFromRecord(Adaptation record) {
        StagingUWApplicationBean bean = new StagingUWApplicationBean();
        bean.setUWApplicationId(record.get_int(UWPaths._STG_UWApplication._UWApplicationId));
        bean.setUWApplicationBusId(record.getString(UWPaths._STG_UWApplication._UWApplicationBusId));
        bean.setUWApplicationMaster(record.getString(UWPaths._STG_UWApplication._UWApplicationMaster));
        bean.setSourceSystem(record.getString(UWPaths._STG_UWApplication._Context_SourceSystem));
        bean.setDataOwner(record.getString(UWPaths._STG_UWApplication._Context_DataOwner));
        bean.setOperatingCountry(record.getString(UWPaths._STG_UWApplication._Context_OperatingCountry));
        bean.setPolicyholderBusId(record.getString(UWPaths._STG_UWApplication._Policyholder_PolicyholderBusId));
        bean.setPolicyholderName(record.getString(UWPaths._STG_UWApplication._Policyholder_PolicyholderName));
        bean.setMainAffiliateBusId(record.getString(UWPaths._STG_UWApplication._MainAffiliate_MainAffiliateBusId));
        bean.setMainAffiliateFirstName(record.getString(UWPaths._STG_UWApplication._MainAffiliate_MainAffiliateFirstName));
        bean.setMainAffiliateLastName(record.getString(UWPaths._STG_UWApplication._MainAffiliate_MainAffiliateLastName));
        bean.setMainAffiliateDateOfBirth(record.getDate(UWPaths._STG_UWApplication._MainAffiliate_MainAffiliateDateOfBirth));
        bean.setMainAffiliateSSN(record.getString(UWPaths._STG_UWApplication._MainAffiliate_MainAffiliateSSN));
        bean.setMainAffiliateTown(record.getString(UWPaths._STG_UWApplication._MainAffiliate_MainAffiliateTown));
        bean.setMainAffiliatePostCode(record.getString(UWPaths._STG_UWApplication._MainAffiliate_MainAffiliatePostCode));
        bean.setMainAffiliateCountry(record.getString(UWPaths._STG_UWApplication._MainAffiliate_MainAffiliateCountry));
        bean.setSubmissionDate(record.getDate(UWPaths._STG_UWApplication._Details_SubmissionDate));
        bean.setIsActive(record.get_boolean(UWPaths._STG_UWApplication._Details_IsActive));
        bean.setDocSignId(record.getString(UWPaths._STG_UWApplication._DocumentSigning_DocSignId));
        bean.setSFSignedDate(record.getDate(UWPaths._STG_UWApplication._DocumentSigning_SFSignedDate));
        bean.setCreatedBy(record.getString(UWPaths._STG_UWApplication._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(UWPaths._STG_UWApplication._Control_CreationTimestamp));
		bean.setLastUpdatedBy(record.getString(UWPaths._STG_UWApplication._Control_LastUpdatedBy));
		bean.setLastUpdateTimeStamp(record.getDate(UWPaths._STG_UWApplication._Control_LastUpdateTimestamp));
		bean.setDeletionTimeStamp(record.getDate(UWPaths._STG_UWApplication._Control_DeletionTimestamp));
		bean.setLastSyncTimeStamp(record.getDate(UWPaths._STG_UWApplication._Control_LastSyncTimestamp));
		bean.setAction(record.getString(UWPaths._STG_UWApplication._Control_Action));
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{UWPaths._STG_UWApplication._UWApplicationId};
    }

    @Override
    public Object[] retrievePks(StagingUWApplicationBean bean) {
        return new Object[]{bean.getUWApplicationId()};
    }

}
