package com.elipslife.mdm.uw.staging;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;

public class StagingUWCaseWriterSession extends StagingUWCaseWriter {
    private Session session;

    public StagingUWCaseWriterSession(Session session) {
        super();
        this.session = session;
    }

    public StagingUWCaseWriterSession(Session session, String dataspace) {
        super(dataspace);
        this.session = session;
    }

    @Override
    protected void executeProcedure(Procedure procedure) {
        ProcedureResult result = ProgrammaticService.createForSession(session, dataSpace).execute(procedure);
        if (result.hasFailed()) {
            throw new RuntimeException(result.getException());
        }
    }
}
