package com.elipslife.ebx.api.rest.rest;

import com.orchestranetworks.rest.RESTApplicationAbstract;
import io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/api")
public class RESTApplication extends RESTApplicationAbstract {

	public RESTApplication() {
        super((cfg)-> {
            cfg.addPackages(RESTApplication.class.getPackage());
            //Swagger integration
            cfg.register(OpenApiResource.class);
            cfg.register(AcceptHeaderOpenApiResource.class);
         });
	}
}