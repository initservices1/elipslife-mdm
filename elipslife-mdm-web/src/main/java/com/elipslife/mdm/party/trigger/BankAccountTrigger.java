package com.elipslife.mdm.party.trigger;

import java.util.Date;

import com.elipslife.mdm.path.PartyPaths;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class BankAccountTrigger extends TableTrigger{

	@Override
	public void setup(TriggerSetupContext arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		
		ValueContextForUpdate vcf = aContext.getOccurrenceContextForUpdate();
				
		this.updateValue(vcf);
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
		
		ValueContextForUpdate vcf = aContext.getOccurrenceContextForUpdate();
				
		this.updateValue(vcf);
	}

	private void updateValue(final ValueContextForUpdate vcf) throws OperationException {

		vcf.setValueEnablingPrivilegeForNode(new Date(), PartyPaths._BankAccount._Control_LastSyncTimestamp);
		
	}
}