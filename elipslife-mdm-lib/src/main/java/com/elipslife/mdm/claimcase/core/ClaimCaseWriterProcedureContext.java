package com.elipslife.mdm.claimcase.core;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class ClaimCaseWriterProcedureContext extends ClaimCaseWriter {
	private final ProcedureContext procedureContext;

	public ClaimCaseWriterProcedureContext(ProcedureContext procedureContext) {
		super();
		this.procedureContext = procedureContext;
	}

	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
