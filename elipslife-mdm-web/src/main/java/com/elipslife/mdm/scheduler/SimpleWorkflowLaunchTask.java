package com.elipslife.mdm.scheduler;

import java.util.List;

import org.apache.commons.codec.binary.StringUtils;

import com.orchestranetworks.scheduler.ScheduledExecutionContext;
import com.orchestranetworks.scheduler.ScheduledTask;
import com.orchestranetworks.scheduler.ScheduledTaskInterruption;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ProcessInstance;
import com.orchestranetworks.workflow.ProcessInstanceKey;
import com.orchestranetworks.workflow.ProcessLauncher;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.WorkflowEngine;

public class SimpleWorkflowLaunchTask extends ScheduledTask {
	
	public String workflowToLaunch;
	
	@Override
	public void execute(ScheduledExecutionContext context) throws OperationException, ScheduledTaskInterruption {
		
		WorkflowEngine workflowEngine = WorkflowEngine.getFromRepository(context.getRepository(), context.getSession());
		ProcessLauncher processLauncher = workflowEngine.getProcessLauncher(PublishedProcessKey.forName(getWorkflowToLaunch()));
		
		// To check if there are any running Contract_MigrateLandingToCore workflows
		if(StringUtils.equals(workflowToLaunch, "Contract_MigrateLandingToCore")) {
			boolean isProcessRunning = false;
			List<ProcessInstanceKey> processInstanceKeys = workflowEngine.getProcessInstanceKeys(PublishedProcessKey.forName(getWorkflowToLaunch()));
			if(processInstanceKeys.isEmpty()) {
				processLauncher.launchProcess();
			} else {
				for(int key=0; key < processInstanceKeys.size(); key++) {
					ProcessInstance instance = workflowEngine.getProcessInstance(processInstanceKeys.get(key));
					if(!(instance.isCompleted() || instance.isInError())) {
						isProcessRunning = true;
						context.addExecutionInformation(String.format("Contract_MigrateLandingToCore workflow already running."));
					}
				}
				if(!isProcessRunning) {
					processLauncher.launchProcess();
				}
			}
		} else {
			processLauncher.launchProcess();
		}
	}

	public String getWorkflowToLaunch() {
		return workflowToLaunch;
	}

	public void setWorkflowToLaunch(String workflowToLaunch) {
		this.workflowToLaunch = workflowToLaunch;
	}
}