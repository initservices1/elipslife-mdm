package com.elipslife.mdm.party.constraint;

import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

import com.butos.ebx.smart.validation.SmartRecordAndTableLevelValidationCheck;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class AddressConstraints extends SmartRecordAndTableLevelValidationCheck {

	@Override
	public void setup(ConstraintContextOnTable arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1) throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void executeBusinessValidations(ValueContext recordContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap, boolean isFstRec) {

		UserMessage userMsg = null;
		Date validFrom = (Date) recordContext.getValue(PartyPaths._Address._Control_ValidFrom);
		Date validTo = (Date) recordContext.getValue(PartyPaths._Address._Control_ValidTo);

		if ((validFrom != null) && (validTo != null) && validFrom.after(validTo))
		{
			userMsg = UserMessage.createError("Valid To must be after the Valid From.");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Address._Control_ValidTo),
				userMsg);
		}
		
		if (validFrom != null && validFrom.after(new Date())){
			
			userMsg = UserMessage.createError("Valid From cant be a future date.");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Address._Control_ValidFrom),
				userMsg);
			
		}
		
		String addressType = String.valueOf(recordContext.getValue(PartyPaths._Address._AddressInfo_AddressType));
		switch (addressType) {
		
		case PartyConstants.Entities.Party.AddressType.POSTAL_ADDRESS:
			physicalAddressValidationChecks(recordContext, validationInfoMap);
				break;
			
		default:
//			VM.log.kernelWarn("Address Type '".concat(addressType).concat("' doesn't exists in MDM."));
			break;
				
		}
	}

	private void physicalAddressValidationChecks(ValueContext recordContext,
			Map<SchemaNode, HashSet<UserMessage>> validationInfoMap) {
		
		UserMessage userMsg;
		String country = String.valueOf(recordContext.getValue(PartyPaths._Address._PostalAddress_Country));
		String postCode = String.valueOf(recordContext.getValue(PartyPaths._Address._PostalAddress_PostCode));
		String town = String.valueOf(recordContext.getValue(PartyPaths._Address._PostalAddress_Town));
		String street = String.valueOf(recordContext.getValue(PartyPaths._Address._PostalAddress_Street));
		
		if("null".equalsIgnoreCase(country)) {
			userMsg = UserMessage.createError("Country is mandatory.");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Address._PostalAddress_Country),
				userMsg);
		}
		
		if("null".equalsIgnoreCase(postCode)) {
			userMsg = UserMessage.createError("Post Code is mandatory.");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Address._PostalAddress_PostCode),
				userMsg);
		}
		
		if("null".equalsIgnoreCase(town)) {
			userMsg = UserMessage.createError("Town is mandatory.");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Address._PostalAddress_Town),
				userMsg);
		}
		
		if("null".equalsIgnoreCase(street)) {
			userMsg = UserMessage.createError("Street is mandatory.");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Address._PostalAddress_Street),
				userMsg);
		}
		
	}

}
