package com.elipslife.mdm.uw.trigger;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.UWPaths;
import com.elipslife.mdm.uw.core.UWCaseBean;
import com.elipslife.mdm.uw.core.UWCaseReader;
import com.elipslife.mdm.uw.core.UWCaseWriter;
import com.elipslife.mdm.uw.core.UWCaseWriterProcedureContext;
import com.elipslife.mdm.uw.staging.StagingUWCaseBean;
import com.elipslife.mdm.uw.staging.StagingUWCaseReader;
import com.elipslife.mdm.uw.staging.StagingUWCaseWriter;
import com.elipslife.mdm.uw.staging.StagingUWCaseWriterProcedureContext;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.base.text.Severity;
import com.onwbp.boot.VM;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValidationReport;

public class StagingUWCaseTableTrigger extends TableTrigger {

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {
		super.handleAfterCreate(aContext);
		
		ValidationReport validationReport = aContext.getAdaptationOccurrence().getValidationReport();
		boolean hasErrors = validationReport.hasItemsOfSeverity(Severity.ERROR) || validationReport.hasItemsOfSeverity(Severity.FATAL);
		if(!hasErrors) {
			moveUWCaseFromStagingToMaster(aContext.getSession(), aContext.getProcedureContext(), aContext.getAdaptationOccurrence());
		}
	}

	public void moveUWCaseFromStagingToMaster(Session session, ProcedureContext context, Adaptation adaptation) {
		// instantiate bean, reader and writer
		StagingUWCaseReader stagingUWCaseReader = new StagingUWCaseReader();
		StagingUWCaseBean stagingUWCaseBean = stagingUWCaseReader.createBeanFromRecord(adaptation);
		StagingUWCaseWriter stagingUWCaseWriter = new StagingUWCaseWriterProcedureContext(context);

		UWCaseReader uwCaseReader = new UWCaseReader();
		List<UWCaseBean> uwCaseBeans = uwCaseReader.findBeansByUWCaseBusId(stagingUWCaseBean.getUWCaseBusId());
		if (uwCaseBeans.size() > 1) {
			throw new RuntimeException(String.format("More than one UWCases record found with UWCases bus id",
					adaptation.getString(UWPaths._STG_UWCase._UWCaseBusId)));
		}
		
		UWCaseBean uwCaseBean = uwCaseBeans.isEmpty() ? null : uwCaseBeans.get(0);
		UWCaseWriter uwApplicationWriter = new UWCaseWriterProcedureContext(context);
		if (uwCaseBean != null) {
			// update
			Date coreRecordLastUpdateTimestamp = uwCaseBean.getLastUpdateTimeStamp();
			Date stagingRecordLastUpdateTimestamp = stagingUWCaseBean.getLastUpdateTimeStamp();
			if (stagingRecordLastUpdateTimestamp != null && (coreRecordLastUpdateTimestamp == null
					|| stagingRecordLastUpdateTimestamp.after(coreRecordLastUpdateTimestamp)
					|| stagingRecordLastUpdateTimestamp.equals(coreRecordLastUpdateTimestamp))) {
				
				transferbean(uwCaseBean, stagingUWCaseBean);
				String action = stagingUWCaseBean.getAction() != null ? stagingUWCaseBean.getAction() : null;
				if(action != null) {
					if(action.equals(Constants.RecordOperations.DELETE)) {
						uwCaseBean.setLastSyncAction(Constants.RecordOperations.DELETE);
						uwCaseBean.setIsActive(false);
					} else {
						uwCaseBean.setLastSyncAction(Constants.RecordOperations.UPDATE);
					}
				}
				uwApplicationWriter.updateBean(uwCaseBean);
				stagingUWCaseBean.setUWCaseMaster(String.valueOf(uwCaseBean.getUWCaseId()));
				stagingUWCaseWriter.updateBean(stagingUWCaseBean);

			} else {
				stagingUWCaseBean.setUWCaseMaster(String.valueOf(uwCaseBean.getUWCaseId()));
				stagingUWCaseWriter.updateBean(stagingUWCaseBean);
				VM.log.kernelWarn("***************************************************************");
				VM.log.kernelWarn("Ignoring Record Update To UWCase Master For UWCase Stage Recod : " +
						stagingUWCaseBean.getSourceSystem() + " and " +
						stagingUWCaseBean.getApplicantBusId());
			}
			
		} else {
			// create
			UWCaseBean coreUWCaseBean = new UWCaseBean();
			transferbean(coreUWCaseBean, stagingUWCaseBean);
			coreUWCaseBean.setLastSyncAction(Constants.RecordOperations.CREATE);
			Optional<UWCaseBean> coreUWCaseBeanOptional = uwApplicationWriter.createBeanOrThrow(coreUWCaseBean);
			stagingUWCaseBean.setUWCaseMaster(String.valueOf(coreUWCaseBeanOptional.get().getUWCaseId()));
			stagingUWCaseWriter.updateBean(stagingUWCaseBean);
		}
	}

	public void transferbean(UWCaseBean uwCaseBean, StagingUWCaseBean stagingUWCaseBean) {
		
		uwCaseBean.setUWCaseBusId(stagingUWCaseBean.getUWCaseBusId());
		uwCaseBean.setSourceSystem(stagingUWCaseBean.getSourceSystem());
		uwCaseBean.setDataOwner(stagingUWCaseBean.getDataOwner());
		uwCaseBean.setUWApplicationBusId(stagingUWCaseBean.getUWApplicationBusId());
		uwCaseBean.setContractNumber(stagingUWCaseBean.getContractNumber());
		uwCaseBean.setApplicantBusId(stagingUWCaseBean.getApplicantBusId());
		uwCaseBean.setApplicantRelationship(stagingUWCaseBean.getApplicantRelationship());
		uwCaseBean.setApplicantFirstName(stagingUWCaseBean.getApplicantFirstName());
		uwCaseBean.setApplicantLastName(stagingUWCaseBean.getApplicantLastName());
		uwCaseBean.setApplicantDateOfBirth(stagingUWCaseBean.getApplicantDateOfBirth());
		uwCaseBean.setApplicantSSN(stagingUWCaseBean.getApplicantSSN());
		uwCaseBean.setApplicantTown(stagingUWCaseBean.getApplicantTown());
		uwCaseBean.setApplicantPostCode(stagingUWCaseBean.getApplicantPostCode());
		uwCaseBean.setApplicantCountry(stagingUWCaseBean.getApplicantCountry());
		uwCaseBean.setApplicantClass(stagingUWCaseBean.getApplicantClass());
		uwCaseBean.setProductType(stagingUWCaseBean.getProductType());
		uwCaseBean.setEligiblityDate(stagingUWCaseBean.getEligiblityDate());
		uwCaseBean.setApplicationReason(stagingUWCaseBean.getApplicationReason());
		uwCaseBean.setApplicationReasonText(stagingUWCaseBean.getApplicationReasonText());
		uwCaseBean.setInforceCoverage(stagingUWCaseBean.getInforceCoverage());
		uwCaseBean.setAdditionalCoverage(stagingUWCaseBean.getAdditionalCoverage());
		uwCaseBean.setIsActive(stagingUWCaseBean.getIsActive());
		uwCaseBean.setCaseState(stagingUWCaseBean.getCaseState());
		uwCaseBean.setCreatedBy(stagingUWCaseBean.getCreatedBy());
		uwCaseBean.setCreationTimeStamp(stagingUWCaseBean.getCreationTimeStamp());
		uwCaseBean.setLastUpdatedBy(stagingUWCaseBean.getLastUpdatedBy());
		uwCaseBean.setLastUpdateTimeStamp(stagingUWCaseBean.getLastUpdateTimeStamp());
		uwCaseBean.setDeletionTimeStamp(stagingUWCaseBean.getDeletionTimeStamp());
	}

	@Override
	public void setup(TriggerSetupContext aContext) {

	}

}
