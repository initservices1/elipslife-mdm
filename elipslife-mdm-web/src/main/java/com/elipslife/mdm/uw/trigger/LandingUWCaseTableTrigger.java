package com.elipslife.mdm.uw.trigger;

import java.util.Date;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.uw.landing.LandingUWCaseBean;
import com.elipslife.mdm.uw.landing.LandingUWCaseReader;
import com.elipslife.mdm.uw.landing.LandingUWCaseWriter;
import com.elipslife.mdm.uw.landing.LandingUWCaseWriterProcedureContext;
import com.elipslife.mdm.uw.staging.StagingUWCaseBean;
import com.elipslife.mdm.uw.staging.StagingUWCaseWriter;
import com.elipslife.mdm.uw.staging.StagingUWCaseWriterSession;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;

public class LandingUWCaseTableTrigger extends TableTrigger {

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext aContext) throws OperationException {
		super.handleAfterCreate(aContext);
		
		// only allow DataSpace "Landing UW" to move
		if (aContext.getAdaptationHome().getKey().getName().equals(Constants.DataSpace.LANDING_UW_REFERENCE)) {
			moveUWCaseFromLandingToStaging(aContext.getSession(), aContext.getProcedureContext(),
					aContext.getAdaptationOccurrence());
		}
	}
	
	public void moveUWCaseFromLandingToStaging(Session session, ProcedureContext context, Adaptation adaptation) {
		// 1. Read landing UWCase	
		LandingUWCaseReader landingUWCaseReader = new LandingUWCaseReader();
		LandingUWCaseBean landingUWCaseBean = landingUWCaseReader.createBeanFromRecord(adaptation);
		
		// 2. Copy values into new Staging UWCase
		StagingUWCaseBean stagingUWCaseBean = new StagingUWCaseBean();
		transferbean(landingUWCaseBean, stagingUWCaseBean);
		
		// 3. Create Staging UWCase
		StagingUWCaseWriter stagingUWCaseWriter = new StagingUWCaseWriterSession(session, Constants.DataSpace.UW_REFERENCE);
		stagingUWCaseWriter.createBeanOrThrow(stagingUWCaseBean);
		
		// 4. Delete Landing UWCase
		LandingUWCaseWriter landingUWCaseWriter = new LandingUWCaseWriterProcedureContext(context);
		landingUWCaseWriter.delete(landingUWCaseBean);
	}
	
	public void transferbean(LandingUWCaseBean applicationBean, StagingUWCaseBean bean) {
		
		bean.setUWCaseBusId(applicationBean.getUWCaseBusId());
		bean.setSourceSystem(applicationBean.getSourceSystem());
		bean.setDataOwner(applicationBean.getDataOwner());
		bean.setUWApplicationBusId(applicationBean.getUWApplicationBusId());
		bean.setContractNumber(applicationBean.getContractNumber());
		bean.setApplicantBusId(applicationBean.getApplicantBusId());
		bean.setApplicantRelationship(applicationBean.getApplicantRelationship());
		bean.setApplicantFirstName(applicationBean.getApplicantFirstName());
		bean.setApplicantLastName(applicationBean.getApplicantLastName());
		bean.setApplicantDateOfBirth(applicationBean.getApplicantDateOfBirth());
		bean.setApplicantSSN(applicationBean.getApplicantSSN());
		bean.setApplicantTown(applicationBean.getApplicantTown());
		bean.setApplicantPostCode(applicationBean.getApplicantPostCode());
		bean.setApplicantCountry(applicationBean.getApplicantCountry());
		bean.setApplicantClass(applicationBean.getApplicantClass());
		bean.setProductType(applicationBean.getProductType());
		bean.setEligiblityDate(applicationBean.getEligiblityDate());
		bean.setApplicationReason(applicationBean.getApplicationReason());
		bean.setApplicationReasonText(applicationBean.getApplicationReasonText());
		bean.setIsActive(applicationBean.getIsActive());
		bean.setCaseState(applicationBean.getCaseState());
		bean.setCreatedBy(applicationBean.getCreatedBy());
		bean.setCreationTimeStamp(applicationBean.getCreationTimeStamp());
		bean.setLastUpdatedBy(applicationBean.getLastUpdatedBy());
		bean.setLastUpdateTimeStamp(applicationBean.getLastUpdateTimeStamp());
		bean.setDeletionTimeStamp(applicationBean.getDeletionTimeStamp());
		bean.setLastSyncTimeStamp(new Date());
		bean.setAction(applicationBean.getAction());
	}

	@Override
	public void setup(TriggerSetupContext aContext) {

	}

}
