package com.elipslife.ebx.api.rest.convert.bean;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;

import com.elipslife.ebx.api.rest.convert.AbstractConverter;
import com.elipslife.ebx.api.rest.models.Exposure;
import com.elipslife.ebx.api.rest.models.InsuranceContract;
import com.elipslife.ebx.api.rest.models.SynchroniseInsuranceContract;
import com.elipslife.mdm.contract.landing.LandingExposureBean;

public class ExposureConverter extends AbstractConverter<LandingExposureBean[], SynchroniseInsuranceContract> {

	@Override
	public LandingExposureBean[] convert(SynchroniseInsuranceContract synchroniseInsuranceContract)
			throws DatatypeConfigurationException {
		
		ArrayList<LandingExposureBean> landingExposureBeans = new ArrayList<>();

		InsuranceContract insuranceContract = synchroniseInsuranceContract.data;
		
		for(Exposure exposure : insuranceContract.exposures == null ? new ArrayList<Exposure>() : insuranceContract.exposures ) {
			
			LandingExposureBean landingExposureBean = new LandingExposureBean();
			
			landingExposureBean.setExposureBusId(exposure.exposureBusId);
			landingExposureBean.setName(exposure.name);
			
			if(exposure.product != null) {
				landingExposureBean.setProductBusId(exposure.product.productBusId);
				landingExposureBean.setProductGroup(exposure.product.productGroup);
				landingExposureBean.setProductName(exposure.product.productName);
				landingExposureBean.setSourceProductBusId(exposure.product.productBusId);
			}
			
			Date creationTimestamp = insuranceContract.creationTimestamp == null ? null : Date.from(insuranceContract.creationTimestamp.atZone(ZoneId.systemDefault()).toInstant());
			Date lastUpdateTimestamp = insuranceContract.lastUpdateTimestamp == null ? null : Date.from(insuranceContract.lastUpdateTimestamp.atZone(ZoneId.systemDefault()).toInstant());
			
			landingExposureBean.setSourceSystem(synchroniseInsuranceContract.context.sourceSystemId);
			landingExposureBean.setSourceContractBusId(insuranceContract.sourceContractBusId);
			landingExposureBean.setCreationTimestamp(creationTimestamp);
			landingExposureBean.setLastUpdateTimestamp(lastUpdateTimestamp);
			landingExposureBean.setAction(insuranceContract.action);
			landingExposureBean.setActionBy(synchroniseInsuranceContract.context.userId);
			
			landingExposureBeans.add(landingExposureBean);
		}
		
		return landingExposureBeans.toArray(new LandingExposureBean[0]);
	}
}