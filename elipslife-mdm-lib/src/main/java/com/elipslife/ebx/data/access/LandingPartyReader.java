package com.elipslife.ebx.data.access;

import com.elipslife.ebx.domain.bean.LandingPartyBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class LandingPartyReader extends BasePartyReader<LandingPartyBean> {

	public LandingPartyReader() {
		this(PartyConstants.DataSpace.LANDING_REFERENCE);
	}
	
	public LandingPartyReader(String dataSpace) {
		super(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_Party.getPathInSchema()));
	}
	
	
	public LandingPartyReader(AdaptationTable table) {
		super(table);
	}
	

	@Override
	public LandingPartyBean createBeanFromRecord(Adaptation record) {
		
		LandingPartyBean landingPartyBean = new LandingPartyBean();
		
		landingPartyBean.setId(record.get_int(LandingPaths._LDG_Party._Party_PartyId));
		
		landingPartyBean.setDataOwner(record.getString(LandingPaths._LDG_Party._Party_DataOwner));
		landingPartyBean.setOperatingCountry(record.getString(LandingPaths._LDG_Party._Party_OperatingCountry));
		landingPartyBean.setCrmId(record.getString(LandingPaths._LDG_Party._Party_CRMId));
		landingPartyBean.setPartyType(record.getString(LandingPaths._LDG_Party._Party_PartyType));
		landingPartyBean.setRelationshipOwner(record.getString(LandingPaths._LDG_Party._Party_RelationshipOwner));
		landingPartyBean.setDataResponsible(record.getString(LandingPaths._LDG_Party._Party_DataResponsible));
		landingPartyBean.setLanguage(record.getString(LandingPaths._LDG_Party._Party_Language));
		landingPartyBean.setIsActive((Boolean)record.get(LandingPaths._LDG_Party._Party_IsActive));
		landingPartyBean.setCorrelationId(record.getString(LandingPaths._LDG_Party._Party_CorrelationId));  // Correlation id
		landingPartyBean.setMdmStagingProcessResponse(record.getString(LandingPaths._LDG_Party._Party_MdmStagingProcessResponse)); // MDM Staging Process Response
		landingPartyBean.setFirstName(record.getString(LandingPaths._LDG_Party._Person_FirstName));
		landingPartyBean.setMiddleName(record.getString(LandingPaths._LDG_Party._Person_MiddleName));
		landingPartyBean.setLastName(record.getString(LandingPaths._LDG_Party._Person_LastName));
		landingPartyBean.setDateOfBirth(record.getDate(LandingPaths._LDG_Party._Person_DateOfBirth));
		landingPartyBean.setGender(record.getString(LandingPaths._LDG_Party._Person_Gender));
		landingPartyBean.setTitle(record.getString(LandingPaths._LDG_Party._Person_Title));
		landingPartyBean.setSalutation(record.getString(LandingPaths._LDG_Party._Person_Salutation));
		landingPartyBean.setLetterSalutation(record.getString(LandingPaths._LDG_Party._Person_LetterSalutation));
		landingPartyBean.setCivilStatus(record.getString(LandingPaths._LDG_Party._Person_CivilStatus));
		landingPartyBean.setBirthCity(record.getString(LandingPaths._LDG_Party._Person_BirthCity));
		landingPartyBean.setBirthCountry(record.getString(LandingPaths._LDG_Party._Person_BirthCountry));
		landingPartyBean.setIdDocumentNumber(record.getString(LandingPaths._LDG_Party._IdDocument_ID_DocumentNumber));
		landingPartyBean.setIdDocumentType(record.getString(LandingPaths._LDG_Party._IdDocument_ID_DocumentType));
		landingPartyBean.setIdIssueDate(record.getDate(LandingPaths._LDG_Party._IdDocument_ID_IssueDate));
		landingPartyBean.setIdExpiryDate(record.getDate(LandingPaths._LDG_Party._IdDocument_ID_ExpiryDate));
		landingPartyBean.setIdIssuingAuthority(record.getString(LandingPaths._LDG_Party._IdDocument_ID_IssuingAuthority));
		landingPartyBean.setSourceParentBusId(record.getString(LandingPaths._LDG_Party._Organisation_SourceParentBusId));
		landingPartyBean.setOrgName(record.getString(LandingPaths._LDG_Party._Organisation_OrgName));
		landingPartyBean.setOrgName2(record.getString(LandingPaths._LDG_Party._Organisation_OrgName2));
		landingPartyBean.setCompanyType(record.getString(LandingPaths._LDG_Party._Organisation_CompanyType));
		landingPartyBean.setCommercialRegNo(record.getString(LandingPaths._LDG_Party._Organisation_CommercialRegNo));
		landingPartyBean.setUidNumber(record.getString(LandingPaths._LDG_Party._Organisation_UIDNumber));
		landingPartyBean.setVatNumber(record.getString(LandingPaths._LDG_Party._Organisation_VATNumber));
		landingPartyBean.setIndustry(record.getString(LandingPaths._LDG_Party._Organisation_Industry));
		landingPartyBean.setSubIndustry(record.getString(LandingPaths._LDG_Party._Organisation_SubIndustry));
		landingPartyBean.setPreferredEmail(record.getString(LandingPaths._LDG_Party._Organisation_PreferredEmail));
		landingPartyBean.setPreferredCommChannel(record.getString(LandingPaths._LDG_Party._Organisation_PreferredCommChannel));
		landingPartyBean.setSourceOrganisationBusId(record.getString(LandingPaths._LDG_Party._ContactPerson_SourceOrganisationBusId));
		landingPartyBean.setSourceLineManagerBusId(record.getString(LandingPaths._LDG_Party._ContactPerson_SourceLineManagerBusId));
		landingPartyBean.setSourcePartnerBusId(record.getString(LandingPaths._LDG_Party._ContactPerson_SourcePartnerBusId));
		landingPartyBean.setDepartment(record.getString(LandingPaths._LDG_Party._ContactPerson_Department));
		landingPartyBean.setFunction(record.getString(LandingPaths._LDG_Party._ContactPerson_Function));
		landingPartyBean.setIsExecutor((Boolean)record.get(LandingPaths._LDG_Party._ContactPerson_IsExecutor));
		landingPartyBean.setIsLegalRepresentative((Boolean)record.get(LandingPaths._LDG_Party._ContactPerson_IsLegalRepresentative));
		landingPartyBean.setIsSeniorManager((Boolean)record.get(LandingPaths._LDG_Party._ContactPerson_IsSeniorManager));
		landingPartyBean.setIsUltimateBeneficialOwner((Boolean)record.get(LandingPaths._LDG_Party._ContactPerson_IsUltimateBeneficialOwner));
		landingPartyBean.setBoRelationship(record.getString(LandingPaths._LDG_Party._ContactPerson_BORelationship));
		landingPartyBean.setContactPersonState(record.getString(LandingPaths._LDG_Party._ContactPerson_ContactPersonState));
		landingPartyBean.setRoles(record.getString(LandingPaths._LDG_Party._Role_Roles));
		landingPartyBean.setIsVipClient((Boolean)record.get(LandingPaths._LDG_Party._Role_IsVIPClient));
		landingPartyBean.setBlacklist(record.getString(LandingPaths._LDG_Party._Role_Blacklist));
		landingPartyBean.setPartnerState(record.getString(LandingPaths._LDG_Party._Role_PartnerState));
		landingPartyBean.setCompanyPersonalNr(record.getString(LandingPaths._LDG_Party._Role_CompanyPersonalNr));
		landingPartyBean.setSocialSecurityNr(record.getString(LandingPaths._LDG_Party._Role_SocialSecurityNr));
		landingPartyBean.setSocialSecurityNrType(record.getString(LandingPaths._LDG_Party._Role_SocialSecurityNrType));
		landingPartyBean.setBrokerRegNo(record.getString(LandingPaths._LDG_Party._Role_BrokerRegNo));
		landingPartyBean.setBrokerTiering(record.getString(LandingPaths._LDG_Party._Role_BrokerTiering));
		landingPartyBean.setLineOfBusiness(record.getString(LandingPaths._LDG_Party._Role_LineOfBusiness));
		landingPartyBean.setFinancePartyBusId(record.getString(LandingPaths._LDG_Party._Financial_FinancePartyBusId));
		landingPartyBean.setPaymentTerms(record.getString(LandingPaths._LDG_Party._Financial_PaymentTerms));
		landingPartyBean.setFinanceClients(record.getString(LandingPaths._LDG_Party._Financial_FinanceClients));
		landingPartyBean.setCreditorClients(record.getString(LandingPaths._LDG_Party._Financial_CreditorClients));
		landingPartyBean.setSourceSystem(record.getString(LandingPaths._LDG_Party._Control_SourceSystem));
		landingPartyBean.setSourcePartyBusId(record.getString(LandingPaths._LDG_Party._Control_SourcePartyBusId));
		landingPartyBean.setValidFrom(record.getDate(LandingPaths._LDG_Party._Control_ValidFrom));
		landingPartyBean.setValidTo(record.getDate(LandingPaths._LDG_Party._Control_ValidTo));
		landingPartyBean.setCreationTimestamp(record.getDate(LandingPaths._LDG_Party._Control_CreationTimestamp));
		landingPartyBean.setLastUpdateTimestamp(record.getDate(LandingPaths._LDG_Party._Control_LastUpdateTimestamp));
		landingPartyBean.setAction(record.getString(LandingPaths._LDG_Party._Control_Action));
		landingPartyBean.setActionBy(record.getString(LandingPaths._LDG_Party._Control_ActionBy));
		
		return landingPartyBean;
	}

	
	@Override
	protected Path[] getPkPaths() {
		
		return new Path[] { LandingPaths._LDG_Party._Party_PartyId };
	}
	

	@Override
	protected Object[] retrievePks(LandingPartyBean bean) {

		return new Object[] { bean.getId() };
	}
	

	@Override
	protected Path getPathControlSourceSystem() {
		
		return LandingPaths._LDG_Party._Control_SourceSystem;
	}
	

	@Override
	protected Path getPathControlSourcePartyBusId() {

		return LandingPaths._LDG_Party._Control_SourcePartyBusId;
	}
	

	@Override
	protected Path getPathControlValidFrom() {

		return LandingPaths._LDG_Party._Control_ValidFrom;
	}
	

	@Override
	protected Path getPathControlValidTo() {

		return LandingPaths._LDG_Party._Control_ValidTo;
	}

	@Override
	protected Path getPathCrmId() {
		
		return LandingPaths._LDG_Party._Party_CRMId;
	}
}