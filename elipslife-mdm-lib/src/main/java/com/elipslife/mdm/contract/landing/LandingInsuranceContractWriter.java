package com.elipslife.mdm.contract.landing;

import java.util.Date;
import java.util.HashMap;

import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.LandingPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public abstract class LandingInsuranceContractWriter extends AbstractTableWriter<LandingInsuranceContractBean> {

	private LandingInsuranceContractReader landingInsuranceContractReader;
	private boolean useDefaultValues;
	
	public LandingInsuranceContractWriter(boolean useDefaultValues) {
		this(PartyConstants.DataSpace.LANDING_REFERENCE, useDefaultValues);
	}
	
	public LandingInsuranceContractWriter(String dataSpace, boolean useDefaultValues) {
		this(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(
						dataSpace,
						PartyConstants.DataSet.LANDING_MASTER),
				LandingPaths._LDG_InsuranceContract.getPathInSchema()),
				useDefaultValues);
	}
	
	public LandingInsuranceContractWriter(AdaptationTable table, boolean useDefaultValues) {
		super(table);
		
		this.landingInsuranceContractReader = new LandingInsuranceContractReader(table);
		this.useDefaultValues = useDefaultValues;
	}

	@Override
	protected void fillValuesFromBean(HashMap<Path, Object> values, LandingInsuranceContractBean bean) {
		
		values.put(LandingPaths._LDG_InsuranceContract._ContractIds_InsuranceContractId, bean.getId());
		
		if(!useDefaultValues || (bean.getDataOwner() != null && !bean.getDataOwner().isEmpty())) {
			values.put(LandingPaths._LDG_InsuranceContract._ContractIds_DataOwner, bean.getDataOwner());
		}
		
		values.put(LandingPaths._LDG_InsuranceContract._ContractIds_ContractStageBusId, bean.getContractStageBusId());
		values.put(LandingPaths._LDG_InsuranceContract._ContractIds_ContractNumber, bean.getContractNumber());
		values.put(LandingPaths._LDG_InsuranceContract._ContractIds_OfferNumber, bean.getOfferNumber());
		values.put(LandingPaths._LDG_InsuranceContract._ContractIds_ApplicationNumber, bean.getApplicationNumber());
		values.put(LandingPaths._LDG_InsuranceContract._ContractIds_ContractBusId2, bean.getContractBusId2());
		values.put(LandingPaths._LDG_InsuranceContract._ContractIds_CustomerRefNumber, bean.getCustomerRefNumber());
		
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_ContractStage, bean.getContractStage());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_ContractState, bean.getContractState());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_OfferState, bean.getOfferState());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_ApplicationState, bean.getApplicationState());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_ContractClass, bean.getContractClass());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_BusinessContractBeginDate, bean.getBusinessContractBeginDate());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_ContractExpiryDate, bean.getContractExpiryDate());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_ContractBeginDate, bean.getContractBeginDate());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_ContractEndDate, bean.getContractEndDate());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_BusinessScope, bean.getBusinessScope());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_ContractName, bean.getContractName());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_AlternativeContractName, bean.getAlternativeContractName());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_ClientSegment, bean.getClientSegment());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_DistributionAgreementBusId, bean.getDistributionAgreementBusId());
		
		
		if(!useDefaultValues || bean.getIsActive() != null) {
			values.put(LandingPaths._LDG_InsuranceContract._ContractCore_IsActive, bean.getIsActive());
		}
		
		if(!useDefaultValues || bean.getIsProfitParticipation() != null) {
			values.put(LandingPaths._LDG_InsuranceContract._ContractCore_IsProfitParticipation, bean.getIsProfitParticipation());
		}
		
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_LineOfBusiness, bean.getLineOfBusiness());
		values.put(LandingPaths._LDG_InsuranceContract._ContractCore_OperatingCountry, bean.getOperatingCountry());
		
		values.put(LandingPaths._LDG_InsuranceContract._Parties_Policyholder_SourcePolicyholderBusId, bean.getSourcePolicyholderBusId());
		values.put(LandingPaths._LDG_InsuranceContract._Parties_Policyholder_PolicyholderBusId, bean.getPolicyholderBusId());
		
		values.put(LandingPaths._LDG_InsuranceContract._Parties_Broker_SourceBrokerBusId, bean.getSourceBrokerBusId());
		values.put(LandingPaths._LDG_InsuranceContract._Parties_Broker_BrokerBusId, bean.getBrokerBusId());
		
		values.put(LandingPaths._LDG_InsuranceContract._Parties_AttachedPartner_SourceAttachedPartnerBusId, bean.getSourceAttachedPartnerBusId());
		values.put(LandingPaths._LDG_InsuranceContract._Parties_AttachedPartner_AttachedPartnerBusId, bean.getAttachedPartnerBusId());
		
		values.put(LandingPaths._LDG_InsuranceContract._Parties_AdminResponsible_SourceAdminResponsibleBusId, bean.getSourceAdminResponsibleBusId());
		values.put(LandingPaths._LDG_InsuranceContract._Parties_AdminResponsible_AdminResponsibleUserName, bean.getAdminResponsibleUserName());
		
		values.put(LandingPaths._LDG_InsuranceContract._Parties_ClaimResponsible_SourceClaimResponsibleBusId, bean.getSourceClaimResponsibleBusId());
		values.put(LandingPaths._LDG_InsuranceContract._Parties_ClaimResponsible_ClaimResponsibleUserName, bean.getClaimResponsibleUserName());
		
		values.put(LandingPaths._LDG_InsuranceContract._Parties_CaseMgmtResponsible_SourceCaseMgmtResponsibleBusId, bean.getSourceCaseMgmtResponsibleBusId());
		values.put(LandingPaths._LDG_InsuranceContract._Parties_CaseMgmtResponsible_CaseMgmtResponsibleUserName, bean.getCaseMgmtResponsibleUserName());
		
		values.put(LandingPaths._LDG_InsuranceContract._Parties_SalesResponsible_SourceSalesResponsibleBusId, bean.getSourceSalesResponsibleBusId());
		values.put(LandingPaths._LDG_InsuranceContract._Parties_SalesResponsible_SalesResponsibleUserName, bean.getSalesResponsibleUserName());
		
		values.put(LandingPaths._LDG_InsuranceContract._Control_SourceSystem, bean.getSourceSystem());
		values.put(LandingPaths._LDG_InsuranceContract._Control_SourceContractBusId, bean.getSourceContractBusId());
		values.put(LandingPaths._LDG_InsuranceContract._Control_CreationTimestamp, bean.getCreationTimestamp());
		values.put(LandingPaths._LDG_InsuranceContract._Control_LastUpdateTimestamp, bean.getLastUpdateTimestamp());
		values.put(LandingPaths._LDG_InsuranceContract._Control_LastSyncTimestamp, new Date());
		values.put(LandingPaths._LDG_InsuranceContract._Control_Action, bean.getAction());
		values.put(LandingPaths._LDG_InsuranceContract._Control_ActionBy, bean.getActionBy());
	}

	@Override
	public AbstractTableReader<LandingInsuranceContractBean> getTableReader() {
		return this.landingInsuranceContractReader;
	}
}