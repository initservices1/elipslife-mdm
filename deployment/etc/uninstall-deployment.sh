#!/bin/bash

namespace=elipslife-mdm
echo "You are going to delete the K8s Namespace ${namespace}, the connected Persistent Volumes and Database."
read -p "Are you sure [yN]? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

pvs=$(kubectl get pvc -n ${namespace} -o jsonpath={.items.*.spec.volumeName})


echo "Delete Namespace"
kubectl delete namespace ${namespace}


echo "Delete Persistent Volumes"
resouce_group=mc_butos_mixedakscluster_westeurope

storage_account=$(az storage account list -g ${resouce_group} --query "[0].name" -o tsv)
storage_key=$(az storage account keys list \
   --resource-group ${resouce_group} \
   --account-name ${storage_account} \
   --query "[0].value" -o tsv)

for pv in ${pvs}
do
  kubectl delete pv ${pv}
  az storage share delete \
   --name kubernetes-dynamic-${pv} \
   --account-name ${storage_account} \
   --account-key ${storage_key} || true
done

echo "Delete Database"
az sql db delete -g butos -s butos-dev --name ${namespace} -y

echo "Uninstall finished."
