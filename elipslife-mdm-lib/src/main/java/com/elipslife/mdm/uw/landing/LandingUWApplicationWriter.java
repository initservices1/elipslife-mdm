package com.elipslife.mdm.uw.landing;

import java.util.Date;
import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.LandingUWPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class LandingUWApplicationWriter extends AbstractTableWriter<LandingUWApplicationBean> {
    private LandingUWApplicationReader reader;

    public LandingUWApplicationWriter() {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.LANDING_UW_REFERENCE,
				Constants.DataSet.LANDING_UW_MASTER), LandingUWPaths._UWApplication.getPathInSchema()));
    }

    public LandingUWApplicationWriter(String dataspace) {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace,
				Constants.DataSet.LANDING_UW_MASTER), LandingUWPaths._UWApplication.getPathInSchema()));
    }

    public LandingUWApplicationWriter(AdaptationTable table) {
        super(table);
        reader = new LandingUWApplicationReader(table);
    }

    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, LandingUWApplicationBean bean) {
        values.put(LandingUWPaths._UWApplication._UWApplicationId, bean.getUWApplicationId());
        values.put(LandingUWPaths._UWApplication._UWApplicationBusId, bean.getUWApplicationBusId());
        values.put(LandingUWPaths._UWApplication._Context_SourceSystem, bean.getSourceSystem());
        values.put(LandingUWPaths._UWApplication._Context_DataOwner, bean.getDataOwner());
        values.put(LandingUWPaths._UWApplication._Context_OperatingCountry, bean.getOperatingCountry());
        values.put(LandingUWPaths._UWApplication._Policyholder_PolicyholderBusId, bean.getPolicyholderBusId());
        values.put(LandingUWPaths._UWApplication._Policyholder_PolicyholderName, bean.getPolicyholderName());
        values.put(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateBusId, bean.getMainAffiliateBusId());
        values.put(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateFirstName, bean.getMainAffiliateFirstName());
        values.put(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateLastName, bean.getMainAffiliateLastName());
        values.put(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateDateOfBirth, bean.getMainAffiliateDateOfBirth());
        values.put(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateSSN, bean.getMainAffiliateSSN());
        values.put(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateTown, bean.getMainAffiliateTown());
        values.put(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliatePostCode, bean.getMainAffiliatePostCode());
        values.put(LandingUWPaths._UWApplication._MainAffiliate_MainAffiliateCountry, bean.getMainAffiliateCountry());
        values.put(LandingUWPaths._UWApplication._Details_SubmissionDate, bean.getSubmissionDate());
        values.put(LandingUWPaths._UWApplication._Details_IsActive, bean.getIsActive());
        values.put(LandingUWPaths._UWApplication._DocumentSigning_DocSignId, bean.getDocSignId());
        values.put(LandingUWPaths._UWApplication._DocumentSigning_SFSignedDate, bean.getSFSignedDate());
        values.put(LandingUWPaths._UWApplication._Control_CreatedBy, bean.getCreatedBy());
        values.put(LandingUWPaths._UWApplication._Control_CreationTimestamp, bean.getCreationTimeStamp());
        values.put(LandingUWPaths._UWApplication._Control_LastUpdatedBy, bean.getLastUpdatedBy());
        values.put(LandingUWPaths._UWApplication._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
        values.put(LandingUWPaths._UWApplication._Control_DeletionTimestamp, bean.getDeletionTimeStamp());
        values.put(LandingUWPaths._UWApplication._Control_LastSyncTimestamp, new Date());
        values.put(LandingUWPaths._UWApplication._Control_Action, bean.getAction());
    }

    @Override
    public AbstractTableReader<LandingUWApplicationBean> getTableReader() {
        return reader;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub
		
	}
}
