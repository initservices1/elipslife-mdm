package com.elipslife.ebx.domain.bean.convert;

import java.util.Date;

import com.elipslife.mdm.uw.landing.LandingUWApplicationBean;
import com.elipslife.mdm.uw.landing.LandingUWCaseBean;
import com.elipslife.mdm.uw.staging.StagingUWApplicationBean;
import com.elipslife.mdm.uw.staging.StagingUWCaseBean;

public class UWConverter {

	public static StagingUWApplicationBean transferUWApplicationBean(LandingUWApplicationBean applicationBean) {

		StagingUWApplicationBean bean = new StagingUWApplicationBean();
		bean.setUWApplicationBusId(applicationBean.getUWApplicationBusId());
		bean.setSourceSystem(applicationBean.getSourceSystem());
		bean.setDataOwner(applicationBean.getDataOwner());
		bean.setOperatingCountry(applicationBean.getOperatingCountry());
		bean.setPolicyholderBusId(applicationBean.getPolicyholderBusId());
		bean.setPolicyholderName(applicationBean.getPolicyholderName());
		bean.setMainAffiliateBusId(applicationBean.getMainAffiliateBusId());
		bean.setMainAffiliateFirstName(applicationBean.getMainAffiliateFirstName());
		bean.setMainAffiliateLastName(applicationBean.getMainAffiliateLastName());
		bean.setMainAffiliateDateOfBirth(applicationBean.getMainAffiliateDateOfBirth());
		bean.setMainAffiliateSSN(applicationBean.getMainAffiliateSSN());
		bean.setMainAffiliateTown(applicationBean.getMainAffiliateTown());
		bean.setMainAffiliatePostCode(applicationBean.getMainAffiliatePostCode());
		bean.setMainAffiliateCountry(applicationBean.getMainAffiliateCountry());
		bean.setSubmissionDate(applicationBean.getSubmissionDate());
		bean.setIsActive(applicationBean.getIsActive());
		bean.setDocSignId(applicationBean.getDocSignId());
		bean.setSFSignedDate(applicationBean.getSFSignedDate());
		bean.setCreatedBy(applicationBean.getCreatedBy());
		bean.setCreationTimeStamp(applicationBean.getCreationTimeStamp());
		bean.setLastUpdatedBy(applicationBean.getLastUpdatedBy());
		bean.setLastUpdateTimeStamp(applicationBean.getLastUpdateTimeStamp());
		bean.setDeletionTimeStamp(applicationBean.getDeletionTimeStamp());
		bean.setLastSyncTimeStamp(new Date());
		bean.setAction(applicationBean.getAction());

		return bean;

	}

	public static StagingUWCaseBean transferUWCasebean(LandingUWCaseBean applicationBean) {
		
		StagingUWCaseBean bean = new StagingUWCaseBean();
		bean.setUWCaseBusId(applicationBean.getUWCaseBusId());
		bean.setSourceSystem(applicationBean.getSourceSystem());
		bean.setDataOwner(applicationBean.getDataOwner());
		bean.setUWApplicationBusId(applicationBean.getUWApplicationBusId());
		bean.setContractNumber(applicationBean.getContractNumber());
		bean.setApplicantBusId(applicationBean.getApplicantBusId());
		bean.setApplicantRelationship(applicationBean.getApplicantRelationship());
		bean.setApplicantFirstName(applicationBean.getApplicantFirstName());
		bean.setApplicantLastName(applicationBean.getApplicantLastName());
		bean.setApplicantDateOfBirth(applicationBean.getApplicantDateOfBirth());
		bean.setApplicantSSN(applicationBean.getApplicantSSN());
		bean.setApplicantTown(applicationBean.getApplicantTown());
		bean.setApplicantPostCode(applicationBean.getApplicantPostCode());
		bean.setApplicantCountry(applicationBean.getApplicantCountry());
		bean.setApplicantClass(applicationBean.getApplicantClass());
		bean.setProductType(applicationBean.getProductType());
		bean.setEligiblityDate(applicationBean.getEligiblityDate());
		bean.setApplicationReason(applicationBean.getApplicationReason());
		bean.setApplicationReasonText(applicationBean.getApplicationReasonText());
		bean.setInforceCoverage(applicationBean.getInforceCoverage());
		bean.setAdditionalCoverage(applicationBean.getAdditionalCoverage());
		bean.setIsActive(applicationBean.getIsActive());
		bean.setCaseState(applicationBean.getCaseState());
		bean.setCreatedBy(applicationBean.getCreatedBy());
		bean.setCreationTimeStamp(applicationBean.getCreationTimeStamp());
		bean.setLastUpdatedBy(applicationBean.getLastUpdatedBy());
		bean.setLastUpdateTimeStamp(applicationBean.getLastUpdateTimeStamp());
		bean.setDeletionTimeStamp(applicationBean.getDeletionTimeStamp());
		bean.setLastSyncTimeStamp(new Date());
		bean.setAction(applicationBean.getAction());
		
		return bean;
	}

}
