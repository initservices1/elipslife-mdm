package com.elipslife.mdm.hr.trigger;

import java.util.Date;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TableTrigger;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;

public class BuildingTableTrigger extends TableTrigger {

	public void handleBeforeCreate(BeforeCreateOccurrenceContext aContext) throws OperationException {
		super.handleBeforeCreate(aContext);

		aContext.getOccurrenceContextForUpdate().setValue(Constants.RecordOperations.CREATE,
				HrModelPaths._Building._Control_LastSyncAction);
		aContext.getOccurrenceContextForUpdate().setValue(new Date(),
				HrModelPaths._Building._Control_LastSyncTimestamp);
		
		String buildingBusId = (String)aContext.getOccurrenceContext().getValue(HrModelPaths._Building._Building_BuildingBusId);
		if(buildingBusId != null) {
			aContext.getOccurrenceContextForUpdate().setValue(buildingBusId, HrModelPaths._Building._Building_BuildingId);
		}
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext aContext) throws OperationException {
		super.handleBeforeModify(aContext);

		aContext.getOccurrenceContextForUpdate().setValue(Constants.RecordOperations.UPDATE,
				HrModelPaths._Building._Control_LastSyncAction);
		aContext.getOccurrenceContextForUpdate().setValue(new Date(),
				HrModelPaths._Building._Control_LastSyncTimestamp);
	}
	
	@Override
	public void setup(TriggerSetupContext aContext) {

	}
}