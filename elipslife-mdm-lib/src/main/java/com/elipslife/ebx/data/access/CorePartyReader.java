package com.elipslife.ebx.data.access;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.CorePartyBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.xpath.filter.XPathPredicateGroup.LinkingType;
import ch.butos.ebx.lib.xpath.filter.XPathPredicate.Operation;

public class CorePartyReader extends BasePartyReader<CorePartyBean> {

	public CorePartyReader() {
		super(RepositoryHelper.getTable(
				RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
				PartyPaths._Party.getPathInSchema()));
	}
	
	public CorePartyReader(String dataSpace) {
        this(RepositoryHelper.getTable(
                RepositoryHelper.getDataSet(dataSpace, PartyConstants.DataSet.PARTY_MASTER),
                PartyPaths._Party.getPathInSchema()));
    }
	
	
	public CorePartyReader(AdaptationTable table) {
		super(table);
	}

	
	@Override
	public CorePartyBean createBeanFromRecord(Adaptation record) {
		
		if(record == null) { return null; }
		
		CorePartyBean corePartyBean = new CorePartyBean();
		
		corePartyBean.setId(record.get_int(PartyPaths._Party._PartyInfo_PartyId));
		
		// corePartyBean.setMasterRecord(record.getString(PartyPaths._Party._PartyInfo_MasterRecord));
		corePartyBean.setDataOwner(record.getString(PartyPaths._Party._PartyInfo_DataOwner));
		corePartyBean.setOperatingCountry(record.getString(PartyPaths._Party._PartyInfo_OperatingCountry));
		corePartyBean.setPartyBusId(record.getString(PartyPaths._Party._PartyInfo_PartyBusId));
		corePartyBean.setCrmId(record.getString(PartyPaths._Party._PartyInfo_CRMId));
		corePartyBean.setPartyType(record.getString(PartyPaths._Party._PartyInfo_PartyType));
		corePartyBean.setPartyName(record.getString(PartyPaths._Party._PartyInfo_PartyName));
		corePartyBean.setPartyNameSearchText(record.getString(PartyPaths._Party._PartyInfo_PartyNameSearchText));
		corePartyBean.setRelationshipOwner(record.getString(PartyPaths._Party._PartyInfo_RelationshipOwner));
		corePartyBean.setDataResponsible(record.getString(PartyPaths._Party._PartyInfo_DataResponsible));
		corePartyBean.setLanguage(record.getString(PartyPaths._Party._PartyInfo_Language));
		corePartyBean.setIsActive((Boolean)record.get(PartyPaths._Party._PartyInfo_IsActive));
		corePartyBean.setLegalAddressId(record.getString(PartyPaths._Party._PartyInfo_LegalPostalAddress));
		corePartyBean.setFirstName(record.getString(PartyPaths._Party._Person_FirstName));
		corePartyBean.setMiddleName(record.getString(PartyPaths._Party._Person_MiddleName));
		corePartyBean.setLastName(record.getString(PartyPaths._Party._Person_LastName));
		corePartyBean.setDateOfBirth(record.getDate(PartyPaths._Party._Person_DateOfBirth));
		corePartyBean.setGender(record.getString(PartyPaths._Party._Person_Gender));
		corePartyBean.setTitle(record.getString(PartyPaths._Party._Person_Title));
		corePartyBean.setSalutation(record.getString(PartyPaths._Party._Person_Salutation));
		corePartyBean.setLetterSalutation(record.getString(PartyPaths._Party._Person_LetterSalutation));
		corePartyBean.setCivilStatus(record.getString(PartyPaths._Party._Person_CivilStatus));
		corePartyBean.setBirthCity(record.getString(PartyPaths._Party._Person_BirthCity));
		corePartyBean.setBirthCountry(record.getString(PartyPaths._Party._Person_BirthCountry));
		corePartyBean.setIdDocumentNumber(record.getString(PartyPaths._Party._IdDocument_ID_DocumentNumber));
		corePartyBean.setIdDocumentType(record.getString(PartyPaths._Party._IdDocument_ID_DocumentType));
		corePartyBean.setIdIssueDate(record.getDate(PartyPaths._Party._IdDocument_ID_IssueDate));
		corePartyBean.setIdExpiryDate(record.getDate(PartyPaths._Party._IdDocument_ID_ExpiryDate));
		corePartyBean.setIdIssuingAuthority(record.getString(PartyPaths._Party._IdDocument_ID_IssuingAuthority));
		corePartyBean.setParentId(record.getString(PartyPaths._Party._Organisation_ParentId));
		corePartyBean.setSourceParentBusId(record.getString(PartyPaths._Party._Organisation_SourceParentBusId));
		corePartyBean.setOrgName(record.getString(PartyPaths._Party._Organisation_OrgName));
		corePartyBean.setOrgName2(record.getString(PartyPaths._Party._Organisation_OrgName2));
		corePartyBean.setCompanyType(record.getString(PartyPaths._Party._Organisation_CompanyType));
		corePartyBean.setCommercialRegNo(record.getString(PartyPaths._Party._Organisation_CommercialRegNo));
		corePartyBean.setUidNumber(record.getString(PartyPaths._Party._Organisation_UIDNumber));
		corePartyBean.setVatNumber(record.getString(PartyPaths._Party._Organisation_VATNumber));
		corePartyBean.setIndustry(record.getString(PartyPaths._Party._Organisation_Industry));
		corePartyBean.setSubIndustry(record.getString(PartyPaths._Party._Organisation_SubIndustry));
		corePartyBean.setPreferredEmail(record.getString(PartyPaths._Party._Organisation_PreferredEmail));
		corePartyBean.setPreferredCommChannel(record.getString(PartyPaths._Party._Organisation_PreferredCommChannel));
		corePartyBean.setOrganisationId(record.getString(PartyPaths._Party._ContactPerson_OrganisationId));
		corePartyBean.setSourceOrganisationBusId(record.getString(PartyPaths._Party._ContactPerson_SourceOrganisationBusId));
		corePartyBean.setLineManagerId(record.getString(PartyPaths._Party._ContactPerson_LineManagerId));
		corePartyBean.setSourceLineManagerBusId(record.getString(PartyPaths._Party._ContactPerson_SourceLineManagerBusId));
		corePartyBean.setPartnerId(record.getString(PartyPaths._Party._ContactPerson_PartnerId));
		corePartyBean.setSourcePartnerBusId(record.getString(PartyPaths._Party._ContactPerson_SourcePartnerBusId));
		corePartyBean.setDepartment(record.getString(PartyPaths._Party._ContactPerson_Department));
		corePartyBean.setFunction(record.getString(PartyPaths._Party._ContactPerson_Function));
		corePartyBean.setIsExecutor((Boolean)record.get(PartyPaths._Party._ContactPerson_IsExecutor));
		corePartyBean.setIsLegalRepresentative((Boolean)record.get(PartyPaths._Party._ContactPerson_IsLegalRepresentative));
		corePartyBean.setIsSeniorManager((Boolean)record.get(PartyPaths._Party._ContactPerson_IsSeniorManager));
		corePartyBean.setIsUltimateBeneficialOwner((Boolean)record.get(PartyPaths._Party._ContactPerson_IsUltimateBeneficialOwner));
		corePartyBean.setBoRelationship(record.getString(PartyPaths._Party._ContactPerson_BORelationship));
		corePartyBean.setContactPersonState(record.getString(PartyPaths._Party._ContactPerson_ContactPersonState));
		corePartyBean.setRoles(record.getString(PartyPaths._Party._Role_Roles));
		corePartyBean.setIsBeneficiary((Boolean)record.get(PartyPaths._Party._Role_IsBeneficiary));
		corePartyBean.setIsInsuredPerson((Boolean)record.get(PartyPaths._Party._Role_IsInsuredPerson));
		corePartyBean.setIsInjuredPerson((Boolean)record.get(PartyPaths._Party._Role_IsInjuredPerson));
		corePartyBean.setIsElipsLifeLegalEntity((Boolean)record.get(PartyPaths._Party._Role_IsElipsLifeLegalEntity));
		corePartyBean.setIsBusinessPartner((Boolean)record.get(PartyPaths._Party._Role_IsBusinessPartner));
		corePartyBean.setIsPolicyholder((Boolean)record.get(PartyPaths._Party._Role_IsPolicyholder));
		corePartyBean.setIsReCoInsurer((Boolean)record.get(PartyPaths._Party._Role_IsReCoInsurer));
		corePartyBean.setIsVendor((Boolean)record.get(PartyPaths._Party._Role_IsVendor));
		corePartyBean.setIsMedicalServiceProvider((Boolean)record.get(PartyPaths._Party._Role_IsMedicalServiceProvider));
		corePartyBean.setIsStateRegulatoryAuthority((Boolean)record.get(PartyPaths._Party._Role_IsStateRegulatoryAuthority));
		corePartyBean.setIsDistributionPartner((Boolean)record.get(PartyPaths._Party._Role_IsDistributionPartner));
		corePartyBean.setIsBroker((Boolean)record.get(PartyPaths._Party._Role_IsBroker));
		corePartyBean.setIsAssociation((Boolean)record.get(PartyPaths._Party._Role_IsAssociation));
		corePartyBean.setIsBank((Boolean)record.get(PartyPaths._Party._Role_IsBank));
		corePartyBean.setIsConsultant((Boolean)record.get(PartyPaths._Party._Role_IsConsultant));
		corePartyBean.setIsTpa((Boolean)record.get(PartyPaths._Party._Role_IsTPA));
		corePartyBean.setIsInsurance((Boolean)record.get(PartyPaths._Party._Role_IsInsurance));
		corePartyBean.setIsVipClient((Boolean)record.get(PartyPaths._Party._Role_IsVIPClient));
		corePartyBean.setBlacklist(record.getString(PartyPaths._Party._Role_Blacklist));
		corePartyBean.setPartnerState(record.getString(PartyPaths._Party._Role_PartnerState));
		corePartyBean.setCompanyPersonalNr(record.getString(PartyPaths._Party._Role_CompanyPersonalNr));
		corePartyBean.setSocialSecurityNr(record.getString(PartyPaths._Party._Role_SocialSecurityNr));
		corePartyBean.setSocialSecurityNrType(record.getString(PartyPaths._Party._Role_SocialSecurityNrType));
		corePartyBean.setBrokerRegNo(record.getString(PartyPaths._Party._Role_BrokerRegNo));
		corePartyBean.setBrokerTiering(record.getString(PartyPaths._Party._Role_BrokerTiering));
		corePartyBean.setLineOfBusiness(record.getString(PartyPaths._Party._Role_LineOfBusiness));
		corePartyBean.setIsLhlob((Boolean)record.get(PartyPaths._Party._Role_IsLHLOB));
		corePartyBean.setIsAhlob((Boolean)record.get(PartyPaths._Party._Role_IsAHLOB));
		corePartyBean.setFinancePartyBusId(record.getString(PartyPaths._Party._Financial_FinancePartyBusId));
		corePartyBean.setPaymentTerms(record.getString(PartyPaths._Party._Financial_PaymentTerms));
		corePartyBean.setFinanceClients(record.getString(PartyPaths._Party._Financial_FinanceClients));
		corePartyBean.setCreditorClients(record.getString(PartyPaths._Party._Financial_CreditorClients));
		corePartyBean.setValidFrom(record.getDate(PartyPaths._Party._Control_ValidFrom));
		corePartyBean.setValidTo(record.getDate(PartyPaths._Party._Control_ValidTo));
		corePartyBean.setIsExpired((Boolean)record.get(PartyPaths._Party._Control_IsExpired));
		corePartyBean.setSourceSystem(record.getString(PartyPaths._Party._Control_SourceSystem));
		corePartyBean.setSourcePartyBusId(record.getString(PartyPaths._Party._Control_SourcePartyBusId));
		corePartyBean.setCreatedBy(record.getString(PartyPaths._Party._Control_CreatedBy));
		corePartyBean.setCreationTimestamp(record.getDate(PartyPaths._Party._Control_CreationTimestamp));
		corePartyBean.setLastUpdatedBy(record.getString(PartyPaths._Party._Control_LastUpdatedBy));
		corePartyBean.setLastUpdateTimestamp(record.getDate(PartyPaths._Party._Control_LastUpdateTimestamp));
		corePartyBean.setLastSyncAction(record.getString(PartyPaths._Party._Control_LastSyncAction));
		corePartyBean.setLastSyncTimestamp(record.getDate(PartyPaths._Party._Control_LastSyncTimestamp));
		
		return corePartyBean;
	}
	

	@Override
	protected Path[] getPkPaths() {
		
		return new Path[] { PartyPaths._Party._PartyInfo_PartyId };
	}
	

	@Override
	protected Object[] retrievePks(CorePartyBean bean) {

		return new Object[] { bean.getId() };
	}
	

	@Override
	protected Path getPathControlSourceSystem() {

		return PartyPaths._Party._Control_SourceSystem;
	}
	

	@Override
	protected Path getPathControlSourcePartyBusId() {

		return PartyPaths._Party._PartyInfo_PartyBusId;
	}

	
	@Override
	protected Path getPathControlValidFrom() {

		return PartyPaths._Party._Control_ValidFrom;
	}

	
	@Override
	protected Path getPathControlValidTo() {

		return PartyPaths._Party._Control_ValidTo;
	}

	@Override
	protected Path getPathCrmId() {
		
		return PartyPaths._Party._PartyInfo_CRMId;
	}
	
	
	/**
	 * 
	 * @param firstName
	 * @param lastName
	 * @param dateOfBirth
	 * @param socialSecurityNumber
	 * @param socialSecurityNumberType
	 * @param organisationId
	 * @return
	 */
	public List<CorePartyBean> findBeansIndividualPerson(String firstName, String lastName, Date dateOfBirth, String socialSecurityNumber, String socialSecurityNumberType, String organisationId) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		
		XPathPredicate predicatePartyType = new XPathPredicate(Operation.Equals, PartyPaths._Party._PartyInfo_PartyType, PartyConstants.Entities.Party.PartyType.INDIVIDUAL);
		group.addPredicate(predicatePartyType);
		XPathPredicate predicateFirstName = new XPathPredicate(firstName != null ? Operation.Equals : Operation.IsNull, PartyPaths._Party._Person_FirstName, firstName);
		group.addPredicate(predicateFirstName);
		XPathPredicate predicateLastName = new XPathPredicate(lastName != null ? Operation.Equals : Operation.IsNull, PartyPaths._Party._Person_LastName, lastName);
		group.addPredicate(predicateLastName);
		
		/*
		String dateOfBirthString = dateOfBirth != null ? simpleDateFormat.format(dateOfBirth) : null;
		XPathPredicate predicateDateOfBirth = new XPathPredicate(dateOfBirthString != null ? Operation.DateEqual : Operation.IsNull, PartyPaths._Party._Person_DateOfBirth, dateOfBirthString);
		
		XPathPredicateGroup groupDateOfBirthSocialSecurity = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		groupDateOfBirthSocialSecurity.addPredicate(predicateDateOfBirth);
		
		group.addPredicateGroup(groupDateOfBirthSocialSecurity);
		*/
		
		return readRecordsFor(group.toString()).stream().map(this::createBeanFromRecord).collect(Collectors.toList());
	}
	
	
	/**
	 * 
	 * @param organisationName
	 * @param parentOrganisationId
	 * @return
	 */
	public List<Adaptation> findBeansOrganisationUnit(String organisationName, String parentOrganisationId, String partyType) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		
		XPathPredicate predicateOrganisationName = new XPathPredicate(organisationName != null ? Operation.Equals : Operation.IsNull, PartyPaths._Party._Organisation_OrgName, organisationName);
		XPathPredicate predicateParentOrganisationId = new XPathPredicate(parentOrganisationId != null ? Operation.Equals : Operation.IsNull, PartyPaths._Party._Organisation_SourceParentBusId, parentOrganisationId);
		
		group.addPredicate(predicateOrganisationName);
		group.addPredicate(predicateParentOrganisationId);
		
		
		return readRecordsFor(group.toString());
	}
	
	
	/**
	 * 
	 * @param organisationName
	 * @param uidNumber
	 * @param commercialRegNo
	 * @return
	 */
	public List<Adaptation> findBeansCompany(String organisationName, String organisationName2, String uidNumber, String commercialRegNo, String partyType) {
		
		XPathPredicateGroup group = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.And);
		
		XPathPredicate predicateOrganisationName = new XPathPredicate(organisationName != null ? Operation.Equals : Operation.IsNull, PartyPaths._Party._Organisation_OrgName, organisationName);
		group.addPredicate(predicateOrganisationName);
		
		return readRecordsFor(group.toString());
	}
	
	public List<CorePartyBean> findPartyIdByPartyBusId(String partyBusId) {
    	
    	XPathPredicate predicate = new XPathPredicate(Operation.Equals,PartyPaths._Party._PartyInfo_PartyBusId, partyBusId);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate);
		
		return super.readBeansFor(group.toString());
	}
	
	
	/**
	 * 
	 * @return
	 */
	public List<CorePartyBean> findBeansByIsExpired(boolean isExpired) {
		
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._Party._Control_IsExpired, String.valueOf(isExpired));
		
		return readRecordsFor(predicate1.toString()).stream()
				.map(this::createBeanFromRecord)
	            .collect(Collectors.toList());
	}
	
	/**
	 * Find un resolved foreign keys from Party table
	 * 
	 * @return
	 */
	public List<CorePartyBean> findUnResolvedFks() {
		
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._Party._Organisation_ParentId, null);
		XPathPredicateGroup parentGroup = new XPathPredicateGroup(LinkingType.And);
		parentGroup.addPredicate(predicate1);
		
		XPathPredicate orgIdPredicate1 = new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._Party._ContactPerson_OrganisationId, null);
		XPathPredicateGroup orgGroup = new XPathPredicateGroup(LinkingType.And);
		orgGroup.addPredicate(orgIdPredicate1);
		
		XPathPredicate partnerPredicate1 = new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._Party._ContactPerson_PartnerId, null);
		XPathPredicateGroup partnerGroup = new XPathPredicateGroup(LinkingType.And);
		partnerGroup.addPredicate(partnerPredicate1);
		
		XPathPredicate lineMangerPredicate1 = new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._Party._ContactPerson_LineManagerId, null);
		XPathPredicateGroup lineManagerGroup = new XPathPredicateGroup(LinkingType.And);
		lineManagerGroup.addPredicate(lineMangerPredicate1);
		
		XPathPredicateGroup unResolvedFksGroup = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.Or);
		unResolvedFksGroup.addPredicateGroup(parentGroup);
		unResolvedFksGroup.addPredicateGroup(orgGroup);
		unResolvedFksGroup.addPredicateGroup(partnerGroup);
		unResolvedFksGroup.addPredicateGroup(lineManagerGroup);
		
		return super.readBeansFor(unResolvedFksGroup.toString());
	}
	
	/**
	 * Find un resolved FK's in Bulk processing
	 * 
	 * @param dataspaceCreationTimeStamp
	 * @return
	 */
	public List<CorePartyBean> findUnResolvedParentIds(Date dataspaceCreationTimeStamp) {
		
		String dateNowString = DateHelper.convertToXpathFormat(dataspaceCreationTimeStamp);
        
		XPathPredicate predicate1 = new XPathPredicate(XPathPredicate.Operation.IsNotNull, PartyPaths._Party._Organisation_SourceParentBusId, null);
		XPathPredicate predicate2 = new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._Party._Organisation_ParentId, null);
		XPathPredicate predicate3 = new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, PartyPaths._Party._Control_LastSyncTimestamp, dateNowString);
		XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
		group.addPredicate(predicate1);
		group.addPredicate(predicate2);
		group.addPredicate(predicate3);
		
		XPathPredicate sourceOrgPredicate1 = new XPathPredicate(XPathPredicate.Operation.IsNotNull, PartyPaths._Party._ContactPerson_SourceOrganisationBusId, null);
		XPathPredicate orgIdPredicate2 = new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._Party._ContactPerson_OrganisationId, null);
		XPathPredicate orgPredicate3 = new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, PartyPaths._Party._Control_LastSyncTimestamp, dateNowString);
		XPathPredicateGroup orgGroup = new XPathPredicateGroup(LinkingType.And);
		orgGroup.addPredicate(sourceOrgPredicate1);
		orgGroup.addPredicate(orgIdPredicate2);
		orgGroup.addPredicate(orgPredicate3);
		
		XPathPredicate sourcePartnerPredicate1 = new XPathPredicate(XPathPredicate.Operation.IsNotNull, PartyPaths._Party._ContactPerson_SourcePartnerBusId, null);
		XPathPredicate partnerPredicate2 = new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._Party._ContactPerson_PartnerId, null);
		XPathPredicate partnerPredicate3 = new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, PartyPaths._Party._Control_LastSyncTimestamp, dateNowString);
		XPathPredicateGroup partnerGroup = new XPathPredicateGroup(LinkingType.And);
		partnerGroup.addPredicate(sourcePartnerPredicate1);
		partnerGroup.addPredicate(partnerPredicate2);
		partnerGroup.addPredicate(partnerPredicate3);
		
		XPathPredicate sourceLineManagerPredicate1 = new XPathPredicate(XPathPredicate.Operation.IsNotNull, PartyPaths._Party._ContactPerson_SourceLineManagerBusId, null);
		XPathPredicate lineMangerPredicate2 = new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._Party._ContactPerson_LineManagerId, null);
		XPathPredicate lineManagerpredicate3 = new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, PartyPaths._Party._Control_LastSyncTimestamp, dateNowString);
		XPathPredicateGroup lineManagerGroup = new XPathPredicateGroup(LinkingType.And);
		lineManagerGroup.addPredicate(sourceLineManagerPredicate1);
		lineManagerGroup.addPredicate(lineMangerPredicate2);
		lineManagerGroup.addPredicate(lineManagerpredicate3);
		
		XPathPredicateGroup unResolvedFksGroup = new XPathPredicateGroup(XPathPredicateGroup.LinkingType.Or);
		unResolvedFksGroup.addPredicateGroup(group);
		unResolvedFksGroup.addPredicateGroup(orgGroup);
		unResolvedFksGroup.addPredicateGroup(partnerGroup);
		unResolvedFksGroup.addPredicateGroup(lineManagerGroup);
		
		return super.readBeansFor(unResolvedFksGroup.toString());
	}
	
	/**
	 * Find all non empty party roles records
	 * 
	 * @return
	 */
	
	public List<CorePartyBean> findAllPartyRoles() {
		
		XPathPredicate rolesPredicate = new XPathPredicate(XPathPredicate.Operation.IsNotNull, PartyPaths._Party._Role_Roles, null);
		
		return super.readBeansFor(rolesPredicate.toString());
	}
	
	
	public List<Adaptation> findCorePartyByLegalAddress(String legalAddress) {
		
		XPathPredicate legalAddressPredicate = new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._Party._PartyInfo_LegalPostalAddress, legalAddress);
		
		return readRecordsFor(legalAddressPredicate.toString());
	}
}