package com.elipslife.mdm.hr.landing;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.LandingHrModelPaths;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class LandingEmployeeWriter extends AbstractTableWriter<LandingEmployeeBean> {
    private LandingEmployeeReader reader;

    public LandingEmployeeWriter() {
    	this(Constants.DataSpace.LANDING_HR_REFERENCE);
    }

    public LandingEmployeeWriter(String dataspace) {
    	this(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.LANDING_HR_MASTER),
				LandingHrModelPaths._Employee.getPathInSchema()));
    }

    public LandingEmployeeWriter(AdaptationTable table) {
        super(table);
        reader = new LandingEmployeeReader(table);
    }

    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, LandingEmployeeBean bean) {
        values.put(LandingHrModelPaths._Employee._Employee_EmployeeId, bean.getEmployeeId());
        values.put(LandingHrModelPaths._Employee._Employee_DataOwner, bean.getDataOwner());
        values.put(LandingHrModelPaths._Employee._Employee_EmployeeBusId, bean.getEmployeeBusId());
        values.put(LandingHrModelPaths._Employee._Employee_UserName, bean.getUserName());
        values.put(LandingHrModelPaths._Employee._Employee_FirstName, bean.getFirstName());
        values.put(LandingHrModelPaths._Employee._Employee_PreferredFirstName, bean.getPreferredFirstName());
        values.put(LandingHrModelPaths._Employee._Employee_MiddleName, bean.getMiddleName());
        values.put(LandingHrModelPaths._Employee._Employee_LastName, bean.getLastName());
        values.put(LandingHrModelPaths._Employee._Employee_Gender, bean.getGender());
        values.put(LandingHrModelPaths._Employee._Employee_Salutation, bean.getSalutation());
        values.put(LandingHrModelPaths._Employee._Employee_Language, bean.getLanguage());
        values.put(LandingHrModelPaths._Employee._Employee_Email, bean.getEmail());
        values.put(LandingHrModelPaths._Employee._Employee_Telephone, bean.getTelephone());
        values.put(LandingHrModelPaths._Employee._Employee_Mobile, bean.getMobile());
        values.put(LandingHrModelPaths._Employee._Employee_Fax, bean.getFax());
        values.put(LandingHrModelPaths._Employee._Employee_IsActive, bean.getIsActive());
        values.put(LandingHrModelPaths._Employee._Employee_IsSystemEnabled, bean.getIsSystemEnabled());
        values.put(LandingHrModelPaths._Employee._Employment_EmploymentCategory, bean.getEmploymentCategory());
        values.put(LandingHrModelPaths._Employee._Employment_ManagementLevel, bean.getManagementLevel());
        values.put(LandingHrModelPaths._Employee._Employment_SignOffRole, bean.getSignOffRole());
        values.put(LandingHrModelPaths._Employee._JobPosition_FunctionalTitle, bean.getFunctionalTitle());
        values.put(LandingHrModelPaths._Employee._JobPosition_PositionName, bean.getPositionName());
        values.put(LandingHrModelPaths._Employee._JobPosition_JobProfileName, bean.getJobProfileName());
        values.put(LandingHrModelPaths._Employee._JobPosition_IsLineManager, bean.getIsLineManager());
        values.put(LandingHrModelPaths._Employee._JobPosition_OrganisationBusId, bean.getOrganisationBusId());
        values.put(LandingHrModelPaths._Employee._JobPosition_LineManagerBusId, bean.getLineManagerBusId());
        values.put(LandingHrModelPaths._Employee._JobPosition_DottedLineManagerBusId, bean.getDottedLineManagerBusId());
        values.put(LandingHrModelPaths._Employee._WorkplaceLocation_BuildingBusId, bean.getBuildingBusId());
        
        //Employee Private Address
        values.put(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivateStreet, bean.getPrivateStreet());
        values.put(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivatePostCode, bean.getPrivatePostCode());
        values.put(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivateTown, bean.getPrivateTown());
        values.put(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivateStateProvince, bean.getPrivateStateProvince());
        values.put(LandingHrModelPaths._Employee._EmployeePrivateAddress_PrivateCountryCode, bean.getPrivateCountryCode());
        
        
        values.put(LandingHrModelPaths._Employee._EmployerCompany_EmployerBusId, bean.getEmployerBusId());
        //values.put(LandingHrModelPaths._Employee._Control, bean.getControl());
        
        values.put(LandingHrModelPaths._Employee._Control_SourceSystem, bean.getSourceSystem());
        values.put(LandingHrModelPaths._Employee._Control_CreatedBy, bean.getCreatedBy());
        values.put(LandingHrModelPaths._Employee._Control_CreationTimestamp, bean.getCreationTimeStamp());
        values.put(LandingHrModelPaths._Employee._Control_LastUpdatedBy, bean.getLastUpdatedBy());
        values.put(LandingHrModelPaths._Employee._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
        
    }

    @Override
    public AbstractTableReader<LandingEmployeeBean> getTableReader() {
        return reader;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub
		
	}

	
}
