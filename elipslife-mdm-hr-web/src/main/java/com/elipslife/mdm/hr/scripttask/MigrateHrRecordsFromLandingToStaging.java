package com.elipslife.mdm.hr.scripttask;

import org.apache.commons.codec.binary.StringUtils;

import com.elipslife.mdm.hr.common.HrCommonRules;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.workflow.ScriptTaskBean;
import com.orchestranetworks.workflow.ScriptTaskBeanContext;

/*
 * Migrate records from Landing area to Staging area
 * 
 */

public class MigrateHrRecordsFromLandingToStaging extends ScriptTaskBean {

	private String tempDataspace;
	private String tableNamePath;
	private String errorMessage;
	private String hasMinNoOfRecords;

	@Override
	public void executeScript(ScriptTaskBeanContext aContext) throws OperationException {
		
		try {
			if (tempDataspace != null && tableNamePath != null) {

				if (tableNamePath.equals(HrModelPaths._STG_Building.getPathInSchema().format()) && StringUtils.equals(hasMinNoOfRecords, "1")) {
					// Load Building records from landing to staging
					HrCommonRules.migrateBuildingRecordsFromLandingToStaging(aContext, tempDataspace);
				}

				if (tableNamePath.equals(HrModelPaths._STG_Organisation.getPathInSchema().format()) && StringUtils.equals(hasMinNoOfRecords, "1")) {
					// Load Organisation records from landing to staging
					HrCommonRules.migrateOrganisationRecordsFromLandingToStaging(aContext, tempDataspace);
				}

				if (tableNamePath.equals(HrModelPaths._STG_Employee.getPathInSchema().format()) && StringUtils.equals(hasMinNoOfRecords, "1")) {
					// Load Employee records from landing to staging
					HrCommonRules.migrateEmployeeRecordsFromLandingToStaging(aContext, tempDataspace);
				}
			}
		}catch (Exception ex) {
			errorMessage.concat(ex.getMessage());
		}

		
	}

	public String getTableNamePath() {
		return tableNamePath;
	}

	public void setTableNamePath(String tableNamePath) {
		this.tableNamePath = tableNamePath;
	}

	public String getTempDataspace() {
		return tempDataspace;
	}

	public void setTempDataspace(String tempDataspace) {
		this.tempDataspace = tempDataspace;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getHasMinNoOfRecords() {
		return hasMinNoOfRecords;
	}

	public void setHasMinNoOfRecords(String hasMinNoOfRecords) {
		this.hasMinNoOfRecords = hasMinNoOfRecords;
	}
	
	
}
