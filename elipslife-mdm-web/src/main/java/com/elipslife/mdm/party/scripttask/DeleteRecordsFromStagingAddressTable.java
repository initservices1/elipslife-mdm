package com.elipslife.mdm.party.scripttask;

import java.util.ArrayList;
import java.util.HashMap;

import com.butos.ebx.procedure.DeleteRecords;
import com.elipslife.mdm.common.rules.WorkflowRules;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

public class DeleteRecordsFromStagingAddressTable extends ScriptTask {

    @Override
    public void executeScript(ScriptTaskContext aContext) throws OperationException {

        HashMap<String, String> contextVariables = WorkflowRules.getContextVariables(aContext);
        // String dataspace = contextVariables.get("dataspace");
        String dataset = contextVariables.get("dataset");
        String sourceSystem = contextVariables.get("sourceSystem");
        String tempDataspace = contextVariables.get("tempDataspace");

        AdaptationHome dataspaceHome = aContext.getRepository().lookupHome(HomeKey.forBranchName(tempDataspace));
        Adaptation dataSet = dataspaceHome.findAdaptationOrNull(AdaptationName.forName(dataset));
        AdaptationTable aTable = dataSet.getTable(PartyPaths._STG_Address.getPathInSchema());
        ArrayList<Adaptation> deleteRecords = new ArrayList<Adaptation>();
        RequestResult requestResult = null;

        if (sourceSystem != null && !sourceSystem.equalsIgnoreCase("null")) {
            requestResult = WorkflowRules.getRecordsByFieldValue(aTable, PartyPaths._STG_Address._Control_SourceSystem, sourceSystem);
        } else {
            requestResult = aTable.createRequestResult(null);
        }

        for (Adaptation record;(record = requestResult.nextAdaptation()) != null;) {
            deleteRecords.add(record);
        }

        if (!deleteRecords.isEmpty()) {
            DeleteRecords deleteRecordsProcedure = new DeleteRecords(deleteRecords);
            //DeleteRecordsProcedure deleteRecordsProcedure = new DeleteRecordsProcedure(deleteRecords, true, false);
            ProgrammaticService service = ProgrammaticService.createForSession(aContext.getSession(), dataspaceHome);
            ProcedureResult procedureResult = service.execute(deleteRecordsProcedure);
            if (procedureResult.hasFailed()) {
                //errorMessage.concat(procedureResult.getException().getMessage());
                throw new RuntimeException(procedureResult.getException());
            }
        }
    }
}

