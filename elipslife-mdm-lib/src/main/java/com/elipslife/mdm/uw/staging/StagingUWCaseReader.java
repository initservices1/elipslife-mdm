package com.elipslife.mdm.uw.staging;

import java.math.BigDecimal;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.path.UWPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class StagingUWCaseReader extends AbstractTableReader<StagingUWCaseBean> {

    public StagingUWCaseReader() {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._STG_UWCase.getPathInSchema()));
    }

    public StagingUWCaseReader(String dataspace) {
    	this(RepositoryHelper.getTable(			
				RepositoryHelper.getDataSet(Constants.DataSpace.UW_REFERENCE, Constants.DataSet.UW_MASTER),
				UWPaths._STG_UWCase.getPathInSchema()));
    }

    public StagingUWCaseReader(AdaptationTable table) {
		super(table);
	}

	@Override
    public StagingUWCaseBean createBeanFromRecord(Adaptation record) {
        StagingUWCaseBean bean = new StagingUWCaseBean();
        bean.setUWCaseId(record.get_int(UWPaths._STG_UWCase._UWCaseId));
        bean.setUWCaseBusId(record.getString(UWPaths._STG_UWCase._UWCaseBusId));
        bean.setUWCaseMaster(record.getString(UWPaths._STG_UWCase._UWCaseMaster));
        bean.setSourceSystem(record.getString(UWPaths._STG_UWCase._Context_SourceSystem));
        bean.setDataOwner(record.getString(UWPaths._STG_UWCase._Context_DataOwner));
        bean.setUWApplicationBusId(record.getString(UWPaths._STG_UWCase._Context_UWApplicationBusId));
        bean.setContractNumber(record.getString(UWPaths._STG_UWCase._Context_ContractNumber));
        bean.setApplicantBusId(record.getString(UWPaths._STG_UWCase._Applicant_ApplicantBusId));
        bean.setApplicantRelationship(record.getString(UWPaths._STG_UWCase._Applicant_ApplicantRelationship));
        bean.setApplicantFirstName(record.getString(UWPaths._STG_UWCase._Applicant_ApplicantFirstName));
        bean.setApplicantLastName(record.getString(UWPaths._STG_UWCase._Applicant_ApplicantLastName));
        bean.setApplicantDateOfBirth(record.getDate(UWPaths._STG_UWCase._Applicant_ApplicantDateOfBirth));
        bean.setApplicantSSN(record.getString(UWPaths._STG_UWCase._Applicant_ApplicantSSN));
        bean.setApplicantTown(record.getString(UWPaths._STG_UWCase._Applicant_ApplicantTown));
        bean.setApplicantPostCode(record.getString(UWPaths._STG_UWCase._Applicant_ApplicantPostCode));
        bean.setApplicantCountry(record.getString(UWPaths._STG_UWCase._Applicant_ApplicantCountry));
        bean.setApplicantClass(record.getString(UWPaths._STG_UWCase._Details_ApplicantClass));
        bean.setProductType(record.getString(UWPaths._STG_UWCase._Details_ProductType));
        bean.setEligiblityDate(record.getDate(UWPaths._STG_UWCase._Details_EligiblityDate));
        bean.setApplicationReason(record.getString(UWPaths._STG_UWCase._Details_ApplicationReason));
        bean.setApplicationReasonText(record.getString(UWPaths._STG_UWCase._Details_ApplicationReasonText));
        bean.setInforceCoverage((BigDecimal)record.get(UWPaths._STG_UWCase._Details_InforceCoverage));
        bean.setAdditionalCoverage((BigDecimal)record.get(UWPaths._STG_UWCase._Details_AdditionalCoverage));
        bean.setIsActive(record.get_boolean(UWPaths._STG_UWCase._State_IsActive));
        bean.setCaseState(record.getString(UWPaths._STG_UWCase._State_CaseState));
        bean.setCreatedBy(record.getString(UWPaths._STG_UWCase._Control_CreatedBy));
        bean.setCreationTimeStamp(record.getDate(UWPaths._STG_UWCase._Control_CreationTimestamp));
		bean.setLastUpdatedBy(record.getString(UWPaths._STG_UWCase._Control_LastUpdatedBy));
		bean.setLastUpdateTimeStamp(record.getDate(UWPaths._STG_UWCase._Control_LastUpdateTimestamp));
		bean.setDeletionTimeStamp(record.getDate(UWPaths._STG_UWCase._Control_DeletionTimestamp));
		bean.setLastSyncTimeStamp(record.getDate(UWPaths._STG_UWCase._Control_LastSyncTimestamp));
		bean.setAction(record.getString(UWPaths._STG_UWCase._Control_Action));
        return bean;
    }

    @Override
    public Path[] getPkPaths() {
        return new Path[]{UWPaths._STG_UWCase._UWCaseId};
    }

    @Override
    public Object[] retrievePks(StagingUWCaseBean bean) {
        return new Object[]{bean.getUWCaseId()};
    }
}
