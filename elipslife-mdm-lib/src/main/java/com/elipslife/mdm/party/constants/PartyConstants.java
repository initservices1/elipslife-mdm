package com.elipslife.mdm.party.constants;

public class PartyConstants {
	
	public static final boolean ACTIVATED = true;

	public static class SourceSystems {
		
		public static final String CRM = "CRM";
		public static final String BBTL = "BBTL";
		public static final String BBTE = "BBTE";
		public static final String CLMV = "CLMV";
	}
	
	public static class DataSpace {
		
		public static final String LANDING_REFERENCE = "LandingReference";
		public static final String PARTY_REFERENCE = "PartyReference";
		public static final String COMMON_REFERENCE = "CommonReference";
	}
	
	public static class DataSet {
		
		public static final String LANDING_MASTER = "LandingMaster";
		public static final String PARTY_MASTER = "PartyMaster";
		public static final String COMMON_REFERENCE_MASTER = "CommonRefeMaster";
		public static final String CONTRACT_MASTER = "ContractMaster";
	}
	
	public static class RecordOperations {
		
		public static final String CREATE = "C";
		public static final String UPDATE = "U";
		public static final String DELETE = "D";
	}
	
	public static class DataContextVariables {
		
		public static final String TEMP_LANDING_DATASPACE = "tmpLandingDataSpace";
		public static final String TEMP_MASTER_DATASPACE = "tmpMasterDataSpace";
		public static final String COMMIT_SIZE = "commitSize";
		public static final String ERROR_MESSAGES = "errorMessage";
	}
	
	public static class Entities {
		
		public static class Party {
			
			public static class PartyType {
				
				public static final String INDIVIDUAL = "1";
				public static final String CONTACT = "3";
				public static final String COMPANY = "4";
				public static final String ORGANISATION_UNIT = "5";
			}
			
			public static class AddressType {
				
				public static final String POSTAL_ADDRESS = "P";
				public static final String ELECTRONIC_ADDRESS = "E";
				public static final String ADDRESS_USE_ONE = "1";
			}
			
			public static class PartyRole {
				
				public static final String ELIPSLIFE_LEGALENTITY = "1";
				public static final String POLICY_HOLDER = "2";
				public static final String RE_COINSURER = "3";
				public static final String VENDOR = "4";
				public static final String STATE_REGULATORY_AUTHORITY = "5";
				public static final String DISTRIBUTION_PARTNER = "6";
				public static final String BROKER = "7";
				public static final String ASSOCIATION = "8";
				public static final String BANK = "9";
				public static final String CONSULLTANT = "10";
				public static final String TPA = "11";
				public static final String INSURANCE = "12";
				public static final String INSURED_PERSON = "13";
				public static final String INJURED_PERSON = "14";
				public static final String BENEFICIARY = "15";
				public static final String MEDICAL_SERVICE_PROVIDER = "16";
			}
			
			public static class LineOfBusiness {
				
				public static final String LHLOB = "1";
				public static final String AHLOB = "2";
			}
			
			public static class Delimiter {
				
				public static final String LINE_OF_BUSINESS = ",";
				public static final String ROLES = ",";
			}
			
			public static class ContactPersonState {
				public static final String INACTIVE = "0";
				public static final String ACTIVE = "1";
			}
			
			public static class PartnerState {
				public static final String ACTIVE_PARTNER = "0";
				public static final String PROSPECTED_PARTNER = "1";
				public static final String BLACKLISTED_PARTNER = "2";
				public static final String HISTORIC_PARTNER = "3";
			}
			
			public static class RestBulkProcessState {
			    public static final String REST_BULKPROCESS_STATE_FULL = "FULL";
			    public static final String REST_BULKPROCESS_STATE_INCR = "INCR";
			    public static final String BULKPROCESS_PROPERTY = "elipslife.mdm.party.rest.bulkprocess";
			}
			
			public static class CommitSize {
                public static final int CONTRACT_COMMIT_SIZE = 1000;
            }
		}
	}
}