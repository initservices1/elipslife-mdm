package com.elipslife.mdm.contract.scripttask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.butos.ebx.procedure.CreateRecords;
import com.butos.ebx.procedure.RecordValuesBean;
import com.butos.ebx.procedure.UpdateRecords;
import com.elipslife.ebx.domain.bean.convert.ContractConverter;
import com.elipslife.mdm.contract.ContractCoverBuilder;
import com.elipslife.mdm.contract.constants.ContractConstants;
import com.elipslife.mdm.contract.core.CoreContractCoverBean;
import com.elipslife.mdm.contract.core.CoreContractCoverReader;
import com.elipslife.mdm.contract.staging.StagingContractCoverBean;
import com.elipslife.mdm.contract.staging.StagingContractCoverReader;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.ContractPaths;
import com.elipslife.mdm.utilities.DateNullableComparator;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;

import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;

public class ContractMigrateContractCoverStagingToCoreScriptTask extends ScriptTask {

	private static int COMMITSIZE = 1;
	private static int recordCount = 0; // record count
	private static List<StagingContractCoverBean> stagingContractCoverList = new ArrayList<StagingContractCoverBean>();
	private static List<CoreContractCoverBean> coreContractCoversToCreate = new ArrayList<CoreContractCoverBean>();
	private static List<CoreContractCoverBean> coreContractCoversToUpdate = new ArrayList<CoreContractCoverBean>();
	
	@Override
	public void executeScript(ScriptTaskContext aContext) throws OperationException {
		
		// Migrate staging exposures to core
		migrateStagingContractCoverToCore(aContext);
	}
	
	/**
	 * Migrate contract cover records from staging to core
	 * 
	 * @param aContext
	 * @throws OperationException 
	 */
	private void migrateStagingContractCoverToCore(ScriptTaskContext aContext) throws OperationException {
		
		HashMap<String, String> contextVariables = getContextVariables(aContext);
		String commitSize = contextVariables.get(PartyConstants.DataContextVariables.COMMIT_SIZE);
		if (commitSize != null) {
			COMMITSIZE = Integer.parseInt(commitSize);
		}
		String errorMessage = contextVariables.get("errorMessage");
		StringBuilder stringBuilderErrorMessage = new StringBuilder(errorMessage == null ? "" : errorMessage);
		String dataSpace = contextVariables.get(PartyConstants.DataContextVariables.TEMP_MASTER_DATASPACE);
		AdaptationHome dataspaceName = RepositoryHelper.getDataSpace(dataSpace);
		
		// get staging contract covers whose contract covers master is empty
		StagingContractCoverReader stagingContractCoverReader = new StagingContractCoverReader(dataSpace);
		List<StagingContractCoverBean> stagingContractCoverBean = stagingContractCoverReader.getAllBeansWithContractCoverMasterIsEmpty();
		
		CoreContractCoverReader coreContractCoverReader = new CoreContractCoverReader(dataSpace);
		stagingContractCoverBean.forEach(sicb -> {
			try {
				recordCount++;
				boolean hasValidationErrors = stagingContractCoverReader.hasValidationErrors(new Object[] { sicb.getId() });
				// check if staging records has any validation errors
				if(hasValidationErrors) {
					return;
				}
				
				List<CoreContractCoverBean> coreContractCoverBeans = coreContractCoverReader.findBeansBySourceSystemAndCoverBusId(sicb.getSourceSystem(), sicb.getCoverBusId());
				if(coreContractCoverBeans.size() > 1) {
					throw new RuntimeException(String.format("More than one contract cover found with source system %s and cover business ID %s",
							sicb.getSourceSystem(), sicb.getCoverBusId()));
				}
				stagingContractCoverList.add(sicb);
				
				CoreContractCoverBean coreContractCoverBean = coreContractCoverBeans.isEmpty() ? null : coreContractCoverBeans.get(0);
				if(coreContractCoverBean != null) { // Update contract covers
					boolean updateCoreContractCoverBean = DateNullableComparator.getInstance().compare(sicb.getLastUpdateTimestamp(), coreContractCoverBean.getLastUpdateTimestamp()) > 0;
					
					if(!updateCoreContractCoverBean) {
						// sicb.setContractCoverMaster(String.valueOf(coreContractCoverBean.getId()));
						return;
					}
					
					ContractConverter.fillCoreContractCoverBeanWithStagingContractCoverBean(sicb, coreContractCoverBean);
					coreContractCoversToUpdate.add(coreContractCoverBean);
					ContractCoverBuilder.linkExposureReference(dataSpace, coreContractCoverBean);
				}
				else { // Create contract cover
					
					coreContractCoverBean = ContractConverter.createCoreContractCoverBeanFromStagingContractCoverBean(sicb);
					coreContractCoversToCreate.add(coreContractCoverBean);
					ContractCoverBuilder.linkExposureReference(dataSpace, coreContractCoverBean);
				}
				
				if(recordCount > COMMITSIZE) {
					// Create Core Contract Covers
					createCoreContractCoverRecords(dataSpace, coreContractCoversToCreate, dataspaceName, aContext.getSession());
					
					// Update Core contract covers
					updateCoreContractCoverRecords(dataSpace, coreContractCoversToUpdate, dataspaceName, aContext.getSession());
					recordCount = 0;
				}
				// stagingContractCoverWriter.updateBean(stagingContractCoverBean);
				
				// ContractCoverBuilder.createOrUpdateCoreContractCoverByStagingContractCover(context.getSession(), sicb);
			}
			catch(Exception ex) {
				stringBuilderErrorMessage.append(String.format("Migration error for staging contract cover with ID %s:\r\n%s", sicb.getId(), ex.toString()));
			}
		});
		
		if(!coreContractCoversToCreate.isEmpty() || !coreContractCoversToUpdate.isEmpty() || !stagingContractCoverBean.isEmpty()) {
			// Create Core Contract Covers
			createCoreContractCoverRecords(dataSpace, coreContractCoversToCreate, dataspaceName, aContext.getSession());
			
			// Update Core contract covers
			updateCoreContractCoverRecords(dataSpace, coreContractCoversToUpdate, dataspaceName, aContext.getSession());
			
			updateStagingContractCoverMaster(dataSpace, dataspaceName, aContext.getSession());
			recordCount = 0;
		}
		
		aContext.setVariableString("errorMessage", stringBuilderErrorMessage.toString());
	}
	
	/**
	 * Create core contract cover record values
	 * 
	 * @param createCoreExposures
	 * @throws OperationException 
	 */
	private static void createCoreContractCoverRecords(String dataspaceName, List<CoreContractCoverBean> createCoreContractCovers, AdaptationHome dataspace, Session session) throws OperationException {
		
		List<RecordValuesBean> coreContractCoverValuesList = new ArrayList<RecordValuesBean>();
		if(!createCoreContractCovers.isEmpty()) {  // check if create core exposures are not empty
			for (CoreContractCoverBean coreContractCoverBean : createCoreContractCovers) {
				HashMap<Path, Object> values = ContractConverter.fillContractCoverBeanValues(coreContractCoverBean);
				RecordValuesBean rvb = new RecordValuesBean(values);
				coreContractCoverValuesList.add(rvb);
			}
			createCoreContractCovers(dataspaceName, coreContractCoverValuesList, dataspace, session);
		}
	}
	
	/**
	 * Create core contract cover
	 * 
	 * @param coreExposures
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void createCoreContractCovers(String dataspaceName, List<RecordValuesBean> coreContractCoverValues, AdaptationHome dataspace, Session session) throws OperationException {
		
		if(!coreContractCoverValues.isEmpty()) {
			Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
			AdaptationTable aTable = dataset.getTable(ContractPaths._ContractCover.getPathInSchema());
			
			final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
			CreateRecords createRecordsProcedure = new CreateRecords(aTable, coreContractCoverValues);
			ProcedureResult result = service.execute(createRecordsProcedure);
			if (result.hasFailed()) {
				final OperationException exception = result.getException();
				throw exception;
			}
			coreContractCoversToCreate.clear();
			// Update staging contract cover master
			updateStagingContractCoverMaster(dataspaceName, dataspace, session);
		}
	}
	
	/**
	 * Update core contract covers records
	 * 
	 * @param updateCoreExposures
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void updateCoreContractCoverRecords(String dataspaceName, List<CoreContractCoverBean> updateCoreContractCovers, AdaptationHome dataspace, Session session) throws OperationException {
		if(!updateCoreContractCovers.isEmpty()) {  // check if create core exposures are not empty
			List<RecordValuesBean> updateContractCoverValuesList = new ArrayList<RecordValuesBean>();
			Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
			AdaptationTable aTable = dataset.getTable(ContractPaths._ContractCover.getPathInSchema());
			HashMap<Path, Object> values = null;
			RecordValuesBean rvb = null;
			for (CoreContractCoverBean coreContractCoverBean : updateCoreContractCovers) {
				values = ContractConverter.fillContractCoverBeanValues(coreContractCoverBean);
				rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(coreContractCoverBean.getId().toString())), values);
				updateContractCoverValuesList.add(rvb);
			}
			updateCoreContractCovers(dataspaceName, updateContractCoverValuesList, dataspace, session);
		}
	}
	
	/**
	 * Update core exposures
	 * 
	 * @param updateExposureValuesList
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void updateCoreContractCovers(String dataspaceName, List<RecordValuesBean> updateContractCoversList, AdaptationHome dataspace, Session session) throws OperationException {

		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
		UpdateRecords updateRecordsProcedure = new UpdateRecords(updateContractCoversList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
		}
		coreContractCoversToUpdate.clear();
		// Update staging contract cover master
		updateStagingContractCoverMaster(dataspaceName, dataspace, session);
	}
	
	/**
	 * Update staging Contract Cover Master
	 * 
	 * @param dataspace
	 * @param session
	 * @throws OperationException
	 */
	private static void updateStagingContractCoverMaster(String dataspaceName, AdaptationHome dataspace, Session session) throws OperationException {
		
		Adaptation dataset = dataspace.findAdaptationOrNull(AdaptationName.forName(ContractConstants.DataSet.CONTRACT_MASTER));
		AdaptationTable aTable = dataset.getTable(ContractPaths._STG_ContractCover.getPathInSchema());
		CoreContractCoverReader coreContractCoverReader = new CoreContractCoverReader(dataspaceName);
		
		final ProgrammaticService service = ProgrammaticService.createForSession(session, dataspace);
		List<RecordValuesBean> recordValuesBeanList = new ArrayList<RecordValuesBean>();
		List<StagingContractCoverBean> alreadyProcessedRecords = new ArrayList<StagingContractCoverBean>();
		HashMap<Path, Object> values = null;
		RecordValuesBean rvb = null;
		for (StagingContractCoverBean stagingContractCoverBean : stagingContractCoverList) {
			alreadyProcessedRecords.add(stagingContractCoverBean);
			List<CoreContractCoverBean> contractCovers = coreContractCoverReader.findBeansBySourceSystemAndCoverBusId(stagingContractCoverBean.getSourceSystem(), 
					stagingContractCoverBean.getCoverBusId());
			Integer contractCoverId = contractCovers.isEmpty() ? null : contractCovers.get(0).getId();
			values = new HashMap<Path, Object>();
			values.put(ContractPaths._STG_ContractCover._Exposure_ContractCoverMaster, String.valueOf(contractCoverId));
			rvb = new RecordValuesBean(aTable.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(stagingContractCoverBean.getId().toString())), values);
			recordValuesBeanList.add(rvb);
		}
		stagingContractCoverList.removeAll(alreadyProcessedRecords);
		
		// For Updates
		UpdateRecords updateRecordsProcedure = new UpdateRecords(recordValuesBeanList);
		ProcedureResult result = service.execute(updateRecordsProcedure);
		if (result.hasFailed()) {
			final OperationException exception = result.getException();
			throw exception;
		}
	}
	
	/**
	 * Return data context variables
	 * 
	 * @param dataContext
	 * @return
	 */
	private static HashMap<String, String> getContextVariables(com.orchestranetworks.workflow.DataContextReadOnly dataContext) {
		
		HashMap<String, String> hashMap = new HashMap<String, String>();
		Iterator<String> variableNames = dataContext.getVariableNames();
		while(variableNames.hasNext()) {
			String variableName = variableNames.next();
			String variableValue = dataContext.getVariableString(variableName);
			hashMap.put(variableName, variableValue);
		}
		
		return hashMap;
	}
}