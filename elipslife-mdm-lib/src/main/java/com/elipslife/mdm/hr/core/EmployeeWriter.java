package com.elipslife.mdm.hr.core;

import java.util.HashMap;

import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.hr.constants.HrModelPaths;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Procedure;

import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableReader;
import ch.butos.ebx.lib.legacy.wrapper.repository.AbstractTableWriter;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;


public class EmployeeWriter extends AbstractTableWriter<EmployeeBean> {
    private EmployeeReader reader;

    public EmployeeWriter() {
    	super(RepositoryHelper.getTable(RepositoryHelper.getDataSet(Constants.DataSpace.HR_REFERENCE, Constants.DataSet.HR_MASTER),
				HrModelPaths._Employee.getPathInSchema()));

		this.reader = new EmployeeReader();
    }
    
    public EmployeeWriter(String dataspace) {
    	super(RepositoryHelper.getTable(RepositoryHelper.getDataSet(dataspace, Constants.DataSet.HR_MASTER),
				HrModelPaths._Employee.getPathInSchema()));

		this.reader = new EmployeeReader(dataspace);
    }

    @Override
    protected void fillValuesFromBean(HashMap<Path, Object> values, EmployeeBean bean) {
    	values.put(HrModelPaths._Employee._Employee_EmployeeId, bean.getEmployeeId());
        values.put(HrModelPaths._Employee._Employee_DataOwner, bean.getDataOwner());
        values.put(HrModelPaths._Employee._Employee_EmployeeBusId, bean.getEmployeeBusId());
        values.put(HrModelPaths._Employee._Employee_UserName, bean.getUserName());
        values.put(HrModelPaths._Employee._Employee_FirstName, bean.getFirstName());
        values.put(HrModelPaths._Employee._Employee_PreferredFirstName, bean.getPreferredFirstName());
        values.put(HrModelPaths._Employee._Employee_MiddleName, bean.getMiddleName());
        values.put(HrModelPaths._Employee._Employee_LastName, bean.getLastName());
        values.put(HrModelPaths._Employee._Employee_Gender, bean.getGender());
        values.put(HrModelPaths._Employee._Employee_Salutation, bean.getSalutation());
        values.put(HrModelPaths._Employee._Employee_Language, bean.getLanguage());
        values.put(HrModelPaths._Employee._Employee_Email, bean.getEmail());
        values.put(HrModelPaths._Employee._Employee_Telephone, bean.getTelephone());
        values.put(HrModelPaths._Employee._Employee_Mobile, bean.getMobile());
        values.put(HrModelPaths._Employee._Employee_Fax, bean.getFax());
        values.put(HrModelPaths._Employee._Employee_IsActive, bean.getIsActive());
        values.put(HrModelPaths._Employee._Employee_IsSystemEnabled, bean.getIsSystemEnabled());
        values.put(HrModelPaths._Employee._Employment_EmploymentCategory, bean.getEmploymentCategory());
        values.put(HrModelPaths._Employee._Employment_ManagementLevel, bean.getManagementLevel());
        values.put(HrModelPaths._Employee._Employment_SignOffRole, bean.getSignOffRole());
        values.put(HrModelPaths._Employee._JobPosition_FunctionalTitle, bean.getFunctionalTitle());
        values.put(HrModelPaths._Employee._JobPosition_PositionName, bean.getPositionName());
        values.put(HrModelPaths._Employee._JobPosition_JobProfileName, bean.getJobProfileName());
        values.put(HrModelPaths._Employee._JobPosition_IsLineManager, bean.getIsLineManager());
        values.put(HrModelPaths._Employee._JobPosition_OrganisationBusId, bean.getOrganisationBusId());
        values.put(HrModelPaths._Employee._JobPosition_LineManagerBusId, bean.getLineManagerBusId());
        values.put(HrModelPaths._Employee._JobPosition_LineManagerId, bean.getLineManagerId());
        values.put(HrModelPaths._Employee._JobPosition_DottedLineManagerBusId, bean.getDottedLineManagerBusId());
        values.put(HrModelPaths._Employee._JobPosition_DottedLineManagerId, bean.getDottedLineManagerId());
        values.put(HrModelPaths._Employee._WorkplaceLocation_BuildingBusId, bean.getBuildingBusId());
        
        //Employee Private Address
        values.put(HrModelPaths._Employee._EmployeePrivateAddress_PrivateStreet, bean.getPrivateStreet());
        values.put(HrModelPaths._Employee._EmployeePrivateAddress_PrivatePostCode, bean.getPrivatePostCode());
        values.put(HrModelPaths._Employee._EmployeePrivateAddress_PrivateTown, bean.getPrivateTown());
        values.put(HrModelPaths._Employee._EmployeePrivateAddress_PrivateStateProvince, bean.getPrivateStateProvince());
        values.put(HrModelPaths._Employee._EmployeePrivateAddress_PrivateCountryCode, bean.getPrivateCountryCode());
        
        values.put(HrModelPaths._Employee._EmployerCompany_EmployerBusId, bean.getEmployerBusId());
        // Control
        values.put(HrModelPaths._Employee._Control_SourceSystem, bean.getSourceSystem());
		values.put(HrModelPaths._Employee._Control_CreatedBy, bean.getCreatedBy());
		values.put(HrModelPaths._Employee._Control_CreationTimestamp, bean.getCreationTimeStamp());
		values.put(HrModelPaths._Employee._Control_LastUpdatedBy, bean.getLastUpdatedBy());
		values.put(HrModelPaths._Employee._Control_LastUpdateTimestamp, bean.getLastUpdateTimeStamp());
        
    }

    @Override
    public AbstractTableReader<EmployeeBean> getTableReader() {
        return reader;
    }

	@Override
	protected void executeProcedure(Procedure procedure) {
		// TODO Auto-generated method stub
		
	}
}
