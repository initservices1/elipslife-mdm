package com.elipslife.ebx.data.access;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.elipslife.ebx.domain.bean.StagingAddressBean;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.schema.Path;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;
import ch.butos.ebx.lib.legacy.wrapper.repository.RepositoryHelper;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicate;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup;
import ch.butos.ebx.lib.legacy.xpath.filter.XPathPredicateGroup.LinkingType;

public class StagingAddressReader extends BaseAddressReader<StagingAddressBean> {

    public StagingAddressReader() {
        super(RepositoryHelper.getTable(
            RepositoryHelper.getDataSet(PartyConstants.DataSpace.PARTY_REFERENCE, PartyConstants.DataSet.PARTY_MASTER),
            PartyPaths._STG_Address.getPathInSchema()));
    }

    public StagingAddressReader(String dataspace) {
        this(RepositoryHelper.getTable(
            RepositoryHelper.getDataSet(dataspace, PartyConstants.DataSet.PARTY_MASTER),
            PartyPaths._STG_Address.getPathInSchema()));
    }

    public StagingAddressReader(AdaptationTable table) {
        super(table);
    }


    @Override
    public StagingAddressBean createBeanFromRecord(Adaptation record) {

        StagingAddressBean stagingAddressBean = new StagingAddressBean();

        stagingAddressBean.setId(record.get_int(PartyPaths._STG_Address._Address_AddressId));

        stagingAddressBean.setAddressType(record.getString(PartyPaths._STG_Address._Address_AddressType));
        stagingAddressBean.setAddressUsage(record.getString(PartyPaths._STG_Address._Address_AddressUse));
        stagingAddressBean.setSourceAddressBusId(record.getString(PartyPaths._STG_Address._Address_SourceAddressBusId));
        stagingAddressBean.setCoreAddressId(record.getString(PartyPaths._STG_Address._Address_CoreAddressId));
        stagingAddressBean.setStreet(record.getString(PartyPaths._STG_Address._PostalAddress_Street));
        stagingAddressBean.setStreet2(record.getString(PartyPaths._STG_Address._PostalAddress_Street2));
        stagingAddressBean.setPostCode(record.getString(PartyPaths._STG_Address._PostalAddress_PostCode));
        stagingAddressBean.setPoBox(record.getString(PartyPaths._STG_Address._PostalAddress_POBox));
        stagingAddressBean.setPoBoxPostCode(record.getString(PartyPaths._STG_Address._PostalAddress_POBoxPostCode));
        stagingAddressBean.setPoBoxTown(record.getString(PartyPaths._STG_Address._PostalAddress_POBoxTown));
        stagingAddressBean.setDistrict(record.getString(PartyPaths._STG_Address._PostalAddress_District));
        stagingAddressBean.setTown(record.getString(PartyPaths._STG_Address._PostalAddress_Town));
        stagingAddressBean.setStateProvince(record.getString(PartyPaths._STG_Address._PostalAddress_StateProvince));
        stagingAddressBean.setCountry(record.getString(PartyPaths._STG_Address._PostalAddress_Country));
        stagingAddressBean.setCorrelationId(record.getString(PartyPaths._STG_Address._Address_CorrelationId));
        stagingAddressBean.setSourceSystem(record.getString(PartyPaths._STG_Address._Control_SourceSystem));
        stagingAddressBean.setSourcePartyBusId(record.getString(PartyPaths._STG_Address._Control_SourcePartyBusId));
        stagingAddressBean.setValidFrom(record.getDate(PartyPaths._STG_Address._Control_ValidFrom));
        stagingAddressBean.setValidTo(record.getDate(PartyPaths._STG_Address._Control_ValidTo));
        stagingAddressBean.setCreationTimestamp(record.getDate(PartyPaths._STG_Address._Control_CreationTimestamp));
        stagingAddressBean.setLastUpdateTimestamp(record.getDate(PartyPaths._STG_Address._Control_LastUpdateTimestamp));
        stagingAddressBean.setAction(record.getString(PartyPaths._STG_Address._Control_Action));
        stagingAddressBean.setActionBy(record.getString(PartyPaths._STG_Address._Control_ActionBy));
        stagingAddressBean.setElectronicAddressType(record.getString(PartyPaths._STG_Address._ElectronicAddress_ElectronicAddressType));
        stagingAddressBean.setAddress(record.getString(PartyPaths._STG_Address._ElectronicAddress_Address));

        return stagingAddressBean;
    }


    @Override
    protected Path[] getPkPaths() {

        return new Path[] {PartyPaths._STG_Address._Address_AddressId};
    }


    @Override
    protected Object[] retrievePks(StagingAddressBean bean) {

        return new Object[] {bean.getId()};
    }


    /**
     * 
     * @param coreAddressId
     * @return
     */
    public List<StagingAddressBean> findBeansByCoreAddressId(String coreAddressId) {

        XPathPredicate predicate1 =
            (coreAddressId == null || coreAddressId.isEmpty())
                ? new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._STG_Address._Address_CoreAddressId, coreAddressId)
                : new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._STG_Address._Address_CoreAddressId, coreAddressId);

        return readRecordsFor(predicate1.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }


    /**
     * Gets beans by core address within current validation date
     * 
     * @param coreAddressId
     * @return
     */
    public List<StagingAddressBean> findBeansByCoreAddressIdUpToDate(String coreAddressId) {

        Date dateNow = new Date();

        return findBeansByCoreAddressIdAndDate(coreAddressId, dateNow);
    }


    /**
     * Gets beans by core address within given validation date
     * 
     * @param coreAddressId
     * @param date
     * @return
     */
    public List<StagingAddressBean> findBeansByCoreAddressIdAndDate(String coreAddressId, Date date) {

        XPathPredicate predicate1 =
            (coreAddressId == null || coreAddressId.isEmpty())
                ? new XPathPredicate(XPathPredicate.Operation.IsNull, PartyPaths._STG_Address._Address_CoreAddressId, coreAddressId)
                : new XPathPredicate(XPathPredicate.Operation.Equals, PartyPaths._STG_Address._Address_CoreAddressId, coreAddressId);

        date = removeTime(date);

        XPathPredicateGroup groupValidDate1 = new XPathPredicateGroup(LinkingType.And);
        String dateNowString = simpleDateFormat.format(date);
        XPathPredicate predicate3 =
            new XPathPredicate(XPathPredicate.Operation.DateLessThan, PartyPaths._STG_Address._Control_ValidFrom, dateNowString);
        groupValidDate1.addPredicate(predicate3);

        XPathPredicate predicate4 =
            new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, PartyPaths._STG_Address._Control_ValidTo, dateNowString);
        groupValidDate1.addPredicate(predicate4);


        XPathPredicateGroup groupValidDate2 = new XPathPredicateGroup(LinkingType.Or);
        XPathPredicate predicate5 = new XPathPredicate(XPathPredicate.Operation.DateEqual, PartyPaths._STG_Address._Control_ValidFrom, dateNowString);
        groupValidDate2.addPredicate(predicate5);

        XPathPredicate predicate6 = new XPathPredicate(XPathPredicate.Operation.DateEqual, PartyPaths._STG_Address._Control_ValidTo, dateNowString);
        groupValidDate2.addPredicate(predicate6);

        XPathPredicateGroup groupValidDate = new XPathPredicateGroup(LinkingType.Or);
        groupValidDate.addPredicateGroup(groupValidDate1);
        groupValidDate.addPredicateGroup(groupValidDate2);


        XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
        group.addPredicate(predicate1);
        group.addPredicateGroup(groupValidDate);

        return readRecordsFor(group.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }

    /**
     * Get staging Addresses which are created after the mentioned lastSyncTimestamp
     * 
     * @param lastSyncTimestamp
     * @return
     */
    public List<StagingAddressBean> getStagingAddressesBeansByLastSyncTimeStamp(Date lastSyncTimestamp) {

        String dateNowString = DateHelper.convertToXpathFormat(lastSyncTimestamp);

        XPathPredicate predicate1 =
            new XPathPredicate(XPathPredicate.Operation.DateGreaterThan, PartyPaths._STG_Address._Control_LastSyncTimestamp, dateNowString);
        XPathPredicateGroup group = new XPathPredicateGroup(LinkingType.And);
        group.addPredicate(predicate1);

        return readRecordsFor(group.toString()).stream()
            .map(this::createBeanFromRecord)
            .collect(Collectors.toList());
    }

    @Override
    protected Path getPathAddressId() {

        return PartyPaths._STG_Address._Address_AddressId;
    }


    @Override
    protected Path getPathSourceAddressBusId() {

        return PartyPaths._STG_Address._Address_SourceAddressBusId;
    }


    @Override
    protected Path getPathControlSourceSystem() {

        return PartyPaths._STG_Address._Control_SourceSystem;
    }


    @Override
    protected Path getPathControlSourcePartyBusId() {

        return PartyPaths._STG_Address._Control_SourcePartyBusId;
    }


    @Override
    protected Path getPathControlValidFrom() {

        return PartyPaths._STG_Address._Control_ValidFrom;
    }


    @Override
    protected Path getPathControlValidTo() {

        return PartyPaths._STG_Address._Control_ValidTo;
    }
}
