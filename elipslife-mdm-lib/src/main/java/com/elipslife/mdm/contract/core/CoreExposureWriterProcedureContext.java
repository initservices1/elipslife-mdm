package com.elipslife.mdm.contract.core;

import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;

public class CoreExposureWriterProcedureContext extends CoreExposureWriter {

	private ProcedureContext procedureContext;
	
	public CoreExposureWriterProcedureContext(ProcedureContext procedureContext) {
		this.procedureContext = procedureContext;
	}
	
	@Override
	protected void executeProcedure(Procedure procedure) {
		try {
			procedure.execute(this.procedureContext);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}