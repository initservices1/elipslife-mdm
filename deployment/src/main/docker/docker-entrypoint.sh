#!/bin/bash
set -e

mkdir -p ${EBX_HOME}/repository/archives/
cp ${EBX_HOME}/prepopulate/archives/* ${EBX_HOME}/repository/archives/

exec "$@"
