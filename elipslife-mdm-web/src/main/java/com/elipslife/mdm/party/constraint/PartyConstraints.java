package com.elipslife.mdm.party.constraint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.butos.ebx.smart.validation.SmartRecordAndTableLevelValidationCheck;
import com.elipslife.ebx.data.access.CorePartyIdentifierReader;
import com.elipslife.ebx.domain.bean.CorePartyIdentifierBean;
import com.elipslife.mdm.common.constants.Constants;
import com.elipslife.mdm.party.constants.PartyConstants;
import com.elipslife.mdm.path.PartyPaths;
import com.onwbp.base.text.UserMessage;
import com.onwbp.boot.VM;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.schema.SchemaNode;

import ch.butos.ebx.lib.legacy.util.date.DateHelper;

/**
 *
 *
 */
public class PartyConstraints extends SmartRecordAndTableLevelValidationCheck
{
	@Override
	public void setup(final ConstraintContextOnTable arg0) {
	}

	@Override
	public String toUserDocumentation(final Locale arg0, final ValueContext arg1) throws InvalidSchemaException	{
		//TODO auto generated : to check
		return null;
	}

	@Override
	protected void executeBusinessValidations(
		final ValueContext recordContext,
		final Map<SchemaNode, HashSet<UserMessage>> validationInfoMap,
		final boolean isFstRec)	{
		
		UserMessage userMsg = null;
		Date validFrom = (Date) recordContext.getValue(PartyPaths._Party._Control_ValidFrom);
		Date validTo = (Date) recordContext.getValue(PartyPaths._Party._Control_ValidTo);
		
		validateCrmIdWithPartyBusId(recordContext, validationInfoMap);

		if ((validFrom != null) && (validTo != null) && validFrom.after(validTo))
		{
			userMsg = UserMessage.createError("Valid To must be after the Valid From.");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Party._Control_ValidTo),
				userMsg);
		}
		
		
		if (validFrom != null && validFrom.after(new Date())){
			
			userMsg = UserMessage.createError("Valid From cant be a future date.");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Party._Control_ValidFrom),
				userMsg);
		}
		
		
		String partyType = String.valueOf(recordContext.getValue(PartyPaths._Party._PartyInfo_PartyType));
		
		switch (partyType) {
		
		case PartyConstants.Entities.Party.PartyType.INDIVIDUAL:
		case PartyConstants.Entities.Party.PartyType.CONTACT:
			personTypeValidationChecks(recordContext, validationInfoMap);
			break;
			
		case PartyConstants.Entities.Party.PartyType.COMPANY:
			orgValidationChecks(recordContext, validationInfoMap);
			break;
			
		case PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT:
			organisationUnitValidationChecks(recordContext, validationInfoMap);
			break;
			
		default:
			VM.log.kernelWarn("Party Type '".concat(partyType).concat("' doesn't exists in MDM."));
			break;
		}
		
		
		switch (partyType) {
		case PartyConstants.Entities.Party.PartyType.COMPANY:
		case PartyConstants.Entities.Party.PartyType.ORGANISATION_UNIT:
			validateBusinessId(
					recordContext,
					validationInfoMap,PartyPaths._Party._Organisation_SourceParentBusId,
					PartyPaths._Party._Organisation_ParentId);
			break;
		case PartyConstants.Entities.Party.PartyType.CONTACT:
			
			validateBusinessId(
					recordContext,
					validationInfoMap,PartyPaths._Party._ContactPerson_SourceOrganisationBusId,
					PartyPaths._Party._ContactPerson_OrganisationId);
			
			validateBusinessId(
					recordContext,
					validationInfoMap,PartyPaths._Party._ContactPerson_SourcePartnerBusId,
					PartyPaths._Party._ContactPerson_PartnerId);
			
			validateBusinessId(
					recordContext,
					validationInfoMap,PartyPaths._Party._ContactPerson_SourceLineManagerBusId,
					PartyPaths._Party._ContactPerson_LineManagerId);
			
			break;
		}
	}
	
	
	/**
	 * 
	 * @param recordContext
	 * @param validationInfoMap
	 * @param sourceBusinessIdPath
	 * @param linkedPartyIdPath
	 */
	private void validateBusinessId(
			final ValueContext recordContext,
			final Map<SchemaNode, HashSet<UserMessage>> validationInfoMap,
			Path sourceBusinessIdPath,
			Path linkedPartyIdPath) {
		
		String sourceBusinessId = (String)recordContext.getValue(sourceBusinessIdPath);
		String linkedPartyId = (String)recordContext.getValue(linkedPartyIdPath);
		
		if(sourceBusinessId == null || sourceBusinessId.isEmpty()) {
			return;
		}
		
		if(linkedPartyId == null || linkedPartyId.isEmpty()) {
			UserMessage userMessage = UserMessage.createWarning("Linked party not found");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(linkedPartyIdPath),
				userMessage);
			
			return;
		}
		
		String sourceSystem = (String)recordContext.getValue(PartyPaths._Party._Control_SourceSystem);
		CorePartyIdentifierReader corePartyIdentifierReader = new CorePartyIdentifierReader();
		List<CorePartyIdentifierBean> corePartyIdentifierBeans = corePartyIdentifierReader.getBeansBySourceSystem(sourceSystem, sourceBusinessId);
		CorePartyIdentifierBean corePartyIdentifierBean = corePartyIdentifierBeans.size() > 0 ? corePartyIdentifierBeans.get(0) : null;
		
		if(corePartyIdentifierBean == null) {
			UserMessage userMessage = UserMessage.createWarning("Linked party not found");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(linkedPartyIdPath),
				userMessage);
			
			return;
		}
	}
	
	/**
	 * 
	 * @param recordContext
	 * @param validationInfoMap
	 */
	private void orgValidationChecks(final ValueContext recordContext,
			final Map<SchemaNode, HashSet<UserMessage>> validationInfoMap) {
		UserMessage userMsg;
		String OrgName = String.valueOf(recordContext.getValue(PartyPaths._Party._Organisation_OrgName));
		
		if("null".equalsIgnoreCase(OrgName)) {
			userMsg = UserMessage.createError("Org Name is Mandatory");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Party._Organisation_OrgName),
				userMsg);
		}
	}

	/**
	 * 
	 * @param recordContext
	 * @param validationInfoMap
	 * @throws ParseException 
	 * @throws PathAccessException 
	 */
	private void organisationUnitValidationChecks(final ValueContext recordContext, final Map<SchemaNode, HashSet<UserMessage>> validationInfoMap) {
		
		UserMessage userMsg;
		Date dateofBirth = (Date)recordContext.getValue(PartyPaths._Party._Person_DateOfBirth);
		try {
			if(dateofBirth != null && !(dateofBirth.equals(new SimpleDateFormat("yyyy-MM-dd").parse("0001-01-01")) || dateofBirth.equals(new SimpleDateFormat("yyyy-MM-dd").parse("9999-12-31")))) {
				if(dateofBirth.after(new Date())) {
					userMsg = UserMessage.createError("Date of Birth cant be a future date.");
					this.insertUserMsg(validationInfoMap, recordContext.getNode(PartyPaths._Party._Person_DateOfBirth), userMsg);
				}
				else {
					LocalDate locaDate = LocalDate.now();
					LocalDate dateOfBirthLocal = dateofBirth.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					
					if(!Period.between(dateOfBirthLocal.plusYears(130), locaDate).isNegative()) {
					userMsg = UserMessage.createError("Age can't be greater than 130 years.");
					this.insertUserMsg(validationInfoMap, recordContext.getNode(PartyPaths._Party._Person_DateOfBirth), userMsg);
					}
				}
			}
		} catch (PathAccessException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param recordContext
	 * @param validationInfoMap
	 */
	private void personTypeValidationChecks(final ValueContext recordContext,
			final Map<SchemaNode, HashSet<UserMessage>> validationInfoMap) {
		UserMessage userMsg;
		String firstName = String.valueOf(recordContext.getValue(PartyPaths._Party._Person_FirstName));
		String lastName = String.valueOf(recordContext.getValue(PartyPaths._Party._Person_LastName));
		Date dateofBirth = (Date)recordContext.getValue(PartyPaths._Party._Person_DateOfBirth);
		
		if("null".equalsIgnoreCase(firstName)) {
			userMsg = UserMessage.createError("First Name is Mandatory");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Party._Person_FirstName),
				userMsg);
		}
		
		if("null".equalsIgnoreCase(lastName)) {
			userMsg = UserMessage.createError("Last Name is Mandatory");
			this.insertUserMsg(
				validationInfoMap,
				recordContext.getNode(PartyPaths._Party._Person_LastName),
				userMsg);
		}
		
		try 
		{
			if(dateofBirth != null && !(dateofBirth.equals(new SimpleDateFormat("yyyy-MM-dd").parse("01-01-01")) || dateofBirth.equals(new SimpleDateFormat("yyyy-MM-dd").parse("9999-12-31")))) {
				if(dateofBirth.after(new Date())) {
					userMsg = UserMessage.createError("Date of Birth cant be a future date.");
					this.insertUserMsg(validationInfoMap, recordContext.getNode(PartyPaths._Party._Person_DateOfBirth), userMsg);
				}
				else {
					LocalDate locaDate = LocalDate.now();
					LocalDate dateOfBirthLocal = dateofBirth.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					
					if(!Period.between(dateOfBirthLocal.plusYears(130), locaDate).isNegative()) {
					userMsg = UserMessage.createError("Age can't be greater than 130 years.");
					this.insertUserMsg(validationInfoMap, recordContext.getNode(PartyPaths._Party._Person_DateOfBirth), userMsg);
					}
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 1. if CRMId is not equal to Party Bus Id, Show the warning message
	 * 2. Check PartyIdentifier record with associated Party and then check the sourceSystem as CRM and this SrcParty Bus Id is not equal to the
	 *    Party Bus Id. Then flag a warning message.
	 *    
	 * @param recordContext
	 * @param validationInfoMap
	 */
	private void validateCrmIdWithPartyBusId(final ValueContext recordContext, final Map<SchemaNode, HashSet<UserMessage>> validationInfoMap) {
		
		UserMessage userMessage;
		String crmId = (String) recordContext.getValue(PartyPaths._Party._PartyInfo_CRMId);
		String partyBusId = (String) recordContext.getValue(PartyPaths._Party._PartyInfo_PartyBusId);
		
		// if CRMId is not equal to Party Bus Id, Show the warning message
		if(crmId != null & partyBusId != null) {
			
				if(!crmId.equals(partyBusId)) {
					userMessage = UserMessage.createWarning("Party Bus Id is not equal to CRM Id");
					this.insertUserMsg(validationInfoMap, recordContext.getNode(PartyPaths._Party._PartyInfo_PartyBusId), userMessage);
					return ;
				}
		}
		
		String partyId = String.valueOf(recordContext.getValue(PartyPaths._Party._PartyInfo_PartyId));
		CorePartyIdentifierReader corePartyIdentifierReader = new CorePartyIdentifierReader();
		List<CorePartyIdentifierBean> corePartyIdentifierBeans = corePartyIdentifierReader.findBeansByPartyId(partyId);
		corePartyIdentifierBeans.forEach(cpib -> {
			String sourceSystem = cpib.getSourceSystem();
			if(sourceSystem != null) {
				if(sourceSystem.equals(Constants.SourceSystems.CRM)) {
					String srcPartyBusId = cpib.getSourcePartyBusId();
					if(srcPartyBusId != null && partyBusId != null) {
						if(!srcPartyBusId.equals(partyBusId)) {
							UserMessage usrMessage = UserMessage.createWarning("Party Bus Id is not equal to CRM Id");
							this.insertUserMsg(validationInfoMap, recordContext.getNode(PartyPaths._Party._PartyInfo_PartyBusId), usrMessage);
							return ;
						}
					}
					
				}
			}
		});
	}
}